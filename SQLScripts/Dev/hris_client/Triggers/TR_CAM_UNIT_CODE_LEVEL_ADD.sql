CREATE TRIGGER [dbo].[TR_CAM_UNIT_CODE_LEVEL_ADD] 
   ON  [dbo].[TB_CAM_UNIT]
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	DECLARE @unitId AS INT, @parentId AS INT;
	DECLARE @codeLevel AS HIERARCHYID;

	SELECT @unitId = INSERTED.UNIT_ID, @parentId = INSERTED.PARENT_ID FROM INSERTED;

	IF @parentId IS NULL
		SET @codeLevel = CONVERT(hierarchyid, CONCAT('/', @unitId, '/'))
	ELSE
		SET @codeLevel = CONVERT(hierarchyid, CONCAT((SELECT CODE_LEVEL.ToString() FROM TB_CAM_UNIT WHERE UNIT_ID = @parentId), @unitId, '/'));

	UPDATE TB_CAM_UNIT SET CODE_LEVEL = @codeLevel WHERE UNIT_ID = @unitId;
END
