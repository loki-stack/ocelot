CREATE TRIGGER [dbo].[TR_CAM_UNIT_CODE_LEVEL_UPDATE] 
   ON  [dbo].[TB_CAM_UNIT]
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	DECLARE @unitId AS INT, @parentId AS INT, @parentIdOld AS INT;
	DECLARE @codeLevel AS HIERARCHYID, @codeLevelOld AS HIERARCHYID;

	SELECT @unitId = INSERTED.UNIT_ID, @parentId = INSERTED.PARENT_ID FROM INSERTED;
	SELECT @parentIdOld = DELETED.PARENT_ID, @codeLevelOld = DELETED.CODE_LEVEL FROM DELETED;

	IF (@parentId IS NULL AND @parentIdOld IS NOT NULL) OR (@parentId IS NOT NULL AND @parentIdOld IS NULL) OR @parentId <> @parentIdOld
	BEGIN
		IF @parentId IS NULL
			SET @codeLevel = CONVERT(HIERARCHYID, CONCAT('/', @unitId, '/'))
		ELSE
			SET @codeLevel = CONVERT(HIERARCHYID, CONCAT((SELECT CODE_LEVEL.ToString() FROM TB_CAM_UNIT WHERE UNIT_ID = @parentId), @unitId, '/'));

		UPDATE TB_CAM_UNIT SET CODE_LEVEL = @codeLevel WHERE UNIT_ID = @unitId;

		;WITH CTE
		AS
		(
			SELECT UNIT_ID, PARENT_ID
			FROM TB_CAM_UNIT T1
        
			WHERE UNIT_ID = @unitId
			UNION ALL
			SELECT
				T3.UNIT_ID, T3.PARENT_ID
			FROM CTE T2
				JOIN TB_CAM_UNIT T3
					ON T2.UNIT_ID = T3.PARENT_ID
		)

		UPDATE TB_CAM_UNIT SET CODE_LEVEL = CONVERT(HIERARCHYID, REPLACE(CODE_LEVEL.ToString(), @codeLevelOld.ToString(), @codeLevel.ToString())) WHERE UNIT_ID IN (SELECT UNIT_ID FROM CTE WHERE UNIT_ID <> @unitId)
	END
END
