CREATE TABLE TB_CFG_JOB_GRADE (
	[JOB_GRADE_ID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[JOB_GRADE_NAME] [nvarchar](100) NULL,
	[JOB_GRADE_LEVEL] [int] NULL,
	[STATUS_CD] [nvarchar](10) NULL,
	[REMARK] [nvarchar](500) NULL,
	[CREATED_BY] [nvarchar](100) NULL,
	[CREATED_DT] [datetimeoffset](7) NULL,
	[MODIFIED_BY] [nvarchar](100) NULL,
	[MODIFIED_DT] [datetimeoffset](7) NULL
);