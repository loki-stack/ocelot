CREATE TABLE TB_CFG_COST_CENTER (
	[COST_CENTER_ID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[COST_CENTER_CD] [nvarchar](100) NULL,
	[COST_CENTER_NAME] [nvarchar](100) NULL,
	[STATUS_CD] [nvarchar](10) NULL,
	[REMARK] [nvarchar](500) NULL,
	[CREATED_BY] [nvarchar](100) NULL,
	[CREATED_DT] [datetimeoffset](7) NULL,
	[MODIFIED_BY] [nvarchar](100) NULL,
	[MODIFIED_DT] [datetimeoffset](7) NULL
);