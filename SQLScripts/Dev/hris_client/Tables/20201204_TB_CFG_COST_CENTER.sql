CREATE TABLE TB_CFG_COST_CENTER (
	[COST_CENTER_ID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[COST_CENTER_CD] [nvarchar](10) NULL,
	[COST_CENTER_NAME] [nvarchar](100) NULL,
	[SPECIFIC_CLIENT_ID] [int] NULL,
	[SPECIFIC_ENTITY_ID] [int] NULL,
	[STATUS_CD] [nvarchar](10) NULL,
	[REMARK] [nvarchar](200) NULL,
	[CREATED_BY] [nvarchar](100) NULL,
	[CREATED_DT] [datetimeoffset](7) NULL,
	[MODIFIED_BY] [nvarchar](100) NULL,
	[MODIFIED_DT] [datetimeoffset](7) NULL
);