CREATE TABLE [dbo].[TB_PAY_PAYROLL_RUN_IMPORT](
	[PAYROLL_RUN_IMPORT_ID] [int] IDENTITY(1,1) NOT NULL,
	[PAYROLL_RUN_ID] [int] NOT NULL,
	[FILE_NAME] [nvarchar](max) NOT NULL,
	[CONTENT] [varbinary](max) NOT NULL,
	[CREATED_DT] [datetimeoffset](7) NOT NULL,
	[CREATED_BY] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_TB_PAY_PAYROLL_RUN_IMPORT] PRIMARY KEY CLUSTERED 
(
	[PAYROLL_RUN_IMPORT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[TB_PAY_PAYROLL_RUN_IMPORT]  WITH CHECK ADD  CONSTRAINT [FK_TB_PAY_PAYROLL_RUN_IMPORT_TB_PAY_PAYROLL_RUN] FOREIGN KEY([PAYROLL_RUN_ID])
REFERENCES [dbo].[TB_PAY_PAYROLL_RUN] ([PAYROLL_RUN_ID])
GO
ALTER TABLE [dbo].[TB_PAY_PAYROLL_RUN_IMPORT] CHECK CONSTRAINT [FK_TB_PAY_PAYROLL_RUN_IMPORT_TB_PAY_PAYROLL_RUN]
GO
