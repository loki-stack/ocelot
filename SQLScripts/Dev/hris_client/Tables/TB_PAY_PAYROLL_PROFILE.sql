CREATE TABLE [dbo].[TB_PAY_PAYROLL_PROFILE](
	[PAYROLL_PROFILE_ID] [int] IDENTITY(1,1) NOT NULL,
	[CLIENT_ID] [int] NOT NULL,
	[ENTITY_ID] [int] NOT NULL,
	[PROFILE_NAME] [nvarchar](100) NOT NULL,
	[STATUS_CD] [nvarchar](10) NOT NULL,
	[REMARK] [nvarchar](max) NULL,
	[CREATED_DT] [datetimeoffset](7) NULL,
	[CREATED_BY] [nvarchar](20) NULL,
	[MODIFIED_DT] [datetimeoffset](7) NULL,
	[MODIFIED_BY] [nvarchar](20) NULL,
 CONSTRAINT [PK_TB_PAY_PAYROLL_PROFILE] PRIMARY KEY CLUSTERED 
(
	[PAYROLL_PROFILE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
