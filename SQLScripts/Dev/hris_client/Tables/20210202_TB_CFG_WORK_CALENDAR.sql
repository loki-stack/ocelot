CREATE TABLE TB_CFG_WORK_CALENDAR (
	[CALENDAR_ID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[SPECIFIC_ENTITY_ID] INT NULL,
	[CALENDAR_NAME] [nvarchar](200) NULL,
    [PARENT_CALENDAR_ID] [int] NULL,
	[STATUS] [nvarchar](20) NULL,
	[CALENDAR_DAY] BIT NULL,
	[DEFAULT_WORKING_DAY_SUNDAY] [nvarchar](20) NULL,
	[DEFAULT_WORKING_DAY_MONDAY] [nvarchar](20) NULL,
	[DEFAULT_WORKING_DAY_TUESDAY] [nvarchar](20) NULL,
	[DEFAULT_WORKING_DAY_WEDNESDAY] [nvarchar](20) NULL,
	[DEFAULT_WORKING_DAY_THURSDAY] [nvarchar](20) NULL,
	[DEFAULT_WORKING_DAY_FRIDAY] [nvarchar](20) NULL,
	[DEFAULT_WORKING_DAY_SATURDAY] [nvarchar](20) NULL,
	[CREATED_BY] [nvarchar](100) NULL,
	[CREATED_DT] [datetimeoffset](7) NULL,
	[MODIFIED_BY] [nvarchar](100) NULL,
	[MODIFIED_DT] [datetimeoffset](7) NULL
);
