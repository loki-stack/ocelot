CREATE PROCEDURE [dbo].[SP_GET_PAYROLL_PROFILE]
	@clientId int,
	@entityId int,
	@sort nvarchar(max),
	@page int,
	@size int,
	@filter nvarchar(max),
	@fullTextSearch nvarchar(max)
AS
BEGIN
	DECLARE @sql nvarchar(max) = ''
	SET @sql = 'SELECT
		TPPP.PAYROLL_PROFILE_ID AS PayrollProfileId,
		TPPP.CLIENT_ID AS ClientId,
		TPPP.ENTITY_ID AS EntityId,
		TPPP.PROFILE_NAME AS ProfileName,
		TPPP.STATUS_CD AS StatusCd,
		TPPP.REMARK AS Remark,
		TPPP.CREATED_DT AS CreatedDt,
		TPPP.MODIFIED_DT AS ModifiedDt,
		(SELECT COUNT(*) FROM TB_PAY_PAYROLL_PROFILE_EMPLOYEE TPPPE WHERE TPPPE.STATUS_CD = ' + char(39) + 'ACTIVE' + char(39) + ' AND TPPPE.PAYROLL_PROFILE_ID = TPPP.PAYROLL_PROFILE_ID) EmployeeCount
	FROM
		TB_PAY_PAYROLL_PROFILE TPPP
	
	WHERE TPPP.CLIENT_ID = ' + CONVERT(nvarchar(max), @clientId) + ' AND TPPP.ENTITY_ID = ' + CONVERT(nvarchar(max), @entityId) + ' '
	IF @fullTextSearch IS NOT NULL AND @fullTextSearch <> ''
	BEGIN
		SET @sql = @sql + 'AND
		(TPPP.PROFILE_NAME LIKE '+ char(39) + '%' + @fullTextSearch +'%' + char(39) + ')'
	END

	IF @filter IS NOT NULL AND @filter <> ''
	BEGIN
		SET @sql = @sql + 'AND TPPP.STATUS_CD = ' + char(39) + @filter + char(39)
	END

	SET @sql = @sql + ' ORDER BY '+ @sort +' ASC '
	IF @page IS NOT NULL AND @size IS NOT NULL
	BEGIN
		SET @sql = @sql + 'OFFSET '+ CONVERT(nvarchar(max), (@page - 1) * @size) +' ROWS FETCH NEXT '+ CONVERT(nvarchar(max), @size) +' ROWS ONLY;'
	END

	EXECUTE sp_executesql @sql
	return
END
