CREATE PROCEDURE [dbo].[SP_GET_ALL_ORGANIZATION_STRUCTURE]
	@clientId int,
	@entityId int,
	@sort nvarchar(max),
	@page int,
	@size int,
	@filter nvarchar(max),
	@fullTextSearch nvarchar(max),
	@count int output
AS
BEGIN
	DECLARE @sql nvarchar(max) = ''

	SELECT @count = COUNT(*) 
	FROM
		TB_CAM_UNIT
	WHERE
		(@fullTextSearch IS NULL OR @fullTextSearch = '' OR DISPLAY_NAME LIKE '%' + @fullTextSearch + '%' OR UNIT_CODE LIKE '%' + @fullTextSearch + '%') AND
		CLIENT_ID = @clientId AND
		ENTITY_ID = @entityId AND
		(@filter IS NULL OR @filter = '' OR STATUS_CD = @filter);

	SET @sql = 'SELECT
		UNIT_ID AS UnitId,
		PARENT_ID AS ParentId,
		CLIENT_ID AS ClientId,
		ENTITY_ID AS EntityId,
		CODE_LEVEL.ToString() AS CodeLevel,
		FULL_NAME AS FullName,
		DISPLAY_NAME AS DisplayName,
		UNIT_CODE AS UnitCode,
		STATUS_CD AS StatusCd,
		REMARK AS Remark,
		dbo.FN_UNIT_COUNT_EMPLOYEE(UNIT_ID) AS EmployeeCount
	FROM
		TB_CAM_UNIT
	WHERE CLIENT_ID = ' + CONVERT(nvarchar(max), @clientId) + ' AND ENTITY_ID = ' + CONVERT(nvarchar(max), @entityId) + ' '
	IF @fullTextSearch IS NOT NULL AND @fullTextSearch <> ''
	BEGIN
		SET @sql = @sql + 'AND
		(DISPLAY_NAME LIKE '+ char(39) + '%' + @fullTextSearch +'%' + char(39) + 'OR UNIT_CODE LIKE '+ char(39) + '%' + @fullTextSearch +'%' + char(39) + ')'
	END

	IF @filter IS NOT NULL AND @filter <> ''
	BEGIN
		SET @sql = @sql + 'AND STATUS_CD = ' + char(39) + @filter + char(39)
	END

	SET @sql = @sql + ' ORDER BY '+ @sort +' ASC '
	IF @page IS NOT NULL AND @size IS NOT NULL
	BEGIN
		SET @sql = @sql + 'OFFSET '+ CONVERT(nvarchar(max), (@page - 1) * @size) +' ROWS FETCH NEXT '+ CONVERT(nvarchar(max), @size) +' ROWS ONLY;'
	END

	EXECUTE sp_executesql @sql
	return
END
