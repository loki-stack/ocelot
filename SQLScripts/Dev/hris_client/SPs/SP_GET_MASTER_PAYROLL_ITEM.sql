USE [HRIS2_TENANT]
GO
/****** Object:  StoredProcedure [dbo].[GET_MASTER_PAYROLL_ITEM]    Script Date: 1/20/2021 8:28:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GET_MASTER_PAYROLL_ITEM]
	@clientId int,
	@entityId int,
	@sort nvarchar(max),
	@page int,
	@size int,
	@filter nvarchar(max),
	@fullTextSearch nvarchar(max)
AS
BEGIN
	DECLARE @sql nvarchar(max) = ''
	SET @sql = 'SELECT
		PPI.PAYROLL_ITEM_ID AS PayrollItemId,
		PPI.CLIENT_ID AS ClientId,
		PPI.ENTITY_ID AS EntityId,
		PPI.ITEM_NAME AS ItemName,
		PPI.ITEM_CODE AS ItemCode,
		PPI.PAYMENT_SIGN AS PaymentSign,
		PPI.REMARK AS Remark,
		PPI.STATUS_CD AS StatusCd,
		PPI.GL_CODE_ID As GlCodeId
	FROM
		TB_PAY_MASTER_PAYROLL_ITEM PPI
	
	WHERE PPI.CLIENT_ID = ' + CONVERT(nvarchar(max), @clientId) + ' AND PPI.ENTITY_ID = ' + CONVERT(nvarchar(max), @entityId) + ' '
	IF @fullTextSearch IS NOT NULL AND @fullTextSearch <> ''
	BEGIN
		SET @sql = @sql + 'AND
		(PPI.PROFILE_NAME LIKE '+ char(39) + '%' + @fullTextSearch +'%' + char(39) + ')'
	END

	IF @filter IS NOT NULL AND @filter <> ''
	BEGIN
		SET @sql = @sql + 'AND PPI.STATUS_CD = ' + char(39) + @filter + char(39)
	END

	SET @sql = @sql + ' ORDER BY '+ @sort +' ASC '
	IF @page IS NOT NULL AND @size IS NOT NULL
	BEGIN
		SET @sql = @sql + 'OFFSET '+ CONVERT(nvarchar(max), (@page - 1) * @size) +' ROWS FETCH NEXT '+ CONVERT(nvarchar(max), @size) +' ROWS ONLY;'
	END

	EXECUTE sp_executesql @sql
	return
END
