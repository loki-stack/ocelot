
CREATE PROCEDURE [dbo].[SP_GET_MASTER_PAYROLL]
	@masterPayrollItemId int,
	@clientId int,
	@entityId int
AS
BEGIN
	
	SELECT
		TPPI.PAYROLL_ITEM_ID AS PayrollItemId,
		TPPI.PAYROLL_ITEM_FLAG_ID AS PayrollItemFlagId,
		TPPI.FLAG_ID AS FlagId,
		TPPI.CREATED_DT AS CreatedDt
	FROM
		TB_PAY_MASTER_PAYROLL_ITEM_FLAG TPPI
	
	WHERE
		TPPI.PAYROLL_ITEM_ID = @masterPayrollItemId AND
		TPPI.STATUS_CD = 'ACTIVE'
END
