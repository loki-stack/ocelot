DROP PROCEDURE IF EXISTS SP_GET_STATUS_CONFIRMATION;
GO
CREATE PROCEDURE SP_GET_STATUS_CONFIRMATION
	@locale NVARCHAR(20)
AS
BEGIN
	WITH
	t1 AS (
		SELECT * FROM TB_CFG_I18N WHERE LANGUAGE_TAG = @locale AND TEXT_KEY like 'EMP%'
	),
	t2 AS (
		SELECT * FROM t1
			UNION
		SELECT * FROM TB_CFG_I18N WHERE DEFAULT_FLAG = 1 AND TEXT_KEY like 'EMP%' AND TEXT_KEY not in (SELECT TEXT_KEY FROM t1)
	),
	t3 AS (
		SELECT temp2.EMPLOYMENT_STATUS_ID, temp2.EMPLOYMENT_CONTRACT_ID, es.STATUS, es.START_DATE FROM TB_EMP_EMPLOYMENT_STATUS AS es 
		JOIN (
			SELECT EMPLOYMENT_STATUS_ID, EMPLOYMENT_CONTRACT_ID, MAX(START_DATE) AS START_DATE FROM
			( 
				SELECT es2.EMPLOYMENT_STATUS_ID, es2.EMPLOYMENT_CONTRACT_ID, es1.STATUS, es1.START_DATE 
				FROM TB_EMP_EMPLOYMENT_STATUS AS es1
				join TB_EMP_EMPLOYMENT_STATUS AS es2 on es1.EMPLOYMENT_CONTRACT_ID = es2.EMPLOYMENT_CONTRACT_ID
				WHERE es1.START_DATE < es2.START_DATE AND es1.EMPLOYMENT_STATUS_ID != es2.EMPLOYMENT_STATUS_ID
			) temp1
			GROUP BY EMPLOYMENT_STATUS_ID, EMPLOYMENT_CONTRACT_ID
		) temp2 on es.EMPLOYMENT_CONTRACT_ID = temp2.EMPLOYMENT_CONTRACT_ID AND es.START_DATE = temp2.START_DATE
	)
	SELECT DISTINCT (TRIM(f.TEXT_VALUE) + ' ' + TRIM(m.TEXT_VALUE) + ' ' + TRIM(l.TEXT_VALUE)) AS FullName
	, emp.EMPLOYEE_ID AS EmployeeId, ec.EMPLOYEE_NO AS EmployeeNo, ca.DISPLAY_NAME AS BusinessUnit, ec.EMPLOYMENT_CONTRACT_ID AS EmploymentContractId
	, es.EMPLOYMENT_STATUS_ID AS EmploymentStatusId, t3.STATUS AS LastStatus, es.STATUS AS NewStatus, es.START_DATE AS StartDate, es.CONFIRMED as Confirmed
	FROM TB_EMP_EMPLOYEE AS emp
	left join t2 AS f ON emp.FIRST_NAME = f.TEXT_KEY
	left join t2 AS m ON emp.MIDDLE_NAME = m.TEXT_KEY
	left join t2 AS l ON emp.LAST_NAME = l.TEXT_KEY
	join TB_EMP_EMPLOYMENT_CONTRACT AS ec ON emp.EMPLOYEE_ID = ec.EMPLOYEE_ID
	join TB_EMP_EMPLOYMENT_STATUS AS es ON ec.EMPLOYMENT_CONTRACT_ID = es.EMPLOYMENT_CONTRACT_ID
	left join t3 ON es.EMPLOYMENT_STATUS_ID = t3.EMPLOYMENT_STATUS_ID AND es.STATUS != t3.STATUS
	join TB_EMP_MOVEMENT AS mo ON ec.EMPLOYMENT_CONTRACT_ID = mo.EMPLOYMENT_CONTRACT_ID
	left join TB_CAM_UNIT AS ca ON mo.BUSINESS_UNIT_ID = ca.UNIT_ID
	WHERE es.CONFIRMED = 0 AND es.START_DATE <= CAST(DATEADD(DAY, 14, GETDATE()) AS DATE)
END;