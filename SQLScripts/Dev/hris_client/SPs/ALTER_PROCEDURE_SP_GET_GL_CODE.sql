ALTER PROCEDURE [dbo].[SP_GET_GL_CODE]
	@sort nvarchar(max),
	@page int,
	@size int,
	@filter nvarchar(max),
	@fullTextSearch nvarchar(max)
AS
BEGIN
	DECLARE @sql nvarchar(max) = ''
	SET @sql = 'SELECT
		GL_CODE_ID AS GlCodeId,
		GL_CODE AS GlCode,
		CURRENCY AS Currency,
		ACCOUNT_TYPE AS AccountType,
		STATUS_CD AS StatusCd,
		REMARK AS Remark
	FROM
		TB_PAY_GL_CODE
	
	WHERE 1 = 1 '
	IF @fullTextSearch IS NOT NULL AND @fullTextSearch <> ''
	BEGIN
		SET @sql = @sql + 'AND
		(GL_CODE LIKE '+ char(39) + '%' + @fullTextSearch +'%' + char(39) + ')'
	END

	IF @filter IS NOT NULL AND @filter <> ''
	BEGIN
		SET @sql = @sql + 'AND STATUS_CD = ' + char(39) + @filter + char(39)
	END

	IF @sort IS NOT NULL AND @sort <> ''
		BEGIN
			SET @sql = @sql + ' ORDER BY '+ @sort +' ASC '
		END
	ELSE
		BEGIN
			SET @sql = @sql + ' ORDER BY GL_CODE_ID ASC '
		END

	
	IF @page IS NOT NULL AND @size IS NOT NULL
	BEGIN
		SET @sql = @sql + 'OFFSET '+ CONVERT(nvarchar(max), (@page - 1) * @size) +' ROWS FETCH NEXT '+ CONVERT(nvarchar(max), @size) +' ROWS ONLY;'
	END

	EXECUTE sp_executesql @sql
	return
END
