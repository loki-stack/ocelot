ALTER PROCEDURE [dbo].[SP_GET_EXCHANGE_RATE_DEATAIL]
	@exchangeRateId int
AS
BEGIN
	SELECT
		TPXR.EXCHANGE_RATE_ID AS ExchangeRateId,
		TPXR.PAYROLL_CYCLE_ID AS PayrollCycleId,
		TPPC.DATE_START AS StartDate,
		TPPC.DATE_END AS EndDate,
		TPXR.CURRENCY_FROM AS CurrencyFrom,
		TPXR.CURRENCY_TO AS CurrencyTo,
		TPXR.AVERAGE_RATE AS AverageRate,
		TPXR.MONTH_END_RATE AS MonthEndRate,
		TPXR.CREATED_DT AS CreatedDt,
		TPXR.MODIFIED_DT AS ModifiedDt
	FROM
		TB_PAY_EXCHANGE_RATE TPXR
	LEFT JOIN
		TB_PAY_PAYROLL_CYCLE TPPC
	ON
		TPXR.PAYROLL_CYCLE_ID = TPPC.PAYROLL_CYCLE_ID
	WHERE 
		TPXR.EXCHANGE_RATE_ID = @exchangeRateId
END
