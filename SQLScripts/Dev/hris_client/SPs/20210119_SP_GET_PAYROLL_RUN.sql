DROP PROCEDURE IF EXISTS SP_GET_PAYROLL_RUN;
GO
CREATE PROCEDURE SP_GET_PAYROLL_RUN
	@sort nvarchar(max),
	@page int,
	@size int,
	@filter nvarchar(max),
	@fullTextSearch nvarchar(max)
AS
BEGIN
	DECLARE @sql nvarchar(max) = ''
	SET @sql = 'SELECT
		TPPR.PAYROLL_RUN_ID AS PayrollRunId,
		TPPR.PAYROLL_CYCLE_ID AS PayrollCycleId,
		null AS ItemCode,
		TPPR.NAME AS Name,
		TPPR.STATUS_CD AS StatusCd,
		TPPR.REMARK AS Remark,
		TPPC.DATE_START AS DateStart,
		TPPC.DATE_END AS DateEnd,
		TPPR.MODIFIED_DT AS ModifiedDt
	FROM
		TB_PAY_PAYROLL_RUN TPPR
	INNER JOIN
		TB_PAY_PAYROLL_CYCLE TPPC
	ON
		TPPC.PAYROLL_CYCLE_ID = TPPR.PAYROLL_CYCLE_ID
	
	WHERE 1 = 1 '
	IF @fullTextSearch IS NOT NULL AND @fullTextSearch <> ''
	BEGIN
		SET @sql = @sql + 'AND
		(TPPR.PROFILE_NAME LIKE '+ char(39) + '%' + @fullTextSearch +'%' + char(39) + ')'
	END

	IF @filter IS NOT NULL AND @filter <> ''
	BEGIN
		SET @sql = @sql + 'AND TPPR.STATUS_CD = ' + char(39) + @filter + char(39)
	END

	IF @sort IS NOT NULL AND @sort <> ''
	BEGIN
		SET @sql = @sql + ' ORDER BY TPPR.'+ @sort +' ASC '
	END
	
	IF @page IS NOT NULL AND @size IS NOT NULL
	BEGIN
		SET @sql = @sql + 'OFFSET '+ CONVERT(nvarchar(max), (@page - 1) * @size) +' ROWS FETCH NEXT '+ CONVERT(nvarchar(max), @size) +' ROWS ONLY;'
	END

	EXECUTE sp_executesql @sql
	return
END
