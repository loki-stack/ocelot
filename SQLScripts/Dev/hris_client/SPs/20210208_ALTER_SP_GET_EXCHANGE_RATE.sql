ALTER PROCEDURE [dbo].[SP_GET_EXCHANGE_RATE]
	@sort nvarchar(max),
	@page int,
	@size int,
	@filter nvarchar(max),
	@fullTextSearch nvarchar(max)
AS
BEGIN
	DECLARE @sql nvarchar(max) = ''
	SET @sql = 'SELECT
		TPXR.EXCHANGE_RATE_ID AS ExchangeRateId,
		TPXR.PAYROLL_CYCLE_ID AS PayrollCycleId,
		TPPC.DATE_START AS StartDate,
		TPPC.DATE_END AS EndDate,
		TPXR.CURRENCY_FROM AS CurrencyFrom,
		TPXR.CURRENCY_TO AS CurrencyTo,
		TPXR.AVERAGE_RATE AS AverageRate,
		TPXR.MONTH_END_RATE AS MonthEndRate,
		TPXR.CREATED_DT AS CreatedDt,
		TPXR.MODIFIED_DT AS ModifiedDt
	FROM
		TB_PAY_EXCHANGE_RATE TPXR
	LEFT JOIN
		TB_PAY_PAYROLL_CYCLE TPPC
	ON
		TPXR.PAYROLL_CYCLE_ID = TPPC.PAYROLL_CYCLE_ID
	WHERE 1 = 1 '
	IF @fullTextSearch IS NOT NULL AND @fullTextSearch <> ''
	BEGIN
		SET @sql = @sql + 'AND
		(TPXR.CURRENCY_FROM LIKE '+ char(39) + '%' + @fullTextSearch +'%' + char(39) + ' OR TPXR.CURRENCY_TO LIKE '+ char(39) + '%' + @fullTextSearch +'%' + char(39) + ')'
	END

	IF @filter IS NOT NULL AND @filter <> ''
	BEGIN
		SET @sql = @sql + 'AND TPXR.STATUS_CD = ' + char(39) + @filter + char(39)
	END

	IF @sort IS NOT NULL AND @sort <> ''
	BEGIN
		SET @sql = @sql + ' ORDER BY TPXR.'+ @sort +' ASC '
	END
	
	IF @page IS NOT NULL AND @size IS NOT NULL
	BEGIN
		SET @sql = @sql + 'OFFSET '+ CONVERT(nvarchar(max), (@page - 1) * @size) +' ROWS FETCH NEXT '+ CONVERT(nvarchar(max), @size) +' ROWS ONLY;'
	END

	EXECUTE sp_executesql @sql
	return
END
