CREATE PROCEDURE [dbo].[SP_GET_ORGANIZATION_STRUCTURE_BY_ID]
	@unitId int
AS
BEGIN
	SELECT
		UNIT_ID AS UnitId,
		PARENT_ID AS ParentId,
		CLIENT_ID AS ClientId,
		ENTITY_ID AS EntityId,
		CODE_LEVEL.ToString() AS CodeLevel,
		FULL_NAME AS FullName,
		DISPLAY_NAME AS DisplayName,
		UNIT_CODE AS UnitCode,
		STATUS_CD AS StatusCd,
		REMARK AS Remark,
		dbo.FN_UNIT_COUNT_EMPLOYEE(UNIT_ID) AS EmployeeCount
	FROM
		TB_CAM_UNIT
	WHERE
		UNIT_ID = @unitId
END