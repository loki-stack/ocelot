UPDATE TB_CFG_I18N SET TEXT_VALUE = 'Payment:Payable' WHERE  TEXT_KEY  = '00001.6'
GO
UPDATE TB_CFG_I18N SET TEXT_VALUE = REPLACE(TEXT_VALUE, 'Wage:', 'Sum to category:') WHERE  TEXT_KEY like '%0001.%'
GO

DECLARE @parentId int = 0;
SELECT @parentId = CODE_ID FROM TB_CFG_GENERIC_CODE WHERE CODE_TYPE = 'CODE' and CODE_VALUE = '00001'

INSERT INTO TB_CFG_GENERIC_CODE (PARENT_CODE_ID, SPECIFIC_CLIENT_ID, SPECIFIC_COMPANY_ID, CODE_TYPE, CODE_VALUE, DISPLAY_ORDER,STATUS_CD, CREATE_DT, CREATE_BY, MODIFY_DT, MODIFY_BY)
values (@parentId, 0, 0, '00001', '7', 7, 'ACTIVE', SYSDATETIMEOFFSET(), 'SYSTEM', SYSDATETIMEOFFSET(), 'SYSTEM')
INSERT INTO TB_CFG_GENERIC_CODE (PARENT_CODE_ID, SPECIFIC_CLIENT_ID, SPECIFIC_COMPANY_ID, CODE_TYPE, CODE_VALUE, DISPLAY_ORDER,STATUS_CD, CREATE_DT, CREATE_BY, MODIFY_DT, MODIFY_BY)
values (@parentId, 0, 0, '00001', '8', 8, 'ACTIVE', SYSDATETIMEOFFSET(), 'SYSTEM', SYSDATETIMEOFFSET(), 'SYSTEM')
INSERT INTO TB_CFG_GENERIC_CODE (PARENT_CODE_ID, SPECIFIC_CLIENT_ID, SPECIFIC_COMPANY_ID, CODE_TYPE, CODE_VALUE, DISPLAY_ORDER,STATUS_CD, CREATE_DT, CREATE_BY, MODIFY_DT, MODIFY_BY)
values (@parentId, 0, 0, '00001', '9', 9, 'ACTIVE', SYSDATETIMEOFFSET(), 'SYSTEM', SYSDATETIMEOFFSET(), 'SYSTEM')

GO
INSERT INTO TB_CFG_I18N(SPECIFIC_CLIENT_ID, SPECIFIC_ENTITY_ID, LANGUAGE_TAG, TEXT_KEY, TEXT_VALUE, DEFAULT_FLAG, CREATE_DT, CREATE_BY, MODIFY_DT, MODIFY_BY)
VALUES (0, 0, 'en', '00001.7', 'Sum to category:Fixed Income', 1, SYSDATETIMEOFFSET(), 'SYSTEM', SYSDATETIMEOFFSET(), 'SYSTEM'),
(0, 0, 'en', '00001.8', 'Sum to category:Overtime', 1, SYSDATETIMEOFFSET(), 'SYSTEM', SYSDATETIMEOFFSET(), 'SYSTEM'),
(0, 0, 'en', '00001.9', 'Sum to category:Bonus base', 1, SYSDATETIMEOFFSET(), 'SYSTEM', SYSDATETIMEOFFSET(), 'SYSTEM')