CREATE TYPE UDT_PAYROLLRUNTRANSACTION_ARRAY AS TABLE 
(
	TransactionId int,
	DeleteTransaction nvarchar(20),
	EmployeeId int,
	ItemCode nvarchar(20),
	DisplayText nvarchar(max),
	CalculationStartDate datetimeoffset(7),
	CalculationEndDate datetimeoffset(7),
	TaxCycle datetime,
	EAO713Cycle datetime,
	MPFCycle datetime,
	BaseAmount int,
	Units nvarchar(max),
	Param1 nvarchar(max),
	Param2 nvarchar(max),
	Param3 nvarchar(max),
	FinalTransactionAmount int,
	InputCurrency nvarchar(max),
	PaymentCurrency nvarchar(max),
	ExchangeRate float,
	CostCenterCode nvarchar(max)
)
