DROP PROCEDURE IF EXISTS SP_GET_GENERIC_CODE;
GO
CREATE PROCEDURE SP_GET_GENERIC_CODE
	@locale NVARCHAR(30), 
	@clientid INT,
	@entityid INT,
    @codeTypes UDT_STRING_ARRAY READONLY
AS
BEGIN
	WITH
	   t1 AS (
		   SELECT * FROM TB_CFG_I18N WHERE SPECIFIC_CLIENT_ID = @clientid AND SPECIFIC_ENTITY_ID = @entityid AND LANGUAGE_TAG = @locale 
	   ),
	   t2 AS (
		   SELECT * FROM t1
			   UNION
		   SELECT * FROM TB_CFG_I18N WHERE SPECIFIC_CLIENT_ID = @clientid AND SPECIFIC_ENTITY_ID = @entityid AND LANGUAGE_TAG <> @locale AND DEFAULT_FLAG = 1 AND TEXT_KEY not in (select TEXT_KEY from t1)
	   ),
	   t3 AS (
		   SELECT * FROM t2
			   UNION
		   SELECT * FROM TB_CFG_I18N WHERE SPECIFIC_CLIENT_ID = @clientid AND SPECIFIC_ENTITY_ID = 0 AND LANGUAGE_TAG <> @locale AND DEFAULT_FLAG = 1 AND TEXT_KEY not in (select TEXT_KEY from t2)
	   ),
	   t4 AS (
		   SELECT * FROM t3
			   UNION
		   SELECT * FROM TB_CFG_I18N WHERE SPECIFIC_CLIENT_ID = 0 AND SPECIFIC_ENTITY_ID = 0 AND LANGUAGE_TAG = @locale AND TEXT_KEY not in (select TEXT_KEY from t3)
	   ),
	   t5 AS (
		   SELECT * FROM t4
			   UNION
		   SELECT * FROM TB_CFG_I18N WHERE SPECIFIC_CLIENT_ID = 0 AND SPECIFIC_ENTITY_ID = 0 AND LANGUAGE_TAG <> @locale AND DEFAULT_FLAG = 1 AND TEXT_KEY not in (select TEXT_KEY from t4)
	   ),
	   t6 AS (
		   SELECT * FROM TB_CFG_GENERIC_CODE WHERE SPECIFIC_CLIENT_ID = @clientid AND SPECIFIC_COMPANY_ID = @entityid
	   ),
	   t7 AS (
		   SELECT * FROM t6
			   UNION
		   SELECT * FROM TB_CFG_GENERIC_CODE WHERE SPECIFIC_CLIENT_ID = @clientid AND SPECIFIC_COMPANY_ID = 0 AND I18N_TEXT_KEY not in (select I18N_TEXT_KEY from t6)
	   ),
	   t8 AS (
		   SELECT * FROM t7
			   UNION
		   SELECT * FROM TB_CFG_GENERIC_CODE WHERE SPECIFIC_CLIENT_ID = 0 AND SPECIFIC_COMPANY_ID = 0 AND I18N_TEXT_KEY not in (select I18N_TEXT_KEY from t7)
	   )

	SELECT 
		CODE_ID AS CodeId,
		CODE_LEVEL.ToString() AS CodeLevel, 
		CODE_TYPE AS CodeType, 
		CODE_VALUE AS CodeValue, 
		DISPLAY_ORDER AS DisplayOrder, 
		I18N_TEXT_KEY AS I18nTextKey, 
		STATUS_CD AS StatusCd, 
		TEXT_VALUE AS TextValue
	FROM t8 LEFT JOIN t5 ON I18N_TEXT_KEY = TEXT_KEY
	WHERE (SELECT COUNT(*) FROM @codeTypes) = 0 OR (CODE_TYPE IN (SELECT * FROM @codeTypes))
	ORDER BY CODE_LEVEL;
END;