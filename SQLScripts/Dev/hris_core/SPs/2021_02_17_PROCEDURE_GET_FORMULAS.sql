ALTER   PROCEDURE [dbo].[GET_FORMULAS]
	@clientId int,
	@entityId int
AS
BEGIN
	IF (@clientId = 0 AND @entityId = 0)
		BEGIN
			SELECT 
				FML.FORMULAS_ID as FormulasId,
				FML.NAME as Name,
				FML.DESCRIPTION as Description,
				FML.LEVEL  as Level,
				FML.STATUS_CD as StatusCd,
				FML.CREATED_DT as CreatedDt,
				FML.MODIFIED_DT as ModifiedDt,
				FML.FORMULAS as Formulas,
				FML.EDIT_ABLE as EditAble,
				FML.CLIENT_ID as ClientId,
				FML.ENTITY_ID as EntityId
			FROM
				TB_PAY_FORMULAS FML
			WHERE 
				FML.CLIENT_ID = @clientId AND
				FML.ENTITY_ID = @entityId
		END
		ELSE IF (@clientId <> 0 AND @entityId = 0)
		BEGIN
			SELECT 
				FML.FORMULAS_ID as FormulasId,
				FML.NAME as Name,
				FML.DESCRIPTION as Description,
				FML.LEVEL  as Level,
				FML.STATUS_CD as StatusCd,
				FML.CREATED_DT as CreatedDt,
				FML.MODIFIED_DT as ModifiedDt,
				FML.FORMULAS as Formulas,
				FML.EDIT_ABLE as EditAble,
				FML.CLIENT_ID as ClientId,
				FML.ENTITY_ID as EntityId
			FROM
				TB_PAY_FORMULAS FML
			WHERE
					(FML.CLIENT_ID = @clientId AND FML.ENTITY_ID = 0) OR 
					(FML.CLIENT_ID = 0 AND FML.ENTITY_ID = 0);
		END
		ELSE IF (@clientId <> 0 AND @entityId <> 0)
		BEGIN
			SELECT 
				FML.FORMULAS_ID as FormulasId,
				FML.NAME as Name,
				FML.DESCRIPTION as Description,
				FML.LEVEL  as Level,
				FML.STATUS_CD as StatusCd,
				FML.CREATED_DT as CreatedDt,
				FML.MODIFIED_DT as ModifiedDt,
				FML.FORMULAS as Formulas,
				FML.EDIT_ABLE as EditAble,
				FML.CLIENT_ID as ClientId,
				FML.ENTITY_ID as EntityId
			FROM
				TB_PAY_FORMULAS FML
			WHERE
				(FML.CLIENT_ID = 0 AND FML.ENTITY_ID = 0) OR
				(FML.CLIENT_ID = @clientId AND FML.ENTITY_ID = 0) OR
				(FML.CLIENT_ID = @clientId AND FML.ENTITY_ID = @entityId);
		END
END
