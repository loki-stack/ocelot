-- ----------------------------------------------------------------------------
-- Message template table
-- CLIENT_ID is denormalized for future quick filtering only
-- ENTITY_ID is FK to TB_CAM_ENTITY
-- TEMPLATE_TYPE = APP (In App), EML (Email), SMS, NOT (Notification GCM etc)
-- PORTAL -- BS, HR, EE (?? not sure if relevant)
-- STATUS = char(2) -> even now RAM and Disk is cheap, comparing 2 chars is 
--                     always faster than compare with varchar(10)   
-- ----------------------------------------------------------------------------
CREATE TABLE TB_MSG_TEMPLATE (
    [TEMPLATE_ID] [int] IDENTITY(1,1) NOT NULL,
    [CLIENT_ID] int NOT NULL,
	[ENTITY_ID] int NOT NULL,
	[TEMPLATE_CODE] [nvarchar](20) NOT NULL,
	[TEMPLATE_TYPE] [char] (3) NOT NULL,
	[FLAG_HTML] bit NOT NULL,
	[PORTAL] [char](2) NOT NULL,
	[STATUS] [char](2) NOT NULL,
    [CREATED_BY] [nvarchar](100) NOT NULL,
    [CREATED_DT] [datetimeoffset](7) NOT NULL,
    [MODIFIED_BY] [nvarchar](100) NULL,
    [MODIFIED_DT] [datetimeoffset](7) NULL,
	CONSTRAINT [PK_TB_MSG_TEMPLATE] PRIMARY KEY (TEMPLATE_ID) 
);
GO

-- ----------------------------------------------------------------------------
-- Message template i18n
-- LANG_CODE = en, en_US, zh, etc
-- STATUS -- still have status at this level - to enable/disable certain lang
--           version.  
-- ----------------------------------------------------------------------------
CREATE TABLE TB_MSG_TEMPLATE_LANG (
    [TEMPLATE_LANG_ID] [int] IDENTITY(1,1) NOT NULL,
    [TEMPLATE_ID] int NOT NULL,
	[LANG_CODE] [nvarchar] (10) NOT NULL,
	[TITLE] [nvarchar](500) NOT NULL,
	[CONTENT] [nvarchar](max) NOT NULL,
	[STATUS] [char](2) NOT NULL,
    [CREATED_BY] [nvarchar](100) NOT NULL,
    [CREATED_DT] [datetimeoffset](7) NOT NULL,
    [MODIFIED_BY] [nvarchar](100) NULL,
    [MODIFIED_DT] [datetimeoffset](7) NULL,
	CONSTRAINT [PK_TB_MSG_TEMPLATE_LANG] PRIMARY KEY (TEMPLATE_LANG_ID) 
);
GO

ALTER TABLE TB_MSG_TEMPLATE_LANG
ADD CONSTRAINT FK_TB_MSG_TEMPLATE_LANG_TB_MSG_TEMPLATE
FOREIGN KEY ([TEMPLATE_ID]) REFERENCES TB_MSG_TEMPLATE([TEMPLATE_ID]);
GO



-- ----------------------------------------------------------------------------
-- Message table for final content
-- TEMPLATE_LANG_ID -- FK - to reference to the template and language info
-- STATUS - NW (New), RT (Retry), FL (Failed - after max retries), 
--          XX(Cancelled), SC (Success)
-- PORTAL - BS, HR, EE (?? Not sure if relevant)
-- USER_ID - if for notification, refer to TB_SEC_USER
-- ----------------------------------------------------------------------------
CREATE TABLE TB_MSG_MESSAGE (
    [MESSAGE_ID] [bigint] IDENTITY(1,1) NOT NULL,
    [TEMPLATE_LANG_ID] int NOT NULL,
	[MESSAGE_TYPE] [char] (2) NOT NULL,
	[RECIPIENT] [nvarchar](200) NOT NULL,
	[MESSAGE_TITLE] [nvarchar](500) NOT NULL,
	[MESSAGE_CONTENT] [nvarchar](max) NOT NULL,
	[PROCESSED_DT] [datetimeoffset](7) NULL,
	[STATUS] [char](2) NOT NULL,
	[ATTEMPT_COUNT] [int] NULL,
	[PORTAL] [char](2) NOT NULL,
	[FLAG_HTML] bit NOT NULL,
	[USER_ID] int NULL,
    [CREATED_BY] [nvarchar](100) NOT NULL,
    [CREATED_DT] [datetimeoffset](7) NOT NULL,
    [MODIFIED_BY] [nvarchar](100) NULL,
    [MODIFIED_DT] [datetimeoffset](7) NULL,
	CONSTRAINT [PK_TB_MSG_MESSAGE] PRIMARY KEY (MESSAGE_ID) 
);
GO

ALTER TABLE TB_MSG_MESSAGE
ADD CONSTRAINT FK_TB_MSG_MESSAGE_TB_MSG_TEMPLATE_LANG
FOREIGN KEY ([TEMPLATE_LANG_ID]) REFERENCES TB_MSG_TEMPLATE_LANG([TEMPLATE_LANG_ID]);
GO

ALTER TABLE TB_MSG_MESSAGE
ADD CONSTRAINT FK_TB_MSG_MESSAGE_TB_SEC_USER
FOREIGN KEY ([USER_ID]) REFERENCES TB_SEC_USER([USER_ID]);
GO



