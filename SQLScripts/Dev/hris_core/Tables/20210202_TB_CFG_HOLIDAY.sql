CREATE TABLE TB_CFG_HOLIDAY (
	[HOLIDAY_DATE_ID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[DATE] [date] NULL,
	[DESCRIPTION_PRIMARY_LANGUAGE] [nvarchar](400) NULL,
	[DESCRIPTION_SECONDARY_LANGUAGE] [nvarchar](400) NULL,
	[HOLIDAY_TYPE] [nvarchar](20) NULL,
	[CALENDAR_ID] [int] NULL,
	[IS_WORKING_DAY] [bit] NULL,
	[CREATED_BY] [nvarchar](100) NULL,
	[CREATED_DT] [datetimeoffset](7) NULL,
	[MODIFIED_BY] [nvarchar](100) NULL,
	[MODIFIED_DT] [datetimeoffset](7) NULL,
	CONSTRAINT FK_TB_CFG_HOLIDAY_TB_CAM_GLOBAL_CALENDAR FOREIGN KEY (CALENDAR_ID)
    REFERENCES TB_CFG_GLOBAL_CALENDAR(GLOBAL_CALENDAR_ID)
);