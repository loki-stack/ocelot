CREATE TABLE [TB_CFG_SETTING](
	[SETTING_ID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[SPECIFIC_CLIENT_ID] [int] NOT NULL,
	[SPECIFIC_ENTITY_ID] [int] NOT NULL,
	[PDF_STATUS] [bit] NOT NULL,
	[PRIMARY_LANGUAGE] [nvarchar](50) NULL,
	[SECONDARY_LANGUAGE] [nvarchar](50) NULL,
	[CREATED_BY] [nvarchar](100) NOT NULL,
	[CREATED_DT] [datetimeoffset](7) NOT NULL,
	[MODIFIED_BY] [nvarchar](100) NOT NULL,
	[MODIFIED_DT] [datetimeoffset](7) NOT NULL
);