@echo off
SET DbName=%1
SET SqlCmdPara=-S 172.20.20.18 -U sa -P 123456a@
sqlcmd %SqlCmdPara% -Q "CREATE DATABASE %DbName%"
echo Created Db %DbName%

cd ./Tables
echo Go to %cd%
for /R %%f in (*.sql) do (
	echo %%f
	sqlcmd %SqlCmdPara% -d %DbName% -i "%%f"
)
cd ..
echo Created all tables.

cd ./SPs
echo Go to %cd%
for /R %%f in (*.sql) do (
	echo %%f
	sqlcmd %SqlCmdPara% -d %DbName% -i "%%f"
)
cd ..
echo Created all Stored Procedures

echo Done.