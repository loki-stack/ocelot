USE [HRIS_CORE]
GO

INSERT INTO [dbo].[TB_CFG_GENERIC_CODE]
           ([PARENT_CODE_ID]
           ,[CODE_LEVEL]
           ,[SPECIFIC_CLIENT_ID]
           ,[SPECIFIC_COMPANY_ID]
           ,[CODE_TYPE]
           ,[CODE_VALUE]
           ,[DISPLAY_ORDER]
           ,[STATUS_CD]
           ,[CREATE_BY]
           ,[CREATE_DT]
           ,[MODIFY_BY]
           ,[MODIFY_DT])
     VALUES
           (NULL
           ,NULL
           ,0
           ,0
           ,'CODE'
           ,'REMU_PKG'
           ,0
           ,'ACTIVE'
           ,'SYSTEM'
           ,'2020-12-08 00:00:00'
           ,'SYSTEM'
           ,'2020-12-08 00:00:00')
GO

-- update code level
DECLARE @code_level varchar(50);

update [dbo].[TB_CFG_GENERIC_CODE] set CODE_LEVEL = CONCAT('/', CONVERT(varchar(10), CODE_ID), '/') WHERE CODE_TYPE='CODE' AND CODE_VALUE = 'REMU_PKG';
GO 

DECLARE @parent_id int;

SELECT TOP 1 @parent_id = CODE_ID FROM  [dbo].[TB_CFG_GENERIC_CODE]  WHERE CODE_TYPE='CODE' AND CODE_VALUE = 'REMU_PKG'; 

-- clild items
INSERT INTO [dbo].[TB_CFG_GENERIC_CODE]
           ([PARENT_CODE_ID]
           ,[CODE_LEVEL]
           ,[SPECIFIC_CLIENT_ID]
           ,[SPECIFIC_COMPANY_ID]
           ,[CODE_TYPE]
           ,[CODE_VALUE]
           ,[DISPLAY_ORDER]
           ,[STATUS_CD]
           ,[CREATE_BY]
           ,[CREATE_DT]
           ,[MODIFY_BY]
           ,[MODIFY_DT])
     VALUES
           (@parent_id,NULL,0,0,'REMU_PKG','FULLTIME',1,'ACTIVE','SYSTEM','2020-12-08 00:00:00','SYSTEM','2020-12-08 00:00:00'),
		   (@parent_id,NULL,0,0,'REMU_PKG','PARTTIME',2,'ACTIVE','SYSTEM','2020-12-08 00:00:00','SYSTEM','2020-12-08 00:00:00'),
		   (@parent_id,NULL,0,0,'REMU_PKG','CONTRACT',3,'ACTIVE','SYSTEM','2020-12-08 00:00:00','SYSTEM','2020-12-08 00:00:00'),
		   (@parent_id,NULL,0,0,'REMU_PKG','HOUR',4,'ACTIVE','SYSTEM','2020-12-08 00:00:00','SYSTEM','2020-12-08 00:00:00')
;

GO

-- update code_level
UPDATE [dbo].[TB_CFG_GENERIC_CODE] set CODE_LEVEL = CONCAT('/', CONVERT(varchar(10),PARENT_CODE_ID), '/', CONVERT(varchar(10), CODE_ID), '/') 
	WHERE CODE_TYPE='REMU_PKG' AND CODE_LEVEL is NULL;
GO 
		   
-- insert i18n text
INSERT INTO [dbo].[TB_CFG_I18N]
           ([SPECIFIC_CLIENT_ID]
           ,[SPECIFIC_ENTITY_ID]
           ,[LANGUAGE_TAG]
           ,[TEXT_KEY]
           ,[TEXT_VALUE]
           ,[DEFAULT_FLAG]
           ,[CREATE_BY]
           ,[CREATE_DT]
           ,[MODIFY_BY]
           ,[MODIFY_DT]
           ,[DESCRIPTION])
     VALUES
           (0,0,'en','REMU_PKG.FULLTIME','Full-time',1,'SYSTEM','2020-12-08 00:00:00','SYSTEM','2020-12-08 00:00:00',''),
		   (0,0,'en','REMU_PKG.PARTTIME','Part-time',1,'SYSTEM','2020-12-08 00:00:00','SYSTEM','2020-12-08 00:00:00',''),
		   (0,0,'en','REMU_PKG.CONTRACT','Contract',1,'SYSTEM','2020-12-08 00:00:00','SYSTEM','2020-12-08 00:00:00',''),
		   (0,0,'en','REMU_PKG.HOUR','Hour',1,'SYSTEM','2020-12-08 00:00:00','SYSTEM','2020-12-08 00:00:00','');
		   
GO






