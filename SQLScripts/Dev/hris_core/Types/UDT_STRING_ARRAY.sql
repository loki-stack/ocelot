DROP TYPE IF EXISTS UDT_STRING_ARRAY;
GO

CREATE TYPE UDT_STRING_ARRAY AS TABLE
(
	VALUE NVARCHAR(100)
)
GO