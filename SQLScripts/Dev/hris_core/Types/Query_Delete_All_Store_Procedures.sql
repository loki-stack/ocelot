DECLARE @procName VARCHAR(500) 
DECLARE cur CURSOR FOR SELECT [name] FROM sys.objects WHERE TYPE = 'p' 
OPEN cur 
FETCH NEXT FROM cur INTO @procName 
WHILE @@fetch_status = 0 
BEGIN
 EXEC('DROP PROCEDURE ' + @procName) 
 FETCH NEXT FROM cur INTO @procName 
END
CLOSE cur 
DEALLOCATE cur;