USE [HRIS_CORE]
GO

-- remove the default value constraints - your DB constraints name may be difference,
-- update the scripts accordingly
ALTER TABLE [TB_SEC_SECURITY_PROFILE]
DROP CONSTRAINT [DF__TB_SEC_SE__START__3B40CD36], [DF__TB_SEC_SE__END_D__3C34F16F];

ALTER TABLE [TB_SEC_SECURITY_PROFILE]
DROP COLUMN [START_DT], [END_DT];
GO