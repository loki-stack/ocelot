USE [HRIS_CORE]
GO
ALTER TABLE TB_CFG_I18N
DROP CONSTRAINT PK_TB_CFG_I18N;
GO
ALTER TABLE TB_CFG_I18N
ALTER COLUMN TEXT_KEY nvarchar(41) NOT NULL;
GO
ALTER TABLE TB_CFG_I18N
ADD CONSTRAINT [PK_TB_CFG_I18N] PRIMARY KEY CLUSTERED 
(
	[SPECIFIC_CLIENT_ID] ASC,
	[SPECIFIC_ENTITY_ID] ASC,
	[LANGUAGE_TAG] ASC,
	[TEXT_KEY] ASC
)