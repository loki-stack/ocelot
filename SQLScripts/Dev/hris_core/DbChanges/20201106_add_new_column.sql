--add CLIENT_CODE column in [TB_CLIENT] table
USE [HRIS_CATALOG]
GO

ALTER TABLE [TB_CLIENT]
ADD [CLIENT_CODE] [nvarchar](50) NOT NULL DEFAULT ''
GO