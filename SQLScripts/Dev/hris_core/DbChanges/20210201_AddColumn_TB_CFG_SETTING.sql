ALTER TABLE TB_CFG_SETTING
ADD PAYROLL_CYCLE_FREQUENCY NVARCHAR(20);
GO
ALTER TABLE TB_CFG_SETTING
ADD PAYROLL_CYCLE_START_DAY INT;
GO
ALTER TABLE TB_CFG_SETTING
ADD PAYROLL_CUT_OFF_DAY INT;