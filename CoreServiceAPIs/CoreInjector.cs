﻿using CommonLib.Services;
// using CommonLib.Repositories.Interfaces;
// using CommonLib.Repositories.Impl;
using CommonLib.Validation;
using CoreServiceAPIs.Authentication.Repositories.Impl;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Authentication.Services.Impl;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using CoreServiceAPIs.Common.Interfaces;
using CoreServiceAPIs.Common.Repositories;
using CoreServiceAPIs.Common.Repositories.Impl;
using CoreServiceAPIs.Common.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CoreServiceAPIs
{
    public class CoreInjector
    {
        public static void RegisterServices(IServiceCollection services, IConfiguration configuration)
        {
            //disable automatic model state validation
            services.AddSingleton<IObjectModelValidator, IgnoreAutoValidator>();
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            services.AddScoped<IRefreshTokenRepository, RefreshTokenRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IFunctionRepository, FuncRepository>();
            services.AddScoped<IRoleGroupRepository, RoleGroupRepository>();
            services.AddScoped<ISettingRepository, SettingRepository>();
            services.AddScoped<IGenericCodeRepository, GenericCodeRepository>();
            services.AddScoped<IFormulasRepository, FormulasRepository>();
            services.AddScoped<IMenuRepository, MenuRepository>();
            services.AddScoped<IUserSecurityProfileRepository, UserSecurityProfileRepository>();
            services.AddScoped<ISecurityProfileRepository, SecurityProfileRepository>();
            services.AddScoped<IClientRepository, ClientRepository>();
            services.AddScoped<ICfgI18nRepository, CfgI18nRepository>();
            services.AddScoped<IDataAccessGroupRepository, DataAccessGroupRepository>();
            services.AddScoped<IGlobalCalendarRepository, GlobalCalendarRepository>();
            services.AddScoped<IHolidayRepository, HolidayRepository>();
            services.AddScoped<CommonLib.Repositories.Interfaces.IMessageRepository, CommonLib.Repositories.Impl.MessageRepository>();
            services.AddScoped<CommonLib.Repositories.Interfaces.IMessageTemplateRepository, CommonLib.Repositories.Impl.MessageTemplateRepository>();
            services.AddScoped<CommonLib.Repositories.Interfaces.IMessageTemplateLangRepository, CommonLib.Repositories.Impl.MessageTemplateLangRepository>();

            services.AddScoped<IJwtAuthenticationService, JwtAuthenticationService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IFunctionService, FunctionService>();
            services.AddScoped<IRoleGroupService, RoleGroupService>();
            services.AddScoped<ISettingService, SettingService>();
            services.AddScoped<IGenericCodeService, GenericCodeService>();
            services.AddScoped<IFormulasService, FormulasService>();
            services.AddScoped<IClientService, ClientService>();
            services.AddScoped<IMenuService, MenuService>();
            services.AddScoped<IUserSecurityProfileService, UserSecurityProfileService>();
            services.AddScoped<ISecurityProfileService, SecurityProfileService>();
            services.AddScoped<IClientService, ClientService>();
            services.AddScoped<IDataAccessGroupService, DataAccessGroupService>();
            services.AddScoped<IValidator, Validator>();
            services.AddScoped<IGlobalCalendarService, GlobalCalendarService>();
            services.AddScoped<ISendEmailService, SendEmailService>();
            // for emails and messages
            services.AddScoped<IMessageService, MessageService>();
            services.AddScoped<CommonLib.Repositories.Interfaces.IMessageRepository, CommonLib.Repositories.Impl.MessageRepository>();
            services.AddScoped<CommonLib.Repositories.Interfaces.IMessageTemplateLangRepository, CommonLib.Repositories.Impl.MessageTemplateLangRepository>();
            services.AddScoped<CommonLib.Repositories.Interfaces.IMessageTemplateRepository, CommonLib.Repositories.Impl.MessageTemplateRepository>();
            services.AddScoped<IEmailService, EmailService>();
        }
    }
}