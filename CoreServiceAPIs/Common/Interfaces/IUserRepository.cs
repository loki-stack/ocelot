﻿using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Common.Dtos;
using CoreServiceAPIs.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Common.Interfaces
{
    public interface IUserRepository : IBaseRepository<TbSecUser>
    {
        Task<TbSecUser> GetUserByUserName(string username);

        Task<TbSecUser> GetUserByEmail(string email);

        Task<UserViewModel> GetUserInformation(int id);

        Task<List<UserViewModel>> GetAll(PaginationRequest request, int clientId);

        Task UpdateLastActive(int id);

        Task<CreateObjectResult> CreateUser(CreateUserRequest request, StdRequestParam requestParam);

        Task<UpdateObjectResult> UpdateUser(UpdateUserRequest request);

        /// <summary>
        /// Find user by reset password token
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        Task<TbSecUser> GetUserByPwdToken(string token);

        /// <summary>
        /// Get user that matched username or email, and clientId + entityId
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="clientId"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        Task<TbSecUser> GetEntityUser(string userName, int clientId, int entityId);

        Task<int?> Update(TbSecUser user);

        Task<TbSecUser> GetUserByEmployeeId(int clientId, int entityId, int employeeId);

        bool CheckExistUsername(int clientId, int userId, string username);

        Task UpdatePwdToken(int userId, string resetPwdToken, DateTimeOffset? resetPwdExpiryDt);
    }
}