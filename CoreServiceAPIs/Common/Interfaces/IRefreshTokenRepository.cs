﻿using CommonLib.Models;
using CommonLib.Models.Core;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Common.Interfaces
{
    public interface IRefreshTokenRepository : IBaseRepository<TbSecRefreshToken>
    {
        Task<TbSecRefreshToken> GetInformation(string refreshToken);
    }
}
