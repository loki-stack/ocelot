﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Common.Interfaces
{
    public interface IBaseRepository<T> where T : class
    {
        IEnumerable<T> All(Expression<Func<T, bool>> where);

        T Save(T entity);

        void Delete(T entity);

        void Update(T entity);

        void Patch(T entity);

        Task<T> Find(object id);

        bool Exist(Expression<Func<T, bool>> where);
    }
}
