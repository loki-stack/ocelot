namespace CoreServiceAPIs.Common.Dtos
{
    public class SecurityProfileMngViewModel
    {
        public int SecurityProfileId { get; set; }
        //public RoleGroupViewModel FAR { get; set; }
        public int roleGroupId { get; set; }
        public string RoleGroupNameTxt { get; set; }
        public string RoleGroupDescriptionTxt { get; set; }
        public int DataAccessGroupId { get; set; }
        public string DataAccessGroupNameTxt { get; set; }
        public string DataAccessDescriptionTxt { get; set; }
        //public DataAccessGroupViewModel DAR { get; set; }
    }
}