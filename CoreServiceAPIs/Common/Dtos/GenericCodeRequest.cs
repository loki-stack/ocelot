﻿using System.Collections.Generic;

namespace CoreServiceAPIs.Common.Dtos
{
    public class GenericCodeRequest
    {
        public string Locale { get; set; } = "zh-hk";
        public List<string> CodeTypes { get; set; }
    }
}