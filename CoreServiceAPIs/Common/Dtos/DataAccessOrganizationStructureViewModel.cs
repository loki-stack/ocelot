namespace CoreServiceAPIs.Common.Dtos
{
    public class DataAccessOrganizationStructureViewModel
    {
        public int DataAccessGroupId { get; set; }
        public string NodeLevel { get; set; }
        public bool IsSelectAllDescendants { get; set; }
        public int DescendantCount { get; set; }
    }
}