using CommonLib.Validation;
using CoreServiceAPIs.Common.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CoreServiceAPIs.Common.Dtos
{
    public class CreateDataAccessGroupRequest
    {
        public int DataAccessGroupId { get; set; }
        public int SpecificClientId { get; set; }
        public int SpecificEntityId { get; set; }


        [Required(ErrorMessage = ValMsg.VAL_REQUIRED)]
        [MaxLength(100, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "dataAccessGroupNameTxt", Level = Level.ERROR, Placeholder = new string[] { "darManagementModule.dataAccessGroupNameTxt", "100" })]
        public string DataAccessGroupNameTxt { get; set; }

        [Required(ErrorMessage = ValMsg.VAL_REQUIRED)]
        [MaxLength(200, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "dataAccessGroupDescriptionTxt", Level = Level.ERROR, Placeholder = new string[] { "darManagementModule.dataAccessGroupDescriptionTxt", "200" })]
        public string DataAccessGroupDescriptionTxt { get; set; }
        public string CreateBy { get; set; }
        public DateTimeOffset CreateDt { get; set; }
    }
}
