﻿using CommonLib.Models;
using CommonLib.Models.Core;
using CoreServiceAPIs.Common.Constants;
using CoreServiceAPIs.Common.ShardingUtil;
using System;

namespace CoreServiceAPIs.Common.Dtos
{
    public class UserViewModel
    {
        public int UserId { get; set; }
        public string SSOId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }

        // public string Avatar { get; set; }
        public byte[] Avatar { get; set; }

        public string Fullname { get; set; }
        public int? EntityId { get; set; }
        public string EntityName { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public int? ClientId { get; set; }
        public string ClientName { get; set; }
        public string StatusCD { get; set; }
        public string StatusName { get; set; }
        public DateTime? LastActive { get; set; }
        public string LastActiveStr { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string SingleSignOnId { get; set; }
        public DateTime? EssAccessStartDt { get; set; }
        public DateTime? EssAccessEndDt { get; set; }
        public bool? EssAccessFlag { get; set; }
        public int? EmployeeId { get; set; }

        public UserViewModel()
        {
        }

        public UserViewModel(TbSecUser user)
        {
            this.UserId = user.UserId;
            this.Username = user.UserName;
            this.Email = user.Email;
            this.Avatar = user.Avatar;
            this.Fullname = user.FullName;
            this.ClientId = user.ClientId;
            this.EntityId = user.EntityId;
            this.StatusCD = user.StatusCd;
            this.EmployeeId = user?.EmployeeId;
            if (this.StatusCD == Status.ACTIVE_CD)
            {
                this.StatusName = Status.ACTIVE_VALUE;
            }
            else this.StatusName = Status.INACTIVE_VALUE;
            this.LastActive = user.LastActive;
            if (this.LastActive != null)
            {
                this.LastActiveStr = Utils.TimeAgo(this.LastActive.Value);
            }
            this.CreatedDate = user.CreatedDt;
            this.CreatedBy = user.CreatedBy;
            this.SingleSignOnId = user.SingleSignOnId;
            this.EssAccessStartDt = user.EssAccessStartDt;
            this.EssAccessEndDt = user.EssAccessEndDt;
            this.EssAccessFlag = user.EssAccessFlag;
        }
    }
}