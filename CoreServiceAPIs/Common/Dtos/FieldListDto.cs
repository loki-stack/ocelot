﻿using Microsoft.EntityFrameworkCore;

namespace CoreServiceAPIs.Common.Dtos
{
    public class FieldListDto
    {
        public long Id { get; set; }
        public string Value { get; set; }
        public string CodeType { get; set; }
        public string CodeValue { get; set; }
        public string TextValue { get; set; }
        public HierarchyId Node { get; set; }
    }
}