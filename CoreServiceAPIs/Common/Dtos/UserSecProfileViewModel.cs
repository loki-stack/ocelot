using System;
public class UserSecProfileViewModel
{
    public int UserId { get; set; }
    public DateTimeOffset StartDt { get; set; }
    public DateTimeOffset? EndDt { get; set; }
}
