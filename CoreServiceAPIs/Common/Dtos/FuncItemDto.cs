namespace CoreServiceAPIs.Common.Dtos
{
    public class FuncItemDto
    {
        public int ItemId { get; set; }
        public string ItemType { get; set; }
        public string ItemCode { get; set; }
        public string ItemText { get; set; }
        public bool IsSelected { get; set; }

    }
}