﻿using System.Collections.Generic;

namespace CoreServiceAPIs.Common.Dtos
{
    public class UpdateSecProfileRequest
    {
        public string Name { get; set; }
        public List<SecProfileDetailRequest> Detail { get; set; }
    }
}