﻿using System.Collections.Generic;

namespace CoreServiceAPIs.Common.Dtos
{
    public class CreateRoleGroupRequest
    {
        public int RoleGroupId { get; set; }
        public int SpecificClientId { get; set; }
        public int SpecificEntityId { get; set; }
        public string RoleGroupNameTxt { get; set; }
        public string RoleGroupDescriptionTxt { get; set; }
        public bool BundleGroupFlag { get; set; }
        public List<int> FuncIds { get; set; }
    }
}