﻿namespace CoreServiceAPIs.Common.Dtos
{
    public class ModuleGenericCodeViewModel
    {
        public int CodeId { get; set; }
        public string CodeValue { get; set; }
        public string TextKey { get; set; }
    }
}