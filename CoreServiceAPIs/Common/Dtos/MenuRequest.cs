﻿namespace CoreServiceAPIs.Common.Dtos
{
    public class MenuRequest
    {
        public string Locale { get; set; } = "en";
        public string ClientLevel { get; set; } = string.Empty;
        public string EntityLevel { get; set; } = string.Empty;
    }
}