﻿using System;
using System.Text.Json.Serialization;

namespace CoreServiceAPIs.Common.Dtos
{
    public class UpdateGenericCodeRequest
    {
        [JsonIgnore]
        public int Id { get; set; }

        public int ModuleId { get; set; }
        public int ParentCodeId { get; set; }
        public string CodeValue { get; set; }
        public string Status { get; set; }
        public int DisplayOrder { get; set; }
        public string Name { get; set; }
        public string Remark { get; set; }

        [JsonIgnore]
        public string Locale { get; set; }

        [JsonIgnore]
        public string ModifiedBy { get; set; }

        [JsonIgnore]
        public DateTimeOffset ModifiedDt { get; set; }
    }
}