﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace CoreServiceAPIs.Common.Dtos
{
    public class TreeView
    {
        public long Id { get; set; }
        public string Value { get; set; }
        public string CodeType { get; set; }
        public string CodeValue { get; set; }
        public string TextValue { get; set; }
        public HierarchyId Node { get; set; }
        public List<TreeView> Children { get; set; }
    }
}