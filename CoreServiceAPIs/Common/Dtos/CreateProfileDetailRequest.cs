namespace CoreServiceAPIs.Common.Dtos
{
    public class CreateProfileDetailRequest
    {
        public int SecurityProfileId { get; set; }
        public int RoleGroupId { get; set; }
        public int DataAccessGroupId { get; set; }

    }
}