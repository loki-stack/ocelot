namespace CoreServiceAPIs.Common.Dtos
{
    public class PublicInfoDto
    {
        public string ApplicationName { get; set; }
        public string Environment { get; set; }
        public string DbInfo { get; set; }

    }
}