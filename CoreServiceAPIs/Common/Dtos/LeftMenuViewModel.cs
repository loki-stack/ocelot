﻿using System.Collections.Generic;

namespace CoreServiceAPIs.Common.Dtos
{
    public class LeftMenuViewModel
    {
        public string ModuleCd { get; set; }
        public string ModuleName { get; set; }
        public List<LeftMenuFunctionViewModel> Functions { get; set; }
    }
}