﻿namespace CoreServiceAPIs.Common.Dtos
{
    public class RoleFunctionViewModel
    {
        public int FunctionId { get; set; }
        public string FunctionCd { get; set; }
    }
}