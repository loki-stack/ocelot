﻿using System;

namespace CoreServiceAPIs.Common.Dtos
{
    public class GetObjectRequest
    {
        public DateTime? CreatedDateFrom { get; set; }
        public DateTime? CreatedDateTo { get; set; }
    }
}