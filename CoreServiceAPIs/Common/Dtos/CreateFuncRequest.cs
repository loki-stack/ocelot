﻿namespace CoreServiceAPIs.Common.Dtos
{
    public class CreateFuncRequest
    {
        public int FunctionId { get; set; }
        public string MenuCd { get; set; }
        public string ModuleCd { get; set; }
        public string FeatureCd { get; set; }
        public string FunctionCd { get; set; }
        public string RoleCd { get; set; }
        public int MenuDisplayOrder { get; set; }
    }
}