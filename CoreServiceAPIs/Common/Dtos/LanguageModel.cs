﻿namespace CoreServiceAPIs.Common.Dtos
{
    public class LanguageModel
    {
        public string LanguageCode { get; set; }
        public string LanguageValue { get; set; }
    }
}
