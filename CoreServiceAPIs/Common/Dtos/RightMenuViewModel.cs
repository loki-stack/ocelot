﻿using System.Collections.Generic;

namespace CoreServiceAPIs.Common.Dtos
{
    public class RightMenuViewModel
    {
        public string ModuleCd { get; set; }
        public string ModuleName { get; set; }
        public List<RightMenuFunctionViewModel> Functions { get; set; }
    }
}