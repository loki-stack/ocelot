﻿using System.Collections.Generic;

namespace CoreServiceAPIs.Common.Dtos
{
    public class ClientViewModel
    {
        public int ClientId { get; set; }
        public string ClientLevel { get; set; }
        public string ClientCode { get; set; }
        public string ClientName { get; set; }
        public bool IsSelected { get; set; }
        public List<ClientEntityViewModel> Entities { get; set; }
    }
}