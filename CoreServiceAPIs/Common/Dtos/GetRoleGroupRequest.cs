﻿using System;

namespace CoreServiceAPIs.Common.Dtos
{
    public class GetRoleGroupRequest
    {
        public DateTime? CreatedDateFrom { get; set; }
        public DateTime? CreatedDateTo { get; set; }
    }
}