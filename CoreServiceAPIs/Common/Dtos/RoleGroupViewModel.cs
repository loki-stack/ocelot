﻿using System;
using System.Collections.Generic;

namespace CoreServiceAPIs.Common.Dtos
{
    public class RoleGroupViewModel
    {
        public int RoleGroupId { get; set; }
        public int SpecificClientId { get; set; }
        public int SpecificEntityId { get; set; }
        public string RoleGroupNameTxt { get; set; }
        public string RoleGroupDescriptionTxt { get; set; }
        public string CreateBy { get; set; }
        public DateTimeOffset CreateDt { get; set; }
        public string ModifyBy { get; set; }
        public DateTimeOffset ModifyDt { get; set; }
        public List<RoleFunctionViewModel> Funcs { get; set; }
        public int SecurityProfileMappingCount { get; set; }
        public string LastModified { get; set; }
    }
}