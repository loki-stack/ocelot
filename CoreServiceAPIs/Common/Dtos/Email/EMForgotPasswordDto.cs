namespace CoreServiceAPIs.Common.Email.Dtos
{
    public class EMForgotPasswordDto
    {
        public string UserFullName { get; set; }
        public string ResetPasswordURL { get; set; }
    }
}