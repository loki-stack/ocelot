﻿namespace CoreServiceAPIs.Common.Dtos
{
    public class RightMenuFunctionViewModel
    {
        public string ModuleCd { get; set; }
        public string FunctionCd { get; set; }
        public string ModuleName { get; set; }
        public string FunctionName { get; set; }
        public string FunctionNature { get; set; }
        public string MenuName { get; set; }
    }
}