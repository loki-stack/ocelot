using System;
using System.Collections.Generic;

namespace CoreServiceAPIs.Common.Dtos
{
    public class DataAccessGroupViewModel
    {

        public int DataAccessGroupId { get; set; }
        public int SpecificClientId { get; set; }
        public int SpecificEntityId { get; set; }
        public string DataAccessGroupNameTxt { get; set; }
        public string DataAccessGroupDescriptionTxt { get; set; }
        public string ModifyBy { get; set; }
        public DateTimeOffset ModifyDt { get; set; }

        public int SecurityProfileMappingCount { get; set; }

        public string LastModified { get; set; }
        public List<DataAccessGradeViewModel> Grade { get; set; }
        public List<DataAccessGroupClientEntityDto> Entities { get; set; }

        public List<int> RemunerationPackageId { get; set; }
        public List<DataAccessOrganizationStructureViewModel> OrganizationStructure { get; set; }
    }
}