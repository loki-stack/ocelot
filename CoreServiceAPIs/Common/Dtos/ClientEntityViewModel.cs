﻿namespace CoreServiceAPIs.Common.Dtos
{
    public class ClientEntityViewModel
    {
        public int EntityId { get; set; }
        public string EntityLevel { get; set; }
        public string EntityCode { get; set; }
        public string EntityName { get; set; }
    }
}