namespace CoreServiceAPIs.Common.Dtos
{
    public class DataAccessClientEntityViewModel
    {
        public int DataAccessClientEntityId { get; set; }
        public int DataAccessGroupId { get; set; }
        public string ClientLevel { get; set; }
        public string EntityLevel { get; set; }

    }
}