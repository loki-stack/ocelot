﻿namespace CoreServiceAPIs.Common.Dtos
{
    public class SecProfileDetailRequest
    {
        public int SpecificClientId { get; set; }
        public int SpecificEntityId { get; set; }
        public int RoleGroupId { get; set; }
        public string RoleGroupNameTxt { get; set; }
        public string RoleGroupDescriptionTxt { get; set; }
        public int DataAccessGroupId { get; set; }
        public string DataAccessGroupNameTxt { get; set; }
        public string DataAccessGroupDescriptionTxt { get; set; }
    }
}