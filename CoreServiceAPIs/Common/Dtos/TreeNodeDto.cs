using System.Collections.Generic;

namespace CoreServiceAPIs.Common.Dtos
{
    public class TreeNodeDto<T>
    {
        public TreeNodeDto()
        {
            Children = new List<TreeNodeDto<T>>();
        }

        public string Key { get; set; }
        public T Data { get; set; }
        public List<TreeNodeDto<T>> Children { get; set; }
        public bool IsSelectable { get; set; }

    }
}