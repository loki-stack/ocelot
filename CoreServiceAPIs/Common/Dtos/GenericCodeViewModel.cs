﻿using Microsoft.EntityFrameworkCore;
using System;

namespace CoreServiceAPIs.Common.Dtos
{
    public class GenericCodeViewModel
    {
        public int Id { get; set; }
        public string CodeValue { get; set; }
        public string CodeType { get; set; }
        public int? ParentCodeId { get; set; }
        public int ClientId { get; set; }
        public int EntityId { get; set; }
        public string ParentName { get; set; }
        public HierarchyId CodeLevel { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string StatusTxt { get; set; }
        public int DisplayOrder { get; set; }
        public string Remark { get; set; }
        public string CreateBy { get; set; }
        public DateTimeOffset CreateDt { get; set; }
        public string ModifyBy { get; set; }
        public DateTimeOffset ModifyDt { get; set; }
    }
}