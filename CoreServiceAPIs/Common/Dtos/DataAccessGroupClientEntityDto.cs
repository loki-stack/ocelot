namespace CoreServiceAPIs.Common.Dtos
{
    public class DataAccessGroupClientEntityDto
    {
        public int DataAccessClientEntityId { get; set; }
        public int DataAccessGroupId { get; set; }
        public int ClientId { get; set; }
        public string ClientLevel { get; set; }
        public string ClientCode { get; set; }
        public string ClientName { get; set; }
        public int EntityId { get; set; }
        public string EntityLevel { get; set; }
        public string EntityCode { get; set; }
        public string EntityName { get; set; }
    }
}