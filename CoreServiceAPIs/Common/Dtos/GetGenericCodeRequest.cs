﻿namespace CoreServiceAPIs.Common.Dtos
{
    public class GetGenericCodeRequest : PaginationRequest
    {
        public string CodeType { get; set; } = string.Empty;
        public int ResData { get; set; } = 0;
    }
}