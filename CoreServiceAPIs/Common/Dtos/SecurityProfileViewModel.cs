﻿using System;
using System.Collections.Generic;

namespace CoreServiceAPIs.Common.Dtos
{
    public class SecurityProfileViewModel
    {
        public int SecurityProfileId { get; set; }
        public int SpecificClientId { get; set; }
        public int SpecificEntityId { get; set; }
        public string SecurityProfileNameTxt { get; set; }
        public string SecurityProfileDescriptionTxt { get; set; }
        public string CreateBy { get; set; }
        public DateTimeOffset CreateDt { get; set; }

        public string ModifyBy { get; set; }
        public DateTimeOffset ModifyDt { get; set; }
        public int MappedProfileCount { get; set; }
        public string LastModified { get; set; }
        public DateTimeOffset StartDt { get; set; }
        public DateTimeOffset? EndDt { get; set; }

        public List<RoleGroupViewModel> FAR { get; set; }
        public List<DataAccessGroupViewModel> DAR { get; set; }
        public List<UserSecProfileViewModel> UserSecurityProfile { get; set; }
    }
}