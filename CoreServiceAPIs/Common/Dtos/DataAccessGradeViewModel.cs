namespace CoreServiceAPIs.Common.Dtos
{
    public class DataAccessGradeViewModel
    {
        public int DataAccessGroupId { get; set; }
        public int DataAccessGradeId { get; set; }

        public int MinGradeLevelNum { get; set; }
        public int MaxGradeLevelNum { get; set; }

    }
}