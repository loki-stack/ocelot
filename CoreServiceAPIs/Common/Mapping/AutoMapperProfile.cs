﻿using AutoMapper;
using CommonLib.Models;
using CommonLib.Models.Core;
using CoreServiceAPIs.Authentication.Dtos.GlobalCalendar;
using CoreServiceAPIs.Authentication.Dtos.HolidayCalendar;
using CoreServiceAPIs.Common.Dtos;

namespace CoreServiceAPIs.Common.Mapping
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<CreateFuncRequest, TbSecFunction>();
            CreateMap<UpdateFuncRequest, TbSecFunction>();
            CreateMap<CreateRoleGroupRequest, TbSecRoleGroup>();
            CreateMap<CreateFuncRequest, TbSecFunction>();
            //TbCfgGenericCode
            CreateMap<CreateGenericCodeRequest, TbCfgGenericCode>()
                .ForMember(t => t.StatusCd, u => u.MapFrom(x => x.Status));

            //TbCfgI18n
            CreateMap<CreateGenericCodeRequest, TbCfgI18n>()
                .ForMember(t => t.TextValue, u => u.MapFrom(x => x.Name));

            CreateMap<TbSecFunction, FuncViewModel>();
            CreateMap<CreateSecProfileRequest, TbSecSecurityProfile>()
                .ForMember(t => t.SecurityProfileNameTxt, u => u.MapFrom(x => x.SecurityProfileNameTxt));

            CreateMap<TbSecSecurityProfile, SecurityProfileViewModel>()
                .ForMember(s => s.SecurityProfileNameTxt, t => t.MapFrom(x => x.SecurityProfileNameTxt));

            CreateMap<CreateDataAccessGroupRequest, TbSecDataAccessGroup>()
                .ForMember(s => s.SpecificCompanyId, t => t.MapFrom(x => x.SpecificEntityId));

            CreateMap<UpdateDataAccessGroupRequest, TbSecDataAccessGroup>()
                .ForMember(s => s.SpecificCompanyId, t => t.MapFrom(x => x.SpecificEntityId));

            //CreateMap<TbEmpMpfRegisteredScheme, MPFRegisteredSchemeViewList>();

            CreateMap<GlobalCalendarModel, TbCfgGlobalCalendar>();
            CreateMap<TbCfgGlobalCalendar, GlobalCalendarViewModel>()
                .ForMember(s => s.Holidays, t => t.MapFrom(x => x.TbCfgHoliday))
                .ReverseMap(); ;

            CreateMap<TbCfgHoliday, HolidayViewModel>();
            CreateMap<HolidayModel, TbCfgHoliday>();
        }
    }
}