using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Common.Repositories.Interfaces
{
    public interface ICfgI18nRepository
    {
        /// <summary>
        /// Get the dictionary based on the locale, clientId, entityId and keys provided.
        /// 
        /// </summary>
        Task<Dictionary<string, string>> GetTextDictionary(string locale, int clientId, int entityId, HashSet<string> keys);
    }
}