using CommonLib.Models;
using CommonLib.Models.Core;
using CoreServiceAPIs.Common.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Common.Repositories.Impl
{
    public class CfgI18nRepository : ICfgI18nRepository
    {
        private readonly CoreDbContext _context;
        private readonly ILogger<CfgI18nRepository> _logger;

        public CfgI18nRepository(CoreDbContext context, ILogger<CfgI18nRepository> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<Dictionary<string, string>> GetTextDictionary(string locale, int clientId, int entityId, HashSet<string> keys)
        {
            // throw new System.NotImplementedException();
            // Dictionary<string, string> dict = new Dictionary<string, string>();
            var rs = await _context.TbCfgI18n
                .Where(e => e.LanguageTag == locale)
                .Where(e => e.SpecificClientId == clientId)
                .Where(e => e.SpecificEntityId == entityId)
                .Where(e => keys.Contains(e.TextKey))
                .Select(e => new { e.TextKey, e.TextValue })
                .ToDictionaryAsync(d => d.TextKey, d => d.TextValue);

            return rs;
        }
    }
}