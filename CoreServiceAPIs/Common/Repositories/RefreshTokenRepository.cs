﻿using CommonLib.Models;
using CommonLib.Models.Core;
using CoreServiceAPIs.Common.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Common.Repositories
{
    public class RefreshTokenRepository : BaseRepository<TbSecRefreshToken, CoreDbContext>, IRefreshTokenRepository
    {
        public RefreshTokenRepository(CoreDbContext context) : base(context)
        {

        }

        public async Task<TbSecRefreshToken> GetInformation(string refreshToken)
        {
            return await _context.TbSecRefreshToken.SingleOrDefaultAsync(x => x.Token == refreshToken);
        }
    }
}
