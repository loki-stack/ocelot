﻿using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Common.Constants;
using CoreServiceAPIs.Common.Dtos;
using CoreServiceAPIs.Common.Interfaces;
using CoreServiceAPIs.Common.Models;
using CoreServiceAPIs.Common.ShardingUtil;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Common.Repositories
{
    public class UserRepository : BaseRepository<TbSecUser, CoreDbContext>, IUserRepository
    {
        private readonly ILogger<IUserRepository> _logger;

        public UserRepository(CoreDbContext context, ILogger<IUserRepository> logger) : base(context)
        {
            _logger = logger;
        }

        public async Task<TbSecUser> GetUserByUserName(string username)
        {
            TbSecUser user = new TbSecUser();
            user = await _context.TbSecUser.Where(x => x.UserName == username).FirstOrDefaultAsync();
            return user;
        }

        public async Task<TbSecUser> GetUserByEmail(string email)
        {
            TbSecUser user = new TbSecUser();
            user = await _context.TbSecUser.Where(x => x.Email == email).FirstOrDefaultAsync();
            return user;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="clientId"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public async Task<TbSecUser> GetEntityUser(string userName, int clientId, int entityId)
        {
            TbSecUser user = new TbSecUser();
            user = await _context.TbSecUser.Where(x => string.Equals(x.UserName, userName) || string.Equals(x.Email, userName))
            .Where(x => x.ClientId == clientId)
            .Where(x => x.EntityId == entityId)
                .FirstOrDefaultAsync();
            return user;
        }

        /// <summary>
        /// Get by reset password token
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<TbSecUser> GetUserByPwdToken(string token)
        {
            TbSecUser user = new TbSecUser();
            user = await _context.TbSecUser.Where(x => x.ResetPwdToken == token).FirstOrDefaultAsync();
            return user;
        }

        public async Task<CreateObjectResult> CreateUser(CreateUserRequest request, StdRequestParam requestParam)
        {
            CreateObjectResult result = new CreateObjectResult();
            //check user exists
            if (_context.TbSecUser.Any(x => x.UserName == request.Username))
            {
                result.IsSuccess = false;
                result.Error = "Username already exists";
                return result;
            }
            if (_context.TbSecUser.Any(x => x.Email == request.Email))
            {
                result.IsSuccess = false;
                result.Error = "Email is already in use";
                return result;
            }
            try
            {
                TbSecUser user = new TbSecUser();
                user.UserName = request.Username;
                user.Password = Utils.MD5Hash(request.Username, request.Password);
                user.Email = request.Email;
                user.FullName = request.Fullname;
                user.StatusCd = Status.ACTIVE_CD;
                user.EntityId = requestParam.EntityId;
                user.ClientId = requestParam.ClientId;
                user.CreatedDt = DateTime.Now;
                user.CreatedBy = request.CreatedBy;
                await _context.TbSecUser.AddAsync(user);
                _context.SaveChanges();
                result.IsSuccess = true;
                result.Id = user.UserId;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                result.IsSuccess = false;
                result.Error = "An error occurred";
            }
            return result;
        }

        public async Task<UpdateObjectResult> UpdateUser(UpdateUserRequest request)
        {
            UpdateObjectResult result = new UpdateObjectResult();
            try
            {
                TbSecUser user = new TbSecUser();
                user = await _context.TbSecUser.FindAsync(request.UserId);
                if (user == null)
                {
                    result.IsSuccess = false;
                    result.Error = "User does not exist";
                    return result;
                }
                user.FullName = request.Fullname;
                user.Email = request.Email;
                user.StatusCd = request.StatusCD;
                if (request.IsUpdatePassword)
                {
                    user.Password = request.Password;
                }
                user.ModifiedDt = DateTime.Now;
                user.ModifiedBy = request.ModifiedBy;
                _context.Entry(user).State = EntityState.Modified;
                _context.SaveChanges();
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                result.IsSuccess = false;
                result.Error = "An error occurred";
            }
            return result;
        }

        public async Task<List<UserViewModel>> GetAll(PaginationRequest request, int clientId)
        {
            GetUserRequest userRequest = new GetUserRequest();
            if (!string.IsNullOrEmpty(request.Filter))
            {
                userRequest = JsonConvert.DeserializeObject<GetUserRequest>(request.Filter);
            }
            List<UserViewModel> listUser = new List<UserViewModel>();
            var q = (from u in _context.TbSecUser
                     join c in _context.TbCamClient
                     on u.ClientId equals c.ClientId
                     join e in _context.TbCamEntity
                     on u.EntityId equals e.EntityId
                     select new
                     {
                         u,
                         ClientName = c.ClientNameTxt,
                         EntityName = e.EntityNameTxt
                     }).AsQueryable();

            if (clientId > 0)
            {
                q = q.Where(x => x.u.ClientId == clientId);
            }

            var result = await (q).ToListAsync();

            if (!string.IsNullOrEmpty(request.Sort))
            {
                char sortCharacter = request.Sort.ElementAt(0);
                string sortField = request.Sort.Substring(1, request.Sort.Length - 1);
                if (sortField.ToLower().Contains("username"))
                {
                    if (sortCharacter == '-')
                        result = result.OrderByDescending(x => x.u.UserName).ToList();
                    else result = result.OrderBy(x => x.u.UserName).ToList();
                }
                if (sortField.ToLower().Contains("email"))
                {
                    if (sortCharacter == '-')
                        result = result.OrderByDescending(x => x.u.Email).ToList();
                    else result = result.OrderBy(x => x.u.Email).ToList();
                }
                if (sortField.ToLower().Contains("status"))
                {
                    if (sortCharacter == '-')
                        result = result.OrderByDescending(x => x.u.StatusCd).ToList();
                    else result = result.OrderBy(x => x.u.StatusCd).ToList();
                }
                if (sortField.ToLower().Contains("createddate"))
                {
                    if (sortCharacter == '-')
                        result = result.OrderByDescending(x => x.u.CreatedDt).ToList();
                    else result = result.OrderBy(x => x.u.CreatedDt).ToList();
                }
                if (sortField.ToLower().Contains("lastactive"))
                {
                    if (sortCharacter == '-')
                        result = result.OrderByDescending(x => x.u.LastActive).ToList();
                    else result = result.OrderBy(x => x.u.LastActive).ToList();
                }
            }

            if (!string.IsNullOrEmpty(userRequest.StatusCD))
            {
                result = result.Where(x => x.u.StatusCd == userRequest.StatusCD).ToList();
            }
            if (userRequest.CreatedDateFrom != null && userRequest.CreatedDateTo != null)
            {
                result = result.Where(x => x.u.CreatedDt >= userRequest.CreatedDateFrom.Value && x.u.CreatedDt <= userRequest.CreatedDateTo.Value).ToList();
            }
            if (userRequest.LastActiveFrom != null && userRequest.LastActiveTo != null)
            {
                result = result.Where(x => x.u.CreatedDt >= userRequest.LastActiveFrom.Value && x.u.CreatedDt <= userRequest.LastActiveTo.Value).ToList();
            }

            foreach (var item in result)
            {
                UserViewModel user = new UserViewModel(item.u);
                user.EntityName = item.EntityName;
                user.ClientName = item.ClientName;
                listUser.Add(user);
            }

            if (!string.IsNullOrEmpty(request.FullTextSearch))
            {
                listUser = listUser.Where(x => x.Username.Contains(request.FullTextSearch) ||
                                               x.Email.Contains(request.FullTextSearch) ||
                                               x.StatusName.Contains(request.FullTextSearch)).ToList();
            }
            return listUser;
        }

        public async Task<UserViewModel> GetUserInformation(int id)
        {
            UserViewModel user = null;
            var result = await (from u in _context.TbSecUser
                                join c in _context.TbCamClient
                                on u.ClientId equals c.ClientId
                                join e in _context.TbCamEntity
                                on u.EntityId equals e.EntityId
                                where u.UserId == id
                                select new
                                {
                                    u,
                                    ClientName = c.ClientNameTxt,
                                    EntityName = e.EntityNameTxt
                                }).FirstOrDefaultAsync();
            if (result != null)
            {
                user = new UserViewModel(result.u);
                user.EntityName = result.EntityName;
                user.ClientName = result.ClientName;
            }
            return user;
        }

        public async Task UpdateLastActive(int id)
        {
            TbSecUser user = new TbSecUser();
            user = await _context.TbSecUser.SingleOrDefaultAsync(x => x.UserId == id);
            user.LastActive = DateTime.Now;
            _context.Entry(user).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public async Task<int?> Update(TbSecUser user)
        {
            _context.TbSecUser.Update(user);
            await _context.SaveChangesAsync();
            return user.UserId;
        }

        public async Task<TbSecUser> GetUserByEmployeeId(int clientId, int entityId, int employeeId)
        {
            return await _context.TbSecUser.FirstOrDefaultAsync(x => x.ClientId == clientId
                        && x.EntityId == entityId && x.EmployeeId == employeeId);
        }

        public bool CheckExistUsername(int clientId, int userId, string username)
        {
            return _context.TbSecUser.Any(x => x.UserId != userId && x.UserName == username && x.ClientId == clientId);
        }

        public async Task UpdatePwdToken(int userId, string resetPwdToken, DateTimeOffset? resetPwdExpiryDt)
        {
            TbSecUser user = await _context.TbSecUser.Where(x => x.UserId == userId).FirstOrDefaultAsync();
            user.ResetPwdToken = resetPwdToken;
            user.ResetPwdExpiryDt = resetPwdExpiryDt;
            _context.Entry(user).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}