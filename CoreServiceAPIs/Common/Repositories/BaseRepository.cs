﻿using CoreServiceAPIs.Common.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Common.Repositories
{
    public abstract class BaseRepository<T, TContext> : IBaseRepository<T> where T : class where TContext : DbContext
    {
        protected readonly TContext _context;
        private DbSet<T> _entity;

        public BaseRepository(TContext context)
        {
            _context = context;
            _entity = _context.Set<T>();
        }

        public IEnumerable<T> All(Expression<Func<T, bool>> where)
        {
            return _entity.Where(where);
        }

        public void Delete(T entity)
        {
            _context.Remove(entity);
            _context.SaveChanges();
        }

        public bool Exist(Expression<Func<T, bool>> where)
        {
            return _entity.Any(where);
        }

        public async Task<T> Find(object id)
        {
            var e = await _entity.FindAsync(id);
            if (e != null)
            {
                _context.Entry(e).State = EntityState.Detached;
            }
            return e;
        }

        public void Patch(T entity)
        {
            _context.Attach(entity).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public T Save(T entity)
        {
            _entity.Add(entity);
            _context.SaveChanges();
            return entity;
        }

        public void Update(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}
