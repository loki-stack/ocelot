﻿namespace CoreServiceAPIs.Common.Models
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public string TokenExpires { get; set; }
        public string RefreshTokenExpries { get; set; }
        public string ChangePasswordUrl { get; set; }
        public string EntityCodeParam { get; set; }
        public string ClientCodeParam { get; set; }
        public string TokenParam { get; set; }
    }
}
