﻿namespace CoreServiceAPIs.Common.Models
{
    public class UpdateObjectResult
    {
        public bool IsSuccess { get; set; }
        public string Error { get; set; }
    }

    public class UpdateObjectResult<T>
    {
        public T Data { get; set; }
        public bool IsSuccess { get; set; }
        public string Error { get; set; }
    }
}