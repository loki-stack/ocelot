using CommonLib.Services;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Common.Scheduler
{
    public class SendEmailTimerService : IHostedService
    {
        private readonly IMessageService _msgService;
        private readonly ILogger<SendEmailTimerService> _logger;
        private Timer _timer;
        private static int usingResource = 0;

        public SendEmailTimerService(IMessageService msgService,
            ILogger<SendEmailTimerService> logger)
        {
            this._msgService = msgService;
            this._logger = logger;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            // timer repeates call to RemoveScheduledAccounts every 24 hours.
            _timer = new Timer(
                async o => await ProcessEmail(o),
                null,
                TimeSpan.Zero,
                TimeSpan.FromMinutes(2)
            );

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        private async Task ProcessEmail(object state)
        {
            if (0 == Interlocked.Exchange(ref usingResource, 1)) // thread control
            {
                _logger.LogInformation($"-----Email processing running----");
                await _msgService.ProcessEmails("system");
                Interlocked.Exchange(ref usingResource, 0);
            }
            else
            {
                _logger.LogInformation($"-----old task running--{Thread.CurrentThread.Name}--denied");
            }
        }
    }
}