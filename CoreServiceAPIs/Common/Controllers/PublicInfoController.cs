﻿using CommonLib.Dtos;
using CoreServiceAPIs.Common.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Common.Controllers
{
    /// <summary>
    /// Return public information about the application
    /// </summary>
    [Route("api/COM/[action]/[controller]")]
    [ApiController]
    [Authorize]
    public class PublicInfoController : ControllerBase
    {
        private readonly ILogger<PublicInfoController> _logger;
        private readonly IConfiguration _config;
        private readonly IWebHostEnvironment _env;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="config"></param>
        /// <param name="env"></param>
        public PublicInfoController(ILogger<PublicInfoController> logger, IConfiguration config, IWebHostEnvironment env)
        {
            this._logger = logger;
            this._config = config;
            this._env = env;
        }

        /// <summary>
        /// Get public information of the application
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [ActionName("COM9901")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<PublicInfoDto>>> PublicInfo([FromQuery] ApiRequest request)
        {
            _logger.LogInformation($"PublicInfo");
            ApiResponse<PublicInfoDto> apiResponse = new ApiResponse<PublicInfoDto>();
            PublicInfoDto data = new PublicInfoDto();

            await Task.Run(
                () =>
                {
                    data.Environment = this._env.EnvironmentName;
                    data.ApplicationName = this._env.ApplicationName;
                    data.DbInfo = GetDbBaseInfo(this._config["ConnectionStrings:DefaultConnection"]);
                    // TODO - in future - get GitInfo, version, build timestamp
                }
            );
            apiResponse.Data = data;
            return apiResponse;
        }

        private string GetDbBaseInfo(string input)
        {
            string[] subs = input.Split(';');
            string[] vals = new string[2];

            foreach (var sub in subs)
            {
                if (sub.ToLower().StartsWith("server"))
                {
                    vals[0] = sub;
                }
                else if (sub.ToLower().StartsWith("database"))
                {
                    vals[1] = sub;
                }

            }
            return string.Join(';', vals);
        }
    }
}