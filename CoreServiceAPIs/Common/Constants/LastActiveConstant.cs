﻿namespace CoreServiceAPIs.Common.Constants
{
    public enum LastActivePeriod
    {
        LAST_SEVEN_DAYS = 0,
        LAST_MONTH = 1,
        THIS_QUARTER = 2,
        LAST_QUARTER = 3,
        THIS_YEAR = 4,
        LAST_YEAR = 5
    }
}
