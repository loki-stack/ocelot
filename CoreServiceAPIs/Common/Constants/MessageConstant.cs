﻿#pragma warning disable 1591

namespace CoreServiceAPIs.Common.Constants
{
    public struct Level
    {
        public const string ERROR = "ERROR";
        public const string SUCCESS = "SUCCESS";
        public const string INFO = "INFO";
        public const string WARN = "WARN";
    }

    public struct Toast
    {
        public const string FORM = "FORM";
        public const string NOTIFICATION = "NOTIFICATION";
        public const string TOAST = "toast";
    }

    public struct Component
    {
        public const string NAME = "name";
        public const string CODE = "code";
        public const string REMARK = "remark";
        public const string STATUS = "status";
        public const string DISPLAYORDER = "displayOrder";
        public const string UNIT_CODE = "unitCode";
        public const string ACCOUNT_CODE = "accountCode";
        public const string CODEVALUE = "codeValue";
        public const string START_DATE = "startDate";
        public const string USERNAME = "username";
        public const string GLOBAL_LEVEL_CALENDAR_NAME = "globalLevelCalendarName";
        public const string COUNTRY = "country";
        public const string DATE = "date";
        public const string DESCRIPTION_PRIMARY_LANGUAGE = "descriptionPrimaryLanguage";
        public const string HOLIDAY_TYPE = "holidayType";
        public const string IS_WORKING_DAY = "isWorkingDay";
    }

    public struct Label
    {
        #region RoleGroup

        public const string RG01_E_001 = "RG01_E_001";
        public const string RG01_E_002 = "RG01_E_002"; // doest not exist
        public const string RG01_E_003 = "RG01_E_003"; // validation error
        public const string RG01_E_004 = "RG01_E_004"; // delete failed
        public const string RG01_E_005 = "RG01_E_005"; // add failed

        public const string RG01_I_001 = "RG01_I_001"; // role group added successfully
        public const string RG01_I_002 = "RG01_I_002"; // role group deleted
        public const string RG01_I_003 = "RG01_I_003"; // role group updated successfully

        public const string T_ROLEGROUP_INFO = "T_ROLEGROUP_INFO";
        public const string T_ROLEGROUP_ERROR = "T_ROLEGROUP_ERROR";

        #endregion RoleGroup

        #region DataAccessGroup

        public const string T_DATAACCESSGROUP_INFO = "T_DATAACCESSGROUP_INFO";
        public const string T_DATAACCESSGROUP_ERROR = "T_DATAACCESSGROUP_ERROR";

        public const string DAR01_E_001 = "DAR01_E_001"; // add failed
        public const string DAR01_E_002 = "DAR01_E_002"; // not exists
        public const string DAR01_E_003 = "DAR01_E_003"; // validation errors
        public const string DAR01_E_004 = "DAR01_E_004"; // update failed
        public const string DAR01_E_005 = "DAR01_E_005";
        public const string DAR01_E_006 = "DAR01_E_006";
        public const string DAR01_I_001 = "DAR01_I_001"; // add successfully
        public const string DAR01_I_002 = "DAR01_I_002"; // update successfully
        public const string DAR01_I_003 = "DAR01_I_003";
        public const string DAR01_I_004 = "DAR01_I_004";

        public const string DAR01_V_001 = "DAR01_V_001"; // DAR not exists
        public const string DAR01_V_002 = "DAR01_V_002";
        public const string DAR01_V_003 = "DAR01_V_003";

        #endregion DataAccessGroup

        #region Funtion

        public const string FUNC01E001 = "FUNC01E001";
        public const string FUNC01E002 = "FUNC01E002";
        public const string T_FUNCTION_INFO = "T_FUNCTION_INFO";
        public const string T_FUNCTION_ERROR = "T_FUNCTION_ERROR";

        #endregion Funtion

        #region Security Profile

        public const string SP01_E_001 = "SP01_E_001";
        public const string SP01_E_002 = "SP01_E_002";
        public const string SP01_I_001 = "SP01_I_001";
        public const string T_SECPRO_INFO = "T_SECPRO_INFO";
        public const string T_SECPRO_ERROR = "T_SECPRO_ERROR";

        #endregion Security Profile

        #region Menu

        public const string MENU01E001 = "MENU01E001";
        public const string T_MENU_ERROR = "T_MENU_ERROR";

        #endregion Menu

        #region user

        public const string T_USER_ERROR = "T_USER_ERROR";
        public const string T_USER_INFO = "T_USER_INFO";
        public const string T_USER_WARN = "T_USER_WARN";
        public const string USR0101 = "USR0101";
        public const string USR0102 = "USR0102";
        public const string USR0103 = "USR0103";
        public const string USR0104 = "USR0104";
        public const string USR0105 = "USR0105";
        public const string USR0106 = "USR0106";
        public const string USR0107 = "USR0107";
        public const string USR0108 = "USR0108";
        public const string USR0109 = "USR0109";
        public const string USR0110 = "USR0110";
        public const string USR0111 = "USR0111";
        public const string USR0112 = "USR0112";

        #endregion user

        #region authentication

        public const string T_AUTHEN_ERROR = "T_AUTHEN_ERROR";
        public const string T_AUTHEN_INFO = "T_AUTHEN_INFO";
        public const string T_AUTHEN_WARN = "T_AUTHEN_WARN";
        public const string AUTH0101 = "AUTH0101";
        public const string AUTH0102 = "AUTH0102";
        public const string AUTH0103 = "AUTH0103";
        public const string AUTH0104 = "AUTH0104";
        public const string AUTH0105 = "AUTH0105";
        public const string AUTH0106 = "AUTH0106";

        #endregion authentication

        #region MPF trustee

        public const string T_APPROVED_TRUSTEE_ERROR = "T_APPROVED_TRUSTEE_ERROR";
        public const string T_APPROVED_TRUSTEE_INFO = "T_APPROVED_TRUSTEE_INFO";
        public const string T_APPROVED_TRUSTEE_WARN = "T_APPROVED_TRUSTEE_WARN";
        public const string MPFT0101 = "MPFT0101";
        public const string MPFT0102 = "MPFT0102";
        public const string MPFT0103 = "MPFT0103";
        public const string MPFT0104 = "MPFT0104";

        #endregion MPF trustee

        #region Setting

        public const string T_SETTING_ERROR = "T_SETTING_ERROR";
        public const string T_SETTING_INFO = "T_SETTING_INFO";
        public const string T_SETTING_WARN = "T_SETTING_WARN";
        public const string SET0101 = "SET0101";
        public const string SET0102 = "SET0102";
        public const string SET0103 = "SET0103";
        public const string SET0104 = "SET0104";
        public const string SET0105 = "SET0105";
        public const string SET0106 = "SET0106";
        public const string SET0107 = "SET0107";
        public const string SET0108 = "SET0108";
        public const string SET0109 = "SET0109";
        public const string SET0110 = "SET0110";
        public const string SET0111 = "SET0111";
        public const string SET0112 = "SET0112";
        public const string SET0113 = "SET0113";
        public const string SET0114 = "SET0114";
        public const string SET0115 = "SET0115";
        public const string SET0116 = "SET0116";
        public const string SET0117 = "SET0117";
        public const string SET0118 = "SET0118";
        public const string SET0119 = "SET0119";
        public const string SET0120 = "SET0120";

        #endregion Setting

        #region User security profile

        public const string T_USER_SEC_PRO_ERROR = "T_USER_SEC_PRO_ERROR";
        public const string T_USER_SEC_PRO_INFO = "T_USER_SEC_PRO_INFO";
        public const string T_USER_SEC_PRO_WARN = "T_USER_SEC_PRO_WARN";
        public const string USP0101 = "USP0101";
        public const string USP0102 = "USP0102";
        public const string USP0103 = "USP0103";
        public const string USP0104 = "USP0104";
        public const string USP0105 = "USP0105";
        public const string USP0106 = "USP0106";
        public const string USP0107 = "USP0107";

        #endregion User security profile

        #region Internal Job Title

        public const string IJT0101 = "IJT0101";
        public const string IJT0102 = "IJT0102";
        public const string IJT0103 = "IJT0103";
        public const string IJT0104 = "IJT0104";
        public const string IJT0105 = "IJT0105";
        public const string IJT0106 = "IJT0106";
        public const string IJT0107 = "IJT0107";

        #endregion Internal Job Title

        #region External Job Title

        public const string EJT01E001 = "EJT01E001";
        public const string EJT01E002 = "EJT01E002";
        public const string EJT01E003 = "EJT01E003";
        public const string EJT01E004 = "EJT01E004";
        public const string EJT01E005 = "EJT01E005";
        public const string EJT001 = "EJT001";
        public const string EJT002 = "EJT002";
        public const string EJT003 = "EJT003";
        public const string EJT004 = "EJT004";
        public const string EJT005 = "EJT005";
        public const string EJT006 = "EJT006";

        #endregion External Job Title

        #region GenericCode

        public const string GC01E001 = "GC01E001";
        public const string GC01E002 = "GC01E002";
        public const string GC01E003 = "GC01E003";
        public const string GC01E004 = "GC01E004";
        public const string GC01E005 = "GC01E005";
        public const string GC01E006 = "GC01E006";
        public const string GC01E007 = "GC01E007";
        public const string GC0101 = "GC0101";
        public const string GC0102 = "GC0102";
        public const string GC0103 = "GC0103";
        public const string GC0104 = "GC0104";
        public const string GC0105 = "GC0105";
        public const string GC0106 = "GC0106";

        public const string PAY0301 = "PAY0301";
        public const string PAY0303 = "PAY0303";
        public const string PAY0302 = "PAY0302";
        public const string PAY0304 = "PAY0304";
        public const string PAY0305 = "PAY0305";
        public const string PAY0306 = "PAY0306";
        public const string PAY0307 = "PAY0307";

        #endregion GenericCode

        #region Master Pay Item

        public const string MPI0101 = "MPI0101";

        #endregion Master Pay Item

        #region Cost Center

        public const string CC01E001 = "CC01E001";
        public const string CC01E002 = "CC01E002";
        public const string CC01E005 = "CC01E005";
        public const string CC01E006 = "CC01E006";
        public const string CC01E007 = "CC01E007";
        public const string CC01E008 = "CC01E008";
        public const string CC0101 = "CC0101";
        public const string CC0102 = "CC0102";
        public const string CC0103 = "CC0103";
        public const string CC0104 = "CC0104";

        #endregion Cost Center

        #region Registered Scheme

        public const string RS001 = "RS001";
        public const string RS002 = "RS002";
        public const string RS003 = "RS003";
        public const string RS004 = "RS004";
        public const string RS0101 = "RS0101";
        public const string RS0102 = "RS0102";
        public const string RS0103 = "RS0103";
        public const string RS0104 = "RS0104";
        public const string RS0105 = "RS0105";
        public const string RS0106 = "RS0106";

        #endregion Registered Scheme

        #region ORSO Scheme

        public const string ORSO001 = "ORSO001";
        public const string ORSO002 = "ORSO002";

        #endregion ORSO Scheme

        #region employee portal

        public const string ESS50_E_001 = "ESS50_E_001"; // user not exists for entity
        public const string ESS50_E_002 = "ESS50_E_002"; // token not verified
        public const string ESS50_E_003 = "ESS50_E_003"; // password not matched
        public const string ESS50_E_004 = "ESS50_E_004"; // token expired
        public const string ESS50_E_005 = "ESS50_E_005";
        // public const string ESS50_E_006 = "ESS50_E_006";
        // public const string ESS50_E_007 = "ESS50_E_007";
        // public const string ESS50_E_008 = "ESS50_E_008";
        // public const string ESS50_E_009 = "ESS50_E_009";
        // public const string ESS50_E_020 = "ESS50_E_010";

        // Info
        public const string ESS50_I_001 = "ESS50_I_001"; // marked reset

        public const string ESS50_I_002 = "ESS50_I_002"; // Password updated.
        public const string ESS50_I_003 = "ESS50_I_003";

        #endregion employee portal

        #region Global Calendar

        public const string WCL0101 = "WCL0101";
        public const string WCL0102 = "WCL0102";
        public const string WCL0103 = "WCL0103";
        public const string WCL0104 = "WCL0104";
        public const string WCL0105 = "WCL0105";
        public const string WCL0106 = "WCL0106";
        public const string WCL0107 = "WCL0107";
        public const string WCL0108 = "WCL0108";
        public const string WCL0109 = "WCL0109";
        public const string WCL0110 = "WCL0110";
        public const string GLCD0101 = "GLCD0101";
        public const string GLCD0102 = "GLCD0102";
        public const string GLCD0103 = "GLCD0103";

        #endregion Global Calendar

        #region Holiday

        public const string HLD0101 = "HLD0101";
        public const string HLD0102 = "HLD0102";
        public const string HLD0103 = "HLD0103";
        public const string HLD0104 = "HLD0104";
        public const string HLD0105 = "HLD0105";
        public const string HLD0106 = "HLD0106";
        public const string HLD0107 = "HLD0107";
        public const string HLD0108 = "HLD0108";
        public const string HLD0109 = "HLD0109";
        public const string HLD0110 = "HLD0110";
        public const string HLD0111 = "HLD0111";

        #endregion Holiday

        #region Email

        public const string EML001 = "Send email successful";
        public const string EML002 = "Send email fail";

        #endregion Email
    }

    public struct ValMsg
    {
        public const string VAL_REQUIRED = "VAL_REQUIRED";
        public const string VAL_MAXLENGTH = "VAL_MAXLENGTH";
        public const string VAL_DUPLICATE = "VAL_DUPLICATE";
    }

    public enum SignInMethod
    {
        /// <summary>
        /// login with username
        /// </summary>
        USERNAME = 1
    }
}

#pragma warning restore 1591