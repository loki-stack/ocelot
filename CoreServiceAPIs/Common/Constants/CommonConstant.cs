﻿namespace CoreServiceAPIs.Common.Constants
{
    public struct Status
    {
        public const string ACTIVE_CD = "AT";
        public const string INACTIVE_CD = "IT";
        public const string ACTIVE_VALUE = "Active";
        public const string INACTIVE_VALUE = "Inactive";
        public const string ACTIVE = "ACTIVE";
        public const string INACTIVE = "INACTIVE";
    }

    public enum StatusCode
    {
        SUCCESS = 0,
        INVALID = -1,
        ACCOUNT_LOCKED = -2
    }

    public struct GenericCode
    {
        public const string SCHEME_TYPE = "SCHEMETYPE";
        public const string CONTRIBUTION_CYCLE = "CONTRIBUTION";
        public const string PAYMENT_METHOD = "PAYMENTMETHOD";
        public const string CURRENCY = "CURRENCY";
        public const string STATUS = "STATUS";
        public const string FEE_UNIT = "FEEUNIT";
        public const string ROUNDING_TYPE = "ROUNDINGTYPE";
        public const string SCHEME_TYPE_MPF = "MPF";
        public const string SCHEME_TYPE_ORSO = "ORSO";
        public const string FORMULAS_LEVEL = "00007";
    }

    public struct FunctionNature
    {
        public const string SEARCH = "search";
        public const string CREATE = "create";
        public const string UPDATE = "update";
        public const string DELETE = "delete";
        public const string CLONE = "clone";
    }

    public struct FunctionCode
    {
        public const string SEARCH = "01";
        public const string SEARCH_11 = "11";
        public const string CREATE = "02";
        public const string UPDATE = "03";
        public const string DELETE = "04";
        public const string CLONE = "05";
    }
}