﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;

namespace CoreServiceAPIs.Common.ShardingUtil
{
    public class AddRequiredHeaderParameter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (operation.Parameters == null)
                operation.Parameters = new List<OpenApiParameter>();

            operation.Parameters.Add(new OpenApiParameter
            {
                Name = "ray",
                In = ParameterLocation.Header,
                Required = true,
                Schema = new OpenApiSchema
                {
                    Type = "String"
                }
            });
            //operation.Parameters.Add(new OpenApiParameter
            //{
            //    Name = "parameter",
            //    In = ParameterLocation.Header,
            //    Required = true,
            //    Schema = new OpenApiSchema
            //    {
            //        Type = "String"
            //    },
            //    //Description = "Ex: \"locale\":\"en\",\"clientCode\":\"C123\",\"companyCode\":\"ABC\",\"tenantId\":\"T0001\",\"genericCodeList\":[\"GENDER\",\"TITLE\",\"COUNTRY\"]"
            //}); ;
        }
    }
}