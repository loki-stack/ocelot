﻿using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Controllers
{
    [Authorize]
    [Route("api/SEC/[action]/[controller]")]
    [ApiController]
    public class UserSecurityProfileController : ControllerBase
    {
        private readonly IUserSecurityProfileService _userSecurityProfileService;
        private readonly ILogger<UserSecurityProfileController> _logger;
        private readonly IClientService _clientService;
        public UserSecurityProfileController(ILogger<UserSecurityProfileController> logger, IUserSecurityProfileService userSecurityProfileService, IClientService clientService)
        {
            _userSecurityProfileService = userSecurityProfileService;
            _logger = logger;
            _clientService = clientService;
        }
        [ActionName("SEC0801")]
        [HttpGet("GetUserSecurityProfile")]
        public async Task<IActionResult> GetUserSecurityProfile([FromQuery] ApiRequest<GetUserSecurityProfileRequest> request)
        {
            try
            {
                _logger.LogInformation("GetUserSecurityProfile");
                var response = await _userSecurityProfileService.GetUserSecurityProfile(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return BadRequest();
            }
        }

        [ActionName("SEC0802")]
        [HttpPost("CreateUserSecurityProfile")]
        public async Task<IActionResult> CreateUserSecurityProfile([FromBody] ApiRequest<UserSecurityProfileRequest> request)
        {
            try
            {
                _logger.LogInformation("CreateUserSecurityProfile");
                var response = await _userSecurityProfileService.CreateUserSecurityProfile(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return BadRequest();
            }
        }

        [ActionName("SEC0804")]
        [HttpDelete("DeleteUserSecurityProfile")]
        public async Task<IActionResult> DeleteUserSecurityProfile([FromQuery] ApiRequest<UserSecurityProfileRequest> request)
        {
            try
            {
                _logger.LogInformation("DeleteUserSecurityProfile");
                var response = await _userSecurityProfileService.DeleteUserSecurityProfile(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return BadRequest();
            }
        }

        [HttpPut("{userId}")]
        [ActionName("SEC0803")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        //public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> Update(int id, [FromBody]ApiRequest<UserSecurityProfileViewModel> request)
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> Update(int userId, [FromBody] ApiRequest<UserSecurityProfileDto> request)
        {
            try
            {
                _logger.LogInformation($"UpdateUserSecurityProfile --- UserId: {userId}");
                string username = User.FindFirst(ClaimTypes.Name).Value;
                var response = await _userSecurityProfileService.Update(request, username, userId);
                /* if (response.Data == null)
                    return BadRequest(response); */
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
