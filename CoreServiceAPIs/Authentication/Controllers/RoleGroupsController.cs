﻿using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using CoreServiceAPIs.Common.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Controllers
{
    [Route("api/SEC/[action]/[controller]")]
    [ApiController]
    [Authorize]
    public class RoleGroupsController : ControllerBase
    {
        private readonly IRoleGroupService _roleGroupService;

        private readonly IClientService _clientService;
        private readonly ILogger<RoleGroupsController> _logger;

        public RoleGroupsController(IRoleGroupService roleGroupService,
            IClientService clientService, ILogger<RoleGroupsController> logger)
        {
            _roleGroupService = roleGroupService;
            _clientService = clientService;
            _logger = logger;
        }

        [HttpPost]
        [ActionName("SEC0402")]
        [Authorize(Roles = "SEC0402")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> Create([FromBody] ApiRequest<CreateRoleGroupRequest> request)
        {
            try
            {
                string username = User.FindFirst(ClaimTypes.Name).Value;
                _logger.LogInformation($"CreateRoleGroup --- Username: {username}");
                var response = await _roleGroupService.Create(request, username);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        [ActionName("SEC0403")]
        [Authorize(Roles = "SEC0403")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> Update(int id, [FromBody] ApiRequest<CreateRoleGroupRequest> request)
        {
            try
            {
                _logger.LogInformation($"UpdateRoleGroup --- RoleGroupId: {id}");
                request.Data.RoleGroupId = id;
                string username = User.FindFirst(ClaimTypes.Name).Value;
                var response = await _roleGroupService.Update(request, username);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet]
        [ActionName("SEC0401")]
        [Authorize(Roles = "SEC0401")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> GetAll([FromQuery] ApiRequest<PaginationRequest> request)
        {
            try
            {
                _logger.LogInformation("GetAllRoleGroup");
                if (request.Data == null)
                    request.Data = new PaginationRequest();
                var response = await _roleGroupService.GetAll(request);
                if (response.Data == null)
                    return NotFound();
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("{id}")]
        [ActionName("SEC0401")]
        [Authorize(Roles = "SEC0401")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> GetById(int id, [FromQuery] ApiRequest request)
        {
            try
            {
                _logger.LogInformation($"GetRoleGroup --- ByRoleGroupId: {id}");
                var response = await _roleGroupService.GetById(id, request);
                if (response.Data == null)
                    return NotFound();
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpDelete("{id}")]
        [ActionName("SEC0404")]
        [Authorize(Roles = "SEC0404")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> Delete(int id, [FromQuery] ApiRequest request)
        {
            try
            {
                _logger.LogInformation($"DeleteRoleGroup --- RoleGroupId: {id}");
                var response = await _roleGroupService.Delete(id, request);
                if (response.Data == null)
                    return BadRequest();
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("list")]
        [ActionName("SEC0401")]
        [Authorize(Roles = "SEC0401")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<FarManagementDto>>> List([FromQuery] ApiRequest<string> request)
        {
            try
            {
                _logger.LogInformation("List");
                var response = new ApiResponse<FarManagementDto>();
                // TODO
                StdRequestParam param = request.Parameter;
                FarManagementDto data = new FarManagementDto();
                // get all role group
                _logger.LogDebug($"-----");
                data.AllRoleGroup = await _roleGroupService.GetEntityRoleGroup(param.ClientId, param.EntityId);

                response.Data = data;

                // if (response.Data == null)
                //     return NotFound();
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("treeInfo")]
        [ActionName("SEC0401")]
        [Authorize(Roles = "SEC0401")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<FarManagementDto>>> TreeInfo([FromQuery] ApiRequest<int> request)
        {
            try
            {
                _logger.LogInformation("TreeInfo");
                var response = new ApiResponse<FarManagementDto>();
                // TODO
                StdRequestParam param = request.Parameter;
                //
                //data.FuncList = await _roleGroupService.GetAllFuncAsTree("en", clientInfo.ClientId, clientInfo.EntityId);
                // get all role group
                _logger.LogDebug($"-----");
                // function list is global
                // data.FuncList = await _roleGroupService.GetAllFuncAsTree("en", 0, 0);

                //data.AllRoleGroup = await _roleGroupService.GetEntityRoleGroup(clientInfo.ClientId, clientInfo.EntityId);
                //response.Data = data;
                response = await _roleGroupService.GetManageData(request.Data, param.Locale);

                // if (response.Data == null)
                //     return NotFound();
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}