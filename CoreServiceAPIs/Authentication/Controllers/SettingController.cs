﻿using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoreServiceAPIs.Authentication.Controllers
{
    [Authorize]
    [Route("api/CFG/[action]/[controller]")]
    [ApiController]
    public class SettingController : ControllerBase
    {
        private readonly ISettingService _settingService;
        private readonly ILogger<SettingController> _logger;

        public SettingController(ISettingService settingService, ILogger<SettingController> logger)
        {
            _settingService = settingService;
            _logger = logger;
        }

        [ActionName("CFG0201")]
        [HttpGet("EntitySetting")]
        [Authorize(Roles = "CFG0201")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetEntitySetting([FromQuery] ApiRequest request)
        {
            try
            {
                var response = await _settingService.GetEntitySetting(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }

        [ActionName("CFG0201")]
        [HttpGet("EmployeeSetting")]
        [Authorize(Roles = "CFG0201")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetEmployeeSetting([FromQuery] ApiRequest request)
        {
            try
            {
                var response = await _settingService.GetEmployeeSetting(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }

        [ActionName("CFG0203")]
        [HttpPut("EmployeeSetting")]
        [Authorize(Roles = "CFG0203")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateEmployeeSetting([FromBody] ApiRequest<SettingUpdateEmployeeRequest> request)
        {
            try
            {
                var response = await _settingService.UpdateEmployeeSetting(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }

        [ActionName("CFG0201")]
        [HttpGet("PDF")]
        [Authorize(Roles = "CFG0201")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetPDFSetting([FromQuery] ApiRequest request)
        {
            try
            {
                var response = await _settingService.GetPDFSetting(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }

        [ActionName("CFG0203")]
        [HttpPut("PDF")]
        [Authorize(Roles = "CFG0203")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdatePDFSetting([FromBody] ApiRequest<SettingUpdatePDFRequest> request)
        {
            try
            {
                var response = await _settingService.UpdatePDFSetting(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }

        [ActionName("CFG0201")]
        [HttpGet("Language")]
        [Authorize(Roles = "CFG0201")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetLanguageSetting([FromQuery] ApiRequest request)
        {
            try
            {
                var response = await _settingService.GetLanguageSetting(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }

        [ActionName("CFG0203")]
        [HttpPut("Language")]
        [Authorize(Roles = "CFG0203")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateLanguageSetting([FromBody] ApiRequest<SettingUpdateLanguageRequest> request)
        {
            try
            {
                var response = await _settingService.UpdateLanguageSetting(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }

        [ActionName("CFG0201")]
        [HttpGet("ProbationPeriod")]
        [Authorize(Roles = "CFG0201")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetProbationPeriodSetting([FromQuery] ApiRequest request)
        {
            try
            {
                var response = await _settingService.GetProbationPeriodSetting(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }

        [ActionName("CFG0203")]
        [HttpPut("ProbationPeriod")]
        [Authorize(Roles = "CFG0203")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateProbationPeriodSetting([FromBody] ApiRequest<SettingUpdateProbationPeriodRequest> request)
        {
            try
            {
                var response = await _settingService.UpdateProbationPeriodSetting(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }

        [ActionName("CFG0201")]
        [HttpGet("Currency")]
        [Authorize(Roles = "CFG0201")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetCurrencySetting([FromQuery] ApiRequest request)
        {
            try
            {
                var response = await _settingService.GetCurrencySetting(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }

        [ActionName("CFG0203")]
        [HttpPut("Currency")]
        [Authorize(Roles = "CFG0203")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateCurrencySetting([FromBody] ApiRequest<SettingUpdateCurrencyRequest> request)
        {
            try
            {
                var response = await _settingService.UpdateCurrencySetting(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }

        [ActionName("CFG0201")]
        [HttpGet("WorkCalendar")]
        [Authorize(Roles = "CFG0201")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWorkCalendarSetting([FromQuery] ApiRequest request)
        {
            try
            {
                var response = await _settingService.GetWorkCalendarSetting(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }

        [ActionName("CFG0203")]
        [HttpPut("WorkCalendar")]
        [Authorize(Roles = "CFG0203")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateWorkCalendarSetting([FromBody] ApiRequest<SettingUpdateWorkCalendarRequest> request)
        {
            try
            {
                var response = await _settingService.UpdateWorkCaldendarSetting(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }

        [ActionName("CFG0201")]
        [HttpGet("SignInMethod")]
        [Authorize(Roles = "CFG0201")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetSignInSetting([FromQuery] ApiRequest request)
        {
            try
            {
                var response = await _settingService.GetSignInSetting(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }

        [ActionName("CFG0203")]
        [HttpPut("SignInMethod")]
        [Authorize(Roles = "CFG0203")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateSignInSetting([FromBody] ApiRequest<SettingSignInMethodRequest> request)
        {
            try
            {
                var response = await _settingService.UpdateSignInMethodSetting(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }

        [ActionName("CFG0201")]
        [HttpGet("Languages")]
        [Authorize(Roles = "CFG0201")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetLanguages([FromQuery] ApiRequest<string> request)
        {
            try
            {
                var response = await _settingService.GetLanguages(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }

        [ActionName("CFG0201")]
        [HttpGet("NotificationEmail")]
        [Authorize(Roles = "CFG0201")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetNotificationEmailSetting([FromQuery] ApiRequest request)
        {
            try
            {
                var response = await _settingService.GetNotificationEmailSetting(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }

        [ActionName("CFG0203")]
        [HttpPut("NotificationEmail")]
        [Authorize(Roles = "CFG0203")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateNotificationEmailSetting([FromBody] ApiRequest<SettingNotificationEmailRequest> request)
        {
            try
            {
                var response = await _settingService.UpdateNotificationEmailSetting(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }
    }
}