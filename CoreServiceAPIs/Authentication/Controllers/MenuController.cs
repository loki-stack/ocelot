﻿using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using CoreServiceAPIs.Common.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Controllers
{
    [Route("api/COM/[action]/[controller]")]
    [ApiController]
    [Authorize]
    public class MenuController : ControllerBase
    {
        private readonly IMenuService _menuService;
        private readonly ILogger<MenuController> _logger;

        public MenuController(IMenuService menuService, ILogger<MenuController> logger)
        {
            _menuService = menuService;
            _logger = logger;
        }

        [HttpGet]
        [ActionName("COM0301")]
        [Authorize(Roles = "COM0301")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> GetMenu([FromQuery] ApiRequest request)
        {
            try
            {
                if (request.Parameter == null)
                    request.Parameter = new StdRequestParam();
                int userId = Convert.ToInt32(User.FindFirst("Id").Value);
                var result = await _menuService.GetMenu(userId, request);
                if (result.Data == null && result.Message.Toast[0].Level == Level.ERROR)
                    return BadRequest(result);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}