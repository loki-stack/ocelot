using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using CoreServiceAPIs.Common.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Controllers
{
    /// <summary>
    /// </summary>
    [Route("api/SEC/[action]/[controller]")]
    [ApiController]
    [Authorize]
    public class DataAccessGroupsController : ControllerBase
    {
        private readonly IDataAccessGroupService _dataAccessGroupService;

        private readonly IClientService _clientService;
        private readonly ILogger<DataAccessGroupsController> _logger;
        private readonly IGenericCodeService _genericCodeService;

        /// <summary>
        /// DataAccessGroupsController Constructor
        /// </summary>
        public DataAccessGroupsController(
            IDataAccessGroupService dataAccessGroupService
            ,IClientService clientService
            , ILogger<DataAccessGroupsController> logger
            , IGenericCodeService genericCodeService
            )
        {
            _dataAccessGroupService = dataAccessGroupService;
            _genericCodeService = genericCodeService;
            _clientService = clientService;
            _logger = logger;
        }

        /// <summary>
        /// List
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet("list")]
        [ActionName("SEC1001")]
        [Authorize(Roles = "SEC1001")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<DarManagementDto>>> List([FromQuery] ApiRequest<string> request)
        {
            try
            {
                _logger.LogInformation("List");
                var response = new ApiResponse<DarManagementDto>();
                // TODO
                StdRequestParam param = request.Parameter;
                DarManagementDto data = new DarManagementDto();
                data.AllDataAccessGroup = await _dataAccessGroupService.ListAll(param.ClientId, param.EntityId);

                response.Data = data;

                // if (response.Data == null)
                //     return NotFound();
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("SEC1002")]
        [Authorize(Roles = "SEC1002")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> Create([FromBody] ApiRequest<CreateDataAccessGroupRequest> request)
        {
            try
            {
                string username = User.FindFirst(ClaimTypes.Name).Value;
                var response = await _dataAccessGroupService.Create(request, username);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// GetDARById
        /// </summary>
        /// <param name="request"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("dataaccess/{id}")]
        [ActionName("SEC1001")]
        [Authorize(Roles = "SEC1001")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<DarManagementDto>>> GetDARById(int id, [FromQuery] ApiRequest request)
        {
            try
            {
                string username = User.FindFirst(ClaimTypes.Name).Value;
                ClientResult clientInfo = await _clientService.GetClientEntityId(request.Parameter.ClientId, request.Parameter.EntityId);
                var response = await _dataAccessGroupService.GetDARById(id, username, clientInfo);
                var genericCodes = request.Parameter.GenericCodeList.ToList();
                response.Data.Remuneration = await _genericCodeService.GetGenericCodes(request.Parameter.ClientId, request.Parameter.EntityId, request.Parameter.Locale, genericCodes);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// GetAllClientEntitiesAsTree
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("SEC1004")]
        [Authorize(Roles = "SEC1004")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<DarManagementDto>>> GetAllClientEntitiesAsTree([FromBody] ApiRequest<int> request)
        {
            try
            {
                string username = User.FindFirst(ClaimTypes.Name).Value;
                ClientResult clientInfo = await _clientService.GetClientEntityId(request.Parameter.ClientId, request.Parameter.EntityId);
                var response = await _dataAccessGroupService.GetAllClientEntitiesAsTree(request, clientInfo);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="request"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ActionName("SEC1003")]
        [Authorize(Roles = "SEC1003")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> Update(int id, [FromBody] ApiRequest<UpdateDataAccessGroupRequest> request)
        {
            try
            {
                request.Data.DataAccessGroupId = id;
                string username = User.FindFirst(ClaimTypes.Name).Value;
                var response = await _dataAccessGroupService.Update(request, username, request.Parameter.ClientId, request.Parameter.EntityId);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}