﻿using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using CoreServiceAPIs.Common.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Controllers
{
    [Route("api/SEC/[action]/[controller]")]
    [ApiController]
    [Authorize]
    public class FunctionsController : ControllerBase
    {
        private readonly IFunctionService _functionService;
        private readonly ILogger<FunctionsController> _logger;

        public FunctionsController(IFunctionService functionService, ILogger<FunctionsController> logger)
        {
            _functionService = functionService;
            _logger = logger;
        }

        [HttpGet]
        [ActionName("SEC0301")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> GetAll([FromQuery] ApiRequest<PaginationRequest> request)
        {
            try
            {
                if (request.Data == null)
                    request.Data = new PaginationRequest();
                var response = await _functionService.GetAll(request);
                if (response.Data == null)
                    return NotFound();
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("{id}")]
        [ActionName("SEC0301")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> GetById(int id, [FromQuery] ApiRequest request)
        {
            try
            {
                var response = await _functionService.GetById(id, request);
                if (response.Data == null)
                    return NotFound();
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        [ActionName("SEC0302")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> Create([FromBody] ApiRequest<CreateFuncRequest> request)
        {
            try
            {
                string username = User.FindFirst(ClaimTypes.Name).Value;
                var response = await _functionService.Create(request, username);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        [ActionName("SEC0303")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> Update(int id, [FromBody] ApiRequest<UpdateFuncRequest> request)
        {
            try
            {
                request.Data.FunctionId = id;
                string username = User.FindFirst(ClaimTypes.Name).Value;
                var response = await _functionService.Update(request, username);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpDelete("{id}")]
        [ActionName("SEC0304")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> Delete(int id, [FromQuery] ApiRequest request)
        {
            try
            {
                var response = await _functionService.Delete(id, request);
                if (response.Data == null)
                    return BadRequest();
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}