﻿using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using CoreServiceAPIs.Common.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Controllers
{
    [Route("api/SEC/[action]/[controller]")]
    [ApiController]
    [Authorize]
    public class SecurityProfileController : ControllerBase
    {
        private readonly ISecurityProfileService _securityProfileService;

        private readonly IClientService _clientService;
        private readonly ILogger<ISecurityProfileService> _logger;

        public SecurityProfileController(ISecurityProfileService securityProfileService,
            IClientService clientService, ILogger<ISecurityProfileService> logger)
        {
            _securityProfileService = securityProfileService;
            _clientService = clientService;
            _logger = logger;
        }

        [HttpPost]
        [ActionName("SEC0702")]
        [Authorize(Roles = "SEC0702")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> Create([FromBody] ApiRequest<CreateSecProfileRequest> request)
        {
            try
            {
                string username = User.FindFirst(ClaimTypes.Name).Value;
                _logger.LogInformation($"CreateSecurityProfile --- Username: {username}");
                var response = await _securityProfileService.Create(request, username);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        [ActionName("SEC0703")]
        [Authorize(Roles = "SEC0703")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> Update(int id, [FromBody] ApiRequest<UpdateSecProfileRequest> request)
        {
            try
            {
                _logger.LogInformation($"UpdateSecurityProfile --- SecurityProfileId: {id}");
                string username = User.FindFirst(ClaimTypes.Name).Value;
                var response = await _securityProfileService.Update(id, username, request);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet]
        [ActionName("SEC0701")]
        [Authorize(Roles = "SEC0701")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> GetAll([FromQuery] ApiRequest<PaginationRequest> request)
        {
            try
            {
                _logger.LogInformation("GetAllSecurityProfile");
                if (request.Data == null)
                    request.Data = new PaginationRequest();
                var response = await _securityProfileService.GetAll(request);
                if (response.Data == null)
                    return NotFound();
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("list")]
        [ActionName("SEC0701")]
        [Authorize(Roles = "SEC0701")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<SecProfileMngDto>>> List([FromQuery] ApiRequest<string> request)
        {
            try
            {
                _logger.LogInformation("List");
                var response = new ApiResponse<SecProfileMngDto>();

                StdRequestParam param = request.Parameter;
                SecProfileMngDto data = new SecProfileMngDto();
                // TODO
                data.SecurityProfiles = await _securityProfileService.GetAllList(param.ClientId, param.EntityId);
                response.Data = data;
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("save")]
        [ActionName("SEC0703")]
        [Authorize(Roles = "SEC0703")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<bool>>> Save([FromBody] ApiRequest<CreateSecProfileRequest> request)
        {
            try
            {
                _logger.LogInformation("SaveSecurityProfile");
                var response = new ApiResponse<bool>();

                StdRequestParam param = request.Parameter;
                SecProfileMngDto data = new SecProfileMngDto();
                ClientResult clientInfo = await _clientService.GetClientEntityId(param.ClientId, param.EntityId);
                string username = User.FindFirst(ClaimTypes.Name).Value;

                response = await _securityProfileService.Save(request.Data, username);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("grouplist")]
        [ActionName("SEC0701")]
        [Authorize(Roles = "SEC0701")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<SecProfileGroupsDto>>> GetAllGroups([FromQuery] ApiRequest<int> request)
        {
            try
            {
                _logger.LogInformation("GroupList");
                var response = new ApiResponse<SecProfileGroupsDto>();

                StdRequestParam param = request.Parameter;
                SecProfileGroupsDto data = new SecProfileGroupsDto();
                data.SecurityProfileGroups = await _securityProfileService.GetAllGroups(request);
                response.Data = data;
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("{id}")]
        [ActionName("SEC0701")]
        [Authorize(Roles = "SEC0701")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> GetById(int id, [FromQuery] ApiRequest request)
        {
            try
            {
                _logger.LogInformation($"GetSecurityProfile --- BySecurityProfileId: {id}");
                var response = await _securityProfileService.GetById(id, request);
                if (response.Data == null)
                    return NotFound();
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpDelete("{id}")]
        [ActionName("SEC0704")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Delete(int id, [FromQuery] ApiRequest request)
        {
            try
            {
                _logger.LogInformation($"DeleteSecurityProfile --- SecurityProfileId: {id}");
                var response = await _securityProfileService.Delete(id, request);
                if (response.Data == null)
                    return BadRequest();
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}