﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using CoreServiceAPIs.Common.Constants;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreServiceAPIs.Authentication.Controllers
{
    [Route("api/PAY/FML/[action]/[controller]")]
    [ApiController]
    public class FormulasController : ControllerBase
    {
        private readonly IFormulasService _formulasService;

        public FormulasController(IFormulasService formulasService)
        {
            _formulasService = formulasService;
        }

        [HttpGet("formulasLevel")]
        [ActionName("PAY0301")]
        [Authorize(Roles = "PAY0301")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAll(int formulasLevel, [FromQuery] ApiRequest request)
        {
            var response = await _formulasService.GetAll(request, formulasLevel);
            return Ok(response);
        }


        [HttpGet("{formulasId}")]
        [ActionName("PAY0301")]
        [Authorize(Roles = "PAY0301")]
        public async Task<ActionResult<ApiResponse<FormulasResult>>> GetById(int formulasId, [FromQuery] ApiRequest request)
        {
            var response = await _formulasService.GetById(formulasId, request);
            return Ok(response);
        }

        [HttpPut("{formulasId}")]
        [ActionName("PAY0303")]
        [Authorize(Roles = "PAY0303")]
        public async Task<IActionResult> Update(int formulasId, [FromBody] ApiRequest<FormulasUpdateModel> request)
        {
            var resultCheck = new CommonLib.Validation.Validator(null).Validate(request.Data);
            if (resultCheck.Any())
            {
                return StatusCode(StatusCodes.Status400BadRequest, resultCheck);
            }

            var response = await _formulasService.Update(formulasId, request);
            return Ok(response);
        }

        [HttpPost()]
        [ActionName("PAY0302")]
        [Authorize(Roles = "PAY0302")]
        public async Task<IActionResult> Add([FromBody] ApiRequest<FormulasAddModel> request)
        {
            var resultCheck = new CommonLib.Validation.Validator(null).Validate(request.Data);
            if (resultCheck.Any())
            {
                return StatusCode(StatusCodes.Status400BadRequest, resultCheck);
            }

            var response = await _formulasService.Add(request);
            return Ok(response);
        }

        [HttpGet("FormulasTag")]
        [ActionName("PAY0301")]
        [Authorize(Roles = "PAY0301")]
        public async Task<IActionResult> GetAllFormulasTag([FromQuery] ApiRequest request)
        {
            var response = await _formulasService.GetAllFormulasTag(request);
            return Ok(response);
        }

    }
}
