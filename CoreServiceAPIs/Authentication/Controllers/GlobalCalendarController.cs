﻿using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Dtos.GlobalCalendar;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using CoreServiceAPIs.Common.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Controllers
{
    [Route("api/CFG/[action]/")]
    [ApiController]
    [Authorize]
    public class GlobalCalendarController : ControllerBase
    {
        private readonly IGlobalCalendarService _globalCalendarService;
        private readonly ILogger<GlobalCalendarController> _logger;

        public GlobalCalendarController(IGlobalCalendarService globalCalendarService, ILogger<GlobalCalendarController> logger)
        {
            _globalCalendarService = globalCalendarService;
            _logger = logger;
        }

        [HttpGet]
        [Authorize(Roles = "CFG0601")]
        [ActionName("CFG0601/WorkCalendar/GlobalCalendar")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Pagination<GlobalCalendarViewModel>>>> GetFilter([FromQuery] ApiRequest<PaginationRequest> request)
        {
            try
            {
                if (request.Data == null)
                    request.Data = new PaginationRequest();
                var result = await _globalCalendarService.GetFilter(request.Parameter.ClientId, request.Parameter.EntityId, request.Parameter.Locale, request.Ray, request.Data);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("{id}")]
        [Authorize(Roles = "CFG0601")]
        [ActionName("CFG0601/WorkCalendar/GlobalCalendar")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<GlobalCalendarViewModel>>> GetById(int id, [FromQuery] ApiRequest request)
        {
            try
            {
                var result = await _globalCalendarService.GetById(id, request.Ray);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "CFG0604")]
        [ActionName("CFG0604/WorkCalendar/GlobalCalendar")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<GlobalCalendarViewModel>>> DeleteGlobalCalendar([FromRoute] int id, [FromBody] ApiRequest request)
        {
            try
            {
                var result = await _globalCalendarService.DeleteGlobalCalendar(id, request.Ray);
                if (result.Data == null)
                    return BadRequest(result);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        [Authorize(Roles = "CFG0606")]
        [ActionName("CFG0606/WorkCalendar/GlobalCalendar")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<GlobalCalendarViewModel>>> Save([FromBody] ApiRequest<GlobalCalendarDto> request)
        {
            try
            {
                string username = User.FindFirst(ClaimTypes.Name).Value;
                var result = await _globalCalendarService.Save(username, request.Ray, request.Data);
                if (result.Data == null)
                    return BadRequest(result);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "CFG0604")]
        [ActionName("CFG0604/WorkCalendar/Holiday")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<bool?>>> DeleteHoliday([FromRoute] int id, [FromBody] ApiRequest request)
        {
            try
            {
                var result = await _globalCalendarService.DeleteHoliday(id, request.Ray);
                if (result.Data == null)
                    return BadRequest(result);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}