﻿using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using CoreServiceAPIs.Common.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Controllers
{
    [Route("api/CFG/[action]/[controller]")]
    [ApiController]
    [Authorize]
    public class GenericCodeController : ControllerBase
    {
        private readonly IGenericCodeService _genericCodeService;
        private readonly ILogger<GenericCodeController> _logger;

        public GenericCodeController(IGenericCodeService genericCodeService, ILogger<GenericCodeController> logger)
        {
            _genericCodeService = genericCodeService;
            _logger = logger;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param ResponseData="1">Get DropDown Generic Code</param>
        /// <param ResponseData="2">Get Components</param>
        /// <param ResponseData="3">Get Filter By Component</param>
        /// <param ResponseData="4">Get Generic Code from tree</param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("CFG0301")]
        [Authorize(Roles = "CFG0301")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> GetGenericCodes([FromQuery] ApiRequest<GetGenericCodeRequest> request)
        {
            try
            {
                if (request.Parameter == null)
                    request.Parameter = new StdRequestParam() { Locale = "en", GenericCodeList = new string[0] };
                else
                {
                    if (string.IsNullOrEmpty(request.Parameter.Locale))
                        request.Parameter.Locale = "en";
                    if (request.Parameter.GenericCodeList == null)
                        request.Parameter.GenericCodeList = new string[0];
                }
                if (request.Data.ResData == 1)
                {
                    var result = await _genericCodeService.GetDDLGenericCodes(request.Parameter.ClientId, request.Parameter.EntityId
                        , request.Parameter.Locale, request.Ray, request.Parameter.GenericCodeList.OfType<string>().ToList());
                    return Ok(result);
                }
                else if (request.Data.ResData == 2)
                {
                    var result = await _genericCodeService.GetModules(request.Parameter.ClientId, request.Parameter.EntityId, request.Ray, request.Parameter.Locale);
                    return Ok(result);
                }
                else if (request.Data.ResData == 3)
                {
                    if (request.Data == null)
                        request.Data = new GetGenericCodeRequest();
                    var result = await _genericCodeService.GetFilterByModule(request);
                    return Ok(result);
                }
                else if (request.Data.ResData == 4)
                {
                    var result = await _genericCodeService.GetTreeGenericCodes(request.Parameter.ClientId, request.Parameter.EntityId, request.Ray, request.Parameter.Locale);
                    return Ok(result);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("{id}")]
        [ActionName("CFG0301")]
        [Authorize(Roles = "CFG0301")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> GetById(int id, [FromQuery] ApiRequest request)
        {
            try
            {
                if (request.Parameter == null)
                    request.Parameter = new StdRequestParam();
                request.Parameter.Locale = !string.IsNullOrEmpty(request.Parameter.Locale) ? request.Parameter.Locale : "en";
                var result = await _genericCodeService.GetById(id, request);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        [ActionName("CFG0302")]
        [Authorize(Roles = "CFG0302")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> Create([FromBody] ApiRequest<CreateGenericCodeRequest> request)
        {
            try
            {
                string username = User.FindFirst(ClaimTypes.Name).Value;
                request.Data.CreatedBy = username;
                request.Data.CreatedDt = DateTime.Now;
                request.Data.ModifiedBy = username;
                request.Data.ModifiedDt = DateTime.Now;
                if (request.Parameter == null)
                    request.Parameter = new StdRequestParam();
                request.Parameter.Locale = !string.IsNullOrEmpty(request.Parameter.Locale) ? request.Parameter.Locale : "en";
                request.Data.Locale = request.Parameter.Locale;

                var response = await _genericCodeService.Create(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        [ActionName("CFG0303")]
        [Authorize(Roles = "CFG0303")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> Update(int id, [FromBody] ApiRequest<UpdateGenericCodeRequest> request)
        {
            try
            {
                request.Data.Id = id;
                string username = User.FindFirst(ClaimTypes.Name).Value;
                request.Data.ModifiedBy = username;
                request.Data.ModifiedDt = DateTime.Now;
                if (request.Parameter == null)
                    request.Parameter = new StdRequestParam();
                request.Parameter.Locale = !string.IsNullOrEmpty(request.Parameter.Locale) ? request.Parameter.Locale : "en";
                request.Data.Locale = request.Parameter.Locale;

                var response = await _genericCodeService.Update(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}