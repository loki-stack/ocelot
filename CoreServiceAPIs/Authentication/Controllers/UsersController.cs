﻿using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using CoreServiceAPIs.Common.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Controllers
{
    [ApiExplorerSettings(GroupName = "VN User")]
    [Route("api/SEC/[action]/[controller]")]
    [Authorize]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly ILogger<IUserService> _logger;
        private readonly IUserService _userService;

        public UsersController(IUserService userService, ILogger<IUserService> logger)
        {
            _userService = userService;
            _logger = logger;
        }

        [ActionName("SEC0101")]
        [HttpGet]
        [Authorize(Roles = "SEC0101")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAll([FromQuery] ApiRequest<PaginationRequest> request)
        {
            try
            {
                if (request.Data == null) request.Data = new PaginationRequest();
                var response = await _userService.GetAll(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }

        [ActionName("SEC0101")]
        [HttpGet("{id}")]
        [Authorize(Roles = "SEC0101")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetById(int id, [FromQuery] ApiRequest<int> request)
        {
            try
            {
                request.Data = id;
                var response = await _userService.GetById(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }

        [ActionName("SEC0102")]
        [HttpPost]
        [Authorize(Roles = "SEC0102")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Create([FromBody] ApiRequest<CreateUserRequest> request)
        {
            try
            {
                request.Data.CreatedBy = User.FindFirst(ClaimTypes.Name).Value;
                var response = await _userService.CreateUser(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }

        [ActionName("SEC0103")]
        [HttpPut("{id}")]
        [Authorize(Roles = "SEC0103")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Update(int id, [FromBody] ApiRequest<UpdateUserRequest> request)
        {
            try
            {
                request.Data.UserId = id;
                request.Data.ModifiedBy = User.FindFirst(ClaimTypes.Name).Value;
                var response = await _userService.UpdateUser(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }

        [HttpPut()]
        [Authorize(Roles = "SEC0103")]
        [ActionName("SEC0103/PortalAccess")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<int?>> UpdatePortalAccess([FromBody] ApiRequest<UserPortalAccessRequest> request)
        {
            try
            {
                string username = User.FindFirst(ClaimTypes.Name).Value;
                request.Data.ClientId = request.Parameter.ClientId;
                var response = await _userService.UpdatePortalAccess(request.Parameter.ClientId, request.Parameter.EntityId, username, request.Ray, request.Data);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("")]
        [Authorize(Roles = "SEC0101")]
        [ActionName("SEC0101/User/{employeeId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<UserViewModel>> GetUserByEmployeeId([FromRoute] int employeeId, [FromQuery] ApiRequest request)
        {
            try
            {
                var response = await _userService.GetUserByEmployeeId(request.Parameter.ClientId, request.Parameter.EntityId, employeeId, request.Ray);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        [Authorize(Roles = "SEC0102")]
        [ActionName("SEC0102/SendInvitation/{userId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> SendInvitation([FromRoute] int userId, [FromBody] ApiRequest<SendInvitationRequest> request)
        {
            try
            {
                string username = User.FindFirst(ClaimTypes.Name).Value;
                var response = await _userService.SendInvitation(request, userId, username);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}