﻿using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Controllers
{
    [ApiExplorerSettings(GroupName = "VN Authentication")]
    [Authorize]
    [Route("api/SEC/[action]/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly ILogger<AuthenticationController> _logger;
        private readonly IClientService _clientService;
        private readonly IJwtAuthenticationService _jwtAuthenticateionService;

        private readonly IUserService _userService;

        public AuthenticationController(IJwtAuthenticationService jwtAuthenticateionService, IClientService clientService, ILogger<AuthenticationController> logger,
         IUserService userService)
        {
            _jwtAuthenticateionService = jwtAuthenticateionService;
            _clientService = clientService;
            _logger = logger;
            this._userService = userService;
        }

        [AllowAnonymous]
        [ActionName("SEC0201")]
        [HttpPost(Name = "authenticate")]
        public async Task<IActionResult> Authenticate([FromBody] ApiRequest<AuthenticateRequest> request)
        {
            try
            {
                var response = await _jwtAuthenticateionService.Authenticate(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }

        [AllowAnonymous]
        [ActionName("SEC0202")]
        [HttpPost(Name = "refresh")]
        public async Task<IActionResult> Refresh([FromBody] ApiRequest<RefreshTokenRequest> request)
        {
            try
            {
                var response = await _jwtAuthenticateionService.RefreshToken(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }

        [AllowAnonymous]
        [ActionName("SEC5004")]
        [HttpPost(Name = "essauthenticate")]
        public async Task<IActionResult> EssAuthenticate([FromBody] ApiRequest<AuthenticateRequest> request)
        {
            try
            {
                //TODO: Change ClientCode, EntityCode to ClientId, EntityId
                //ClientResult clientInfo = await _clientService.GetClientEntityId(request.Parameter.ClientCode, request.Parameter.EntityCode);
                // ClientResult clientInfo = await _clientService.GetClientEntityId(request.Parameter.ClientId, request.Parameter.EntityId);
                // request.Parameter.ClientId = clientInfo.ClientId;
                // request.Parameter.EntityId = clientInfo.EntityId; (aw -  remove)
                var response = await _jwtAuthenticateionService.EssAuthenticate(request); //verify url is corresponding with userDetails
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return BadRequest();
            }
        }

        [ActionName("SEC5005")]
        [HttpPost(Name = "esschangepassword")]
        public async Task<IActionResult> EssChangePassword([FromBody] ApiRequest<ChangePasswordDto> request)
        {
            try
            {
                request.Data.IsAuthenticated = false;

                //authenticate
                var auth = new ApiRequest<AuthenticateRequest>();
                auth.Data = new AuthenticateRequest();
                auth.Data.Account = request.Data.Username;
                auth.Data.Password = request.Data.CurrentPassword;
                auth.Ray = request.Ray;
                var currentPassword = await _jwtAuthenticateionService.Authenticate(auth);
                if (currentPassword.Data != null)
                {
                    request.Data.IsAuthenticated = true;
                }

                //change password
                var response = await _userService.ChangePassword(request); //verify url is corresponding with userDetails
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return BadRequest();
            }
        }

        /// <summary>
        /// Allow user to mark forgot password. System should send an email to user to reset password
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [ActionName("SEC5003")]
        [HttpPost(Name = "forgotPassword")]
        public async Task<IActionResult> ForgotPassword([FromBody] ApiRequest<string> request)
        {
            try
            {
                //TODO: Change ClientCode, EntityCode to ClientId, EntityId
                //ClientResult clientInfo = await _clientService.GetClientEntityId(request.Parameter.ClientCode, request.Parameter.EntityCode);
                // ClientResult clientInfo = await _clientService.GetClientEntityId(request.Parameter.ClientId, request.Parameter.EntityId);
                // request.Parameter.ClientId = clientInfo.ClientId;
                // request.Parameter.EntityId = clientInfo.EntityId; (aw -  remove)
                // ApiRequest<AuthenticateRequest> verifyUrl = new ApiRequest<AuthenticateRequest>();
                // verifyUrl.Parameter = request.Parameter;
                // verifyUrl.Data = new AuthenticateRequest();
                // verifyUrl.Data.Account = request.Data;
                // var verificationValidation = await _jwtAuthenticateionService.EssAuthenticate(verifyUrl); //verify url is corresponding with userDetails
                // if(verificationValidation.Data == null)
                // {
                //     return Ok(verificationValidation);
                // }
                var response = await _userService.MarkReset(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return BadRequest();
            }
        }

        /// <summary>
        /// The link should come from email, if verified, let user to reset the password.
        /// Else show error.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [ActionName("SEC5001")]
        [HttpPost(Name = "verifyToken")]
        public async Task<IActionResult> VerifyToken([FromBody] ApiRequest<string> request)
        {
            try
            {
                var response = await _userService.VerifyToken(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return BadRequest();
            }
        }

        /// <summary>
        /// Reset password - required token, new password, confirm password
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [ActionName("SEC5103/reset")]
        [HttpPost(Name = "resetPassword")]
        public async Task<IActionResult> ResetPassword([FromBody] ApiRequest<ResetPasswordDto> request)
        {
            try
            {
                var response = await _userService.ResetPassword(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return BadRequest();
            }
        }

        /// <summary>
        /// Create password for user first login.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [ActionName("SEC5103/firstLogin")]
        [HttpPost(Name = "firstLogin")]
        public async Task<IActionResult> EssFirstLogin([FromBody] ApiRequest<ResetPasswordDto> request)
        {
            try
            {
                var response = await _userService.FirstLogin(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return BadRequest();
            }
        }


    }
}