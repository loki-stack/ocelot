﻿using CommonLib.Models;
using CommonLib.Models.Core;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace CoreServiceAPIs.Authentication.Repositories.Impl
{
    public class UserSecurityProfileRepository : IUserSecurityProfileRepository
    {
        private readonly CoreDbContext _context;
        private readonly ILogger<UserSecurityProfileRepository> _logger;

        public UserSecurityProfileRepository(CoreDbContext context, ILogger<UserSecurityProfileRepository> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<TbSecUserSecurityProfile> CreateUserSecurityProfile(int userId, int securityProfileId)
        {
            try
            {
                _logger.LogInformation($"CreateUserSecurityProfile --- UserId: {userId} , SecurityProfileId: {securityProfileId}");
                TbSecUserSecurityProfile profile = new TbSecUserSecurityProfile
                {
                    UserId = userId,
                    SecurityProfileId = securityProfileId
                };
                await _context.TbSecUserSecurityProfile.AddAsync(profile);
                await _context.SaveChangesAsync();
                return profile;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<bool> DeleteUserSecurityProfile(int userId, int securityProfileId)
        {
            try
            {
                _logger.LogInformation($"DeleteUserSecurityProfile --- UserId: {userId} , SecurityProfileId: {securityProfileId}");
                TbSecUserSecurityProfile profile = _context.TbSecUserSecurityProfile.Where(x => x.UserId == userId && x.SecurityProfileId == securityProfileId).First();
                _context.TbSecUserSecurityProfile.Remove(profile);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return false;
            }
        }

        public async Task<List<UserSecurityProfileViewModel>> GetAllUserSecurityProfile(int userId)
        {
            _logger.LogInformation($"GetAllUserSecurityProfile --- UserId: {userId}");
            List<UserSecurityProfileViewModel> userSecurityProfiles = new List<UserSecurityProfileViewModel>();
            var result = await (from u in _context.TbSecUser
                                join usp in _context.TbSecUserSecurityProfile
                                on u.UserId equals usp.UserId
                                join sp in _context.TbSecSecurityProfile
                                on usp.SecurityProfileId equals sp.SecurityProfileId
                                where u.UserId == userId
                                select new
                                {
                                    u.UserId,
                                    u.FullName,
                                    sp.SecurityProfileId,
                                    sp.SecurityProfileNameTxt,
                                    sp.SecurityProfileDescriptionTxt,
                                    usp.EffectiveStartDt,
                                    usp.EffectiveEndDt,
                                }).Distinct().ToListAsync();
            foreach (var item in result)
            {
                userSecurityProfiles.Add(new UserSecurityProfileViewModel
                {
                    UserId = item.UserId,
                    FullName = item.FullName,
                    SecurityProfileId = item.SecurityProfileId,
                    SecurityProfileName = item.SecurityProfileNameTxt,
                    SecurityProfileDescriptionTxt = item.SecurityProfileDescriptionTxt,
                    StartDt = item.EffectiveStartDt,
                    EndDt = item.EffectiveEndDt,
                });
            }
            return userSecurityProfiles;
        }

        public async Task<TbSecUserSecurityProfile> GetUserSecurityProfile(int userId, int securityProfileId)
        {
            _logger.LogInformation($"GetUserSecurityProfile --- UserId: {userId} , SecurityProfileId: {securityProfileId}");
            return await _context.TbSecUserSecurityProfile.Where(x => x.UserId == userId && x.SecurityProfileId == securityProfileId).FirstOrDefaultAsync();
        }

        public async Task<bool> Update(UserSecurityProfileDto request, string username, int userId)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    _logger.LogInformation($"UpdateUserSecurityProfile --- Username: {username} , UserId: {userId}");
                    var userSecurityProfilesGroup = await _context.TbSecUser.FirstOrDefaultAsync(x => x.UserId == userId);
                    //_mapper.Map<CreateDataAccessGroupRequest, TbSecDataAccessGroup>(request.Data, dataAccessGroup);
                    userSecurityProfilesGroup.ModifiedBy = username;
                    userSecurityProfilesGroup.ModifiedDt = DateTime.Now;
                    userSecurityProfilesGroup.FullName = request.FullName;
                    _logger.LogInformation("retrieved? {userSecurityProfilesGroup}", userSecurityProfilesGroup);
                    _context.TbSecUser.Update(userSecurityProfilesGroup);
                    await _context.SaveChangesAsync();

                    var secProfileList = new List<TbSecUserSecurityProfile>();
                    var spModels = _context.TbSecUserSecurityProfile.Where(x => x.UserId == userId);
                    _context.TbSecUserSecurityProfile.RemoveRange(spModels);
                    if (request.UserSecurityProfiles.Count > 0)
                    {
                        var spIds = request.UserSecurityProfiles.Distinct().ToList();
                        foreach (var sp in spIds)
                        {
                            TbSecUserSecurityProfile model = new TbSecUserSecurityProfile();
                            if (!String.IsNullOrEmpty(sp.EndDt))
                            {
                                model.EffectiveEndDt = DateTimeOffset.Parse(sp.EndDt);
                            }
                            model.EffectiveStartDt = DateTimeOffset.Parse(sp.StartDt);
                            model.UserId = userId;
                            model.SecurityProfileId = sp.SecurityProfileId;
                            secProfileList.Add(model);
                        }
                        await _context.TbSecUserSecurityProfile.AddRangeAsync(secProfileList);
                    }

                    await _context.SaveChangesAsync();
                    await transaction.CommitAsync();
                    return true;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                    await transaction.RollbackAsync();
                    return false;
                }
            }

        }
    }
}
