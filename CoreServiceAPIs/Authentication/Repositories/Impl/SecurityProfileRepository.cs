﻿using AutoMapper;
using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Common.Dtos;
using CoreServiceAPIs.Common.ShardingUtil;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Repositories.Impl
{
    public class SecurityProfileRepository : ISecurityProfileRepository
    {
        private readonly CoreDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<SecurityProfileRepository> _logger;

        public SecurityProfileRepository(CoreDbContext context, IMapper mapper, ILogger<SecurityProfileRepository> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public bool CheckSecurityProfileById(int id)
        {
            _logger.LogInformation($"CheckSecurityProfileById --- SecurityProfileId: {id}");
            return _context.TbSecSecurityProfile.Any(x => x.SecurityProfileId == id);
        }

        public async Task<int> Create(ApiRequest<CreateSecProfileRequest> request, string username)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    _logger.LogInformation($"CreateSecurityProfile --- Username: {username}");
                    var securityProfile = _mapper.Map<CreateSecProfileRequest, TbSecSecurityProfile>(request.Data);
                    securityProfile.CreateBy = username;
                    securityProfile.CreateDt = DateTime.Now;
                    securityProfile.ModifyBy = username;
                    securityProfile.ModifyDt = DateTime.Now;

                    await _context.AddAsync(securityProfile);
                    await _context.SaveChangesAsync();
                    await transaction.CommitAsync();
                    return securityProfile.SecurityProfileId;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                    await transaction.RollbackAsync();
                    return 0;
                }
            }
        }

        public async Task<bool> Delete(int id)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    _logger.LogInformation($"DeleteSecurityProfile --- SecurityProfileId: {id}");
                    var securityPofile = await _context.TbSecSecurityProfile.FindAsync(id);
                    _context.TbSecSecurityProfile.Remove(securityPofile);

                    var secProfileDetails = _context.TbSecSecurityProfileDetail.Where(x => x.SecurityProfileId == id);
                    _context.TbSecSecurityProfileDetail.RemoveRange(secProfileDetails);

                    await _context.SaveChangesAsync();
                    await transaction.CommitAsync();
                    return true;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                    await transaction.RollbackAsync();
                    return false;
                }
            }
        }

        public async Task<List<TbSecSecurityProfile>> GetAll(ApiRequest<PaginationRequest> request)
        {
            try
            {
                _logger.LogInformation("GetAll - SecurityProfile");
                GetObjectRequest objectRequest = new GetObjectRequest();
                if (!string.IsNullOrEmpty(request.Data.Filter))
                {
                    objectRequest = JsonConvert.DeserializeObject<GetObjectRequest>(request.Data.Filter);
                }
                var query = _context.TbSecSecurityProfile.AsQueryable();

                //search by condition
                if (objectRequest.CreatedDateFrom != null && objectRequest.CreatedDateTo != null)
                    query = query.Where(x => x.CreateDt.Date >= objectRequest.CreatedDateFrom.Value.Date && x.CreateDt.Date <= objectRequest.CreatedDateTo.Value.Date);
                if (!string.IsNullOrEmpty(request.Data.FullTextSearch))
                    query = query.Where(x => x.SecurityProfileNameTxt.Contains(request.Data.FullTextSearch) || x.SecurityProfileNameTxt.Contains(request.Data.FullTextSearch));

                //sort data
                if (!string.IsNullOrEmpty(request.Data.Sort))
                {
                    string sortCharacter = request.Data.Sort.Substring(0, 1);
                    string sortField = request.Data.Sort.Substring(1, request.Data.Sort.Length - 1);
                    if (sortField.ToLower().Equals("secprofilename"))
                    {
                        if (sortCharacter.Equals("-"))
                            query = query.OrderByDescending(x => x.SecurityProfileNameTxt);
                        else
                            query = query.OrderBy(x => x.SecurityProfileNameTxt);
                    }
                    if (sortField.ToLower().Equals("createddate"))
                    {
                        if (sortCharacter.Equals("-"))
                            query = query.OrderByDescending(x => x.CreateDt);
                        else
                            query = query.OrderBy(x => x.CreateDt);
                    }
                }

                var result = await query.ToListAsync();
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<TbSecSecurityProfile> GetById(int id)
        {
            try
            {
                _logger.LogInformation($"GetSecurityProfileBy --- SecurityProfileId: {id}");
                var result = await _context.TbSecSecurityProfile.FirstOrDefaultAsync(x => x.SecurityProfileId == id);
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<bool> Update(int securityId, string username, ApiRequest<UpdateSecProfileRequest> request)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    _logger.LogInformation($"UpdateSecurityProfile --- SecurityProfileId: {securityId} , Username: {username}");
                    //Update Security Profile
                    var secProfile = await _context.TbSecSecurityProfile.FirstOrDefaultAsync(x => x.SecurityProfileId == securityId);
                    _mapper.Map<UpdateSecProfileRequest, TbSecSecurityProfile>(request.Data, secProfile);
                    secProfile.ModifyBy = username;
                    secProfile.ModifyDt = DateTime.Now;

                    _context.TbSecSecurityProfile.Update(secProfile);
                    await _context.SaveChangesAsync();

                    //Update Security Profile Detail
                    var secProfileDetails = request.Data.Detail.Select(x => new { x.DataAccessGroupId, x.RoleGroupId }).Distinct().ToList();
                    var details = new List<TbSecSecurityProfileDetail>();
                    var data = _context.TbSecSecurityProfileDetail.Where(x => x.SecurityProfileId == securityId);
                    _context.TbSecSecurityProfileDetail.RemoveRange(data);
                    if (secProfileDetails.Count > 0)
                    {
                        foreach (var item in secProfileDetails)
                        {
                            TbSecSecurityProfileDetail model = new TbSecSecurityProfileDetail();
                            model.SecurityProfileId = securityId;
                            model.RoleGroupId = item.RoleGroupId;
                            model.DataAccessGroupId = item.DataAccessGroupId;

                            details.Add(model);
                        }
                        await _context.TbSecSecurityProfileDetail.AddRangeAsync(details);
                    }
                    await _context.SaveChangesAsync();
                    await transaction.CommitAsync();
                    return true;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                    await transaction.RollbackAsync();
                    return false;
                }
            }
        }

        public async Task<List<SecurityProfileViewModel>> GetAllList(int clientId, int entityId)
        {
            _logger.LogInformation($"GetAllList --- ClientId: {clientId} , EntityId: {entityId}");
            try
            {

                var q = (_context.TbSecRoleGroup
                                ).AsQueryable();

                var p = (_context.TbSecSecurityProfile
                                ).AsQueryable();

                var b = (_context.TbSecDataAccessGroup
                                ).AsQueryable();

                if (entityId > 0)
                {
                    q = q.Where(x => x.SpecificEntityId == entityId);
                    b = b.Where(x => x.SpecificCompanyId == entityId);
                    p = p.Where(x => x.SpecificEntityId == entityId);
                }

                if (clientId > 0)
                {
                    q = q.Where(x => x.SpecificClientId == clientId);
                    b = b.Where(x => x.SpecificClientId == clientId);
                    p = p.Where(x => x.SpecificClientId == clientId);
                }

                var far = await q
                         .Select(x => new RoleGroupViewModel
                         {
                             RoleGroupId = x.RoleGroupId,
                             RoleGroupNameTxt = x.RoleGroupNameTxt,
                             RoleGroupDescriptionTxt = x.RoleGroupDescriptionTxt,
                         }
                            )
                        .ToListAsync();
                // List<TbSecDataAccessGroup> dar = await (b).ToListAsync();
                var dar = (b
                                .Select(x => new DataAccessGroupViewModel
                                {
                                    DataAccessGroupId = x.DataAccessGroupId,
                                    DataAccessGroupNameTxt = x.DataAccessGroupNameTxt,
                                    DataAccessGroupDescriptionTxt = x.DataAccessGroupDescriptionTxt,
                                })
                                .ToList());

                var user = await _context.TbSecUserSecurityProfile
                        .Select(x => new UserSecProfileViewModel
                        {
                            UserId = x.UserId,
                            StartDt = x.EffectiveStartDt,
                            EndDt = x.EffectiveEndDt,
                        }
                        )
                        .ToListAsync();

                var result = await p
                            .Select(x => new SecurityProfileViewModel
                            {
                                SecurityProfileId = x.SecurityProfileId,
                                SecurityProfileNameTxt = x.SecurityProfileNameTxt,
                                SpecificClientId = x.SpecificClientId,
                                SpecificEntityId = x.SpecificEntityId,
                                SecurityProfileDescriptionTxt = x.SecurityProfileDescriptionTxt,
                                ModifyBy = x.ModifyBy,
                                ModifyDt = x.ModifyDt,
                                LastModified = Utils.TimeAgo(x.ModifyDt.DateTime),
                                FAR = far,
                                DAR = dar,
                                UserSecurityProfile = user,
                            }
                            )
                            .ToListAsync();

                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<List<SecurityProfileMngViewModel>> GetAllGroups(int securityProfileId)
        {
            try
            {
                _logger.LogInformation($"GetAllSecurityProfile --- SecurityProfileId: {securityProfileId}");
                var result = await _context.TbSecSecurityProfileDetail
                            .Where(x => x.SecurityProfileId == securityProfileId)
                            .Select(x => new SecurityProfileMngViewModel
                            {
                                roleGroupId = x.RoleGroupId,
                                DataAccessGroupId = x.DataAccessGroupId,
                                RoleGroupNameTxt = _context.TbSecRoleGroup
                                        .Where(y => y.RoleGroupId == x.RoleGroupId)
                                        .Select(m => m.RoleGroupNameTxt).FirstOrDefault(),
                                DataAccessGroupNameTxt = _context.TbSecDataAccessGroup
                                        .Where(y => y.DataAccessGroupId == x.DataAccessGroupId)
                                        .Select(m => m.DataAccessGroupNameTxt).FirstOrDefault(),
                                RoleGroupDescriptionTxt = _context.TbSecRoleGroup
                                        .Where(y => y.RoleGroupId == x.RoleGroupId)
                                        .Select(m => m.RoleGroupDescriptionTxt).FirstOrDefault(),
                                DataAccessDescriptionTxt = _context.TbSecDataAccessGroup
                                        .Where(y => y.DataAccessGroupId == x.DataAccessGroupId)
                                        .Select(m => m.DataAccessGroupDescriptionTxt).FirstOrDefault(),
                                SecurityProfileId = x.SecurityProfileId,
                            }).ToListAsync();

                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<bool> CheckDataAccessIdIsExist(int DataAccessGroupId)
        {
            _logger.LogInformation($"CheckDataAccessIdIsExist --- DataAccessGroupId: {DataAccessGroupId}");
            return await _context.TbSecDataAccessGroup.Where(x => x.DataAccessGroupId == DataAccessGroupId).AnyAsync();
        }
        public async Task<bool> CheckRoleGroupIdIsExist(int RoleGroupId)
        {
            _logger.LogInformation($"CheckRoleGroupIdIsExist --- RoleGroupId: {RoleGroupId}");
            return await _context.TbSecRoleGroup.Where(x => x.RoleGroupId == RoleGroupId).AnyAsync();
        }

        public async Task<bool> CheckSecurityProfileIsExist(int SecurityProfileId)
        {
            _logger.LogInformation($"CheckSecurityProfileIsExist --- SecurityProfileId: {SecurityProfileId}");
            return await _context.TbSecSecurityProfile.Where(x => x.SecurityProfileId == SecurityProfileId).AnyAsync();
        }

        public async Task<bool> Save(CreateSecProfileRequest request, string username)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    _logger.LogInformation($"SaveSecurityProfile --- Username: {username}");
                    var securityProfile = await _context.TbSecSecurityProfile.FirstOrDefaultAsync(x => x.SecurityProfileId == request.SecurityProfileId);
                    securityProfile.ModifyBy = username;
                    securityProfile.ModifyDt = DateTime.Now;
                    securityProfile.SecurityProfileDescriptionTxt = request.SecurityProfileDescriptionTxt;
                    securityProfile.SecurityProfileNameTxt = request.SecurityProfileNameTxt;
                    _logger.LogInformation("Save mode retrieved? {securityProfile}", securityProfile);
                    _context.TbSecSecurityProfile.Update(securityProfile);
                    await _context.SaveChangesAsync();

                    //update TbSecSecurityProfileDetails
                    var profileDetail = new List<TbSecSecurityProfileDetail>();
                    var pdModels = _context.TbSecSecurityProfileDetail.Where(x => x.SecurityProfileId == request.SecurityProfileId);
                    _context.TbSecSecurityProfileDetail.RemoveRange(pdModels);
                    if (request.ProfileDetails.Count > 0)
                    {
                        var pdIds = request.ProfileDetails.Distinct().ToList();
                        foreach (var pdId in pdIds)
                        {
                            TbSecSecurityProfileDetail model = new TbSecSecurityProfileDetail();
                            model.SecurityProfileId = pdId.SecurityProfileId;
                            model.DataAccessGroupId = pdId.DataAccessGroupId;
                            model.RoleGroupId = pdId.RoleGroupId;
                            profileDetail.Add(model);
                        }
                        await _context.TbSecSecurityProfileDetail.AddRangeAsync(profileDetail);
                    }

                    await _context.SaveChangesAsync();
                    await transaction.CommitAsync();

                    return true;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                    return false;
                }
            }
        }
        public async Task<List<SecurityProfileViewModel>> GetUserSecurityProfile(int userId, int clientId, int entityId)
        {
            // var q = (from u in _context.TbSecUser
            //          join usp in _context.TbSecUserSecurityProfile
            //          on u.UserId equals usp.UserId
            //          join sp in _context.TbSecSecurityProfile
            //          on usp.SecurityProfileId equals sp.SecurityProfileId
            //          where u.UserId == userId
            //          select new
            //          {
            //              u.ClientId,
            //              u.EntityId,
            //              u.UserId,
            //              u.FullName,
            //              sp.SecurityProfileId,
            //              sp.SecurityProfileNameTxt,
            //              sp.SecurityProfileDescriptionTxt,
            //              usp.EffectiveStartDt,
            //              usp.EffectiveEndDt,
            //          }).AsQueryable();
            _logger.LogInformation($"GetUserSecurityProfile --- UserId: {userId} , ClientId: {clientId} , EntityId: {entityId}");
            var p = (from u in _context.TbSecSecurityProfile select u).AsQueryable();

            if (clientId > 0)
            {
                // q = q.Where(x => x.ClientId == clientId);
                p = p.Where(x => x.SpecificClientId == clientId);
            }

            if (entityId > 0)
            {
                // q = q.Where(x => x.EntityId == entityId);
                p = p.Where(x => x.SpecificEntityId == entityId);
            }
            // var userSp = await (q).Distinct().ToListAsync();
            var spList = await (p).Distinct().ToListAsync();
            var res = (from sp in spList
                           //    join user in userSp
                           //    on sp.SecurityProfileId equals user.SecurityProfileId into usp
                           //    from j in usp.DefaultIfEmpty()
                           //    where j == null
                       select sp).ToList();
            List<SecurityProfileViewModel> result = (res
                                                        .Select(x => new SecurityProfileViewModel
                                                        {
                                                            SecurityProfileId = x.SecurityProfileId,
                                                            SecurityProfileNameTxt = x.SecurityProfileNameTxt,
                                                            SecurityProfileDescriptionTxt = x.SecurityProfileDescriptionTxt,
                                                        })
                                                        .ToList());
            return result;
        }
    }

}