﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Authentication.Dtos;
using CommonLib.Services;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using CommonLib.Models;
using CommonLib.Dtos;
using CommonLib.Models.Core;

namespace CoreServiceAPIs.Authentication.Repositories.Impl
{
    public class FormulasRepository : IFormulasRepository
    {
        private readonly CoreDbContext _context;
        public FormulasRepository(CoreDbContext context)
        {
            _context = context;
        }

        public async Task<int> Add(FormulasAddModel request, int clientId, int entityId)
        {
            TbPayFormulas model = new TbPayFormulas
            {
                Name = request.Name,
                Description = request.Description,
                Level = request.Level,
                StatusCd = request.StatusCd,
                CreatedDt = DateTimeOffset.Now,
                ModifiedDt = DateTimeOffset.Now,
                Formulas = request.Formulas,
                EditAble = request.EditAble,
                ClientId = clientId,
                EntityId = entityId
            };
            await _context.AddAsync(model);
            await _context.SaveChangesAsync();
            return model.FormulasId;
        }

        /// <summary>
        /// Add new Formulas Tag
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<int> AddFormulasTag(FormulasTagAddModel request)
        {
            TbPayFormulasTag model = new TbPayFormulasTag
            {
                Flag = request.Flag,
                TagName = request.TagName,
                Remark = request.Remark,
                FormulasId = request.FormulasId,
                FormulasRepeat = request.FormulasRepeat
            };
            await _context.AddAsync(model);
            await _context.SaveChangesAsync();
            return model.FormulasTagId;
        }

        /// <summary>
        /// Get All Formulas
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public async Task<List<FormulasResult>> GetAll(int level, int clientId, int entityId)
        {
            var clientIdPar = new SqlParameter("@clientId", clientId);
            var entityIdPar = new SqlParameter("@entityId", entityId);
            return await _context.Set<FormulasResult>().FromSqlRaw("EXECUTE GET_FORMULAS @clientId, @entityId", clientIdPar, entityIdPar).ToListAsync();
        }

        /// <summary>
        /// Get Formulas by formulasId
        /// </summary>
        /// <param name="formulasId"></param>
        /// <returns></returns>
        public async Task<FormulasResult> GetById(int formulasId, int clientId, int entityId)
        {
            return await _context.TbPayFormulas.Where(n => n.FormulasId == formulasId && n.ClientId == clientId && n.EntityId == entityId).Select(n => new FormulasResult()
                                                              {
                                                                  FormulasId = n.FormulasId,
                                                                  Name = n.Name,
                                                                  Description = n.Description,
                                                                  Level = n.Level,
                                                                  StatusCd = n.StatusCd,
                                                                  Formulas = n.Formulas,
                                                                  EditAble = n.EditAble,
                                                                  CreatedDt = n.CreatedDt,
                                                                  ModifiedDt = n.ModifiedDt,
                                                                  ClientId = n.ClientId.Value, //TODO: Thao
                                                                  EntityId = n.EntityId.Value  //TODO: Thao
                                                              }).FirstOrDefaultAsync();
        }

        public async Task<FormulasResult> GetById(int formulasId)
        {
            return await _context.TbPayFormulas.Where(n => n.FormulasId == formulasId).Select(n => new FormulasResult()
            {
                FormulasId = n.FormulasId,
                Name = n.Name,
                Description = n.Description,
                Level = n.Level,
                StatusCd = n.StatusCd,
                Formulas = n.Formulas,
                EditAble = n.EditAble,
                CreatedDt = n.CreatedDt,
                ModifiedDt = n.ModifiedDt,
                ClientId = n.ClientId.Value, //TODO: Thao
                EntityId = n.EntityId.Value //TODO: Thao
            }).FirstOrDefaultAsync();
        }

        public async Task<List<FormulasTagViewResult>> GetFormulasTag()
        {
            return await _context.Set<FormulasTagViewResult>().FromSqlRaw("EXECUTE GET_FORMULAS_TAG").ToListAsync();
        }

        public async Task<FormulasTagViewResult> GetFormulasTagById(int formulasTagId)
        {
            return await _context.TbPayFormulasTag.Where(n => n.FormulasTagId == formulasTagId).Select(
                                                            n => new FormulasTagViewResult()
                                                            {
                                                                FormulasTagId = n.FormulasTagId,
                                                                Flag = n.Flag,
                                                                TagName = n.TagName,
                                                                Remark = n.Remark,
                                                                FieldName = n.FieldName,
                                                                Predefined = n.Predefined,
                                                                FormulasId = n.FormulasId,
                                                                ValueType = n.ValueType,
                                                                CreatedDt = n.CreatedDt,
                                                                ModifiedDt = n.ModifiedDt
                                                            }    
                                                            ).FirstOrDefaultAsync();
        }

        public async Task<FormulasTagViewResult> GetFormulasTagFromFormulasId(int formulasId)
        {
            return await _context.TbPayFormulasTag.Where(n => n.FormulasRepeat == formulasId).Select(
                                                            n => new FormulasTagViewResult()
                                                            {
                                                                TagName = n.TagName,
                                                                Flag = n.Flag,
                                                                Remark = n.Remark,
                                                                FormulasTagId = n.FormulasTagId
                                                            }
                                                            ).FirstOrDefaultAsync();
        }

        public async Task<int?> Update(int formulasId, FormulasUpdateModel request)
        {
            TbPayFormulas model = new TbPayFormulas();
            model = await _context.TbPayFormulas.FindAsync(formulasId);

            if (model != null)
            {
                model.Name = request.Name;
                model.Description = request.Description;
                model.Level = request.Level;
                model.StatusCd = request.StatusCd;
                model.Formulas = request.Formulas;
                model.ModifiedDt = DateTimeOffset.Now;
            }
            else
            {
                return null;
            }

            _context.Entry(model).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return formulasId;
        }

        /// <summary>
        /// Update Formulas Tag
        /// </summary>
        /// <param name="formulasTagId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<int?> UpdateFormulasTag(int formulasTagId, FormulasTagAddModel request)
        {
            TbPayFormulasTag model = new TbPayFormulasTag();
            model = await _context.TbPayFormulasTag.FindAsync(formulasTagId);

            if (model != null)
            {
                model.Flag = request.Flag;
                model.TagName = request.TagName;
                model.Remark = request.Remark;
                model.FormulasId = request.FormulasId;
                model.ModifiedDt = DateTimeOffset.Now;
                model.FormulasRepeat = request.FormulasRepeat;
            }
            else
            {
                return null;
            }

            _context.Entry(model).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return formulasTagId;
        }
    }
}
