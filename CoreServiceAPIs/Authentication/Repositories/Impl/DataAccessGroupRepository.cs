using AutoMapper;
using CommonLib.Dtos;
using CommonLib.Models.Core;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Common.Dtos;
using CoreServiceAPIs.Common.ShardingUtil;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Repositories.Impl
{
    /// <summary>
    /// DataAccessGroupRepository Class
    /// </summary>
    /// <returns></returns>
    public class DataAccessGroupRepository : IDataAccessGroupRepository
    {
        private readonly CoreDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<DataAccessGroupRepository> _logger;

        /// <summary>
        /// DAR Constructor
        /// </summary>
        /// <param name="context">context</param>
        /// <param name="mapper">mapper</param>
        /// <param name="logger">logger</param>
        /// <returns></returns>
        public DataAccessGroupRepository(CoreDbContext context, IMapper mapper, ILogger<DataAccessGroupRepository> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }
        /// <summary>
        /// Get List of DAR by client code and/or entity code
        /// </summary>
        /// <param name="clientId">clientId</param>
        /// <param name="entityId">entityId</param>
        /// <returns>DataAccessGroupViewModel</returns>
        public async Task<List<DataAccessGroupViewModel>> ListAll(int clientId, int entityId)
        {
            _logger.LogInformation($"ListAll -- clientId - {clientId} &  entityId - {entityId}");
            try
            {
                var q = (_context.TbSecDataAccessGroup
                                .Include(x => x.TbSecSecurityProfileDetail)
                                .Include(x => x.TbSecDataAccessGrade)
                                .Include(x => x.TbSecDataAccessRemunerationPackage)
                                ).AsQueryable();

                if (clientId > 0)
                {
                    q = q.Where(x => x.SpecificClientId == clientId);
                }

                if (entityId > 0)
                {
                    q = q.Where(x => x.SpecificCompanyId == entityId);
                }

                var result = await q
                            .Select(x => new DataAccessGroupViewModel
                            {
                                DataAccessGroupId = x.DataAccessGroupId,
                                SpecificClientId = x.SpecificClientId,
                                SpecificEntityId = x.SpecificCompanyId,
                                DataAccessGroupNameTxt = x.DataAccessGroupNameTxt,
                                DataAccessGroupDescriptionTxt = x.DataAccessGroupDescriptionTxt,
                                ModifyBy = x.ModifyBy,
                                ModifyDt = x.ModifyDt,
                                OrganizationStructure = entityId > 0 ?
                                                        _context.TbSecDataAccessOrganizationStructure
                                                        .Where(e => e.DataAccessGroupId == x.DataAccessGroupId)
                                                        .Select(e => new DataAccessOrganizationStructureViewModel
                                                        {
                                                            DataAccessGroupId = e.DataAccessGroupId,
                                                            NodeLevel = e.NodeLevel.ToString(),
                                                        })
                                                        .ToList() :
                                                        new List<DataAccessOrganizationStructureViewModel>(),
                                Grade = x.TbSecDataAccessGrade.Select(y => new DataAccessGradeViewModel
                                {
                                    MinGradeLevelNum = y.MinGradeLevelNum,
                                    MaxGradeLevelNum = y.MaxGradeLevelNum,
                                }).ToList(),
                                LastModified = Utils.TimeAgo(x.ModifyDt.DateTime),
                                SecurityProfileMappingCount = x.TbSecSecurityProfileDetail
                                                                    .Select(m => new { m.DataAccessGroupId, m.SecurityProfileId })
                                                                    .Distinct()
                                                                    .Count(),
                            }).ToListAsync();

                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Quick Create new DAR
        /// </summary>
        /// <param name="request">request</param>
        /// <param name="username">username</param>
        /// <returns>int</returns>
        public async Task<int> QuickCreate(ApiRequest<CreateDataAccessGroupRequest> request, string username)
        {
            _logger.LogInformation($"QuickCreate -- Description - {request.Data.DataAccessGroupDescriptionTxt} & Name -{request.Data.DataAccessGroupNameTxt}");
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var dataAccessGroup = _mapper.Map<CreateDataAccessGroupRequest, TbSecDataAccessGroup>(request.Data);
                    dataAccessGroup.CreateBy = username;
                    dataAccessGroup.CreateDt = DateTime.Now;
                    dataAccessGroup.ModifyBy = username;
                    dataAccessGroup.ModifyDt = DateTime.Now;

                    await _context.AddAsync(dataAccessGroup);
                    await _context.SaveChangesAsync();
                    await transaction.CommitAsync();
                    return dataAccessGroup.DataAccessGroupId;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                    await transaction.RollbackAsync();
                    return 0;
                }
            }
        }
        private async Task<List<DataAccessGradeViewModel>> GetGrade(int dataAccessId)
        {
            _logger.LogInformation($"GetGrade -- dataAccessId - {dataAccessId}");
            List<DataAccessGradeViewModel> grade = await _context.TbSecDataAccessGrade
                                                    .Where(n => n.DataAccessGroupId == dataAccessId)
                                                    .Select(y => new DataAccessGradeViewModel
                                                    {
                                                        DataAccessGroupId = y.DataAccessGroupId,
                                                        DataAccessGradeId = y.DataAccessGradeId,
                                                        MinGradeLevelNum = y.MinGradeLevelNum,
                                                        MaxGradeLevelNum = y.MaxGradeLevelNum
                                                    }).ToListAsync();
            return grade;
        }

        /// <summary>
        /// Get list of clientEntities in tree format
        /// </summary>
        /// <param name="clientId">clientId</param>
        /// <param name="entityId">entityId</param>
        /// <param name="dataAccessGroupId">dataAccessGroupId</param>
        /// <returns>DataAccessGroupClientEntityDto</returns>
        public async Task<List<TreeNodeDto<DarClientEntityDto>>> GetTree(int dataAccessGroupId, int clientId, int entityId)
        {
            _logger.LogInformation($"GetTree -- dataAccessGroupId - {dataAccessGroupId} & clientId - {clientId} & entityId - {entityId}");
            var q = (from g in _context.TbCamClient
                     join i in _context.TbCamEntity on g.ClientId equals i.ParentClientId into parent
                     from p in parent.DefaultIfEmpty()
                     select p).AsQueryable();

            if (clientId > 0)
            {
                q = q.Where(x => x.ParentClient.ClientId == clientId);
            }

            if (entityId > 0)
            {
                q = q.Where(x => x.EntityId == entityId);
            }

            var parents = (await q
                .Select(x => new { x.ParentClient.ClientId, x.ParentClient.ClientCode, x.ParentClient.ClientNameTxt, x.ParentClient.ClientLevel, })
                .Where(x => x.ClientLevel != null && x.ClientLevel.GetLevel() > 0)
                .ToListAsync())
                .ToHashSet();

            var child = (await q
                .Where(x => x.EntityLevel != null && x.EntityLevel.GetLevel() > 0)
                .Select(x => new DarClientEntityDto
                {
                    ItemId = x.EntityId,
                    DataAccessGroupId = dataAccessGroupId,
                    ItemType = "Entity",
                    ItemLevel = x.EntityLevel.ToString(),
                    ItemCode = x.EntityCode,
                    ItemText = x.EntityNameTxt,
                    IsSelected = _context.TbSecDataAccessClientEntity
                                        .Where(p =>
                                            p.DataAccessGroupId == dataAccessGroupId &&
                                            p.ClientLevel == x.ParentClient.ClientLevel &&
                                            p.EntityLevel == x.EntityLevel)
                                        .Any(),
                }).ToListAsync());

            var root = parents.Select(x => new TreeNodeDto<DarClientEntityDto>
            {
                Key = x.ClientCode,
                Data = new DarClientEntityDto
                {
                    ItemId = x.ClientId,
                    ItemType = "Client",
                    ItemCode = x.ClientCode,
                    ItemLevel = x.ClientLevel.ToString(),
                    ItemText = x.ClientNameTxt,
                    IsSelected = _context.TbSecDataAccessClientEntity
                                    .Where(p =>
                                        p.DataAccessGroupId == dataAccessGroupId &&
                                        p.ClientLevel == p.EntityLevel &&
                                        (p.ClientLevel == x.ClientLevel || p.ClientLevel.GetLevel() == 0))
                                    .Any(),
                },
                Children = (x.ClientLevel.GetLevel() > 0 ? GetTreeMethod(x.ClientLevel, dataAccessGroupId, child) : null),
            }).ToList();

            return root;

        }

        private List<TreeNodeDto<DarClientEntityDto>> GetTreeMethod(HierarchyId node, int dataAccessGroupId, List<DarClientEntityDto> lstCollection)
        {
            _logger.LogInformation($"GetTreeMethod -- node - {node.ToString()} & dataAccessGroupId - {dataAccessGroupId}");
            List<TreeNodeDto<DarClientEntityDto>> lst = new List<TreeNodeDto<DarClientEntityDto>>();
            try
            {
                var lastItemInCurrentLevel = GetChilds(node, dataAccessGroupId, lstCollection);

                foreach (var item in lastItemInCurrentLevel)
                {
                    TreeNodeDto<DarClientEntityDto> tr = new TreeNodeDto<DarClientEntityDto>
                    {
                        Key = item.Key,
                        Data = item.Data,

                    };
                    tr.Children = GetTreeMethod(HierarchyId.Parse(item.Data.ItemLevel), dataAccessGroupId, lstCollection);
                    lst.Add(tr);
                }

                return lst;

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        private List<TreeNodeDto<DarClientEntityDto>> GetChilds(HierarchyId node, int dataAccessId, List<DarClientEntityDto> lstCollection)
        {
            _logger.LogInformation($"GetChilds -- dataAccessId - {dataAccessId}");
            try
            {
                List<TreeNodeDto<DarClientEntityDto>> child = lstCollection
                                                        .Where(x => HierarchyId.Parse(x.ItemLevel).GetAncestor(1) == node)
                                                        .Select(q => new TreeNodeDto<DarClientEntityDto>
                                                        {
                                                            Key = q.ItemId.ToString(),
                                                            Data = new DarClientEntityDto
                                                            {
                                                                ItemId = q.ItemId,
                                                                ItemType = q.ItemType,
                                                                ItemCode = q.ItemCode,
                                                                IsSelected = q.IsSelected,
                                                                ItemText = q.ItemText,
                                                                DataAccessGroupId = q.DataAccessGroupId,
                                                                ItemLevel = q.ItemLevel
                                                            }
                                                        }).ToList();
                return child;

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Get Data Access Details By it's ID
        /// </summary>
        /// <param name="id">CreateDataAccessGroupRequest</param>
        /// <param name="username">string</param>
        /// <param name="clientInfo">ClientResult</param>
        /// <returns>DataAccessGroupViewModel</returns>
        public async Task<DataAccessGroupViewModel> GetByDataAccessId(int id, string username, ClientResult clientInfo)
        {
            _logger.LogInformation($"GetByDataAccessId -- dataAccessId - {id}");
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var q = (from g in _context.TbSecDataAccessClientEntity
                             select g).AsQueryable();

                    if (clientInfo.ClientId > 0)
                    {
                        q = q.Where(x => x.ClientLevel == HierarchyId.Parse(clientInfo.ClientLevel));
                    }

                    if (clientInfo.EntityId > 0)
                    {
                        q = q.Where(x => x.EntityLevel == HierarchyId.Parse(clientInfo.EntityLevel));
                    }

                    List<DataAccessGradeViewModel> grade = await GetGrade(id);

                    var result = await _context.TbSecDataAccessGroup
                            .Include(x => x.TbSecDataAccessClientEntity)
                            .Include(x => x.TbSecDataAccessGrade)
                            .Include(x => x.TbSecDataAccessRemunerationPackage)
                            .Where(x => x.DataAccessGroupId == id)
                            .Select(x => new DataAccessGroupViewModel
                            {
                                DataAccessGroupId = x.DataAccessGroupId,
                                SpecificClientId = x.SpecificClientId,
                                SpecificEntityId = x.SpecificCompanyId,
                                DataAccessGroupNameTxt = x.DataAccessGroupNameTxt,
                                //DataAccessGroupDescriptionTxt = x.DataAccessGroupDescriptionTxt,
                                Grade = grade,
                                Entities = q
                                            .Where(p => p.DataAccessGroupId == id)
                                            .Select(p => new DataAccessGroupClientEntityDto
                                            {
                                                DataAccessClientEntityId = p.DataAccessClientEntityId,
                                                DataAccessGroupId = p.DataAccessGroupId,
                                                ClientId = x.SpecificClientId,
                                                ClientLevel = p.ClientLevel.ToString(),
                                                ClientCode = clientInfo.ClientCode,
                                                EntityLevel = p.EntityLevel.ToString(),
                                            }).ToList(),
                                RemunerationPackageId = x.TbSecDataAccessRemunerationPackage.Select(p => p.RemunerationPackageId).ToList(),
                            }).FirstOrDefaultAsync();
                    return result;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                    return null;
                }
            }

        }

        /// <summary>
        /// Return true if Data Access Group 's ID is exist
        /// </summary>id
        /// <param name="id">int</param>
        /// <returns>bool</returns>
        public bool CheckDataAccessGroupById(int id)
        {
            _logger.LogInformation($"CheckDataAccessGroupById -- DataAccessGroupId - {id}");
            return _context.TbSecDataAccessGroup.Any(x => x.DataAccessGroupId == id);
        }
        private List<DataAccessOrganizationStructureViewModel> UpdateAllDescendantChecked(List<DataAccessOrganizationStructureViewModel> organizationStructureList)
        {
            _logger.LogInformation($"UpdateAllDescendantChecked");
            List<DataAccessOrganizationStructureViewModel> orgStrucs = new List<DataAccessOrganizationStructureViewModel>();
            try
            {
                foreach (var list in organizationStructureList)
                {
                    DataAccessOrganizationStructureViewModel model = new DataAccessOrganizationStructureViewModel();
                    int countSelectedDescendant = 0;
                    countSelectedDescendant = organizationStructureList
                                                .Where(x => HierarchyId.Parse(x.NodeLevel).GetLevel() >= (HierarchyId.Parse(list.NodeLevel)).GetLevel() && HierarchyId.Parse(x.NodeLevel).IsDescendantOf(HierarchyId.Parse(list.NodeLevel)))
                                                .Count();
                    model.IsSelectAllDescendants = (countSelectedDescendant == list.DescendantCount && list.DescendantCount > 1);
                    model.NodeLevel = list.NodeLevel;
                    model.DataAccessGroupId = list.DataAccessGroupId;
                    var temp = organizationStructureList.Where(x => HierarchyId.Parse(x.NodeLevel).IsDescendantOf(HierarchyId.Parse(list.NodeLevel)));
                    if ((HierarchyId.Parse(list.NodeLevel)).GetLevel() == 0 && organizationStructureList.Count == 1)
                    {
                        orgStrucs.Add(list);
                        continue;
                    }
                    orgStrucs.Add(model);
                }

                return orgStrucs;

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Update Data Access Group By it's ID
        /// </summary>
        /// <param name="request">UpdateDataAccessGroupRequest</param>
        /// <param name="username">string</param>
        /// <param name="clientId">clientId</param>
        /// <param name="entityId">entityId</param>
        /// <returns>bool</returns>
        public async Task<bool> Update(ApiRequest<UpdateDataAccessGroupRequest> request, string username, int clientId, int entityId)
        {
            _logger.LogInformation($"Update -- Description - {request.Data.DataAccessGroupDescriptionTxt} & Name - {request.Data.DataAccessGroupNameTxt}");
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var dataAccessGroup = await _context.TbSecDataAccessGroup.FirstOrDefaultAsync(x => x.DataAccessGroupId == request.Data.DataAccessGroupId);
                    dataAccessGroup.ModifyBy = username;
                    dataAccessGroup.ModifyDt = DateTime.Now;
                    dataAccessGroup.DataAccessGroupNameTxt = request.Data.DataAccessGroupNameTxt;
                    _logger.LogInformation($"retrieved - DataAccessGroupId - {dataAccessGroup.DataAccessGroupId}");
                    _context.TbSecDataAccessGroup.Update(dataAccessGroup);
                    await _context.SaveChangesAsync();

                    //update organization structure
                    var organizationStructure = new List<TbSecDataAccessOrganizationStructure>();
                    var OrgModels = _context.TbSecDataAccessOrganizationStructure.Where(x => x.DataAccessGroupId == dataAccessGroup.DataAccessGroupId);
                    _context.TbSecDataAccessOrganizationStructure.RemoveRange(OrgModels);
                    if (request.Data.OrganizationStructure.Count > 0)
                    {
                        var orgIds = UpdateAllDescendantChecked(request.Data.OrganizationStructure.Distinct().ToList());
                        _logger.LogInformation($"Add Organization Structure");

                        foreach (var orgId in orgIds)
                        {
                            TbSecDataAccessOrganizationStructure model = new TbSecDataAccessOrganizationStructure();
                            model.NodeLevel = HierarchyId.Parse(orgId.NodeLevel);
                            model.DataAccessGroupId = orgId.DataAccessGroupId;
                            model.IsSelectAllDescendants = orgId.IsSelectAllDescendants;
                            var IsDescendant = orgIds
                                                .Where(x => HierarchyId.Parse(orgId.NodeLevel).GetLevel() > HierarchyId.Parse(x.NodeLevel).GetLevel() && HierarchyId.Parse(orgId.NodeLevel).IsDescendantOf(HierarchyId.Parse(x.NodeLevel)));

                            if (IsDescendant.Any(x => x.IsSelectAllDescendants) && HierarchyId.Parse(orgId.NodeLevel).GetLevel() > 0)
                            {
                                continue;
                            }
                            organizationStructure.Add(model);
                        }
                        await _context.TbSecDataAccessOrganizationStructure.AddRangeAsync(organizationStructure);
                    }

                    //update remuneration
                    var remuneration = new List<TbSecDataAccessRemunerationPackage>();
                    var RModels = _context.TbSecDataAccessRemunerationPackage.Where(x => x.DataAccessGroupId == dataAccessGroup.DataAccessGroupId);
                    _context.TbSecDataAccessRemunerationPackage.RemoveRange(RModels);
                    if (request.Data.RemunerationPackageId.Count > 0)
                    {
                        _logger.LogInformation($"Add Remuneration Package");
                        var rmnrtIds = request.Data.RemunerationPackageId.Distinct().ToList();
                        foreach (var rmnrtId in rmnrtIds)
                        {
                            TbSecDataAccessRemunerationPackage model = new TbSecDataAccessRemunerationPackage();
                            model.RemunerationPackageId = rmnrtId;
                            model.DataAccessGroupId = dataAccessGroup.DataAccessGroupId;
                            remuneration.Add(model);
                        }
                        await _context.TbSecDataAccessRemunerationPackage.AddRangeAsync(remuneration);
                    }

                    //update grade
                    var grade = new List<TbSecDataAccessGrade>();
                    var GModels = _context.TbSecDataAccessGrade.Where(x => x.DataAccessGroupId == dataAccessGroup.DataAccessGroupId);
                    _context.TbSecDataAccessGrade.RemoveRange(GModels);
                    if (request.Data.Grade.Count > 0)
                    {
                        _logger.LogInformation($"Add Grade");
                        var gradeItms = request.Data.Grade.Select(m => new { request.Data.DataAccessGroupId, m.MaxGradeLevelNum, m.MinGradeLevelNum }).Distinct().ToList();
                        foreach (var grd in gradeItms)
                        {
                            TbSecDataAccessGrade model = new TbSecDataAccessGrade();
                            model.DataAccessGroupId = request.Data.DataAccessGroupId;
                            model.MaxGradeLevelNum = grd.MaxGradeLevelNum;
                            model.MinGradeLevelNum = grd.MinGradeLevelNum;
                            grade.Add(model);
                        }
                        await _context.TbSecDataAccessGrade.AddRangeAsync(grade);
                    }


                    //update client-entities
                    if (clientId > 0 && entityId > 0)
                    {
                        var clientEntities = new List<TbSecDataAccessClientEntity>();
                        _logger.LogInformation($" Update Selected Client Entity - {clientId} && {entityId}");
                        if (request.Data.clientEntity.Count > 0)
                        {
                            var ceItemsWithDACEid = request.Data.clientEntity.Select(m => new { request.Data.DataAccessGroupId, m.ClientLevel, m.EntityLevel, m.DataAccessClientEntityId }).Distinct().ToList();
                            if (ceItemsWithDACEid != null)
                            {
                                foreach (var ceItem in ceItemsWithDACEid)
                                {
                                    if (ceItem.DataAccessClientEntityId > 0)
                                    {
                                        var ceModels = _context.TbSecDataAccessClientEntity.Where(x => x.DataAccessClientEntityId == ceItem.DataAccessClientEntityId);
                                        if (ceModels != null)
                                        {
                                            _context.TbSecDataAccessClientEntity.RemoveRange(ceModels);
                                        }

                                    }
                                    else
                                    {
                                        break;
                                    }
                                }

                            }

                            var ceItems = request.Data.clientEntity.Select(m => new { request.Data.DataAccessGroupId, m.ClientLevel, m.EntityLevel }).Distinct().ToList();
                            if (ceItems.Count > 0)
                            {
                                foreach (var ceItem in ceItems)
                                {
                                    TbSecDataAccessClientEntity model = new TbSecDataAccessClientEntity();
                                    var ceModels = _context.TbSecDataAccessClientEntity.Where(x => x.DataAccessGroupId == dataAccessGroup.DataAccessGroupId && x.ClientLevel == HierarchyId.Parse(ceItem.ClientLevel) && x.EntityLevel == HierarchyId.Parse(ceItem.EntityLevel));
                                    if (ceModels != null)
                                    {
                                        _context.TbSecDataAccessClientEntity.RemoveRange(ceModels);
                                    }
                                    var clientEntitiesList = new List<TbSecDataAccessClientEntity>();
                                    model.DataAccessGroupId = request.Data.DataAccessGroupId;
                                    model.ClientLevel = HierarchyId.Parse(ceItem.ClientLevel);
                                    model.EntityLevel = HierarchyId.Parse(ceItem.EntityLevel);
                                    clientEntities.Add(model);
                                }
                                await _context.TbSecDataAccessClientEntity.AddRangeAsync(clientEntities);

                            }
                        }
                        else
                        {
                            TbSecDataAccessClientEntity model = new TbSecDataAccessClientEntity();
                            var ceModels = _context.TbSecDataAccessClientEntity.Where(x => x.DataAccessGroupId == dataAccessGroup.DataAccessGroupId);
                            _context.TbSecDataAccessClientEntity.RemoveRange(ceModels);
                        }
                        await _context.TbSecDataAccessClientEntity.AddRangeAsync(clientEntities);

                    }
                    else if (request.Data.clientEntity.Count > 0)
                    {
                        _logger.LogInformation($"Update Selected Client Entity - {clientId} && {entityId}");
                        var clientEntities = new List<TbSecDataAccessClientEntity>();
                        var ceModels = _context.TbSecDataAccessClientEntity.Where(x => x.DataAccessGroupId == dataAccessGroup.DataAccessGroupId);
                        _context.TbSecDataAccessClientEntity.RemoveRange(ceModels);
                        var clientEntitiesList = new List<TbSecDataAccessClientEntity>();
                        var ceItems = request.Data.clientEntity.Select(m => new { request.Data.DataAccessGroupId, m.ClientLevel, m.EntityLevel }).Distinct().ToList();
                        foreach (var ceItem in ceItems)
                        {
                            TbSecDataAccessClientEntity model = new TbSecDataAccessClientEntity();
                            model.DataAccessGroupId = request.Data.DataAccessGroupId;
                            model.ClientLevel = HierarchyId.Parse(ceItem.ClientLevel);
                            model.EntityLevel = HierarchyId.Parse(ceItem.EntityLevel);
                            clientEntities.Add(model);
                        }
                        await _context.TbSecDataAccessClientEntity.AddRangeAsync(clientEntities);
                    }

                    await _context.SaveChangesAsync();
                    await transaction.CommitAsync();
                    return true;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                    await transaction.RollbackAsync();
                    return false;
                }
            }

        }
        /// <summary>
        /// Check if all entities is selected
        /// </summary>
        /// <param name="dataAccessGroupId"></param>
        /// <returns></returns>
        public async Task<bool> IsAllEntities(int dataAccessGroupId)
        {
            _logger.LogInformation($"Check IsAllEntities - {dataAccessGroupId}");
            try
            {
                var rs = await _context.TbSecDataAccessClientEntity
                    .Where(e => e.ClientLevel.GetLevel() == 0)
                    .Where(e => e.EntityLevel.GetLevel() == 0)
                    .Where(e => e.DataAccessGroupId == dataAccessGroupId)
                    .FirstOrDefaultAsync();
                return rs != null;

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Get Data Access Group Organization Structure
        /// </summary>
        /// <param name="dataAccessGroupId"></param>
        /// <returns></returns>
        public async Task<List<DataAccessOrganizationStructureDto>> GetOrganizationStructure(int dataAccessGroupId)
        {
            _logger.LogInformation($"GetOrganizationStructure - {dataAccessGroupId}");
            try
            {
                List<DataAccessOrganizationStructureDto> rs = await _context.TbSecDataAccessOrganizationStructure
                    .Where(e => e.DataAccessGroupId == dataAccessGroupId)
                    .Select(e => new DataAccessOrganizationStructureDto
                    {
                        DataAccessGroupId = e.DataAccessGroupId,
                        NodeLevel = e.NodeLevel.ToString(),
                    })
                    .ToListAsync();

                return rs;

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }


    }
}