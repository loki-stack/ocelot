﻿using CommonLib.Models;
using CommonLib.Models.Core;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Common.Constants;
using CoreServiceAPIs.Common.Dtos;
using CoreServiceAPIs.Common.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Repositories.Impl
{
    public class SettingRepository : ISettingRepository
    {
        private readonly CoreDbContext _context;

        public SettingRepository(CoreDbContext context)
        {
            _context = context;
        }

        public async Task<TbCfgSetting> CreateDefaultSetting(int clientId, int entityId)
        {
            TbCfgSetting setting = new TbCfgSetting
            {
                SpecificClientId = clientId,
                SpecificEntityId = entityId,
                PdfStatus = false,
                PrimaryLanguage = string.Empty,
                SecondaryLanguage = string.Empty,
                CreatedBy = "SYSTEM",
                CreatedDt = DateTime.UtcNow,
                ModifiedBy = "SYSTEM",
                ModifiedDt = DateTime.UtcNow
            };
            await _context.AddAsync(setting);
            await _context.SaveChangesAsync();
            return setting;
        }

        public async Task<List<LanguageModel>> GetLanguages()
        {
            List<LanguageModel> languages = new List<LanguageModel>();
            var result = await (from g in _context.TbCfgGenericCode
                                join i in _context.TbCfgI18n
                                on g.I18nTextKey equals i.TextKey
                                where g.CodeType == "LOCALE"
                                select new
                                {
                                    LanguageCode = g.CodeValue,
                                    LanguageValue = i.TextValue
                                }).ToListAsync();
            foreach (var item in result)
            {
                LanguageModel lang = new LanguageModel
                {
                    LanguageCode = item.LanguageCode,
                    LanguageValue = item.LanguageValue
                };
                languages.Add(lang);
            }
            return languages;
        }

        public async Task<TbCfgSetting> GetSettingOfClientEntity(int clientId, int entityId)
        {
            return await _context.TbCfgSetting.Where(x => x.SpecificClientId == clientId && x.SpecificEntityId == entityId).FirstOrDefaultAsync();
        }

        public async Task UpdateCurrencySetting(int clientId, int entityId, string salaryCurrency, string paymentCurrency)
        {
            TbCfgSetting setting = await _context.TbCfgSetting.Where(x => x.SpecificClientId == clientId && x.SpecificEntityId == entityId).FirstOrDefaultAsync();
            setting.SalaryCurrency = salaryCurrency;
            setting.PaymentCurrency = paymentCurrency;
            _context.Entry(setting).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateEmployeeSetting(int clientId, int entityId, SettingUpdateEmployeeRequest request)
        {
            TbCfgSetting setting = await _context.TbCfgSetting.Where(x => x.SpecificClientId == clientId && x.SpecificEntityId == entityId).FirstOrDefaultAsync();
            setting.PaymentCurrency = request.PaymentCurrency;
            setting.SalaryCurrency = request.SalaryCurrency;
            setting.ProbationPeriod = request.ProbationPeriod;
            setting.WorkCalendar = request.WorkCalendar;
            _context.Entry(setting).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateLanguageSetting(int clientId, int entityId, string primaryLanguageCode, string secondaryLanguageCode)
        {
            TbCfgSetting setting = await _context.TbCfgSetting.Where(x => x.SpecificClientId == clientId && x.SpecificEntityId == entityId).FirstOrDefaultAsync();
            setting.PrimaryLanguage = primaryLanguageCode;
            setting.SecondaryLanguage = secondaryLanguageCode;
            _context.Entry(setting).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdatePDFSetting(int clientId, int entityId, bool pdfStatus)
        {
            TbCfgSetting setting = await _context.TbCfgSetting.Where(x => x.SpecificClientId == clientId && x.SpecificEntityId == entityId).FirstOrDefaultAsync();
            setting.PdfStatus = pdfStatus;
            _context.Entry(setting).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateProbationPeriodSetting(int clientId, int entityId, int probationPeriod)
        {
            TbCfgSetting setting = await _context.TbCfgSetting.Where(x => x.SpecificClientId == clientId && x.SpecificEntityId == entityId).FirstOrDefaultAsync();
            setting.ProbationPeriod = probationPeriod;
            _context.Entry(setting).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateWorkCalendarSetting(int clientId, int entityId, int workCalendar)
        {
            TbCfgSetting setting = await _context.TbCfgSetting.Where(x => x.SpecificClientId == clientId && x.SpecificEntityId == entityId).FirstOrDefaultAsync();
            setting.WorkCalendar = workCalendar;
            _context.Entry(setting).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateSignInMethod(int clientId, int entityId, SignInMethod signInMethod)
        {
            TbCfgSetting setting = await _context.TbCfgSetting.Where(x => x.SpecificClientId == clientId && x.SpecificEntityId == entityId).FirstOrDefaultAsync();
            setting.SignInMethod = (int)signInMethod;
            _context.Entry(setting).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateNotificationEmail(int clientId, int entityId, string notificationEmail)
        {
            TbCfgSetting setting = await _context.TbCfgSetting.Where(x => x.SpecificClientId == clientId && x.SpecificEntityId == entityId).FirstOrDefaultAsync();
            setting.NotificationEmail = notificationEmail;
            _context.Entry(setting).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}