﻿using AutoMapper;
using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Common.Dtos;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Repositories.Impl
{
    public class FuncRepository : IFunctionRepository
    {
        private readonly CoreDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<SecurityProfileRepository> _logger;

        public FuncRepository(CoreDbContext context, IMapper mapper, ILogger<SecurityProfileRepository> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<int> Create(ApiRequest<CreateFuncRequest> request, string username)
        {
            try
            {
                var func = _mapper.Map<CreateFuncRequest, TbSecFunction>(request.Data);
                func.CreateBy = username;
                func.CreateDt = DateTime.Now;
                func.ModifyBy = username;
                func.ModifyDt = DateTime.Now;

                await _context.AddAsync(func);
                await _context.SaveChangesAsync();
                return func.FunctionId;
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex.ToString());
                return 0;
            }
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                var func = await _context.TbSecFunction.FindAsync(id);
                _context.TbSecFunction.Remove(func);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex.ToString());
                return false;
            }
        }

        public async Task<List<FuncViewModel>> GetAll(ApiRequest<PaginationRequest> request)
        {
            try
            {
                GetFunctionRequest funcRequest = new GetFunctionRequest();
                if (!string.IsNullOrEmpty(request.Data.Filter))
                {
                    funcRequest = JsonConvert.DeserializeObject<GetFunctionRequest>(request.Data.Filter);
                }

                var query = _context.TbSecFunction.AsQueryable();

                //search by condition
                if (funcRequest.CreatedDateFrom != null && funcRequest.CreatedDateTo != null)
                    query = query.Where(x => x.CreateDt.Date >= funcRequest.CreatedDateFrom.Value.Date && x.CreateDt.Date <= funcRequest.CreatedDateTo.Value.Date);
                if (!string.IsNullOrEmpty(request.Data.FullTextSearch))
                    query = query.Where(x => x.FunctionCd.Contains(request.Data.FullTextSearch));

                //sort data
                if (!string.IsNullOrEmpty(request.Data.Sort))
                {
                    string sortCharacter = request.Data.Sort.Substring(0, 1);
                    string sortField = request.Data.Sort.Substring(1, request.Data.Sort.Length - 1);
                    if (sortField.ToLower().Equals("functioncd"))
                    {
                        if (sortCharacter.Equals("-"))
                            query = query.OrderByDescending(x => x.FunctionCd);
                        else
                            query = query.OrderBy(x => x.FunctionCd);
                    }
                    if (sortField.ToLower().Equals("createddate"))
                    {
                        if (sortCharacter.Equals("-"))
                            query = query.OrderByDescending(x => x.CreateDt);
                        else
                            query = query.OrderBy(x => x.CreateDt);
                    }
                }

                var result = await query.Select(x => new FuncViewModel
                {
                    FunctionId = x.FunctionId,
                    MenuCd = x.MenuCd,
                    ModuleCd = x.ModuleCd,
                    FeatureCd = x.FeatureCd,
                    FunctionCd = x.FunctionCd,
                    RoleCd = x.RoleCd,
                    MenuDisplayOrder = x.MenuDisplayOrder,
                    CreateBy = x.CreateBy,
                    CreateDt = x.CreateDt,
                    ModifyBy = x.ModifyBy,
                    ModifyDt = x.ModifyDt
                }).ToListAsync();

                return result;
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex.ToString());
                return null;
            }
        }

        public async Task<FuncViewModel> GetById(int id)
        {
            try
            {
                var result = await _context.TbSecFunction
                                    .Select(x => new FuncViewModel
                                    {
                                        FunctionId = x.FunctionId,
                                        MenuCd = x.MenuCd,
                                        ModuleCd = x.ModuleCd,
                                        FeatureCd = x.FeatureCd,
                                        FunctionCd = x.FunctionCd,
                                        RoleCd = x.RoleCd,
                                        MenuDisplayOrder = x.MenuDisplayOrder,
                                        CreateBy = x.CreateBy,
                                        CreateDt = x.CreateDt,
                                        ModifyBy = x.ModifyBy,
                                        ModifyDt = x.ModifyDt
                                    })
                                    .FirstOrDefaultAsync(x => x.FunctionId == id);
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex.ToString());
                return null;
            }
        }

        public async Task<bool> Update(ApiRequest<UpdateFuncRequest> request, string username)
        {
            try
            {
                var func = await _context.TbSecFunction.FirstOrDefaultAsync(x => x.FunctionId == request.Data.FunctionId);
                _mapper.Map<UpdateFuncRequest, TbSecFunction>(request.Data, func);
                func.ModifyBy = username;
                func.ModifyDt = DateTime.Now;
                _context.TbSecFunction.Update(func);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex.ToString());
                return false;
            }
        }

        public bool CheckExistFuncByName(string functionCd, int functionId)
        {
            var result = _context.TbSecFunction
                          .Any(x => x.FunctionCd == functionCd && (functionId == 0 || x.FunctionId != functionId));
            return result;
        }

        public bool CheckExistFuncById(int id)
        {
            var result = _context.TbSecFunction.Any(x => x.FunctionId == id);
            return result;
        }

        public async Task<List<string>> GetFunctionCdByUserId(int userId)
        {
            //  var query = _context.TbSecFunction.AsQueryable();
            var roleList = await (from sf in _context.TbSecFunction
                                  join rolefunc in _context.TbSecRoleFunction on sf.FunctionId equals rolefunc.FunctionId
                                  join sfd in _context.TbSecSecurityProfileDetail on rolefunc.RoleGroupId equals sfd.RoleGroupId
                                  join usfd in _context.TbSecUserSecurityProfile on sfd.SecurityProfileId equals usfd.SecurityProfileId
                                  where usfd.UserId == userId
                                  select sf.RoleCd
            ).Distinct().ToListAsync();
            return roleList;
        }

        public async Task<List<TbSecFunction>> GetAllWithSort()
        {
            var list = await _context.TbSecFunction
                .OrderBy(e => e.ModuleCd).ThenBy(e => e.FeatureCd).ThenBy(e => e.FunctionCd)
                .ToListAsync();
            return list;
        }
    }
}