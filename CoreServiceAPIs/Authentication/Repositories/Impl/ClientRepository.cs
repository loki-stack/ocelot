﻿using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Repositories.Impl
{
    public class ClientRepository : IClientRepository
    {
        private readonly CoreDbContext _context;
        private readonly ILogger<ClientRepository> _logger;

        public ClientRepository(CoreDbContext context, ILogger<ClientRepository> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<List<ClientResult>> GetClientEntities(int userId)
        {
            var userIdParam = new SqlParameter("@userid", userId);

            var result = await _context.Set<ClientResult>()
             .FromSqlRaw("EXECUTE SP_GET_CLIENT_ENTIY @userid", userIdParam)
             .ToListAsync();

            return result;
        }

        public async Task<ClientResult> GetClientEntityId(int clientId, int entityId)
        {
            try
            {
                ClientResult result = new ClientResult();

                result.ClientId = clientId;
                result.EntityId = entityId;

                // client info
                TbCamClient client = await _context.TbCamClient
                    .Where(e => e.ClientId == clientId)
                    .FirstOrDefaultAsync();

                if (client != null)
                {
                    result.ClientCode = client.ClientCode;
                    result.ClientName = client.ClientNameTxt;
                    result.ClientLevel = client.ClientLevel.ToString();
                }
                else
                {
                    result.ClientId = -1;
                }

                // entity info
                TbCamEntity entity = await _context.TbCamEntity
                        .Where(e => e.EntityId == entityId)
                        .FirstOrDefaultAsync();

                if (entity != null)
                {
                    result.EntityCode = entity.EntityCode;
                    result.EntityName = entity.EntityNameTxt;
                    result.EntityLevel = entity.EntityLevel.ToString();
                }
                else
                {
                    if (result.ClientId == 0)
                    {
                        result.EntityId = 0;
                    }
                    else
                    {
                        result.EntityId = -1;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<ClientResult> GetClientEntityId(string clientCode, string entityCode)
        {
            try
            {
                ClientResult result = new ClientResult();

                result.ClientCode = clientCode;
                result.EntityCode = entityCode;

                // client info
                TbCamClient client = await _context.TbCamClient
                    .Where(e => e.ClientCode == clientCode)
                    .FirstOrDefaultAsync();

                if (client != null)
                {
                    result.ClientId = client.ClientId;
                    result.ClientCode = client.ClientCode;
                    result.ClientName = client.ClientNameTxt;
                    result.ClientLevel = client.ClientLevel.ToString();
                }
                else
                {
                    result.ClientId = -1;
                }

                // entity info
                TbCamEntity entity = await _context.TbCamEntity
                        .Where(e => e.EntityCode == entityCode)
                        .FirstOrDefaultAsync();

                if (entity != null)
                {
                    result.EntityId = entity.EntityId;
                    result.EntityCode = entity.EntityCode;
                    result.EntityName = entity.EntityNameTxt;
                    result.EntityLevel = entity.EntityLevel.ToString();
                    if (entity.ParentClientId != result.ClientId)
                    {
                        result.EntityId = -1;
                    }
                }
                else
                {
                    if (result.ClientId == 0)
                    {
                        result.EntityId = 0;
                    }
                    else
                    {
                        result.EntityId = -1;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }
    }
}