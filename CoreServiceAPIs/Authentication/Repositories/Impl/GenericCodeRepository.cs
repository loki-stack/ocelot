﻿using AutoMapper;
using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Common.Dtos;
using CoreServiceAPIs.Common.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Repositories.Impl
{
    public class GenericCodeRepository : IGenericCodeRepository
    {
        private readonly CoreDbContext _context;
        private readonly ILogger<GenericCodeRepository> _logger;
        private readonly IMapper _mapper;

        public GenericCodeRepository(CoreDbContext context, ILogger<GenericCodeRepository> logger, IMapper mapper)
        {
            _context = context;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<Dictionary<string, List<GenericCodeResult>>> GetDDLGenericCodes(int clientId, int entityId, string locale,
    List<string> genericCodes)
        {
            using (var table = new DataTable())
            {
                table.Columns.Add("VALUE", typeof(string));

                if (genericCodes != null && genericCodes.Count() > 0)
                {
                    foreach (var item in genericCodes)
                    {
                        table.Rows.Add(item);
                    }
                }

                var localeParam = new SqlParameter("@locale", locale);
                var clientIdParam = new SqlParameter("@clientid", clientId);
                var entityIdParam = new SqlParameter("@entityid", entityId);
                var codeTypeParams = new SqlParameter("@codeTypes", SqlDbType.Structured)
                {
                    TypeName = "UDT_STRING_ARRAY",
                    Value = table
                };

                var data = await _context.Set<GenericCodeResult>()
                    .FromSqlRaw("EXECUTE SP_GET_GENERIC_CODE @locale, @clientid, @entityid, @codeTypes", localeParam, clientIdParam, entityIdParam, codeTypeParams)
                    .ToListAsync();

                var result = data.GroupBy(x => x.CodeType)
                            .ToDictionary(g => g.Key.ToLower(), g => g.Select(x => x).OrderBy(x => x.DisplayOrder).ToList());

                return result;
            }
        }

        public async Task<List<ModuleGenericCodeViewModel>> GetModules(int clientId, int entityId, string locale)
        {
            var genericCodes = GetGenericCodes(clientId, entityId, locale);

            return await (from g in genericCodes
                          where g.ParentCodeId == null
                          select new ModuleGenericCodeViewModel
                          {
                              CodeId = g.CodeId,
                              CodeValue = g.CodeValue,
                              TextKey = g.TextValue
                          }).OrderBy(x => x.TextKey).ToListAsync();
        }

        public async Task<List<GenericCodeViewModel>> GetFilterByModule(int clientId, int entityId, string locale, string codeType)
        {
            var genericCodes = GetGenericCodes(clientId, entityId, locale);

            var result = (from g in _context.TbCfgGenericCode
                          join i in _context.TbCfgI18n on g.I18nTextKey equals i.TextKey
                          join g1 in genericCodes on g.ParentCodeId equals g1.CodeId into temp1
                          from t1 in temp1.DefaultIfEmpty()
                          join g2 in genericCodes on g.StatusCd equals g2.CodeValue into temp2
                          from t2 in temp2.DefaultIfEmpty()
                          where g.SpecificClientId == clientId && g.SpecificCompanyId == entityId && i.LanguageTag == locale
                            && i.SpecificClientId == clientId && i.SpecificEntityId == entityId && g.CodeType == codeType
                          select new GenericCodeViewModel
                          {
                              Id = g.CodeId,
                              CodeType = g.CodeType,
                              CodeValue = g.CodeValue,
                              ParentCodeId = g.ParentCodeId,
                              ClientId = g.SpecificClientId,
                              EntityId = g.SpecificCompanyId,
                              ParentName = t1 != null ? t1.TextValue : string.Empty,
                              CodeLevel = g.CodeLevel,
                              Name = i.TextValue,
                              Status = g.StatusCd,
                              StatusTxt = t2 != null ? t2.TextValue : string.Empty,
                              DisplayOrder = g.DisplayOrder,
                              Remark = g.Remark,
                              CreateDt = g.CreateDt,
                              CreateBy = g.CreateBy,
                              ModifyBy = g.ModifyBy,
                              ModifyDt = g.ModifyDt
                          }).OrderBy(x => x.DisplayOrder).ToList();
            return result;
        }

        private IQueryable<GenericCodeDto> GetGenericCodes(int clientId, int entityId, string locale)
        {
            var t1 = (from i in _context.TbCfgI18n
                      where i.SpecificClientId == clientId && i.SpecificEntityId == entityId && i.LanguageTag == locale
                      select i).AsQueryable();

            var t2 = t1.Union(from i in _context.TbCfgI18n
                              where i.SpecificClientId == clientId && i.SpecificEntityId == entityId && i.LanguageTag != locale && i.DefaultFlag == true
                                    && !t1.Select(x => x.TextKey).Contains(i.TextKey)
                              select i).AsQueryable();

            var t3 = t2.Union(from i in _context.TbCfgI18n
                              where i.SpecificClientId == clientId && i.SpecificEntityId == 0 && i.LanguageTag != locale && i.DefaultFlag == true
                                    && !t2.Select(x => x.TextKey).Contains(i.TextKey)
                              select i).AsQueryable();

            var t4 = t3.Union(from i in _context.TbCfgI18n
                              where i.SpecificClientId == 0 && i.SpecificEntityId == 0 && i.LanguageTag == locale
                                    && !t3.Select(x => x.TextKey).Contains(i.TextKey)
                              select i).AsQueryable();

            var t5 = t4.Union(from i in _context.TbCfgI18n
                              where i.SpecificClientId == 0 && i.SpecificEntityId == 0 && i.LanguageTag != locale && i.DefaultFlag == true
                                    && !t4.Select(x => x.TextKey).Contains(i.TextKey)
                              select i).AsQueryable();

            var g1 = (from g in _context.TbCfgGenericCode
                      where g.SpecificClientId == clientId && g.SpecificCompanyId == entityId
                      select g).AsQueryable();

            var g2 = g1.Union(from g in _context.TbCfgGenericCode
                              where g.SpecificClientId == clientId && g.SpecificCompanyId == 0 && !g1.Select(x => x.I18nTextKey).Contains(g.I18nTextKey)
                              select g).AsQueryable();

            var g3 = g2.Union(from g in _context.TbCfgGenericCode
                              where g.SpecificClientId == 0 && g.SpecificCompanyId == 0 && !g2.Select(x => x.I18nTextKey).Contains(g.I18nTextKey)
                              select g).AsQueryable();

            var result = (from g in g3
                          join t in t5 on g.I18nTextKey equals t.TextKey
                          select new GenericCodeDto
                          {
                              CodeId = g.CodeId,
                              CodeType = g.CodeType,
                              CodeValue = g.CodeValue,
                              DisplayOrder = g.DisplayOrder,
                              StatusCd = g.StatusCd,
                              I18NTextKey = g.I18nTextKey,
                              TextValue = t.TextValue,
                              ParentCodeId = g.ParentCodeId
                          }).AsQueryable();
            return result;
        }

        public async Task<GenericCodeViewModel> GetById(int id, string locale)
        {
            var model = await _context.TbCfgGenericCode.FirstOrDefaultAsync(x => x.CodeId == id);
            var genericCodes = GetGenericCodes(model.SpecificClientId, model.SpecificCompanyId, locale);
            var result = await (from g in _context.TbCfgGenericCode
                                join i in _context.TbCfgI18n on g.I18nTextKey equals i.TextKey
                                join d1 in genericCodes on g.ParentCodeId equals d1.CodeId into temp1
                                from t1 in temp1.DefaultIfEmpty()
                                join d2 in genericCodes on g.StatusCd equals d2.CodeValue into temp2
                                from t2 in temp2.DefaultIfEmpty()
                                where g.CodeId == id && i.LanguageTag == locale && g.SpecificClientId == i.SpecificClientId && g.SpecificCompanyId == i.SpecificEntityId
                                select new GenericCodeViewModel
                                {
                                    Id = g.CodeId,
                                    CodeType = g.CodeType,
                                    CodeValue = g.CodeValue,
                                    ParentCodeId = g.ParentCodeId,
                                    ClientId = g.SpecificClientId,
                                    EntityId = g.SpecificCompanyId,
                                    ParentName = t1 != null ? t1.TextValue : string.Empty,
                                    CodeLevel = g.CodeLevel,
                                    Name = i.TextValue,
                                    Status = g.StatusCd,
                                    StatusTxt = t2 != null ? t2.TextValue : string.Empty,
                                    DisplayOrder = g.DisplayOrder,
                                    Remark = g.Remark,
                                    CreateDt = g.CreateDt,
                                    CreateBy = g.CreateBy,
                                    ModifyBy = g.ModifyBy,
                                    ModifyDt = g.ModifyDt
                                }).FirstOrDefaultAsync();
            return result;
        }

        public async Task<CreateObjectResult<GenericCodeViewModel>> Create(string locale, int clientId, int entityId, CreateGenericCodeRequest request)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                CreateObjectResult<GenericCodeViewModel> result = new CreateObjectResult<GenericCodeViewModel>();
                try
                {
                    var genericCode = new TbCfgGenericCode();
                    bool isExist = CheckExistGenericCode(request.ParentCodeId, clientId, entityId, request.ModuleId, 0, request.CodeValue);
                    if (!isExist)
                    {
                        //Create generic code
                        genericCode = _mapper.Map<CreateGenericCodeRequest, TbCfgGenericCode>(request);
                        genericCode.SpecificClientId = clientId;
                        genericCode.SpecificCompanyId = entityId;
                        //Get code type of parent
                        var codeValue = _context.TbCfgGenericCode
                                                .FirstOrDefault(x => x.CodeId == request.ModuleId)
                                                .CodeValue;
                        genericCode.CodeType = codeValue;
                        await _context.TbCfgGenericCode.AddAsync(genericCode);
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        string codeType = _context.TbCfgGenericCode.FirstOrDefault(x => x.CodeId == request.ParentCodeId).CodeValue;
                        genericCode = _context.TbCfgGenericCode.FirstOrDefault(x => x.CodeType == codeType && x.CodeValue == request.CodeValue
                                                                               && x.ParentCodeId == request.ParentCodeId && x.SpecificClientId == clientId);
                    }

                    var i18n = _mapper.Map<CreateGenericCodeRequest, TbCfgI18n>(request);
                    i18n.SpecificClientId = clientId;
                    i18n.SpecificEntityId = entityId;
                    i18n.LanguageTag = request.Locale;
                    i18n.DefaultFlag = true;
                    //Create I18n
                    //Get I18nTextKey
                    i18n.TextKey = _context.TbCfgGenericCode.FirstOrDefault(x => x.CodeId == genericCode.CodeId)
                                            .I18nTextKey;
                    await _context.TbCfgI18n.AddAsync(i18n);
                    await _context.SaveChangesAsync();
                    await transaction.CommitAsync();
                    result.IsSuccess = true;
                    result.Data = await GetById(genericCode.CodeId, locale);
                    return result;
                }
                catch (Exception ex)
                {
                    await transaction.RollbackAsync();
                    _logger.LogWarning(ex.ToString());
                    throw ex;
                }
            }
        }

        public async Task<UpdateObjectResult<GenericCodeViewModel>> Update(string locale, UpdateGenericCodeRequest request)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                UpdateObjectResult<GenericCodeViewModel> result = new UpdateObjectResult<GenericCodeViewModel>();
                try
                {
                    //Update generic code
                    var genericCode = await _context.TbCfgGenericCode.FirstOrDefaultAsync(x => x.CodeId == request.Id);
                    var i18nTextKeyOld = genericCode.I18nTextKey;
                    genericCode.CodeValue = request.CodeValue;
                    genericCode.StatusCd = request.Status;
                    genericCode.ModifyBy = request.ModifiedBy;
                    genericCode.ModifyDt = request.ModifiedDt;
                    genericCode.ParentCodeId = request.ParentCodeId;
                    genericCode.DisplayOrder = request.DisplayOrder;
                    genericCode.Remark = request.Remark;
                    _context.Entry(genericCode).State = EntityState.Modified;
                    await _context.SaveChangesAsync();

                    //Update I18n
                    //Get I18nTextKey
                    var i18n = await _context.TbCfgI18n
                                    .FirstOrDefaultAsync(x => x.TextKey == i18nTextKeyOld && x.SpecificClientId == genericCode.SpecificClientId
                                     && x.SpecificEntityId == genericCode.SpecificCompanyId && x.LanguageTag == request.Locale);
                    _context.TbCfgI18n.Remove(i18n);
                    await _context.SaveChangesAsync();
                    i18n.TextValue = request.Name;
                    i18n.ModifyBy = request.ModifiedBy;
                    i18n.ModifyDt = request.ModifiedDt;
                    i18n.TextKey = genericCode.I18nTextKey;
                    i18n.SpecificClientId = genericCode.SpecificClientId;
                    i18n.SpecificEntityId = genericCode.SpecificCompanyId;
                    i18n.LanguageTag = i18n.LanguageTag;
                    i18n.DefaultFlag = true;

                    await _context.TbCfgI18n.AddAsync(i18n);
                    await _context.SaveChangesAsync();
                    await transaction.CommitAsync();

                    result.Data = await GetById(genericCode.CodeId, locale);
                    result.IsSuccess = true;
                    return result;
                }
                catch (Exception ex)
                {
                    await transaction.RollbackAsync();
                    _logger.LogWarning(ex.ToString());
                    throw ex;
                }
            }
        }

        public bool CheckExistGenericCodeById(int id)
        {
            return _context.TbCfgGenericCode.Any(x => x.CodeId == id);
        }

        public bool CheckExistGenericCode(int parentCodeId, int clientId, int entityId, int moduleId, int codeId, string codeValue)
        {
            string codeType = _context.TbCfgGenericCode.FirstOrDefault(x => x.CodeId == moduleId).CodeValue;
            return _context.TbCfgGenericCode
                    .Any(x => x.CodeType == codeType && x.CodeValue == codeValue
                                && x.ParentCodeId == parentCodeId && x.SpecificClientId == clientId && x.SpecificCompanyId == entityId && (codeId == 0 || x.CodeId != codeId));
        }

        public bool CheckExistI18N(int parentCodeId, int clientId, int entityId, string locale, string codeValue, int codeId, int moduleId)
        {
            string codeType = _context.TbCfgGenericCode.FirstOrDefault(x => x.CodeId == moduleId).CodeValue;
            string textKey = codeType + "." + codeValue;
            if (codeId == 0)
            {
                return _context.TbCfgI18n
                .Any(x => x.SpecificClientId == clientId && x.SpecificEntityId == entityId
                        && x.LanguageTag == locale && x.TextKey == textKey);
            }
            else
            {
                var i18ns = (from g in _context.TbCfgGenericCode
                             join i in _context.TbCfgI18n on g.I18nTextKey equals i.TextKey
                             where g.CodeId != codeId && g.I18nTextKey == textKey && i.SpecificClientId == clientId
                             && i.SpecificEntityId == entityId && i.LanguageTag == locale
                             select i).ToList();
                if (i18ns.Count > 0)
                    return true;
                return false;
            }
        }

        public async Task<List<GenericCodeViewModel>> GetTreeGenericCodes(int clientId, int entityId, string locale)
        {
            var result = await (from g in _context.TbCfgGenericCode
                                join i in _context.TbCfgI18n on g.I18nTextKey equals i.TextKey
                                where i.LanguageTag == locale && g.SpecificClientId == clientId && g.SpecificCompanyId == entityId && g.ParentCodeId != null
                                && i.SpecificClientId == clientId && i.SpecificEntityId == entityId && i.LanguageTag == locale
                                select new GenericCodeViewModel
                                {
                                    Id = g.CodeId,
                                    CodeType = g.CodeType,
                                    CodeValue = g.CodeValue,
                                    ParentCodeId = g.ParentCodeId,
                                    CodeLevel = g.CodeLevel,
                                    Name = i.TextValue,
                                    Status = g.StatusCd,
                                    DisplayOrder = g.DisplayOrder,
                                    Remark = g.Remark,
                                    CreateDt = g.CreateDt,
                                    CreateBy = g.CreateBy,
                                    ModifyBy = g.ModifyBy,
                                    ModifyDt = g.ModifyDt
                                }).OrderBy(x => x.CodeValue).ToListAsync();

            return result;
        }

        public async Task<List<GenericCodeViewModel>> GetParent(string locale)
        {
            var result = await (from g in _context.TbCfgGenericCode
                                join i in _context.TbCfgI18n on g.I18nTextKey equals i.TextKey
                                where i.LanguageTag == locale && g.ParentCodeId == null && g.SpecificClientId == i.SpecificClientId
                                && g.SpecificCompanyId == i.SpecificEntityId
                                select new GenericCodeViewModel
                                {
                                    Id = g.CodeId,
                                    CodeType = g.CodeType,
                                    CodeValue = g.CodeValue,
                                    ParentCodeId = g.ParentCodeId,
                                    CodeLevel = g.CodeLevel,
                                    Name = i.TextValue,
                                    Status = g.StatusCd,
                                    DisplayOrder = g.DisplayOrder,
                                    Remark = g.Remark,
                                    CreateDt = g.CreateDt,
                                    CreateBy = g.CreateBy,
                                    ModifyBy = g.ModifyBy,
                                    ModifyDt = g.ModifyDt
                                }).OrderBy(x => x.CodeValue).ToListAsync();
            return result;
        }

        public async Task<Dictionary<string, List<GenericCodeResult>>> GetGenericCodes(int clientId, int entityId, string locale,
    List<string> genericCodes)
        {
            using (var table = new DataTable())
            {
                table.Columns.Add("VALUE", typeof(string));

                if (genericCodes != null && genericCodes.Count() > 0)
                {
                    foreach (var item in genericCodes)
                    {
                        table.Rows.Add(item);
                    }
                }

                var localeParam = new SqlParameter("@locale", locale);
                var clientIdParam = new SqlParameter("@clientid", clientId);
                var entityIdParam = new SqlParameter("@entityid", entityId);
                var codeTypeParams = new SqlParameter("@codeTypes", SqlDbType.Structured)
                {
                    TypeName = "UDT_STRING_ARRAY",
                    Value = table
                };

                var data = await _context.Set<GenericCodeResult>()
                    .FromSqlRaw("EXECUTE SP_GET_GENERIC_CODE @locale, @clientid, @entityid, @codeTypes", localeParam, clientIdParam, entityIdParam, codeTypeParams)
                    .ToListAsync();

                var result = data.GroupBy(x => x.CodeType)
                            .ToDictionary(g => g.Key.ToLower(), g => g.Select(x => x).ToList());

                return result;
            }
        }
    }
}