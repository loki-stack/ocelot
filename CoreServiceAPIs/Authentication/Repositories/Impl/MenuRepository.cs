﻿using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Repositories.Impl
{
    public class MenuRepository : IMenuRepository
    {
        private readonly CoreDbContext _context;
        private readonly ILogger<MenuRepository> _logger;

        public MenuRepository(CoreDbContext context, ILogger<MenuRepository> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<Dictionary<string, IList>> GetMenu(int userId, ApiRequest request)
        {
            var result = new Dictionary<string, IList>();
            var userIdParam = new SqlParameter("@userid", userId);
            var localeParam = new SqlParameter("@locale", request.Parameter.Locale);
            var clientIdParam = new SqlParameter("@clientid", request.Parameter.ClientId);
            var entityIdParam = new SqlParameter("@entityid", request.Parameter.EntityId);

            var leftMenus = await _context.Set<LeftMenuResult>()
             .FromSqlRaw("EXECUTE SP_GET_LEFT_MENU @userid, @locale, @clientid, @entityid", userIdParam, localeParam, clientIdParam, entityIdParam)
             .ToListAsync();

            var rightMenus = await _context.Set<RightMenuResult>()
                .FromSqlRaw("EXECUTE SP_GET_RIGHT_MENU @userid, @locale, @clientid, @entityid", userIdParam, localeParam, clientIdParam, entityIdParam)
                .ToListAsync();

            result.Add("leftMenu", leftMenus);
            result.Add("rightMenu", rightMenus);
            return result;
        }
    }
}