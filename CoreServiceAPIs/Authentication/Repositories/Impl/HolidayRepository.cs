﻿using CommonLib.Models;
using CommonLib.Models.Core;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Repositories.Impl
{
    public class HolidayRepository : IHolidayRepository
    {
        private readonly CoreDbContext _context;

        public HolidayRepository(CoreDbContext context)
        {
            _context = context;
        }

        public bool CheckExistHolidayById(int id)
        {
            return _context.TbCfgHoliday.Any(x => x.HolidayDateId == id);
        }

        public async Task<bool> Create(TbCfgHoliday holiday)
        {
            await _context.TbCfgHoliday.AddAsync(holiday);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<bool> CreateMany(List<TbCfgHoliday> holidays)
        {
            await _context.TbCfgHoliday.AddRangeAsync(holidays);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<bool> Delete(int id)
        {
            var holiday = await _context.TbCfgHoliday.FirstOrDefaultAsync(x => x.HolidayDateId == id);
            _context.TbCfgHoliday.Remove(holiday);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<bool> DeleteMany(List<TbCfgHoliday> holidays)
        {
            _context.TbCfgHoliday.RemoveRange(holidays);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<TbCfgHoliday> GetById(int id)
        {
            var holiday = await _context.TbCfgHoliday.AsNoTracking().FirstOrDefaultAsync(x => x.HolidayDateId == id);
            return holiday;
        }

        public async Task<List<TbCfgHoliday>> GetByIds(List<int> ids)
        {
            var holidays = await _context.TbCfgHoliday.Where(x => ids.Contains(x.HolidayDateId)).ToListAsync();
            return holidays;
        }

        public async Task<List<TbCfgHoliday>> GetHolidayByCalendarId(int calendarId)
        {
            var holidays = await _context.TbCfgHoliday.Where(x => x.CalendarId == calendarId).AsNoTracking().ToListAsync();
            return holidays;
        }

        public async Task<bool> Update(TbCfgHoliday holiday)
        {
            _context.TbCfgHoliday.Update(holiday);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<bool> UpdateMany(List<TbCfgHoliday> holidays)
        {
            _context.TbCfgHoliday.UpdateRange(holidays);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}