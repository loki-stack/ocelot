﻿using CommonLib.Models;
using CommonLib.Models.Client;
using CommonLib.Models.Core;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Common.Constants;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Repositories.Impl
{
    public class GlobalCalendarRepository : IGlobalCalendarRepository
    {
        private readonly CoreDbContext _context;

        public GlobalCalendarRepository(CoreDbContext context)
        {
            _context = context;
        }

        protected ClientBasicDBContext CreateContext(string _connectionString)
        {
            return new ClientBasicDBContext(_connectionString);
        }

        public bool CheckExistGlobalCalendarById(int id)
        {
            return _context.TbCfgGlobalCalendar.Any(x => x.GlobalCalendarId == id);
        }

        public async Task<int> Create(TbCfgGlobalCalendar globalCalendar)
        {
            await _context.TbCfgGlobalCalendar.AddAsync(globalCalendar);
            await _context.SaveChangesAsync();
            return globalCalendar.GlobalCalendarId;
        }

        public async Task<bool> Delete(int id)
        {
            var globalCalendar = await _context.TbCfgGlobalCalendar.FirstOrDefaultAsync(x => x.GlobalCalendarId == id);
            _context.TbCfgGlobalCalendar.Remove(globalCalendar);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<TbCfgGlobalCalendar> GetById(int id)
        {
            var globalCalendar = await _context.TbCfgGlobalCalendar.Include(x => x.TbCfgHoliday).FirstOrDefaultAsync(x => x.GlobalCalendarId == id);
            return globalCalendar;
        }

        public async Task<List<TbCfgGlobalCalendar>> GetFilter()
        {
            return await _context.TbCfgGlobalCalendar
                            .Include(x => x.TbCfgHoliday)
                            .ToListAsync();
        }

        public async Task<int> Update(TbCfgGlobalCalendar globalCalendar)
        {
            _context.TbCfgGlobalCalendar.Update(globalCalendar);
            await _context.SaveChangesAsync();
            return globalCalendar.GlobalCalendarId;
        }

        public bool CheckExistCalendarNameOfCountry(string calendarName, int calendarId)
        {
            return _context.TbCfgGlobalCalendar.Any(x => x.GlobalLevelCalendarName.Trim().ToLower() == calendarName.Trim().ToLower() && (calendarId == 0 || x.GlobalCalendarId != calendarId));
        }

        public async Task<bool> CheckLinkClientEntity(int calendarId)
        {
            bool isLink = false;
            var entities = await _context.TbCamEntity.ToListAsync();
            foreach (var entity in entities)
            {
                if (!string.IsNullOrEmpty(entity.ConnStringParam))
                {
                    using (var clientContext = CreateContext(entity.ConnStringParam))
                    {
                        bool isExist = await clientContext.TbCamWorkCalendar.AnyAsync(x => x.ParentCalendarId == calendarId);
                        if (isExist)
                        {
                            isLink = true;
                            break;
                        }
                    }
                }
            }
            return isLink;
        }

        public async Task<bool> CheckExistGlobalActiveOfCountry(string country, int calendarId)
        {
            return await _context.TbCfgGlobalCalendar.AnyAsync(x => x.Country == country && x.Status == Status.ACTIVE && (calendarId == 0 || x.GlobalCalendarId != calendarId));
        }
    }
}