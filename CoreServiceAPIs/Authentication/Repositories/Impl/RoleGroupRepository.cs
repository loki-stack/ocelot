﻿using AutoMapper;
using CommonLib.Dtos;
using CommonLib.Models.Core;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Common.Dtos;
using CoreServiceAPIs.Common.ShardingUtil;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Repositories.Impl
{
    public class RoleGroupRepository : IRoleGroupRepository
    {
        private readonly CoreDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<SecurityProfileRepository> _logger;

        public RoleGroupRepository(CoreDbContext context, IMapper mapper, ILogger<SecurityProfileRepository> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<int> Create(ApiRequest<CreateRoleGroupRequest> request, string username)
        {
            _logger.LogInformation($"Create - description - {request.Data.RoleGroupDescriptionTxt} & name -{request.Data.RoleGroupNameTxt}");
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var roleGroup = _mapper.Map<CreateRoleGroupRequest, TbSecRoleGroup>(request.Data);
                    roleGroup.CreateBy = username;
                    roleGroup.CreateDt = DateTime.Now;
                    roleGroup.ModifyBy = username;
                    roleGroup.ModifyDt = DateTime.Now;

                    await _context.AddAsync(roleGroup);
                    await _context.SaveChangesAsync();
                    if (request.Data.FuncIds.Count > 0)
                    {
                        var funcIds = request.Data.FuncIds.Distinct().ToList();
                        IList<TbSecRoleFunction> details = new List<TbSecRoleFunction>();
                        foreach (var funcId in funcIds)
                        {
                            TbSecRoleFunction model = new TbSecRoleFunction();
                            model.FunctionId = funcId;
                            model.RoleGroupId = roleGroup.RoleGroupId;
                            details.Add(model);
                        }
                        await _context.AddRangeAsync(details);
                        await _context.SaveChangesAsync();
                    }
                    await transaction.CommitAsync();
                    return roleGroup.RoleGroupId;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                    await transaction.RollbackAsync();
                    return 0;
                }
            }
        }

        //create w/o funcIds
        public async Task<int> QuickCreate(ApiRequest<CreateRoleGroupRequest> request, string username)
        {
            _logger.LogInformation($"QuickCreate - description - {request.Data.RoleGroupDescriptionTxt} & name -{request.Data.RoleGroupNameTxt}");
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var roleGroup = _mapper.Map<CreateRoleGroupRequest, TbSecRoleGroup>(request.Data);
                    roleGroup.CreateBy = username;
                    roleGroup.CreateDt = DateTime.Now;
                    roleGroup.ModifyBy = username;
                    roleGroup.ModifyDt = DateTime.Now;

                    await _context.AddAsync(roleGroup);
                    await _context.SaveChangesAsync();
                    await transaction.CommitAsync();
                    return roleGroup.RoleGroupId;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                    await transaction.RollbackAsync();
                    return 0;
                }
            }
        }

        public async Task<bool> Update(ApiRequest<CreateRoleGroupRequest> request, string username)
        {
            _logger.LogInformation($"Update - RoleGroupId - {request.Data.RoleGroupId} & description - {request.Data.RoleGroupDescriptionTxt} & name -{request.Data.RoleGroupNameTxt}");
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var roleGroup = await _context.TbSecRoleGroup.FirstOrDefaultAsync(x => x.RoleGroupId == request.Data.RoleGroupId);
                    _mapper.Map<CreateRoleGroupRequest, TbSecRoleGroup>(request.Data, roleGroup);
                    roleGroup.ModifyBy = username;
                    roleGroup.ModifyDt = DateTime.Now;
                    _context.TbSecRoleGroup.Update(roleGroup);
                    await _context.SaveChangesAsync();

                    var details = new List<TbSecRoleFunction>();
                    var models = _context.TbSecRoleFunction.Where(x => x.RoleGroupId == roleGroup.RoleGroupId);
                    _context.TbSecRoleFunction.RemoveRange(models);
                    if (request.Data.FuncIds.Count > 0)
                    {
                        var funcIds = request.Data.FuncIds.Distinct().ToList();
                        foreach (var funcId in funcIds)
                        {
                            TbSecRoleFunction model = new TbSecRoleFunction();
                            model.FunctionId = funcId;
                            model.RoleGroupId = roleGroup.RoleGroupId;
                            details.Add(model);
                        }
                        await _context.TbSecRoleFunction.AddRangeAsync(details);
                    }
                    await _context.SaveChangesAsync();
                    await transaction.CommitAsync();
                    return true;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                    await transaction.RollbackAsync();
                    return false;
                }
            }
        }

        public async Task<List<RoleGroupViewModel>> GetAll(ApiRequest<PaginationRequest> request)
        {
            _logger.LogInformation($"GetAll - filter - {request.Data.Filter} & size -{request.Data.Size}");
            try
            {
                GetRoleGroupRequest roleGroupRequest = new GetRoleGroupRequest();
                if (!string.IsNullOrEmpty(request.Data.Filter))
                {
                    roleGroupRequest = JsonConvert.DeserializeObject<GetRoleGroupRequest>(request.Data.Filter);
                }
                var query = _context.TbSecRoleGroup
                            .Include(x => x.TbSecRoleFunction)
                            .ThenInclude(x => x.Function)
                            .Select(x => new
                            {
                                RoleGroupId = x.RoleGroupId,
                                SpecificClientId = x.SpecificClientId,
                                SpecificEntityId = x.SpecificEntityId,
                                RoleGroupNameTxt = x.RoleGroupNameTxt,
                                RoleGroupDescriptionTxt = x.RoleGroupDescriptionTxt,
                                CreateDt = x.CreateDt,
                                Funcs = x.TbSecRoleFunction.Select(y => new
                                {
                                    FunctionId = y.Function.FunctionId,
                                    FunctionCd = y.Function.FunctionCd
                                }).AsQueryable()
                            })
                            .AsQueryable();
                //search by condition
                if (roleGroupRequest.CreatedDateFrom != null && roleGroupRequest.CreatedDateTo != null)
                    query = query.Where(x => x.CreateDt.Date >= roleGroupRequest.CreatedDateFrom.Value.Date && x.CreateDt.Date <= roleGroupRequest.CreatedDateTo.Value.Date);
                if (!string.IsNullOrEmpty(request.Data.FullTextSearch))
                    query = query.Where(x => x.RoleGroupNameTxt.Contains(request.Data.FullTextSearch) || x.RoleGroupNameTxt.Contains(request.Data.FullTextSearch));

                //sort data
                if (!string.IsNullOrEmpty(request.Data.Sort))
                {
                    string sortCharacter = request.Data.Sort.Substring(0, 1);
                    string sortField = request.Data.Sort.Substring(1, request.Data.Sort.Length - 1);
                    if (sortField.ToLower().Equals("rolegroupname"))
                    {
                        if (sortCharacter.Equals("-"))
                            query = query.OrderByDescending(x => x.RoleGroupNameTxt);
                        else
                            query = query.OrderBy(x => x.RoleGroupNameTxt);
                    }
                    if (sortField.ToLower().Equals("createddate"))
                    {
                        if (sortCharacter.Equals("-"))
                            query = query.OrderByDescending(x => x.CreateDt);
                        else
                            query = query.OrderBy(x => x.CreateDt);
                    }
                }

                var result = await query.Select(x => new RoleGroupViewModel
                {
                    RoleGroupId = x.RoleGroupId,
                    SpecificClientId = x.SpecificClientId,
                    SpecificEntityId = x.SpecificEntityId,
                    RoleGroupNameTxt = x.RoleGroupNameTxt,
                    RoleGroupDescriptionTxt = x.RoleGroupDescriptionTxt,
                    CreateDt = x.CreateDt,
                    Funcs = x.Funcs.Select(f => new RoleFunctionViewModel
                    {
                        FunctionId = f.FunctionId,
                        FunctionCd = f.FunctionCd
                    }).ToList()
                }).ToListAsync();

                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<RoleGroupViewModel> GetById(int id)
        {
            _logger.LogInformation($"GetById - id - {id}");
            try
            {
                var result = await _context.TbSecRoleGroup
                        .Where(x => x.RoleGroupId == id)
                        .Include(x => x.TbSecRoleFunction)
                        .ThenInclude(x => x.Function)
                        .Select(x => new RoleGroupViewModel
                        {
                            RoleGroupId = x.RoleGroupId,
                            SpecificClientId = x.SpecificClientId,
                            SpecificEntityId = x.SpecificEntityId,
                            RoleGroupNameTxt = x.RoleGroupNameTxt,
                            RoleGroupDescriptionTxt = x.RoleGroupDescriptionTxt,
                            CreateDt = x.CreateDt,
                            Funcs = x.TbSecRoleFunction.Select(y => new RoleFunctionViewModel
                            {
                                FunctionId = y.Function.FunctionId,
                                FunctionCd = y.Function.FunctionCd
                            }).ToList()
                        }).FirstOrDefaultAsync();
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<bool> Delete(int id)
        {
            _logger.LogInformation($"Delete - id - {id}");
            try
            {
                var roleGroup = await _context.TbSecRoleGroup.FindAsync(id);
                _context.TbSecRoleGroup.Remove(roleGroup);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return false;
            }
        }

        public bool CheckRoleGroupById(int id)
        {
            _logger.LogInformation($"CheckRoleGroupById - id - {id}");
            return _context.TbSecRoleGroup.Any(x => x.RoleGroupId == id);
        }

        public async Task<List<RoleGroupViewModel>> GetEntityRoleGroup(int clientId, int entityId)
        {
            _logger.LogInformation($"GetEntityRoleGroup -- {clientId} - {entityId}");
            try
            {

                var q = (_context.TbSecRoleGroup
                                    .Include(x => x.TbSecSecurityProfileDetail)
                                    .Include(x => x.TbSecRoleFunction)
                                    .ThenInclude(x => x.Function)
                                ).AsQueryable();

                if (entityId > 0)
                {
                    q = q.Where(x => x.SpecificEntityId == entityId);
                }

                if (clientId > 0)
                {
                    q = q.Where(x => x.SpecificClientId == clientId);
                }

                var result = await q
                            .Select(x => new RoleGroupViewModel
                            {
                                RoleGroupId = x.RoleGroupId,
                                SpecificClientId = x.SpecificClientId,
                                SpecificEntityId = x.SpecificEntityId,
                                RoleGroupNameTxt = x.RoleGroupNameTxt,
                                RoleGroupDescriptionTxt = x.RoleGroupDescriptionTxt,
                                CreateDt = x.CreateDt,
                                ModifyDt = x.ModifyDt,
                                ModifyBy = x.ModifyBy,
                                LastModified = Utils.TimeAgo(x.ModifyDt.DateTime),
                                SecurityProfileMappingCount = x.TbSecSecurityProfileDetail
                                                                .Select(m => new { m.RoleGroupId, m.SecurityProfileId })
                                                                .Distinct()
                                                                .Count(),
                                Funcs = x.TbSecRoleFunction.Select(y => new RoleFunctionViewModel
                                {
                                    FunctionId = y.Function.FunctionId,
                                    FunctionCd = y.Function.FunctionCd
                                }
                                ).ToList()
                            }
                            )
                            .ToListAsync();

                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }
    }
}