﻿using CommonLib.Models;
using CommonLib.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Repositories.Interfaces
{
    public interface IHolidayRepository
    {
        Task<bool> Create(TbCfgHoliday holiday);

        Task<bool> CreateMany(List<TbCfgHoliday> holidays);

        Task<bool> Update(TbCfgHoliday holiday);

        Task<bool> UpdateMany(List<TbCfgHoliday> holidays);

        Task<TbCfgHoliday> GetById(int id);

        bool CheckExistHolidayById(int id);

        Task<List<TbCfgHoliday>> GetHolidayByCalendarId(int calendarId);

        Task<bool> Delete(int id);

        Task<bool> DeleteMany(List<TbCfgHoliday> holidays);

        Task<List<TbCfgHoliday>> GetByIds(List<int> ids);
    }
}