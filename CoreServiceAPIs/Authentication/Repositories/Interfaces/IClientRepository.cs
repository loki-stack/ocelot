﻿using CommonLib.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Repositories.Interfaces
{
    public interface IClientRepository
    {
        Task<List<ClientResult>> GetClientEntities(int userId);

        Task<ClientResult> GetClientEntityId(int clientId, int entityId);
        Task<ClientResult> GetClientEntityId(string clientCode, string entityCode);
    }
}