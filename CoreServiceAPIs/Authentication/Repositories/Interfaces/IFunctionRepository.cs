﻿using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using CoreServiceAPIs.Common.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Repositories.Interfaces
{
    public interface IFunctionRepository
    {
        Task<List<FuncViewModel>> GetAll(ApiRequest<PaginationRequest> request);

        Task<FuncViewModel> GetById(int id);

        Task<int> Create(ApiRequest<CreateFuncRequest> request, string username);

        Task<bool> Update(ApiRequest<UpdateFuncRequest> request, string username);

        Task<bool> Delete(int id);

        bool CheckExistFuncByName(string functionCd, int functionId);

        bool CheckExistFuncById(int id);

        Task<List<string>> GetFunctionCdByUserId(int userId);

        Task<List<TbSecFunction>> GetAllWithSort();
    }
}