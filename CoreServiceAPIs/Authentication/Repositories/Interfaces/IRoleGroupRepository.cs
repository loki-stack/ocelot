﻿using CommonLib.Dtos;
using CoreServiceAPIs.Common.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Repositories.Interfaces
{
    public interface IRoleGroupRepository
    {
        Task<int> Create(ApiRequest<CreateRoleGroupRequest> request, string username);
        Task<int> QuickCreate(ApiRequest<CreateRoleGroupRequest> request, string username);

        Task<bool> Update(ApiRequest<CreateRoleGroupRequest> request, string username);

        Task<List<RoleGroupViewModel>> GetAll(ApiRequest<PaginationRequest> request);

        Task<RoleGroupViewModel> GetById(int id);

        Task<bool> Delete(int id);

        bool CheckRoleGroupById(int id);

        Task<List<RoleGroupViewModel>> GetEntityRoleGroup(int clientId, int entityId);
    }
}