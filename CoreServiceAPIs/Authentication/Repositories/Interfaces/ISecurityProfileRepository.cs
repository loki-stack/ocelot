﻿using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using CoreServiceAPIs.Common.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Repositories.Interfaces
{
    public interface ISecurityProfileRepository
    {
        Task<int> Create(ApiRequest<CreateSecProfileRequest> request, string username);

        Task<bool> Update(int securityId, string username, ApiRequest<UpdateSecProfileRequest> request);

        Task<bool> Delete(int id);

        Task<List<TbSecSecurityProfile>> GetAll(ApiRequest<PaginationRequest> request);

        Task<TbSecSecurityProfile> GetById(int id);

        bool CheckSecurityProfileById(int id);

        Task<List<SecurityProfileViewModel>> GetAllList(int clientId, int entityId);
        Task<List<SecurityProfileMngViewModel>> GetAllGroups(int securityProfileId);
        Task<bool> CheckDataAccessIdIsExist(int DataAccessGroupId);
        Task<bool> CheckRoleGroupIdIsExist(int RoleGroupId);
        Task<bool> CheckSecurityProfileIsExist(int SecurityProfileId);
        Task<bool> Save(CreateSecProfileRequest request, string username);
        Task<List<SecurityProfileViewModel>> GetUserSecurityProfile(int userId, int clientId, int entityId);
    }
}