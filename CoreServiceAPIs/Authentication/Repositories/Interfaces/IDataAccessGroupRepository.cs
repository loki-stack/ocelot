using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Common.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Repositories.Interfaces
{
    /// <summary>
    /// </summary>
    public interface IDataAccessGroupRepository
    {
        /// <summary>
        /// Get Dar List
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        Task<List<DataAccessGroupViewModel>> ListAll(int clientId, int entityId);

        /// <summary>
        /// Quick Create Dar Group
        /// </summary>
        /// <param name="request"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        Task<int> QuickCreate(ApiRequest<CreateDataAccessGroupRequest> request, string username);

        /// <summary>
        /// Get Dars by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="username"></param>
        /// <param name="clientInfo"></param>
        /// <returns></returns>
        Task<DataAccessGroupViewModel> GetByDataAccessId(int id, string username, ClientResult clientInfo);

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="request"></param>
        /// <param name="clientId"></param>
        /// <param name="entityId"></param>
        Task<bool> Update(ApiRequest<UpdateDataAccessGroupRequest> request, string username, int clientId, int entityId);

        /// <summary>
        /// Get Distinct Parent Code-Level Pairs' list
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="entityId"></param>
        /// <param name="dataAccessGroupId"></param>
        /// <returns></returns>
        Task<List<TreeNodeDto<DarClientEntityDto>>> GetTree(int dataAccessGroupId, int clientId, int entityId);

        /// <summary>
        /// Check Dar by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool CheckDataAccessGroupById(int id);
        //Task<List<DataAccessClientEntityTreeDto>> GetClientEntityTree (List<DataAccessClientEntityTreeDto> tree, int dataAccessId);
        /// <summary>
        /// Check if All Entities is selected
        /// </summary>
        /// <param name="dataAccessGroupId"></param>
        /// <returns></returns>
        Task<bool> IsAllEntities(int dataAccessGroupId);

        /// <summary>
        /// Get Organization Structure by dar's id
        /// </summary>
        /// <param name="dataAccessGroupId"></param>
        /// <returns></returns>
        Task<List<DataAccessOrganizationStructureDto>> GetOrganizationStructure(int dataAccessGroupId);
    }
}