﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Dtos;

namespace CoreServiceAPIs.Authentication.Repositories.Interfaces
{
    public interface IFormulasRepository
    {
        Task<List<FormulasResult>> GetAll(int level, int clientId, int entityId);
        Task<FormulasResult> GetById(int formulasId, int clientId, int entityId);
        Task<FormulasResult> GetById(int formulasId);
        Task<int?> Update(int formulasId, FormulasUpdateModel request);
        Task<int> Add(FormulasAddModel request, int clientId, int entityId);

        Task<List<FormulasTagViewResult>> GetFormulasTag();
        Task<FormulasTagViewResult> GetFormulasTagById(int formulasTagId);

        Task<int> AddFormulasTag(FormulasTagAddModel request);
        Task<int?> UpdateFormulasTag(int formulasTagId, FormulasTagAddModel request);
        Task<FormulasTagViewResult> GetFormulasTagFromFormulasId(int formulasId);
    }
}
