﻿using CommonLib.Dtos;
using CoreServiceAPIs.Common.Dtos;
using CoreServiceAPIs.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Repositories.Interfaces
{
    public interface IGenericCodeRepository
    {
        Task<Dictionary<string, List<GenericCodeResult>>> GetDDLGenericCodes(int clientId, int entityId, string locale,
    List<string> genericCodes);

        Task<List<ModuleGenericCodeViewModel>> GetModules(int clientId, int entityId, string locale);

        Task<List<GenericCodeViewModel>> GetFilterByModule(int clientId, int entityId, string locale, string codeType);

        Task<GenericCodeViewModel> GetById(int id, string locale);

        Task<CreateObjectResult<GenericCodeViewModel>> Create(string locale, int clientId, int entityId, CreateGenericCodeRequest request);

        Task<UpdateObjectResult<GenericCodeViewModel>> Update(string locale, UpdateGenericCodeRequest request);

        bool CheckExistGenericCodeById(int id);

        bool CheckExistGenericCode(int parentCodeId, int clientId, int entityId, int moduleId, int codeId, string codeValue);

        bool CheckExistI18N(int parentCodeId, int clientId, int entityId, string locale, string codeValue, int codeId, int moduleId);

        Task<List<GenericCodeViewModel>> GetTreeGenericCodes(int clientId, int entityId, string locale);

        Task<Dictionary<string, List<GenericCodeResult>>> GetGenericCodes(int clientId, int entityId, string locale,
    List<string> genericCodes);

        Task<List<GenericCodeViewModel>> GetParent(string locale);
    }
}