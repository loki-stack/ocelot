﻿using CommonLib.Models;
using CommonLib.Models.Core;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Common.Constants;
using CoreServiceAPIs.Common.Dtos;
using CoreServiceAPIs.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Repositories.Interfaces
{
    public interface ISettingRepository
    {
        Task<List<LanguageModel>> GetLanguages();

        Task<TbCfgSetting> GetSettingOfClientEntity(int clientId, int entityId);

        Task<TbCfgSetting> CreateDefaultSetting(int clientId, int entityId);

        Task UpdatePDFSetting(int clientId, int entityId, bool pdfStatus);

        Task UpdateLanguageSetting(int clientId, int entityId, string primaryLanguageCode, string secondaryLanguageCode);

        Task UpdateProbationPeriodSetting(int clientId, int entityId, int probationPeriod);

        Task UpdateCurrencySetting(int clientId, int entityId, string salaryCurrency, string paymentCurrency);

        Task UpdateWorkCalendarSetting(int clientId, int entityId, int workCalendar);

        Task UpdateEmployeeSetting(int clientId, int entityId, SettingUpdateEmployeeRequest request);

        Task UpdateSignInMethod(int clientId, int entityId, SignInMethod signInMethod);

        Task UpdateNotificationEmail(int clientId, int entityId, string notificationEmail);
    }
}