﻿using CommonLib.Models;
using CommonLib.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Repositories.Interfaces
{
    public interface IGlobalCalendarRepository
    {
        Task<int> Create(TbCfgGlobalCalendar globalCalendar);

        Task<int> Update(TbCfgGlobalCalendar globalCalendar);

        Task<TbCfgGlobalCalendar> GetById(int id);

        bool CheckExistGlobalCalendarById(int id);

        Task<List<TbCfgGlobalCalendar>> GetFilter();

        Task<bool> Delete(int id);

        bool CheckExistCalendarNameOfCountry(string calendarName, int calendarId);

        Task<bool> CheckLinkClientEntity(int calendarId);

        Task<bool> CheckExistGlobalActiveOfCountry(string country, int calendarId);
    }
}