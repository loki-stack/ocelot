﻿using CommonLib.Dtos;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Repositories.Interfaces
{
    public interface IMenuRepository
    {
        Task<Dictionary<string, IList>> GetMenu(int userId, ApiRequest request);
    }
}