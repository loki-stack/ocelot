﻿using CommonLib.Models;
using CommonLib.Models.Core;
using CoreServiceAPIs.Authentication.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Repositories.Interfaces
{
    public interface IUserSecurityProfileRepository
    {
        Task<List<UserSecurityProfileViewModel>> GetAllUserSecurityProfile(int userId);
        Task<TbSecUserSecurityProfile> GetUserSecurityProfile(int userId, int securityProfileId);
        Task<TbSecUserSecurityProfile> CreateUserSecurityProfile(int userId, int securityProfileId);
        Task<bool> DeleteUserSecurityProfile(int userId, int securityProfileId);
        Task<bool> Update(UserSecurityProfileDto request, string username, int userId);
    }
}
