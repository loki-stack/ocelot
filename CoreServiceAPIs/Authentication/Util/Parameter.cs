﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Util
{
    public class Parameter
    {
        public static string HandleParameter(string parameter)
        {
            string str_parameter = string.Empty;
            var pattern = @"\[\#";
            var tagParameter = Regex.Matches(parameter, pattern).Cast<Match>().Select(m => m.Value).ToList();
            if(tagParameter == null || tagParameter.Count == 0)
            {
                str_parameter = "[#" + parameter;
            }else
            {
                str_parameter = parameter;
            }
            string par_comp = str_parameter[str_parameter.Length - 1].ToString();
            if (!par_comp.Equals("]"))
            {
                str_parameter = str_parameter + ']';
            }
            return str_parameter;
        }
    }
}
