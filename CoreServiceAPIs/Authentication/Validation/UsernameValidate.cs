﻿using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Common.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CoreServiceAPIs.Authentication.Validation
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class UsernameValidate : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var userRepository = validationContext.GetService<IUserRepository>();

            var instance = validationContext.ObjectInstance as UserPortalAccessRequest;

            if (!string.IsNullOrEmpty(instance.Username) && userRepository.CheckExistUsername(instance.ClientId.Value, instance.UserId.Value, instance.Username))
            {
                return new ValidationResult(this.ErrorMessage, new List<string>() { validationContext.MemberName });
            }
            return ValidationResult.Success;
        }
    }
}