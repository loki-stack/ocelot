﻿using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Common.Dtos;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CoreServiceAPIs.Authentication.Validation
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class UniqueGenericCodeValidate : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var clientRepository = validationContext.GetService<IGenericCodeRepository>();

            var instance = validationContext.ObjectInstance as CreateGenericCodeRequest;

            if (clientRepository.CheckExistGenericCode(0, 0, 0, 0, 0, ""))
            {
                return new ValidationResult(this.ErrorMessage, new List<string>());
            }
            return ValidationResult.Success;
        }
    }
}
