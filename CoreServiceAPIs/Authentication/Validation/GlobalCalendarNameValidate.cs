﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using CoreServiceAPIs.Authentication.Dtos.GlobalCalendar;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace CoreServiceAPIs.Authentication.Validation
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class GlobalCalendarNameValidate : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var calendarRepository = validationContext.GetService<IGlobalCalendarRepository>();

            var instance = validationContext.ObjectInstance as GlobalCalendarModel;

            if (calendarRepository.CheckExistCalendarNameOfCountry(value.ToString(), instance.GlobalCalendarId))
            {
                return new ValidationResult(this.ErrorMessage, new List<string>() { validationContext.MemberName });
            }
            return ValidationResult.Success;
        }
    }
}