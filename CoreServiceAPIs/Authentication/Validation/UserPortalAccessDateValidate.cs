﻿using CoreServiceAPIs.Authentication.Dtos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CoreServiceAPIs.Authentication.Validation
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class UserPortalAccessDateValidate : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var instance = validationContext.ObjectInstance as UserPortalAccessRequest;

            if (instance.StartDate != null && instance.EndDate != null
                && (instance.StartDate.Value.Date.CompareTo(instance.EndDate.Value) > 0))
            {
                return new ValidationResult(this.ErrorMessage, new List<string>() { validationContext.MemberName });
            }
            return ValidationResult.Success;
        }
    }
}