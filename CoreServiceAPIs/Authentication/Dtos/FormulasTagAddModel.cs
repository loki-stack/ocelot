﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Dtos
{
    public class FormulasTagAddModel
    {
        public byte? Flag { get; set; }
        public string TagName { get; set; }
        public string Remark { get; set; }
        public int? FormulasId { get; set; }
        public int? FormulasRepeat { get; set; }
    }
}
