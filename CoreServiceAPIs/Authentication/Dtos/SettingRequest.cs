﻿using CoreServiceAPIs.Common.Constants;

namespace CoreServiceAPIs.Authentication.Dtos
{
    public class SettingUpdatePDFRequest
    {
        public bool PDFStatus { get; set; }
    }

    public class SettingUpdateLanguageRequest
    {
        public string PrimaryLanguageCode { get; set; }
        public string SecondaryLanguageCode { get; set; }
    }

    public class SettingUpdateProbationPeriodRequest
    {
        public int ProbationPeriod { get; set; }
    }

    public class SettingUpdateCurrencyRequest
    {
        public string SalaryCurrency { get; set; }
        public string PaymentCurrency { get; set; }
    }

    public class SettingUpdateWorkCalendarRequest
    {
        public int WorkCalendar { get; set; }
    }

    public class SettingUpdateEmployeeRequest
    {
        public int ProbationPeriod { get; set; }
        public string SalaryCurrency { get; set; }
        public string PaymentCurrency { get; set; }
        public int WorkCalendar { get; set; }
    }

    public class SettingSignInMethodRequest
    {
        public SignInMethod SignInMethod { get; set; }
    }

    public class SettingNotificationEmailRequest
    {
        public string NotificationEmail { get; set; }
    }
}