namespace CoreServiceAPIs.Authentication.Dtos
{
    public class ForgotPasswordDto
    {
        public bool ResetStatus { get; set; }
        public string TempLink { get; set; }
    }
}