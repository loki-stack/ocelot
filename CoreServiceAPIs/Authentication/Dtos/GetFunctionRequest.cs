﻿using System;

namespace CoreServiceAPIs.Authentication.Dtos
{
    public class GetFunctionRequest
    {
        public DateTime? CreatedDateFrom { get; set; }
        public DateTime? CreatedDateTo { get; set; }
    }
}