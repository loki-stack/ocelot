using CoreServiceAPIs.Common.Dtos;
using System.Collections.Generic;

namespace CoreServiceAPIs.Authentication.Dtos
{
    public class SecProfileGroupsDto
    {
        public SecProfileGroupsDto()
        {
            SecurityProfileGroups = new List<SecurityProfileMngViewModel>();
        }
        public List<SecurityProfileMngViewModel> SecurityProfileGroups { get; set; }
    }
}