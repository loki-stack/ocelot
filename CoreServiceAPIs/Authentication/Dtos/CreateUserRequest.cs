﻿using System.Text.Json.Serialization;

namespace CoreServiceAPIs.Authentication.Dtos
{
    public class CreateUserRequest
    {
        public string SSOId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        // public string Avatar { get; set; }
        public byte[] Avatar { get; set; }
        public string Fullname { get; set; }
        public int EntityId { get; set; }
        public int RoleId { get; set; }
        public int LanguageId { get; set; }
        public string ClientName { get; set; }
        [JsonIgnore]
        public string CreatedBy { get; set; }
    }
}
