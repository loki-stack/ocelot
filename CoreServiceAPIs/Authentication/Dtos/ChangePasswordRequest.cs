﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Dtos
{
    public class ChangePasswordRequest
    {
        public string Password { get; set; }
        public string PasswordConfirm { get; set; }
    }
}
