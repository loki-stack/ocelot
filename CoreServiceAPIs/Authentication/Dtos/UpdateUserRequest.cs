﻿using System.Text.Json.Serialization;

namespace CoreServiceAPIs.Authentication.Dtos
{
    public class UpdateUserRequest
    {
        [JsonIgnore]
        public int UserId { get; set; }
        public string Email { get; set; }
        public string Fullname { get; set; }
        public bool IsUpdatePassword { get; set; }
        public string Password { get; set; }
        public string StatusCD { get; set; }
        [JsonIgnore]
        public string ModifiedBy { get; set; }
        
    }
}
