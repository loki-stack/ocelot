﻿using CommonLib.Validation;
using CoreServiceAPIs.Common.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Dtos.HolidayCalendar
{
    public class HolidayModel
    {
        public int HolidayDateId { get; set; }

        [Required(ErrorMessage = Label.HLD0101)]
        [ExtraResult(ComponentId = Component.DATE, Level = Level.ERROR, Placeholder = new string[] { })]
        public DateTime? Date { get; set; }

        [Required(ErrorMessage = Label.HLD0102)]
        [MaxLength(400, ErrorMessage = Label.HLD0106)]
        [ExtraResult(ComponentId = Component.DESCRIPTION_PRIMARY_LANGUAGE, Level = Level.ERROR, Placeholder = new string[] { })]
        public string DescriptionPrimaryLanguage { get; set; }

        public string DescriptionSecondaryLanguage { get; set; }

        [Required(ErrorMessage = Label.HLD0103)]
        [MaxLength(20, ErrorMessage = Label.HLD0107)]
        [ExtraResult(ComponentId = Component.HOLIDAY_TYPE, Level = Level.ERROR, Placeholder = new string[] { })]
        public string HolidayType { get; set; }

        public int? CalendarId { get; set; }

        [Required(ErrorMessage = Label.HLD0104)]
        [ExtraResult(ComponentId = Component.IS_WORKING_DAY, Level = Level.ERROR, Placeholder = new string[] { })]
        public bool? IsWorkingDay { get; set; }

        [JsonIgnore]
        public Guid GuidId { get; set; } = Guid.NewGuid();

        [JsonIgnore]
        public string CreatedBy { get; set; }

        [JsonIgnore]
        public DateTimeOffset? CreatedDt { get; set; }

        [JsonIgnore]
        public string ModifiedBy { get; set; }

        [JsonIgnore]
        public DateTimeOffset? ModifiedDt { get; set; }
    }
}