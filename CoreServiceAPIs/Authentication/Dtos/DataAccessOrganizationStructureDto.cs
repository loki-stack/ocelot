namespace CoreServiceAPIs.Authentication.Dtos
{
    public class DataAccessOrganizationStructureDto
    {
        public int DataAccessGroupId { get; set; }
        public string NodeLevel { get; set; }
    }
}