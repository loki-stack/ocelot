﻿using System.Text.Json.Serialization;

namespace CoreServiceAPIs.Authentication.Dtos
{
    public class ChangeClientRequest
    {
        [JsonIgnore]
        public string Token { get; set; }
        public int ClientId { get; set; }
        public int ClientEntityId { get; set; }
    }
}