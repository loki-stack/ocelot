using CoreServiceAPIs.Common.Dtos;
using System.Collections.Generic;

namespace CoreServiceAPIs.Authentication.Dtos
{
    public class SecProfileMngDto
    {
        public SecProfileMngDto()
        {
            SecurityProfiles = new List<SecurityProfileViewModel>();
        }
        public List<SecurityProfileViewModel> SecurityProfiles { get; set; }
    }
}