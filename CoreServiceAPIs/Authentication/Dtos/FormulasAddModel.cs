﻿using CommonLib.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Dtos
{
    public class FormulasAddModel
    {
        [JsonIgnore]
        public int FormulasId { get; set; }
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        [RegularExpression("1|2|3", ErrorMessage = "The Level must be either '1' or '2' or '3' only.")]
        [ExtraResult(ComponentId = "Level", Level = Common.Constants.Level.ERROR, Placeholder = new string[] { })]
        public byte Level { get; set; }
        [Required]
        [RegularExpression("INACTIVE|ACTIVE", ErrorMessage = "The StatusCd must be either 'INACTIVE' or 'ACTIVE' only.")]
        [MaxLength(10)]
        [ExtraResult(ComponentId = "StatusCd", Level = Common.Constants.Level.ERROR, Placeholder = new string[] { })]
        public string StatusCd { get; set; }
        public string Formulas { get; set; }
        public bool EditAble { get; set; }
    }
}
