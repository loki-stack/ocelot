namespace CoreServiceAPIs.Authentication.Dtos
{
    public class UserSecurityProfileUpdateDto
    {
        public int UserId { get; set; }
        public int SecurityProfileId { get; set; }
        public string StartDt { get; set; }
        public string? EndDt { get; set; }
    }
}