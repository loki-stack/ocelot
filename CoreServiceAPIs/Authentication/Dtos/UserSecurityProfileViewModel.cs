﻿using CoreServiceAPIs.Common.Dtos;
using System;
using System.Collections.Generic;

namespace CoreServiceAPIs.Authentication.Dtos
{
    public class UserSecurityProfileViewModel
    {
        public int UserId { get; set; }
        public string FullName { get; set; }
        public int SecurityProfileId { get; set; }
        public string SecurityProfileName { get; set; }
        public string SecurityProfileDescriptionTxt { get; set; }
        public DateTimeOffset StartDt { get; set; }
        public DateTimeOffset? EndDt { get; set; }
        public List<SecurityProfileViewModel> SecurityProfile { get; set; }
    }
}
