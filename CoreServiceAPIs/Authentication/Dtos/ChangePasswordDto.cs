namespace CoreServiceAPIs.Authentication.Dtos
{
    public class ChangePasswordDto
    {
        public bool IsAuthenticated { get; set; }
        public string CurrentPassword { get; set; }
        public string Username { get; set; }
        public int UserId { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
    }
}