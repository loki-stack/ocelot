﻿using System;

namespace CoreServiceAPIs.Authentication.Dtos
{
    public class GetUserRequest
    {
        public DateTime? CreatedDateFrom { get; set; }
        public DateTime? CreatedDateTo { get; set; }
        public DateTime? LastActiveFrom { get; set; }
        public DateTime? LastActiveTo { get; set; }
        public string StatusCD { get; set; }
    }
}
