﻿using CoreServiceAPIs.Authentication.Dtos.HolidayCalendar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Dtos.GlobalCalendar
{
    public class GlobalCalendarViewModel
    {
        public int GlobalCalendarId { get; set; }
        public string GlobalLevelCalendarName { get; set; }
        public string Country { get; set; }
        public string Status { get; set; }
        public string StatusName { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
        public List<HolidayViewModel> Holidays { get; set; }
    }
}