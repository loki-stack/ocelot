﻿using CommonLib.Validation;
using CoreServiceAPIs.Authentication.Validation;
using CoreServiceAPIs.Common.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Dtos.GlobalCalendar
{
    public class GlobalCalendarModel
    {
        public int GlobalCalendarId { get; set; } = 0;

        [Required(ErrorMessage = Label.WCL0101)]
        [MaxLength(200, ErrorMessage = Label.WCL0107)]
        [GlobalCalendarNameValidate(ErrorMessage = Label.GLCD0101)]
        [ExtraResult(ComponentId = Component.GLOBAL_LEVEL_CALENDAR_NAME, Level = Level.ERROR, Placeholder = new string[] { })]
        public string GlobalLevelCalendarName { get; set; }

        [Required(ErrorMessage = Label.WCL0102)]
        [MaxLength(20, ErrorMessage = Label.WCL0108)]
        [ExtraResult(ComponentId = Component.COUNTRY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string Country { get; set; }

        [Required(ErrorMessage = Label.WCL0103)]
        [MaxLength(20, ErrorMessage = Label.WCL0110)]
        [ExtraResult(ComponentId = Component.STATUS, Level = Level.ERROR, Placeholder = new string[] { })]
        public string Status { get; set; }

        [JsonIgnore]
        public string CreatedBy { get; set; }

        [JsonIgnore]
        public DateTimeOffset? CreatedDt { get; set; }

        [JsonIgnore]
        public string ModifiedBy { get; set; }

        [JsonIgnore]
        public DateTimeOffset? ModifiedDt { get; set; }
    }
}