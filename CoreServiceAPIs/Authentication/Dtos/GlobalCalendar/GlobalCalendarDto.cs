﻿using CoreServiceAPIs.Authentication.Dtos.HolidayCalendar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Dtos.GlobalCalendar
{
    public class GlobalCalendarDto
    {
        public GlobalCalendarModel GlobalCalendar { get; set; }
        public List<HolidayModel> Holidays { get; set; }
    }
}