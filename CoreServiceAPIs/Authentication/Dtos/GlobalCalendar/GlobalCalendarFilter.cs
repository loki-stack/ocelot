﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Dtos.GlobalCalendar
{
    public class GlobalCalendarFilter
    {
        public string Status { get; set; }
    }
}