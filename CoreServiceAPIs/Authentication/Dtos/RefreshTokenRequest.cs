﻿namespace CoreServiceAPIs.Authentication.Dtos
{
    public class RefreshTokenRequest
    {
        public string Token { get; set; }
        public string RefreshToken { get; set; }
    }
}
