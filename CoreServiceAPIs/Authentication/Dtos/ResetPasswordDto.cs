namespace CoreServiceAPIs.Authentication.Dtos
{
    public class ResetPasswordDto
    {
        public bool IsVerified { get; set; }
        public bool ResetStatus { get; set; }

        public string Token { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public string Email { get; set; }

    }
}