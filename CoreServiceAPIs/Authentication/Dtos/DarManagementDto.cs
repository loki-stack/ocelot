using CommonLib.Dtos;
using CoreServiceAPIs.Common.Dtos;
using System.Collections.Generic;

namespace CoreServiceAPIs.Authentication.Dtos
{
    public class DarManagementDto
    {
        public DarManagementDto()
        {
            AllDataAccessGroup = new List<DataAccessGroupViewModel>();
        }
        public DataAccessGroupViewModel SelectedDataAccessGroup { get; set; }

        public List<DataAccessGroupViewModel> AllDataAccessGroup { get; set; }

        public Dictionary<string, List<GenericCodeResult>> Remuneration { get; set; }

        public List<TreeNodeDto<DarClientEntityDto>> AllClientEntities { get; set; }
        public List<DataAccessOrganizationStructureDto> AllOrganizationStructures { get; set; }

        public bool IsSelectAllEntity { get; set; }

    }
}