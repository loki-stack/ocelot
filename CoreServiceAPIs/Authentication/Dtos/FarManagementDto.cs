using CoreServiceAPIs.Common.Dtos;
using System.Collections.Generic;

namespace CoreServiceAPIs.Authentication.Dtos
{
    public class FarManagementDto
    {
        public FarManagementDto()
        {
            AllRoleGroup = new List<RoleGroupViewModel>();
            SelectedFuncList = new List<TreeNodeDto<FuncItemDto>>();
            FuncList = new List<TreeNodeDto<FuncItemDto>>();
        }
        public RoleGroupViewModel SelectedRoleGroup { get; set; }

        public List<RoleGroupViewModel> AllRoleGroup { get; set; }

        public List<TreeNodeDto<FuncItemDto>> SelectedFuncList { get; set; }
        public List<TreeNodeDto<FuncItemDto>> FuncList { get; set; }

    }
}