﻿namespace CoreServiceAPIs.Authentication.Dtos
{
    public class UserSecurityProfileRequest
    {
        public int UserId { get; set; }
        public int SecurityProfileId { get; set; }
    }

    public class GetUserSecurityProfileRequest
    {
        public int UserId { get; set; }
    }
}
