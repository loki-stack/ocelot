namespace CoreServiceAPIs.Authentication.Dtos
{
    /// <summary>
    /// 
    /// </summary>
    public class DarClientEntityDto
    {
        public int ItemId { get; set; }
        public string ItemType { get; set; }
        public string ItemCode { get; set; }
        public string ItemText { get; set; }
        public bool IsSelected { get; set; }

        public string ItemLevel { get; set; }
        public int DataAccessGroupId { get; set; }



    }
}