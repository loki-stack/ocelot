﻿using CommonLib.Validation;
using CoreServiceAPIs.Authentication.Validation;
using CoreServiceAPIs.Common.Constants;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace CoreServiceAPIs.Authentication.Dtos
{
    public class UserPortalAccessRequest
    {
        public int EmployeeId { get; set; }

        [UserPortalAccessDateValidate(ErrorMessage = Label.USR0111)]
        [ExtraResult(ComponentId = Component.START_DATE, Level = Level.ERROR, Placeholder = new string[] { })]
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        [MaxLength(50, ErrorMessage = Label.USR0112)]
        [UsernameValidate(ErrorMessage = Label.USR0108)]
        [ExtraResult(ComponentId = Component.USERNAME, Level = Level.ERROR, Placeholder = new string[] { })]
        public string Username { get; set; }

        public string SingleSignOnId { get; set; }
        public string Email { get; set; }
        public bool? EssAccessFlag { get; set; }

        [JsonIgnore]
        public int? UserId { get; set; }

        [JsonIgnore]
        public int? ClientId { get; set; }
    }
}