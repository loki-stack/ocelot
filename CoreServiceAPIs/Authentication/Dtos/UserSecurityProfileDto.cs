using System.Collections.Generic;

namespace CoreServiceAPIs.Authentication.Dtos
{
    public class UserSecurityProfileDto
    {
        public string FullName { get; set; }
        public List<UserSecurityProfileUpdateDto> UserSecurityProfiles { get; set; }
    }
}