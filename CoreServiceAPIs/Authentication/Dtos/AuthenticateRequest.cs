﻿namespace CoreServiceAPIs.Authentication.Dtos
{
    public class AuthenticateRequest
    {
        public string Account { get; set; }
        public string Password { get; set; }
    }
}
