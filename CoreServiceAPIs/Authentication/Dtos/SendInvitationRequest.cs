﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Dtos
{
    public class SendInvitationRequest
    {
        public string UserName { get; set; }
        public string Email { get; set; }
    }
}
