﻿using System.Collections.Generic;

namespace CoreServiceAPIs.Authentication.Models
{
    public class ClientEntityRoleModel
    {
        public int RoleGroupId { get; set; }
        public string RoleGroupName { get; set; }
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public List<EntityRoleModel> EntityRoles { get; set; }
        public ClientEntityRoleModel()
        {
            EntityRoles = new List<EntityRoleModel>();
        }
    }

    public class EntityRoleModel
    {
        public int EntityId { get; set; }
        public string EntityName { get; set; }
        public string Description { get; set; }
        public bool IsCreate { get; set; }
        public bool IsUpdate { get; set; }
        public bool IsExport { get; set; }
    }
}
