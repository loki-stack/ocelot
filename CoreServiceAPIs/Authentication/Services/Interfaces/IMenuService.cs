﻿using CommonLib.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Services.Interfaces
{
    public interface IMenuService
    {
        Task<ApiResponse<Dictionary<string, object>>> GetMenu(int userId, ApiRequest request);
    }
}