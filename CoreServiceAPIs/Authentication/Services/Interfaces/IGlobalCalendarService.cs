﻿using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Dtos.GlobalCalendar;
using CoreServiceAPIs.Common.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Services.Interfaces
{
    public interface IGlobalCalendarService
    {
        Task<ApiResponse<Dictionary<string, object>>> GetFilter(int clientId, int entityId, string locale, string ray, PaginationRequest request);

        Task<ApiResponse<GlobalCalendarViewModel>> Save(string username, string ray, GlobalCalendarDto globalCalendarDto);

        Task<ApiResponse<GlobalCalendarViewModel>> GetById(int id, string ray);

        Task<ApiResponse<bool?>> DeleteGlobalCalendar(int id, string ray);

        Task<ApiResponse<bool?>> DeleteHoliday(int id, string ray);
    }
}