using CommonLib.Models;
using CommonLib.Models.Core;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Services.Interfaces
{
    public interface IEmailService
    {
        Task<bool> RegisterEmailRequest(TbSecUser user, int clientId, int entityId);
    }
}