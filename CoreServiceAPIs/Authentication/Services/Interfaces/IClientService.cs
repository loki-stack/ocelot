﻿using CommonLib.Dtos;

// using CoreServiceAPIs.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Services.Interfaces
{
    public interface IClientService
    {
        Task<ApiResponse<Dictionary<string, object>>> GetClientEntities(int userId, ApiRequest request);

        Task<ClientResult> GetClientEntityId(int clientId, int entityId);
        Task<ClientResult> GetClientEntityId(string clientCode, string entityCode);
    }
}