﻿using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Services.Interfaces
{
    public interface IFormulasService
    {
        Task<ApiResponse<List<FormulasResult>>> GetAll(ApiRequest request, int level);
        Task<ApiResponse<FormulasResult>> GetById(int formulasId, ApiRequest request);
        Task<ApiResponse<int>> Update(int formulasId, ApiRequest<FormulasUpdateModel> request);
        Task<ApiResponse<int>> Add(ApiRequest<FormulasAddModel> request);

        Task<ApiResponse<List<FormulasTagViewResult>>> GetAllFormulasTag(ApiRequest request);
        Task<ApiResponse<FormulasTagViewResult>> GetFormulasById(int formulasTagId, ApiRequest request);
    }
}
