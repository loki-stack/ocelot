﻿using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Common.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Services.Interfaces
{
    public interface IRoleGroupService
    {
        Task<ApiResponse<Dictionary<string, object>>> Create(ApiRequest<CreateRoleGroupRequest> request, string username);

        Task<ApiResponse<Dictionary<string, object>>> Update(ApiRequest<CreateRoleGroupRequest> request, string username);

        Task<ApiResponse<Dictionary<string, object>>> GetAll(ApiRequest<PaginationRequest> request);

        Task<ApiResponse<Dictionary<string, object>>> GetById(int id, ApiRequest request);

        Task<ApiResponse<Dictionary<string, object>>> Delete(int id, ApiRequest request);

        /// <summary>
        /// Get all functions as tree node format
        /// </summary>
        Task<List<TreeNodeDto<FuncItemDto>>> GetAllFuncAsTree(string locale, int clientId, int entityId);

        List<TreeNodeDto<FuncItemDto>> ToTreeNode(List<TbSecFunction> functions, Dictionary<string, string> dictFunc);

        Task<ApiResponse<FarManagementDto>> GetManageData(int id, string locale);

        Task<List<RoleGroupViewModel>> GetEntityRoleGroup(int clientId, int entityId);

    }
}