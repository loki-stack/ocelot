using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Common.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Services.Interfaces
{
    public interface IDataAccessGroupService
    {
        Task<List<DataAccessGroupViewModel>> ListAll(int clientId, int entityId);
        Task<ApiResponse<Dictionary<string, object>>> Create(ApiRequest<CreateDataAccessGroupRequest> request, string username);
        Task<ApiResponse<DarManagementDto>> GetDARById(int id, string username, ClientResult clientInfo);
        Task<ApiResponse<Dictionary<string, object>>> Update(ApiRequest<UpdateDataAccessGroupRequest> request, string username, int clientId, int entityId);
        Task<ApiResponse<DarManagementDto>> GetAllClientEntitiesAsTree(ApiRequest<int> request, ClientResult clientInfo);
    }
}