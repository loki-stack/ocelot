﻿using CommonLib.Dtos;
using CoreServiceAPIs.Common.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Services.Interfaces
{
    public interface IGenericCodeService
    {
        Task<ApiResponse<Dictionary<string, object>>> GetDDLGenericCodes(int clientId, int entityId, string locale, string ray, List<string> genericCodeList);

        Task<ApiResponse<Dictionary<string, object>>> GetModules(int clientId, int entityId, string ray, string locale);

        Task<ApiResponse<Dictionary<string, object>>> GetFilterByModule(ApiRequest<GetGenericCodeRequest> request);

        Task<ApiResponse<Dictionary<string, object>>> GetById(int id, ApiRequest request);

        Task<ApiResponse<Dictionary<string, object>>> Create(ApiRequest<CreateGenericCodeRequest> request);

        Task<ApiResponse<Dictionary<string, object>>> Update(ApiRequest<UpdateGenericCodeRequest> request);

        Task<ApiResponse<Dictionary<string, object>>> GetTreeGenericCodes(int clientId, int entityId, string ray, string locale);

        /// <summary>
        /// Overloaded method to return generic codes in dictionary for more flexibility
        /// </summary>
        Task<Dictionary<string, List<GenericCodeResult>>> GetGenericCodes(int clientId, int entityId, string locale,
            List<string> genericCodes);
    }
}