﻿using CommonLib.Dtos;
using CoreServiceAPIs.Common.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Services.Interfaces
{
    public interface ISecurityProfileService
    {
        Task<ApiResponse<Dictionary<string, object>>> Create(ApiRequest<CreateSecProfileRequest> request, string username);

        Task<ApiResponse<Dictionary<string, object>>> Update(int securityId, string username, ApiRequest<UpdateSecProfileRequest> request);

        Task<ApiResponse<Dictionary<string, object>>> GetAll(ApiRequest<PaginationRequest> request);

        Task<ApiResponse<Dictionary<string, object>>> GetById(int id, ApiRequest request);

        Task<ApiResponse<Dictionary<string, object>>> Delete(int id, ApiRequest request);

        List<CommonLib.Dtos.MessageItem> ValidateSecurityProfile(UpdateSecProfileRequest request);

        Task<List<SecurityProfileViewModel>> GetAllList(int clientId, int entityId);
        Task<List<SecurityProfileMngViewModel>> GetAllGroups(ApiRequest<int> request);
        Task<ApiResponse<bool>> Save(CreateSecProfileRequest request, string username);
    }
}