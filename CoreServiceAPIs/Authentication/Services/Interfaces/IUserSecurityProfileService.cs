﻿using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Services.Interfaces
{
    public interface IUserSecurityProfileService
    {
        Task<ApiResponse<Dictionary<string, object>>> GetUserSecurityProfile(ApiRequest<GetUserSecurityProfileRequest> request);
        Task<ApiResponse<Dictionary<string, object>>> CreateUserSecurityProfile(ApiRequest<UserSecurityProfileRequest> request);
        Task<ApiResponse<string>> DeleteUserSecurityProfile(ApiRequest<UserSecurityProfileRequest> request);
        Task<ApiResponse<Dictionary<string, object>>> Update(ApiRequest<UserSecurityProfileDto> request, string username, int userId);
    }
}