﻿using CommonLib.Dtos;
using CoreServiceAPIs.Common.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Services.Interfaces
{
    public interface IFunctionService
    {
        Task<ApiResponse<Dictionary<string, object>>> GetAll(ApiRequest<PaginationRequest> request);

        Task<ApiResponse<Dictionary<string, object>>> GetById(int id, ApiRequest request);

        Task<ApiResponse<Dictionary<string, object>>> Create(ApiRequest<CreateFuncRequest> request, string username);

        Task<ApiResponse<Dictionary<string, object>>> Update(ApiRequest<UpdateFuncRequest> request, string username);

        Task<ApiResponse<Dictionary<string, object>>> Delete(int id, ApiRequest request);


    }
}