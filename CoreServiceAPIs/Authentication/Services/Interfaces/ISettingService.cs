﻿using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Services.Interfaces
{
    public interface ISettingService
    {
        Task<ApiResponse<Dictionary<string, object>>> GetEntitySetting(ApiRequest request);

        Task<ApiResponse<Dictionary<string, object>>> GetLanguages(ApiRequest<string> request);

        Task<ApiResponse<Dictionary<string, object>>> GetPDFSetting(ApiRequest request);

        Task<ApiResponse<Dictionary<string, object>>> GetLanguageSetting(ApiRequest request);

        Task<ApiResponse<Dictionary<string, object>>> GetProbationPeriodSetting(ApiRequest request);

        Task<ApiResponse<Dictionary<string, object>>> GetCurrencySetting(ApiRequest request);

        Task<ApiResponse<Dictionary<string, object>>> GetWorkCalendarSetting(ApiRequest request);

        Task<ApiResponse<Dictionary<string, object>>> GetEmployeeSetting(ApiRequest request);

        Task<ApiResponse<Dictionary<string, object>>> GetSignInSetting(ApiRequest request);

        Task<ApiResponse<Dictionary<string, object>>> GetNotificationEmailSetting(ApiRequest request);

        Task<ApiResponse<string>> UpdatePDFSetting(ApiRequest<SettingUpdatePDFRequest> request);

        Task<ApiResponse<string>> UpdateLanguageSetting(ApiRequest<SettingUpdateLanguageRequest> request);

        Task<ApiResponse<string>> UpdateProbationPeriodSetting(ApiRequest<SettingUpdateProbationPeriodRequest> request);

        Task<ApiResponse<string>> UpdateCurrencySetting(ApiRequest<SettingUpdateCurrencyRequest> request);

        Task<ApiResponse<string>> UpdateWorkCaldendarSetting(ApiRequest<SettingUpdateWorkCalendarRequest> request);

        Task<ApiResponse<string>> UpdateEmployeeSetting(ApiRequest<SettingUpdateEmployeeRequest> request);

        Task<ApiResponse<string>> UpdateSignInMethodSetting(ApiRequest<SettingSignInMethodRequest> request);
        Task<ApiResponse<string>> UpdateNotificationEmailSetting(ApiRequest<SettingNotificationEmailRequest> request);
    }
}