﻿using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Common.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Services.Interfaces
{
    public interface IUserService
    {
        Task<TbSecUser> CheckUserExists(string username);

        Task<ApiResponse<Dictionary<string, object>>> GetById(ApiRequest<int> request);

        Task<ApiResponse<Dictionary<string, object>>> GetAll(ApiRequest<PaginationRequest> request);

        Task<ApiResponse<int?>> CreateUser(ApiRequest<CreateUserRequest> request);

        Task<ApiResponse<int?>> UpdateUser(ApiRequest<UpdateUserRequest> request);

        /// <summary>
        /// Forgot password handling - mark status as reset, system will email user with links
        /// to reset the password
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<ApiResponse<ForgotPasswordDto>> MarkReset(ApiRequest<string> request);

        /// <summary>
        /// Verify token before allow user to reset password
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<ApiResponse<ResetPasswordDto>> VerifyToken(ApiRequest<string> request);

        /// <summary>
        /// Reset password - required token, new password, confirm password
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<ApiResponse<ResetPasswordDto>> ResetPassword(ApiRequest<ResetPasswordDto> request);

        /// <summary>
        /// Change password - required current password, new password, confirm password
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<ApiResponse<Dictionary<string, object>>> ChangePassword(ApiRequest<ChangePasswordDto> request);

        Task<ApiResponse<int?>> UpdatePortalAccess(int clientId, int entityId, string username, string ray, UserPortalAccessRequest userPortal);

        /// <summary>
        /// Create password for user first login.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<ApiResponse<ResetPasswordDto>> FirstLogin(ApiRequest<ResetPasswordDto> request);

        Task<ApiResponse<UserViewModel>> GetUserByEmployeeId(int clientId, int entityId, int employeeId, string ray);
        Task<ApiResponse<bool>> SendInvitation(ApiRequest<SendInvitationRequest> request, int userId, string userName);
    }
}