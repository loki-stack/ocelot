﻿using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Dtos;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Services.Interfaces
{
    public interface IJwtAuthenticationService
    {
        Task<ApiResponse<Dictionary<string, object>>> Authenticate(ApiRequest<AuthenticateRequest> request);
        Task<ApiResponse<Dictionary<string, object>>> EssAuthenticate(ApiRequest<AuthenticateRequest> request);

        Task<ApiResponse<Dictionary<string, object>>> RefreshToken(ApiRequest<RefreshTokenRequest> request);

        ClaimsPrincipal GetPrincipalFromToken(string token);
    }
}