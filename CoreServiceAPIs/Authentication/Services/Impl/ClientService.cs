﻿using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using CoreServiceAPIs.Common.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Services.Impl
{
    public class ClientService : IClientService
    {
        private readonly IClientRepository _clientRepository;

        public ClientService(IClientRepository clientRepository)
        {
            _clientRepository = clientRepository;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetClientEntities(int userId, ApiRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray != null ? request.Ray : string.Empty;
            response.Data = new Dictionary<string, object>();
            var clients = await _clientRepository.GetClientEntities(userId);
            var result = clients.GroupBy(x => new { x.ClientId, x.ClientLevel, x.ClientCode, x.ClientName })
                                 .Select(x => new ClientViewModel
                                 {
                                     ClientId = x.Key.ClientId,
                                     ClientLevel = x.Key.ClientLevel,
                                     ClientCode = x.Key.ClientCode,
                                     ClientName = x.Key.ClientName,
                                     IsSelected = false,
                                     Entities = x.Select(y => new ClientEntityViewModel
                                     {
                                         EntityId = y.EntityId,
                                         EntityLevel = y.EntityLevel,
                                         EntityCode = y.EntityCode,
                                         EntityName = y.EntityName
                                     }).ToList()
                                 }).ToList();

            foreach (var item in result)
            {
                if (item.Entities.Any(x => x.EntityId == 0))
                {
                    item.IsSelected = true;
                    item.Entities.Remove(item.Entities.FirstOrDefault(x => x.EntityId == 0));
                }
            }

            Dictionary<string, List<ClientViewModel>> dic = new Dictionary<string, List<ClientViewModel>>();
            response.Data.Add("clients", result);
            return response;
        }

        public async Task<ClientResult> GetClientEntityId(int clientId, int entityId)
        {
            return await _clientRepository.GetClientEntityId(clientId, entityId);
        }

        public async Task<ClientResult> GetClientEntityId(string clientCode, string entityCode)
        {
            return await _clientRepository.GetClientEntityId(clientCode, entityCode);
        }
    }
}