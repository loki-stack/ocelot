﻿using CommonLib.Dtos;
using CommonLib.EmailTemplate;
﻿using Antlr4.StringTemplate;
using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using CommonLib.Repositories.Interfaces;
using CommonLib.Services;
using CommonLib.Utils;
using CommonLib.Validation;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using CoreServiceAPIs.Common.Constants;
using CoreServiceAPIs.Common.Dtos;
using CoreServiceAPIs.Common.Email.Dtos;
using CoreServiceAPIs.Common.Interfaces;
using CoreServiceAPIs.Common.Models;
using CoreServiceAPIs.Common.ShardingUtil;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Services.Impl
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IValidator _validator;
        private readonly SmtpSettings _smtpSettings;
        private readonly MailSettings _mailSettings;
        private readonly ILogger<UserService> _logger;
        private readonly IEmailService _emailService;
        private readonly ISendEmailService _sendEmailService;
        private readonly IOptions<Common.Models.AppSettings> _appSetting;
        private readonly IClientRepository _clientRepository;

        public UserService(IUserRepository userRepository,
                           IValidator validator,
                           IOptions<SmtpSettings> smtpSettings,
                           IOptions<MailSettings> mailSettings,
                           IEmailService emailService,
                           ISendEmailService sendEmailService,
                           IOptions<Common.Models.AppSettings> appSetting,
                           ILogger<UserService> logger,
                           IClientRepository clientRepository)
        {
            _userRepository = userRepository;
            _validator = validator;
            _smtpSettings = smtpSettings.Value;
            _mailSettings = mailSettings.Value;
            _emailService = emailService;
            _appSetting = appSetting;
            _logger = logger;
            _clientRepository = clientRepository;
            _sendEmailService = sendEmailService;
        }

        public async Task<TbSecUser> CheckUserExists(string username)
        {
            _logger.LogInformation($"CheckUserExists --- Username: {username}");
            return await _userRepository.GetUserByUserName(username);
        }

        public async Task<ApiResponse<int?>> CreateUser(ApiRequest<CreateUserRequest> request)
        {
            _logger.LogInformation("CreateUser");
            ApiResponse<int?> response = new ApiResponse<int?>();
            response.Ray = request.Ray;
            CreateObjectResult result = await _userRepository.CreateUser(request.Data, request.Parameter);
            if (!result.IsSuccess)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.USR0103
                });
                response.Data = null;
                return response;
            }
            response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.USR0104
            });
            response.Data = result.Id;
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetAll(ApiRequest<PaginationRequest> request)
        {
            _logger.LogInformation("GetAllUser");
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();
            List<UserViewModel> listUser = await _userRepository.GetAll(request.Data, request.Parameter.ClientId);
            Pagination<UserViewModel> pagination = new Pagination<UserViewModel>();
            if (request.Data.Size.HasValue)
            {
                pagination.Page = request.Data.Page.Value;
                pagination.Size = request.Data.Size.Value;
                pagination.TotalPages = (listUser.Count + request.Data.Size.Value - 1) / request.Data.Size.Value;
                pagination.TotalElements = listUser.Count;

                List<UserViewModel> users = new List<UserViewModel>();
                users = listUser.Skip((request.Data.Page.Value - 1) * request.Data.Size.Value).Take(request.Data.Size.Value).ToList();
                pagination.NumberOfElements = users.Count;
                pagination.Content = users;
            }
            else
            {
                pagination.TotalElements = listUser.Count;
                pagination.Content = listUser;
            }

            response.Data.Add("pagination", pagination);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetById(ApiRequest<int> request)
        {
            _logger.LogInformation("GetUserById");
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();
            UserViewModel user = await _userRepository.GetUserInformation(request.Data);
            if (user == null)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.USR0102
                });
                response.Data = null;
                return response;
            }
            response.Data.Add("user", user);
            return response;
        }

        public async Task<ApiResponse<int?>> UpdateUser(ApiRequest<UpdateUserRequest> request)
        {
            _logger.LogInformation("UpdateUser");
            ApiResponse<int?> response = new ApiResponse<int?>();
            response.Ray = request.Ray;
            UpdateObjectResult result = await _userRepository.UpdateUser(request.Data);
            if (!result.IsSuccess)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.USR0105
                });
                response.Data = null;
                return response;
            }
            response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.USR0106
            });
            response.Data = request.Data.UserId;
            return response;
        }

        public async Task<ApiResponse<ForgotPasswordDto>> MarkReset(ApiRequest<string> request)
        {
            _logger.LogInformation("MarkReset");
            ApiResponse<ForgotPasswordDto> response = new ApiResponse<ForgotPasswordDto>();
            ForgotPasswordDto data = new ForgotPasswordDto();
            response.Ray = request.Ray;

            string userLoginName = request.Data;

            // try with email first
            TbSecUser user = await _userRepository.GetUserByEmail(userLoginName);
            if (user == null)
            {
                // try with user name
                user = await _userRepository.GetUserByUserName(userLoginName);
            }

            if (user != null)
            {
                // check if user entity is matched
                if (user.EntityId != request.Parameter.EntityId)
                {
                    MessageUtil.AddToast(response, Level.ERROR, Label.ESS50_E_001); // user not exists for entity
                    return response;
                }

                Guid guid = Guid.NewGuid();
                string gstr = guid.ToString();
                string md5 = Utils.MD5Hash(user.UserName, gstr);
                string token = md5 + gstr.Substring(gstr.Length - 12);
                //
                user.ResetPwdToken = token;
                user.ResetPwdExpiryDt = DateTimeOffset.Now.AddDays(2); // expired in 2 days
                user.ModifiedDt = DateTime.Now;
                user.ModifiedBy = userLoginName;

                await _userRepository.Update(user);
                // TODO -- email user about the links to reset password

                // response data
                data.ResetStatus = true;
                data.TempLink = user.ResetPwdToken; // tobe remove - for testing use only

                MessageUtil.AddToast(response, Level.INFO, Label.ESS50_I_001); // marked reset
                response.Data = data;

                // await RegisterEmailRequest(user, request.Parameter.ClientId, request.Parameter.EntityId);
                await _emailService.RegisterEmailRequest(user, request.Parameter.ClientId, request.Parameter.EntityId);
            }
            else
            {
                data.ResetStatus = false;
                MessageUtil.AddToast(response, Level.ERROR, Label.ESS50_E_001); // user not exists for entity
            }
            return response;
        }

        public async Task<ApiResponse<ResetPasswordDto>> VerifyToken(ApiRequest<string> request)
        {
            _logger.LogInformation("VerifyToken");
            ApiResponse<ResetPasswordDto> response = new ApiResponse<ResetPasswordDto>();
            ResetPasswordDto data = new ResetPasswordDto();
            response.Ray = request.Ray;
            string token = request.Data;

            // try with email first
            TbSecUser user = await _userRepository.GetUserByPwdToken(token);
            if (user != null)
            {
                // check if reset expiry date time still valid
                if (user.ResetPwdExpiryDt?.CompareTo(DateTimeOffset.Now) >= 0)
                {
                    data.IsVerified = true;
                }
                else
                {
                    data.IsVerified = false;
                }
                data.Email = user.Email;
                response.Data = data;
            }
            else
            {
                data.IsVerified = false;
                MessageUtil.AddToast(response, Level.ERROR, Label.ESS50_E_002); // token not verified
            }
            return response;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<ResetPasswordDto>> ResetPassword(ApiRequest<ResetPasswordDto> request)
        {
            _logger.LogInformation("ResetPassword");
            ApiResponse<ResetPasswordDto> response = new ApiResponse<ResetPasswordDto>();
            ResetPasswordDto data = new ResetPasswordDto();
            response.Ray = request.Ray;
            ResetPasswordDto reqData = request.Data;

            // ensure both password is matching
            if (!reqData.NewPassword.Equals(reqData.ConfirmPassword))
            {
                data.ResetStatus = false;
                MessageUtil.AddToast(response, Level.ERROR, Label.ESS50_E_003); // password not matched
                return response;
            }

            // try with email first
            TbSecUser user = await _userRepository.GetUserByPwdToken(reqData.Token);
            if (user != null)
            {
                // check if reset expiry date time still valid
                if (user.ResetPwdExpiryDt?.CompareTo(DateTimeOffset.Now) >= 0)
                {
                    data.ResetStatus = true;

                    user.Password = Utils.MD5Hash(user.UserName, reqData.NewPassword);
                    user.ResetPwdExpiryDt = null;
                    user.ResetPwdToken = null;
                    user.ModifiedBy = user.UserName;
                    user.ModifiedDt = DateTime.Now;

                    await _userRepository.Update(user);

                    MessageUtil.AddToast(response, Level.INFO, Label.ESS50_I_002); //Password updated. Please login with the new password.
                }
                else
                {
                    data.ResetStatus = false;
                    MessageUtil.AddToast(response, Level.ERROR, Label.ESS50_E_004); // token expired
                }
                response.Data = data;
            }
            else
            {
                data.ResetStatus = false;
                MessageUtil.AddToast(response, Level.ERROR, Label.ESS50_E_001); // user not exists
            }
            return response;
        }

        /// <summary>
        /// Change password - required current password, new password, confirm password
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<Dictionary<string, object>>> ChangePassword(ApiRequest<ChangePasswordDto> request)
        {
            _logger.LogInformation("ChangePassword");
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();
            if (!request.Data.IsAuthenticated)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.AUTH0101 //wrong current password
                });
                response.Data = null;
                return response;
            }

            // ensure both password is matching
            if (!request.Data.NewPassword.Equals(request.Data.ConfirmPassword))
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.ESS50_E_003 //wrong new password
                });
                response.Data = null;
                return response;
            }

            // ensure current password is not matching with new password
            if (request.Data.CurrentPassword.Equals(request.Data.ConfirmPassword))
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.ESS50_E_005 //wrong new password
                });
                response.Data = null;
                return response;
            }

            TbSecUser user = await _userRepository.GetUserByUserName(request.Data.Username);
            if (user != null)
            {
                user.Password = Utils.MD5Hash(user.UserName, request.Data.NewPassword);
                user.ResetPwdExpiryDt = null;
                user.ResetPwdToken = null;
                user.ModifiedBy = user.UserName;
                user.ModifiedDt = DateTime.Now;

                await _userRepository.Update(user);

                MessageUtil.AddToast(response, Level.INFO, Label.ESS50_I_002); //Password updated. Please login with the new password.
                response.Data.Add("update", true);
            }
            else
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.ESS50_E_001 // user not exists
                });
                response.Data = null;
                return response;
            }

            return response;
        }

        public async Task<ApiResponse<int?>> UpdatePortalAccess(int clientId, int entityId, string username, string ray, UserPortalAccessRequest userPortal)
        {
            _logger.LogInformation($"UpdatePortalAccess --- ClientId: {clientId} , EntityId: {entityId} , Username: {username} , EmployeeId: {userPortal.EmployeeId}");
            ApiResponse<int?> response = new ApiResponse<int?>
            {
                Ray = ray ?? string.Empty,
                Data = new int?(),
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                    ValidateResult = new List<MessageItem>()
                }
            };

            var user = await _userRepository.GetUserByEmployeeId(clientId, entityId, userPortal.EmployeeId);
            //user not exist
            if (user == null)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.USR0110
                });
                response.Message.ValidateResult.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.USR0102
                });
                response.Data = null;
                return response;
            }
            userPortal.UserId = user.UserId;
            //validate username
            var messages = _validator.Validate(userPortal);
            if (messages.Count > 0)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.USR0110
                });
                response.Message.ValidateResult = messages;
                response.Data = null;
                return response;
            }

            user.UserName = userPortal.Username;
            user.EssAccessFlag = true;
            user.EssAccessStartDt = userPortal.StartDate;
            user.EssAccessEndDt = userPortal.EndDate;
            user.Email = userPortal.Email;
            user.EssAccessFlag = userPortal.EssAccessFlag;
            user.SingleSignOnId = userPortal.SingleSignOnId;
            user.ModifiedBy = username;
            user.ModifiedDt = DateTime.Now;

            await _userRepository.Update(user);

            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.USR0109
            });

            response.Data = user.UserId;
            return response;
        }

        /// <summary>
        /// Create password for user first login.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<ResetPasswordDto>> FirstLogin(ApiRequest<ResetPasswordDto> request)
        {
            _logger.LogInformation("FirstLogin");
            ApiResponse<ResetPasswordDto> response = new ApiResponse<ResetPasswordDto>();
            ResetPasswordDto data = new ResetPasswordDto();
            response.Ray = request.Ray;
            ResetPasswordDto reqData = request.Data;
            List<MessageItem> valResult = response.Message.ValidateResult;

            if (!request.Data.ConfirmPassword.Equals(request.Data.NewPassword)) // password not matched
            {
                valResult.Add(new MessageItem
                {
                    ComponentId = "confirmPwd",
                    Level = "ERROR",
                    LabelCode = Label.ESS50_E_003,
                });
                data.ResetStatus = false;
                return response;
            }

            // try with email first
            TbSecUser user = await _userRepository.GetUserByPwdToken(reqData.Token);
            if (user != null)
            {
                // check if reset expiry date time still valid
                if (user.ResetPwdExpiryDt?.CompareTo(DateTimeOffset.Now) >= 0)
                {
                    data.ResetStatus = true;

                    user.Password = Utils.MD5Hash(user.UserName, reqData.NewPassword);
                    user.ResetPwdExpiryDt = null;
                    user.ResetPwdToken = null;
                    user.ModifiedBy = user.UserName;
                    user.ModifiedDt = DateTime.Now;

                    await _userRepository.Update(user);

                    MessageUtil.AddToast(response, Level.INFO, Label.ESS50_I_003); //Password setup process is completed.
                }
                else
                {
                    data.ResetStatus = false;
                    MessageUtil.AddToast(response, Level.ERROR, Label.ESS50_E_004); // token expired
                }
                response.Data = data;
            }
            else
            {
                data.ResetStatus = false;
                MessageUtil.AddToast(response, Level.ERROR, Label.ESS50_E_001); // user not exists
            }
            return response;
        }

        public async Task<ApiResponse<UserViewModel>> GetUserByEmployeeId(int clientId, int entityId, int employeeId, string ray)
        {
            _logger.LogInformation($"GetUserByEmployeeId --- ClientId: {clientId} , EntityId: {entityId} , EmployeeId: {employeeId}");
            ApiResponse<UserViewModel> response = new ApiResponse<UserViewModel>
            {
                Ray = ray ?? string.Empty,
                Data = new UserViewModel(),
            };

            var user = await _userRepository.GetUserByEmployeeId(clientId, entityId, employeeId);
            var userVm = user != null ? new UserViewModel(user) : null;
            response.Data = userVm;
            return response;
        }

        public async Task<ApiResponse<bool>> SendInvitation(ApiRequest<SendInvitationRequest> request, int userId, string createdBy)
        {
            _logger.LogInformation($"SendInvitation --- UserId: {userId} , CreatedBy: {createdBy}");
            ApiResponse<bool> response = new ApiResponse<bool>();
            response.Ray = request.Ray;

            TbSecUser user = await _userRepository.Find(userId);
            Guid guid = Guid.NewGuid();
            string gstr = guid.ToString();
            string md5 = Utils.MD5Hash(request.Data.UserName, gstr);
            string token = md5 + gstr.Substring(gstr.Length - 12);
            user.ResetPwdToken = token;
            user.ResetPwdExpiryDt = DateTimeOffset.Now.AddDays(2); // expired in 2 days
            await _userRepository.UpdatePwdToken(userId, user.ResetPwdToken, user.ResetPwdExpiryDt);

            ClientResult client = await _clientRepository.GetClientEntityId(request.Parameter.ClientId, request.Parameter.EntityId);
            string passwordLink = _appSetting.Value.ChangePasswordUrl;
            passwordLink = passwordLink.Replace(_appSetting.Value.ClientCodeParam, client.ClientCode)
                                       .Replace(_appSetting.Value.EntityCodeParam, client.EntityCode)
                                       .Replace(_appSetting.Value.TokenParam, token);

            TbMsgMessage message = new TbMsgMessage
            {
                MessageType = CommonLib.Constants.Messages.MessageTemplateType.Email,
                Recipient = request.Data.Email,
                ProcessedDt = DateTime.UtcNow,
                Status = CommonLib.Constants.Messages.MessageStatus.NEW,
                Portal = CommonLib.Constants.Messages.Portal.EE,
                FlagHtml = true,
                AttemptCount = 0,
                UserId = userId,
                CreatedBy = createdBy,
                CreatedDt = DateTime.UtcNow
            };
            EML001 eml001 = new EML001
            {
                UserName = request.Data.UserName,
                Link = passwordLink.ToString()
            };
            var isSuccess = await _sendEmailService.Send(eml001, _smtpSettings, _mailSettings, request.Parameter, message);
            if (isSuccess)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.SUCCESS,
                    LabelCode = Label.EML001
                });
            }
            else
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.EML002
                });
            }
            response.Data = isSuccess;
            return response;
        }
    }
}