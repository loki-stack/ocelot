﻿using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using CommonLib.Utils;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Authentication.Models;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using CoreServiceAPIs.Common.Constants;
using CoreServiceAPIs.Common.Dtos;
using CoreServiceAPIs.Common.Interfaces;
using CoreServiceAPIs.Common.ShardingUtil;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Services.Impl
{
    public class JwtAuthenticationService : IJwtAuthenticationService
    {
        private readonly IUserRepository _userRepository;
        private readonly IRefreshTokenRepository _refreshTokenRepository;
        private readonly Common.Models.AppSettings _appSettings;
        private readonly TokenValidationParameters _tokenValidationParameters;
        private readonly ILogger<JwtAuthenticationService> _logger;

        private readonly IFunctionRepository _funcRepo;

        public JwtAuthenticationService(IOptions<Common.Models.AppSettings> appSettings, IUserRepository userRepository,
                                        IRefreshTokenRepository refreshTokenRepository,
                                        TokenValidationParameters tokenValidationParameters,
                                        IFunctionRepository funcRepo,
                                        ILogger<JwtAuthenticationService> logger)
        {
            _appSettings = appSettings.Value;
            _userRepository = userRepository;
            _refreshTokenRepository = refreshTokenRepository;
            _tokenValidationParameters = tokenValidationParameters;
            _funcRepo = funcRepo;
            _logger = logger;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> Authenticate(ApiRequest<AuthenticateRequest> request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();

            //authentication
            AuthenticationResult authentication = await ProcessAuthentication(request.Data.Account, request.Data.Password, true);

            if (!authentication.Success)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.AUTH0101
                });
                response.Data = null;
                return response;
            }

            UserViewModel userViewModel = await _userRepository.GetUserInformation(authentication.UserId);
            response.Data.Add("user", userViewModel);
            response.Data.Add("authen", authentication);
            //update last active
            await _userRepository.UpdateLastActive(authentication.UserId);
            return response;
        }

        /// <summary>
        /// Authentication for ESS portal user
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<Dictionary<string, object>>> EssAuthenticate(ApiRequest<AuthenticateRequest> request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();

            // TODO - improve by single repository call -- GetUserByUserNameOrEmail
            TbSecUser user = await _userRepository.GetEntityUser(request.Data.Account, request.Parameter.ClientId,
                request.Parameter.EntityId);
            if (!IsUserValid(user))
            {
                MessageUtil.AddToast(response, Level.ERROR, Label.AUTH0101); //user doesnt exist
                response.Data = null;
                return response;
            }
            // TbSecUser user = await _userRepository.GetUserByUserName(request.Data.Account);
            // if (user == null)
            // {
            //     user = await _userRepository.GetUserByEmail(request.Data.Account);
            //     if (user == null)
            //     {
            //         MessageUtil.AddToast(response, Level.ERROR, Label.AUTH0101); //user doesnt exist
            //         response.Data = null;
            //         return response;
            //     }
            // }

            // if (user.ClientId != request.Parameter.ClientId || user.EntityId != request.Parameter.EntityId)
            // {
            //     MessageUtil.AddToast(response, Level.ERROR, Label.AUTH0101); // mismatch url
            //     response.Data = null;
            //     return response;
            // }

            var password = request.Data.Password;
            if (user.Password != Utils.MD5Hash(user.UserName, password))
            {
                MessageUtil.AddToast(response, Level.ERROR, Label.AUTH0101); // incorrect password
                response.Data = null;
                return response;
            }

            if (user.StatusCd == Status.INACTIVE_CD)
            {
                MessageUtil.AddToast(response, Level.ERROR, Label.AUTH0101); // user account not active
                response.Data = null;
                return response;
            }

            AuthenticationResult authentication = await ProcessToken(user, true);
            UserViewModel userViewModel = await _userRepository.GetUserInformation(authentication.UserId);
            response.Data.Add("user", userViewModel);
            response.Data.Add("authen", authentication);
            //update last active
            await _userRepository.UpdateLastActive(authentication.UserId);
            return response;
        }

        private bool IsUserValid(TbSecUser user)
        {
            if (user == null)
            {
                _logger.LogInformation($"user is null");
                return false;
            }

            if ((user.EssAccessFlag == null) || (user.EssAccessFlag == false))
            {
                _logger.LogInformation($"user access is not enabled - {user.EssAccessFlag}");
                return false;
            }

            DateTime curr = DateTime.Now;
            if (curr.Date.CompareTo(user.EssAccessStartDt) < 0)
            {
                _logger.LogInformation($"user access - current date [{curr}] is less than start date [{user.EssAccessStartDt}]");
                return false;
            }

            if ((user.EssAccessEndDt != null) && (curr.Date.CompareTo(user.EssAccessEndDt) > 0))
            {
                _logger.LogInformation($"user access - current date [{curr}] is after end date [{user.EssAccessEndDt}]");
                return false;
            }

            // all good if reached here
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<Dictionary<string, object>>> RefreshToken(ApiRequest<RefreshTokenRequest> request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();

            AuthenticationResult authentication = new AuthenticationResult();
            authentication = await ProcessRefreshToken(request.Data.Token, request.Data.RefreshToken);
            if (!authentication.Success)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.AUTH0103
                });
                response.Data = null;
                return response;
            }
            response.Data.Add("authen", authentication);
            return response;
        }

        public async Task<AuthenticationResult> ProcessAuthentication(string account, string password, bool checkPassword)
        {
            TbSecUser user = await _userRepository.GetUserByUserName(account);
            if (user == null)
            {
                user = await _userRepository.GetUserByEmail(account);
                if (user == null)
                {
                    return new AuthenticationResult { Errors = new[] { "Account does not exist " } };
                }
            }
            if (checkPassword)
            {
                if (user.Password != Utils.MD5Hash(user.UserName, password))
                {
                    return new AuthenticationResult { Errors = new[] { "Password does not correct " } };
                }
            }
            if (user.StatusCd == Status.INACTIVE_CD)
            {
                return new AuthenticationResult { Errors = new[] { "User is inactive " } };
            }

            AuthenticationResult result = await ProcessToken(user, false);
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="includeEeId"></param>
        /// <returns></returns>
        private async Task<AuthenticationResult> ProcessToken(TbSecUser user, bool includeEeId)
        {
            var roles = await _funcRepo.GetFunctionCdByUserId(user.UserId);
            var token = GenerateSecurityToken(user, roles, includeEeId);
            var refreshToken = new TbSecRefreshToken
            {
                Token = Guid.NewGuid().ToString(),
                JwtId = token.Id,
                UserId = user.UserId,
                CreationDate = DateTime.Now,
                ExpiryDate = DateTime.Now.AddDays(Convert.ToInt32(_appSettings.RefreshTokenExpries)),
                Invalidated = false,
                Used = false
            };
            _refreshTokenRepository.Save(refreshToken);
            var tokenHandler = new JwtSecurityTokenHandler();
            return new AuthenticationResult
            {
                Success = true,
                Token = tokenHandler.WriteToken(token),
                RefreshToken = refreshToken.Token,
                UserId = user.UserId
            };

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="roles"></param>
        /// <returns></returns>
        private SecurityToken GenerateSecurityToken(TbSecUser user, List<string> roles)
        {
            return GenerateSecurityToken(user, roles, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="roles"></param>
        /// <param name="includeEeId"></param>
        /// <returns></returns>
        private SecurityToken GenerateSecurityToken(TbSecUser user, List<string> roles, bool includeEeId)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenKey = Encoding.ASCII.GetBytes(_appSettings.Secret);

            var claims = new List<Claim> {
               new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
               new Claim(ClaimTypes.Name, user.UserName),
               new Claim("Id", user.UserId.ToString())
            };

            if (roles != null)
            {
                foreach (string role in roles)
                {
                    claims.Add(new Claim(ClaimTypes.Role, role));
                }
            }

            if (includeEeId)
            {
                claims.Add(new Claim("ClientId", user.ClientId.ToString()));
                claims.Add(new Claim("EntityId", user.EntityId.ToString()));
                if (user.EmployeeId != null)
                {
                    claims.Add(new Claim("EmployeeId", user.EmployeeId.ToString()));
                }
            }

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddMinutes(Convert.ToInt32(_appSettings.TokenExpires)),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(tokenKey), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return token;
        }

        public ClaimsPrincipal GetPrincipalFromToken(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            try
            {
                var tokenValidationParameters = _tokenValidationParameters.Clone();
                tokenValidationParameters.ValidateLifetime = false;
                var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out var validatedToken);
                if (!IsJwtWithValidSecurityAlgorithm(validatedToken))
                {
                    return null;
                }
                return principal;
            }
            catch
            {
                return null;
            }
        }

        private bool IsJwtWithValidSecurityAlgorithm(SecurityToken validatedToken)
        {
            return (validatedToken is JwtSecurityToken jwtSecurityToken) &&
                   jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256,
                       StringComparison.InvariantCultureIgnoreCase);
        }

        public async Task<AuthenticationResult> ProcessRefreshToken(string token, string refreshToken)
        {
            var validatedToken = GetPrincipalFromToken(token);
            if (validatedToken == null)
            {
                return new AuthenticationResult { Errors = new[] { "Invalid Token" } };
            }
            var expiryDateUnix = long.Parse(validatedToken.Claims.Single(x => x.Type == JwtRegisteredClaimNames.Exp).Value);
            var expiryDateTimeUtc = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(expiryDateUnix);
            if (expiryDateTimeUtc > DateTime.UtcNow)
            {
                return new AuthenticationResult { Errors = new[] { "This token hasn't expired yet" } };
            }

            var jti = validatedToken.Claims.Single(x => x.Type == JwtRegisteredClaimNames.Jti).Value;

            var storedRefreshToken = await _refreshTokenRepository.GetInformation(refreshToken);

            if (storedRefreshToken == null)
            {
                return new AuthenticationResult { Errors = new[] { "This refresh token does not exist" } };
            }

            if (DateTime.UtcNow > storedRefreshToken.ExpiryDate)
            {
                return new AuthenticationResult { Errors = new[] { "This refresh token has expired" } };
            }

            if (storedRefreshToken.Invalidated.Value)
            {
                return new AuthenticationResult { Errors = new[] { "This refresh token has been invalidated" } };
            }

            if (storedRefreshToken.Used.Value)
            {
                return new AuthenticationResult { Errors = new[] { "This refresh token has been used" } };
            }

            if (storedRefreshToken.JwtId != jti)
            {
                return new AuthenticationResult { Errors = new[] { "This refresh token does not match this JWT" } };
            }

            storedRefreshToken.Used = true;
            _refreshTokenRepository.Update(storedRefreshToken);
            string id = validatedToken.Claims.Single(x => x.Type == "Id").Value;
            var user = await _userRepository.Find(Convert.ToInt32(id));
            return await ProcessAuthentication(user.UserName, user.Password, false);
        }
    }
}