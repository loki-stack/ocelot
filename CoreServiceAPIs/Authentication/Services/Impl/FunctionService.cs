﻿using AutoMapper;
using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using CoreServiceAPIs.Common.Constants;
using CoreServiceAPIs.Common.Dtos;
using CoreServiceAPIs.Common.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Services.Impl
{
    public class FunctionService : IFunctionService
    {
        private readonly IFunctionRepository _functionRepository;
        private readonly IMapper _mapper;
        private readonly ICfgI18nRepository _cfgI18nRepository;

        public FunctionService(IFunctionRepository functionRepository, IMapper mapper, ICfgI18nRepository cfgI18nRepository)
        {
            _functionRepository = functionRepository;
            _mapper = mapper;
            _cfgI18nRepository = cfgI18nRepository;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> Create(ApiRequest<CreateFuncRequest> request, string username)
        {
            List<CommonLib.Dtos.MessageItem> validateResult = new List<CommonLib.Dtos.MessageItem>();
            //Validate
            if (_functionRepository.CheckExistFuncByName(request.Data.FunctionCd, 0))
            {
                validateResult.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.FUNC01E001
                });
            }

            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray != null ? request.Ray : string.Empty;
            response.Data = new Dictionary<string, object>(); ;
            if (validateResult.Count > 0)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.T_FUNCTION_ERROR
                });
                response.Message.ValidateResult = validateResult;
                response.Data = null;
                return response;
            }

            var id = await _functionRepository.Create(request, username);
            if (id == 0)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.T_FUNCTION_ERROR,
                });
                response.Data = null;
                return response;
            }
            response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.T_FUNCTION_INFO,
            });
            response.Data.Add("funcId", id);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> Delete(int id, ApiRequest request)
        {
            List<CommonLib.Dtos.MessageItem> validateResult = new List<CommonLib.Dtos.MessageItem>();
            //Validate
            if (!_functionRepository.CheckExistFuncById(id))
            {
                validateResult.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.FUNC01E002,
                });
            }
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray != null ? request.Ray : string.Empty;
            response.Data = new Dictionary<string, object>();

            if (validateResult.Count > 0)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.T_FUNCTION_ERROR
                });
                response.Message.ValidateResult = validateResult;
                response.Data = null;
                return response;
            }

            var result = await _functionRepository.Delete(id);
            if (!result)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.T_FUNCTION_ERROR,
                    Placeholder = new string[] { "FUNC0104", "Exception!!!" }
                });
                response.Data = null;
                return response;
            }
            response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.T_FUNCTION_INFO,
                Placeholder = new string[] { "FUNC0104", "Delete successfully!" }
            });
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetAll(ApiRequest<PaginationRequest> request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray != null ? request.Ray : string.Empty;
            response.Data = new Dictionary<string, object>(); ;
            var funcs = await _functionRepository.GetAll(request);

            var pagination = new Pagination<FuncViewModel>();
            pagination.Page = request.Data.Page.Value;
            pagination.Size = request.Data.Size.Value;
            pagination.TotalPages = (funcs.Count + request.Data.Size.Value - 1) / request.Data.Size.Value;
            pagination.TotalElements = funcs.Count;

            var data = funcs.Skip((request.Data.Page.Value - 1) * request.Data.Size.Value).Take(request.Data.Size.Value).ToList();
            pagination.NumberOfElements = data.Count;
            pagination.Content = data;
            response.Data.Add("funcs", pagination);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetById(int id, ApiRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray != null ? request.Ray : string.Empty;
            response.Data = new Dictionary<string, object>();
            var func = await _functionRepository.GetById(id);
            if (func == null)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.FUNC01E002,
                });
                response.Data = null;
                return response;
            }
            response.Data.Add("func", func);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> Update(ApiRequest<UpdateFuncRequest> request, string username)
        {
            List<CommonLib.Dtos.MessageItem> validateResult = new List<CommonLib.Dtos.MessageItem>();
            //Validate
            if (!_functionRepository.CheckExistFuncById(request.Data.FunctionId))
            {
                validateResult.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.FUNC01E002
                });
            }
            if (_functionRepository.CheckExistFuncByName(request.Data.FunctionCd, request.Data.FunctionId))
            {
                validateResult.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.FUNC01E001
                });
            }
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray != null ? request.Ray : string.Empty;
            response.Data = new Dictionary<string, object>();
            if (validateResult.Count > 0)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.T_FUNCTION_ERROR
                });
                response.Data = null;
                response.Message.ValidateResult = validateResult;
                return response;
            }

            var result = await _functionRepository.Update(request, username);
            if (!result)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.T_FUNCTION_ERROR
                });
                response.Data = null;
                return response;
            }
            response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.T_FUNCTION_INFO
            });
            return response;
        }
    }
}