﻿using AutoMapper;
using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using CommonLib.Validation;
using CoreServiceAPIs.Authentication.Dtos.GlobalCalendar;
using CoreServiceAPIs.Authentication.Dtos.HolidayCalendar;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using CoreServiceAPIs.Common.Constants;
using CoreServiceAPIs.Common.Dtos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace CoreServiceAPIs.Authentication.Services.Impl
{
    public class GlobalCalendarService : IGlobalCalendarService
    {
        private readonly IGlobalCalendarRepository _globalCalendarRepository;
        private readonly IHolidayRepository _holidayRepository;
        private readonly IGenericCodeRepository _genericCodeRepository;
        private readonly IMapper _mapper;
        private readonly IValidator _validator;

        public GlobalCalendarService(IGlobalCalendarRepository globalCalendarRepository,
                                     IHolidayRepository holidayRepository,
                                     IGenericCodeRepository genericCodeRepository,
                                     IMapper mapper,
                                     IValidator validator)
        {
            _globalCalendarRepository = globalCalendarRepository;
            _holidayRepository = holidayRepository;
            _genericCodeRepository = genericCodeRepository;
            _mapper = mapper;
            _validator = validator;
        }

        public async Task<ApiResponse<GlobalCalendarViewModel>> Save(string username, string ray, GlobalCalendarDto globalCalendarDto)
        {
            ApiResponse<GlobalCalendarViewModel> response = new ApiResponse<GlobalCalendarViewModel>
            {
                Ray = ray ?? string.Empty,
                Data = new GlobalCalendarViewModel(),
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                    ValidateResult = new List<MessageItem>()
                }
            };
            //Validate data
            response = await ValidateData(ray, globalCalendarDto);
            if (response.Message.ValidateResult.Count > 0 || response.Message.Toast.Count > 0)
            {
                return response;
            }

            DateTime currentDate = DateTime.Now;
            int id = 0;
            //Save data
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    //Save global calendar
                    id = await SaveGlobalCalendar(username, currentDate, globalCalendarDto.GlobalCalendar);
                    //Save holiday
                    await SaveHoliday(username, currentDate, id, globalCalendarDto.Holidays);
                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }

            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.WCL0104
            });
            var data = await GetById(id, ray);
            response.Data = data.Data;
            return response;
        }

        public async Task<ApiResponse<bool?>> DeleteGlobalCalendar(int id, string ray)
        {
            ApiResponse<bool?> response = new ApiResponse<bool?>
            {
                Ray = ray ?? string.Empty,
                Data = null,
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                }
            };

            //check exist Calendar
            bool isExist = _globalCalendarRepository.CheckExistGlobalCalendarById(id);
            if (!isExist)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.WCL0106
                });
                response.Data = null;
                return response;
            }

            //check global link to client/entity work calendar
            bool isLink = await _globalCalendarRepository.CheckLinkClientEntity(id);
            if (isLink)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.GLCD0102
                });
                response.Data = null;
                return response;
            }

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    //delete holiday
                    var holidays = await _holidayRepository.GetHolidayByCalendarId(id);
                    await _holidayRepository.DeleteMany(holidays);
                    //delete global calendar
                    await _globalCalendarRepository.Delete(id);
                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }

            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.WCL0109
            });
            response.Data = true;
            return response;
        }

        public async Task<ApiResponse<GlobalCalendarViewModel>> GetById(int id, string ray)
        {
            ApiResponse<GlobalCalendarViewModel> response = new ApiResponse<GlobalCalendarViewModel>
            {
                Ray = ray ?? string.Empty,
                Data = new GlobalCalendarViewModel()
            };
            var globalCalendar = await _globalCalendarRepository.GetById(id);
            var globalCalendarVm = _mapper.Map<TbCfgGlobalCalendar, GlobalCalendarViewModel>(globalCalendar);
            response.Data = globalCalendarVm;
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetFilter(int clientId, int entityId, string locale, string ray, PaginationRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>
            {
                Ray = ray ?? string.Empty,
                Data = new Dictionary<string, object>()
            };
            var result = await _globalCalendarRepository.GetFilter();
            var globalCalendars = _mapper.Map<List<TbCfgGlobalCalendar>, List<GlobalCalendarViewModel>>(result);
            GlobalCalendarFilter filter = new GlobalCalendarFilter();
            if (!string.IsNullOrEmpty(request.Filter))
            {
                filter = JsonConvert.DeserializeObject<GlobalCalendarFilter>(request.Filter);
            }
            //sort by item from request
            if (!string.IsNullOrEmpty(request.Sort))
            {
                string sortCharacter = request.Sort.Substring(0, 1);
                string sortField = request.Sort.Substring(1, request.Sort.Length - 1);
                if (sortField.ToLower().Contains("calendar"))
                {
                    if (sortCharacter == "-")
                        globalCalendars = globalCalendars.OrderByDescending(x => x.GlobalLevelCalendarName).ToList();
                    else globalCalendars = globalCalendars.OrderBy(x => x.GlobalLevelCalendarName).ToList();
                }
                if (sortField.ToLower().Contains("country"))
                {
                    if (sortCharacter == "-")
                        globalCalendars = globalCalendars.OrderByDescending(x => x.Country).ToList();
                    else globalCalendars = globalCalendars.OrderBy(x => x.Country).ToList();
                }
                if (sortField.ToLower().Contains("status"))
                {
                    if (sortCharacter == "-")
                        globalCalendars = globalCalendars.OrderByDescending(x => x.Status).ToList();
                    else globalCalendars = globalCalendars.OrderBy(x => x.Status).ToList();
                }
            }
            //search by text
            if (!string.IsNullOrEmpty(request.FullTextSearch))
            {
                globalCalendars = globalCalendars.Where(x => x.GlobalLevelCalendarName.ToLower().Contains(request.FullTextSearch.ToLower())).ToList();
            }

            if (!string.IsNullOrEmpty(filter.Status))
            {
                globalCalendars = globalCalendars.Where(x => x.Status == filter.Status).ToList();
            }

            // get generic code status
            var genericCodes = await _genericCodeRepository.GetDDLGenericCodes(clientId, entityId, locale, new List<string>() { GenericCode.STATUS });

            List<GenericCodeResult> gcStatuses = new List<GenericCodeResult>();

            if (genericCodes.ContainsKey(GenericCode.STATUS.ToLower()))
            {
                gcStatuses = genericCodes[GenericCode.STATUS.ToLower()];
            }

            var data = (from gc in globalCalendars
                        join s in gcStatuses on gc.Status equals s.CodeValue into temp1
                        from p1 in temp1.DefaultIfEmpty()
                        select new GlobalCalendarViewModel
                        {
                            GlobalCalendarId = gc.GlobalCalendarId,
                            GlobalLevelCalendarName = gc.GlobalLevelCalendarName,
                            Country = gc.Country,
                            Status = gc.Status,
                            StatusName = p1 != null ? p1.TextValue : string.Empty,
                            Holidays = gc.Holidays,
                            CreatedBy = gc.CreatedBy,
                            CreatedDt = gc.CreatedDt,
                            ModifiedBy = gc.ModifiedBy,
                            ModifiedDt = gc.ModifiedDt
                        }).ToList();

            Pagination<GlobalCalendarViewModel> pagination = new Pagination<GlobalCalendarViewModel>();
            var dataPaging = new List<GlobalCalendarViewModel>();
            if (request.Size != null)
            {
                pagination.Size = request.Size.Value;
                pagination.TotalPages = (data.Count + request.Size.Value - 1) / request.Size.Value;
                dataPaging = data.Skip((request.Page.Value - 1) * request.Size.Value).Take(request.Size.Value).ToList();
            }
            else
            {
                dataPaging = data;
            }
            pagination.Page = request.Page.Value;
            pagination.TotalElements = data.Count;
            pagination.NumberOfElements = dataPaging.Count;
            pagination.Content = dataPaging;
            response.Data.Add("pagination", pagination);
            return response;
        }

        private async Task<ApiResponse<GlobalCalendarViewModel>> ValidateData(string ray, GlobalCalendarDto globalCalendarDto)
        {
            ApiResponse<GlobalCalendarViewModel> response = new ApiResponse<GlobalCalendarViewModel>
            {
                Ray = ray ?? string.Empty,
                Data = new GlobalCalendarViewModel(),
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                    ValidateResult = new List<MessageItem>()
                }
            };

            //validate data
            var messages = _validator.Validate(globalCalendarDto.GlobalCalendar);
            if (globalCalendarDto.Holidays != null)
            {
                for (int i = 0; i < globalCalendarDto.Holidays.Count; i++)
                {
                    var message = _validator.Validate(globalCalendarDto.Holidays[i]);
                    for (int j = 0; j < message.Count; j++)
                    {
                        message[j].ComponentId = $"globalCalendar[{i}]_{message[j].ComponentId}";
                    }
                    messages.AddRange(message);
                }
            }

            //return respone when data invalid
            if (messages.Count > 0)
            {
                response.Message.ValidateResult = messages;
                response.Data = null;
                return response;
            }

            //check exist global calendar active of country
            bool isExistGlobalActiveOfCountry = await _globalCalendarRepository.CheckExistGlobalActiveOfCountry(globalCalendarDto.GlobalCalendar.Country, globalCalendarDto.GlobalCalendar.GlobalCalendarId);

            if (isExistGlobalActiveOfCountry && globalCalendarDto.GlobalCalendar.Status == Status.ACTIVE)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.GLCD0103
                });
                response.Data = null;
                return response;
            }

            //check exist global calendar
            if (globalCalendarDto.GlobalCalendar.GlobalCalendarId != 0)
            {
                bool isExist = _globalCalendarRepository.CheckExistGlobalCalendarById(globalCalendarDto.GlobalCalendar.GlobalCalendarId);
                if (!isExist)
                {
                    response.Message.Toast.Add(new MessageItem
                    {
                        Level = Level.ERROR,
                        LabelCode = Label.WCL0106
                    });
                    response.Data = null;
                    return response;
                }

                messages = new List<MessageItem>();
                var holidayEntities = await _holidayRepository.GetHolidayByCalendarId(globalCalendarDto.GlobalCalendar.GlobalCalendarId);
                if (globalCalendarDto.Holidays != null)
                {
                    //check exist holiday
                    for (int i = 0; i < globalCalendarDto.Holidays.Count; i++)
                    {
                        if (globalCalendarDto.Holidays[i].HolidayDateId != 0)
                        {
                            isExist = holidayEntities.Any(x => x.HolidayDateId == globalCalendarDto.Holidays[i].HolidayDateId);
                            if (!isExist)
                            {
                                messages.Add(new MessageItem
                                {
                                    ComponentId = $"holiday[{i}]",
                                    Level = Level.ERROR,
                                    LabelCode = Label.HLD0105
                                });
                            }
                        }
                    }

                    if (messages.Count > 0)
                    {
                        response.Message.Toast = messages;
                        response.Data = null;
                        return response;
                    }
                }
            }

            //check date exist of holiday
            var toasts = new List<MessageItem>();
            for (int i = 0; i < globalCalendarDto.Holidays.Count; i++)
            {
                bool isExistDate = globalCalendarDto.Holidays.Any(x => x.Date.Value.Date == globalCalendarDto.Holidays[i].Date.Value.Date && x.GuidId != globalCalendarDto.Holidays[i].GuidId);
                if (isExistDate)
                {
                    toasts.Add(new MessageItem
                    {
                        Level = Level.ERROR,
                        LabelCode = Label.HLD0111,
                        Placeholder = new string[] { i.ToString() }
                    });
                }
            }

            if (toasts.Count > 0)
            {
                response.Message.Toast = toasts;
                response.Data = null;
                return response;
            }
            return response;
        }

        private async Task<int> SaveGlobalCalendar(string username, DateTime currentDate, GlobalCalendarModel globalCalendar)
        {
            int id = 0;
            if (globalCalendar.GlobalCalendarId == 0)
            {
                var globalCalendarEntity = _mapper.Map<GlobalCalendarModel, TbCfgGlobalCalendar>(globalCalendar);
                globalCalendarEntity.CreatedBy = username;
                globalCalendarEntity.CreatedDt = currentDate;
                globalCalendarEntity.ModifiedBy = username;
                globalCalendarEntity.ModifiedDt = currentDate;

                id = await _globalCalendarRepository.Create(globalCalendarEntity);
            }
            else
            {
                var globalCalendarEntity = await _globalCalendarRepository.GetById(globalCalendar.GlobalCalendarId);
                //globalCalendarEntity.GlobalLevelCalendarName = globalCalendar.GlobalLevelCalendarName;
                //globalCalendarEntity.Country = globalCalendar.Country;
                globalCalendarEntity.Status = globalCalendar.Status;
                globalCalendarEntity.ModifiedBy = username;
                globalCalendarEntity.ModifiedDt = currentDate;

                id = await _globalCalendarRepository.Update(globalCalendarEntity);
            }

            return id;
        }

        private async Task SaveHoliday(string username, DateTime currentDate, int calendarId, List<HolidayModel> holidayModels)
        {
            var lstHolidayCreate = holidayModels.Where(x => x.HolidayDateId == 0).ToList();
            var lstHolidayUpdate = holidayModels.Where(x => x.HolidayDateId != 0).ToList();

            //Create holiday
            if (lstHolidayCreate.Count > 0)
            {
                foreach (var holiday in lstHolidayCreate)
                {
                    holiday.CalendarId = calendarId;
                    holiday.CreatedBy = username;
                    holiday.CreatedDt = currentDate;
                    holiday.ModifiedBy = username;
                    holiday.ModifiedDt = currentDate;
                }

                var holidayEntities = _mapper.Map<List<HolidayModel>, List<TbCfgHoliday>>(lstHolidayCreate);
                await _holidayRepository.CreateMany(holidayEntities);
            }

            //Update holiday
            if (lstHolidayUpdate.Count > 0)
            {
                if (calendarId != 0)
                {
                    var ids = lstHolidayUpdate.Select(x => x.HolidayDateId).ToList();
                    var holidayEntities = await _holidayRepository.GetByIds(ids);
                    foreach (var holiday in holidayEntities)
                    {
                        var holidayModel = lstHolidayUpdate.FirstOrDefault(x => x.HolidayDateId == holiday.HolidayDateId);
                        holiday.Date = holidayModel.Date;
                        holiday.DescriptionPrimaryLanguage = holidayModel.DescriptionPrimaryLanguage;
                        holiday.DescriptionSecondaryLanguage = holidayModel.DescriptionSecondaryLanguage;
                        holiday.HolidayType = holidayModel.HolidayType;
                        holiday.IsWorkingDay = holidayModel.IsWorkingDay;
                        holiday.ModifiedBy = username;
                        holiday.ModifiedDt = currentDate;
                    }
                    await _holidayRepository.UpdateMany(holidayEntities);
                }
            }
        }

        public async Task<ApiResponse<bool?>> DeleteHoliday(int id, string ray)
        {
            ApiResponse<bool?> response = new ApiResponse<bool?>
            {
                Ray = ray ?? string.Empty,
                Data = null,
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                }
            };
            //Check exist holiday
            bool isExist = _holidayRepository.CheckExistHolidayById(id);
            if (!isExist)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.HLD0110
                });
                response.Data = null;
                return response;
            }
            await _holidayRepository.Delete(id);
            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.HLD0108
            });
            response.Data = true;
            return response;
        }
    }
}