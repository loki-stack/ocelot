﻿using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using CoreServiceAPIs.Common.Constants;
using CoreServiceAPIs.Common.Dtos;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Services.Impl
{
    public class UserSecurityProfileService : IUserSecurityProfileService
    {
        private readonly IUserSecurityProfileRepository _userSecurityProfileRepository;
        private readonly ISecurityProfileRepository _secProfileRepository;
        private readonly ILogger<UserSecurityProfileService> _logger;
        public UserSecurityProfileService(ILogger<UserSecurityProfileService> logger, IUserSecurityProfileRepository userSecurityProfileRepository, ISecurityProfileRepository secProfileRepository)
        {
            _secProfileRepository = secProfileRepository;
            _userSecurityProfileRepository = userSecurityProfileRepository;
            _logger = logger;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> CreateUserSecurityProfile(ApiRequest<UserSecurityProfileRequest> request)
        {
            _logger.LogInformation("CreateUserSecurityProfile");
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();
            //check user security profile exist
            TbSecUserSecurityProfile userSecurityProfile = await _userSecurityProfileRepository.GetUserSecurityProfile(request.Data.UserId, request.Data.SecurityProfileId);
            if (userSecurityProfile != null)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.USP0102
                });
                response.Data = null;
                return response;
            }

            TbSecUserSecurityProfile profile = await _userSecurityProfileRepository.CreateUserSecurityProfile(request.Data.UserId, request.Data.SecurityProfileId);
            if (profile == null)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.USP0103
                });
                response.Data = null;
                return response;
            }
            response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.USP0104
            });
            response.Data.Add("userSecurityProfile", profile);
            return response;
        }

        public async Task<ApiResponse<string>> DeleteUserSecurityProfile(ApiRequest<UserSecurityProfileRequest> request)
        {
            _logger.LogInformation("DeleteUserSecurityProfile");
            ApiResponse<string> response = new ApiResponse<string>();
            response.Ray = request.Ray;
            //check user security profile exist
            TbSecUserSecurityProfile userSecurityProfile = await _userSecurityProfileRepository.GetUserSecurityProfile(request.Data.UserId, request.Data.SecurityProfileId);
            if (userSecurityProfile == null)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.USP0105,
                    Placeholder = new string[] { "USP0104", "User Security Profile doesn't exists" }
                });
                response.Data = null;
                return response;
            }

            bool isDeleted = await _userSecurityProfileRepository.DeleteUserSecurityProfile(request.Data.UserId, request.Data.SecurityProfileId);
            if (!isDeleted)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.USP0106
                });
                response.Data = null;
                return response;
            }
            response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.USP0107
            });
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetUserSecurityProfile(ApiRequest<GetUserSecurityProfileRequest> request)
        {
            _logger.LogInformation("GetUserSecurityProfile");
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();

            List<UserSecurityProfileViewModel> model = await _userSecurityProfileRepository.GetAllUserSecurityProfile(request.Data.UserId);
            response.Data.Add("userSecurityProfiles", model);

            List<SecurityProfileViewModel> sp = await _secProfileRepository.GetUserSecurityProfile(request.Data.UserId, request.Parameter.ClientId, request.Parameter.EntityId);
            response.Data.Add("allSecurityProfileList", sp);
            return response;
        }
        public async Task<ApiResponse<Dictionary<string, object>>> Update(ApiRequest<UserSecurityProfileDto> request, string username, int userId)
        {
            _logger.LogInformation($"UpdateUserSecurityProfile --- Username: {username} , UserId: {userId}");
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();

            if (request.Data.UserSecurityProfiles.Count == 0)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.USR0107, // require
                });
                response.Data = null;
                return response;
            }

            var model = await _userSecurityProfileRepository.Update(request.Data, username, userId);
            if (!model)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.USR0105, // update failed
                });
                response.Data = null;
                return response;
            }

            response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
            {
                Level = Level.INFO,
                LabelCode = Label.USR0106, // update success
            });

            response.Data.Add("success", model);
            return response;
        }
    }
}
