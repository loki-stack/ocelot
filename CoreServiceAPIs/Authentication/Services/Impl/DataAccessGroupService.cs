using AutoMapper;
using CommonLib.Dtos;
using CommonLib.Validation;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using CoreServiceAPIs.Common.Constants;
using CoreServiceAPIs.Common.Dtos;
using CoreServiceAPIs.Common.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace CoreServiceAPIs.Authentication.Services.Impl
{
    /// <summary>
    /// DataAccessGroupService
    /// </summary>
    /// <returns></returns>
    public class DataAccessGroupService : IDataAccessGroupService
    {
        private readonly IDataAccessGroupRepository _dataAccessGroupRepository;
        private readonly IFunctionRepository _functionRepository;
        private readonly ICfgI18nRepository _cfgI18nRepository;
        private readonly IMapper _mapper;

        private readonly IValidator _validator;

        /// <summary>
        /// DataAccessGroupService Constructor
        /// </summary>
        /// <param name="dataAccessGroupRepository"></param>
        /// <param name="functionRepository"></param>
        /// <param name="cfgI18nRepository"></param>
        /// <param name="mapper"></param>
        /// <param name="validator"></param>
        /// <returns></returns>
        public DataAccessGroupService(
            IDataAccessGroupRepository dataAccessGroupRepository
            , IFunctionRepository functionRepository
            , ICfgI18nRepository cfgI18nRepository
            , IMapper mapper
            , IValidator validator)
        {
            _dataAccessGroupRepository = dataAccessGroupRepository;
            _functionRepository = functionRepository;
            _cfgI18nRepository = cfgI18nRepository;
            _mapper = mapper;
            _validator = validator;
        }

        /// <summary>
        /// List All Data Access Group in level-based
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public async Task<List<DataAccessGroupViewModel>> ListAll(int clientId, int entityId)
        {
            return await _dataAccessGroupRepository.ListAll(clientId, entityId);
        }

        /// <summary>
        /// Create New Data Access Group
        /// </summary>
        /// <param name="request"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        public async Task<ApiResponse<Dictionary<string, object>>> Create(ApiRequest<CreateDataAccessGroupRequest> request, string username)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray != null ? request.Ray : string.Empty;
            response.Data = new Dictionary<string, object>();
            bool isValid = validate(response.Message.ValidateResult, request.Data);
            if (!isValid)
            {
                return response;
            }

            var id = await _dataAccessGroupRepository.QuickCreate(request, username);
            if (id == 0)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.DAR01_E_001, // add failed
                    Placeholder = new string[] { }
                });
                response.Data = null;
                return response;
            }

            response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
            {
                Level = Level.INFO,
                LabelCode = Label.DAR01_I_001, // add successful
                Placeholder = new string[] { }
            });
            response.Data.Add("dataAccessGroupId", id);
            return response;
        }

        /// <summary>
        /// Get Data Access Group By Its ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="username"></param>
        /// <param name="clientInfo"></param>
        /// <returns></returns>
        public async Task<ApiResponse<DarManagementDto>> GetDARById(int id, string username, ClientResult clientInfo)
        {
            ApiResponse<DarManagementDto> response = new ApiResponse<DarManagementDto>();
            response.Data = new DarManagementDto();
            var selectedGroup = await _dataAccessGroupRepository.GetByDataAccessId(id, username, clientInfo);
            var tree = await _dataAccessGroupRepository.GetTree(id, clientInfo.ClientId, clientInfo.EntityId);

            if (selectedGroup == null)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.DAR01_E_002, // not exists
                    Placeholder = new string[] { }
                });
                response.Data = null;
                return response;
            }
            response.Data.SelectedDataAccessGroup = selectedGroup;
            response.Data.AllClientEntities = tree;
            if (clientInfo.EntityId > 0)
            {
                response.Data.AllOrganizationStructures = await _dataAccessGroupRepository.GetOrganizationStructure(id);
            }

            response.Data.IsSelectAllEntity = await _dataAccessGroupRepository.IsAllEntities(id);
            return response;
        }

        /// <summary>
        /// Get DataAccessClientEntity in Tree Format
        /// </summary>
        /// <param name="request"></param>
        /// <param name="clientInfo"></param>
        /// <returns></returns>
        public async Task<ApiResponse<DarManagementDto>> GetAllClientEntitiesAsTree(ApiRequest<int> request, ClientResult clientInfo)
        {
            ApiResponse<DarManagementDto> response = new ApiResponse<DarManagementDto>();
            response.Ray = request.Ray != null ? request.Ray : string.Empty;
            response.Data = new DarManagementDto();
            var allClientEntities = await _dataAccessGroupRepository.GetTree(request.Data, clientInfo.ClientId, clientInfo.EntityId);
            response.Data.AllClientEntities = allClientEntities;
            return response;
        }

        /// <summary>
        /// Update DataAccess Group
        /// </summary>
        /// <param name="request"></param>
        /// <param name="username"></param>
        /// <param name="clientId"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public async Task<ApiResponse<Dictionary<string, object>>> Update(ApiRequest<UpdateDataAccessGroupRequest> request, string username, int clientId, int entityId)
        {
            List<CommonLib.Dtos.MessageItem> validateResult = new List<CommonLib.Dtos.MessageItem>();
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray != null ? request.Ray : string.Empty;
            response.Data = new Dictionary<string, object>();
            bool isValid = validate(response.Message.ValidateResult, request.Data);
            if (!isValid)
            {
                return response;
            }
            //Validate
            if (!_dataAccessGroupRepository.CheckDataAccessGroupById(request.Data.DataAccessGroupId))
            {
                validateResult.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.DAR01_V_001, // dar group not exists
                });

            }

            if (validateResult.Count > 0)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.DAR01_E_003, // validation error
                });
                response.Message.ValidateResult = validateResult;
                response.Data = null;
                return response;
            }

            var result = await _dataAccessGroupRepository.Update(request, username, clientId, entityId);
            if (!result)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.DAR01_E_004, // update failed
                });
                response.Data = null;
                return response;
            }

            response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
            {
                Level = Level.INFO,
                LabelCode = Label.DAR01_I_002, // update success
            });
            return response;
        }

        /// <summary>
        /// Validate api object
        /// </summary>
        /// <param name="valResult"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        private bool validate<TModel>(List<MessageItem> valResult, TModel data) where TModel : class
        {
            bool rv = true;

            // test
            List<MessageItem> vrs = _validator.Validate(data);
            if (vrs.Count > 0)
            {
                rv = false;
                //valResult.Concat(vrs); // add to vrs list
                vrs.ForEach((item) =>
                {
                    valResult.Add(item);
                });
            }

            return rv;
        }
    }
}