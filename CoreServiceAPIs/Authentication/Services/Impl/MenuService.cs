﻿using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using CoreServiceAPIs.Common.Constants;
using CoreServiceAPIs.Common.Dtos;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Services.Impl
{
    public class MenuService : IMenuService
    {
        private readonly IMenuRepository _menuRepository;

        public MenuService(IMenuRepository menuRepository)
        {
            _menuRepository = menuRepository;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetMenu(int userId, ApiRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray != null ? request.Ray : string.Empty;
            response.Data = new Dictionary<string, object>();
            var menus = await _menuRepository.GetMenu(userId, request);
            var leftMenus = menus["leftMenu"] as List<LeftMenuResult>;
            foreach (var item in leftMenus)
            {
                if (!string.IsNullOrEmpty(item.FunctionCd))
                {
                    var code = item.FunctionCd.Substring(item.FunctionCd.Length - 2, 2);
                    item.FunctionNature = GetFucntionNature(code);
                }
            }
            var leftMenuResult = leftMenus.GroupBy(x => new { x.ModuleCd, x.ModuleName })
                                           .Select(x => new LeftMenuViewModel
                                           {
                                               ModuleCd = x.Key.ModuleCd,
                                               ModuleName = x.Key.ModuleName,
                                               Functions = x.Select(y => new LeftMenuFunctionViewModel
                                               {
                                                   ModuleCd = y.ModuleCd,
                                                   FunctionCd = y.FunctionCd,
                                                   ModuleName = y.ModuleName,
                                                   FunctionName = y.FunctionName,
                                                   FunctionNature = y.FunctionNature,
                                                   MenuName = y.MenuName
                                               }).ToList()
                                           }).ToList();

            var rightMenus = menus["rightMenu"] as List<RightMenuResult>;
            foreach (var item in rightMenus)
            {
                if (!string.IsNullOrEmpty(item.FunctionCd))
                {
                    var code = item.FunctionCd.Substring(item.FunctionCd.Length - 2, 2);
                    item.FunctionNature = GetFucntionNature(code);
                }
            }
            var rightMenuResult = rightMenus.GroupBy(x => new { x.ModuleCd, x.ModuleName })
                                           .Select(x => new RightMenuViewModel
                                           {
                                               ModuleCd = x.Key.ModuleCd,
                                               ModuleName = x.Key.ModuleName,
                                               Functions = x.Select(y => new RightMenuFunctionViewModel
                                               {
                                                   ModuleCd = y.ModuleCd,
                                                   FunctionCd = y.FunctionCd,
                                                   ModuleName = y.ModuleName,
                                                   FunctionName = y.FunctionName,
                                                   FunctionNature = y.FunctionNature,
                                                   MenuName = y.MenuName
                                               }).ToList()
                                           }).ToList();

            Dictionary<string, IList> result = new Dictionary<string, IList>();
            result.Add("leftMenu", leftMenuResult);
            result.Add("rightMenu", rightMenuResult);
            response.Data.Add("menus", result);
            return response;
        }

        private string GetFucntionNature(string code)
        {
            string functionNature = string.Empty;
            switch (code)
            {
                case FunctionCode.SEARCH:
                case FunctionCode.SEARCH_11:
                    functionNature = FunctionNature.SEARCH;
                    break;

                case FunctionCode.CREATE:
                    functionNature = FunctionNature.CREATE;
                    break;

                case FunctionCode.UPDATE:
                    functionNature = FunctionNature.UPDATE;
                    break;

                case FunctionCode.DELETE:
                    functionNature = FunctionNature.DELETE;
                    break;

                case FunctionCode.CLONE:
                    functionNature = FunctionNature.CLONE;
                    break;

                default:
                    functionNature = string.Empty;
                    break;
            }
            return functionNature;
        }
    }
}