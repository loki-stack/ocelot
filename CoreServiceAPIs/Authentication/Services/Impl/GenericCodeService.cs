﻿using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using CoreServiceAPIs.Common.Constants;
using CoreServiceAPIs.Common.Dtos;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Services.Impl
{
    public class GenericCodeService : IGenericCodeService
    {
        private readonly IGenericCodeRepository _genericCodeRepository;

        public GenericCodeService(IGenericCodeRepository genericCodeRepository)
        {
            _genericCodeRepository = genericCodeRepository;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetDDLGenericCodes(int clientId, int entityId, string locale, string ray, List<string> genericCodeList)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = ray != null ? ray : string.Empty;
            response.Data = new Dictionary<string, object>();
            var result = await _genericCodeRepository.GetDDLGenericCodes(clientId, entityId, locale, genericCodeList);
            response.Data.Add("genericCodes", result);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetModules(int clientId, int entityId, string ray, string locale)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = ray != null ? ray : string.Empty;
            response.Data = new Dictionary<string, object>();
            var result = await _genericCodeRepository.GetModules(clientId, entityId, locale);
            response.Data.Add("modules", result);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetFilterByModule(ApiRequest<GetGenericCodeRequest> request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray != null ? request.Ray : string.Empty;
            response.Data = new Dictionary<string, object>();
            var genericCodes = await _genericCodeRepository.GetFilterByModule(request.Parameter.ClientId, request.Parameter.EntityId, request.Parameter.Locale, request.Data.CodeType);

            GenericCodeFilter filter = new GenericCodeFilter();
            if (!string.IsNullOrEmpty(request.Data.Filter))
            {
                filter = JsonConvert.DeserializeObject<GenericCodeFilter>(request.Data.Filter);
            }

            // sort by item from request
            if (!string.IsNullOrEmpty(request.Data.Sort))
            {
                string sortCharacter = request.Data.Sort.Substring(0, 1);
                string sortField = request.Data.Sort.Substring(1, request.Data.Sort.Length - 1);
                if (sortField.ToLower().Contains("name"))
                {
                    if (sortCharacter == "-")
                        genericCodes = genericCodes.OrderByDescending(x => x.Name).ToList();
                    else genericCodes = genericCodes.OrderBy(x => x.Name).ToList();
                }

                if (sortField.ToLower().Contains("codeValue"))
                {
                    if (sortCharacter == "-")
                        genericCodes = genericCodes.OrderByDescending(x => x.CodeValue).ToList();
                    else genericCodes = genericCodes.OrderBy(x => x.CodeValue).ToList();
                }

                if (sortField.ToLower().Contains("displayorder"))
                {
                    if (sortCharacter == "-")
                        genericCodes = genericCodes.OrderByDescending(x => x.DisplayOrder).ToList();
                    else genericCodes = genericCodes.OrderBy(x => x.DisplayOrder).ToList();
                }

                if (sortField.ToLower().Contains("status"))
                {
                    if (sortCharacter == "-")
                        genericCodes = genericCodes.OrderByDescending(x => x.Status).ToList();
                    else genericCodes = genericCodes.OrderBy(x => x.Status).ToList();
                }

                if (sortField.ToLower().Contains("remark"))
                {
                    if (sortCharacter == "-")
                        genericCodes = genericCodes.OrderByDescending(x => x.Remark).ToList();
                    else genericCodes = genericCodes.OrderBy(x => x.Remark).ToList();
                }

                if (sortField.ToLower().Contains("createby"))
                {
                    if (sortCharacter == "-")
                        genericCodes = genericCodes.OrderByDescending(x => x.CreateBy).ToList();
                    else genericCodes = genericCodes.OrderBy(x => x.CreateBy).ToList();
                }

                if (sortField.ToLower().Contains("createddate"))
                {
                    if (sortCharacter == "-")
                        genericCodes = genericCodes.OrderByDescending(x => x.CreateDt).ToList();
                    else genericCodes = genericCodes.OrderBy(x => x.CreateDt).ToList();
                }

                if (sortField.ToLower().Contains("updateby"))
                {
                    if (sortCharacter == "-")
                        genericCodes = genericCodes.OrderByDescending(x => x.ModifyBy).ToList();
                    else genericCodes = genericCodes.OrderBy(x => x.ModifyBy).ToList();
                }

                if (sortField.ToLower().Contains("updateddate"))
                {
                    if (sortCharacter == "-")
                        genericCodes = genericCodes.OrderByDescending(x => x.ModifyDt).ToList();
                    else genericCodes = genericCodes.OrderBy(x => x.ModifyDt).ToList();
                }
            }

            // search by text
            if (!string.IsNullOrEmpty(filter.Status))
            {
                genericCodes = genericCodes.Where(x => x.Status == filter.Status).ToList();
            }

            if (!string.IsNullOrEmpty(request.Data.FullTextSearch))
            {
                genericCodes = genericCodes.Where(x => x.CodeValue.ToLower().Contains(request.Data.FullTextSearch.ToLower()) || x.Name.ToLower().Contains(request.Data.FullTextSearch.ToLower())).ToList();
            }

            Pagination<GenericCodeViewModel> pagination = new Pagination<GenericCodeViewModel>();
            var result = new List<GenericCodeViewModel>();
            if (request.Data.Size != null)
            {
                pagination.Size = request.Data.Size.Value;
                pagination.TotalPages = (genericCodes.Count + request.Data.Size.Value - 1) / request.Data.Size.Value;
                result = genericCodes.Skip((request.Data.Page.Value - 1) * request.Data.Size.Value).Take(request.Data.Size.Value).ToList();
            }
            else
            {
                result = genericCodes;
            }
            pagination.TotalElements = genericCodes.Count;
            pagination.Page = request.Data.Page.Value;
            pagination.NumberOfElements = result.Count;
            pagination.Content = result;
            response.Data.Add("pagination", pagination);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetById(int id, ApiRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray != null ? request.Ray : string.Empty;
            response.Data = new Dictionary<string, object>();
            var genericCode = await _genericCodeRepository.GetById(id, request.Parameter.Locale);
            response.Data.Add("genericCode", genericCode);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> Create(ApiRequest<CreateGenericCodeRequest> request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray != null ? request.Ray : string.Empty;
            response.Data = new Dictionary<string, object>();
            response.Message = ValidateCreateUpdate(
                request.Data.ParentCodeId,
                request.Parameter.ClientId,
                request.Parameter.EntityId,
                request.Parameter.Locale,
                request.Data.CodeValue,
                request.Data.Status,
                0,
                request.Data.Name,
                request.Data.Remark,
                request.Data.DisplayOrder,
                request.Data.ModuleId);
            if (response.Message.Alert.Count > 0 || response.Message.ValidateResult.Count > 0)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.GC0101
                });
                response.Data = null;
                return response;
            }
            var result = await _genericCodeRepository.Create(request.Parameter.Locale, request.Parameter.ClientId, request.Parameter.EntityId, request.Data);
            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.GC0102
            });
            response.Data.Add("genericCode", result.Data);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> Update(ApiRequest<UpdateGenericCodeRequest> request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray != null ? request.Ray : string.Empty;
            response.Data = new Dictionary<string, object>();

            if (!_genericCodeRepository.CheckExistGenericCodeById(request.Data.Id))
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.GC0103
                });
                response.Message.Alert.Add(new MessageItem
                {
                    ComponentId = Component.CODE,
                    Level = Level.ERROR,
                    LabelCode = Label.GC01E004
                });
                response.Data = null;
                return response;
            }
            var genericCode = await _genericCodeRepository.GetById(request.Data.Id, request.Parameter.Locale);
            response.Message = ValidateCreateUpdate(request.Data.ParentCodeId, genericCode.ClientId, genericCode.EntityId, request.Parameter.Locale, request.Data.CodeValue, request.Data.Status, request.Data.Id, request.Data.Name,
                request.Data.Remark, request.Data.DisplayOrder, request.Data.ModuleId);
            if (response.Message.Alert.Count > 0 || response.Message.ValidateResult.Count > 0)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.GC0103
                });
                response.Data = null;
                return response;
            }
            var result = await _genericCodeRepository.Update(request.Parameter.Locale, request.Data);
            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.GC0104
            });
            response.Data.Add("genericCode", result.Data);
            return response;
        }

        private List<MessageItem> ValidateCommon(string codeValue, string status, string textValue, string remark, int displayOrder, int parentCodeId, int clientId, int entityId, int codeId, int moduleId)
        {
            List<MessageItem> validateResult = new List<MessageItem>();
            if (string.IsNullOrEmpty(codeValue))
            {
                validateResult.Add(new MessageItem
                {
                    ComponentId = Component.CODEVALUE,
                    Level = Level.ERROR,
                    LabelCode = Label.GC01E001
                });
            }

            if (string.IsNullOrEmpty(textValue))
            {
                validateResult.Add(new MessageItem
                {
                    ComponentId = Component.NAME,
                    Level = Level.ERROR,
                    LabelCode = Label.GC01E005
                });
            }

            if (string.IsNullOrEmpty(status))
            {
                validateResult.Add(new MessageItem
                {
                    ComponentId = Component.STATUS,
                    Level = Level.ERROR,
                    LabelCode = Label.GC01E002
                });
            }

            return validateResult;
        }

        private Message ValidateCreateUpdate(int parentCodeId, int clientId, int entityId, string locale, string codeValue, string status, int codeId, string textValue, string remark, int displayOrder, int moduleId)
        {
            var message = new Message();
            message.Toast = new List<MessageItem>();
            message.Alert = new List<MessageItem>();
            message.ValidateResult = new List<MessageItem>();
            List<MessageItem> validateResult = ValidateCommon(codeValue, status, textValue, remark, displayOrder, parentCodeId, clientId, entityId, codeId, moduleId);
            if (validateResult.Count > 0)
            {
                message.ValidateResult = validateResult;
                return message;
            }

            if (codeId != 0)
            {
                if (_genericCodeRepository.CheckExistGenericCode(parentCodeId, clientId, entityId, moduleId, codeId, codeValue))
                {
                    message.ValidateResult.Add(new MessageItem
                    {
                        ComponentId = Component.CODEVALUE,
                        Level = Level.ERROR,
                        LabelCode = Label.GC01E003
                    });
                }
                return message;
            }

            bool isExist = _genericCodeRepository.CheckExistI18N(parentCodeId, clientId, entityId, locale, codeValue, codeId, moduleId);
            if (isExist)
            {
                message.ValidateResult.Add(new MessageItem
                {
                    ComponentId = Component.CODEVALUE,
                    Level = Level.ERROR,
                    LabelCode = Label.GC01E003
                });
            }
            return message;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetTreeGenericCodes(int clientId, int entityId, string ray, string locale)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = ray != null ? ray : string.Empty;
            response.Data = new Dictionary<string, object>();
            var genericCodes = await _genericCodeRepository.GetTreeGenericCodes(clientId, entityId, locale);
            var parents = await _genericCodeRepository.GetParent(locale);
            var nodesColect = genericCodes.Select(x => new FieldListDto
            {
                Id = x.Id,
                Value = x.Name,
                CodeType = x.CodeType,
                CodeValue = x.CodeValue,
                TextValue = x.Name,
                Node = x.CodeLevel
            }).ToList();

            List<TreeView> result = new List<TreeView>();
            foreach (var item in parents)
            {
                TreeView treeView = new TreeView
                {
                    Id = item.Id,
                    Value = item.CodeValue,
                    CodeType = item.CodeType,
                    CodeValue = item.CodeValue,
                    TextValue = item.Name,
                    Node = item.CodeLevel
                };
                treeView.Children = GetTreeMethod(item.CodeLevel, nodesColect);
                result.Add(treeView);
            }
            response.Data.Add("genericCodes", result);
            return response;
        }

        private List<TreeView> GetTreeMethod(HierarchyId node, List<FieldListDto> lstCollection)
        {
            List<TreeView> lst = new List<TreeView>();
            //HierarchyId node = HierarchyId.Parse(nodeStr);
            var lastItemInCurrentLevel = GetChilds(node, lstCollection);

            foreach (var item in lastItemInCurrentLevel)
            {
                TreeView tr = new TreeView
                {
                    Id = item.Id,
                    Value = item.Value,
                    CodeType = item.CodeType,
                    CodeValue = item.CodeValue,
                    TextValue = item.TextValue,
                    Node = item.Node
                };
                tr.Children = GetTreeMethod(item.Node, lstCollection);
                lst.Add(tr);
            }

            return lst;
        }

        private List<TreeView> GetChilds(HierarchyId node, List<FieldListDto> lstCollection)
        {
            List<TreeView> child = lstCollection.Where(x => x.Node.GetAncestor(1) == node).Select(q => new TreeView { Id = q.Id, Node = q.Node, CodeType = q.CodeType, TextValue = q.TextValue, CodeValue = q.CodeValue, Value = q.Value }).ToList();
            return child;
        }

        public async Task<Dictionary<string, List<GenericCodeResult>>> GetGenericCodes(int clientId, int entityId, string locale,
    List<string> genericCodes)
        {
            return await _genericCodeRepository.GetGenericCodes(clientId, entityId, locale, genericCodes);
        }
    }
}