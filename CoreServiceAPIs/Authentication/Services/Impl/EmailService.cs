using Antlr4.StringTemplate;
using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using CommonLib.Repositories.Interfaces;
using CommonLib.Services;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using CoreServiceAPIs.Common.Email.Dtos;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Services.Impl
{
    public class EmailService : IEmailService
    {
        private readonly IMessageService _msgService;
        private readonly IClientService _clientService;
        private readonly ILogger<EmailService> _logger;
        private readonly IMessageTemplateRepository _msgTemplateRepo;
        private readonly IMessageTemplateLangRepository _msgTemplateLangRepo;
        private readonly IMessageRepository _msgRepo;
        protected readonly MailSettings _mailSettings;
        //
        public EmailService(IMessageService msgService
        , IClientService clientService
        , ILogger<EmailService> logger
        , IMessageTemplateRepository msgTemplateRepo
        , IMessageTemplateLangRepository msgTemplateLangRepo
        , IMessageRepository msgRepo
        , IOptions<MailSettings> mailSettings)
        {
            _msgRepo = msgRepo;
            _msgTemplateRepo = msgTemplateRepo;
            _msgTemplateLangRepo = msgTemplateLangRepo;
            _logger = logger;
            _clientService = clientService;
            _msgService = msgService;
            _mailSettings = mailSettings.Value;
        }

        public async Task<bool> RegisterEmailRequest(TbSecUser user, int clientId, int entityId)
        {
            try
            {
                ClientResult clientInfo = await _clientService.GetClientEntityId(clientId, entityId);

                var templateCode = CommonLib.Constants.Messages.MessageTemplateCode.EM_ESS_FORGOT_PWD;
                var templateType = CommonLib.Constants.Messages.MessageTemplateType.Email;

                TbMsgTemplate template = await _msgTemplateRepo.GetById(clientId, entityId, templateCode, templateType);
                if (template == null)
                {
                    // if not found client/entity level message, use global
                    template = await _msgTemplateRepo.GetById(0, 0, templateCode, templateType);
                }

                if (template == null)
                {
                    _logger.LogWarning($"templateCode not found - {templateCode}");
                    return false;
                }

                TbMsgTemplateLang templateLang = await _msgTemplateLangRepo.GetByTemplateIdAndLang(template.TemplateId, "en");
                if (templateLang == null)
                {
                    _logger.LogWarning($"templateLang is null");
                    return false;
                }

                //http://localhost:3100/Client%20A/ENTITYA1/passport/reset-password/d22b2f355e5a3c99b7fda16bdd97fbe923522006441e
                string urlClientCode = Uri.EscapeDataString(clientInfo.ClientCode);
                string urlEntityCode = Uri.EscapeDataString(clientInfo.EntityCode);
                string token = Uri.EscapeDataString(user.ResetPwdToken);
                string url = $"{_mailSettings.EssPortalBaseUrl}/{urlClientCode}/{urlEntityCode}/passport/reset-password/{token}";
                EMForgotPasswordDto data = new EMForgotPasswordDto
                {
                    UserFullName = user.FullName,
                    ResetPasswordURL = url
                };

                string title = string.Empty;
                if ((templateLang.Title != null) && templateLang.Title.Contains("$"))
                {
                    Template templateTitle = new Template(templateLang.Title, '$', '$');
                    templateTitle.Group.RegisterRenderer(typeof(string), new StringRenderer());
                    templateTitle.Add("data", data);
                    title = templateTitle.Render();
                }
                else
                {
                    title = templateLang.Title;
                }

                string content = string.Empty;
                if ((templateLang.Content != null) && templateLang.Content.Contains("$"))
                {
                    Template templateContent = new Template(templateLang.Content, '$', '$');
                    templateContent.Group.RegisterRenderer(typeof(string), new StringRenderer());
                    templateContent.Add("data", data);
                    content = templateContent.Render();
                }
                else
                {
                    content = templateLang.Content;
                }

                // insert record to tb_msg_message
                var msg = await InsertEmailRecord(
                                templateLang,
                                user.UserName,
                                title,
                                content,
                                user.Email
                            );


            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return false;
            }
            return true;
        }

        private async Task<bool> InsertEmailRecord(
           TbMsgTemplateLang templateLang,
           string username,
           string title,
           string content,
           string recipient
           )
        {
            _logger.LogInformation($"InsertEmailRecord");
            try
            {
                MessageCreateDto msg = new MessageCreateDto
                {
                    TemplateLangId = templateLang.TemplateLangId,
                    Recipient = recipient,
                    Status = CommonLib.Constants.Messages.MessageStatus.NEW,
                };
                msg.MessageTitle = title;
                msg.MessageContent = content;

                var entityMsg = await _msgService.Create(msg, username);
                return true;
            }

            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return false;
            }

        }
    }
}