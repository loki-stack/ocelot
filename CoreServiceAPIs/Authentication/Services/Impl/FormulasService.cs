﻿using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace CoreServiceAPIs.Authentication.Services.Impl
{
    public class FormulasService : IFormulasService
    {
        private readonly IFormulasRepository _formulasRepository;
        private readonly CoreServiceAPIs.Authentication.Repositories.Interfaces.IGenericCodeRepository _genericCodeRepository;

        public FormulasService(IFormulasRepository formulasRepository, CoreServiceAPIs.Authentication.Repositories.Interfaces.IGenericCodeRepository genericCodeRepository)
        {
            _formulasRepository = formulasRepository;
            _genericCodeRepository = genericCodeRepository;
        }

        public async Task<ApiResponse<int>> Add(ApiRequest<FormulasAddModel> request)
        {
            int formulasId = 0;
            ApiResponse<int> response = new ApiResponse<int>
            {
                Ray = request.Ray ?? string.Empty,
                Data = 0
            };

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    // Add New Formulas
                    formulasId = await _formulasRepository.Add(request.Data, request.Parameter.ClientId, request.Parameter.EntityId);
                    if (formulasId == 0)
                    {
                        response.Message.Toast.Add(new MessageItem()
                        {
                            Level = Common.Constants.Level.ERROR,
                            LabelCode = Common.Constants.Label.PAY0301
                        });
                        scope.Dispose();
                        return response;
                    }
                    response.Data = formulasId;

                    string tag_name = Util.Parameter.HandleParameter(request.Data.Name);
                    FormulasTagAddModel model = new FormulasTagAddModel()
                    {
                        Flag = 1,
                        FormulasId = formulasId,
                        TagName = tag_name,
                        Remark = request.Data.Description,
                        FormulasRepeat = formulasId
                    };

                    int formulasTagId = await _formulasRepository.AddFormulasTag(model);

                    response.Message.Toast.Add(new MessageItem()
                    {
                        Level = Common.Constants.Level.SUCCESS,
                        LabelCode = Common.Constants.Label.PAY0302
                    });
                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }

            return response;
        }


        public async Task<ApiResponse<List<FormulasResult>>> GetAll(ApiRequest request, int level)
        {
            ApiResponse<List<FormulasResult>> response = new ApiResponse<List<FormulasResult>>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new List<FormulasResult>()
            };

            response.Data = await _formulasRepository.GetAll(level, request.Parameter.ClientId, request.Parameter.EntityId);
            List<string> genericCodeList = new List<string>();
            genericCodeList.Add(Common.Constants.GenericCode.STATUS);
            genericCodeList.Add(Common.Constants.GenericCode.FORMULAS_LEVEL);
            var genericCodes = await _genericCodeRepository.GetGenericCodes(request.Parameter.ClientId, request.Parameter.EntityId, request.Parameter.Locale, genericCodeList);
            List<GenericCodeResult> statuses = new List<GenericCodeResult>();
            List<GenericCodeResult> levels = new List<GenericCodeResult>();
            if (genericCodes.ContainsKey(Common.Constants.GenericCode.STATUS.ToLower()))
            {
                statuses = genericCodes[Common.Constants.GenericCode.STATUS.ToLower()];
            }
            if (genericCodes.ContainsKey(Common.Constants.GenericCode.FORMULAS_LEVEL.ToLower()))
            {
                levels = genericCodes[Common.Constants.GenericCode.FORMULAS_LEVEL.ToLower()];
            }
            foreach (var item in response.Data)
            {
                if (statuses.Any(n => n.CodeValue == item.StatusCd))
                {
                    item.StatusText = statuses.First(n => n.CodeValue == item.StatusCd).TextValue;
                }
                if (levels.Any(m => m.CodeValue == item.Level.ToString()))
                {
                    item.LevelText = levels.First(mbox => mbox.CodeValue == item.Level.ToString()).TextValue;
                }
            }

            return response;
        }

        public async Task<ApiResponse<List<FormulasTagViewResult>>> GetAllFormulasTag(ApiRequest request)
        {
            ApiResponse<List<FormulasTagViewResult>> response = new ApiResponse<List<FormulasTagViewResult>>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new List<FormulasTagViewResult>()
            };

            response.Data = await _formulasRepository.GetFormulasTag();

            return response;
        }

        public async Task<ApiResponse<FormulasResult>> GetById(int formulasId, ApiRequest request)
        {
            ApiResponse<FormulasResult> response = new ApiResponse<FormulasResult>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new FormulasResult()
            };

            response.Data = await _formulasRepository.GetById(formulasId);

            List<string> genericCodeList = new List<string>();
            genericCodeList.Add(Common.Constants.GenericCode.STATUS);
            genericCodeList.Add(Common.Constants.GenericCode.FORMULAS_LEVEL);
            var genericCodes = await _genericCodeRepository.GetGenericCodes(request.Parameter.ClientId, request.Parameter.EntityId, request.Parameter.Locale, genericCodeList);
            List<GenericCodeResult> statuses = new List<GenericCodeResult>();
            List<GenericCodeResult> levels = new List<GenericCodeResult>();
            if (genericCodes.ContainsKey(Common.Constants.GenericCode.STATUS.ToLower()))
            {
                statuses = genericCodes[Common.Constants.GenericCode.STATUS.ToLower()];
            }
            if (genericCodes.ContainsKey(Common.Constants.GenericCode.FORMULAS_LEVEL.ToLower()))
            {
                levels = genericCodes[Common.Constants.GenericCode.FORMULAS_LEVEL.ToLower()];
            }
            if (response.Data != null)
            {
                if (statuses.Any(n => n.CodeValue == response.Data.StatusCd))
                {
                    response.Data.StatusText = statuses.First(n => n.CodeValue == response.Data.StatusCd).TextValue;
                }

                if (levels.Any(n => n.CodeValue == response.Data.Level.ToString()))
                {
                    response.Data.LevelText = levels.First(n => n.CodeValue == response.Data.Level.ToString()).TextValue;
                }
            }

            return response;
        }

        public async Task<ApiResponse<FormulasTagViewResult>> GetFormulasById(int formulasTagId, ApiRequest request)
        {
            ApiResponse<FormulasTagViewResult> response = new ApiResponse<FormulasTagViewResult>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new FormulasTagViewResult()
            };

            response.Data = await _formulasRepository.GetFormulasTagById(formulasTagId);

            return response;
        }

        public async Task<ApiResponse<int>> Update(int formulasId, ApiRequest<FormulasUpdateModel> request)
        {
            ApiResponse<int> response = new ApiResponse<int>
            {
                Ray = request.Ray ?? string.Empty,
                Data = 0
            };

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var objFormulas = await _formulasRepository.GetById(formulasId, request.Parameter.ClientId, request.Parameter.EntityId);
                    if (objFormulas == null)
                    {
                        response.Message.Toast.Add(new MessageItem()
                        {
                            Level =  Common.Constants.Level.ERROR,
                            LabelCode = Common.Constants.Label.PAY0306
                        });
                        scope.Dispose();
                        return response;
                    }
                    // Update Formulas
                    if (await _formulasRepository.Update(formulasId, request.Data) == null)
                    {
                        response.Message.Toast.Add(new MessageItem()
                        {
                            Level = Common.Constants.Level.ERROR,
                            LabelCode = Common.Constants.Label.PAY0304
                        });
                        scope.Dispose();
                        return response;
                    }
                    response.Data = formulasId;

                    // Update Formulas Tag
                    FormulasTagViewResult model = await _formulasRepository.GetFormulasTagFromFormulasId(formulasId);
                    string tag_name = Util.Parameter.HandleParameter(request.Data.Name);
                    if(model != null)
                    {
                        FormulasTagAddModel newModel = new FormulasTagAddModel()
                        {
                            Flag = 1,
                            FormulasId = formulasId,
                            TagName = tag_name,
                            Remark = request.Data.Description,
                            FormulasRepeat = formulasId
                        };
                        await _formulasRepository.UpdateFormulasTag(model.FormulasTagId, newModel);

                    }

                    response.Message.Toast.Add(new MessageItem()
                    {
                        Level = Common.Constants.Level.SUCCESS,
                        LabelCode = Common.Constants.Label.PAY0305
                    });
                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }
            return response;
        }
    }
}
