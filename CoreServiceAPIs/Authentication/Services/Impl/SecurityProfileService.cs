﻿using AutoMapper;
using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using CoreServiceAPIs.Common.Constants;
using CoreServiceAPIs.Common.Dtos;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Services.Impl
{
    public class SecurityProfileService : ISecurityProfileService
    {
        private readonly ISecurityProfileRepository _secProfileRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<SecurityProfileService> _logger;

        public SecurityProfileService(ISecurityProfileRepository secProfileRepository, IMapper mapper, ILogger<SecurityProfileService> logger)
        {
            _secProfileRepository = secProfileRepository;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> Create(ApiRequest<CreateSecProfileRequest> request, string username)
        {
            _logger.LogInformation($"CreateSecurityProfile --- Username: {username}");
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray != null ? request.Ray : string.Empty;
            response.Data = new Dictionary<string, object>();

            var id = await _secProfileRepository.Create(request, username);
            if (id == 0)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.T_SECPRO_ERROR,
                    Placeholder = new string[] { "SEC0702", "Exception!!!" }
                });
                response.Data = null;
                return response;
            }

            response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.SP01_I_001,
                Placeholder = new string[] { "SEC0702", "Create Successfully!" }
            });
            response.Data.Add("securityProfileId", id);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> Delete(int id, ApiRequest request)
        {
            _logger.LogInformation($"DeleteSecurityProfile --- SecurityProfileId: {id}");
            List<CommonLib.Dtos.MessageItem> validateResult = new List<CommonLib.Dtos.MessageItem>();
            //Validate
            if (!_secProfileRepository.CheckSecurityProfileById(id))
            {
                validateResult.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.SP01_E_001
                });
            }

            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray != null ? request.Ray : string.Empty;
            response.Data = new Dictionary<string, object>();

            if (validateResult.Count > 0)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.T_SECPRO_ERROR
                });
                response.Message.ValidateResult = validateResult;
                response.Data = null;
                return response;
            }

            var result = await _secProfileRepository.Delete(id);
            if (!result)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.T_SECPRO_ERROR
                });
                response.Data = null;
                return response;
            }
            response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.T_SECPRO_INFO
            });
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetAll(ApiRequest<PaginationRequest> request)
        {
            _logger.LogInformation("GetAllSecurityProfile");
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray != null ? request.Ray : string.Empty;
            response.Data = new Dictionary<string, object>();
            var securityPofiles = await _secProfileRepository.GetAll(request);
            var result = _mapper.Map<List<TbSecSecurityProfile>, List<SecurityProfileViewModel>>(securityPofiles);

            var pagination = new Pagination<SecurityProfileViewModel>();
            pagination.Page = request.Data.Page.Value;
            pagination.Size = request.Data.Size.Value;
            pagination.TotalPages = (securityPofiles.Count + request.Data.Size.Value - 1) / request.Data.Size.Value;
            pagination.TotalElements = securityPofiles.Count;

            var data = securityPofiles.Skip((request.Data.Page.Value - 1) * request.Data.Size.Value).Take(request.Data.Size.Value).ToList();
            pagination.NumberOfElements = data.Count;
            pagination.Content = result;
            response.Data.Add("securityPofiles", pagination);

            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetById(int id, ApiRequest request)
        {
            _logger.LogInformation($"GetBySecurityProfileId --- SecurityProfileId: {id}");
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray != null ? request.Ray : string.Empty;
            response.Data = new Dictionary<string, object>();
            var securityProfile = await _secProfileRepository.GetById(id);
            var result = _mapper.Map<TbSecSecurityProfile, SecurityProfileViewModel>(securityProfile);
            response.Data.Add("securityProfile", result);
            return response;
        }
        public async Task<ApiResponse<Dictionary<string, object>>> Update(int securityId, string username, ApiRequest<UpdateSecProfileRequest> request)
        {
            _logger.LogInformation($"UpdateSecurityProfile -- SecurityProfileId: {securityId} , Username: {username}");
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray != null ? request.Ray : string.Empty;
            response.Data = new Dictionary<string, object>();

            List<CommonLib.Dtos.MessageItem> validateResult = ValidateSecurityProfile(request.Data);
            if (validateResult.Count > 0)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.T_SECPRO_ERROR
                });
                response.Data = null;
                response.Message.ValidateResult = validateResult;
                return response;
            }
            //Validate
            if (!_secProfileRepository.CheckSecurityProfileById(securityId))
            {
                validateResult.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.SP01_E_001
                });
            }

            if (validateResult.Count > 0)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.T_SECPRO_ERROR
                });
                response.Message.ValidateResult = validateResult;
                response.Data = null;
                return response;
            }

            var result = await _secProfileRepository.Update(securityId, username, request);
            if (!result)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.T_SECPRO_ERROR
                });
                response.Data = null;
                return response;
            }

            response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.T_SECPRO_INFO
            });
            return response;
        }

        public List<CommonLib.Dtos.MessageItem> ValidateSecurityProfile(UpdateSecProfileRequest request)
        {
            _logger.LogInformation("ValidateSecurityProfile");
            List<CommonLib.Dtos.MessageItem> validateResult = new List<CommonLib.Dtos.MessageItem>();
            if (string.IsNullOrEmpty(request.Name))
            {
                validateResult.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.SP01_E_002
                });
            }
            return validateResult;
        }

        public async Task<List<SecurityProfileViewModel>> GetAllList(int clientId, int entityId)
        {
            _logger.LogInformation($"GetAllList --- ClientId: {clientId} , EntityId: {entityId}");
            // TODO get all the security profile
            return await _secProfileRepository.GetAllList(clientId, entityId);
        }

        public async Task<List<SecurityProfileMngViewModel>> GetAllGroups(ApiRequest<int> request)
        {
            _logger.LogInformation("GetAllSecurityProfileGroups");
            return await _secProfileRepository.GetAllGroups(request.Data);
        }

        public async Task<ApiResponse<bool>> Save(CreateSecProfileRequest request, string username)
        {
            _logger.LogInformation($"SaveSecurityProfile --- Username: {username}");
            ApiResponse<bool> response = new ApiResponse<bool>();

            foreach (var item in request.ProfileDetails)
            {
                bool checkDataAccessIdIsExist = await _secProfileRepository.CheckDataAccessIdIsExist(item.DataAccessGroupId);
                if (!checkDataAccessIdIsExist)
                {
                    response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                    {
                        Level = Level.ERROR,
                        LabelCode = Label.T_SECPRO_ERROR,
                        Placeholder = new string[] { "SEC0703", "Data Access is no longer exist." }
                    });
                    response.Data = false;
                    return response;
                }
                bool checkRoleGroupIdIsExist = await _secProfileRepository.CheckRoleGroupIdIsExist(item.RoleGroupId);
                if (!checkRoleGroupIdIsExist)
                {
                    response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                    {
                        Level = Level.ERROR,
                        LabelCode = Label.T_SECPRO_ERROR,
                        Placeholder = new string[] { "SEC0703", "Role Group is no longer exist." }
                    });
                    response.Data = false;
                    return response;
                }
            }
            bool checkSecurityProfileIsExist = await _secProfileRepository.CheckSecurityProfileIsExist(request.SecurityProfileId);
            if (!checkSecurityProfileIsExist)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.T_SECPRO_ERROR,
                    Placeholder = new string[] { "SEC0703", "No such Security Profile is found!" }
                });
                response.Data = false;
                return response;
            }

            response.Data = await _secProfileRepository.Save(request, username);

            if (!response.Data)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.T_SECPRO_ERROR,
                    Placeholder = new string[] { "SEC0703", "Saving Failed." }
                });
                response.Data = false;

            }
            else
            {

                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.SUCCESS,
                    LabelCode = Label.T_SECPRO_INFO
                });

            }

            return response;
        }
    }
}