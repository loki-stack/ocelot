﻿using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using CoreServiceAPIs.Common.Constants;
using CoreServiceAPIs.Common.Dtos;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Services.Impl
{
    public class SettingService : ISettingService
    {
        private readonly ISettingRepository _settingRepository;
        private readonly ILogger<SettingService> _logger;

        public SettingService(ISettingRepository settingRepository, ILogger<SettingService> logger)
        {
            _settingRepository = settingRepository;
            _logger = logger;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetCurrencySetting(ApiRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();

            TbCfgSetting setting = await _settingRepository.GetSettingOfClientEntity(request.Parameter.ClientId, request.Parameter.EntityId);
            if (setting == null)
                setting = await _settingRepository.CreateDefaultSetting(request.Parameter.ClientId, request.Parameter.EntityId);
            response.Data.Add("salaryCurrency", setting.SalaryCurrency);
            response.Data.Add("paymentCurrency", setting.PaymentCurrency);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetEmployeeSetting(ApiRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();

            TbCfgSetting setting = await _settingRepository.GetSettingOfClientEntity(request.Parameter.ClientId, request.Parameter.EntityId);
            if (setting == null)
                setting = await _settingRepository.CreateDefaultSetting(request.Parameter.ClientId, request.Parameter.EntityId);
            response.Data.Add("workCalendar", setting.WorkCalendar);
            response.Data.Add("probationPeriod", setting.ProbationPeriod);
            response.Data.Add("salaryCurrency", setting.SalaryCurrency);
            response.Data.Add("paymentCurrency", setting.PaymentCurrency);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetEntitySetting(ApiRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();

            TbCfgSetting setting = await _settingRepository.GetSettingOfClientEntity(request.Parameter.ClientId, request.Parameter.EntityId);
            if (setting == null)
                setting = await _settingRepository.CreateDefaultSetting(request.Parameter.ClientId, request.Parameter.EntityId);
            response.Data.Add("entitySetting", setting);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetLanguages(ApiRequest<string> request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();

            List<LanguageModel> languages = await _settingRepository.GetLanguages();
            response.Data.Add("languages", languages);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetLanguageSetting(ApiRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();

            TbCfgSetting setting = await _settingRepository.GetSettingOfClientEntity(request.Parameter.ClientId, request.Parameter.EntityId);
            if (setting == null)
                setting = await _settingRepository.CreateDefaultSetting(request.Parameter.ClientId, request.Parameter.EntityId);
            response.Data.Add("primaryLanguage", setting.PrimaryLanguage);
            response.Data.Add("secondaryLanguage", setting.SecondaryLanguage);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetPDFSetting(ApiRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();

            TbCfgSetting setting = await _settingRepository.GetSettingOfClientEntity(request.Parameter.ClientId, request.Parameter.EntityId);
            if (setting == null)
                setting = await _settingRepository.CreateDefaultSetting(request.Parameter.ClientId, request.Parameter.EntityId);
            response.Data.Add("pdfSetting", setting.PdfStatus);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetProbationPeriodSetting(ApiRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();

            TbCfgSetting setting = await _settingRepository.GetSettingOfClientEntity(request.Parameter.ClientId, request.Parameter.EntityId);
            if (setting == null)
                setting = await _settingRepository.CreateDefaultSetting(request.Parameter.ClientId, request.Parameter.EntityId);
            response.Data.Add("probationPeriodSetting", setting.ProbationPeriod);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetWorkCalendarSetting(ApiRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();

            TbCfgSetting setting = await _settingRepository.GetSettingOfClientEntity(request.Parameter.ClientId, request.Parameter.EntityId);
            if (setting == null)
                setting = await _settingRepository.CreateDefaultSetting(request.Parameter.ClientId, request.Parameter.EntityId);
            response.Data.Add("workCalendar", setting.WorkCalendar);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetSignInSetting(ApiRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();

            TbCfgSetting setting = await _settingRepository.GetSettingOfClientEntity(request.Parameter.ClientId, request.Parameter.EntityId);
            if (setting == null)
                setting = await _settingRepository.CreateDefaultSetting(request.Parameter.ClientId, request.Parameter.EntityId);
            response.Data.Add("signInMethod", setting.SignInMethod);
            return response;
        }

        public async Task<ApiResponse<string>> UpdateCurrencySetting(ApiRequest<SettingUpdateCurrencyRequest> request)
        {
            ApiResponse<string> response = new ApiResponse<string>();
            response.Ray = request.Ray;

            try
            {
                await _settingRepository.UpdateCurrencySetting(request.Parameter.ClientId, request.Parameter.EntityId, request.Data.SalaryCurrency, request.Data.PaymentCurrency);
                response.Data = "Success";
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.SUCCESS,
                    LabelCode = Label.SET0112
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.SET0111
                });
                response.Data = null;
            }
            return response;
        }

        public async Task<ApiResponse<string>> UpdateEmployeeSetting(ApiRequest<SettingUpdateEmployeeRequest> request)
        {
            ApiResponse<string> response = new ApiResponse<string>();
            response.Ray = request.Ray;

            try
            {
                await _settingRepository.UpdateEmployeeSetting(request.Parameter.ClientId, request.Parameter.EntityId, request.Data);
                response.Data = "Success";
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.SUCCESS,
                    LabelCode = Label.SET0116
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.SET0115
                });
                response.Data = null;
            }
            return response;
        }

        public async Task<ApiResponse<string>> UpdateLanguageSetting(ApiRequest<SettingUpdateLanguageRequest> request)
        {
            ApiResponse<string> response = new ApiResponse<string>();
            response.Ray = request.Ray;
            if (request.Data.PrimaryLanguageCode == request.Data.SecondaryLanguageCode)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.SET0105
                });
                response.Data = null;
                return response;
            }

            try
            {
                await _settingRepository.UpdateLanguageSetting(request.Parameter.ClientId, request.Parameter.EntityId, request.Data.PrimaryLanguageCode, request.Data.SecondaryLanguageCode);
                response.Data = "Success";
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.SUCCESS,
                    LabelCode = Label.SET0107
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.SET0106
                });
                response.Data = null;
            }
            return response;
        }

        public async Task<ApiResponse<string>> UpdatePDFSetting(ApiRequest<SettingUpdatePDFRequest> request)
        {
            ApiResponse<string> response = new ApiResponse<string>();
            response.Ray = request.Ray;
            try
            {
                await _settingRepository.UpdatePDFSetting(request.Parameter.ClientId, request.Parameter.EntityId, request.Data.PDFStatus);
                response.Data = "Success";
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.SUCCESS,
                    LabelCode = Label.SET0103
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.SET0102
                });
                response.Data = null;
            }
            return response;
        }

        public async Task<ApiResponse<string>> UpdateProbationPeriodSetting(ApiRequest<SettingUpdateProbationPeriodRequest> request)
        {
            ApiResponse<string> response = new ApiResponse<string>();
            response.Ray = request.Ray;
            try
            {
                await _settingRepository.UpdateProbationPeriodSetting(request.Parameter.ClientId, request.Parameter.EntityId, request.Data.ProbationPeriod);
                response.Data = "Success";
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.SUCCESS,
                    LabelCode = Label.SET0110
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.SET0109
                });
                response.Data = null;
            }
            return response;
        }

        public async Task<ApiResponse<string>> UpdateWorkCaldendarSetting(ApiRequest<SettingUpdateWorkCalendarRequest> request)
        {
            ApiResponse<string> response = new ApiResponse<string>();
            response.Ray = request.Ray;
            try
            {
                await _settingRepository.UpdateWorkCalendarSetting(request.Parameter.ClientId, request.Parameter.EntityId, request.Data.WorkCalendar);
                response.Data = "Success";
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.SUCCESS,
                    LabelCode = Label.SET0114
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.SET0113
                });
                response.Data = null;
            }
            return response;
        }

        public async Task<ApiResponse<string>> UpdateSignInMethodSetting(ApiRequest<SettingSignInMethodRequest> request)
        {
            ApiResponse<string> response = new ApiResponse<string>();
            response.Ray = request.Ray;
            try
            {
                await _settingRepository.UpdateSignInMethod(request.Parameter.ClientId, request.Parameter.EntityId, request.Data.SignInMethod);
                response.Data = "Success";
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.SUCCESS,
                    LabelCode = Label.SET0118
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.SET0117
                });
                response.Data = null;
            }
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetNotificationEmailSetting(ApiRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();

            TbCfgSetting setting = await _settingRepository.GetSettingOfClientEntity(request.Parameter.ClientId, request.Parameter.EntityId);
            if (setting == null)
                setting = await _settingRepository.CreateDefaultSetting(request.Parameter.ClientId, request.Parameter.EntityId);
            response.Data.Add("notificationEmail", setting.NotificationEmail);
            return response;
        }

        public async Task<ApiResponse<string>> UpdateNotificationEmailSetting(ApiRequest<SettingNotificationEmailRequest> request)
        {
            ApiResponse<string> response = new ApiResponse<string>();
            response.Ray = request.Ray;
            try
            {
                await _settingRepository.UpdateNotificationEmail(request.Parameter.ClientId, request.Parameter.EntityId, request.Data.NotificationEmail);
                response.Data = "Success";
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.SUCCESS,
                    LabelCode = Label.SET0120
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.SET0119
                });
                response.Data = null;
            }
            return response;
        }
    }
}