﻿using AutoMapper;
using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using CoreServiceAPIs.Common.Constants;
using CoreServiceAPIs.Common.Dtos;
using CoreServiceAPIs.Common.Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreServiceAPIs.Authentication.Services.Impl
{
    public class RoleGroupService : IRoleGroupService
    {
        private readonly IRoleGroupRepository _roleGroupRepository;
        private readonly IFunctionRepository _functionRepository;
        private readonly ICfgI18nRepository _cfgI18nRepository;
        private readonly IMapper _mapper;

        private readonly ILogger<RoleGroupService> _logger;

        public RoleGroupService(IRoleGroupRepository roleGroupRepository, IFunctionRepository functionRepository,
            ICfgI18nRepository cfgI18nRepository, IMapper mapper, ILogger<RoleGroupService> logger)
        {
            _roleGroupRepository = roleGroupRepository;
            _functionRepository = functionRepository;
            _cfgI18nRepository = cfgI18nRepository;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> Create(ApiRequest<CreateRoleGroupRequest> request, string username)
        {
            _logger.LogInformation($"CreateRoleGroup --- Username: {username}");
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray != null ? request.Ray : string.Empty;
            response.Data = new Dictionary<string, object>();
            var id = 0;
            if (request.Data.FuncIds == null)
            {
                id = await _roleGroupRepository.QuickCreate(request, username);
            }
            else
            {
                id = await _roleGroupRepository.Create(request, username);
            }
            if (id == 0)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.RG01_E_001,
                });
                response.Data = null;
                return response;
            }

            response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.RG01_I_001, // add successfully 
            });
            response.Data.Add("roleGroupId", id);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> Delete(int id, ApiRequest request)
        {
            _logger.LogInformation($"DeleteRoleGroup --- RoleGroupId: {id}");
            List<CommonLib.Dtos.MessageItem> validateResult = new List<CommonLib.Dtos.MessageItem>();
            //Validate
            if (!_roleGroupRepository.CheckRoleGroupById(id))
            {
                validateResult.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.RG01_E_002, // Role Group does not exists.
                });
            }

            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray != null ? request.Ray : string.Empty;
            response.Data = new Dictionary<string, object>();

            if (validateResult.Count > 0)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.RG01_E_003, // Validation error.
                });
                response.Data = null;
                response.Message.ValidateResult = validateResult;
                return response;
            }

            var result = await _roleGroupRepository.Delete(id);
            if (!result)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.RG01_E_004, // Role Group delete failed.
                });
                response.Data = null;
                return response;
            }
            response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.RG01_I_002, // role group deleted.
            });
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetAll(ApiRequest<PaginationRequest> request)
        {
            _logger.LogInformation("GetAllRoleGroup");
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray != null ? request.Ray : string.Empty;
            response.Data = new Dictionary<string, object>();
            var roleGroups = await _roleGroupRepository.GetAll(request);
            var pagination = new Pagination<RoleGroupViewModel>();
            pagination.Page = request.Data.Page.Value;
            pagination.Size = request.Data.Size.Value;
            pagination.TotalPages = (roleGroups.Count + request.Data.Size.Value - 1) / request.Data.Size.Value;
            pagination.TotalElements = roleGroups.Count;

            var data = roleGroups.Skip((request.Data.Page.Value - 1) * request.Data.Size.Value).Take(request.Data.Size.Value).ToList();
            pagination.NumberOfElements = data.Count;
            pagination.Content = data;
            response.Data.Add("roleGroups", pagination);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetById(int id, ApiRequest request)
        {
            _logger.LogInformation($"GetRoleGroupById --- RoleGroupId: {id}");
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray != null ? request.Ray : string.Empty;
            response.Data = new Dictionary<string, object>();
            var roleGroup = await _roleGroupRepository.GetById(id);
            response.Data.Add("roleGroup", roleGroup);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> Update(ApiRequest<CreateRoleGroupRequest> request, string username)
        {
            _logger.LogInformation($"UpdateRoleGroup --- Username: {username}");
            List<CommonLib.Dtos.MessageItem> validateResult = new List<CommonLib.Dtos.MessageItem>();
            //Validate
            if (!_roleGroupRepository.CheckRoleGroupById(request.Data.RoleGroupId))
            {
                validateResult.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.RG01_E_002, // not exists
                });
            }
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray != null ? request.Ray : string.Empty;
            response.Data = new Dictionary<string, object>();

            if (validateResult.Count > 0)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.RG01_E_003, // validation error
                });
                response.Message.ValidateResult = validateResult;
                response.Data = null;
                return response;
            }

            var result = await _roleGroupRepository.Update(request, username);
            if (!result)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.RG01_E_005, // role group add failed
                });
                response.Data = null;
                return response;
            }

            response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.RG01_I_003 // update successfully
            });
            return response;
        }

        public async Task<List<TreeNodeDto<FuncItemDto>>> GetAllFuncAsTree(string locale, int clientId, int entityId)
        {
            _logger.LogInformation($"GetAllFuncAsTree --- ClientId: {clientId} , EntityId: {entityId}");
            // throw new NotImplementedException();
            var allFuncs = await _functionRepository.GetAllWithSort();
            HashSet<string> keys = new HashSet<string>();
            foreach (var secFunction in allFuncs)
            {
                keys.Add("FAR." + secFunction.ModuleCd);
                keys.Add("FAR." + secFunction.FeatureCd);
                keys.Add("FAR." + secFunction.FunctionCd);
            }

            Dictionary<string, string> dictFunc = await _cfgI18nRepository.GetTextDictionary(locale, clientId, entityId, keys);

            List<TreeNodeDto<FuncItemDto>> root = ToTreeNode(allFuncs, dictFunc);
            return root;
        }

        public List<TreeNodeDto<FuncItemDto>> ToTreeNode(List<TbSecFunction> functions, Dictionary<string, string> dictFunc)
        {
            _logger.LogInformation("ToTreeNode");
            List<TreeNodeDto<FuncItemDto>> root = new List<TreeNodeDto<FuncItemDto>>();
            string prevModule = string.Empty;
            string prevFeature = string.Empty;
            TreeNodeDto<FuncItemDto> moduleNode = new TreeNodeDto<FuncItemDto>();
            TreeNodeDto<FuncItemDto> featureNode = new TreeNodeDto<FuncItemDto>();
            foreach (var secFunction in functions)
            {
                // check module
                if (!prevModule.Equals(secFunction.ModuleCd))
                {
                    _logger.LogInformation($"new module: {secFunction.ModuleCd}");
                    prevModule = secFunction.ModuleCd;
                    moduleNode = new TreeNodeDto<FuncItemDto>();
                    moduleNode.Key = secFunction.ModuleCd + secFunction.FunctionId;
                    moduleNode.IsSelectable = true;
                    moduleNode.Data = newFuncItemDto(secFunction.FunctionId, "Module", secFunction.ModuleCd, dictFunc);
                    root.Add(moduleNode);
                }

                // check feature
                if (!prevFeature.Equals(secFunction.FeatureCd))
                {
                    _logger.LogInformation($"new feature: {secFunction.FeatureCd}");
                    prevFeature = secFunction.FeatureCd;
                    featureNode = new TreeNodeDto<FuncItemDto>();
                    featureNode.Key = secFunction.FeatureCd + secFunction.FunctionId;
                    featureNode.IsSelectable = true;
                    featureNode.Data = newFuncItemDto(secFunction.FunctionId, "Feature", secFunction.FeatureCd, dictFunc);
                    moduleNode.Children.Add(featureNode);
                }

                // function node
                TreeNodeDto<FuncItemDto> funcNode = new TreeNodeDto<FuncItemDto>();
                funcNode.IsSelectable = true;
                funcNode.Key = secFunction.FunctionCd + secFunction.FunctionId;
                funcNode.Data = newFuncItemDto(secFunction.FunctionId, "Function", secFunction.FunctionCd, dictFunc);
                featureNode.Children.Add(funcNode);
            }

            return root;
        }

        private FuncItemDto newFuncItemDto(int id, string itemType, string itemCode, Dictionary<string, string> dictFunc)
        {
            _logger.LogInformation($"newFuncItemDto --- ItemId: {id}, ItemType: {itemType}, ItemCode: {itemCode}");
            var itemText = string.Empty;
            if (!dictFunc.TryGetValue("FAR." + itemCode, out itemText))
            {
                itemText = itemCode;
            }
            return new FuncItemDto
            {
                ItemId = id,
                ItemType = itemType,
                ItemCode = itemCode,
                ItemText = itemText
            };
        }

        public async Task<List<RoleGroupViewModel>> GetEntityRoleGroup(int clientId, int entityId)
        {
            _logger.LogInformation($"GetEntityRoleGroup --- ClientId: {clientId} , EntityId: {entityId}");
            return await _roleGroupRepository.GetEntityRoleGroup(clientId, entityId);
        }

        public async Task<ApiResponse<FarManagementDto>> GetManageData(int id, string locale)
        {
            _logger.LogInformation($"GetManageData --- RolegroupId: {id}");
            ApiResponse<FarManagementDto> response = new ApiResponse<FarManagementDto>();
            FarManagementDto data = new FarManagementDto();

            //
            List<TreeNodeDto<FuncItemDto>> funcList = await this.GetAllFuncAsTree(locale, 0, 0);
            // var allFuncGroup = _roleGroupRepository.GetAll();
            RoleGroupViewModel rgroup = await _roleGroupRepository.GetById(id);
            if (rgroup != null)
            {
                foreach (var func in rgroup.Funcs)
                {
                    markFuncTrue(func, funcList);
                }
            }

            data.FuncList = funcList;
            response.Data = data;
            return response;
        }

        private void markFuncTrue(RoleFunctionViewModel func, List<TreeNodeDto<FuncItemDto>> list)
        {
            _logger.LogInformation("MarkFuncTrue");
            int id = func.FunctionId;
            foreach (var module in list)
            {
                foreach (var feature in module.Children)
                {
                    foreach (var funcItem in feature.Children)
                    {
                        if (funcItem.Data.ItemId == id)
                        {
                            funcItem.Data.IsSelected = true;
                            return; // exit
                        }
                    }
                }
            }
        }
    }
}