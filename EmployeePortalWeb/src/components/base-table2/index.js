import React, { Component } from "react";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { Paginator } from "primereact/paginator";
import { Button } from "primereact/button";
import SidebarUpdate from "./components/sidebar";
import FilterSearchBox from "./components/filter-component/filter-search-box";
import FilterDropdown from "./components/filter-component/filter-dropdown";
import FilterDate from "./components/filter-component/filter-date";
import {
  STATUS,
  REG_CHECK_FIRST_SPACE,
  ARR_IPP,
  ACTIONS_KEY,
} from "./constant";
import moment from "moment";
import "./index.scss";

class BaseTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lstData: props.lstData,
      filter: {},
    };
  }

  componentDidMount() {}

  componentDidUpdate = (prevProps, prevState) => {
    const {
      sortField: prevSortField = "",
      sortOrder: prevSortOrder = "",
      filter: prevFilter = {},
      paginatorFirst: prevPaginationFirst,
      paginatorRows: prevPaginationRows,
    } = prevState;
    const {
      tableHeight = 0,
      sortField = "",
      sortOrder = "",
      filter = {},
      paginatorFirst,
      paginatorRows,
    } = this.state;
    const { lstData: propsLstData = [] } = this.props;
    const { lstData: prevPropsLstData = [] } = prevProps;

    const currentTableHeight = document.getElementById(
      "base_table2-table-container"
    ).offsetHeight;

    if (
      JSON.stringify(prevPropsLstData) !== JSON.stringify(propsLstData) ||
      tableHeight !== currentTableHeight ||
      prevSortField !== sortField ||
      prevSortOrder !== sortOrder ||
      prevPaginationFirst !== paginatorFirst ||
      prevPaginationRows !== paginatorRows ||
      JSON.stringify(prevFilter) !== JSON.stringify(filter)
    ) {
      this.setState({ tableHeight: currentTableHeight }, this.updateCssTable);
    }

    if (JSON.stringify(propsLstData) !== JSON.stringify(prevPropsLstData)) {
      this.setState({ lstData: propsLstData });
    }
  };

  componentWillUnmount() {
    this.removeEventModalAction();
  }

  renderColumns = (arrColumn) => {
    const { customSort } = this.props;
    const { openFilter, filter: stateFilter = {} } = this.state;
    const elementTable = document.getElementById("base_table2-table");

    return arrColumn.map((column, index) => {
      const {
        field = "",
        header = "",
        sortable = false,
        filter = {
          type: "",
          value: "",
          options: [],
          matchMode: "",
          valueFormat: "",
          itemTemplate: undefined,
        },
        headerStyle = { backgroundColor: "#e0e0e0" },
        frozen,
        lastFrozen = false,
      } = column || {};

      let { otherProps = {} } = column || {};
      if (sortable && customSort) {
        otherProps = {
          ...otherProps,
          sortFunction: (e) => this.customSortFunc(e, customSort),
        };
      }

      return (
        <Column
          key={field}
          columnKey={field}
          field={field}
          header={header}
          headerStyle={headerStyle}
          filter={openFilter}
          filterElement={
            <>
              {filter.type === "input" && (
                <FilterSearchBox
                  value={stateFilter[field]?.value || ""}
                  keyFilter={field}
                  placeholder={filter.placeholder || `Search by ${header}`}
                  onChange={this.handleChangeInputFilter}
                />
              )}
              {filter.type === "dropdown" && (
                <FilterDropdown
                  value={stateFilter[field]?.value || ""}
                  keyFilter={field}
                  options={filter.options}
                  placeholder={filter.placeholder || `Search by ${header}`}
                  onChange={this.handleChangeInputFilter}
                  appendTo={elementTable}
                  itemTemplate={
                    filter.itemTemplate ||
                    (field === "statusCd" && this.renderStatus)
                  }
                  valueTemplate={filter.valueTemplate}
                />
              )}
              {filter.type === "date" && (
                <FilterDate
                  value={stateFilter[field]?.value || ""}
                  keyFilter={field}
                  placeholder={filter.placeholder || `Search by ${header}`}
                  onChange={this.handleChangeInputFilter}
                  appendTo={elementTable}
                />
              )}
              {!filter.type && <div style={{ height: 35 }} />}
            </>
          }
          filterMatchMode={filter.matchMode}
          sortable={sortable}
          frozen={frozen || lastFrozen}
          bodyStyle={lastFrozen ? { borderRight: "1px solid #e9ecef" } : {}}
          body={field === "statusCd" && this.renderStatus}
          {...otherProps}
        ></Column>
      );
    });
  };

  customSortFunc = (e, customSort) => {
    const { lstData = [] } = this.props;
    const { customSortData = [], filter = {} } = this.state;
    let result = customSort(e, lstData);
    result = this.filterFunc(filter, result);

    if (JSON.stringify(result) !== JSON.stringify(customSortData)) {
      this.setState({ customSortData: result });
    }

    return result;
  };

  handleChangeInputFilter = (value = "", key, type = "") => {
    if (REG_CHECK_FIRST_SPACE.test(value)) return;

    const { lstData: propsLstData = [] } = this.props;
    const { filter = {} } = this.state;

    let newFilter = { ...filter, [key]: { value, type } };
    let newLstData = [...propsLstData];

    newLstData = this.filterFunc(newFilter, newLstData);

    this.setState({
      lstData: newLstData,
      filter: newFilter,
    });
  };

  filterFunc = (filter, lstData) => {
    let newLstData = [...lstData];

    Object.keys(filter).forEach((keyF) => {
      const valueF = filter[keyF].value;
      const typeF = filter[keyF].type;

      newLstData = newLstData.filter((item) => {
        if (typeF === "date") {
          return (
            !valueF || moment(item[keyF]).isSame(valueF.toString(), "date")
          );
        } else if (typeF === "dropdown") {
          return !valueF || item[keyF].replaceAll("\n", " ") === valueF;
        } else if (typeF === "input") {
          return item[keyF]
            .replaceAll("\n", " ")
            .toLowerCase()
            .includes(valueF.toLowerCase());
        } else return false;
      });
    });

    return newLstData;
  };

  renderStatus = (rowData) => {
    let { statusCd = "", label = "" } = rowData || {};
    return (
      <div className="status-container">
        {statusCd.trim() === STATUS.ACTIVE ? (
          <>
            <div className="status-icon status-active" />
            &nbsp;Active
          </>
        ) : statusCd.trim() === STATUS.INACTIVE ? (
          <>
            <div className="status-icon status-inactive" />
            &nbsp;Inactive
          </>
        ) : (
          <>{label}</>
        )}
      </div>
    );
  };

  iconActions = () => {
    return (
      <>
        <i className="pi pi-ellipsis-v" />
      </>
    );
  };

  openModalActions = (e) => {
    const { paginatorRows = 10 } = this.state;
    const coordinateModalActions = this.getCoordinateModalActions(
      e.index % paginatorRows
    );

    this.setState(
      {
        isOpenModalActions: true,
        rowData: e.data,
        coordinateModalActions,
        indexBtnActions: e.index,
      },
      this.addEventModalAction
    );
  };

  getCoordinateModalActions = (indexBtnActions) => {
    const tableActions = document.getElementById("table-actions");
    if (!tableActions) return;

    const lstBtnActions = tableActions.querySelectorAll("td");
    if (indexBtnActions > lstBtnActions.length - 1) return;

    const btnTarget = lstBtnActions[indexBtnActions];
    const rectTarget = btnTarget.getBoundingClientRect();
    return { x: rectTarget.right, y: rectTarget.top };
  };

  handleResizeWhenOpenModalActions = () => {
    const { indexBtnActions } = this.state;
    if (indexBtnActions === -1) return;

    const coordinateModalActions = this.getCoordinateModalActions(
      indexBtnActions
    );
    this.setState({ coordinateModalActions });
  };

  handleClickOutsideModalActions = (e) => {
    if (this._modalActionsRef && this._modalActionsRef.contains(e.target)) {
      return;
    }
    this.setState(
      {
        isOpenModalActions: false,
        coordinateModalActions: {},
        rowData: {},
      },
      this.removeEventModalAction
    );
  };

  funcVisibleSidebar = (visibleSidebarUpdate) => {
    this.setState(
      {
        visibleSidebarUpdate,
        isOpenModalActions: false,
        coordinateModalActions: {},
      },
      () => this.removeEventModalAction
    );
  };

  addEventModalAction = () => {
    window.addEventListener("resize", this.handleResizeWhenOpenModalActions);
    document.addEventListener("click", this.handleClickOutsideModalActions);
  };

  removeEventModalAction = () => {
    window.removeEventListener("resize", this.handleResizeWhenOpenModalActions);
    document.removeEventListener("click", this.handleClickOutsideModalActions);
  };

  handleSaveSideBar = () => {
    const { onSave } = this.props.sidebarData || {};
    if (onSave) onSave();
    this.setState({ visibleSidebarUpdate: false });
  };

  handleHideSideBar = () => {
    const { onCancel } = this.props.sidebarData || {};
    if (onCancel) onCancel();
    this.setState({ visibleSidebarUpdate: false });
  };

  updateCssTable = () => {
    const arrElementTableBody = document.getElementsByClassName(
      "p-datatable-tbody"
    );
    const arrElementTableHead = document.getElementsByClassName(
      "p-datatable-thead"
    );
    if (arrElementTableHead.length > 0) {
      const arrTable0Tr =
        arrElementTableHead[0] &&
        arrElementTableHead[0].getElementsByTagName("tr");
      const arrTable1Tr =
        arrElementTableHead[1] &&
        arrElementTableHead[1].getElementsByTagName("tr");
      const arrTable2Tr =
        arrElementTableHead[2] &&
        arrElementTableHead[2].getElementsByTagName("tr");

      if (arrTable0Tr && arrTable1Tr && arrTable2Tr) {
        for (let index = 0; index < arrTable0Tr.length; index++) {
          if (!arrTable0Tr[index] || !arrTable1Tr[index] || !arrTable2Tr[index])
            break;

          arrTable0Tr[index].style.removeProperty("height");
          arrTable1Tr[index].style.removeProperty("height");
          arrTable2Tr[index].style.removeProperty("height");

          const maxHeight = Math.max(
            arrTable0Tr[index].offsetHeight,
            arrTable1Tr[index].offsetHeight,
            arrTable2Tr[index].offsetHeight
          );

          arrTable0Tr[index].style.height = `${maxHeight}px`;
          arrTable1Tr[index].style.height = `${maxHeight}px`;
          arrTable2Tr[index].style.height = `${maxHeight}px`;
        }
      }
    }
    if (arrElementTableBody.length > 0) {
      const arrTable0Tr =
        arrElementTableBody[0] &&
        arrElementTableBody[0].getElementsByTagName("tr");
      const arrTable1Tr =
        arrElementTableBody[1] &&
        arrElementTableBody[1].getElementsByTagName("tr");
      const arrTable2Tr =
        arrElementTableBody[2] &&
        arrElementTableBody[2].getElementsByTagName("tr");

      if (arrTable0Tr && arrTable1Tr && arrTable2Tr) {
        for (let index = 0; index < arrTable0Tr.length; index++) {
          if (!arrTable0Tr[index] || !arrTable1Tr[index] || !arrTable2Tr[index])
            break;

          arrTable0Tr[index].style.removeProperty("height");
          arrTable1Tr[index].style.removeProperty("height");
          arrTable2Tr[index].style.removeProperty("height");

          const maxHeight = Math.max(
            arrTable0Tr[index].offsetHeight,
            arrTable1Tr[index].offsetHeight,
            arrTable2Tr[index].offsetHeight
          );

          arrTable0Tr[index].style.height = `${maxHeight}px`;
          arrTable1Tr[index].style.height = `${maxHeight}px`;
          arrTable2Tr[index].style.height = `${maxHeight}px`;
        }
      }
    }
  };

  render() {
    const {
      lstData = [],
      sortField,
      sortOrder,
      paginatorFirst = 0,
      paginatorRows = 10,
      coordinateModalActions = { x: 0, y: 0 },
      isOpenModalActions = false,
      typeUpdate = "",
      rowData = {},
      openFilter,
      visibleSidebarUpdate,
      customSortData,
    } = this.state;

    const {
      customWidthFrozen = 300,
      loading = true,
      formTable,
      prePageContent = null,
      mainLabel = "",
      subLabel = "",
      actions = [],
      headActionButtons = {},
      sidebarData = {},
      propsDirectAction,
      reOrderRows = false,
      customSort,
    } = this.props;

    const { addButton = {}, otherButtons = [], customFilter = {} } =
      headActionButtons || {};

    return (
      <>
        {prePageContent}
        <div className="base_table2-page">
          {/* HEADER */}
          <div className="head_label">
            <div className="head_label-main">{mainLabel || "Main label"}</div>
            <div className="head_label-sub">
              {subLabel || "Additional description if needed"}
            </div>
          </div>

          {/* HEAD ACTIONS */}
          <div className={`head_actions ${headActionButtons.className}`}>
            <div className="head_actions-search_filter">
              <i
                className={customFilter?.icon || "pi pi-filter"}
                onClick={(e) => {
                  if (customFilter?.action) {
                    customFilter.action(e);
                  } else this.setState({ openFilter: !openFilter });
                }}
              />
              {customFilter.extend}
            </div>

            <div
              className={`head_actions-button ${headActionButtons.className}`}
            >
              <Button
                label={addButton?.label || "Add"}
                className={addButton?.className || "p-button-danger"}
                onClick={() => {
                  propsDirectAction(
                    addButton?.keyAction || ACTIONS_KEY.ADD,
                    {},
                    this.funcVisibleSidebar
                  );
                }}
                style={addButton?.order ? { order: addButton?.order } : {}}
              />
              {otherButtons.length > 0 &&
                otherButtons.map((button, index) => (
                  <Button
                    key={index}
                    label={button.label}
                    className={button.className}
                    onClick={button.onClick}
                  />
                ))}
            </div>
          </div>

          <div id="base_table2-table" className="base_table2-table">
            <div
              id="base_table2-table-container"
              className="base_table2-table-container"
            >
              <div className="base_table2-page_1">
                <DataTable
                  ref={(ref) => (this.dt = ref)}
                  value={lstData || []}
                  scrollable
                  frozenWidth={`${
                    reOrderRows ? customWidthFrozen + 42 : customWidthFrozen
                  }px`}
                  loading={loading}
                  tableClassName="base_table2-table-normal"
                  removableSort
                  sortField={sortField}
                  sortOrder={sortOrder}
                  onSort={(e) =>
                    this.setState({
                      sortField: e.sortField,
                      sortOrder: e.sortOrder,
                    })
                  }
                  first={paginatorFirst || 0}
                  rows={paginatorRows || 10}
                  onPage={(e) =>
                    this.setState({
                      paginatorFirst: e.first,
                      paginatorRows: e.rows,
                    })
                  }
                  reorderableColumns={reOrderRows}
                  onRowReorder={(e) => {
                    if (sortField || sortOrder) return;
                    this.setState({ lstData: e.value });
                  }}
                >
                  {reOrderRows && (
                    <Column
                      frozen
                      rowReorder
                      headerStyle={{
                        width: "42px",
                        backgroundColor: "#e0e0e0",
                      }}
                    />
                  )}
                  {this.renderColumns(formTable)}
                </DataTable>
              </div>

              <div className="base_table2-page_2">
                <DataTable
                  id="table-actions"
                  ref={(el) => (this.dt_actions = el)}
                  value={
                    customSort && sortField && sortOrder
                      ? customSortData
                      : lstData || []
                  }
                  scrollable
                  frozenWidth="81px"
                  loading={loading}
                  tableClassName="base_table2-table-normal"
                  sortField={sortField}
                  sortOrder={sortOrder}
                  onSort={(e) => {}}
                  onRowClick={this.openModalActions}
                  first={paginatorFirst || 0}
                  rows={paginatorRows || 10}
                  onPage={(e) => {
                    this.setState({
                      paginatorFirst: e.first,
                      paginatorRows: e.rows,
                    });
                  }}
                >
                  <Column
                    field="action"
                    header="Action"
                    body={this.iconActions}
                    headerStyle={{
                      backgroundColor: "#e0e0e0",
                      width: "80px",
                      borderLeft: "1px solid #e9ecef",
                      cursor: "default",
                    }}
                    className="col-action"
                    bodyClassName="col-action col-action-body"
                    columnKey="action"
                    frozen
                    filter={openFilter}
                    filterElement={<div style={{ height: 35 }} />}
                    sortable
                  ></Column>
                </DataTable>
              </div>
            </div>

            {/* PAGINATION */}
            <Paginator
              first={paginatorFirst || 0}
              rows={paginatorRows || 10}
              totalRecords={lstData.length}
              rowsPerPageOptions={ARR_IPP}
              onPageChange={(e) => {
                this.setState({
                  paginatorFirst: e.first,
                  paginatorRows: e.rows,
                });
              }}
              template="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink RowsPerPageDropdown"
              currentPageReportTemplate="Showing {first} to {last} of {totalRecords}"
            />
          </div>

          {/* ACTIONS MODAL */}
          <div
            className="actions-modal-container"
            style={isOpenModalActions ? { display: "block" } : {}}
          >
            <div
              ref={(ref) => (this._modalActionsRef = ref)}
              className="actions-modal"
              style={
                isOpenModalActions
                  ? {
                      left: coordinateModalActions.x - 100,
                      top: coordinateModalActions.y,
                    }
                  : {}
              }
            >
              {actions.map((action, index) => (
                <div
                  key={action.key}
                  className="actions-modal-item"
                  style={index > 0 ? { borderTop: "1px solid #e9ecef" } : {}}
                  onClick={async () => {
                    if (propsDirectAction)
                      propsDirectAction(
                        action.key,
                        rowData,
                        this.funcVisibleSidebar
                      );
                  }}
                >
                  {action.label}&nbsp;
                  <i className={`pi ${action.icon}`} />
                </div>
              ))}
            </div>
          </div>

          {/* SIDEBAR */}
          <SidebarUpdate
            visible={visibleSidebarUpdate}
            type={typeUpdate}
            rowData={typeUpdate === "edit" ? rowData : {}}
            unitId={rowData.unitId}
            onHide={this.handleHideSideBar}
            onSave={this.handleSaveSideBar}
            sidebarData={sidebarData}
          />
        </div>
      </>
    );
  }
}
export default BaseTable;
