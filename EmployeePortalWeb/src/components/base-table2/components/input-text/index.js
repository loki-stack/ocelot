import React from "react";
import { InputText } from "primereact/inputtext";

const CustomInputText = (props) => {
  const { field, inputProps = {}, errors = {}, label, required } = props;
  return (
    <div className="p-field">
      <label
        className={required ? "p-label-required" : undefined}
        htmlFor={field}
      >
        {label}
      </label>
      <div>
        <InputText {...inputProps} />
        {errors[field] && (
          <small className="p-invalid p-d-block">{errors[field].message}</small>
        )}
      </div>
    </div>
  );
};
export default CustomInputText;
