import React from "react";
import { InputSwitch } from "primereact/inputswitch";

const CustomInputSwitch = (props) => {
  const {
    inputProps = {},
    label,
    field,
    required,
    arrOptions = ["", ""],
  } = props;
  const { value } = inputProps;
  const checked = value === arrOptions[0].value;

  return (
    <div className="p-field p-field-status">
      <label
        className={required ? "p-label-required" : undefined}
        htmlFor={field}
      >
        {label}
      </label>
      <div className="status-container">
        <InputSwitch
          id={field}
          name={field}
          checked={checked}
          {...inputProps}
        />
        <div className="status-value_label">
          {checked ? arrOptions[0].label : arrOptions[1].label}
        </div>
      </div>
    </div>
  );
};

export default CustomInputSwitch;
