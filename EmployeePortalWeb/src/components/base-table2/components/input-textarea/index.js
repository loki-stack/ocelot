import React from "react";
import { InputTextarea } from "primereact/inputtextarea";

const CustomInputTextarea = (props) => {
  const { inputProps = {}, errors = {}, label, field, required } = props;
  const { name = "" } = inputProps;

  return (
    <div className="p-field">
      <label
        className={required ? "p-label-required" : undefined}
        htmlFor={field}
      >
        {label}
      </label>
      <div>
        <InputTextarea id={field} name={field} {...inputProps} />
        {errors[name] && (
          <small className="p-invalid p-d-block">{errors[name].message}</small>
        )}
      </div>
    </div>
  );
};
export default CustomInputTextarea;
