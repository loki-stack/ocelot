import React from "react";
import { InputText } from "primereact/inputtext";

const FilterSearchBox = (props) => {
  return (
    <InputText
      type="text"
      value={props.value}
      placeholder={props.placeholder || "Filter by Input"}
      onChange={(e) => props.onChange(e.target.value, props.keyFilter, "input")}
      required
    />
  );
};
export default FilterSearchBox;
