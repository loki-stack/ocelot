import React from "react";
import { Dropdown } from "primereact/dropdown";

const FilterDropdown = (props) => {
  return (
    <Dropdown
      value={props.value}
      options={props.options}
      onChange={(e) =>
        props.onChange(e.target.value, props.keyFilter, "dropdown")
      }
      itemTemplate={props.itemTemplate}
      valueTemplate={props.valueTemplate}
      placeholder={props.placeholder || "Filter by Dropdown"}
      className="p-column-filter"
      appendTo={props.appendTo}
      optionLabel={props.optionLabel}
      optionValue={props.optionValue}
    />
  );
};
export default FilterDropdown;
