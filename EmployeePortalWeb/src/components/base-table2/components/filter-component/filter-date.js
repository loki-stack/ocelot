import React from "react";
import { Calendar } from "primereact/calendar";

const FilterDate = (props) => {
  return (
    <Calendar
      value={props.value}
      onChange={(e) => props.onChange(e.value, props.keyFilter, "date")}
      placeholder={props.placeholder || "Filter by Date"}
      dateFormat="dd/mm/yy"
      className="p-column-filter"
      appendTo={props.appendTo}
    />
  );
};
export default FilterDate;
