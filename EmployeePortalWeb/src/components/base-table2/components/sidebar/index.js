import React, { Component } from "react";
import { connect } from "react-redux";
import { Sidebar } from "primereact/sidebar";
import Button from "../button";
import "../index.scss";

class SidebarUpdate extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidUpdate(prevProps) {}

  loadDataOnShow = async () => {};

  handleSave = () => {
    this.props.onSave();
  };

  handleCancel = () => {
    this.props.onHide();
  };

  handleChangeValue = (e, key) => {};

  render() {
    const { visible, sidebarData = {} } = this.props;
    const { header = {}, body, disableSave, hideBottomButton } =
      sidebarData || {};

    return (
      <Sidebar
        visible={visible}
        position="right"
        className="ui-sidebar-lg sidebar_update"
        onShow={this.loadDataOnShow}
        onHide={this.handleCancel}
      >
        <div className="sidebar_update-main_content">
          <div className="sidebar_update-header">
            <div className="sidebar_update-header-main">
              {header.main || "Main label"}
            </div>
            <div className="sidebar_update-header-sub">
              {header.sub || "Additional description if needed"}
            </div>
          </div>

          <div className="p-fluid sidebar_update-form">{body}</div>
        </div>

        {!hideBottomButton && (
          <div className="form_buttons-container">
            <Button
              label="Cancel"
              className="form_buttons p-button-secondary"
              onClick={this.handleCancel}
            />
            <Button
              label="Save"
              className="form_buttons p-button-danger"
              onClick={this.handleSave}
              disabled={disableSave}
            />
          </div>
        )}
      </Sidebar>
    );
  }
}

const mapStateToProps = (state) => ({
  authInfo: state.auth,
});

export default connect(mapStateToProps)(SidebarUpdate);
