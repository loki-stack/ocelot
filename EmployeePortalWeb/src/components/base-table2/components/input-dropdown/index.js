import React from "react";
import { Dropdown } from "primereact/dropdown";

const CustomInputDropdown = (props) => {
  const {
    inputProps = {},
    errors = {},
    label,
    required,
    arrOptions = [],
  } = props;
  const { name = "" } = inputProps;

  return (
    <div className="p-field">
      <label
        className={required ? "p-label-required" : undefined}
        htmlFor={inputProps?.id}
      >
        {label}
      </label>
      <div>
        <Dropdown {...inputProps} options={arrOptions} />
        {errors[name] && (
          <small className="p-invalid p-d-block">{errors[name].message}</small>
        )}
      </div>
    </div>
  );
};
export default CustomInputDropdown;
