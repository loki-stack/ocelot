import React from "react";
import { RadioButton } from "primereact/radiobutton";
import "../index.scss";

const CustomRadioButton = (props) => {
  const {
    field,
    arrOptions = [],
    inputProps = {},
    errors = {},
    label,
    required,
  } = props;
  const {
    value,
    labelOption = "label",
    valueOption = "value",
    disabled,
    onChange,
  } = inputProps;

  return (
    <div className="p-field">
      <label
        className={required ? "p-label-required" : undefined}
        htmlFor={inputProps?.id}
      >
        {label}
      </label>
      <div>
        {arrOptions.map((option, index) => {
          return (
            <div key={index} className="p-field-radiobutton">
              <RadioButton
                inputId={field + index}
                name="category"
                value={option[valueOption]}
                onChange={(e) => onChange(e)}
                checked={option[valueOption] === value}
                disabled={disabled}
              />
              <label htmlFor={field + index}>{option[labelOption]}</label>
            </div>
          );
        })}
        {errors[field] && (
          <small className="p-invalid p-d-block">{errors[field].message}</small>
        )}
      </div>
    </div>
  );
};
export default CustomRadioButton;
