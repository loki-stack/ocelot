import React from "react";
import { Dialog } from "primereact/dialog";
import Button from "../button";
import "../index.scss";
import "../../index.scss";

export default function CustomDialog(props) {
  const renderHeader = () => {
    return (
      <>
        <div className="head_label-main base-dialog-header">
          {props.header?.main || "Header"}
        </div>
      </>
    );
  };

  const renderFooter = () => {
    return (
      <div className="base-dialog-footer">
        <Button
          label="Cancel"
          className="p-button-secondary"
          onClick={props.onHide}
        />
        <Button
          label="Save"
          className="p-button-danger"
          onClick={props.onSave}
        />
      </div>
    );
  };

  return (
    <Dialog
      visible={props.visible}
      header={renderHeader()}
      footer={renderFooter()}
      className={`base-dialog ${props.className}`}
      style={props.style}
      modal
      onHide={props.onHide}
    >
      <div className="head_label-sub base-dialog-header">
        {props.header?.sub || "Additional description if needed"}
      </div>
      <div className="base-dialog-content">{props.content}</div>
    </Dialog>
  );
}
