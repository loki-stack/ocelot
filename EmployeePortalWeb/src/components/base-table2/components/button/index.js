import React from "react";
import { Button } from "primereact/button";
import "../index.scss";

export default function CustomButton(props) {
  const { label = "", className = "", disabled, onClick, style = {} } = props;
  return (
    <Button
      label={label}
      className={`base-button ${className}`}
      style={style}
      disabled={disabled}
      onClick={onClick}
    />
  );
}
