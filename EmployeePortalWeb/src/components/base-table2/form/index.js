import React, { useEffect, useState } from "react";
import { Card } from "primereact/card";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { ACTIONS_KEY, REG_CHECK_FIRST_SPACE, TYPE_INPUT } from "../constant";
import CustomInputText from "../components/input-text";
import CustomInputTextarea from "../components/input-textarea";
import CustomInputSwitch from "../components/input-switch";
import CustomInputDropdown from "../components/input-dropdown";
import CustomRadioButton from "../components/radio-button";

const BaseForm = (props) => {
  const {
    tabKey = "",
    formElements = [],
    formData = {},
    getSidebarFormData,
    currentAction = "",
  } = props;

  const {
    register,
    getValues,
    setValue,
    errors,
    clearErrors,
    trigger,
  } = useForm({
    mode: "onSubmit",
    reValidateMode: "onChange",
    defaultValues: {},
    resolver: undefined,
    context: undefined,
    criteriaMode: "firstError",
    shouldFocusError: true,
    shouldUnregister: true,
  });
  const { t } = useTranslation();
  const [triggerForm, setTriggerForm] = useState({});
  const [arrFieldsRequired, setArrFieldsRequired] = useState([]);

  const requiredMsg = t("common.error.isRequired");
  const ruleRequired = (fieldName) => {
    return {
      value: true,
      message: fieldName + requiredMsg,
    };
  };

  useEffect(() => {
    const arrFields = [];
    const arrFieldsRequired = [];

    formElements.forEach((formElementData, index) => {
      const { field = "", label = "", required } = formElementData || {};
      register(field, required ? { required: ruleRequired(t(label)) } : {});
      if (required) {
        arrFieldsRequired.push(field);
      }
      arrFields.push(field);
    });

    const setFormValue = (arrFields, arrFieldsRequired) => {
      arrFields.forEach(async (field) => {
        await setValue(field, formData[field], { shouldValidate: true });
        arrFieldsRequired.includes(field) && clearErrors(arrFieldsRequired);
      });
    };

    setFormValue(arrFields, arrFieldsRequired);
    setArrFieldsRequired(arrFieldsRequired);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onChangeValueInput = async (field, value, shouldValidate) => {
    if (REG_CHECK_FIRST_SPACE.test(value)) return;
    await setValue(field, value, { shouldValidate: shouldValidate });

    let newTriggerForm = { ...triggerForm };
    if (arrFieldsRequired.includes(field)) {
      const valueTrigger = await trigger(field);
      newTriggerForm = { ...triggerForm, [field]: valueTrigger };
    }

    setTriggerForm(newTriggerForm);

    const arrValuesFormData = Object.values(newTriggerForm);
    const isValid =
      (currentAction === ACTIONS_KEY.ADD
        ? arrValuesFormData.length === arrFieldsRequired.length
        : true) && !arrValuesFormData.includes(false);
    getSidebarFormData(getValues(), { [tabKey]: isValid });
  };

  const valuesForm = getValues();

  return (
    <Card className="form">
      <div className="p-fluid">
        {formElements.map((formElementData, index) => {
          const {
            type = "",
            label = "",
            field = "",
            required,
            arrOptions = [],
            inputProps = {},
            customElement: CustomElement,
          } = formElementData || {};

          let formElement;
          const propsFormElement = {
            key: index + "",
            label,
            field,
            required,
            arrOptions,
            inputProps: {
              value:
                Object.keys(valuesForm).length > 0 && valuesForm[field]
                  ? valuesForm[field]
                  : type === TYPE_INPUT.DROPDOWN
                  ? null
                  : "",
              onChange: (e) => {
                let { value } = e?.target;
                if (type === TYPE_INPUT.INPUT_SWITCH) {
                  value = value ? arrOptions[0].value : arrOptions[1].value;
                }
                onChangeValueInput(field, value, true);
              },
              ...inputProps,
            },
            errors,
          };

          switch (type) {
            case TYPE_INPUT.INPUT_TEXT:
              formElement = <CustomInputText {...propsFormElement} />;
              break;

            case TYPE_INPUT.INPUT_TEXT_AREA:
              formElement = <CustomInputTextarea {...propsFormElement} />;
              break;

            case TYPE_INPUT.INPUT_SWITCH:
              formElement = <CustomInputSwitch {...propsFormElement} />;
              break;

            case TYPE_INPUT.DROPDOWN:
              formElement = <CustomInputDropdown {...propsFormElement} />;
              break;

            case TYPE_INPUT.RADIO_BUTTON:
              formElement = <CustomRadioButton {...propsFormElement} />;
              break;

            case TYPE_INPUT.CUSTOM:
              formElement = <CustomElement {...propsFormElement} />;
              break;

            default:
              break;
          }

          return formElement;
        })}
      </div>
    </Card>
  );
};

export default BaseForm;
