export const ARR_IPP = [10, 20, 50];

export const ACTIONS_KEY = {
  ADD: "add",
  VIEW: "view",
  EDIT: "edit",
  COPY: "copy",
  RESULT: "result",
};

export const ACTIONS = [
  { key: ACTIONS_KEY.VIEW, label: "View", icon: "pi-eye" },
  { key: ACTIONS_KEY.EDIT, label: "Edit", icon: "pi-pencil" },
  { key: ACTIONS_KEY.COPY, label: "Copy", icon: "pi-copy" },
];

export const DATETIME_FORMAT = "DD/MM/YYYY hh:mm A";

export const STATUS = {
  ACTIVE: "ACTIVE",
  INACTIVE: "INACTIVE",
};

export const ARR_OPTIONS_STATUS = [
  { label: "All", statusCd: "", value: "" },
  { label: "Active", statusCd: STATUS.ACTIVE, value: STATUS.ACTIVE },
  { label: "Inactive", statusCd: STATUS.INACTIVE, value: STATUS.INACTIVE },
];

export const REG_CHECK_FIRST_SPACE = /^[ ]/;

export const TYPE_INPUT = {
  INPUT_TEXT: "inputText",
  INPUT_TEXT_AREA: "inputTextArea",
  INPUT_SWITCH: "inputSwitch",
  DROPDOWN: "dropdown",
  RADIO_BUTTON: "radioButton",
  CUSTOM: "custom",
};
