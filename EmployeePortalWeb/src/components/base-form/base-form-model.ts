import { BCModel } from "./../base-control/base-cotrol-model";
/**
 * Base form model
 */
export interface BFModel {
  controls: BCModel[];
  layout: BFLayoutModel;
}
/**
 * Base table config model
 */
export interface BFLayoutModel {
  /**
   * Carbon grid config display
   */
  config: any;

  rows: BFLayoutRowModel[];
}

export interface BFLayoutRowModel {
  columns: BFLayoutColumModel[];
  /**
   * Carbon row config display
   */
  config: any;
}

export interface BFLayoutColumModel {
  /**
   * Name of controls
   */
  controls: string;
  /**
   * Carbon colum config display
   * { sm: { span:4, offset:0} }
   */
  config: any;
}
