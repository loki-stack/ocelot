import "./base-form.scss";

import BaseControl, { validateControl } from "./../base-control/base-control";
import { IS_OBJECT_EMPTY, JSON_EQUAL, SLEEP } from "./../../utils/utils";
import React, { useEffect, useImperativeHandle, useRef, useState } from "react";

import { Button } from "primereact/button";
import { Message } from "primereact/message";
import { useTranslation } from "react-i18next";

const initFormState = (form, controlsConfig, t) => {
  let formState = {
    invalid: false,
  };
  controlsConfig.forEach((control) => {
    let ruleList = control.ruleList || [];
    if (control.required) {
      ruleList.push({
        name: "required",
      });
    }
    if (!form.hasOwnProperty(control.key)) {
      form[control.key] = undefined;
    }
    const controlState = validateControl(
      ruleList,
      form[control.key],
      t,
      control.type
    );
    formState[control.key] = controlState;
    if (controlState.invalid) {
      formState.invalid = true;
    }
  });
  return {
    form,
    formState,
  };
};
const BaseForm = React.forwardRef((props, ref) => {
  const { t } = useTranslation();
  // extract props data
  const id = props.id || "BaseForm_default_id";
  // init state control
  let initState = {
    touched: false,
    loading: true,
    submiting: false,
    form: props.form || {},
    formState: {
      invalid: false,
    },
    formAlert: [],
  };
  const [state, setState] = useState(initState);
  // Unsubcribe function
  const mountedRef = useRef(true);
  useEffect(() => {
    return () => {
      mountedRef.current = false;
    };
  }, []);
  // On change
  useEffect(() => {
    const initForm = async () => {
      await SLEEP(1);
      let _form = {};
      if (props.loadFornFn) {
        _form = await props.loadFornFn();
      } else {
        _form = props.form || {};
      }

      var initF = initFormState(_form, props.config?.controls || [], t);
      const _state = {
        loading: false,
        submiting: false,
        touched: props.defaultTouch,
        ...initF,
        formAlert: [],
      };
      if (!mountedRef.current) return null;
      setState(_state);
    };
    initForm();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.loadFornFn, props.form, props.defaultTouch]);

  useEffect(() => {
    const _state = {
      ...state,
      touched: props.touched,
    };
    if (!mountedRef.current) return null;
    setState(_state);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.touched]);

  // onbeforeunload
  useEffect(() => {
    if (props.readOnly) {
      window.onbeforeunload = function () {};
    } else {
      window.onbeforeunload = function () {};
      // window.onbeforeunload = function () {
      //   return "Your work will be lost.";
      // };
    }
  });
  useEffect(
    () => () => {
      window.onbeforeunload = function () {};
    },
    []
  );
  /** Watch change */
  useEffect(() => {
    if (props.onStateChange) {
      props.onStateChange({
        loading: state.loading,
        submitting: state.submitting,
        touched: state.touched,
        invalid: state.formState.invalid,
        formState: state.formState,
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state.loading, state.submitting, state.touched, state.formState]);

  /** Expose function  */
  useImperativeHandle(ref, () => ({
    submitForm() {
      return onSubmitForm();
    },
    submitFormResult(res) {
      return onSubmitFormResult(res);
    },
    getFormState() {
      let formState = initFormState(state.form, props.config?.controls || [], t)
        .formState;
      return {
        ...formState,
        touched: state.touched,
      };
    },
    getForm() {
      return {
        ...state.form,
      };
    },
    setValidation(validateResult) {
      return onSetValidation(validateResult);
    },
  }));

  const validateFormState = (_formState) => {
    let invalid = false;
    for (const key in _formState) {
      if (_formState[key].invalid) {
        invalid = true;
      }
    }
    return {
      ..._formState,
      invalid,
    };
  };

  const onChangeModelControl = (control, data) => {
    let _state = { ...state };

    if (data.valueObj) {
      _state.form[control.key + "_obj"] = data.valueObj;
    }
    _state.form[control.key] = data.value;

    _state.formState[control.key] = data.controlState;
    _state.formState = validateFormState(_state.formState);
    _state.touched = true;
    if (props.onChange) {
      props.onChange({
        state: _state,
        changed: {
          control,
          data,
        },
      });
    }
    if (!JSON_EQUAL(_state, state)) {
      setState(_state);
    }
    if (props.onChangeFormState) {
      // if (
      //   state.formState.invalid !== _state.formState.invalid ||
      //   state.touched !== _state.touched
      // ) {
      props.onChangeFormState({
        ..._state.formState,
        touched: _state.touched,
      });
      // }
    }
    return true;
  };

  const onSubmitForm = async () => {
    // Validate 1st
    const _state = { ...state };
    const formState = validateFormState(state.formState);
    if (formState.invalid) {
      _state.formState = formState;
      setState(_state);
      return {
        form: {
          ...state.form,
        },
        formState: {
          ...state.formState,
        },
        touched: state.touched,
      };
    }
    setState({
      ...state,
      submiting: true,
    });
    await SLEEP(1);
    let res;

    if (props.config?.formSubmitFn) {
      res = await props.config?.formSubmitFn({
        form: _state.form,
        formState: _state.formState,
        touched: _state.touched,
      });
      if (res && res.data && !IS_OBJECT_EMPTY(res.data)) {
        _state.formState.invalid = false;
      } else {
        _state.formState.invalid = true;
      }

      if (res && res.message && res.message.alert) {
        _state.formAlert = res.message.alert;
      }

      if (
        res &&
        (!res.message || !res.message.alert) &&
        _state.formState.invalid
      ) {
        _state.formAlert = [
          {
            level: "WARN",
            labelCode: "FORM_INVALID",
          },
        ];
      }

      if (res && res.message && res.message.validateResult) {
        res.message.validateResult.forEach((item) => {
          for (const key in _state.formState) {
            if (_state.formState.hasOwnProperty(key)) {
              if (key === item.componentId) {
                const param = {};
                if (item.placeholder) {
                  item.placeholder.forEach((element, placeIndex) => {
                    param["" + placeIndex] = element;
                  });
                }
                _state.formState[key] = {
                  invalid: true,
                  error: t("dynamic:" + item.labelCode, param),
                  ruleDetail: {
                    name: t("From backend"),
                    param: null,
                  },
                };
              }
            }
          }
        });
      }
    }

    let isChangeState = false;
    if (props.config?.afterFormSubmitFn) {
      isChangeState = await props.config?.afterFormSubmitFn({
        res,
        form: _state.form,
        formState: _state.formState,
      });
    }

    if (!isChangeState) {
      // _state.touched = false;
      _state.submiting = false;
      if (props.onChange) {
        props.onChange({
          state: _state,
        });
      }
      setState(_state);
    }

    return {
      form: {
        ..._state.form,
      },
      formState: {
        ..._state.formState,
      },
    };
  };
  const onSetValidation = (validateResult) => {
    let _state = { ...state };
    let hasInvalid = false;
    validateResult.forEach((item) => {
      for (const key in _state.formState) {
        if (_state.formState.hasOwnProperty(key)) {
          if (key === item.componentId) {
            const param = {};
            if (item.placeholder) {
              item.placeholder.forEach((element, placeIndex) => {
                param["" + placeIndex] = element;
              });
            }
            hasInvalid = true;
            _state.formState[key] = {
              invalid: true,
              error: t("dynamic:" + item.labelCode, param),
              ruleDetail: {
                name: t("From backend"),
                param: null,
              },
            };
          }
        }
      }
    });
    _state.formState.invalid = hasInvalid;
    setState(_state);
    return hasInvalid;
  };
  const onSubmitFormResult = async (res) => {
    let _state = { ...state };
    if (res && res.message && res.message.validateResult) {
      res.message.validateResult.forEach((item) => {
        for (const key in _state.formState) {
          if (_state.formState.hasOwnProperty(key)) {
            if (key === item.componentId) {
              _state.formState.invalid = true;
              const param = {};
              if (item.placeholder) {
                item.placeholder.forEach((element, placeIndex) => {
                  param["" + placeIndex] = element;
                });
              }
              _state.formState[key] = {
                invalid: true,
                error: t("dynamic:" + item.labelCode, param),
                ruleDetail: {
                  name: t("From backend"),
                  param: null,
                },
              };
            }
          }
        }
      });
    }
    setState(_state);
  };
  const renderAction = (action) => {
    switch (action.type) {
      case "submit":
        return (
          <Button
            {...action.config}
            type="submit"
            icon={state.submiting ? "pi pi-spin pi-spinner" : null}
            disabled={
              state.submiting || (state.formState.invalid && state.touched)
            }
            onClick={(e) => onSubmitForm(e)}
            label={action.label}
          ></Button>
        );
      default:
        break;
    }
    return null;
  };

  const buildLayout = () => {
    let autoFocus = props.autoFocus;
    const rows = [];
    const layoutConfig = props.config
      ? { ...props.config.layout }
      : {
          rows: [],
        };

    const controlsConfig = props.config ? [...props.config.controls] : [];

    layoutConfig.rows.forEach((row, rowIndex) => {
      const columns = [];
      row.columns.forEach((column, columnIndex) => {
        let controlObj = controlsConfig.find((x) => x.key === column.control);
        const holder = [];
        if (controlObj) {
          let control = { ...controlObj };
          let controlState = state.formState[control.key];
          let value = state.form[column.control];
          if (!control.config) {
            control.config = {};
          }
          if (props.readOnly) {
            control.config.disabled = true;
            control.config.readOnly = true;
            control.viewMode = true;
          }
          holder.push(
            <div
              key={`${id}-${rowIndex}-${columnIndex}-holder`}
              id={`${id}-${rowIndex}-${columnIndex}-holder`}
              className={`bf-control ${
                !control.config.disabled && !control.config.readOnly & autoFocus
                  ? "autofocus"
                  : ""
              }`}
            >
              <BaseControl
                id={`${id}-${control.key}`}
                key={`${id}-${rowIndex}-${columnIndex}-holder-control-${state.touched}`}
                onChange={(data) => onChangeModelControl(control, data)}
                {...control}
                controlState={controlState}
                value={value}
                autoFocus={
                  !control.config.disabled &&
                  !control.config.readOnly & autoFocus
                    ? true
                    : false
                }
                loading={props.loading}
                touched={state.touched || props.touched}
                onTouched={() => {
                  if (props.onTouched) {
                    props.onTouched();
                    return;
                  }
                  if (!state.touched) {
                    setState({
                      ...state,
                      touched: true,
                    });
                  }
                }}
              />
            </div>
          );
          if (!control.config.disabled && !control.config.readOnly) {
            autoFocus = false;
          }
        } else if (column.component) {
          holder.push(
            <div
              key={`${id}-${rowIndex}-${columnIndex}-holder`}
              id={`${id}-${rowIndex}-${columnIndex}-holder`}
              className={`bf-component`}
            >
              {column.component({
                isSubmiting: state.submiting,
                form: {
                  ...state.form,
                },
                formState: {
                  ...state.formState,
                },
                onSubmitForm,
              })}
            </div>
          );
        } else if (column.action) {
          holder.push(
            <div
              key={`${id}-${rowIndex}-${columnIndex}-holder`}
              id={`${id}-${rowIndex}-${columnIndex}-holder`}
              className={`bf-action`}
            >
              {renderAction(column.action)}
            </div>
          );
        }
        columns.push(
          <div
            {...column.config}
            key={`bf-column-${columnIndex}`}
            className={`bf-column bf-column-${columnIndex} p-col ${
              column.config ? column.config.className : ""
            }`}
          >
            {holder}
          </div>
        );
      });
      rows.push(
        <div
          {...row.config}
          key={`bf-row-${rowIndex}`}
          className={`bf-row bf-row-${rowIndex} p-grid ${
            row.config ? row.config.className : ""
          }`}
        >
          {columns}
        </div>
      );
    });
    return (
      <>
        <form
          id={id}
          className={`${props.readOnly ? "bf-main-readonly" : ""} bf-main`}
        >
          {rows}
        </form>
      </>
    );
  };
  const buildAlert = () => {
    return state.formAlert.map((alter) => {
      const param = {};
      if (alter.placeholder) {
        alter.placeholder.forEach((element, placeIndex) => {
          param["" + placeIndex] = element;
        });
      }
      return (
        <>
          <Message
            className="bf-invalid"
            severity={alter.level.toLowerCase()}
            text={t(alter.labelCode, {
              ...param,
            })}
          ></Message>
        </>
      );
    });
  };
  return (
    <>
      <span style={{ display: "none" }}>{JSON.stringify(state.formState)}</span>
      {buildAlert()}
      {buildLayout()}
    </>
  );
});

export default BaseForm;
