import "./main-side-modal.scss";

import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { Animated } from "react-animated-css";
import BaseForm from "./../base-form/base-form";
import { Button } from "primereact/button";
import { Dialog } from "primereact/dialog";
import { closeSideModal } from "../../redux/actions/sideModal";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

const MainSideModal = () => {
  const formRef = useRef();
  let history = useHistory();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [state, setState] = useState({
    form: {},
    formState: {},
    touched: false,
  });
  const modal = useSelector((state) => state.sideModal);

  // onbeforeunload
  useEffect(() => {
    window.onbeforeunload = function () {};
    // window.onbeforeunload = function () {
    //   return "Your work will be lost.";
    // };
  });
  useEffect(
    () => () => {
      window.onbeforeunload = function () {};
    },
    []
  );

  useEffect(() => {
    return history.listen((location) => {
      if (history.action === "PUSH") {
        onClose();
      }

      if (history.action === "POP") {
        onClose();
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [history]);

  const onClose = () => {
    dispatch(closeSideModal({}));
  };
  const onClickSecondaryButton = async () => {
    if (modal.secondButtonClickFn) {
      modal.secondButtonClickFn({ closeFn: () => onClose() });
      return;
    }
  };
  const onClickPrimaryButton = async () => {
    if (modal.form) {
      const res = await formRef.current.submitForm();
      if (modal.primaryButtonClickFn) {
        await modal.primaryButtonClickFn({
          closeFn: () => onClose(),
          formState: res.formState,
          form: res.form,
        });
      }
      return;
    }
    if (modal.primaryButtonClickFn) {
      await modal.primaryButtonClickFn({ closeFn: () => onClose() });
      return;
    }
  };
  const onChangeForm = (e) => {
    setState({
      ...state,
      touched: true,
      ...e.state,
    });
  };
  const renderModalBody = () => {
    if (modal.renderModalBody) {
      return modal.renderModalBody();
    }
    if (modal.form) {
      return (
        <BaseForm
          ref={formRef}
          {...modal.form}
          touched={state.touched}
          onChange={(e) => onChangeForm(e)}
          onTouched={() => {
            setState({
              ...state,
              touched: true,
            });
          }}
        />
      );
    }
    if (modal.content) {
      return modal.content;
    }
    return null;
  };
  const renderFooter = () => {
    if (modal.renderFooter) {
      return modal.renderFooter();
    } else {
      return (
        <div className="carbon-btn-group-full">
          {modal.hideSecond || modal.hidePrimary ? (
            <div style={{ flexGrow: 1 }}></div>
          ) : null}
          {!modal.hideSecond ? (
            <Button
              type="button"
              label={modal.secondaryButtonText || t("Cancel")}
              // icon="pi pi-times"
              className="p-button-secondary"
              disabled={modal.preventClose}
              onClick={onClickSecondaryButton}
            />
          ) : null}
          {!modal.hidePrimary ? (
            <Button
              type="submit"
              onClick={onClickPrimaryButton}
              disabled={
                modal.form ? state.formState.invalid || !state.touched : false
              }
              label={modal.primaryButtonText || t("Save")}
              autoFocus
            />
          ) : null}
        </div>
      );
    }
  };
  return (
    <div className="main-side-modal">
      <Dialog
        header={modal.title || t("Info")}
        footer={renderFooter()}
        visible={modal.isOpen}
        modal
        onHide={onClose}
      >
        <Animated
          animationIn="bounceInRight"
          animationOut="bounceOutRight"
          animationInDuration={200}
          animationOutDuration={200}
          isVisible={modal.isOpen}
        ></Animated>
        {renderModalBody()}
      </Dialog>
    </div>
  );
};

export default MainSideModal;
