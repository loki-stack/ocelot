import React from "react";
import { Route, Redirect } from "react-router-dom";
import { useSelector } from "react-redux";

/**
 * For protecting the pages to be accessible after login
 * @param {} param0
 */
const GuardedRoute = ({ component: Component, auth, ...rest }) => {
  const clientInfo = useSelector((state) => state.global?.clientInfo);
  let loginPage = "/";
  if (clientInfo) {
    loginPage = `/${clientInfo?.clientCode}/${clientInfo?.entityCode}/passport/login`;
  }

  if (auth !== true) {
    if (!window.location.pathname.includes("/passport/login")) {
      window.sessionStorage.setItem(
        "path_after_login",
        window.location.pathname
      );
    }
  }

  return (
    <Route
      {...rest}
      render={(props) =>
        auth === true ? <Component {...props} /> : <Redirect to={loginPage} />
      }
    />
  );
};
export default GuardedRoute;
