/**
 * Base table config model
 */
export interface BCModel {
  /**
   * Key model
   */
  key: string;
  /**
   * Type of control
   */
  label: string;
  /**
   * Place holder
   */
  placeholder: string;
  /**
   * Type of control
   */

  mode?: "filter-table" | "normal";
  /**
   * Type of control
   */
  type?:
    | "select"
    | "multiselect"
    | "checkbox"
    | "radio"
    | "input"
    | "textarea"
    | "phone"
    | "email"
    | "password"
    | "date"
    | "number"
    | "toogle"
    | "daterange";
}

export const DEFAULT_CONTROL_ITEM = {
  type: "input",
};
export const getControlModel = (column: BCModel) => {
  return {
    ...DEFAULT_CONTROL_ITEM,
    ...column,
  };
};
export const getControlModelByType = (dataType: string) => {
  switch (dataType) {
    case "string":
      return {
        ...DEFAULT_CONTROL_ITEM,
        placeholder: "Input text",
      };
    case "date":
      return {
        ...DEFAULT_CONTROL_ITEM,
        placeholder: "Choose a date",
        type: "date",
      };
    default:
      break;
  }
  return {
    ...DEFAULT_CONTROL_ITEM,
  };
};
