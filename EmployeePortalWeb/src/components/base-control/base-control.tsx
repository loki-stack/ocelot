import "./base-control.scss";

import React, { useEffect, useState } from "react";

import CheckboxControl from "./checkbox/checkbox-control";
import DateControl from "./date-control/date-control";
import DateRangeControl from "./date-range-control/date-range-control";
import InputControl from "./input-control/input-control";
import NumberControl from "./number-control/number-control";
import { ProgressBar } from "primereact/progressbar";
import RadioControl from "./radio-control/radio-control";
import SelectControl from "./select-control/select-control";
import ToggleControl from "./toggle-control/toggle-control";
import { useTranslation } from "react-i18next";
import MultiSelectControl from "./multiselect-control/multiselect-control";

/**
 *
 */
export interface BCRules {
  /**
   * Name of rule
   */
  name: string;
  /**
   * Param of rule
   */
  param?: any;
  /**
   * Default error message for rule. Using stranlate using param
   */
  error?: string;
}
/**
 *
 */
export interface BCValidateResult {
  /**
   * Control invalid
   */
  invalid: boolean;
  /**
   * Error message if control invalid
   */
  error?: string;
  /**
   * Detail rule invalid
   */
  ruleDetail?: BCRules;

  isRequired?: boolean;
}
/**
 *
/**
 *
 *
 * @export
 * @interface BCProps
 */
export interface BCProps {
  /**
   * Unix id
   */
  id?: string;
  /**
   * Type of control
   */
  label?: string;
  /**
   * Class name of control
   */
  className?: string;
  /**
   * Control without value
   */
  noLabel?: boolean;

  /**
   * Control required
   */
  required?: boolean;

  /**
   * Function on change
   * @returns is change parent state
   */
  onChange?: (param: {
    controlState: BCValidateResult;
    value: any;
    valueStr: string;
    [key: string]: any;
  }) => boolean;

  /**
   * Function on update data truely
   * @returns is change parent state
   */
  onTrueUpdateValue?: (param: {
    controlState: BCValidateResult;
    value: any;
    valueStr: string;
    [key: string]: any;
  }) => boolean;

  /**
   * Place holder
   */
  placeholder?: string;
  /**
   * Value of control
   */
  value?: any;
  /**
   * Value of control
   */
  ruleList?: BCRules[];
  /**
   * Other config available
   */
  config?: any;
  /**
   * From filter control
   */
  fromFilter: boolean;
  /**
   * Type of control
   */
  type?:
    | "select"
    | "multiselect"
    | "checkbox"
    | "radio"
    | "input"
    | "textarea"
    | "phone"
    | "email"
    | "password"
    | "date"
    | "number"
    | "toogle"
    | "daterange";
  /**
   * First focus element
   */
  autoFocus: boolean;
  /**
   * Addtional props
   */
  [key: string]: any;
}

export interface BCStates {
  /**
   * Controler touched
   */
  touched: boolean;
  /**
   * Data of control
   */
  value: any;
  /**
   * Display text of value
   */
  valueStr: string;
  /**
   * State of control
   */
  controlState: BCValidateResult;
  /**
   * Controler loading state
   */
  loading?: boolean;

  /**
   * Addtional state
   */
  [key: string]: any;
}

/**
 *
 * @param ruleList  List rule to validate
 * @param value Value
 * @param type Type of control validate
 */
export const validateControl = (
  ruleList: BCRules[],
  value: any,
  t: any,
  type?: string
): BCValidateResult => {
  let controlInvalid = false;
  let result: BCValidateResult = {
    invalid: false,
  };
  if (!ruleList) {
    return result;
  }
  ruleList.forEach((rule) => {
    const ruleName = rule.name;
    const errorDefault = rule.error;
    const ruleParam = rule.param || "";
    if (controlInvalid) {
      return;
    }
    switch (ruleName) {
      case "required":
        if (type === "daterange") {
          if (
            !value ||
            !Array.isArray(value) ||
            value.length !== 2 ||
            !value[0] ||
            !value[1]
          ) {
            controlInvalid = true;
            result = {
              invalid: true,
              error: errorDefault || t("Field is required"),
              isRequired: true,
              ruleDetail: rule,
            };
          }
        } else if (type === "multiselect") {
          if (!value || !Array.isArray(value) || value.length === 0) {
            controlInvalid = true;
            result = {
              invalid: true,
              error: errorDefault || t("Field is required"),
              isRequired: true,
              ruleDetail: rule,
            };
          }
        } else if (type === "date") {
          if (!value) {
            controlInvalid = true;
            result = {
              invalid: true,
              error: errorDefault || t("Field is required"),
              isRequired: true,
              ruleDetail: rule,
            };
          }
        } else {
          if (value === undefined || value === null || value === "") {
            controlInvalid = true;
            result = {
              invalid: true,
              error: errorDefault || t("Field is required"),
              isRequired: true,
              ruleDetail: rule,
            };
          }
        }
        break;
      case "pattern":
        value = value ? value.trim() : null;
        var pattern = new RegExp(ruleParam);
        if (!pattern.test(value)) {
          controlInvalid = true;
          result = {
            invalid: true,
            error: errorDefault || t("Pattern not match"),
            ruleDetail: rule,
          };
        }
        break;
      case "maxLength":
        value = value ? value.toString().trim() : "";
        if (value.length > ruleParam) {
          controlInvalid = true;
          result = {
            invalid: true,
            error:
              errorDefault ||
              t("Max length {{number}} characters", {
                number: ruleParam,
              }),
            ruleDetail: rule,
          };
        }

        break;
      case "minLength":
        value = value ? value.trim() : "";
        if (value.length < ruleParam) {
          controlInvalid = true;
          result = {
            invalid: true,
            error:
              errorDefault ||
              t("Min length {{number}} characters", {
                number: ruleParam,
              }),
            ruleDetail: rule,
          };
        }

        break;
      default:
        break;
    }
  });
  return result;
};

export const renderError = (state: any, props: any) => {
  return (
    <>
      {state.controlState.invalid && (state.touched || props.touched) ? (
        <small
          id={`${props.id}-error`}
          className="p-invalid p-d-block p-invalid-custom"
        >
          {/* {!state.controlState.isRequired ||
          !props.fromFrom ||
          props.formSubmited
            ? state.controlState.error
            : ""} */}
          {state.controlState.error}
        </small>
      ) : null}
    </>
  );
};

const BaseControl: React.FC<BCProps> = (props) => {
  let id = props.id || "BaseControl_default_id";
  const { t } = useTranslation();
  // prepare state
  useEffect(() => {}, [props]);
  const getControl = () => {
    let _props = { ...props };
    if (_props.config?.readOnly) {
      _props.placeholder = t("N/A");
    }
    if (_props.componentRender) {
      return <CommonControl className="common-control" {..._props} id={id} />;
    }
    switch (_props.type) {
      case "select":
        return <SelectControl className="select-control" {..._props} id={id} />;
      case "multiselect":
        return (
          <MultiSelectControl
            className="multiselect-control"
            {..._props}
            id={id}
          />
        );

      case "checkbox":
        return (
          <CheckboxControl className="checkbox-control" {..._props} id={id} />
        );
      case "toogle":
        return (
          <ToggleControl className="checkbox-control" {..._props} id={id} />
        );
      case "number":
        return <NumberControl className="number-control" {..._props} id={id} />;

      case "radio":
        return <RadioControl className="radio-control" {..._props} id={id} />;

      case "input" || "textarea" || "phone" || "email" || "password":
        return <InputControl className="input-control" {..._props} id={id} />;
      case "daterange":
        return (
          <DateRangeControl
            className="date-range-control"
            {..._props}
            id={id}
          />
        );
      case "date":
        return <DateControl className="date-control" {..._props} id={id} />;
      default:
        return <InputControl className="input-control" {..._props} id={id} />;
    }
  };

  return (
    <div
      className={`base-control ${
        props.config?.readOnly ? "base-control-readonly" : ""
      }`}
    >
      {getControl()}
    </div>
  );
};

const CommonControl: React.FC<BCProps> = (props, ref) => {
  const { t } = useTranslation();
  // extract props
  const ruleList = props.ruleList || [];
  if (props.required) {
    ruleList.push({
      name: "required",
    });
  }
  // State
  let initState: BCStates = {
    touched: false,
    value: props.value,
    valueStr: props.getValueStr
      ? props.getValueStr()
      : (props.value || "").toString(),
    controlState: {
      invalid: false,
    },
  };

  initState.controlState =
    props.controlState || validateControl(ruleList || [], initState.value, t);

  // prepare state
  const [state, setState] = useState(initState);
  useEffect(() => {}, [props]);

  const onFocus = () => {
    if (props.onTouched) {
      return props.onTouched();
    }
    if (!state.touched) {
      setState({
        ...state,
        touched: true,
      });
    }
  };
  /**
   * On change
   * @param {*} event
   */
  const onChange = (value: any) => {
    const valueStr = props.getValueStr
      ? props.getValueStr()
      : (props.value || "").toString();
    const controlState = validateControl(ruleList, value, t);
    let _state = {
      ...state,
      value,
      valueStr,
      controlState,
    };
    if (props.onChange) {
      props.onChange({
        controlState: _state.controlState,
        value: _state.value,
        valueStr: _state.valueStr,
      });
    }
    if (props.onTrueUpdateValue) {
      props.onTrueUpdateValue({
        controlState: state.controlState,
        value: state.value,
        valueStr: state.valueStr,
      });
    }
    setState(_state);
  };
  const commonControl = () => {
    return (
      <>
        <div className={`p-field ${props.noLabel ? "no-label" : ""}`}>
          <label htmlFor={props.id}>
            {props.label}
            {props.required && !props.noRequiredLabel ? (
              <small className="required p-invalid">&nbsp;*</small>
            ) : null}
          </label>
          <div className="p-inputgroup">
            {props.loading ? (
              <>
                <ProgressBar
                  style={{ width: "100%", minHeight: "2.5rem" }}
                  mode="indeterminate"
                />
              </>
            ) : (
              props.componentRender({
                onFocus: onFocus,
                onChange: onChange,
              })
            )}
          </div>
          {renderError(state, props)}
        </div>
      </>
    );
  };
  return <>{commonControl()}</>;
};
export default BaseControl;
