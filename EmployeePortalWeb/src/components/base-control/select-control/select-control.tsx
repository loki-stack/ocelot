import "./select-control.scss";

import { BCProps, BCStates, validateControl } from "./../base-control";
import React, { useEffect, useRef, useState } from "react";

import { Button } from "primereact/button";
import { Dropdown } from "primereact/dropdown";
import { JSON_EQUAL } from "./../../../utils/utils";
import { ProgressBar } from "primereact/progressbar";
import { useTranslation } from "react-i18next";

export interface SelectControlProps extends BCProps {}

export interface SelectControlState extends BCStates {}
export interface SelectControlOption {
  /**
   * Item group
   */
  group?: string;
  /**
   * Item label
   */
  label: string;
  /**
   * Item value
   */
  value: string;
  /**
   * Item data
   */
  className?: string;
  /**
   * Item data
   */
  data?: string;

  /**
   * Addtional props
   */
  [key: string]: any;
}

const groupOption = (items: SelectControlOption[]) => {
  items = items.sort((a, b) => {
    return (a.group || "").trim().localeCompare((b.group || "").trim());
  });
  let listGroup: string[] = [];
  let newItems: SelectControlOption[] = [];
  items.forEach((item) => {
    if (!listGroup.includes(item.group || "")) {
      listGroup.push(item.group || "");
      newItems.push({
        label: item.group || "",
        value: item.group || "",
        disabled: true,
        className: "menu-group",
      });
    }
    newItems.push(item);
  });
  return newItems;
};
const SelectControl: React.FC<SelectControlProps> = (props) => {
  const { t } = useTranslation();
  // extract props data
  const ruleList = props.ruleList || [];
  if (props.required) {
    ruleList.push({
      name: "required",
    });
  }
  // init state control
  let initState: SelectControlState = {
    touched: false,
    loading: true,
    items: [],
    value: props.value ? props.value : null,
    valueStr: props.value ? props.value.toString() : null,
    valueObj: {
      value: props.value ? props.value : null,
    },
    controlState: {
      invalid: true,
    },
    parentState: {
      root: true,
    },
  };

  const [state, setState] = useState(initState);
  // unsubcribe
  const mountedRef = useRef(true);
  useEffect(() => {
    return () => {
      mountedRef.current = false;
    };
  }, []);
  // init list selected item
  useEffect(() => {
    const getData = async () => {
      let items: any = [];
      if (props.searchFn) {
        const res = await props.searchFn("");
        if (res) {
          items = res;
          if (items && items.length > 0 && items[0].group) {
            items = groupOption(items);
          }
        }
      } else {
        items = props.enum || [];
        if (items && items.length > 0 && items[0].group) {
          items = groupOption(items);
        }
      }

      const value = props.value;
      const valueObj = items.find((x: any) => x.value === value);
      const valueStr = valueObj?.label;
      const ruleList = props.ruleList || [];
      if (props.required) {
        ruleList.push({
          name: "required",
        });
      }
      let controlState =
        props.controlState || validateControl(ruleList || [], value, t);
      if (!mountedRef.current) return;
      setState({
        ...state,
        value,
        valueStr,
        valueObj,
        controlState,
        loading: false,
        touched: false,
        items,
      });
    };
    getData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    props.searchFn,
    props.enum,
    props.controlState,
    props.value,
    props.required,
    props.ruleList,
    t,
  ]);

  const onChange = async (value: any) => {
    const valueObj = state.items.find((x: any) => x.value === value);
    const valueStr = valueObj?.label;
    const controlState = validateControl(ruleList, value, t);
    let _state = {
      ...state,
      value,
      valueStr,
      controlState,
      valueObj,
      loading: false,
    };

    if (props.onChange) {
      props.onChange({
        controlState: _state.controlState,
        value: _state.value,
        valueStr: _state.valueStr,
        valueObj: _state.valueObj,
      });
    }
    if (props.onTrueUpdateValue) {
      props.onTrueUpdateValue({
        controlState: _state.controlState,
        value: _state.value,
        valueStr: _state.valueStr,
        valueObj: _state.valueObj,
      });
    }
    if (!JSON_EQUAL(_state, state)) {
      setState(_state);
    }
  };

  const onFocus = () => {
    if (props.onTouched) {
      props.onTouched();
      return;
    }
    if (!state.touched) {
      setState({
        ...state,
        touched: true,
      });
    }
  };

  const valueTemplate = (option: any, props: any) => {
    if (state.loading) {
      return (
        <div className="bc-sc-loading">
          <i className="pi pi-spin pi-spinner"></i>&nbsp;{t("loading")}
        </div>
      );
    }
    if (option) {
      if (option.displayLabel) {
        return (
          <div className={`menu-selected-option`}>{option.displayLabel}</div>
        );
      }

      return (
        <div className={`menu-selected-option`}>
          <div>
            {option.group ? `${option.group} / ` : null}
            {option.label}
          </div>
        </div>
      );
    }

    return <span>{props.placeholder}</span>;
  };
  const onclickOptionParent = (e: any, parentId: string) => {
    e.preventDefault();
    e.stopPropagation();
    let _state = {
      ...state,
    };
    if (!_state.parentState) {
      _state.parentState = {};
    }
    _state.parentState[parentId] = !_state.parentState[parentId];
    setState(_state);
  };
  const itemTemplate = (option: any) => {
    return (
      <div
        className={`menu-option ${props.isTree ? "has-tree" : ""} 
        ${state?.parentState[option.parentId] ? "show-option" : ""} `}
        style={option.style}
      >
        {option.hasChild ? (
          <Button
            type="button"
            icon={`pi ${
              state?.parentState[option.value]
                ? "pi-angle-down"
                : "pi-angle-right"
            }`}
            onClick={(e) => onclickOptionParent(e, option.value)}
            className="toggle-menu-btn  p-button-sm p-button-text"
          />
        ) : null}
        {option.isLeaf ? (
          <Button
            type="button"
            icon="pi pi-plus"
            className="toggle-menu-btn opacity-0 p-button-sm p-button-text"
          />
        ) : null}
        <div>{option.label}</div>
      </div>
    );
  };
  return (
    <>
      <div
        className={`select-control-inner p-field ${
          props.noLabel ? "no-label" : ""
        }`}
      >
        <label htmlFor={props.id}>
          {props.label}
          {props.required && !props.noRequiredLabel ? (
            <small className="required p-invalid">&nbsp;*</small>
          ) : null}
        </label>
        <div className="p-inputgroup">
          {props.loading ? (
            <>
              <ProgressBar
                style={{ width: "100%", minHeight: "2.5rem" }}
                mode="indeterminate"
              />
            </>
          ) : (
            <Dropdown
              emptyFilterMessage={
                state.loading ? t("Loading") : t("No results found")
              }
              options={state.items || []}
              style={{ width: "100%" }}
              appendTo={document.body}
              filter
              tooltip={state.valueStr}
              tooltipOptions={{ position: "top" }}
              showClear
              {...props.config}
              disabled={
                props.config?.disabled ||
                props.config?.readOnly ||
                props.loading
              }
              placeholder={props.placeholder || t("Choose an option")}
              id={props.id}
              value={state.value}
              optionLabel="label"
              filterBy="label"
              panelClassName={`${props.isTree ? "dropdown-tree" : ""}`}
              valueTemplate={valueTemplate}
              itemTemplate={itemTemplate}
              onChange={(event) => onChange(event.target.value)}
              onFocus={() => onFocus()}
              autoFocus={props.autoFocus}
            />
          )}
        </div>
        {state.controlState.invalid && (state.touched || props.touched) ? (
          <small
            id={`${props.id}-error`}
            className="p-invalid p-d-block p-invalid-custom"
          >
            {state.controlState.error}
          </small>
        ) : null}
      </div>
    </>
  );
};

export default SelectControl;
