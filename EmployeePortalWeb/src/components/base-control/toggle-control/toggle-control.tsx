import "./toggle-control.scss";

import { BCProps, BCStates, validateControl } from "./../base-control";
import React, { useEffect, useRef, useState } from "react";

import { Button } from "primereact/button";
import { InputSwitch } from "primereact/inputswitch";
import { useTranslation } from "react-i18next";

export interface ToggleControlProps extends BCProps {}

export interface ToggleControlState extends BCStates {}

const ToggleControl: React.FC<ToggleControlProps> = (props) => {
  const { t } = useTranslation();
  // extract props
  const ruleList = props.ruleList || [];
  // if (props.required) {
  //   ruleList.push({
  //     name: "required",
  //   });
  // }

  // State
  let initState: ToggleControlState = {
    touched: false,
    value: props.value ? true : false,
    valueStr: props.value ? t("True") : t("False"),
    controlState: {
      invalid: false,
    },
  };
  initState.controlState =
    props.controlState ||
    validateControl(ruleList || [], initState.value, t, "toggle");

  // prepare state
  const [state, setState] = useState(initState);

  const mountedRef = useRef(true);
  // unsubcribe
  useEffect(() => {
    return () => {
      mountedRef.current = false;
    };
  }, []);
  // Update state if control state changed
  useEffect(() => {
    const ruleList = props.ruleList || [];
    // if (props.required) {
    //   ruleList.push({
    //     name: "required",
    //   });
    // }

    let controlState =
      props.controlState ||
      validateControl(ruleList || [], props.value, t, "toggle");
    if (!mountedRef.current) return;
    setState({
      ...state,
      value: props.value ? true : false,
      valueStr: props.value ? t("True") : t("False"),
      controlState,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.controlState, props.required, props.value]);
  /**
   * On change
   * @param {*} event
   */
  const onChange = (value: boolean) => {
    const valueStr = value
      ? props.trueValue || t("Yes")
      : props.falseValue || t("No");
    const controlState = validateControl(ruleList, value, t, "toggle");
    let _state = {
      ...state,
      value,
      valueStr,
      controlState,
    };
    if (props.onChange) {
      props.onChange({
        controlState: _state.controlState,
        value: _state.value,
        valueStr: _state.valueStr,
      });
    }
    if (props.onTrueUpdateValue) {
      props.onTrueUpdateValue({
        controlState: _state.controlState,
        value: _state.value,
        valueStr: _state.valueStr,
      });
    }
    setState(_state);
  };

  const onFocus = () => {
    if (props.onTouched) {
      return props.onTouched();
    }
    if (!state.touched) {
      setState({
        ...state,
        touched: true,
      });
    }
  };

  return (
    <>
      <div
        className={`toggle-control-inner p-field ${
          props.noLabel ? "no-label" : ""
        }`}
      >
        <label htmlFor={props.id}>
          {props.label}
          {props.required && !props.noRequiredLabel ? (
            <small className="required p-invalid">&nbsp;*</small>
          ) : null}
        </label>
        <div className="p-inputgroup">
          <InputSwitch
            {...props.config}
            id={props.id}
            className={`${props.className} p-inputswitch-custom`}
            checked={state.value}
            disabled={state.submitting || props.config?.disabled}
            onFocus={() => onFocus()}
            onChange={(e) => onChange(e.value)}
          />
          <Button
            type="button"
            disabled={state.submitting || props.config?.disabled}
            className="btn-input-switch p-button-text p-button-plain p-button-sm"
            autoFocus={props.autoFocus}
            onFocus={() => onFocus()}
            onClick={() => onChange(!state.value)}
            label={
              state.value
                ? props.trueValue || t("Yes")
                : props.falseValue || t("No")
            }
          />
        </div>
        {state.controlState.invalid && state.touched ? (
          <small
            id={`${props.id}-error`}
            className="p-invalid p-d-block p-invalid-custom"
          >
            {state.controlState.error}
          </small>
        ) : null}
      </div>
    </>
  );
};

export default ToggleControl;
