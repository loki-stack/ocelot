import "./number-control.scss";

import { BCProps, BCStates, validateControl } from "./../base-control";
import React, { useEffect, useRef, useState } from "react";

import { InputNumber } from "primereact/inputnumber";
import { JSON_EQUAL } from "./../../../utils/utils";
import { Slider } from "primereact/slider";
import { useTranslation } from "react-i18next";

export interface NumberControlProps extends BCProps {}

export interface NumberControlState extends BCStates {}

const NumberControl: React.FC<NumberControlProps> = (props) => {
  const { t } = useTranslation();
  // extract props data
  const id = props.id || "";
  const ruleList = props.ruleList || [];
  if (props.required) {
    ruleList.push({
      name: "required",
    });
  }
  let placeholder = props.placeholder;
  if (props.max !== undefined && props.min !== undefined) {
    placeholder = t("Input number {{min}} - {{max}}", {
      min: props.min,
      max: props.max,
    });
  } else if (props.max !== undefined) {
    placeholder = t("Input number less than {{max}}", {
      max: props.max,
    });
  } else if (props.min !== undefined) {
    placeholder = t("Input number more than {{min}}", {
      min: props.min,
    });
  } else {
    placeholder = t("Input number");
  }
  // init state control
  let initState: NumberControlState = {
    touched: false,
    value: props.value,
    valueStr: (props.value || "").toString(),
    controlState: {
      invalid: false,
    },
  };
  initState.controlState =
    props.controlState || validateControl(ruleList || [], initState.value, t);
  // Update state if value changed
  const [state, setState] = useState(initState);
  // unsubcribe
  const mountedRef = useRef(true);
  useEffect(() => {
    return () => {
      mountedRef.current = false;
    };
  }, []);
  useEffect(() => {
    let value = props.value;

    const ruleList = props.ruleList || [];
    if (props.required) {
      ruleList.push({
        name: "required",
      });
    }
    let controlState =
      props.controlState || validateControl(ruleList || [], value, t);
    setState({
      touched: false,
      value,
      valueStr: (value || "").toString(),
      controlState,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.value, props.controlState]);

  // Update if rule update
  useEffect(() => {
    if (!mountedRef.current) return;
    onChange(state.value);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.required, props.ruleList, props.max, props.min]);

  const onChange = async (value: any) => {
    if (props.max && value > props.max) {
      value = props.max;
    }
    if (props.min && value < props.min) {
      value = props.min;
    }

    const valueStr = value ? value.toString() : "";
    const controlState = validateControl(ruleList, value, t);
    let _state = {
      ...state,
      value,
      valueStr,
      controlState,
      loading: false,
    };

    if (props.onChange) {
      props.onChange({
        controlState: _state.controlState,
        value: _state.value,
        valueStr: _state.valueStr,
      });
    }
    if (props.onTrueUpdateValue) {
      props.onTrueUpdateValue({
        controlState: _state.controlState,
        value: _state.value,
        valueStr: _state.valueStr,
      });
    }
    if (!JSON_EQUAL(_state, state)) {
      setState(_state);
    }
  };
  const onBlur = () => {
    if (props.onTrueUpdateValue) {
      props.onTrueUpdateValue({
        controlState: state.controlState,
        value: state.value,
        valueStr: state.valueStr,
      });
    }
  };
  const onFocus = () => {
    if (props.onTouched) {
      return props.onTouched();
    }
    if (!state.touched) {
      setState({
        ...state,
        touched: true,
      });
    }
  };
  const onKeyUp = (event: any) => {
    if (event.key === "Enter") {
      if (props.onTrueUpdateValue) {
        props.onTrueUpdateValue({
          controlState: state.controlState,
          value: state.value,
          valueStr: state.valueStr,
        });
      }
    }
  };

  return (
    <>
      <div
        className={`number-control-inner p-field ${
          props.noLabel ? "no-label" : ""
        }`}
      >
        <label htmlFor={props.id}>
          {props.label}
          {props.required && !props.noRequiredLabel ? (
            <small className="required p-invalid">&nbsp;*</small>
          ) : null}
        </label>
        <div className="p-inputgroup p-inputgroup-number">
          <InputNumber
            {...props.config}
            showButtons
            id={id}
            className={`${props.className} ${
              state.controlState.invalid && state.touched ? "p-invalid" : ""
            } `}
            max={props.max}
            min={props.min}
            placeholder={placeholder}
            autoFocus={props.autoFocus}
            value={state.value}
            onValueChange={(event) => onChange(event.value)}
            onBlur={() => onBlur()}
            onKeyUp={(event: any) => onKeyUp(event)}
            onFocus={() => onFocus()}
          />
          {props.slider ? (
            <Slider
              className="silder-number"
              value={state.value}
              max={props.max}
              min={props.min}
              onChange={(event) => {
                onChange(event.value);
              }}
            />
          ) : null}
        </div>
        {state.controlState.invalid && (state.touched || props.touched) ? (
          <small
            id={`${props.id}-error`}
            className="p-invalid p-d-block p-invalid-custom"
          >
            {state.controlState.error}
          </small>
        ) : null}
      </div>
    </>
  );
};

export default NumberControl;
