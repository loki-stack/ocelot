import "./date-control.scss";

import { BCProps, BCStates, validateControl } from "./../base-control";
import React, { useEffect, useRef, useState } from "react";

import { Calendar } from "primereact/calendar";
import { JSON_EQUAL, SLEEP } from "./../../../utils/utils";
import moment from "moment";
import { useTranslation } from "react-i18next";

export interface DateControlProps extends BCProps {}

export interface DateControlState extends BCStates {}
const DATE_FORMART = "dd/mm/yy";
const DateControl: React.FC<DateControlProps> = (props) => {
  const { t } = useTranslation();
  // extract props data
  const id = props.id || "";
  const ruleList = props.ruleList || [];
  if (props.required) {
    ruleList.push({
      name: "required",
    });
  }

  let value;
  let valueStr = "";
  if (props.value) {
    try {
      value = moment(props.value).toDate();
      valueStr = moment(value).format("DD/MM/YYYY");
    } catch (error) {}
  }

  // init state control
  let initState: DateControlState = {
    touched: false,
    loading: true,
    value,
    valueStr,
    controlState: {
      invalid: false,
    },
  };
  initState.controlState =
    props.controlState || validateControl(ruleList || [], initState.value, t);
  const [state, setState] = useState(initState);
  const mountedRef = useRef(true);
  // unsubcribe
  useEffect(() => {
    return () => {
      mountedRef.current = false;
    };
  }, []);
  // Update state if control state changed
  useEffect(() => {
    const getData = async () => {
      await SLEEP(1);
      const ruleList = props.ruleList || [];
      if (props.required) {
        ruleList.push({
          name: "required",
        });
      }
      let value;
      let valueStr = "";
      if (props.value) {
        try {
          value = moment(props.value).toDate();
          valueStr = moment(value).format("DD/MM/YYYY");
        } catch (error) {}
      }
      let controlState =
        props.controlState || validateControl(ruleList || [], value, t);
      if (!mountedRef.current) return;
      setState({
        ...state,
        loading: false,
        value,
        valueStr,
        controlState,
      });
    };
    getData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.controlState, props.required, props.value]);

  useEffect(() => {
    if (
      props.defaultValue &&
      props.defaultValue?.getTime() !== state.lastDefault?.getTime()
    ) {
      onChange(props.defaultValue, true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.defaultValue, state.lastDefault]);

  const onChange = async (value: any, updateLastDefault = false) => {
    const valueStr = moment(value).format("DD/MM/YYYY");
    const controlState = validateControl(ruleList, value, t);
    await SLEEP(1);
    let _state = {
      ...state,
      value,
      valueStr,
      controlState,
      loading: false,
    } as any;
    if (updateLastDefault) {
      _state.lastDefault = props.defaultValue;
    }
    if (props.onChange) {
      props.onChange({
        controlState: _state.controlState,
        value: _state.value,
        valueStr: _state.valueStr,
      });
    }
    if (props.onTrueUpdateValue) {
      props.onTrueUpdateValue({
        controlState: _state.controlState,
        value: _state.value,
        valueStr: _state.valueStr,
      });
    }
    if (!JSON_EQUAL(_state, state)) {
      setState(_state);
    }
  };
  const onFocus = () => {
    if (props.onTouched) {
      props.onTouched();
      return;
    }
    if (!state.touched) {
      setState({
        ...state,
        touched: true,
      });
    }
  };
  const onKeyUp = (event: any) => {
    if (event.key === "Enter") {
      if (props.onTrueUpdateValue) {
        props.onTrueUpdateValue({
          controlState: state.controlState,
          value: state.value,
          valueStr: state.valueStr,
        });
      }
    }
  };

  return (
    <>
      <div
        className={`date-control-inner p-field ${
          props.noLabel ? "no-label" : ""
        }`}
      >
        <label htmlFor={props.id}>
          {props.label}
          {props.required && !props.noRequiredLabel ? (
            <small className="required p-invalid">&nbsp;*</small>
          ) : null}
        </label>
        <div className="p-inputgroup">
          {state.loading ? null : (
            <Calendar
              numberOfMonths={1}
              {...props.config}
              dateFormat={DATE_FORMART}
              onKeyUp={onKeyUp}
              placeholder={props.placeholder || t("DD/MM/YYYY")}
              id={id}
              value={state.value}
              onChange={(event) => onChange(event.value)}
              onFocus={() => onFocus()}
              showIcon
              readOnlyInput={props.config?.readOnly}
              disabled={props.config?.readOnly}
              monthNavigator
              yearNavigator
              yearRange={
                props.config.yearRange ||
                `${new Date().getFullYear() - 100}:${
                  new Date().getFullYear() + 100
                }`
              }
            />
          )}
        </div>
        {state.controlState.invalid && (state.touched || props.touched) ? (
          <small
            id={`${props.id}-error`}
            className="p-invalid p-d-block p-invalid-custom"
          >
            {state.controlState.error}
          </small>
        ) : null}
      </div>
    </>
  );
};

export default DateControl;
