import "./checkbox-control.scss";

import { BCProps, BCStates, validateControl } from "./../base-control";
import React, { useState } from "react";

import { Checkbox } from "primereact/checkbox";
import { ProgressBar } from "primereact/progressbar";
import { useTranslation } from "react-i18next";

export interface CheckboxControlProps extends BCProps {}

export interface CheckboxControlState extends BCStates {}

const CheckboxControl: React.FC<CheckboxControlProps> = (props) => {
  const { t } = useTranslation();
  // extract props
  const ruleList = props.ruleList || [];
  if (props.required) {
    ruleList.push({
      name: "required",
    });
  }

  // State
  let initState: CheckboxControlState = {
    touched: false,
    value: props.value ? true : false,
    valueStr: props.value ? t("True") : t("False"),
    controlState: {
      invalid: false,
    },
  };
  initState.controlState =
    props.controlState || validateControl(ruleList || [], initState.value, t);

  // prepare state
  const [state, setState] = useState(initState);
  /**
   * On change
   * @param {*} event
   */
  const onChange = (value: boolean) => {
    const valueStr = value ? t("True") : t("False");
    const controlState = validateControl(ruleList, value, t);
    let _state = {
      ...state,
      value,
      valueStr,
      controlState,
    };
    if (props.onChange) {
      props.onChange({
        controlState: _state.controlState,
        value: _state.value,
        valueStr: _state.valueStr,
      });
    }
    if (props.onTrueUpdateValue) {
      props.onTrueUpdateValue({
        controlState: state.controlState,
        value: state.value,
        valueStr: state.valueStr,
      });
    }
    setState(_state);
  };

  const onFocus = () => {
    if (props.onTouched) {
      props.onTouched();
      return;
    }
    if (!state.touched) {
      setState({
        ...state,
        touched: true,
      });
    }
  };
  return (
    <>
      <div className="p-field">
        <div className="p-inputgroup">
          {props.loading ? (
            <>
              <ProgressBar
                style={{ width: "100%", minHeight: "2.5rem" }}
                mode="indeterminate"
              />
            </>
          ) : (
            <>
              <Checkbox
                {...props.config}
                id={props.id}
                className={props.className}
                checked={state.value}
                onChange={(event) => onChange(event.checked)}
                onFocus={() => onFocus()}
                autoFocus={props.autoFocus}
              />
              <label className="p-checkbox-label" htmlFor={props.id}>
                &nbsp;&nbsp;{props.label}
              </label>
            </>
          )}
        </div>
      </div>
    </>
  );
};

export default CheckboxControl;
