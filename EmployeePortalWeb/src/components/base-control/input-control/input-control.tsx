import "./input-control.scss";

import {
  BCProps,
  BCStates,
  renderError,
  validateControl,
} from "./../base-control";
import { JSON_EQUAL, SLEEP } from "./../../../utils/utils";
import React, { useEffect, useRef, useState } from "react";

import { InputText } from "primereact/inputtext";
import { InputTextarea } from "primereact/inputtextarea";
import { Password } from "primereact/password";
import { useTranslation } from "react-i18next";
import { Button } from "primereact/button";

export interface InputControlProps extends BCProps {}
export interface InputControlState extends BCStates {}
const InputControl: React.FC<InputControlProps> = (props) => {
  const { t } = useTranslation();
  // extract props data
  const id = props.id || "";
  const lazy = props.fromFilter;
  const ruleList = props.ruleList || [];
  if (props.required) {
    ruleList.push({
      name: "required",
    });
  }
  let maxLength = props.maxLength;

  if (!maxLength) {
    if (props.type === "textarea") {
      maxLength = 500;
    } else {
      maxLength = 100;
    }
  }

  if (maxLength) {
    ruleList.push({
      name: "maxLength",
      param: maxLength,
    });
  }

  if (props.minLength) {
    ruleList.push({
      name: "minLength",
      param: props.minLength,
    });
  }
  if (props.type === "email") {
    ruleList.push({
      name: "pattern",
      param: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      error: t("Email invalid"),
    });
  }
  if (props.showPasswordHint) {
    ruleList.push({
      name: "pattern",
      param: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/,
      error: t("Password invalid"),
    });
  }

  if (props.pattern) {
    ruleList.push({
      name: "pattern",
      param: props.pattern,
      error: props.patternMessage || t("Field invalid"),
    });
  }

  // init state control
  let initState: InputControlState = {
    touched: false,
    value: props.value,
    valueStr: (props.value || "").toString(),
    controlState: {
      invalid: false,
    },
  };
  initState.controlState =
    props.controlState || validateControl(ruleList || [], initState.value, t);
  const [state, setState] = useState(initState);
  const mountedRef = useRef(true);
  // unsubcribe
  useEffect(() => {
    return () => {
      mountedRef.current = false;
    };
  }, []);
  // Update state if control state changed
  useEffect(() => {
    const ruleList = props.ruleList || [];
    if (props.required) {
      ruleList.push({
        name: "required",
      });
    }

    let controlState =
      props.controlState || validateControl(ruleList || [], props.value, t);
    if (!mountedRef.current) return;
    setState({
      ...state,
      value: props.value,
      setDefault: true,
      controlState,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.controlState, props.required, props.value]);
  // Update if rule update
  useEffect(() => {
    if (!mountedRef.current) return;
    onChange(state.value);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.required, props.ruleList]);

  useEffect(() => {
    if (
      props.defaultValue &&
      props.defaultValue !== state.lastDefault &&
      state.setDefault
    ) {
      onChange(props.defaultValue, true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.defaultValue, state.lastDefault]);

  // Update display value if state value changed
  useEffect(() => {
    if (lazy) {
      try {
        if (id) {
          let elm = document.getElementById(id) as any;
          if (elm) elm.value = state.value || "";
        }
      } catch (error) {}
    }
  }, [id, lazy, state.value]);

  // onbeforeunload
  useEffect(() => {
    if (props.type === "textarea" && props.viewMode) {
      try {
        let listTextArea = document.querySelectorAll(`#${id}`);
        [].forEach.call(listTextArea, function (textArea: any) {
          textArea.style.height = textArea.scrollHeight + "px";
        });
      } catch (error) {}
    }
  });

  const onChange = async (value: string, updateLastDefault = false) => {
    if (value && maxLength && state.value && value.length > maxLength + 1) {
      value = value.substring(0, maxLength + 1);
    }

    const valueStr = value ? value.toString() : "";
    const controlState = validateControl(ruleList, value, t);
    let _state = { ...state };
    if (updateLastDefault) {
      _state.lastDefault = props.defaultValue;
    }
    // onChange
    if (!lazy && props.onChange) {
      props.onChange({
        controlState: controlState,
        value: value,
        valueStr: valueStr,
      });
    }
    if (!lazy) {
      // do set state
      _state.value = value;
      _state.valueStr = valueStr;
      _state.controlState = controlState;
      if (!JSON_EQUAL(_state, state)) {
        setState(_state);
      }
      return;
    }
    await localStorage.setItem(id, value);
    await SLEEP(500);
    const storageValue = await localStorage.getItem(id);
    if (storageValue === value) {
      if (props.onTrueUpdateValue) {
        props.onTrueUpdateValue({
          controlState: controlState,
          value: value,
          valueStr: valueStr,
        });
      }
      if (lazy && props.onChange) {
        props.onChange({
          controlState: controlState,
          value: value,
          valueStr: valueStr,
        });
      }

      if (lazy) {
        _state.value = value;
        _state.valueStr = valueStr;
        _state.controlState = controlState;
        if (_state !== state) {
          setState(_state);
        }
      }
    }
  };
  const onBlur = () => {
    if (props.onTrueUpdateValue) {
      props.onTrueUpdateValue({
        controlState: state.controlState,
        value: state.value,
        valueStr: state.valueStr,
      });
    }
  };
  const onKeyUp = (event: any) => {
    if (event.key === "Enter") {
      if (props.onTrueUpdateValue) {
        props.onTrueUpdateValue({
          controlState: state.controlState,
          value: state.value,
          valueStr: state.valueStr,
        });
      }
    }
  };
  const onKeyDown = (event: any) => {
    if (props.fromFilter) {
      try {
        if (id) {
          let elm = document.getElementById(id) as any;
          var startPos = elm.selectionStart;
          if (
            (!elm.value || elm.value.trim() === "" || startPos === 0) &&
            event.keyCode === 32
          ) {
            event.preventDefault();
          }
        }
      } catch (error) {}
    }
  };
  const onFocus = () => {
    if (props.onTouched) {
      props.onTouched();
      return;
    }
    if (!state.touched) {
      setState({
        ...state,
        touched: true,
      });
    }
  };
  const renderControl = () => {
    let commonProp = {
      ...props.config,
      id,
      className: `${props.className} ${
        state.controlState.invalid && state.touched ? "p-invalid" : ""
      }`,
      placeholder: props.placeholder || t("Input text"),
      autoFocus: props.autoFocus,
      onChange: (event: any) => onChange(event.currentTarget.value),
      onBlur: () => onBlur(),
      onKeyUp: (event: any) => onKeyUp(event),
      onFocus: () => onFocus(),
      onKeyDown: (event: any) => onKeyDown(event),
      tooltipOptions: { position: "top" },
    };
    commonProp.tooltip = state.valueStr || "";
    if (!lazy) {
      commonProp.value = state.value || "";
    }

    if (props.revealState !== undefined && !props.revealState) {
      //added custom value - aw
      //console.log('-----input-control --- show as *****--', props.revealState);//added custom value - aw
      commonProp.value = "********"; //added custom value - aw
    } //added custom value - aw
    else {
      //added custom value - aw
      //console.log('-----input-control---reveal--', props.revealState);//added custom value - aw
    } //added custom value - aw
    if (props.type === "textarea") {
      if (props.config?.readOnly) {
        commonProp.rows = 1;
        commonProp.autoResize = true;
      }
      return <InputTextarea rows={4} {...commonProp} />;
    }
    if (props.id === "employeeProfile-dob") {
      //added custom value - aw
    } //added custom value - aw
    if (props.type === "password") {
      commonProp.tooltip = null;
      // if (props.id === 'employeeProfile-dob') {//added custom value - aw
      //   console.log('---input-control--render--2', props.revealState, commonProp.value, props);//added custom value - aw
      // }//added custom value - aw
      let pholder = props.placeholder || t("Input password"); //added custom value - aw
      if (props.revealState !== undefined && !props.revealState) {
        //added custom value - aw
        pholder = "*******"; //added custom value - aw
      } //added custom value - aw

      return (
        <Password
          promptLabel={t("Please enter a password")}
          weakLabel={t("Week")}
          mediumLabel={t("Medium")}
          strongLabel={t("Strong")}
          panelClassName="bc-pc-panel"
          feedback={true}
          {...commonProp}
          placeholder={pholder}
        />
      );
    }
    return <InputText {...commonProp} />;
  };
  const renderAction = () => {
    if (!props.action) {
      return null;
    } else {
      return (
        <Button
          label={props.action?.label}
          icon={props.action?.icon}
          tooltip={props.action?.tooltip}
          tooltipOptions={{ position: "top" }}
          onClick={props.action?.clickFn}
          onMouseDown={props.action?.onMouseDown} // added custom function - aw
          onMouseLeave={props.action?.onMouseLeave} // added custom function - aw
          onTouchStart={props.action?.onTouchStart} // added custom function - aw
          onTouchEnd={props.action?.onTouchEnd} // added custom function - aw
          onMouseUp={props.action?.onMouseUp} // added custom function - aw
          onTouchMove={props.action?.onTouchMove} // added custom function - aw
          className={props.action?.className + " custom-input-action-btn"}
          type="button"
        />
      );
    }
  };
  const renderPasswordHint = () => {
    let rule1 = true;
    let rule2 = true;
    let rule3 = true;
    let rule4 = true;
    let rule5 = true;
    let value = state.value;
    if (!value || value.length < 6) {
      rule1 = false;
    }
    if (!value || !value.match(/(.*\d.*)/)) {
      rule2 = false;
    }
    if (!value || !value.match(/(.*[a-z].*)/)) {
      rule3 = false;
    }
    if (!value || !value.match(/(.*[A-Z].*)/)) {
      rule4 = false;
    }
    if (!value || !value.match(/^([A-Za-z\d]+)$/)) {
      rule5 = false;
    }
    if (rule1 && rule2 && rule3 && rule4 && rule5) {
      return;
    }
    return (
      <div className="passwordHint">
        <span className="message">{t("Password must include at least")}</span>
        <ul>
          <li>
            <i
              className={
                rule1
                  ? "pi pi-check-circle valid"
                  : "pi pi-exclamation-circle invalid"
              }
            ></i>
            {t("6 characters")}
          </li>
          <li>
            <i
              className={
                rule2
                  ? "pi pi-check-circle valid"
                  : "pi pi-exclamation-circle invalid"
              }
            ></i>
            {t("1 number")}
          </li>
          <li>
            <i
              className={
                rule3
                  ? "pi pi-check-circle valid"
                  : "pi pi-exclamation-circle invalid"
              }
            ></i>
            {t("1 lower case")}
          </li>
          <li>
            <i
              className={
                rule4
                  ? "pi pi-check-circle valid"
                  : "pi pi-exclamation-circle invalid"
              }
            ></i>
            {t("1 upper case")}
          </li>
          <li>
            <i
              className={
                rule5
                  ? "pi pi-check-circle valid"
                  : "pi pi-exclamation-circle invalid"
              }
            ></i>
            {t("Alphanumeric only")}
          </li>
        </ul>
      </div>
    );
  };
  return (
    <>
      <div
        className={`input-control-inner p-field ${
          props.noLabel ? "no-label" : ""
        }`}
      >
        <label htmlFor={props.id}>
          {props.label}
          {props.required && !props.noRequiredLabel ? (
            <small className="required p-invalid">&nbsp;*</small>
          ) : null}
          {props.tooltip ? (
            <Button
              type="button"
              tooltip={props.tooltip}
              tooltipOptions={{ position: "top" }}
              icon="pi pi-info-circle"
              className="p-button-rounded label-help p-button-text p-button-plain"
            />
          ) : null}
        </label>
        <div className="p-inputgroup">
          {renderControl()}
          {renderAction()}
        </div>
        {props.showPasswordHint
          ? renderPasswordHint()
          : renderError(state, props)}
      </div>
    </>
  );
};

export default InputControl;
