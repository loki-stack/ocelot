import "./App.scss";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import "primeflex/primeflex.css";
import "react-lazy-load-image-component/src/effects/blur.css";
// routing
import { Redirect, Route, Switch } from "react-router-dom";
import { useSelector } from "react-redux";

// components
import MainModal from "./components/main-modal/main-modal";
import MainSideModal from "./components/main-side-modal/main-side-modal";
import MainToast from "./components/main-toast/main-toast";
import Login from "./portal/modules/public/login/login";
import Support from "./portal/modules/public/support/support";
import Portal from "./portal/portal";
import { AppConstant } from "./constants";
import GuardedRoute from "./components/guarded-route/guarded-route";

/**
 * Application component
 */
function App() {
  const auth = useSelector((state) => state.auth);
  console.log("--app----auth-", auth);

  return (
    <>
      <Switch>
        <GuardedRoute
          path={"/" + AppConstant.PORTAL}
          component={Portal}
          auth={auth?.isLoggedIn}
        />
        <Route path="/:clientCode/:entityCode/passport" component={Login} />
        <Route path="/:clientCode/:entityCode/support" component={Support} />
        <Route exact path={`/`}>
          <Redirect to={`/Client A/ENTITYA1/passport/login`} />{" "}
          {/* TO BE REMOVED after test!!!! */}
        </Route>
      </Switch>
      <MainModal />
      <MainSideModal />
      <MainToast />
    </>
  );
}

export default App;
