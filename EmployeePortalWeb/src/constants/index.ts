/**
 * Module code
 */
export enum AppConstant {
  PORTAL = "hris-ess",
}

export enum ModuleCode {
  EMP = "EMP",
}

export const FILE_HOST = "http://localhost:5000";

export enum MODULE_CODE {
  SecurityManagement = "SEC",
  ClientManagement = "CAM",
  Common = "COM",
  SystemConfiguration = "CFG",
  EmploymentManagement = "EMP",
  LeaveManagement = "LEV",
  Payroll = "PAY",
  Workflow = "WOF",
  Messaging = "MSG",
  Reporting = "RPT",
  EmployeeSelfService = "ESS",
}

export enum GENERIC_CODE {
  NAMEPREFIX = "NAMEPREFIX",
  COUNTRY = "COUNTRY",
  GENDER = "GENDER",
  MARITALSTATUS = "MARITALSTATUS",
  NATIONALITY = "NATIONALITY",
  NATURE = "NATURE",
  AREA = "AREA",
  DISTRICT = "DISTRICT",
  RELATIONSHIP = "RELATIONSHIP",

  SCHEME_TYPE = "SCHEMETYPE",
  CONTRIBUTION_CYCLE = "CONTRIBUTION",
  PAYMENT_METHOD = "PAYMENTMETHOD",
  CURRENCY = "CURRENCY",
  STATUS = "STATUS",
  FEE_UNIT = "FEEUNIT",
  ROUNDING_TYPE = "ROUNDINGTYPE",
  LOCALE = "LOCALE",
  TERREASON = "TERREASON",
  SALARYUNIT = "SALARYUNIT",
  STAFFTAGS = "STAFFTAGS",
  EXTERNALJOBTITLE = "EXTERNALJOBTITLE",
  INTERNALJOBTITLE = "INTERNALJOBTITLE",
  INTERFACEFILEFORMAT = "INTERFACEFILEFORMAT",
  STATEMENTFORMAT = "STATEMENTFORMAT",
  REASONDEPARTURE = "REASONDEPARTURE",
  EESTATUS = "EESTATUS",
  STAFFTYPE = "STAFFTYPE",
  WORKCALENDAR = "WORKCALENDAR",
}
export enum GENERIC_CODE_RES {
  GET_COMPONENT_ITEM = "1", // [ locale]
  GET_COMPONENT = "2",
  GET_FILTER = "3",
  GET_TREE = "4",
}
export enum PAYROLL_ITEM_CODE {
  MC = "3",
  VC_EE = "4",
  VC_ER = "5",
}
export enum STATUS_CODE {
  ACTIVE = "AC",
}

export enum GENERIC_CODE_COST {
  MARRIED = "MA",
  CHINA = "CN",
  OTHER = "OT",
  CLOSED = "CLOSED",
}

export enum FILE_CATEGORIES {
  PAYSLIP = "PAYSLIP",
}
