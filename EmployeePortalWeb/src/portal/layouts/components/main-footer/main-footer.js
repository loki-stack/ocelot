import "./main-footer.scss";

import React, { useState } from "react";

import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";

const MainFooter = () => {
  const { t } = useTranslation();
  let history = useHistory();
  const [state, setState] = useState({
    menuIndex: 1,
  });
  const clickFooterMenu = (menuIndex) => {
    setState({
      menuIndex,
    });
  };
  const homeMenu = (menuIndex) => {
    setState({
      menuIndex,
    });
    history.push(`/hris-ess/EMP/EMP5001/view`);
  };
  const payslipMenu = (menuIndex) => {
    setState({
      menuIndex,
    });
    history.push(`/hris-ess/PAY/PAY5001/search`);
  };
  const taxreturnMenu = (menuIndex) => {
    setState({
      menuIndex,
    });
    history.push(`/hris-ess/PAY/PAY5101/search`);
  };
  const infoMenu = (menuIndex) => {
    setState({
      menuIndex,
    });
    history.push(`/hris-ess/EMP/EMP5001/view`);
  };
  return (
    <div className="l-footer">
      <div
        className={`l-footer-item  ${state.menuIndex === 1 ? "active" : ""}`}
        onClick={() => homeMenu(1)}
      >
        <div className="l-footer-item-content">
          <i className="pi pi-home"></i>
          <small>{t("Menu.Home")}</small>
        </div>
      </div>
      <div
        className={`l-footer-item  ${state.menuIndex === 2 ? "active" : ""}`}
        onClick={() => infoMenu(2)}
      >
        <div className="l-footer-item-content">
          <i className="pi pi-users"></i>
          <small>{t("Menu.Info")}</small>
        </div>
      </div>
      <div
        className={`l-footer-item  ${state.menuIndex === 3 ? "active" : ""}`}
        onClick={() => payslipMenu(3)}
      >
        <div className="l-footer-item-content">
          <i className="pi pi-money-bill"></i>
          <small>{t("Menu.Payslip")}</small>
        </div>
      </div>
      <div
        className={`l-footer-item  ${state.menuIndex === 4 ? "active" : ""}`}
        onClick={() => taxreturnMenu(4)}
      >
        <div className="l-footer-item-content">
          <i className="pi pi-money-bill"></i>
          <small>{t("Menu.Tax")}</small>
        </div>
      </div>
      <div
        className={`l-footer-item  ${state.menuIndex === 5 ? "active" : ""}`}
        onClick={() => clickFooterMenu(5)}
      >
        <div className="l-footer-item-content">
          <i className="pi pi-user"></i>
          <small>{t("Profile")}</small>
        </div>
      </div>
    </div>
  );
};

export default MainFooter;
