import "./main-user.scss";

import React, { useRef, useState } from "react";
import { useEffect } from "react";
import { StorageKey, StorageSerive } from "../../../../services/storageService";

import { Button } from "primereact/button";
import { OverlayPanel } from "primereact/overlaypanel";
import { TieredMenu } from "primereact/tieredmenu";
// import { useParams } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { useSelector, useDispatch } from "react-redux";

import { setSideBar } from "../../../../redux/actions/sidebar";
import MainPasswordDialog from "./main-password-dialog";
import { setAuthInfo } from "../../../../redux/actions/auth";
import { FileService } from "../../../../services/employee/index";
import { ApiRequest } from "../../../../services/utils/index";

/**
 *
 */
const MainUser = () => {
  const { t } = useTranslation();
  // let { portal } = useParams();
  let op = useRef();

  const [state, setState] = useState({});
  const toggleNotification = async (e) => {
    op.toggle(e);
    let userInfo = await StorageSerive.getItem("USER_INFO");
    try {
      userInfo = JSON.parse(userInfo);
    } catch (error) {
      userInfo = {};
    }
    setState({
      ...state,
      ...userInfo,
    });
  };

  // --------------------------------------------------------------------------
  const [showPwdHistDialog, setShowPwdHistDialog] = useState(false);
  const [avatar, setAvatar] = useState(null);
  const clientId = useSelector((state) => state.auth.data?.user?.clientId);
  const entityId = useSelector((state) => state.auth.data?.user?.entityId);
  const downloadRes = useRef();
  const [ray] = useState(ApiRequest.createRay("MainUser"));

  const axiosCallback = (resp) => {
    downloadRes.current = resp;
  };

  useEffect(() => {
    const loadAvatar = async (avatarId) => {
      try {
        const imgAvatar = await FileService.fileDownloadFile(
          {
            id: avatarId,
            ray,
            //...apiRequestParam,
            parameterLocale: "en",
            parameterClientId: clientId,
            parameterEntityId: entityId,
          },
          {
            responseType: "blob",
            callback: axiosCallback,
          }
        );

        if (downloadRes.current?.status === 200) {
          setAvatar(URL.createObjectURL(imgAvatar));
          return URL.createObjectURL(imgAvatar);
        } else {
          setAvatar("/assets/images/default.png");
          return "/assets/images/default.png";
        }
      } catch (err) {
        setAvatar("/assets/images/default.png");
        return "/assets/images/default.png";
      }
    };
    if (state.avatar > 0) {
      loadAvatar(state.avatar);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state.avatar]);

  const clientInfo = useSelector((state) => state.global.clientInfo);
  const dispatch = useDispatch();

  // --------------------------------------------------------------------------
  const onLogout = async () => {
    await StorageSerive.removeToken();
    // const tennantInfo = await StorageSerive.getItemJSON(
    //   StorageKey.TENNANT_INFO
    // );
    // const url = `/${tennantInfo.data.client.clientCode}/${tennantInfo.data.client.entityCode}`;
    const url = `/${clientInfo?.clientCode}/${clientInfo?.entityCode}/passport/login`;
    await StorageSerive.removeItem(StorageKey.USER_INFO);
    await StorageSerive.removeItem(StorageKey.TENNANT_INFO);
    // history.push(`/${portal}/passport/login`);
    // window.location.href = `/${portal}/passport/login`;
    dispatch(setSideBar({}));
    dispatch(setAuthInfo({ isLoggedIn: false }));
    window.location.href = url;
    window.sessionStorage.removeItem("path_after_login");
  };

  // --------------------------------------------------------------------------
  const showPasswordHist = () => {
    console.log("--main-user--showPasswordHist");
    setShowPwdHistDialog(true);
  };

  const closePasswordHist = () => {
    setShowPwdHistDialog(false);
  };

  // --------------------------------------------------------------------------
  const handleRenderPwdHistDialog = () => {
    if (!showPasswordHist) {
      return <></>;
    }

    return (
      <MainPasswordDialog
        closeFn={closePasswordHist}
        visible={showPwdHistDialog}
      />
    );
  };

  // --------------------------------------------------------------------------
  const items = [
    // {
    //   label: t("Edit profile"),
    //   icon: "pi pi-fw pi-briefcase",
    // },
    // {
    //   label: t("View payslip Password"),
    //   icon: "pi pi-fw pi-key",
    //   command: (event) => showPasswordHist(),
    // },
    // {
    //   label: t("Settings"),
    //   icon: "pi pi-fw pi-cog",
    // },
    {
      separator: true,
    },
    {
      label: t("Log-out"),
      icon: "pi pi-fw pi-power-off",
      command: (event) => onLogout(),
    },
  ];
  return (
    <>
      <div className="main-user">
        <Button
          type="button"
          onClick={(e) => toggleNotification(e)}
          className="p-button-secondary p-button-text"
          icon={`pi pi-user`}
          iconPos="right"
        ></Button>
        <OverlayPanel
          ref={(el) => (op = el)}
          id="overlay_panel"
          className="main-user-panel"
          style={{ width: "20rem" }}
        >
          <div className="user-info">
            <div className="avatar">
              {state.avatar ? (
                <img
                  alt="user-avatar"
                  // src={loadAvatar(state.avatar)}
                  src={avatar}
                  onError={(e) => (e.target.src = "/assets/images/default.png")}
                />
              ) : (
                <img
                  src={"/assets/images/default.png"}
                  onError={(e) => (e.target.src = "/assets/images/default.png")}
                  alt="hihi"
                />
              )}
            </div>
            <div className="info">
              <p className="fullname">{state.fullname}</p>
              <p className="roleName">{state.roleName}</p>
              <p className="email">{state.email}</p>
            </div>
          </div>
          <TieredMenu model={items} />
        </OverlayPanel>
        {handleRenderPwdHistDialog()}
      </div>
    </>
  );
};
export default MainUser;
