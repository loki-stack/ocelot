import { Button } from "primereact/button";
import React from "react";
import { useTranslation } from "react-i18next";

function MainLogo() {
  const { t } = useTranslation();

  return (
    <>
      <div className="main-logo p-d-none p-d-md-block">
        <Button
          type="button"
          className="p-button-secondary p-button-text"
          iconPos="right"
        >
          {t("app.topTitle")}
        </Button>
      </div>
    </>
  );
}
export default MainLogo;
