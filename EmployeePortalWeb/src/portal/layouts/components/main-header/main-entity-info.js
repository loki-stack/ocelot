import { Button } from "primereact/button";
import React from "react";
// import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";

function MainEntityInfo() {
  // const { t } = useTranslation();
  const clientInfo = useSelector((state) => state.global.clientInfo);

  return (
    <>
      <div className="main-entity-info">
        <Button
          type="button"
          className="p-button-secondary p-button-text"
          iconPos="right"
        >
          <span className="p-d-none p-d-md-inline">
            {clientInfo?.clientNameTxt} -{" "}
          </span>
          {clientInfo?.entityDisplayName}
        </Button>
      </div>
    </>
  );
}
export default MainEntityInfo;
