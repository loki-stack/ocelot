import React from "react";

import { Button } from "primereact/button";
import { Menubar } from "primereact/menubar";

import MainLogo from "./main-logo";
import MainToggle from "./main-toggle";
import MainUser from "./main-user";
import MainEntityInfo from "./main-entity-info";

// scss
import "./main-header.scss";

/**
 *
 */
const MainHeader = () => {
  const renderLeftHeader = () => {
    return (
      <div className="p-d-flex">
        <MainToggle />
        <MainLogo />
        <div className="p-d-none p-d-md-block">
          <Button
            type="button"
            disabled
            className="p-button-secondary p-button-text"
            icon={`pi pi-ellipsis-v`}
            iconPos="right"
          ></Button>
        </div>
        <MainEntityInfo />
      </div>
    );
  };

  const renderRightHeader = () => {
    return (
      <>
        <div className="p-d-sm-block">
          <MainUser />
        </div>
      </>
    );
  };
  return (
    <>
      <div className="main-header">
        <Menubar start={renderLeftHeader()} end={renderRightHeader()} />
      </div>
    </>
  );
};
export default MainHeader;
