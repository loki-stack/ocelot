import "./main-sidebar.scss";

import React, { useEffect, useState } from "react";

import { Animated } from "react-animated-css";
import { ApiRequest } from "./../../../../services/utils/index";
import { CAMELIZE } from "./../../../../utils/utils";
// import { MenuService } from "../../../../services/security";
import { ScrollPanel } from "primereact/scrollpanel";
import { TieredMenu } from "primereact/tieredmenu";
import { Tree } from "primereact/tree";
import { setSideBar } from "../../../../redux/actions/sidebar";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { useLocation } from "react-router-dom";
// import { useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import { AppConstant } from "../../../../constants";

const caculateKeys = (pathname) => {
  const path = pathname;
  let split = path.split("/");
  let _expandedKeys = {};
  let leafKeys;
  let moduleCd, functionCd;
  if (split.length >= 5) {
    moduleCd = split[2];
    functionCd = split[3];
    _expandedKeys[moduleCd] = true;
    _expandedKeys[moduleCd + "-" + functionCd] = true;
    leafKeys = moduleCd + "-" + functionCd;
    return {
      expandedKeys: _expandedKeys,
      leafKeys,
    };
  } else {
    return {
      expandedKeys: undefined,
      leafKeys: undefined,
    };
  }
};
function MainSideBar() {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const sideBarInfo = useSelector((state) => state.sidebar) || null;
  let history = useHistory();
  let location = useLocation();
  // const clientLevel = useSelector(
  //   (state) => state.client.selectedClient?.data?.client?.clientLevel
  // );
  // const entityLevel = useSelector(
  //   (state) => state.client.selectedClient?.data?.entity?.entityLevel
  // );
  const [ray] = useState(ApiRequest.createRay("MainSideBar"));
  // prepare state
  // let { portal } = useParams();
  const portal = AppConstant.PORTAL;
  useEffect(() => {
    const getNavigation = async () => {
      const keys = caculateKeys(location.pathname);
      //   const apiRequestParam = await ApiRequest.createParam();
      //   var res = await MenuService.menuGetMenu({
      //     ray,
      //     ...apiRequestParam,
      //   });

      //   console.log("---sidebar---useEffect", res);

      //   if (res && res.data) {
      //     dispatch(
      //       setSideBar({
      //         mustLoad: false,
      //         data: res.data.menus,
      //         expandedKeys: keys.expandedKeys,
      //         leafKeys: keys.leafKeys,
      //       })
      //     );
      //     // GOTO first navigation
      //     // history.push(`/${portal}/`);
      //   } else {
      let selectedKey = keys.leafKeys;
      if (!selectedKey) {
        return;
      }
      let _expandedKeys = {};
      let split = selectedKey.split("-");
      _expandedKeys[split[0]] = true;

      if (split.length >= 2) {
        _expandedKeys[split[0] + "-" + split[1]] = true;
      }

      // menus.findIndex
      let selectedArr = menus.find((x) => x.moduleCd === split[0]);
      // console.log(
      //   "---sidebar---getNavigation--",
      //   selectedArr,
      //   split[0],
      //   selectedArr.function?.length
      // );

      let newSideBarInfo = {
        selectedKey: Array.isArray(selectedArr.function)
          ? selectedKey
          : split[0],
        expandedKeys: _expandedKeys,
      };
      dispatch(setSideBar(newSideBarInfo));
      //   }
    };
    getNavigation();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    dispatch,
    history,
    location.pathname,
    portal,
    ray,
    // sideBarInfo,
  ]);

  const onSelect = async (e) => {
    let data = e.node.data;
    // console.log("---sidebar---onselect--", data);
    if (data.module && data.function) {
      let link = `/${portal}/${data.function.moduleCd}/${
        data.function.functionCd
      }/${CAMELIZE(data.function.functionNature)}`;
      history.push(link);
      let _expandedKeys = {};

      _expandedKeys[data.module.moduleCd] = true;
      _expandedKeys[
        data.module.moduleCd + "-" + data.function.functionCd
      ] = true;
      dispatch(
        setSideBar({
          expandedKeys: _expandedKeys,
          isOpenSidebarMobile: false,
        })
      );
    }
  };

  const processMenu = (menus) => {
    let items, command, key;
    var result = menus.map((module) => {
      if (module.functions && module.functions.length > 0) {
        items = module.functions.map((_function) => {
          key = module.moduleCd + "-" + _function.functionCd;
          return {
            key,
            label: t(_function.menuName),
            icon: "pi pi-fw pi-calendar-plus",
            className:
              sideBarInfo &&
              sideBarInfo.expandedKeys &&
              sideBarInfo.expandedKeys[key]
                ? "menu-active menu-sub menu-leaf"
                : "menu-sub menu-leaf",
            command: () =>
              onSelect({
                node: {
                  data: {
                    module,
                    function: _function,
                  },
                },
              }),
          };
        });
      }
      command = () =>
        onSelect({
          node: {
            data: {
              module,
            },
          },
        });
      key = module.moduleCd;
      return {
        key,
        label: t(module.moduleName),
        icon: "pi pi-fw pi-calendar-plus",
        className:
          sideBarInfo &&
          sideBarInfo.expandedKeys &&
          sideBarInfo.expandedKeys[key]
            ? "menu-active menu-main"
            : "menu-main",
        command,
        items,
      };
    });
    return result;
  };

  // const fixMenu = (pmenus) => {
  //   let menus = [...pmenus];
  // };

  let menus = [
    //...pmenus,
    {
      moduleCd: "EMP",
      moduleName: "Home", // <-- TODO - use translation i18n
      icon: "pi pi-home",
      function: {
        moduleCd: "EMP",
        functionCd: "EMP5001",
        functionNature: "view",
      },
    },
    {
      moduleCd: "EMP1",
      moduleName: "Info",
      icon: "pi pi-users",
      functions: [],
    },
    {
      moduleCd: "PAY",
      moduleName: "Payroll", // <-- TODO - use translation i18n
      icon: "pi pi-money-bill",
      functions: [
        {
          moduleCd: "PAY",
          functionCd: "PAY5001",
          functionNature: "search",
          icon: "pi pi-money-bill",
          menuName: "Payslip", // <-- TODO - use translation i18n
        },
        {
          moduleCd: "PAY",
          functionCd: "PAY5101",
          functionNature: "search",
          icon: "pi pi-money-bill",
          menuName: "Tax Return", // <-- TODO - use translation i18n
        },
      ],
    },
  ];

  const processMenuTree = (pmenus) => {
    // fix menus
    // console.log('-----processMenuTree-----', pmenus);

    var result = menus.map((module) => {
      let children, key;
      if (module.functions && module.functions.length > 0) {
        children = module.functions.map((_function) => {
          key = module.moduleCd + "-" + _function.functionCd;
          return {
            key,
            label: t(_function.menuName),
            //icon: "pi pi-fw pi-calendar-plus",
            icon: _function.icon,
            className:
              sideBarInfo &&
              sideBarInfo.expandedKeys &&
              sideBarInfo.expandedKeys[key]
                ? "menu-active menu-sub menu-leaf"
                : "menu-sub menu-leaf",
            leaf: true,
            data: {
              module,
              function: _function,
            },
          };
        });
      } else {
      }
      key = module.moduleCd;
      return {
        key: module.moduleCd,
        label: t(module.moduleName),
        // icon: "pi pi-fw pi-calendar-plus",
        icon: module.icon,
        className:
          sideBarInfo &&
          sideBarInfo.expandedKeys &&
          sideBarInfo.expandedKeys[key]
            ? "menu-active menu-main"
            : "menu-sub",
        children,
        data: {
          module,
          function: module.function,
        },
      };
    });
    // console.log("-----processMenuTree----result-", result);
    return result;
  };
  // console.log("---sidebar---render", sideBarInfo);
  return (
    <>
      <div className="p-d-none p-d-md-block">
        <Animated
          animationIn="slideInLeft"
          animationOut="slideOutLeft"
          animationInDuration={200}
          animationOutDuration={200}
          isVisible={!sideBarInfo.isOpenSidebar}
        >
          <div className="card main-sidebar close">
            <TieredMenu model={processMenu(sideBarInfo.data?.leftMenu || [])} />
          </div>
        </Animated>
        <Animated
          animationIn="slideInLeft"
          animationOut="slideOutLeft"
          animationInDuration={200}
          animationOutDuration={200}
          isVisible={sideBarInfo.isOpenSidebar}
        >
          <div className={`card main-sidebar open`}>
            <ScrollPanel>
              <Tree
                className="tree-sidebar"
                value={processMenuTree(sideBarInfo.data?.leftMenu || [])}
                selectionMode="single"
                expandedKeys={sideBarInfo.expandedKeys}
                onToggle={(e) => {
                  // console.log("---sidebar---onToggle--", e.value);
                  dispatch(
                    setSideBar({
                      expandedKeys: e.value,
                    })
                  );
                }}
                selectionKeys={sideBarInfo.selectedKey}
                onSelectionChange={(e) => {
                  let selectedKey = e.value;
                  if (!selectedKey) {
                    return;
                  }
                  let _expandedKeys = {};
                  let split = selectedKey.split("-");
                  _expandedKeys[split[0]] = true;

                  if (split.length >= 2) {
                    _expandedKeys[split[0] + "-" + split[1]] = true;
                    // _expandedKeys[
                    //   split[0] + "-" + split[1] + "-" + split[2]
                    // ] = true;
                  }
                  let newSideBarInfo = {
                    selectedKey: selectedKey,
                    expandedKeys: _expandedKeys,
                  };
                  // console.log(
                  //   "---sidebar---onSelectionChange--",
                  //   e,
                  //   newSideBarInfo
                  // );
                  dispatch(setSideBar(newSideBarInfo));
                }}
                onSelect={(e) => onSelect(e)}
              />
            </ScrollPanel>
          </div>
        </Animated>
      </div>
      <div className="hide-md">
        <Animated
          animationIn="slideInLeft"
          animationOut="slideOutLeft"
          animationInDuration={200}
          animationOutDuration={200}
          isVisible={sideBarInfo.isOpenSidebarMobile}
        >
          <div
            className={`card main-sidebar ${
              sideBarInfo.isOpenSidebarMobile ? "open" : "close"
            }`}
          >
            <ScrollPanel>
              <Tree
                className="tree-sidebar"
                value={processMenuTree(sideBarInfo.data?.leftMenu || [])}
                selectionMode="single"
                expandedKeys={sideBarInfo.expandedKeys}
                onToggle={(e) => {
                  dispatch(
                    setSideBar({
                      expandedKeys: e.value,
                    })
                  );
                }}
                selectionKeys={sideBarInfo.selectedKey}
                onSelectionChange={(e) => {
                  let selectedKey = e.value;
                  if (!selectedKey) {
                    return;
                  }
                  let _expandedKeys = {};
                  let split = selectedKey.split("-");
                  _expandedKeys[split[0]] = true;

                  if (split.length >= 2) {
                    _expandedKeys[split[0] + "-" + split[1]] = true;
                    _expandedKeys[
                      split[0] + "-" + split[1] + "-" + split[2]
                    ] = true;
                  }
                  let newSideBarInfo = {
                    selectedKey: selectedKey,
                    expandedKeys: _expandedKeys,
                  };
                  dispatch(setSideBar(newSideBarInfo));
                }}
                onSelect={(e) => onSelect(e)}
              />
            </ScrollPanel>
          </div>
        </Animated>
      </div>
    </>
  );
}
export default MainSideBar;
