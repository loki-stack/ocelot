import "./horizontal-layout.scss";

import MainHeader from "./components/main-header/main-header";
import MainSideBar from "./components/main-sidebar/main-sidebar";
import MainFooter from "./components/main-footer/main-footer";
import React from "react";
import { useSelector } from "react-redux";

//components
import EmployeeRouter from "./../employee-router";

/**
 * Horizontal Layout
 */
const HorizontalLayout = () => {
  const sideBarInfo = useSelector((state) => state.sidebar) || null;
  return (
    <>
      <MainHeader />
      <MainSideBar />
      <main
        className={`${
          sideBarInfo.isOpenSidebar ? "open-sidebar" : "close-sidebar"
        } main-content`}
      >
        <EmployeeRouter />
      </main>
      <MainFooter />
    </>
  );
};

export default HorizontalLayout;
