import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";

// service
import { VnAuthenticationService } from "./../../../../../services/security";
import { ApiRequest } from "../../../../../services/utils/index";

//component
import { ProgressSpinner } from "primereact/progressspinner";

//custom component
import { getControlModel } from "./../../../../../components/base-control/base-cotrol-model";
import BaseForm from "./../../../../../components/base-form/base-form";

const ForgotPassword = () => {
  const [ray] = useState(ApiRequest.createRay("ResetPassword"));

  const { clientCode, entityCode } = useParams();
  const clientInfo = useSelector((state) => state.global.clientInfo);

  const { t } = useTranslation();
  const entityId = useSelector((state) => state.global.clientInfo?.entityId);
  const clientId = useSelector((state) => state.global.clientInfo?.clientId);
  const [state] = useState({
    form: {
      email: "",
    },
  });

  //state
  const [isLoading, setLoading] = useState(true);
  const [resetLink, setResetLink] = useState("");

  useEffect(() => {
    setLoading(false);
  }, []);

  // functions -------------------------------------------------------------
  const afterFormSubmitFn = async (payload) => {
    const { res, formState } = payload;
    // console.log("---forgot-pwd-after-submit--", payload);
    if (!formState.invalid) {
      if (res) {
        let token = res.data.tempLink;
        setResetLink(
          `/${clientCode}/${entityCode}/passport/reset-password/${token}`
        );
        // return false;
      }
      // return false;
    }
    // formState.invalid = false; // reset
    return false;
  };

  const formSubmitFn = async (payload) => {
    const { form } = payload;
    const res = await VnAuthenticationService.authenticationForgotPassword({
      body: {
        ray,
        parameter: {
          clientId: clientId,
          entityId: entityId,
        },
        data: form.email,
      },
    });
    return res;
  };

  // render
  const renderForgotPasswordForm = {
    controls: [
      getControlModel({
        key: "email",
        label: t("login.Username/Email"),
        placeholder: t("login.YourEmailAddress/Username"),
        required: true,
        noRequiredLabel: true,
      }),
    ],
    layout: {
      rows: [
        {
          columns: [
            {
              control: "email",
            },
          ],
        },
        {
          config: {
            style: {
              marginBottom: "1rem",
              marginTop: "1rem",
            },
          },
          columns: [
            {
              action: {
                type: "submit",
                label: t("common.Next"),
                submitingLabel: t("common.Submitting"),
                config: {
                  kind: "primary",
                  style: {
                    width: "100%",
                    maxWidth: "100%",
                    textAlign: "center",
                  },
                },
              },
            },
          ],
        },
      ],
    },
    formSubmitFn,
    afterFormSubmitFn,
  };

  const renderResetLink = () => {
    if (resetLink.length > 0) {
      return (
        <>
          <br />
          <br />
          <Link className="forgot" to={resetLink}>
            reset link (for testing purpose)
          </Link>
        </>
      );
    }
    return <></>;
  };

  const renderContent = () => {
    if (isLoading) {
      return <ProgressSpinner />;
    }

    return (
      <>
        <BaseForm config={renderForgotPasswordForm} form={state.form} />
        {renderResetLink()}
      </>
    );
  };

  return (
    <>
      <h1>{t("login.forgotPassword")}</h1>
      <h2>{clientInfo?.entityDisplayName}</h2>
      {renderContent()}
    </>
  );
};

export default ForgotPassword;
