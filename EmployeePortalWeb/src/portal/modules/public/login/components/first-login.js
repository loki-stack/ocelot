import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";
import { ProgressSpinner } from "primereact/progressspinner";
import { useParams } from "react-router-dom";
// import { useSelector } from "react-redux";

// service
import { VnAuthenticationService } from "../../../../../services/security";
import { ApiRequest } from "../../../../../services/utils/index";
// components
import { getControlModel } from "../../../../../components/base-control/base-cotrol-model";
import BaseForm from "../../../../../components/base-form/base-form";
// import { PasswordInfo } from "./password-info";
// import { Menu } from 'primereact/menu';

/**
 * Reset password form
 */
const FirstLogin = () => {
  const [ray] = useState(ApiRequest.createRay("FirstLogin"));
  const { clientCode, entityCode } = useParams();
  // component state
  const [isLoading, setLoading] = useState(true);
  const [isVerified, setVerified] = useState(false);
  const [userEmail, setUserEmail] = useState("");
  const [state /* , setState */] = useState({
    form: {
      newPwd: "",
      confirmPwd: "",
    },
  });
  // const [newPwdTouched, setNewPwdTouched] = useState(false);
  const [confirmPwdTouched, setConfirmPwdTouched] = useState(false);
  // const [showTip, setShowTip] = useState("none");
  let history = useHistory();
  const { t } = useTranslation();
  // route
  const { token } = useParams();
  // effects -- CDM
  useEffect(() => {
    const verifyToken = async () => {
      try {
        let res = await VnAuthenticationService.authenticationVerifyToken({
          body: {
            ray,
            data: token,
          },
        });
        // console.log('-----reset-password-CDM--', res);
        if (res?.data?.isVerified) {
          // console.log('-----reset-password-CDM--verified', res?.data?.isVerified);
          setVerified(res?.data.isVerified);
          setUserEmail(res?.data.email);
        }
      } catch (err) {
        // console.error("---err--CDM", err);
      } finally {
        setLoading(false);
      }
    };
    verifyToken();
  }, [token, ray]);

  // functions -------------------------------------------------------------
  const afterFormSubmitFn = async (payload) => {
    const { formState } = payload;
    // console.log("reset true - pending", formState);
    if (!formState.invalid) {
      var npage = window.sessionStorage.getItem("path_after_login");
      if (npage != null) {
        window.sessionStorage.removeItem("path_after_login");
        history.push(npage);
      } else {
        history.push(`/${clientCode}/${entityCode}/passport/login`);
      }
      return true;
    }
    // formState.invalid = false; // reset button
    return false;
  };

  const formSubmitFn = async (payload) => {
    const { form } = payload;
    // console.log("first-login - payload", payload);
    const res = await VnAuthenticationService.authenticationEssFirstLogin({
      body: {
        data: {
          token: token,
          newPassword: form.newPwd,
          confirmPassword: form.confirmPwd,
        },
      },
    });
    return res;
  };

  // render
  const renderNotVerified = () => {
    return (
      <>
        <div>{t("login.error.token")}</div>
      </>
    );
  };

  // const pwdInfo = () => PasswordInfo(state, showTip, t);

  const renderForm = {
    controls: [
      getControlModel({
        key: "newPwd",
        label: t("login.newPassword"),
        placeholder: t("login.newPassword"),
        type: "password",
        minLength: 8,
        showPasswordHint: true,
        // ruleList: [
        //   {
        //     name: "required",
        //   },
        //   {
        //     name: "minLength",
        //     param: 8,
        //   },
        //   {
        //     name: "pattern",
        //     error: "Password Invalid!",
        //     param: "(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[A-Za-z\\d@$!%*#?&]{8,}",
        //   },
        // ],
        // onFocus: () => {
        //   console.log("onFocus");
        //   setNewPwdTouched(true);
        //   var lowerCaseRegex = new RegExp("([a-z])\\w+");
        //   var upperCaseRegex = new RegExp("([A-Z])\\w+");
        //   var isNumberRegex = new RegExp("(\\d)\\w+");
        //   setState({
        //     ...state,
        //     isLowerCase: lowerCaseRegex.test(state.form?.newPwd),
        //     isUpperCase: upperCaseRegex.test(state.form?.newPwd),
        //     isNumber: isNumberRegex.test(state.form?.newPwd),
        //     isLengthMin: state.form?.newPwd.length >= 8,
        //   });
        //   setShowTip("");
        // },
        // touched: { newPwdTouched },
        // onTrueUpdateValue: (e) => {
        //   setShowTip("none");
        //   setNewPwdTouched(false);
        // },
      }),
      getControlModel({
        key: "confirmPwd",
        label: t("login.retypeNewPassword"),
        placeholder: t("login.retypeNewPassword"),
        type: "password",
        required: true,
        touched: { confirmPwdTouched },
        noRequiredLabel: true,
        onFocus: () => {
          setConfirmPwdTouched(true);
        },
        onTrueUpdateValue: (e) => {
          setConfirmPwdTouched(false);
        },
        config: {
          feedback: false, // no need to show feedback on confirm password
        },
      }),
    ],
    layout: {
      rows: [
        {
          columns: [
            {
              control: "newPwd",
            },
          ],
        },
        // {
        //   columns: [
        //     {
        //       component: pwdInfo,
        //     },
        //   ],
        // },
        {
          columns: [
            {
              control: "confirmPwd",
            },
          ],
        },
        {
          config: {
            style: {
              marginBottom: "1rem",
              marginTop: "1rem",
            },
          },
          columns: [
            {
              action: {
                type: "submit",
                label: t("login.createPassword"),
                submitingLabel: t("common.createPassword"),
                config: {
                  kind: "primary",
                  style: {
                    width: "100%",
                    maxWidth: "100%",
                    textAlign: "center",
                  },
                },
              },
            },
          ],
        },
      ],
    },
    formSubmitFn,
    afterFormSubmitFn,
  };

  const renderContent = () => {
    if (isLoading) {
      return <ProgressSpinner />;
    }

    if (isVerified) {
      return <BaseForm config={renderForm} form={state.form} />;
    } else {
      return renderNotVerified();
    }
  };

  return (
    <>
      <h2 className="ep-first-login-title">
        {t("login.yourLoginEmailIs")}{" "}
        <span className="ep-first-login-email">{userEmail}</span>{" "}
      </h2>
      {renderContent()}
    </>
  );
};

export default FirstLogin;
