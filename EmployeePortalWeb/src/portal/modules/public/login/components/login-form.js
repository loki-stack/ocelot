import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch } from "react-redux";
import i18next from "i18next";
import { ProgressSpinner } from "primereact/progressspinner";
import { Button } from "primereact/button";
// service
import { VnAuthenticationService } from "./../../../../../services/security";
import { EmpPortalConfigService } from "./../../../../../services/employee";
// import { EmpPortalConfigService } from "./../../../../../services/employee";
import { ApiRequest } from "../../../../../services/utils/index";
import {
  StorageKey,
  StorageSerive,
} from "./../../../../../services/storageService/index";
//redux functions
import { setAuthInfo } from "./../../../../../redux/actions/auth";
import { setSideBar } from "./../../../../../redux/actions/sidebar";

import BaseForm from "./../../../../../components/base-form/base-form";
import { getControlModel } from "./../../../../../components/base-control/base-cotrol-model";

import "../login.scss";

function LoginForm() {
  const [ray] = useState(ApiRequest.createRay("LoginForm"));
  const dispatch = useDispatch();
  const { t } = useTranslation();
  let history = useHistory();
  const { clientCode, entityCode } = useParams();
  const entityId = useSelector((state) => state.global.clientInfo?.entityId);
  const clientId = useSelector((state) => state.global.clientInfo?.clientId);

  const [state, setState] = useState({
    form: {
      username: "",
      password: "",
    },
    loading: true,
  });
  const [clientInfo, setClientInfo] = useState({});

  // const clientInfo = useSelector((state) => state.global.clientInfo);

  useEffect(() => {
    const getData = async () => {
      setState({ ...state, loading: true });
      //const apiRequestParam = await ApiRequest.createParam();
      const ray = ApiRequest.createRay("Login");
      var res = await EmpPortalConfigService.empPortalConfigGetConfigInfo({
        entityCode: entityCode,
        parameterEntityId: entityId,
        ray,
      });

      if (res && res.data) {
        // console.log("res.data", res.data);
        setClientInfo(res.data);
        const client_obj = {
          data: {
            client: res.data,
            entity: res.data,
          },
          clientName: res.data.clientName,
        };
        await StorageSerive.setItem(
          StorageKey.TENNANT_INFO,
          JSON.stringify(client_obj)
        );
        await StorageSerive.setItem(
          StorageKey.PORTAL_CODE,
          res.data.clientCode + "/" + res.data.entityCode
        );
      }
      setState({ loading: false });
    };
    getData();
  }, [entityCode, dispatch]);

  const formSubmitFn = async (payload) => {
    const { form } = payload;
    let res = {};
    res = await VnAuthenticationService.authenticationEssAuthenticate({
      body: {
        ray,
        parameter: {
          clientId: clientId,
          entityId: entityId,
        },
        data: {
          account: form.username,
          password: form.password,
        },
      },
    });
    return res;
  };

  const afterFormSubmitFn = async (payload) => {
    const { res, formState } = payload;
    if (!formState.invalid) {
      await StorageSerive.setToken(res.data.authen);
      if (res.data.user) {
        await StorageSerive.setItem(
          StorageKey.USER_INFO,
          JSON.stringify(res.data.user)
        );
        // Change language client
        let primaryLang = res.data.user.primaryLang || "en";
        i18next.changeLanguage(primaryLang);
      }

      dispatch(
        setAuthInfo({
          ...res,
          isLoggedIn: true,
        })
      );
      //
      dispatch(setSideBar({ mustLoad: true, selectedKey: "homeCD" })); // to load menu

      // handle redirect to payslip
      var npage = window.sessionStorage.getItem("path_after_login");
      if (npage != null) {
        window.sessionStorage.removeItem("path_after_login");
        history.push(npage);
      } else {
        history.push(`/hris-ess/EMP/EMP5001/view`);
      }
      return true;
    }
    return false;
  };

  const loginBtn = () => {
    if (state.loading == true) {
      return {
        component: () => (
          <Button
            type="submit"
            disabled
            label="Log in"
            style={{
              width: "100%",
              maxWidth: "100%",
              textAlign: "center",
            }}
          ></Button>
        ),
      };
    } else {
      return {
        action: {
          type: "submit",
          label: t("login.login"),
          submitingLabel: t("common.Submitting"),
          config: {
            kind: "primary",
            style: {
              width: "100%",
              maxWidth: "100%",
              textAlign: "center",
            },
          },
        },
      };
    }
  };

  const loginForm = {
    controls: [
      getControlModel({
        key: "username",
        label: t("login.Username/Email"),
        placeholder: t("login.yourname@companydotcom"),
        required: true,
        noRequiredLabel: true,
      }),
      getControlModel({
        key: "password",
        label: t("login.password"),
        type: "password",
        placeholder: t("login.yourPassword"),
        required: true,
        noRequiredLabel: true,
        config: {
          feedback: false, // we don't need to show strength during login
        },
      }),
      getControlModel({
        key: "isRemember",
        label: t("login.remember"),
        type: "checkbox",
      }),
    ],
    layout: {
      rows: [
        {
          columns: [
            {
              control: "username",
            },
          ],
        },
        {
          columns: [
            {
              control: "password",
            },
          ],
        },
        {
          columns: [
            // {
            //   control: "isRemember",
            //   config: {
            //     style: {
            //       flexGrow: 1,
            //     },
            //   },
            // },
            {
              component: () => (
                <Link
                  className="forgot"
                  to={`/${clientCode}/${entityCode}/passport/forgot-password`}
                >
                  {t("Forgot Password?")}
                </Link>
              ),
              config: {
                style: {
                  textAlign: "right",
                  marginBottom: "2.5rem",
                  marginTop: "-0.3rem",
                },
              },
            },
          ],
        },
        {
          config: {
            style: {
              marginBottom: "1rem",
              marginTop: "1rem",
            },
          },
          columns: [loginBtn()],
        },
      ],
    },
    formSubmitFn,
    afterFormSubmitFn,
  };
  return (
    <>
      <span>
        {state.loading ? (
          <span>
            <ProgressSpinner style={{ width: "50px", height: "50px" }} />
          </span>
        ) : (
          <h1>{clientInfo?.entityDisplayName}</h1>
        )}
      </span>
      <BaseForm config={loginForm} form={state.form} />
    </>
  );
}

export default LoginForm;
