import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";
import { ProgressSpinner } from "primereact/progressspinner";
import { useParams } from "react-router-dom";
import { useSelector } from "react-redux";

// service
import { VnAuthenticationService } from "./../../../../../services/security";
import { ApiRequest } from "../../../../../services/utils/index";
// components
import { getControlModel } from "./../../../../../components/base-control/base-cotrol-model";
import BaseForm from "./../../../../../components/base-form/base-form";
// import { PasswordInfo } from "./password-info";

/**
 * Reset password form
 */
const ResetPassword = () => {
  const [ray] = useState(ApiRequest.createRay("ResetPassword"));
  const clientInfo = useSelector((state) => state.global.clientInfo);
  // component state
  const [isLoading, setLoading] = useState(true);
  const [isVerified, setVerified] = useState(false);
  const [userEmail, setUserEmail] = useState("");
  const [state /* , setState */] = useState({
    form: {
      newPwd: "",
      confirmPwd: "",
    },
  });
  const { clientCode, entityCode } = useParams();
  let history = useHistory();

  const { t } = useTranslation();

  // route
  const { token } = useParams();
  // const [showTip, setShowTip] = useState("none");

  // effects -- CDM
  useEffect(() => {
    const verifyToken = async () => {
      try {
        let res = await VnAuthenticationService.authenticationVerifyToken({
          body: {
            ray,
            data: token,
          },
        });
        // console.log('-----reset-password-CDM--', res);
        if (res?.data?.isVerified) {
          // console.log('-----reset-password-CDM--verified', res?.data?.isVerified);
          setVerified(res?.data.isVerified);
          setUserEmail(res?.data.email);
        }
      } catch (err) {
        // console.error("---err--CDM", err);
      } finally {
        setLoading(false);
      }
    };
    verifyToken();
  }, [token, ray]);

  // functions -------------------------------------------------------------
  const afterFormSubmitFn = async (payload) => {
    const { res, formState } = payload;
    // console.log("reset-password", res);
    if (!formState.invalid) {
      if (res) {
        history.push(`/${clientCode}/${entityCode}/passport/login`);
        return false;
      }
      return false;
    }
    formState.invalid = false; // reset button
    return false;
  };

  const formSubmitFn = async (payload) => {
    const { form } = payload;
    const res = await VnAuthenticationService.authenticationResetPassword({
      body: {
        data: {
          token: token,
          newPassword: form.newPwd,
          confirmPassword: form.confirmPwd,
        },
      },
    });
    return res;
  };

  // render
  const renderNotVerified = () => {
    return (
      <>
        <div>{t("login.error.token")}</div>
      </>
    );
  };

  // const pwdInfo = () => PasswordInfo(state, showTip, t);

  const renderResetPasswordForm = {
    controls: [
      getControlModel({
        key: "newPwd",
        label: t("login.newPassword"),
        placeholder: t("login.newPassword"),
        type: "password",
        required: true,
        noRequiredLabel: true,
        config: {
          panelClassName: "ep-pwd-feedback",
        },
        minLength: 8,
        showPasswordHint: true,
        // ruleList: [
        //   {
        //     name: "required",
        //   },
        //   {
        //     name: "minLength",
        //     param: 8,
        //   },
        //   {
        //     name: "pattern",
        //     error: "Password Invalid!",
        //     param: "(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[A-Za-z\\d@$!%*#?&]{8,}",
        //   },
        // ],
        // onFocus: () => {
        //   var lowerCaseRegex = new RegExp("([a-z])\\w+");
        //   var upperCaseRegex = new RegExp("([A-Z])\\w+");
        //   var isNumberRegex = new RegExp("(\\d)\\w+");
        //   setState({
        //     ...state,
        //     isLowerCase: lowerCaseRegex.test(state.form?.newPwd),
        //     isUpperCase: upperCaseRegex.test(state.form?.newPwd),
        //     isNumber: isNumberRegex.test(state.form?.newPwd),
        //     isLengthMin: state.form?.newPwd.length >= 8,
        //   });
        //   setShowTip("");
        // },
        // onTrueUpdateValue: (e) => {
        //   setShowTip("none");
        // },
      }),
      getControlModel({
        key: "confirmPwd",
        label: t("login.comfirmPassword"),
        type: "password",
        placeholder: t("login.comfirmPassword"),
        required: true,
        noRequiredLabel: true,
        config: {
          feedback: false, // we don't need to show feedback for confirm password
        },
      }),
    ],
    layout: {
      rows: [
        {
          columns: [
            {
              control: "newPwd",
            },
          ],
        },
        // {
        //   columns: [
        //     {
        //       component: pwdInfo,
        //     },
        //   ],
        // },
        {
          columns: [
            {
              control: "confirmPwd",
            },
          ],
        },
        {
          config: {
            style: {
              marginBottom: "1rem",
              marginTop: "1rem",
            },
          },
          columns: [
            {
              action: {
                type: "submit",
                label: t("login.resetPassword"),
                submitingLabel: t("common.Submitting"),
                config: {
                  kind: "primary",
                  style: {
                    width: "100%",
                    maxWidth: "100%",
                    textAlign: "center",
                  },
                },
              },
            },
          ],
        },
      ],
    },
    formSubmitFn,
    afterFormSubmitFn,
  };

  const renderContent = () => {
    if (isLoading) {
      return <ProgressSpinner />;
    }

    //
    if (isVerified) {
      return <BaseForm config={renderResetPasswordForm} form={state.form} />;
    } else {
      return renderNotVerified();
    }
  };

  return (
    <>
      <h1>{t("login.resetPassword")}</h1>
      <h2>{clientInfo?.entityDisplayName} </h2>
      <h3 className="ep-first-login-title">
        {t("login.yourLoginEmailIs")}{" "}
        <span className="ep-first-login-email">{userEmail}</span>{" "}
      </h3>
      {renderContent()}
    </>
  );
};

export default ResetPassword;
