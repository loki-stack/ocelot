export const PasswordInfo = (state, showTip, t) => {
  if ("none" === showTip) {
    return <></>;
  }
  return (
    <div className="ep-first-login-menu">
      <ul className="ep-first-login-info-list" role="menu">
        <li className="ep-first-login-info-title" role="presentation">
          {t("login.changePassword-title")}
        </li>
        <li className="ep-first-login-list">
          <span className="pi pi-fw pi-info-circle ep-first-login-info-icon"></span>
          {state.isLengthMin ? (
            <span className="ep-first-login-rule-fulfill">
              {t("login.changePassword-rule1")}
            </span>
          ) : (
            <span>{t("login.changePassword-rule1")}</span>
          )}
        </li>
        <li className="ep-first-login-list">
          <span className="pi pi-fw pi-info-circle ep-first-login-info-icon"></span>
          {state.isNumber ? (
            <span className="ep-first-login-rule-fulfill">
              {t("login.changePassword-rule2")}
            </span>
          ) : (
            <span>{t("login.changePassword-rule2")}</span>
          )}
        </li>
        <li className="ep-first-login-list">
          <span className="pi pi-fw pi-info-circle ep-first-login-info-icon"></span>
          {state.isLowerCase ? (
            <span className="ep-first-login-rule-fulfill">
              {t("login.changePassword-rule3")}
            </span>
          ) : (
            <span>{t("login.changePassword-rule3")}</span>
          )}
        </li>
        <li className="ep-first-login-list">
          <span className="pi pi-fw pi-info-circle ep-first-login-info-icon"></span>
          {state.isUpperCase ? (
            <span className="ep-first-login-rule-fulfill">
              {t("login.changePassword-rule4")}
            </span>
          ) : (
            <span>{t("login.changePassword-rule4")}</span>
          )}
        </li>
      </ul>
    </div>
  );
};
