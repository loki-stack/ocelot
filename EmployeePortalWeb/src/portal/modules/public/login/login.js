import React, { /* useCallback, */ useEffect, useState } from "react";
// import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { useTranslation } from "react-i18next";
import { ProgressSpinner } from "primereact/progressspinner";
import "./login.scss";

import {
  Redirect,
  Route,
  Switch,
  useParams,
  useRouteMatch,
  Link,
} from "react-router-dom";
import {
  StorageKey,
  StorageSerive,
} from "./../../../../services/storageService/index";

// component
import LoginForm from "./components/login-form";
import ForgotPassword from "./components/forgot-password";
import ResetPassword from "./components/reset-password";
import FirstLogin from "./components/first-login";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { EmpPortalConfigService } from "../../../../services/employee";

//service
import { ApiRequest } from "../../../../services/utils/index";
// import { setClientInfo } from "./../../../../redux/actions/client";
// import { setSideBar } from "./../../../../redux/actions/sidebar";

// redux action
import { setGlobal } from "./../../../../redux/actions/global";

function Login() {
  // route
  const { clientCode, entityCode } = useParams();
  const match = useRouteMatch();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  // const entityId = useSelector((state) => state.global.clientInfo?.entityId);
  const [state, setState] = useState({
    loading: true,
  });
  const [clientInfo, setClientInfo] = useState({});
  //const clientInfo = useSelector((state) => state.client?.selectedClientStr);

  useEffect(() => {
    const getData = async () => {
      setState({ ...state, loading: true });
      //const apiRequestParam = await ApiRequest.createParam();
      const ray = ApiRequest.createRay("Login");
      var cmd1 = await EmpPortalConfigService.empPortalConfigGetConfigInfo({
        entityCode: entityCode,
        // parameterEntityId: entityId,
        ray,
      });

      const [res] = await Promise.all([cmd1]);

      if (res && res.data) {
        // console.log("res.data", res.data);
        setClientInfo(res.data);
        dispatch(setGlobal({ clientInfo: res.data }));
        const client_obj = {
          data: {
            client: res.data,
            entity: res.data,
          },
          clientName: res.data.clientName,
        };
        await StorageSerive.setItem(
          StorageKey.TENNANT_INFO,
          JSON.stringify(client_obj)
        );
        await StorageSerive.setItem(
          StorageKey.PORTAL_CODE,
          res.data.clientCode + "/" + res.data.entityCode
        );
      }
      setState({
        loading: false,
      });
    };
    getData();
  }, [entityCode, dispatch]);
  // console.log("---login-useEffect--", state);
  // ---- render
  return (
    <>
      <div className="ep-login">
        <div className="p-grid p-grid-fullpage ep-login-no-margin">
          <div className="p-col-12 p-md-6 p-lg-9 p-d-none p-d-md-block ep-login-panel ep-login-bg">
            <div className="ep-login-img-full">
              <LazyLoadImage
                alt="logo"
                effect="blur"
                src="/assets/images/login-bg.png"
              />
            </div>
            <div className="ep-login-info">
              <div className="p-grid">
                <div className="p-col-11 p-md-11 p-lg-9 p-offset-1">
                  <h2 className="login-info-title">
                    <span>
                      {t("login.title")} -{" "}
                      {state.loading ? (
                        <div className="login-spinner">
                          <ProgressSpinner
                            style={{ width: "25px", height: "25px" }}
                            strokeWidth="6"
                          />
                        </div>
                      ) : (
                        clientInfo.entityDisplayName + " "
                      )}
                    </span>
                  </h2>
                </div>
              </div>
            </div>
          </div>
          <div className="p-col-12 p-md-6 p-lg-3 ep-login-panel">
            <div className="ep-login-panel-form">
              <div className="ep-login-header">
                <Link to={`/`}>
                  <LazyLoadImage
                    className="logo"
                    alt={t("logo")}
                    effect="blur"
                    src="/assets/images/logo.png"
                  />
                </Link>
              </div>
              <div className="ep-login-body">
                <Switch>
                  <Route path={`${match.path}/login`} component={LoginForm} />
                  <Route
                    path={`${match.path}/forgot-password`}
                    component={ForgotPassword}
                  />
                  <Route
                    path={`${match.path}/reset-password/:token`}
                    component={ResetPassword}
                  />
                  <Route exact path={`${match.path}`}>
                    <Redirect
                      to={`/${clientCode}/${entityCode}/passport/login`}
                    />
                  </Route>
                  <Route
                    path={`${match.path}/first-login/:token`}
                    component={FirstLogin}
                  />
                </Switch>
              </div>
              <div className="ep-login-footer">
                {t("login.SYSTEM_ADDRESS")}
                {state.loading ? (
                  []
                ) : (
                  <span>
                    {clientInfo.hasFAQ === false &&
                    clientInfo.hasSupport === false ? (
                      []
                    ) : (
                      <Link to={`/${clientCode}/${entityCode}/support`}>
                        <div className="p-mt-3">
                          <u>{t("login.needHelp")}</u>
                        </div>
                      </Link>
                    )}
                  </span>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Login;
