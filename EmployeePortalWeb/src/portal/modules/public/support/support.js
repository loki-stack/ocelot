import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { Card } from "primereact/card";
import { Accordion, AccordionTab } from "primereact/accordion";
import { ProgressSpinner } from "primereact/progressspinner";
import { Animated } from "react-animated-css";
import { useTranslation } from "react-i18next";
import { useParams, Link } from "react-router-dom";

import "./support.scss";
import { ApiRequest } from "../../../../services/utils";
import { EmpPortalConfigService } from "../../../../services/employee";

function Support() {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { clientCode, entityCode } = useParams();
  const entityId = useSelector((state) => state.global.clientInfo?.entityId);
  const [state, setState] = useState({
    supportItem: [],
    faqItem: [],
    loading: true,
  });

  useEffect(() => {
    const getData = async () => {
      setState({ ...state, loading: true });
      const ray = ApiRequest.createRay("Support");
      var res = await EmpPortalConfigService.empPortalConfigGetSupport({
        parameterEntityId: entityId,
        ray,
      });
      let faqItem = [];
      let supportItem = [];
      if (res && res.data) {
        // console.log("---support-----res.data", res.data);
        faqItem = res.data.faqs ? res.data.faqs : [];
        supportItem = res.data.support ? res.data.support : [];
      }
      setState({
        faqItem,
        supportItem,
        loading: false,
      });
    };
    getData();
  }, [entityCode, dispatch]);
  // console.log("---support-use effect--", state);

  return (
    <>
      <Animated
        animationIn="slideInRight"
        animationOut="slideOutRight"
        animationInDuration={200}
        animationOutDuration={200}
        isVisible={true}
      >
        <div className="support-header">
          <Link to={`/${clientCode}/${entityCode}/passport`}>
            <LazyLoadImage
              className="support-logo"
              alt={t("logo")}
              effect="blur"
              src="/assets/images/logo.png"
            />
          </Link>
        </div>
        <div className="p-grid p-justify-center">
          <div className="p-col-12 p-lg-3 p-md-6 p-mt-5">
            {state.loading ? (
              <Card className="" title={t("login.needAssistance")}>
                <div className="p-col-12 p-lg-3 p-md-6">
                  <ProgressSpinner
                    style={{
                      width: "50px",
                      height: "50px",
                      marginLeft: "10rem",
                    }}
                  />
                </div>
              </Card>
            ) : (
              <Card title={t("login.needAssistance")}>
                {state.supportItem.length == 0 ? (
                  <div className="support-no-data">
                    <p>{t("login.noSupport")}</p>
                  </div>
                ) : (
                  state.supportItem
                    .sort((a, b) => {
                      return (
                        a.supportId - b.supportId ||
                        new Date(a.createdDt).getTime() -
                          new Date(b.createdDt).getTime()
                      );
                    })
                    .map((x) => {
                      return (
                        <div>
                          <p className="support-font">{x.supportContactName}</p>
                          <p className="support-font-spacing">
                            {t("login.tel")} {x.supportPhoneNumber}
                          </p>
                          <p
                            className="p-mb-6"
                            className="support-font-spacing"
                          >
                            {t("login.email")} {x.supportEmail}
                          </p>
                        </div>
                      );
                    })
                )}
              </Card>
            )}
          </div>
          <div className="p-col-12 p-lg-5 p-md-6 p-mt-5">
            {state.loading ? (
              <Card title={t("common.faq")}>
                <div className="p-col-12 p-lg-3 p-md-6">
                  <ProgressSpinner
                    style={{
                      width: "50px",
                      height: "50px",
                      marginLeft: "10rem",
                    }}
                  />
                </div>
              </Card>
            ) : (
              <Card title={t("common.faq")}>
                {state.faqItem.length == 0 ? (
                  <p>{t("login.noFAQ")}</p>
                ) : (
                  <Accordion multiple>
                    {state.faqItem
                      .sort(
                        (a, b) => a.faqOrder - b.faqOrder || a.faqId - b.faqId
                      )
                      .map((x) => {
                        return (
                          <AccordionTab header={x.question}>
                            <p className="support-newline">{x.answer}</p>
                          </AccordionTab>
                        );
                      })}
                  </Accordion>
                )}
              </Card>
            )}
          </div>
        </div>
      </Animated>
    </>
  );
}

export default Support;
