import React, { useState, useEffect, useRef } from "react";
import { useTranslation } from "react-i18next";
import { ApiRequest } from "../../../../../../../services/utils";
import { Animated } from "react-animated-css";
import { Button } from "primereact/button";

import { GenericCodeService } from "../../../../../../../services/security";
import { EmpDataService } from "../../../../../../../services/employee";

import { ProgressSpinner } from "primereact/progressspinner";
import BaseForm from "../../../../../../../components/base-form/base-form";
import { getControlModel } from "../../../../../../../components/base-control/base-cotrol-model";
import {
  GENERIC_CODE,
  GENERIC_CODE_RES,
} from "../../../../../../../constants/index";
import { EpPersonalHideAction } from "../modules/ep-profile-constants/ep-personal-hide-action";

const BankAccount = (props) => {
  const [ray] = useState(ApiRequest.createRay("BankAccount"));
  const { t } = useTranslation();

  const [state, setState] = useState({
    bankAccountItem: [],
    loading: true,
  });
  const mountedRef = useRef(true);
  useEffect(() => {
    return () => {
      mountedRef.current = false;
    };
  }, []);

  useEffect(() => {
    const loadData = async () => {
      setState({ ...state, loading: true });
      const apiRequestParam = await ApiRequest.createParam();
      let enumData = {};
      const cmd1 = GenericCodeService.genericCodeGetGenericCodes({
        ray,
        ...apiRequestParam,
        parameterGenericCodeList: [GENERIC_CODE.CURRENCY],
        dataResData: GENERIC_CODE_RES.GET_COMPONENT_ITEM,
      });

      const cmd2 = EmpDataService.empDataGetBankAccountByEmployeeId({
        ray,
        ...apiRequestParam,
        employeeId: props.employeeId,
      });
      const [res1, res2] = await Promise.all([cmd1, cmd2]);
      if (res1 && res1.data) {
        for (const key in res1.data.genericCodes) {
          if (res1.data.genericCodes.hasOwnProperty(key)) {
            const element = res1.data.genericCodes[key];
            const enumElm = element.map((x) => {
              return {
                label: x.textValue,
                value: x.codeValue,
                data: {
                  ...x,
                },
              };
            });
            enumData[key] = enumElm;
          }
        }
      }
      let bankAccountItem = [];
      let account_default = 0;
      if (res2 && res2.data) {
        bankAccountItem = res2.data?.bankAccounts?.map((x) => {
          return {
            ...x,
            eeBankAccountId: x.eeBankAccountId ? x.eeBankAccountId : t("N/A"),
            transactionReference: x.transactionReference
              ? x.transactionReference
              : t("N/A"),
          };
        });
        account_default = res2.data?.bankAccounts?.findIndex(
          (x) => x.defaultAccount === true
        );
      }

      if (!mountedRef.current) return null;
      setState({
        enumData,
        loading: false,
        account_default,
        bankAccountItem,
      });
    };
    loadData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.employeeId, ray]);

  const renderItem = () => {
    const tab = [];
    if (!state.bankAccountItem || state.bankAccountItem.length === 0) {
      if (props.readOnly) {
        return <div style={{ padding: "1rem" }}>{t("No data")}</div>;
      }
      return null;
    }
    state.bankAccountItem.forEach((data, index) => {
      tab.push(
        <div key={index}>
          <div
            className="c-group-sub-title"
            onClick={() => {
              let _state = { ...state };
              _state[index + "-close"] = !_state[index + "-close"];
              setState(_state);
            }}
          >
            {`${state["account_default"] === index ? t("Default") : ""} ${t(
              "employee-profile.account.Account"
            )} (${index + 1})`}

            <div className="c-group-sub-tile-action">
              <Button
                type="button"
                icon={
                  state[index + "-close"]
                    ? "pi pi-chevron-down"
                    : "pi pi-chevron-up"
                }
                className="p-button-text"
              />
            </div>
          </div>
          <div
            className={`${
              state[index + "-close"] ? "hide-panel" : ""
            } c-group-panel`}
          >
            <EmployeeBankAccountItem
              enumData={state.enumData}
              data={data}
              id={index}
              readOnly={props.readOnly}
            />
          </div>
        </div>
      );
    });
    return tab;
  };

  return (
    <>
      <Animated
        animationIn="slideInRight"
        animationOut="slideOutRight"
        animationInDuration={200}
        animationOutDuration={200}
        isVisible={true}
      >
        {state.loading || state.submitting ? (
          <div className="comp-loading">
            <ProgressSpinner />
          </div>
        ) : null}

        <div className="c-group-title">{t("employee-profile.bank")}</div>
        <div className="c-group-panel">{renderItem()}</div>
      </Animated>
    </>
  );
};

const EmployeeBankAccountItem = (props) => {
  const { t } = useTranslation();
  const [accountNoReveal, setAccountNoReveal] = useState(false);
  const formConfig = {
    controls: [
      getControlModel({
        required: true,
        noRequiredLabel: true,
        key: "accountName",
        label: t("employee-profile.account.BankAccountName"),
      }),
      getControlModel({
        required: true,
        noRequiredLabel: true,
        key: "accountCurrency",
        label: t("employee-profile.account.AccountCurrency"),
        type: "select",
        enum: props.enumData?.currency,
        placeholder: "N/A",
      }),
      getControlModel({
        required: true,
        noRequiredLabel: true,
        key: "bankCode",
        label: t("employee-profile.account.BankCode"),
      }),
      getControlModel({
        required: true,
        noRequiredLabel: true,
        key: "branchCode",
        label: t("employee-profile.account.BranchCode"),
        maxLength: 20,
      }),
      getControlModel({
        ...EpPersonalHideAction(
          "accountNo",
          t("employee-profile.account.AccountNo"),
          accountNoReveal,
          "",
          setAccountNoReveal
        ),
      }),
      getControlModel({
        key: "defaultAccount",
        type: "toogle",
        label: t("employee-profile.account.SetAsDefaultAccount"),
      }),
    ],
    layout: {
      rows: [
        {
          columns: [{ control: "accountName" }],
        },
        {
          columns: [
            {
              control: "accountCurrency",
            },
          ],
        },
        {
          columns: [
            {
              config: {
                className: "p-col-12 p-md-6 p-lg-3 ",
              },
              control: "bankCode",
            },
            {
              config: {
                className: "p-col-12 p-md-6 p-lg-3 ",
              },
              control: "branchCode",
            },
            { control: "accountNo" },
          ],
        },
        {
          columns: [{ control: "defaultAccount" }],
        },
      ],
    },
  };
  return (
    <>
      <BaseForm
        id={"employeeBankAccountItem-" + props.id}
        config={formConfig}
        form={props.data}
        readOnly={props.readOnly}
      />
    </>
  );
};

export default BankAccount;
