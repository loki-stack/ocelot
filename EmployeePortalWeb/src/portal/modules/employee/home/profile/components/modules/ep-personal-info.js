import React, { useState, useEffect, useRef } from "react";
import { useTranslation } from "react-i18next";
import { Animated } from "react-animated-css";
// import { Calendar } from "primereact/calendar";
import { ProgressSpinner } from "primereact/progressspinner";
import moment from "moment";
import { GenericCodeService } from "../../../../../../../services/security";
import { ApiRequest } from "../../../../../../../services/utils";
import { getControlModel } from "./../../../../../../../components/base-control/base-cotrol-model";
import BaseForm from "../../../../../../../components/base-form/base-form";
import ToggleControl from "../../../../../../../components/base-control/toggle-control/toggle-control";
import {
  GENERIC_CODE,
  GENERIC_CODE_RES,
} from "../../../../../../../constants/index";
import {
  EpPersonalHideAction,
  momentDateConvert,
  selectStringConvert,
} from "../modules/ep-profile-constants/ep-personal-hide-action";

const PersonalInfo = (props) => {
  const [ray] = useState(ApiRequest.createRay("PersonalInfo"));
  const { t } = useTranslation();
  const [state, setState] = useState({
    loading: true,
    reveal: false,
  });
  const mountedRef = useRef(true);
  useEffect(() => {
    return () => {
      mountedRef.current = false;
    };
  }, []);
  const [languageToogle, setLanguageToogle] = useState(false);

  useEffect(() => {
    const loadData = async () => {
      setState({ ...state, loading: true });
      const apiRequestParam = await ApiRequest.createParam();
      let enumData = {};

      const cmd1 = GenericCodeService.genericCodeGetGenericCodes({
        ray,
        ...apiRequestParam,
        parameterGenericCodeList: [
          GENERIC_CODE.NAMEPREFIX,
          GENERIC_CODE.MARITALSTATUS,
          GENERIC_CODE.NATIONALITY,
          GENERIC_CODE.COUNTRY,
          GENERIC_CODE.GENDER,
        ],
        dataResData: GENERIC_CODE_RES.GET_COMPONENT_ITEM,
      });

      const [res1] = await Promise.all([cmd1]);
      if (res1 && res1.data) {
        for (const key in res1?.data?.genericCodes) {
          if (res1?.data?.genericCodes.hasOwnProperty(key)) {
            const element = res1?.data?.genericCodes[key];
            const enumElm = element.map((x) => {
              return {
                label: x.textValue,
                value: x.codeValue,
                data: {
                  ...x,
                },
              };
            });
            enumData[key] = enumElm;
          }
        }
      }

      let formattedDOB = moment(props?.data?.dateOfBirth).format("MM/DD/YYYY");
      setState({
        ...state,
        enumData,
        data: {
          ...props.data,
          dob: momentDateConvert(props?.data?.dateOfBirth, t),
          maritalStatusString: selectStringConvert(
            enumData?.maritalstatus,
            props.data?.martialStatus,
            t
          ),
          age:
            props?.data?.dateOfBirth !== null
              ? moment()
                  .diff(moment(formattedDOB, "MM/DD/YYYY"), "years")
                  .toString()
              : "N/A",
        },
        loading: false,
      });
    };
    loadData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ray, props.employeeId, props.data]);

  const [dobReveal, setdobReveal] = useState(false);
  const [martialStatusReveal, setMartialStatusReveal] = useState(false);
  const [citizenidReveal, setcitizenidReveal] = useState(false);
  const [passportidReveal, setpassportidReveal] = useState(false);
  const [ageReveal, setAgeReveal] = useState(false);

  const PersonalInfoFormConfig = {
    controls: [
      getControlModel({
        key: "namePrefix",
        label: t("employee-profile.personal.namePrefix"),
        type: "select",
        enum: state.enumData?.nameprefix,
      }),
      getControlModel({
        key: "firstNamePrimary",
        label: t("employee-profile.personal.firstName"),
      }),
      getControlModel({
        key: "middleNamePrimary",
        label: t("employee-profile.personal.middleName"),
      }),
      getControlModel({
        key: "lastNamePrimary",
        label: t("employee-profile.personal.lastName"),
      }),
      getControlModel({
        key: "christianNamePrimary",
        label: t("employee-profile.personal.christian"),
      }),
      getControlModel({
        ...EpPersonalHideAction(
          "age",
          t("employee-profile.personal.Age"),
          ageReveal,
          "",
          setAgeReveal
        ),
      }),
      getControlModel({
        key: "displayNamePrimary",
        label: t("employee-profile.personal.DisplayName"),
        placeholder:
          state?.data?.firstNamePrimary && state?.data?.lastNamePrimary
            ? `${state?.data?.firstNamePrimary}${
                state?.data?.middleNamePrimary
                  ? ` ${state?.data?.middleNamePrimary}`
                  : ""
              } ${state?.data?.lastNamePrimary}`
            : "N/A",
      }),

      getControlModel({
        key: "firstNameSecondary",
        label: t("employee-profile.personal.FirstNameAlternativeLanguage"),
      }),
      getControlModel({
        key: "middleNameSecondary",
        label: t("employee-profile.personal.MiddleNameAlternativeLanguage"),
      }),
      getControlModel({
        key: "lastNameSecondary",
        label: t("employee-profile.personal.LastNameAlternativeLanguage"),
      }),
      getControlModel({
        key: "christianNameSecondary",
        label: t("employee-profile.personal.AliasAlternativeLanguage"),
      }),
      getControlModel({
        key: "displayNameSecondary",
        label: t("employee-profile.personal.DisplayNameAlternativeLanguage"),
        placeholder:
          state?.data?.firstNameSecondary && state?.data?.lastNameSecondary
            ? `${state?.data?.firstNameSecondary}${
                state?.data?.middleNameSecondary
                  ? ` ${state?.data?.middleNameSecondary}`
                  : ""
              } ${state?.data?.lastNameSecondary}`
            : "N/A",
      }),

      getControlModel({
        ...EpPersonalHideAction(
          "dob",
          t("employee-profile.personal.dob"),
          dobReveal,
          "",
          setdobReveal
        ),
      }),
      getControlModel({
        key: "gender",
        label: t("employee-profile.personal.gender"),
        type: "select",
        enum: state.enumData?.gender,
      }),
      getControlModel({
        ...EpPersonalHideAction(
          "maritalStatusString",
          t("employee-profile.personal.marital"),
          martialStatusReveal,
          "",
          setMartialStatusReveal
        ),
      }),
      getControlModel({
        ...EpPersonalHideAction(
          "citizenIdNo",
          t("employee-profile.personal.citizenid"),
          citizenidReveal,
          "",
          setcitizenidReveal
        ),
      }),
      getControlModel({
        key: "nationality",
        label: t("employee-profile.personal.nationality"),
        type: "select",
        enum: state.enumData?.nationality,
      }),
      getControlModel({
        ...EpPersonalHideAction(
          "passportIdNo",
          t("employee-profile.personal.passport"),
          passportidReveal,
          "",
          setpassportidReveal
        ),
      }),
      getControlModel({
        required: state.isPassport,
        key: "countryOfIssue",
        label: t("employee-profile.personal.countryofissue"),
        type: "select",
        enum: state.enumData?.country,
      }),
      getControlModel({
        key: "workVisaHolder",
        label: t("employee-profile.personal.workVisaHolder"),
        type: "toogle",
      }),
      getControlModel({
        key: "workArrivingDate",
        label: t("employee-profile.personal.workArrivingDate"),
        type: "date",
      }),
      getControlModel({
        key: "workVisaExpiryDate",
        label: t("employee-profile.personal.workVisaExpiryDate"),
        type: "date",
      }),
      getControlModel({
        key: "workVisaIssueDate",
        label: t("employee-profile.personal.workVisaIssueDate"),
        type: "date",
      }),
      getControlModel({
        key: "workVisaLandingDate",
        label: t("employee-profile.personal.workVisaLandingDate"),
        type: "date",
      }),
    ],
    layout: {
      rows: [
        {
          columns: [
            {
              config: {
                className: "p-col-6 p-md-6 p-lg-3",
              },
              control: "namePrefix",
            },
          ],
        },
        {
          columns: [
            {
              control: languageToogle
                ? "firstNameSecondary"
                : "firstNamePrimary",
            },

            {
              control: languageToogle
                ? "middleNameSecondary"
                : "middleNamePrimary",
            },
          ],
        },
        {
          columns: [
            {
              control: languageToogle ? "lastNameSecondary" : "lastNamePrimary",
            },
            {
              control: languageToogle
                ? "christianNameSecondary"
                : "christianNamePrimary",
            },
          ],
        },
        {
          columns: [
            {
              control: languageToogle
                ? "displayNameSecondary"
                : "displayNamePrimary",
            },
          ],
        },
        {
          columns: [
            {
              config: {
                className: "p-col-12 p-md-3 p-lg-3",
              },
              control: "dob",
            },
            {
              config: {
                className: "p-col-12 p-md-3 p-lg-3",
              },
              control: "age",
            },
            {
              config: {
                className: "p-col-12 p-md-3 p-lg-3",
              },
              control: "gender",
            },
            {
              config: {
                className: "p-col-12 p-md-3 p-lg-3",
              },
              control: "maritalStatusString",
            },
          ],
        },
        {
          columns: [{ control: "citizenIdNo" }, { control: "nationality" }],
        },
        {
          columns: [{ control: "passportIdNo" }, { control: "countryOfIssue" }],
        },
        {
          columns: [
            {
              config: {
                className: "p-col-12 p-md-3 p-lg-3",
              },
              control: "workVisaHolder",
            },
          ],
        },
        {
          columns: [
            { control: "workArrivingDate" },
            { control: "workVisaExpiryDate" },
            { control: "workVisaIssueDate" },
            { control: "workVisaLandingDate" },
          ],
        },
      ],
    },
  };

  return (
    <>
      <Animated
        animationIn="slideInRight"
        animationOut="slideOutRight"
        animationInDuration={200}
        animationOutDuration={200}
        isVisible={true}
      >
        {state.loading || state.submitting ? (
          <div className="comp-loading">
            <ProgressSpinner />
          </div>
        ) : null}

        <div className="c-group-title ep-language-toogle">
          {t("employee-profile.personalInfo")}
          <ToggleControl
            className="checkbox-control"
            trueValue="Secondary Language"
            falseValue="Primary Language"
            value={languageToogle}
            onChange={(e) => {
              setLanguageToogle(e.value);
            }}
            id={"language-control"}
          />
        </div>
        <div className="c-group-panel">
          <BaseForm
            id="employeeProfile"
            config={PersonalInfoFormConfig}
            readOnly
            form={state.data}
          />
        </div>
      </Animated>
    </>
  );
};

export default PersonalInfo;
