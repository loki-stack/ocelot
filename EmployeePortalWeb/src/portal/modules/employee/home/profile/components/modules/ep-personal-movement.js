import React, { useState, useEffect, useRef } from "react";
import { useTranslation } from "react-i18next";
import { Animated } from "react-animated-css";
import moment from "moment";
import { Button } from "primereact/button";

// services
import { ApiRequest } from "../../../../../../../services/utils";
import { GenericCodeService } from "../../../../../../../services/security";
import {
  EmpDataService,
  JobGradeService,
  CostCenterService,
  WorkingScheduleService,
  // PayrollProfileService,
  OrganizationStructureService,
} from "../../../../../../../services/employee";

//component
import { ProgressSpinner } from "primereact/progressspinner";
import BaseForm from "../../../../../../../components/base-form/base-form";
import { getControlModel } from "../../../../../../../components/base-control/base-cotrol-model";
import {
  GENERIC_CODE,
  GENERIC_CODE_RES,
} from "../../../../../../../constants/index";
import {
  selectStringConvert,
  multiSelectStringConvert,
} from "../modules/ep-profile-constants/ep-personal-hide-action";

import "../../employee-profile.scss";

/** Get data */
const getEmploymentMovement = async (employeeId, ray, enumData, t) => {
  const apiRequestParam = await ApiRequest.createParam();
  var cmd1 = EmpDataService.empDataGetEmploymentContractByEmployeeId({
    ray,
    ...apiRequestParam,
    employeeId: employeeId,
  });

  const [res1] = await Promise.all([cmd1]);
  let employmentContracts = [];

  if (res1 && res1.data) {
    return {
      movement: res1.data?.employmentContracts?.map((x) => {
        employmentContracts.push(x);
        let lastestMovements = x.movements[x.movements?.length - 1];
        return {
          ...x,
          workCalendarString: selectStringConvert(
            enumData?.workingschedule,
            lastestMovements.workCalendar,
            t
          ),
          costCenterString: selectStringConvert(
            enumData?.costcenter,
            lastestMovements.costCenter,
            t
          ),
          jobGrade: lastestMovements ? lastestMovements.jobGrade : t("N/A"),
          // costCenter: lastestMovements ? lastestMovements.costCenter : t("N/A"),
          businessUnitId: lastestMovements
            ? lastestMovements.businessUnitId
            : t("N/A"),
          externalJobTitle: lastestMovements
            ? lastestMovements.externalJobTitle
            : t("N/A"),
          internalJobTitle: lastestMovements
            ? lastestMovements.internalJobTitle
            : t("N/A"),
          directManager: lastestMovements
            ? lastestMovements.directManager
            : t("N/A"),
          assignmentStartDate: lastestMovements
            ? lastestMovements.assignmentStartDate
            : t("DD/MM/YYYY"),
          assignmentEndDate: lastestMovements
            ? lastestMovements.directManager
            : t("DD/MM/YYYY"),
          basicSalaryString: x.basicSalary ? x.basicSalary : t("N/A"),
          notificationRequirements: lastestMovements
            ? lastestMovements?.notificationRequirements
            : t("N/A"),
          movementName: lastestMovements
            ? lastestMovements?.movementName
            : t("N/A"),
          staffTagsString: lastestMovements
            ? multiSelectStringConvert(
                enumData?.stafftags,
                lastestMovements?.staffTags,
                t
              )
            : t("N/A"),
          defaultPaymentCurrencyString: lastestMovements
            ? selectStringConvert(
                enumData?.currency,
                lastestMovements?.defaultPaymentCurrency,
                t
              )
            : t("N/A"),
          defaultSalaryCurrencyString: lastestMovements
            ? selectStringConvert(
                enumData?.currency,
                lastestMovements?.defaultSalaryCurrency,
                t
              )
            : t("N/A"),
          baseSalaryUnitString: lastestMovements
            ? selectStringConvert(
                enumData?.salaryunit,
                lastestMovements?.baseSalaryUnit,
                t
              )
            : t("N/A"),
          remunerationPackageString: lastestMovements
            ? selectStringConvert(
                enumData?.remunerationpackage,
                lastestMovements?.remunerationPackage,
                t
              )
            : t("N/A"),
        };
      }),
      employmentContracts,
    };
  } else {
    return {
      movement: [],
      employmentContracts,
    };
  }
};

const Movement = (props) => {
  const [ray] = useState(ApiRequest.createRay("Movement"));

  const { t } = useTranslation();
  const [state, setState] = useState({
    movement: [],
    employmentContracts: [],
    loading: false,
  });

  const mountedRef = useRef(true);
  useEffect(() => {
    return () => {
      mountedRef.current = false;
    };
  }, []);

  useEffect(() => {
    const loadData = async () => {
      setState({ ...state, loading: true });
      const apiRequestParam = await ApiRequest.createParam();
      let enumData = {};
      const cmd1 = GenericCodeService.genericCodeGetGenericCodes({
        ray,
        ...apiRequestParam,
        parameterGenericCodeList: [
          GENERIC_CODE.TERREASON,
          GENERIC_CODE.EXTERNALJOBTITLE,
          GENERIC_CODE.INTERNALJOBTITLE,
          GENERIC_CODE.CURRENCY,
          GENERIC_CODE.SALARYUNIT,
          GENERIC_CODE.STAFFTAGS,
          GENERIC_CODE.STAFFTYPE,
        ],
        dataResData: GENERIC_CODE_RES.GET_COMPONENT_ITEM,
      });

      const cmd3 = OrganizationStructureService.organizationStructureGetAll({
        ...apiRequestParam,
        ray,
        dataPage: 1,
        dataSize: 10000,
      });
      const cmd4 = JobGradeService.jobGradeGetFilter({
        ...apiRequestParam,
        ray,
        dataPage: 1,
        dataSize: 10000,
      });
      const cmd5 = CostCenterService.costCenterGetFilter({
        ...apiRequestParam,
        ray,
        dataPage: 1,
        dataSize: 10000,
      });
      const cmd6 = WorkingScheduleService.workingScheduleGetAll({
        ...apiRequestParam,
        ray,
        dataPage: 1,
        dataSize: 10000,
      });
      const [
        res1,
        // res2,
        res3,
        res4,
        res5,
        res6,
      ] = await Promise.all([
        cmd1,
        // cmd2,
        cmd3,
        cmd4,
        cmd5,
        cmd6,
      ]);
      if (res1 && res1.data) {
        for (const key in res1.data.genericCodes) {
          if (res1.data.genericCodes.hasOwnProperty(key)) {
            const element = res1.data.genericCodes[key];
            const enumElm = element.map((x) => {
              return {
                label: x.textValue,
                value: x.codeValue,
                data: {
                  ...x,
                },
              };
            });
            enumData[key] = enumElm;
          }
        }
      }
      if (res3 && res3.data) {
        enumData["functionalgroup"] = res3.data.data.map((x) => {
          return {
            label: x.fullName,
            value: x.unitId,
            data: {
              ...x,
            },
          };
        });
      }
      if (res4 && res4.data) {
        enumData["jobgrade"] = res4.data.pagination.content.map((x) => {
          return {
            label: x.name,
            value: x.id,
            data: {
              ...x,
            },
          };
        });
      }
      if (res5 && res5.data) {
        enumData["costcenter"] = res5.data.pagination.content.map((x) => {
          return {
            label: x.name,
            value: x.id,
            data: {
              ...x,
            },
          };
        });
      }
      if (res6 && res6.data) {
        enumData["workingschedule"] = res6.data.map((x) => {
          return {
            label: x.name,
            value: x.id,
            data: {
              ...x,
            },
          };
        });
      }

      if (!mountedRef.current) return null;

      setState({
        enumData,
        loading: false,
        specificEntityId: apiRequestParam.parameterEntityId,
        ...(await getEmploymentMovement(props.employeeId, ray, enumData, t)),
      });
    };
    loadData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.employeeId, props.employmentContractId, ray]);

  const renderItem = () => {
    const tab = [];
    if (!state.movement || state.movement.length === 0) {
      if (props.readOnly) {
        return <div style={{ padding: "1rem" }}>{t("No data")}</div>;
      }
      return null;
    }
    let contractData = state.employmentContracts.map((x) => {
      return {
        label: `${x.referenceId || ""} (${moment(x.contractStartDate).format(
          "DD/MM/YYYY"
        )} - ${moment(x.contractEndDate).format("DD/MM/YYYY")})`,

        value: x.employmentContractId,
        data: {
          ...x,
        },
      };
    });
    state.movement.forEach((data, index) => {
      data.specificEntityId = state.specificEntityId;
      if (data.fixedContract) {
        data.employmentContractId_obj = contractData.find(
          (x) => x.value === data.employmentContractId
        );
      }
      if (data.staffTags && !Array.isArray(data.staffTags)) {
        data.staffTags = data.staffTags.split(",").map((x) => x.trim());
      }

      tab.push(
        <div key={index}>
          <div
            className="c-group-sub-title"
            onClick={() => {
              let _state = { ...state };
              _state[index + "-close"] = !_state[index + "-close"];
              setState(_state);
            }}
          >
            {`${t("employee-profile.movement.PositionRecord")} (${index + 1})`}

            <div className="c-group-sub-tile-action">
              <Button
                type="button"
                icon={
                  state[index + "-close"]
                    ? "pi pi-chevron-down"
                    : "pi pi-chevron-up"
                }
                className="p-button-text"
              />
            </div>
          </div>
          <div
            className={`${
              state[index + "-close"] ? "hide-panel" : ""
            } c-group-panel`}
          >
            <EmployeeMovementItem
              contractData={contractData}
              enumData={state.enumData}
              data={data}
              id={index}
              readOnly={props.readOnly}
            />
          </div>
        </div>
      );
    });
    return tab;
  };

  return (
    <>
      <Animated
        animationIn="slideInRight"
        animationOut="slideOutRight"
        animationInDuration={200}
        animationOutDuration={200}
        isVisible={true}
      >
        {state.loading || state.submitting ? (
          <div className="comp-loading">
            <ProgressSpinner />
          </div>
        ) : null}

        <div className="c-group-title">
          {t("employee-profile.Position Details")}
        </div>
        <div className="c-group-panel">{renderItem()}</div>
      </Animated>
    </>
  );
};

const EmployeeMovementItem = (props) => {
  const { t } = useTranslation();
  // const [costCenterStringReveal, setCostCenterStringReveal] = useState(false);
  // const [workCalendarStringReveal, setWorkCalendarStringReveal] = useState(
  //   false
  // );
  // const [basicSalaryReveal, setBasicSalaryReveal] = useState(false);

  const formConfig = {
    controls: [
      getControlModel({
        key: "internalJobTitle",
        label: t("employee-profile.movement.Internal Job Title"),
        type: "select",
        enum: props.enumData?.internaljobtitle,
      }),
      getControlModel({
        key: "externalJobTitle",
        label: t("employee-profile.movement.External Job Title"),
        type: "select",
        enum: props.enumData?.externaljobtitle,
      }),
      getControlModel({
        key: "jobGrade",
        label: t("employee-profile.movement.Job Grade"),
        type: "select",
        enum: props.enumData?.jobgrade,
      }),
      getControlModel({
        key: "directManager",
        label: t("employee-profile.movement.Direct Manager"),
        type: "select",
        enum: props.enumData?.employee,
      }),
      getControlModel({
        key: "assignmentStartDate",
        label: t("employee-profile.movement.Movement Start Date"),
        type: "date",
      }),
      getControlModel({
        key: "assignmentEndDate",
        label: t("employee-profile.movement.Movement End Date"),
        type: "date",
      }),
      // getControlModel({
      //   key: "employmentContractId",
      //   label: t("employee-profile.movement.Contract"),
      //   type: "select",
      //   enum: props.contractData,
      // }),
      // getControlModel({
      //   key: "notificationRequirements",
      //   label: t("employee-profile.movement.Notice Requirements"),
      // }),
      // getControlModel({
      //   key: "businessUnitId",
      //   label: t("employee-profile.movement.Business Unit"),
      //   type: "select",
      //   enum: props.enumData?.functionalgroup,
      // }),
      // getControlModel({
      //   key: "movementName",
      //   label: t("employee-profile.movement.Movement Name"),
      // }),
      // getControlModel({
      //   key: "costCenter",
      //   label: t("employee-profile.movement.Cost Center"),
      //   type: "select",
      //   enum: props.enumData?.costcenter,
      // }),
      // getControlModel({
      //   key: "workCalendar",
      //   label: t("employee-profile.movement.Work Calendar"),
      //   type: "select",
      //   enum: props.enumData?.workingschedule,
      // }),
      // getControlModel({
      //   ...EpPersonalHideAction(
      //     "costCenterString",
      //     t("employee-profile.movement.Cost Center"),
      //     costCenterStringReveal,
      //     "",
      //     setCostCenterStringReveal
      //   ),
      // }),
      // getControlModel({
      //   ...EpPersonalHideAction(
      //     "workCalendarString",
      //     t("employee-profile.movement.Work Calendar"),
      //     workCalendarStringReveal,
      //     "",
      //     setWorkCalendarStringReveal
      //   ),
      // }),
      // getControlModel({
      //   key: "remunerationPackage",
      //   label: t("employee-profile.movement.Remuneration Package"),
      //   type: "select",
      //   enum: props.enumData?.remunerationpackage,
      //   display: "none",
      // }),
      // getControlModel({
      //   key: "remunerationPackageString",
      //   label: t("employee-profile.movement.Remuneration Package"),
      // }),
      // getControlModel({
      //   key: "basicSalary",
      //   label: t("employee-profile.movement.Basic Salary"),
      //   type: "number",
      // }),
      // getControlModel({
      //   ...EpPersonalHideAction(
      //     "basicSalaryString",
      //     t("employee-profile.movement.Basic Salary"),
      //     basicSalaryReveal,
      //     "",
      //     setBasicSalaryReveal
      //   ),
      // }),
      // getControlModel({
      //   key: "baseSalaryUnit",
      //   label: t("employee-profile.movement.Base Salary Unit"),
      //   type: "select",
      //   enum: props.enumData?.salaryunit,
      // }),
      // getControlModel({
      //   key: "baseSalaryUnitString",
      //   label: t("employee-profile.movement.Base Salary Unit"),
      // }),
      // getControlModel({
      //   key: "defaultSalaryCurrency",
      //   label: t("employee-profile.movement.Default Salary Currency"),
      //   type: "select",
      //   enum: props.enumData?.currency,
      // }),
      // getControlModel({
      //   key: "defaultSalaryCurrencyString",
      //   label: t("employee-profile.movement.Default Salary Currency"),
      // }),
      // getControlModel({
      //   key: "defaultPaymentCurrency",
      //   label: t("employee-profile.movement.Default Payment Currency"),
      //   type: "select",
      //   enum: props.enumData?.currency,
      // }),
      // getControlModel({
      //   key: "defaultPaymentCurrencyString",
      //   label: t("employee-profile.movement.Default Payment Currency"),
      // }),
      // getControlModel({
      //   key: "staffTags",
      //   label: t("employee-profile.movement.Staff Tags"),
      //   type: "multiselect",
      //   enum: props.enumData?.stafftags,
      // }),
      // getControlModel({
      //   key: "staffTagsString",
      //   label: t("employee-profile.movement.Staff Tags"),
      // }),
    ],
    layout: {
      rows: [
        {
          columns: [
            { control: "internalJobTitle" },
            { control: "externalJobTitle" },
          ],
        },
        {
          columns: [{ control: "jobGrade" }, { control: "directManager" }],
        },
        {
          columns: [
            { control: "assignmentStartDate" },
            { control: "assignmentEndDate" },
          ],
        },
      ],
    },
  };
  return (
    <>
      <BaseForm
        id={"Movement" + props.id}
        config={formConfig}
        form={props.data}
        readOnly={props.readOnly}
      />
    </>
  );
};

export default Movement;
