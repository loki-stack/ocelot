//import { getControlModel } from "./../../../../../../../components/base-control/base-cotrol-model";
import { getControlModel } from "./../../../../../../../../components/base-control/base-cotrol-model";

export const ContactInfoFormConfig = (t) => {
  const FormConfig = {
    controls: [
      getControlModel({
        label: t("employee-profile.contact.Home No"),
        key: "homePhone",
        placeholder: "N/A",
      }),
      getControlModel({
        key: "mobilePhone",
        label: t("employee-profile.contact.Mobile No"),
        placeholder: "N/A",
      }),
      getControlModel({
        key: "officePhone",
        label: t("employee-profile.contact.Office No"),
        placeholder: "N/A",
      }),
      getControlModel({
        key: "workEmail",
        label: t("employee-profile.contact.Work E-mail"),
        placeholder: "N/A",
      }),
      getControlModel({
        key: "personalEmail",
        label: t("employee-profile.contact.Personal E-mail"),
        placeholder: "N/A",
      }),
    ],
    layout: {
      rows: [
        {
          columns: [
            {
              control: "homePhone",
            },
            {
              control: "mobilePhone",
            },
          ],
        },
        {
          columns: [
            {
              style: {
                className: "p-col-12 p-md-6 p-lg-6",
              },
              control: "officePhone",
            },
            {},
          ],
        },
        {
          columns: [
            {
              style: {
                className: "p-col-12 p-md-6 p-lg-6",
              },
              control: "workEmail",
            },
            {
              style: {
                className: "p-col-12 p-md-6 p-lg-6",
              },
              control: "personalEmail",
            },
          ],
        },
      ],
    },
  };

  return FormConfig;
};

export const AddressResidentialFormConfig = (
  t,
  state,
  setStateFn,
  languageToogle
) => {
  const FormConfig = {
    controls: [
      getControlModel({
        key: "residentialAddress1Primary",
        label: t("employee-profile.contact.Address line 1"),
      }),
      getControlModel({
        // required: true,,
        // noRequiredLabel: true,
        key: "residentialAddress2Primary",
        label: t("employee-profile.contact.Address line 2"),
      }),
      getControlModel({
        key: "residentialAddress3Primary",
        label: t("employee-profile.contact.Address line 3"),
      }),
      getControlModel({
        key: "residentialDistrictPrimary",
        label: t("employee-profile.contact.District"),
      }),
      getControlModel({
        key: "residentialCityPrimary",
        label: t("employee-profile.contact.City"),
      }),
      getControlModel({
        key: "residentialStatePrimary",
        label: t("employee-profile.contact.State"),
      }),
      getControlModel({
        key: "residentialAddress1Secondary",
        label: t(
          "employee-profile.contact.Address line 1 (Alternative Language)"
        ),
      }),
      getControlModel({
        key: "residentialAddress2Secondary",
        label: t(
          "employee-profile.contact.Address line 2 (Alternative Language)"
        ),
      }),
      getControlModel({
        key: "residentialAddress3Secondary",
        label: t(
          "employee-profile.contact.Address line 3 (Alternative Language)"
        ),
      }),
      getControlModel({
        key: "residentialDistrictSecondary",
        label: t("employee-profile.contact.District (Alternative Language)"),
      }),
      getControlModel({
        key: "residentialCitySecondary",
        label: t("employee-profile.contact.City (Alternative Language)"),
      }),
      getControlModel({
        key: "residentialStateSecondary",
        label: t("employee-profile.contact.State (Alternative Language)"),
      }),
      getControlModel({
        key: "residentialCountry",
        label: t("employee-profile.contact.Country"),
        type: "select",
        enum: state.enumData?.country,
      }),
      getControlModel({
        key: "residentialPostalCode",
        label: t("employee-profile.contact.Postal Code"),
      }),
      getControlModel({
        key: "residentialHongkongTaxForm",
        label: t("employee-profile.contact.Hong Kong District"),
        type: "select",
        enum: state.enumData?.district,
      }),
    ],
    layout: {
      rows: [
        {
          columns: [
            {
              config: {
                style: {
                  className: "p-col-12 p-md-6 p-lg-6",
                },
              },
              control: languageToogle
                ? "residentialAddress1Secondary"
                : "residentialAddress1Primary",
            },
          ],
        },
        {
          columns: [
            {
              config: {
                style: {
                  className: "p-col-12 p-md-6 p-lg-6",
                },
              },
              control: languageToogle
                ? "residentialAddress2Secondary"
                : "residentialAddress2Primary",
            },
          ],
        },
        {
          columns: [
            {
              config: {
                style: {
                  className: "p-col-12 p-md-6 p-lg-6",
                },
              },
              control: languageToogle
                ? "residentialAddress3Secondary"
                : "residentialAddress3Primary",
            },
          ],
        },
        {
          columns: [
            {
              config: {
                style: {
                  className: "p-col-12 p-md-6 p-lg-6",
                },
              },
              control: languageToogle
                ? "residentialDistrictSecondary"
                : "residentialDistrictPrimary",
            },
          ],
        },
        {
          columns: [
            {
              config: {
                style: {
                  className: "p-col-6 p-md-3 p-lg-3",
                },
              },
              control: languageToogle
                ? "residentialCitySecondary"
                : "residentialCityPrimary",
            },
            {
              config: {
                style: {
                  className: "p-col-6 p-md-3 p-lg-3",
                },
              },
              control: languageToogle
                ? "residentialStateSecondary"
                : "residentialStatePrimary",
            },
          ],
        },
        {
          columns: [
            {
              config: {
                style: {
                  className: "p-col-6 p-md-3 p-lg-3",
                },
              },
              control: "residentialCountry",
            },
            {
              config: {
                style: {
                  className: "p-col-6 p-md-3 p-lg-3",
                },
              },
              control: "residentialPostalCode",
            },
          ],
        },
        {
          columns: [
            {
              config: {
                style: {
                  display: state.isChinaResidential ? null : "none",
                  className: "p-col-12 p-md-3 p-lg-3",
                },
              },
              control: "residentialHongkongTaxForm",
            },
            {},
            {},
            {},
          ],
        },
      ],
    },
  };

  return FormConfig;
};

export const AddressPostalFormConfig = (
  t,
  state,
  setStateFn,
  languageToogle
) => {
  const FormConfig = {
    controls: [
      getControlModel({
        key: "postalAddress1Primary",
        label: t("employee-profile.contact.Address line 1"),
      }),
      getControlModel({
        key: "postalAddress2Primary",
        label: t("employee-profile.contact.Address line 2"),
      }),
      getControlModel({
        key: "postalAddress3Primary",
        label: t("employee-profile.contact.Address line 3"),
      }),
      getControlModel({
        key: "postalDistrictPrimary",
        label: t("employee-profile.contact.District"),
      }),
      getControlModel({
        key: "postalCityPrimary",
        label: t("employee-profile.contact.City"),
      }),
      getControlModel({
        key: "postalStatePrimary",
        label: t("employee-profile.contact.State"),
      }),
      getControlModel({
        key: "postalAddress1Secondary",
        label: t(
          "employee-profile.contact.Address line 1 (Alternative Language)"
        ),
      }),
      getControlModel({
        key: "postalAddress2Secondary",
        label: t(
          "employee-profile.contact.Address line 2 (Alternative Language)"
        ),
      }),
      getControlModel({
        key: "postalAddress3Secondary",
        label: t(
          "employee-profile.contact.Address line 3 (Alternative Language)"
        ),
      }),
      getControlModel({
        key: "postalDistrictSecondary",
        label: t("employee-profile.contact.District (Alternative Language)"),
      }),
      getControlModel({
        key: "postalCitySecondary",
        label: t("employee-profile.contact.City (Alternative Language)"),
      }),
      getControlModel({
        key: "postalStateSecondary",
        label: t("employee-profile.contact.State (Alternative Language)"),
      }),
      getControlModel({
        key: "postalCountry",
        label: t("employee-profile.contact.Country"),
        type: "select",
        enum: state.enumData?.country,
      }),
      getControlModel({
        key: "postalPostalCode",
        label: t("employee-profile.contact.Postal Code"),
      }),
      getControlModel({
        key: "postalHongkongTaxForm",
        label: t("employee-profile.contact.Hong Kong District"),
        type: "select",
        enum: state.enumData?.district,
      }),
    ],
    layout: {
      rows: [
        {
          columns: [
            {
              control: languageToogle
                ? "postalAddress2Primary"
                : "postalAddress1Primary",
            },
          ],
        },
        {
          columns: [
            {
              control: languageToogle
                ? "postalAddress2Secondary"
                : "postalAddress2Primary",
            },
          ],
        },
        {
          columns: [
            {
              control: languageToogle
                ? "postalAddress3Secondary"
                : "postalAddress3Primary",
            },
          ],
        },
        {
          columns: [
            {
              control: languageToogle
                ? "postalDistrictSecondary"
                : "postalDistrictPrimary",
            },
          ],
        },
        {
          columns: [
            {
              control: languageToogle
                ? "postalCitySecondary"
                : "postalCityPrimary",
            },
            {
              control: languageToogle
                ? "postalStateSecondary"
                : "postalStatePrimary",
            },
          ],
        },
        {
          columns: [
            {
              control: "postalCountry",
            },
            {
              control: "postalPostalCode",
            },
          ],
        },

        {
          columns: [
            {
              config: {
                style: {
                  display: state.isChinaPostal ? null : "none",
                  className: "p-col-12 p-md-3 p-lg-3",
                },
              },
              control: "postalHongkongTaxForm",
            },
            {},
            {},
            {},
          ],
        },
      ],
    },
  };

  return FormConfig;
};
