import React /* useEffect */ from "react";
import ChangePassword from "./modules/ep-change-password";
import PersonalInfo from "./modules/ep-personal-info";
import Movement from "./modules/ep-personal-movement";
import Contract from "./modules/ep-personal-contract";
import Contact from "./modules/ep-personal-contact";
import Dependants from "./modules/ep-personal-dependants";
import RentalAddress from "./modules/ep-personal-rentalAddress";
import BankAccount from "./modules/ep-personal-bankAccount";
import EmploymentStatus from "./modules/ep-personal-employmentStatus";
import PortalAccess from "./modules/ep-personal-portal-access";

const RightPanel = (props) => {
  switch (props.selectionKeys) {
    case "portalAccess":
      return (
        <PortalAccess
          onStateChange={props.onStateChange}
          employeeId={props.employeeId}
        />
      );
    case "changePassword":
      return (
        <ChangePassword
          onStateChange={props.onStateChange}
          employeeId={props.employeeId}
        />
      );
    case "personalInfo":
      return (
        <PersonalInfo
          onStateChange={props.onStateChange}
          employeeId={props.employeeId}
          data={{
            ...props.basicInfo,
          }}
        />
      );
    case "movement":
      return (
        <Movement
          showing="movement"
          employeeId={props.employeeId}
          data={props.basicInfo}
          readOnly
          hidden={props.hidden}
        />
      );
    case "contract":
      return (
        <Contract
          showing="contract"
          employeeId={props.employeeId}
          data={props.basicInfo}
          readOnly
          hidden={props.hidden}
        />
      );
    case "contact":
      return (
        <Contact
          showing="contact"
          employeeId={props.employeeId}
          data={props.basicInfo}
          emergencyContacts={props.emergencyContacts}
          readOnly={true}
        />
      );
    case "dependants":
      return (
        <Dependants
          showing="dependants"
          employeeId={props.employeeId}
          data={props.basicInfo}
          readOnly={true}
        />
      );
    case "rentalAddress":
      return (
        <RentalAddress
          showing="rentalAddress"
          // ref={props.rightPanelRef}
          employeeId={props.employeeId}
          data={props.basicInfo}
          readOnly={true}
          hidden={props.hidden}
        />
      );
    case "bankAccount":
      return (
        <BankAccount
          showing="bankAccount"
          // ref={props.rightPanelRef}
          employeeId={props.employeeId}
          data={props.basicInfo}
          readOnly={true}
          hidden={props.hidden}
        />
      );
    case "employmentStatus":
      return (
        <EmploymentStatus
          showing="employmentStatus"
          // ref={props.rightPanelRef}
          employeeId={props.employeeId}
          data={props.basicInfo}
          readOnly={true}
          // hidden={props.hidden}
        />
      );

    default:
      return null;
  }
};
export default RightPanel;
