import React, { useState } from "react";
import { useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import { Animated } from "react-animated-css";

// services
// import { EmpDataService } from "./../../../../services/employee";
import { VnAuthenticationService } from "../../../../../../../services/security";
import { ApiRequest } from "../../../../../../../services/utils/index";

//component
import { ProgressSpinner } from "primereact/progressspinner";
import BaseForm from "../../../../../../../components/base-form/base-form";
import { getControlModel } from "../../../../../../../components/base-control/base-cotrol-model";
// import { Card } from "primereact/card";

import "../../employee-profile.scss";

/**
 * Employee landing page after login
 */
const ChangePassword = (props) => {
  const [ray] = useState(ApiRequest.createRay("ChangePassword"));
  const userName = useSelector((state) => state.auth.data?.user?.username);
  const userId = useSelector((state) => state.auth.data?.user?.userId);

  const { t } = useTranslation();
  const [state] = useState({
    loading: false,
    form: {
      currentPwd: "",
      newPwd: "",
      confirmPwd: "",
    },
  });

  //function
  const formSubmitFn = async (payload) => {
    const { form } = payload;
    let res = {};
    res = await VnAuthenticationService.authenticationEssChangePassword({
      body: {
        ray,
        data: {
          currentPassword: form.currentPwd,
          username: userName,
          userId: userId,
          newPassword: form.newPwd,
          confirmPassword: form.confirmPwd,
        },
      },
    });

    return res;
  };

  //form layout
  // console.log("ChangePassword: props, ref", props, ref);
  const changePasswordFormConfig = {
    controls: [
      getControlModel({
        required: true,
        key: "currentPwd",
        label: t("employee-profile.ChangePassword.currentPassword"),
        type: "password",
        placeholder: t("employee-profile.ChangePassword.currentPassword"),
        config: {
          feedback: false, // we don't need to show strength during login
        },
        ruleList: [
          {
            name: "required",
            error: t("employee-profile.ChangePassword.error.required"),
          },
        ],
      }),
      getControlModel({
        key: "newPwd",
        label: t("employee-profile.ChangePassword.newPassword"),
        type: "password",
        placeholder: t("employee-profile.ChangePassword.newPassword"),
        required: true,
        minLength: 8,
        showPasswordHint: true,
      }),
      getControlModel({
        key: "confirmPwd",
        label: t("employee-profile.ChangePassword.verifyPassword"),
        type: "password",
        placeholder: t("employee-profile.ChangePassword.verifyPassword"),
        required: true,
        config: {
          feedback: false, // we don't need to show strength during login
        },
      }),
    ],
    layout: {
      rows: [
        {
          columns: [
            {
              control: "currentPwd",
            },
          ],
        },
        {
          columns: [
            {
              control: "newPwd",
            },
          ],
        },
        {
          columns: [
            {
              control: "confirmPwd",
            },
          ],
        },
        {
          config: {
            style: {
              marginBottom: "1rem",
              marginTop: "1rem",
            },
          },
          columns: [
            {
              action: {
                type: "submit",
                label: t("Next"),
                submitingLabel: t("Submitting"),
                config: {
                  kind: "primary",
                  style: {
                    width: "100%",
                    maxWidth: "100%",
                    textAlign: "center",
                  },
                },
              },
            },
          ],
        },
      ],
    },
    formSubmitFn,
    // afterFormSubmitFn,
  };

  return (
    <>
      <Animated
        animationIn="slideInRight"
        animationOut="slideOutRight"
        animationInDuration={200}
        animationOutDuration={200}
        isVisible={true}
      >
        {state.loading || state.submitting ? (
          <div className="comp-loading">
            <ProgressSpinner />
          </div>
        ) : null}

        <div className="c-group-title">
          {t("employee-profile.ChangePassword.title")}
        </div>
        <div className="c-group-panel">
          <div className="p-field p-grid">
            <div className="p-col-12 p-md-6 p-lg-6">
              <BaseForm
                id="changePassword"
                config={changePasswordFormConfig}
                form={state.companyInfoForm}
              />
            </div>
            {/* <div className="p-md-6 p-lg-6" style={{display: "none"}}>
              <Card title={t("employee-profile.ChangePassword.tips.title")}>
                <h2>{t("employee-profile.ChangePassword.tips.header")}</h2>
                <p>{t("employee-profile.ChangePassword.tips.paragraph1")}</p>
                <br />
                <p>{t("employee-profile.ChangePassword.tips.paragraph2")}</p>
                <li>{t("employee-profile.ChangePassword.tips.point1")}</li>
                <li>{t("employee-profile.ChangePassword.tips.point2")}</li>
                <li>{t("employee-profile.ChangePassword.tips.point3")}</li>
              </Card>
            </div> */}
          </div>
        </div>
      </Animated>
    </>
  );
};

export default ChangePassword;
