import React, { useState, useEffect, useRef } from "react";
import { useTranslation } from "react-i18next";
import { Animated } from "react-animated-css";
// import { Calendar } from "primereact/calendar";
import { ProgressSpinner } from "primereact/progressspinner";
import { VnUserService } from "../../../../../../../services/security";
import { ApiRequest } from "../../../../../../../services/utils";
import { getControlModel } from "./../../../../../../../components/base-control/base-cotrol-model";
import BaseForm from "../../../../../../../components/base-form/base-form";
import ToggleControl from "../../../../../../../components/base-control/toggle-control/toggle-control";

const PortalAccess = (props) => {
  const [ray] = useState(ApiRequest.createRay("PortalAccess"));
  const { t } = useTranslation();
  const [state, setState] = useState({
    loading: true,
    reveal: false,
  });
  const mountedRef = useRef(true);
  useEffect(() => {
    return () => {
      mountedRef.current = false;
    };
  }, []);

  useEffect(() => {
    const loadData = async () => {
      setState({ ...state, loading: true });
      const apiRequestParam = await ApiRequest.createParam();
      let enumData = {};

      const cmd1 = VnUserService.usersGetUserByEmployeeId({
        employeeId: props.employeeId,
        ...apiRequestParam,
      });

      const [res1] = await Promise.all([cmd1]);
      if (res1 && res1.data) {
        setState({
          ...state,
          enumData,
          data: {
            username: res1.data.username,
            singleSignOnId: res1.data.singleSignOnId,
          },
          loading: false,
        });
      }
    };
    loadData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ray, props.employeeId, props.data]);

  const PortalAccessFormConfig = {
    controls: [
      getControlModel({
        key: "username",
        label: t("employee-profile.portalAcess.Username"),
      }),
      getControlModel({
        key: "singleSignOnId",
        label: t("employee-profile.portalAcess.Single Sign On Id"),
      }),
    ],
    layout: {
      rows: [
        {
          columns: [
            {
              component: () => (
                <div className="c-group-sub-title inline-tile">
                  {t("employee-profile.Sign On Setting")}
                </div>
              ),
            },
          ],
        },
        {
          columns: [{ control: "username" }, { control: "singleSignOnId" }],
        },
      ],
    },
  };

  return (
    <>
      <Animated
        animationIn="slideInRight"
        animationOut="slideOutRight"
        animationInDuration={200}
        animationOutDuration={200}
        isVisible={true}
      >
        {state.loading || state.submitting ? (
          <div className="comp-loading">
            <ProgressSpinner />
          </div>
        ) : null}

        <div className="c-group-title ep-language-toogle">
          {t("Portal Access Info")}
        </div>
        <div className="c-group-panel">
          <BaseForm
            id="employeePortalAccess"
            config={PortalAccessFormConfig}
            readOnly
            form={state.data}
          />
        </div>
      </Animated>
    </>
  );
};

export default PortalAccess;
