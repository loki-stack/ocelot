import React, { useState, useEffect, useRef } from "react";
// import { useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import { Animated } from "react-animated-css";

// services
// import { EmpDataService } from "./../../../../services/employee";
// import { VnAuthenticationService } from "../../../../../../../services/security";
import { ApiRequest } from "../../../../../../../services/utils/index";
import { GenericCodeService } from "../../../../../../../services/security";
import { EmpDataService } from "../../../../../../../services/employee";

//component
import { Button } from "primereact/button";
import { ProgressSpinner } from "primereact/progressspinner";
import BaseForm from "../../../../../../../components/base-form/base-form";
import ToggleControl from "../../../../../../../components/base-control/toggle-control/toggle-control";
import { getControlModel } from "../../../../../../../components/base-control/base-cotrol-model";

//const
import {
  GENERIC_CODE,
  GENERIC_CODE_RES,
} from "../../../../../../../constants/index";
import {
  EpPersonalHideAction,
  momentDateConvert,
  // selectStringConvert,
} from "../modules/ep-profile-constants/ep-personal-hide-action";

import "../../employee-profile.scss";

const Depandants = (props) => {
  const [ray] = useState(ApiRequest.createRay("Depandants"));

  const { t } = useTranslation();
  const [state, setState] = useState({
    dependentItem: [],
    loading: true,
  });
  const [languageToogle, setLanguageToogle] = useState(false);
  const mountedRef = useRef(true);

  const getDependant = async (employeeId, ray, t) => {
    const apiRequestParam = await ApiRequest.createParam();
    var cmd1 = EmpDataService.empDataGetDependentByEmployeeId({
      ray,
      ...apiRequestParam,
      employeeId: employeeId,
    });

    const [res1] = await Promise.all([cmd1]);
    if (res1 && res1.data) {
      return {
        datas: res1.data.dependents?.map((x) => {
          return {
            ...x,
            dob: momentDateConvert(x.dateOfBirth, t),
          };
        }),
      };
    }
  };

  useEffect(() => {
    return () => {
      mountedRef.current = false;
    };
  }, []);
  /** Init data */
  useEffect(() => {
    const loadData = async () => {
      const apiRequestParam = await ApiRequest.createParam();
      let enumData = {};
      const cmd1 = GenericCodeService.genericCodeGetGenericCodes({
        ray,
        ...apiRequestParam,
        parameterGenericCodeList: [
          GENERIC_CODE.NAMEPREFIX,
          GENERIC_CODE.GENDER,
          GENERIC_CODE.NATIONALITY,
          GENERIC_CODE.COUNTRY,
          GENERIC_CODE.RELATIONSHIP,
        ],
        dataResData: GENERIC_CODE_RES.GET_COMPONENT_ITEM,
      });
      const [res1] = await Promise.all([cmd1]);
      if (res1 && res1.data) {
        for (const key in res1.data.genericCodes) {
          if (res1.data.genericCodes.hasOwnProperty(key)) {
            const element = res1.data.genericCodes[key];
            const enumElm = element.map((x) => {
              return {
                label: x.textValue,
                value: x.codeValue,
                data: {
                  ...x,
                },
              };
            });
            enumData[key] = enumElm;
          }
        }
      }
      if (!mountedRef.current) return null;
      setState({
        enumData,
        loading: false,
        dependentItem: await getDependant(props.employeeId, ray, t),
      });
    };
    loadData();
  }, [props.employeeId, ray, t]);

  const renderItem = () => {
    const tab = [];
    if (
      !state.dependentItem?.datas ||
      state.dependentItem.datas?.length === 0
    ) {
      if (props.readOnly) {
        return <div style={{ padding: "1rem" }}>{t("No data")}</div>;
      }
      return null;
    }

    state.dependentItem?.datas?.forEach((data, index) => {
      tab.push(
        <div key={index}>
          <div
            className="c-group-sub-title"
            onClick={() => {
              let _state = { ...state };
              _state[index + "-close"] = !_state[index + "-close"];
              setState(_state);
            }}
          >
            {`${t("employee-profile.dependants.Dependants")} (${index + 1})`}

            <div className="c-group-sub-tile-action">
              <Button
                type="button"
                icon={
                  state[index + "-close"]
                    ? "pi pi-chevron-down"
                    : "pi pi-chevron-up"
                }
                className="p-button-text"
              />
            </div>
          </div>
          <div
            className={`${
              state[index + "-close"] ? "hide-panel" : ""
            } c-group-panel`}
          >
            <EmployeeDependentItem
              enumData={state.enumData}
              data={data}
              id={index}
              readOnly
              languageToogle={languageToogle}
            />
          </div>
        </div>
      );
    });
    return tab;
  };

  return (
    <>
      <Animated
        animationIn="slideInRight"
        animationOut="slideOutRight"
        animationInDuration={200}
        animationOutDuration={200}
        isVisible={true}
      >
        {state.loading || state.submitting ? (
          <div className="comp-loading">
            <ProgressSpinner />
          </div>
        ) : null}

        <div className="c-group-title ep-language-toogle">
          <p>{t("employee-profile.dependants.title")}</p>{" "}
          <ToggleControl
            className="checkbox-control"
            trueValue="Secondary Language"
            falseValue="Primary Language"
            value={languageToogle}
            onChange={(e) => {
              setLanguageToogle(e.value);
            }}
            id={"language-control"}
          />
        </div>
        <div className="c-group-panel c-group-panel-no-padding">
          {renderItem()}
        </div>
      </Animated>
    </>
  );
};

const EmployeeDependentItem = (props) => {
  const { t } = useTranslation();
  const [state, setState] = useState({
    isOther: props.data?.relationshipToEmployee === "OT",
  });
  useEffect(() => {
    setState({
      isOther: props.data?.relationshipToEmployee === "OT",
    });
  }, [props.data.relationshipToEmployee]);

  const [citizenidReveal, setcitizenidReveal] = useState(false);
  const [passportIdReveal, setpassportIdReveal] = useState(false);
  const [dobReveal, setdobReveal] = useState(false);

  const formConfig = {
    controls: [
      getControlModel({
        key: "relationshipToEmployee",
        label: t("employee-profile.dependants.Relationship"),
        type: "select",
        enum: props.enumData?.relationship,
      }),
      getControlModel({
        key: "relationshipOther",
        label: t("employee-profile.dependants.relationshipOther"),
      }),
      getControlModel({
        key: "namePrefix",
        label: t("employee-profile.personal.namePrefix"),
        type: "select",
        enum: props.enumData?.nameprefix,
      }),
      getControlModel({
        key: "firstNamePrimary",
        label: t("employee-profile.personal.firstName"),
      }),
      getControlModel({
        key: "middleNamePrimary",
        label: t("employee-profile.personal.middleName"),
      }),
      getControlModel({
        key: "lastNamePrimary",
        label: t("employee-profile.personal.lastName"),
      }),
      getControlModel({
        key: "christianNamePrimary",
        label: t("employee-profile.dependants.christianNamePrimary"),
      }),

      getControlModel({
        key: "firstNameSecondary",
        label: t("employee-profile.dependants.firstNameSecondary"),
      }),
      getControlModel({
        key: "middleNameSecondary",
        label: t("employee-profile.dependants.middleNameSecondary"),
      }),
      getControlModel({
        key: "lastNameSecondary",
        label: t("employee-profile.dependants.lastNameSecondary"),
      }),
      getControlModel({
        key: "christianNameSecondary",
        label: t("employee-profile.dependants.christianNameSecondary"),
      }),
      getControlModel({
        ...EpPersonalHideAction(
          "dob",
          t("employee-profile.dependants.dateOfBirth"),
          dobReveal,
          "",
          setdobReveal
        ),
      }),
      getControlModel({
        key: "gender",
        label: t("employee-profile.dependants.gender"),
        type: "select",
        enum: props.enumData?.gender,
      }),
      getControlModel({
        ...EpPersonalHideAction(
          "citizenIdNo",
          t("employee-profile.dependants.citizenIdNo"),
          citizenidReveal,
          "",
          setcitizenidReveal
        ),
      }),
      getControlModel({
        key: "nationality",
        type: "select",
        enum: props.enumData?.nationality,
        label: t("employee-profile.dependants.nationality"),
      }),
      getControlModel({
        ...EpPersonalHideAction(
          "passportIdNo",
          t("employee-profile.dependants.passportIdNo"),
          passportIdReveal,
          "",
          setpassportIdReveal
        ),
      }),
      getControlModel({
        key: "countryOfIssue",
        type: "select",
        enum: props.enumData?.country,
        label: t("employee-profile.dependants.countryOfIssue"),
      }),
      getControlModel({
        key: "workVisaHolder",
        label: t("employee-profile.dependants.workVisaHolder"),
        type: "toogle",
      }),
      getControlModel({
        key: "address",
        label: t("employee-profile.dependants.address"),
      }),
    ],
    layout: {
      rows: [
        {
          columns: [
            {
              config: {
                className: "p-col-12 p-md-6 p-lg-6 ",
              },
              control: "relationshipToEmployee",
            },
            {
              config: {
                className: "p-col-12 p-md-6 p-lg-6 ",
                style: {
                  display: state.isOther ? null : "none",
                },
              },
              control: "relationshipOther",
            },
          ],
        },
        {
          columns: [
            {
              config: {
                className: "p-col-12 p-md-6 p-lg-6 ",
              },
              control: "namePrefix",
            },
          ],
        },
        {
          columns: [
            {
              control: props.languageToogle
                ? "firstNameSecondary"
                : "firstNamePrimary",
            },
            {
              control: props.languageToogle
                ? "middleNameSecondary"
                : "middleNamePrimary",
            },
          ],
        },
        {
          columns: [
            {
              control: props.languageToogle
                ? "lastNameSecondary"
                : "lastNamePrimary",
            },
            {
              control: props.languageToogle
                ? "christianNameSecondary"
                : "christianNamePrimary",
            },
          ],
        },
        {
          columns: [
            {
              config: {
                className: "p-col-12 p-md-6 p-lg-6 ",
              },
              control: "dob",
            },
            {
              config: {
                className: "p-col-12 p-md-6 p-lg-6 ",
              },
              control: "gender",
            },
          ],
        },
        {
          columns: [{ control: "citizenIdNo" }, { control: "nationality" }],
        },
        {
          columns: [
            { control: "passportIdNo" },
            {
              config: {
                className: "p-col-12 p-md-6 p-lg-3 ",
              },
              control: "countryOfIssue",
            },
            {
              config: {
                className: "p-col-12 p-md-6 p-lg-3 ",
              },
              control: "workVisaHolder",
            },
          ],
        },
        {
          columns: [
            {
              component: () => (
                <h4 className="sub-title-group">
                  {t("employee-profile.dependants.Address(Residential)")}
                </h4>
              ),
            },
          ],
        },
        {
          columns: [{ control: "address" }],
        },
      ],
    },
  };
  return (
    <>
      <BaseForm
        id={"employeeDependentItem" + props.id}
        config={formConfig}
        form={props.data}
        readOnly
      />
    </>
  );
};

export default Depandants;
