/**
 * url
 */
export enum UrlLink {
  BaseURL = "home",
  DefaultLink = "personal-info",
  PersonalInfo = "personal-info",
  ChangePassword = "change-password",
}
/**
 * tabNavTitle
 */
export enum tabNavTitle {
  DefaultTitle = "Personal Info",
  PersonalInfo = "Personal Info",
  ChangePassword = "Change Password",
}
