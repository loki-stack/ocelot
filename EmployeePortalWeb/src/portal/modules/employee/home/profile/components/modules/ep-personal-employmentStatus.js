import React, { useState, useEffect, useRef } from "react";
import { ApiRequest } from "../../../../../../../services/utils";
import { Animated } from "react-animated-css";
import { useTranslation } from "react-i18next";
import moment from "moment";
import { Button } from "primereact/button";

import { GenericCodeService } from "../../../../../../../services/security";
import { EmpDataService } from "../../../../../../../services/employee";

import { ProgressSpinner } from "primereact/progressspinner";
import BaseForm from "../../../../../../../components/base-form/base-form";
import { getControlModel } from "../../../../../../../components/base-control/base-cotrol-model";
import {
  GENERIC_CODE,
  GENERIC_CODE_RES,
} from "../../../../../../../constants/index";
import { selectStringConvert } from "../modules/ep-profile-constants/ep-personal-hide-action";

const getEmploymentStatus = async (employeeId, ray, enumData, t) => {
  const apiRequestParam = await ApiRequest.createParam();
  var cmd1 = EmpDataService.empDataGetEmploymentStatusByEmployeeId({
    ray,
    ...apiRequestParam,
    employeeId: employeeId,
  });

  const [res1] = await Promise.all([cmd1]);

  if (res1 && res1.data) {
    let employmentStatus = [];
    res1.data?.data?.map((x) => {
      x?.employmenStatuses?.map((y) => {
        employmentStatus = employmentStatus.concat({
          ...y,
          confirmed: y.confirmed ? t("Yes") : t("No"),
          status_name: selectStringConvert(enumData?.eestatus, y?.status, t),
        });
        return {};
      });
      return {};
    });

    return {
      employmentStatus: employmentStatus,
      remarkString: res1.data?.remark,
    };
  } else {
    return {
      employmentStatus: [],
      remarkString: res1.data?.remark,
    };
  }
};

const EmploymentStatus = (props) => {
  const [ray] = useState(ApiRequest.createRay("EmploymentStatus"));
  const { t } = useTranslation();

  const [state, setState] = useState({
    employmentStatus: [],
    loading: true,
  });

  const mountedRef = useRef(true);
  useEffect(() => {
    return () => {
      mountedRef.current = false;
    };
  }, []);

  useEffect(() => {
    const loadData = async () => {
      setState({ ...state, loading: true });
      const apiRequestParam = await ApiRequest.createParam();
      let enumData = {};
      const cmd1 = GenericCodeService.genericCodeGetGenericCodes({
        ray,
        ...apiRequestParam,
        parameterGenericCodeList: [GENERIC_CODE.EESTATUS],
        dataResData: GENERIC_CODE_RES.GET_COMPONENT_ITEM,
      });

      let cmd3 = EmpDataService.empDataGetEmploymentContractByEmployeeId({
        ray,
        ...apiRequestParam,
        employeeId: props.employeeId,
      });
      const [res1, res3] = await Promise.all([cmd1, cmd3]);
      if (res1 && res1.data) {
        for (const key in res1.data.genericCodes) {
          if (res1.data.genericCodes.hasOwnProperty(key)) {
            const element = res1.data.genericCodes[key];
            const enumElm = element.map((x) => {
              return {
                label: x.textValue,
                value: x.codeValue,
                data: {
                  ...x,
                },
              };
            });
            enumData[key] = enumElm;
          }
        }
      }

      if (res3 && res3.data) {
        let employmentContracts = [];
        res3.data.employmentContracts.forEach((contract) => {
          let startDate = contract.contractStartDate
            ? moment(contract.contractStartDate).format("DD/MM/YYYY")
            : t("N/A");
          let endDate = contract.contractEndDate
            ? moment(contract.contractEndDate).format("DD/MM/YYYY")
            : t("N/A");
          employmentContracts.push({
            label: `${contract.referenceId || ""} (${startDate} - ${endDate})`,

            value: contract.employmentContractId,
            data: {
              ...contract,
            },
          });
        });
        enumData["contract"] = employmentContracts;
      }

      if (!mountedRef.current) return null;
      setState({
        enumData,
        loading: false,
        ...(await getEmploymentStatus(props.employeeId, ray, enumData, t)),
      });
    };
    loadData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.employeeId, ray]);

  const renderItem = () => {
    const tab = [];
    if (!state.employmentStatus || state.employmentStatus.length === 0) {
      if (props.readOnly) {
        return <div style={{ padding: "1rem" }}>{t("No data")}</div>;
      }
      return null;
    }
    let startDate;
    state.employmentStatus.forEach((data, index) => {
      let newTitle = startDate !== data?.startDate || data?.new;
      startDate = data?.startDate;
      if (newTitle) {
        tab.push(
          <div key={index + "12"}>
            <div
              className="c-group-sub-title"
              onClick={() => {
                let _state = { ...state };
                _state[data.startDate + "-close"] = !_state[
                  data.startDate + "-close"
                ];
                setState(_state);
              }}
            >
              {moment(data.startDate).format("MMM DD,YYYY")}
              <div className="c-group-sub-tile-action">
                <Button
                  type="button"
                  icon={
                    state[data.startDate + "-close"]
                      ? "pi pi-chevron-down"
                      : "pi pi-chevron-up"
                  }
                  className="p-button-text"
                />
              </div>
            </div>
          </div>
        );
      }
      tab.push(
        <div key={index}>
          <div
            className={`${
              state[startDate + "-close"] ? "hide-panel" : ""
            } c-group-panel`}
          >
            {newTitle ? null : (
              <hr
                style={{ position: "relative", top: " -2rem" }}
                className="c-seperate"
              ></hr>
            )}
            <EmployeeEmploymentStatusItem
              enumData={state.enumData}
              data={data}
              id={index}
              readOnly={props.readOnly}
            />
          </div>
        </div>
      );
    });
    return tab;
  };

  return (
    <>
      <Animated
        animationIn="slideInRight"
        animationOut="slideOutRight"
        animationInDuration={200}
        animationOutDuration={200}
        isVisible={true}
      >
        {state.loading || state.submitting ? (
          <div className="comp-loading">
            <ProgressSpinner />
          </div>
        ) : null}

        <div className="c-group-title">{t("employee-profile.employment")}</div>
        <div className="c-group-panel">{renderItem()}</div>
      </Animated>
    </>
  );
};

const EmployeeEmploymentStatusItem = (props) => {
  const { t } = useTranslation();

  const formConfig = {
    controls: [
      getControlModel({
        key: "employmentContractId",
        label: t("employee-profile.status.Contract"),
        type: "select",
        enum: props.enumData?.contract,
      }),
      getControlModel({
        key: "status",
        label: t("employee-profile.status.Status"),
        type: "select",
        enum: props.enumData?.eestatus,
      }),
      getControlModel({
        key: "confirmed",
        label: t("employee-profile.status.Confirmed"),
        type: "toogle",
      }),
    ],
    layout: {
      rows: [
        {
          columns: [
            {
              config: {
                className: "p-col-12 p-md-6 p-lg-6",
              },
              control: "employmentContractId",
            },
            {
              config: {
                className: "p-col-12 p-md-6 p-lg-3 ",
              },
              control: "status",
            },
            {
              control: "confirmed",
            },
          ],
        },
      ],
    },
  };
  return (
    <>
      {!props.readOnly ? (
        <BaseForm
          id={"employeeEmploymentStatusItem-" + props.id}
          config={formConfig}
          form={props.data}
          readOnly={props.readOnly}
        />
      ) : (
        <>
          <div className="ep-emp-status-item p-grid">
            <div className="ep-item-contract p-col-12 p-md-6 p-lg-1">
              <strong>{t("employmentStatus.employmentContract")}:</strong>
              <span style={{ marginLeft: "1rem" }}>
                {props.data?.employmentContractName || t("N/A")}
              </span>
            </div>
            <div className="ep-item-status p-col-12 p-md-6 p-lg-1">
              <strong>{props.data?.status_name || t("N/A")}</strong>
            </div>
          </div>
          <div className="ep-emp-status-item">
            <div className="c-item-remark">
              <strong>{t("employmentStatus.confirmed")}:</strong>
              <span style={{ marginLeft: "6.4rem" }}>
                {props.data?.confirmed ? t("Yes") : t("N/A")}
              </span>
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default EmploymentStatus;
