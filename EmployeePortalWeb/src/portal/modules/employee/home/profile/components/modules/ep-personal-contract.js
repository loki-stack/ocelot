import React, { useEffect, useState, useRef } from "react";
// import { useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import { Animated } from "react-animated-css";
import "../../employee-profile.scss";

//component
import { ProgressSpinner } from "primereact/progressspinner";
import BaseForm from "../../../../../../../components/base-form/base-form";
import { getControlModel } from "./../../../../../../../components/base-control/base-cotrol-model";
import { Button } from "primereact/button";

// services
import { ApiRequest } from "../../../../../../../services/utils";
import { GenericCodeService } from "../../../../../../../services/security";
import { EmpDataService } from "../../../../../../../services/employee";
import {
  GENERIC_CODE,
  GENERIC_CODE_RES,
} from "../../../../../../../constants/index";

/** Get data */
const getEmploymentContract = async (employeeId, ray, enumData, t) => {
  const apiRequestParam = await ApiRequest.createParam();
  var cmd1 = EmpDataService.empDataGetEmploymentContractByEmployeeId({
    ray,
    ...apiRequestParam,
    employeeId: employeeId,
  });

  const [res1] = await Promise.all([cmd1]);

  if (res1 && res1.data) {
    return {
      datas: res1.data?.employmentContracts?.map((x) => {
        return {
          ...x,
        };
      }),
    };
  } else {
    return {
      datas: [],
    };
  }
};

const Contract = (props) => {
  const [ray] = useState(ApiRequest.createRay("Contract"));
  const { t } = useTranslation();
  const [state, setState] = useState({
    loading: false,
  });
  const mountedRef = useRef(true);
  useEffect(() => {
    return () => {
      mountedRef.current = false;
    };
  }, []);

  useEffect(() => {
    const loadData = async () => {
      setState({ ...state, loading: true });
      const apiRequestParam = await ApiRequest.createParam();
      let enumData = {};
      const cmd1 = GenericCodeService.genericCodeGetGenericCodes({
        ray,
        ...apiRequestParam,
        parameterGenericCodeList: [
          GENERIC_CODE.TERREASON,
          GENERIC_CODE.CURRENCY,
          GENERIC_CODE.SALARYUNIT,
          GENERIC_CODE.STAFFTAGS,
          GENERIC_CODE.REMU_PKG,
          GENERIC_CODE.STAFFTYPE,
        ],
        dataResData: GENERIC_CODE_RES.GET_COMPONENT_ITEM,
      });

      const [res1] = await Promise.all([cmd1]);
      if (res1 && res1.data) {
        for (const key in res1.data.genericCodes) {
          if (res1.data.genericCodes.hasOwnProperty(key)) {
            const element = res1.data.genericCodes[key];
            const enumElm = element.map((x) => {
              return {
                label: x.textValue,
                value: x.codeValue,
                data: {
                  ...x,
                },
              };
            });
            enumData[key] = enumElm;
          }
        }
      }

      if (!mountedRef.current) return null;

      setState({
        enumData,
        loading: false,
        employmentContracts: await getEmploymentContract(
          props.employeeId,
          ray,
          enumData,
          t
        ),
      });
    };
    loadData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.employeeId, props.employmentContractId, ray]);

  const renderItem = () => {
    const tab = [];
    if (
      !state.employmentContracts?.datas ||
      state.employmentContracts?.datas.length === 0
    ) {
      if (props.readOnly) {
        return <div style={{ padding: "1rem" }}>{t("No data")}</div>;
      }
      return null;
    }
    let staffId;
    state.employmentContracts?.datas?.forEach((data, index) => {
      if (index === 0) {
        staffId = data.staffId;
      }
      data.staffId = staffId;
      tab.push(
        <div key={index}>
          <div
            className="c-group-sub-title"
            onClick={() => {
              let _state = { ...state };
              _state[index + "-closeContract"] = !_state[
                index + "-closeContract"
              ];
              setState(_state);
            }}
          >
            {`${t("employee-profile.Contract.Contract")} (${index + 1})`}

            <div className="c-group-sub-tile-action">
              <Button
                type="button"
                icon={
                  state[index + "-closeContract"]
                    ? "pi pi-chevron-down"
                    : "pi pi-chevron-up"
                }
                className="p-button-text"
              />
            </div>
          </div>
          <div
            className={`${
              state[index + "-closeContract"] ? "hide-panel" : ""
            } c-group-panel`}
          >
            <ContractItem
              enumData={state.enumData}
              data={data}
              index={index}
              id={index}
              readOnly={props.readOnly}
            />
          </div>
        </div>
      );
    });
    return tab;
  };

  return (
    <>
      <Animated
        animationIn="slideInRight"
        animationOut="slideOutRight"
        animationInDuration={200}
        animationOutDuration={200}
        isVisible={true}
      >
        {state.loading || state.submitting ? (
          <div className="comp-loading">
            <ProgressSpinner />
          </div>
        ) : null}

        <div className="c-group-title">{t("employee-profile.contract")}</div>
        <div className="c-group-panel">{renderItem()}</div>
      </Animated>
    </>
  );
};

const ContractItem = (props) => {
  const { t } = useTranslation();
  const formConfig = {
    controls: [
      getControlModel({
        key: "employeeNo",
        label: t("employee-profile.Contract.EmployeeNo"),
      }),
      getControlModel({
        key: "staffType",
        label: t("Staff Type"),
        type: "select",
        enum: props.enumData?.stafftype,
      }),
      getControlModel({
        key: "contractStartDate",
        label: t("Contract Start Date"),
        type: "date",
      }),
      getControlModel({
        key: "contractEndDate",
        label: t("Contract End Date"),
        type: "date",
      }),
    ],
    layout: {
      rows: [
        {
          columns: [{ control: "employeeNo" }, { control: "staffType" }],
        },
        {
          columns: [
            { control: "contractStartDate" },
            { control: "contractEndDate" },
          ],
        },
      ],
    },
  };
  return (
    <>
      <BaseForm
        id={"Contract" + props.id}
        config={formConfig}
        form={props.data}
        readOnly={props.readOnly}
      />
    </>
  );
};

export default Contract;
