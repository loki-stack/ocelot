import { getControlModel } from "./../../../../../../components/base-control/base-cotrol-model";
import { useTranslation } from "react-i18next";
function EmployeeProfileFormConfig() {
  const { t } = useTranslation();
  const FormConfig = {
    controls: [
      getControlModel({
        key: "role",
        label: t("employee-profile.Role"),
      }),
      getControlModel({
        key: "division",
        label: t("employee-profile.Division"),
      }),
      getControlModel({
        key: "reportTo",
        label: t("employee-profile.ReportTo"),
      }),
      getControlModel({
        key: "staffId",
        label: t("employee-profile.EmployeeNo"),
      }),
      getControlModel({
        key: "mobileNo",
        label: t("employee-profile.MobileNo"),
      }),
      getControlModel({
        key: "email",
        label: t("employee-profile.E-mail"),
      }),
    ],
    layout: {
      rows: [
        {
          columns: [
            { control: "role" },
            { control: "division" },
            { control: "reportTo" },
          ],
        },
        {
          columns: [
            { control: "staffId" },
            { control: "mobileNo" },
            { control: "email" },
          ],
        },
      ],
    },
  };

  return FormConfig;
}

export default EmployeeProfileFormConfig;
