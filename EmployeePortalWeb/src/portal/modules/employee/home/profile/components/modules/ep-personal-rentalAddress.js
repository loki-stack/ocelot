import React, { useState, useEffect, useRef } from "react";
import { useTranslation } from "react-i18next";
import { ApiRequest } from "../../../../../../../services/utils";
import { Animated } from "react-animated-css";
import moment from "moment";
import { Button } from "primereact/button";

import { EmpDataService } from "../../../../../../../services/employee";
import { GenericCodeService } from "../../../../../../../services/security";

import { ProgressSpinner } from "primereact/progressspinner";
import {
  GENERIC_CODE,
  GENERIC_CODE_RES,
} from "../../../../../../../constants/index";
import BaseForm from "../../../../../../../components/base-form/base-form";
import { getControlModel } from "../../../../../../../components/base-control/base-cotrol-model";
// import { EpPersonalHideAction } from "../modules/ep-profile-constants/ep-personal-hide-action";

const RentalAddress = (props) => {
  const [ray] = useState(ApiRequest.createRay("RentalAddress"));
  const { t } = useTranslation();

  const [state, setState] = useState({
    rentalAddresses: [],
    loading: true,
  });

  const mountedRef = useRef(true);
  useEffect(() => {
    return () => {
      mountedRef.current = false;
    };
  }, []);

  useEffect(() => {
    const loadData = async () => {
      setState({ ...state, loading: true });
      const apiRequestParam = await ApiRequest.createParam();
      let enumData = {};

      const cmd1 = GenericCodeService.genericCodeGetGenericCodes({
        ray,
        ...apiRequestParam,
        parameterGenericCodeList: [
          GENERIC_CODE.REASONDEPARTURE,
          GENERIC_CODE.NATURE,
        ],
        dataResData: GENERIC_CODE_RES.GET_COMPONENT_ITEM,
      });

      let cmd2 = EmpDataService.empDataGetRentalAddress({
        ray,
        ...apiRequestParam,
        employeeId: props.employeeId,
      });

      let cmd3 = EmpDataService.empDataGetEmploymentContractByEmployeeId({
        ray,
        ...apiRequestParam,
        employeeId: props.employeeId,
      });

      const [res1, res2, res3] = await Promise.all([cmd1, cmd2, cmd3]);
      if (res1 && res1.data) {
        for (const key in res1.data.genericCodes) {
          if (res1.data.genericCodes.hasOwnProperty(key)) {
            const element = res1.data.genericCodes[key];
            const enumElm = element.map((x) => {
              return {
                label: x.textValue,
                value: x.codeValue,
                data: {
                  ...x,
                },
              };
            });
            enumData[key] = enumElm;
          }
        }
      }
      let rentalAddresses = [];
      // let remarkRentalAddress = "";
      if (res2 && res2.data) {
        rentalAddresses = res2.data.rentalAddresses;
        // remarkRentalAddress = res2.data.remarkRentalAddress;
      }
      if (res3 && res3.data) {
        let employmentContracts = [];
        res3.data.employmentContracts.forEach((contract) => {
          let startDate = contract.contractStartDate
            ? moment(contract.contractStartDate).format("DD/MM/YYYY")
            : t("N/A");
          let endDate = contract.contractEndDate
            ? moment(contract.contractEndDate).format("DD/MM/YYYY")
            : t("N/A");

          employmentContracts.push({
            label: `${contract.referenceId || ""} (${startDate} - ${endDate})`,

            value: contract.employmentContractId,
            data: {
              ...contract,
            },
          });
        });
        enumData["contract"] = employmentContracts;
      }

      if (!mountedRef.current) return null;
      let _state = {
        loading: false,
        enumData,
        rentalAddresses,
      };
      setState(_state);
    };
    loadData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ray, props.employeeId]);

  // console.log("rental - ", state);

  const renderItem = () => {
    const tab = [];
    if (!state.rentalAddresses || state.rentalAddresses.length === 0) {
      if (props.readOnly) {
        return <div style={{ padding: "1rem" }}>{t("No data")}</div>;
      }
      return null;
    }
    state.rentalAddresses.forEach((data, index) => {
      tab.push(
        <div key={index}>
          <div
            className="c-group-sub-title"
            onClick={() => {
              let _state = { ...state };
              _state[index + "-close"] = !_state[index + "-close"];
              setState(_state);
            }}
          >
            {`${t("employee-profile.rental-info")} (${index + 1})`}

            <div className="c-group-sub-tile-action">
              <Button
                type="button"
                icon={
                  state[index + "-close"]
                    ? "pi pi-chevron-down"
                    : "pi pi-chevron-up"
                }
                className="p-button-text"
              />
            </div>
          </div>
          <div
            className={`${
              state[index + "-close"] ? "hide-panel" : ""
            } c-group-panel`}
          >
            <RentailAddressItem
              enumData={state.enumData}
              data={data}
              id={index}
              readOnly={props.readOnly}
              // hidden={props.hidden}
            />
          </div>
        </div>
      );
    });
    return tab;
  };

  return (
    <>
      <Animated
        animationIn="slideInRight"
        animationOut="slideOutRight"
        animationInDuration={200}
        animationOutDuration={200}
        isVisible={true}
      >
        {state.loading || state.submitting ? (
          <div className="comp-loading">
            <ProgressSpinner />
          </div>
        ) : null}

        <div className="c-group-title">{t("employee-profile.rental-info")}</div>
        <div className="c-group-panel">{renderItem()}</div>
      </Animated>
    </>
  );
};

const RentailAddressItem = (props) => {
  const { t } = useTranslation();

  const formConfig = {
    controls: [
      getControlModel({
        key: "nature",
        label: t("employee-profile.rental.Nature"),
        type: "select",
        enum: props.enumData?.nature,
      }),
      getControlModel({
        key: "address",
        label: t("employee-profile.rental.Address"),
        type: "textarea",
      }),
      getControlModel({
        key: "startDate",
        label: t("employee-profile.rental.Start Date"),
        type: "date",
      }),
      getControlModel({
        key: "endDate",
        label: t("employee-profile.rental.End Date"),
        type: "date",
      }),
    ],
    layout: {
      rows: [
        {
          columns: [
            {
              config: {
                className: "p-col-12 p-md-6 p-lg-6 ",
              },
              control: "nature",
            },
          ],
        },
        {
          columns: [{ control: "address" }],
        },
        {
          columns: [
            {
              config: {
                className: "p-col-12 p-md-6 p-lg-6 ",
              },
              control: "startDate",
            },
            {
              config: {
                className: "p-col-12 p-md-6 p-lg-6 ",
              },
              control: "endDate",
            },
          ],
        },
      ],
    },
  };
  return (
    <>
      <BaseForm
        id={"emergencyRentalAddressItem-" + props.id}
        config={formConfig}
        form={props.data}
        readOnly={props.readOnly}
      />
    </>
  );
};

export default RentalAddress;
