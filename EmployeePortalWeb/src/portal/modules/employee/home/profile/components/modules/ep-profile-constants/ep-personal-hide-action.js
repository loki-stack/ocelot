import { useTranslation } from "react-i18next";
import moment from "moment";

export const EpPersonalHideAction = (
  key,
  labelKey,
  revealState,
  datatype,
  setState
) => {
  const { t } = useTranslation();
  const config = {
    key: key,
    label: labelKey,
    type: revealState ? datatype : "password",
    revealState: revealState,
    action: {
      icon: revealState ? "pi pi-eye" : "pi pi-eye-slash",
      tooltip: t("Reveal"),
      onMouseDown: () => {
        setState(true);
      },
      onMouseUp: () => {
        setState(false);
      },
      onMouseLeave: () => {
        setState(false);
      },
      onTouchMove: () => {
        setState(false);
      },
      onTouchEnd: () => {
        setState(false);
      },
      onTouchStart: () => {
        setState(true);
      },
    },
  };

  return config;
};

export const momentDateConvert = (dateData, t) => {
  return dateData ? moment(dateData).format("DD/MM/YYYY") : t("N/A");
};

export const selectStringConvert = (option = [], data, t) => {
  return data && option.length > 0
    ? option.find((y) => y.value === data)?.label
    : t("N/A");
};

export const multiSelectStringConvert = (option = [], data, t) => {
  if (data && !Array.isArray(data) && option.length > 0) {
    data = data
      .split(",")
      .map((x) => option.find((y) => y.value === x.trim())?.label);
  }

  return Array.isArray(data) ? data.join(",") : t("N/A");
};
