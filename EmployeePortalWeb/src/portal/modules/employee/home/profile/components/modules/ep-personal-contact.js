import React, { useState, useEffect } from "react";
// import { useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import { Animated } from "react-animated-css";

// services
import { GenericCodeService } from "../../../../../../../services/security";
import { ApiRequest } from "../../../../../../../services/utils/index";

//component
import { Button } from "primereact/button";
import { ProgressSpinner } from "primereact/progressspinner";
import BaseForm from "../../../../../../../components/base-form/base-form";
import {
  ContactInfoFormConfig,
  AddressResidentialFormConfig,
  AddressPostalFormConfig,
} from "./ep-profile-constants/ep-personal-contact-form";
import {
  GENERIC_CODE,
  GENERIC_CODE_RES,
} from "../../../../../../../constants/index";
import ToggleControl from "../../../../../../../components/base-control/toggle-control/toggle-control";
import { getControlModel } from "../../../../../../../components/base-control/base-cotrol-model";

import "../../employee-profile.scss";

const Contact = (props) => {
  const [ray] = useState(ApiRequest.createRay("Contact"));

  const { t } = useTranslation();
  const [state, setState] = useState({
    loading: true,
    data: props.data,
    emergencyContacts: [],
  });
  const [languageResidentialToogle, setLanguageResidentialToogle] = useState(
    false
  );
  const [languagePostalToogle, setLanguagePostalToogle] = useState(false);

  const renderItem = () => {
    const tab = [];
    if (!state.emergencyContacts || state.emergencyContacts.length === 0) {
      if (props.readOnly) {
        return <div style={{ padding: "1rem" }}>{t("No data")}</div>;
      }
      return null;
    }

    state.emergencyContacts.forEach((data, index) => {
      tab.push(
        <div key={index}>
          <div
            className="c-group-sub-title"
            onClick={() => {
              let _state = { ...state };
              _state[index + "-close"] = !_state[index + "-close"];
              setState(_state);
            }}
          >
            {`${t("employee-profile.Contact")} (${index + 1})`}

            <div className="c-group-sub-tile-action">
              <Button
                type="button"
                icon={
                  state[index + "-close"]
                    ? "pi pi-chevron-down"
                    : "pi pi-chevron-up"
                }
                className="p-button-text"
              />
            </div>
          </div>
          <div
            className={`${
              state[index + "-close"] ? "hide-panel" : ""
            } c-group-panel`}
          >
            <EmergencyContactItem
              enumData={state.enumData}
              data={data}
              id={index}
              readOnly={props.readOnly}
            />
          </div>
        </div>
      );
    });
    return tab;
  };

  useEffect(() => {
    const loadData = async () => {
      setState({ ...state, loading: true });
      const apiRequestParam = await ApiRequest.createParam();
      let enumData = {};

      const cmd1 = GenericCodeService.genericCodeGetGenericCodes({
        ray,
        ...apiRequestParam,
        parameterGenericCodeList: [
          GENERIC_CODE.COUNTRY,
          GENERIC_CODE.DISTRICT,
          GENERIC_CODE.RELATIONSHIP,
        ],
        dataResData: GENERIC_CODE_RES.GET_COMPONENT_ITEM,
      });
      const [res1] = await Promise.all([cmd1]);
      if (res1 && res1.data) {
        for (const key in res1.data.genericCodes) {
          if (res1.data.genericCodes.hasOwnProperty(key)) {
            const element = res1.data.genericCodes[key];
            const enumElm = element.map((x) => {
              return {
                label: x.textValue,
                value: x.codeValue,
                data: {
                  ...x,
                },
              };
            });
            enumData[key] = enumElm;
          }
        }
      }

      let emergencyContacts = [];
      if (props.emergencyContacts) {
        emergencyContacts = props.emergencyContacts;
      }
      setState({
        ...state,
        enumData: enumData,
        loading: false,
        emergencyContacts: emergencyContacts,
        data: props.data,
      });
    };
    loadData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ray, props.employeeId, props.emergencyContacts, props.data]);

  // console.log("ep-personal-contact - ", state.emergencyContacts)

  const setStateFn = (e) => {
    setState({ ...state, e });
  };

  return (
    <>
      <Animated
        animationIn="slideInRight"
        animationOut="slideOutRight"
        animationInDuration={200}
        animationOutDuration={200}
        isVisible={true}
      >
        {state.loading || state.submitting ? (
          <div className="comp-loading">
            <ProgressSpinner />
          </div>
        ) : null}

        <div className="c-group-title">
          {t("employee-profile.contact.title")}
        </div>
        <div className="c-group-panel">
          <BaseForm
            id="contactInfo"
            config={ContactInfoFormConfig(t)}
            form={state.data}
            readOnly
          />
        </div>
        <div className="c-group-title ep-language-toogle">
          {t("employee-profile.contact.Address(Residential)")}
          <ToggleControl
            className="checkbox-control"
            trueValue="Secondary Language"
            falseValue="Primary Language"
            value={languageResidentialToogle}
            onChange={(e) => {
              setLanguageResidentialToogle(e.value);
            }}
            id={"language-control"}
          />
        </div>
        <div className="c-group-panel">
          <BaseForm
            id="addressResidential"
            config={AddressResidentialFormConfig(
              t,
              state,
              setStateFn,
              languageResidentialToogle
            )}
            form={state.data}
            readOnly
          />
        </div>
        <div className="c-group-title ep-language-toogle">
          {t("employee-profile.contact.Address(Postal)")}
          <ToggleControl
            className="checkbox-control"
            trueValue="Secondary Language"
            falseValue="Primary Language"
            value={languagePostalToogle}
            onChange={(e) => {
              setLanguagePostalToogle(e.value);
            }}
            id={"language-control"}
          />
        </div>
        <div className="c-group-panel">
          <BaseForm
            id="addressPostal"
            config={AddressPostalFormConfig(
              t,
              state,
              setStateFn,
              languagePostalToogle
            )}
            form={state.data}
            readOnly
          />
        </div>
        <div className="c-group-title">
          {t("employee-profile.contact.EmergencyContactPerson")}
        </div>
        <div className="c-group-panel c-group-panel-no-padding">
          {renderItem()}
        </div>
      </Animated>
    </>
  );
};

const EmergencyContactItem = (props) => {
  // console.log("EmergencyContactItem", props);
  const { t } = useTranslation();
  const [state, setState] = useState({
    isOther: props.data?.relationshipToEmployee === "OT",
  });
  useEffect(() => {
    setState({
      isOther: props.data?.relationshipToEmployee === "OT",
    });
  }, [props.data.relationshipToEmployee]);
  const formConfig = {
    controls: [
      getControlModel({
        required: true,
        noRequiredLabel: true,
        key: "relationshipToEmployee",
        label: t("employee-profile.contact.Relationship"),
        type: "select",
        enum: props.enumData?.relationship,
      }),
      getControlModel({
        required: state.isOther,
        noRequiredLabel: true,
        key: "relationshipOther",
        label: t("employee-profile.contact.select"),
      }),
      getControlModel({
        required: true,
        noRequiredLabel: true,
        key: "name",
        label: t("employee-profile.contact.name"),
      }),
      getControlModel({
        required: true,
        noRequiredLabel: true,
        label: t("employee-profile.contact.mobilePhone"),
        key: "mobilePhone",
      }),
    ],
    layout: {
      rows: [
        {
          columns: [
            {
              config: {
                className: "p-col-6",
              },
              control: "relationshipToEmployee",
            },
            {
              config: {
                className: "p-col-6",
                style: {
                  display: state.isOther ? null : "none",
                },
              },
              control: "relationshipOther",
            },
          ],
        },
        {
          columns: [
            { control: "name" },
            {
              control: "mobilePhone",
            },
          ],
        },
      ],
    },
  };
  return (
    <>
      <BaseForm
        id={"emergencyContactItem" + props.id}
        config={formConfig}
        form={props.data}
        readOnly
      />
    </>
  );
};

export default Contact;
