import "./employee-profile.scss";

import React, { useRef, useState } from "react";

import { Animated } from "react-animated-css";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import { LazyLoadImage } from "react-lazy-load-image-component";
import BaseForm from "../../../../../components/base-form/base-form";
import { ApiRequest } from "./../../../../../services/utils/index";
import { ProgressSpinner } from "primereact/progressspinner";
// import { EmployeeService } from "./../../../../../services/employee";
import { GenericCodeService } from "./../../../../../services/security";
import {
  EmpDataService,
  FileService /* , EmpPortalFileService */,
} from "./../../../../../services/employee";
import {
  BuildFileFromId /* , BuildFileLink */,
} from "./../../../../../utils/utils";
//components
import RightPanel from "./components/employee-profile-rightPanel";
import FormConfig from "./components/employee-profile-formConfig";
import { Tree } from "primereact/tree";

//const
import {
  GENERIC_CODE,
  GENERIC_CODE_RES,
} from "./../../../../../constants/index";
import {
  StorageKey,
  StorageSerive,
} from "./../../../../../services/storageService/index";

const EmployeeProfile = (props) => {
  const { t } = useTranslation();
  const [ray] = useState(ApiRequest.createRay("EmployeeProfile"));
  const clientId = useSelector((state) => state.auth.data?.user?.clientId);
  const entityId = useSelector((state) => state.auth.data?.user?.entityId);
  /** Setup state */
  const [state, setState] = useState({
    selectedKey: "personalInfo",
  });
  // const [languageToogle, setLanguageToogle] = useState(false);
  const [loading, setLoading] = useState({
    loading: true,
  });

  const [avatar, setAvatar] = useState();
  const downloadRes = useRef();

  const axiosCallback = (resp) => {
    downloadRes.current = resp;
  };

  // --------------------------------------------------------------------------
  const loadAvatar = async (avatarId) => {
    if (avatarId > 0) {
      try {
        const imgAvatar = await FileService.fileDownloadFile(
          {
            id: avatarId,
            ray,
            //...apiRequestParam,
            parameterLocale: "en",
            parameterClientId: clientId,
            parameterEntityId: entityId,
          },
          {
            responseType: "blob",
            callback: axiosCallback,
          }
        );

        if (downloadRes.current?.status === 200) {
          setAvatar(URL.createObjectURL(imgAvatar));
        } else {
          setAvatar("/assets/images/default.png");
        }
      } catch (err) {
        setAvatar("/assets/images/default.png");
      }
    }
  };

  // const rightPanelRef = useRef();
  const mountedRef = useRef(true);
  useEffect(() => {
    return () => {
      mountedRef.current = false;
    };
  }, []);
  /** Init data */
  useEffect(() => {
    const initPage = async () => {
      let _state = {
        ...state,
      };
      // Load generic code
      //const apiRequestParam = await ApiRequest.createParam();
      const cmd1 = GenericCodeService.genericCodeGetGenericCodes({
        ray,
        //...apiRequestParam,
        parameterLocale: "en",
        parameterClientId: clientId,
        parameterEntityId: entityId,
        parameterGenericCodeList: [
          GENERIC_CODE.STATUS,
          GENERIC_CODE.INTERNALJOBTITLE,
        ],
        dataResData: GENERIC_CODE_RES.GET_COMPONENT_ITEM,
      });

      let enumData = {};
      const [res] = await Promise.all([cmd1]);
      if (res && res.data) {
        for (const key in res.data.genericCodes) {
          if (res.data.genericCodes.hasOwnProperty(key)) {
            const element = res.data.genericCodes[key];
            const enumElm = element.map((x) => {
              return {
                label: x.textValue,
                value: x.codeValue,
                data: {
                  ...x,
                },
              };
            });
            enumData[key] = enumElm;
          }
        }
      }

      const retrieveEmpProfileFn = async (
        ray,
        enumData,
        clientId,
        entityId
      ) => {
        //
        const apiRequestParam = await ApiRequest.createParam();
        // const cmd1 = EmployeeService.employeeGetBasicInfo({
        let initForm = {};
        let initForm2 = {};
        let emergencyContact = [];

        const cmd1 = EmpDataService.empDataProfile({
          ray,
          //...apiRequestParam,
          parameterLocale: "en",
          parameterClientId: clientId,
          parameterEntityId: entityId,
        });

        const cmd2 = EmpDataService.empDataGetBasicInfo({
          ray,
          //...apiRequestParam,
          parameterLocale: "en",
          parameterClientId: clientId,
          parameterEntityId: entityId,
        });

        const [res1, res2 /* , res3 */] = await Promise.all([
          cmd1,
          cmd2 /* , cmd3 */,
        ]);

        if (res1 && res1.data) {
          if (res1.data.profile.photo) {
            loadAvatar(res1.data.profile.photo);
          } else {
            setAvatar("/assets/images/default.png");
          }

          initForm = {
            ...res1.data.profile,
            role: enumData?.internaljobtitle?.find(
              (y) => y.value === res1.data.profile.role
            )?.label,
            avatar: BuildFileFromId(
              res1.data.profile.photo,
              apiRequestParam.parameterClientId,
              apiRequestParam.parameterEntityId
            ),
          };

          let mainUserInfo = {
            // ...res3,
            roleName: initForm.role,
            email: res1.data.profile.email,
            fullname: res1.data.profile.name,
            avatar: res1.data.profile.photo,
          };

          await StorageSerive.setItem(
            StorageKey.USER_INFO,
            JSON.stringify(mainUserInfo)
          );
        }

        if (res2 && res2.data) {
          initForm2 = {
            ...res2.data.employee,
          };
          emergencyContact = res2.data.emergencyContacts;
        }

        if (initForm && initForm2) {
          return {
            datas: {
              summary: initForm,
              basicInfo: initForm2,
              emergencyContacts: emergencyContact,
            },
          };
        } else {
          return {
            datas: {},
          };
        }
      };

      setState({
        ..._state,
        enumData,
        data: await retrieveEmpProfileFn(
          ray,
          enumData,
          clientId,
          entityId,
          props.employeeId
        ).finally(setLoading(false)),
      });
    };
    initPage();
    // TODO - FIX ME - hardcoded for test
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.employeeId, ray]);

  const onStateChange = (e) => {
    const { loading, submitting, invalid, touched } = e;
    setState({
      ...state,
      loading,
      submitting,
      invalid,
      touched,
    });
  };

  let nodes = [
    {
      key: "personalInfo",
      label: t("employee-profile.personalInfo"),
      selectable: true,
      leaf: true,
    },
    {
      key: "changePassword",
      label: t("employee-profile.changePassword"),
      selectable: true,
      leaf: true,
    },
    {
      key: "contact",
      label: t("employee-profile.Contact"),
      selectable: true,
      leaf: true,
    },
    {
      key: "dependants",
      label: t("employee-profile.Dependants"),
      selectable: true,
      leaf: true,
    },
    {
      key: "contract",
      label: t("employee-profile.contract"),
      selectable: true,
      leaf: true,
    },
    {
      key: "movement",
      label: t("employee-profile.movement-info"),
      selectable: true,
      leaf: true,
    },
    {
      key: "rentalAddress",
      label: t("employee-profile.rental-info"),
      selectable: true,
      leaf: true,
    },
    {
      key: "bankAccount",
      label: t("employee-profile.bank"),
      selectable: true,
      leaf: true,
    },
    {
      key: "employmentStatus",
      label: t("employee-profile.employment"),
      selectable: true,
      leaf: true,
    },
    {
      key: "portalAccess",
      label: t("employee-profile.EE Portal Access"),
      selectable: true,
      leaf: true,
    },
  ];

  return (
    <>
      {loading ? (
        <div className="comp-loading">
          <ProgressSpinner />
        </div>
      ) : null}
      <Animated
        animationIn="slideInRight"
        animationOut="slideOutRight"
        animationInDuration={200}
        animationOutDuration={200}
        isVisible={true}
      >
        <div className="c-page-body p-grid">
          <div className="p-col-12">
            <div className="c-group-title">{t("Profile")}</div>
            <div className="c-group-panel">
              <div className="p-grid">
                <div
                  className="c-left-panel p-col-12 p-md-3 p-lg-2"
                  style={{ margin: "auto" }}
                >
                  <LazyLoadImage
                    className="c-avatar-thumb"
                    alt={t("Avatar")}
                    effect="blur"
                    src={avatar}
                  />
                </div>
                <div className="styled-readonly c-right-panel p-col-12 p-md-9 p-lg-10">
                  <div className="c-item-main">
                    <div className="c-item-main-desc">{t("Name")}:</div>
                    {/* {state.form?.name} */}
                    {state.data?.datas?.summary?.name}
                  </div>
                  <BaseForm
                    id="employeeProfileSummary"
                    config={FormConfig()}
                    readOnly
                    form={state.data?.datas?.summary}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="c-left-panel p-col-12 p-md-3 p-lg-2">
            <Tree
              className="c-setting-tree"
              selectionMode="single"
              selectionKeys={state.selectedKey}
              onSelectionChange={(e) => {
                setState({
                  ...state,
                  selectedKey: e.value,
                });
              }}
              value={nodes || []}
            />
          </div>
          <div className="right-panel p-col-12 p-md-9 p-lg-10">
            <RightPanel
              onStateChange={onStateChange}
              employeeId={props.employeeId}
              basicInfo={state.data?.datas?.basicInfo}
              emergencyContacts={state.data?.datas?.emergencyContacts}
              selectionKeys={state.selectedKey}
            />
          </div>
        </div>
      </Animated>
    </>
  );
};

export default EmployeeProfile;
