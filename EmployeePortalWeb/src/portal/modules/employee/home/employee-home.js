import { Route, Switch, useRouteMatch } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";

//components
import EmployeeProfile from "./profile/employee-profile";
const EmployeeHome = () => {
  const employeeId = useSelector((state) => state.auth.data?.user?.employeeId);

  // i18n
  const { t } = useTranslation();
  let match = useRouteMatch();
  return (
    <>
      <h1>{t("common.employeehome")}</h1>
      <Switch>
        <Route
          path={`${match.path}/EMP5001/view`}
          render={(props) => (
            <EmployeeProfile {...props} employeeId={employeeId} />
          )}
        />
      </Switch>
    </>
  );
};

export default EmployeeHome;
