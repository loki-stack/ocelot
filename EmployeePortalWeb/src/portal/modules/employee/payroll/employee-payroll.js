import { Route, Switch, useRouteMatch } from "react-router-dom";

import React from "react";
import EmployeePaySlip from "./payslip/ep-payslip";
import EmployeeTaxReturn from "./taxreturn/ep-taxreturn";

import "./employee-payroll.scss";

const EmployeePayRoll = () => {
  let match = useRouteMatch();
  return (
    <>
      <Switch>
        <Route
          path={`${match.path}/PAY5001/search`}
          component={EmployeePaySlip}
        />
        <Route
          path={`${match.path}/PAY5101/search`}
          component={EmployeeTaxReturn}
        />
      </Switch>
    </>
  );
};

export default EmployeePayRoll;
