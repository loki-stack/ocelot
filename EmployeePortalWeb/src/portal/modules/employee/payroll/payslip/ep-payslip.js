import React, { /* useEffect ,*/ useState } from "react";
import { useEffect } from "react";
import { useRef } from "react";
import { useTranslation } from "react-i18next";
import { useSelector /* , useDispatch */ } from "react-redux";
import { saveAs } from "file-saver";
import moment from "moment";

//component
import { Animated } from "react-animated-css";
import BaseTable, {
  BUILD_SEARCH_CONFIG,
} from "../../../../../components/base-table/base-table-portal";
import { getControlModel } from "../../../../../components/base-control/base-cotrol-model";
import {
  DEFAULT_BT_SEARCH,
  getBTConfig,
  getColumModel,
} from "../../../../../components/base-table/base-table-model";

//api
import {
  EmpDataService,
  EmpPortalFileService,
} from "../../../../../services/employee";
import { ApiRequest } from "../../../../../services/utils/index";

//style
import "./ep-payslip.scss";

/** Default search */
const BT_SEARCH = {
  ...DEFAULT_BT_SEARCH,
  sortObj: {
    key: "payslipDescription",
    direction: 1,
  },
  // filterText: "payslipDescription"
};

const tableSearchFn = async (searchConfig, ray) => {
  if (!searchConfig) {
    searchConfig = { ...BT_SEARCH };
  }
  // console.log("--ep-payslip-- tableSearchFn")
  searchConfig = BUILD_SEARCH_CONFIG(searchConfig);
  const apiRequestParam = await ApiRequest.createParam();
  var res = await EmpDataService.empDataGetAllPayslips({
    ray,
    ...apiRequestParam,
  });
  if (res && res.data?.payslips) {
    const destination = [];
    let yearOption = [];
    const source = res.data?.payslips;
    source.forEach((element) => {
      const formattedData = {
        createdYear: new Date(element.createdDt).getFullYear(),
        ...element,
      };
      const createdYear = new Date(element.createdDt).getFullYear();
      yearOption.push(createdYear);
      destination.push(formattedData);
    });
    const uniqueYearOption = Array.from(new Set(yearOption));
    const sortedUniqueYearOption = uniqueYearOption.sort();
    yearOption = [];
    sortedUniqueYearOption.forEach((e) => {
      const formattedData = {
        label: e.toString(),
        value: e.toString(),
      };
      yearOption.push(formattedData);
    });
    return {
      datas: res.data.payslips.map((x) => {
        return {
          ...x,
          id: x.payslipId.toString(),
          createdDate: moment(x.createdDt).format("DD/MM/YYYY"),
          createdYear: new Date(x.createdDt).getFullYear(),
        };
      }),
      yearOption: yearOption,
      total: res.data.payslips.length,
      searchConfig: { ...searchConfig },
    };
  } else {
    return {
      datas: [],
      total: 0,
      searchConfig: searchConfig,
    };
  }
};

const EmployeePaySlip = () => {
  // i18n
  const { t } = useTranslation();
  const [ray] = useState(ApiRequest.createRay("EmployeePaySlip"));
  const employeeId = useSelector((state) => state.auth.data?.user?.employeeId);
  const dataTable = useRef();
  const downloadRes = useRef();
  const [state, setState] = useState({
    tableConfig: null,
  });

  const mountedRef = useRef(true);
  useEffect(() => {
    return () => {
      mountedRef.current = false;
    };
  }, []);

  useEffect(() => {
    const initPage = async () => {
      if (!mountedRef.current) return null;
      setState({
        searchFn: (searchConfig) => tableSearchFn(searchConfig, ray),
        yearFilterOption: (await tableSearchFn(null, ray)).yearOption,
      });
    };
    initPage();
  }, [ray, t, employeeId]);

  const axiosCallback = (resp) => {
    downloadRes.current = resp;
  };

  // actions
  const onMultiDownload = async (item) => {
    let fileIdList = [];
    item.forEach((element) => {
      fileIdList.push(element.fileId);
    });
    const apiRequestParam = await ApiRequest.createParam();
    var res = await EmpPortalFileService.empPortalFileGetFilesAsZip(
      {
        ray,
        ...apiRequestParam,
        data: fileIdList,
        id: employeeId,
      },
      {
        responseType: "blob",
        callback: axiosCallback,
      }
    );

    if (downloadRes.current) {
      // console.log("--------download---", downloadRes.current);
      var filename = downloadRes.current.headers["x-suggested-filename"];
      if (!filename) {
        filename = "payslips.zip";
      }
      saveAs(res, filename);
    }
  };
  const configModel = getBTConfig({
    title: t("payslips.Payslip"),
    description: t("payslips.datatable.description"),
    columns: [
      getColumModel({
        header: t("payslips.datatable.column.Description"),
        key: "payslipDescription",
        dataType: "string",
        frozen: true,
        config: {
          sortable: true,
        },
        control: getControlModel({
          placeholder: t(
            "payslips.datatable.column.desciption-filter-placeholder"
          ),
        }),
      }),
      getColumModel({
        header: t("payslips.datatable.column.createdDate"),
        key: "createdDate",
        dataType: "string",
        config: {
          sortable: true,
        },
        filterMatchMode: "contains",
        control: getControlModel({
          type: "select",
          placeholder: t(
            "payslips.datatable.column.createdDate-filter-placeholder"
          ),
          enum: state.yearFilterOption,
        }),
      }),
    ],
    actionsSingle: [
      {
        renderIcon: "pi pi-download",
        clickFn: (item) => {
          onSingleDownload(item);
        },
      },
    ],
    actionsMulti: [
      {
        renderIcon: "pi pi-download",
        placeholder: t("payslips.datatable.column.action-title-download"),
        kind: "primary",
        clickFn: (item) => {
          onMultiDownload(item);
        },
      },
    ],
    actionWidth: 5,
    hasIndex: false,
    mode: "list",
    defaultMode: "list",
    showGlobal: true,
    // hasColumnSelector: true,
  });

  const onSingleDownload = async (item) => {
    const apiRequestParam = await ApiRequest.createParam();
    var res = await EmpPortalFileService.empPortalFileDownloadFile(
      {
        ray,
        ...apiRequestParam,
        id: item.fileId,
      },
      {
        responseType: "blob",
        callback: axiosCallback,
      }
    );

    if (downloadRes.current) {
      // console.log("--------download---", downloadRes.current);
      var filename = downloadRes.current.headers["x-suggested-filename"];
      if (!filename) {
        filename = "payslips.pdf";
      }
      saveAs(res, filename);
    }
  };

  // console.log("dataTable", dataTable);

  return (
    <div className="ep-payslip-baseTable">
      <Animated
        animationIn="slideInRight"
        animationOut="slideOutRight"
        animationInDuration={200}
        animationOutDuration={200}
        isVisible={true}
      >
        <BaseTable
          ref={dataTable}
          configModel={configModel}
          searchConfig={BT_SEARCH}
          searchFn={state.searchFn}
          isClientSize={true}
        />
      </Animated>
    </div>
  );
};

export default EmployeePaySlip;
