import React, { /* useEffect ,*/ useState } from "react";
import { useEffect } from "react";
import { useRef } from "react";
import { useTranslation } from "react-i18next";
import { useSelector /* , useDispatch */ } from "react-redux";
import { saveAs } from "file-saver";

//component
import { Animated } from "react-animated-css";
import BaseTable, {
  BUILD_SEARCH_CONFIG,
} from "../../../../../components/base-table/base-table";
import { getControlModel } from "../../../../../components/base-control/base-cotrol-model";
import {
  DEFAULT_BT_SEARCH,
  getBTConfig,
  getColumModel,
} from "../../../../../components/base-table/base-table-model";

//api
import {
  EmpDataService,
  EmpPortalFileService,
} from "../../../../../services/employee";
import { ApiRequest } from "../../../../../services/utils/index";

/** Default search */
const BT_SEARCH = {
  ...DEFAULT_BT_SEARCH,
  sortObj: {
    key: "taxReturnDescription",
    direction: 1,
  },
};

const tableSearchFn = async (searchConfig, ray, employeeId) => {
  if (!searchConfig) {
    searchConfig = { ...BT_SEARCH };
  }
  searchConfig = BUILD_SEARCH_CONFIG(searchConfig);
  const apiRequestParam = await ApiRequest.createParam();
  var res = await EmpDataService.empDataGetAllTaxReturn({
    ray,
    ...apiRequestParam,
  });
  if (res && res.data?.taxReturn) {
    const destination = [];
    let yearOption = [];
    const source = res.data?.taxReturn;
    source.forEach((element) => {
      const formattedData = {
        createdYear: new Date(element.createdDt).getFullYear(),
        ...element,
      };
      const createdYear = new Date(element.createdDt).getFullYear();
      yearOption.push(createdYear);
      destination.push(formattedData);
    });
    const uniqueYearOption = Array.from(new Set(yearOption));
    yearOption = [];
    uniqueYearOption.forEach((e) => {
      const formattedData = {
        label: e.toString(),
        value: e.toString(),
      };
      yearOption.push(formattedData);
    });
    return {
      datas: res.data.taxReturn.map((x) => {
        return {
          ...x,
          id: x.taxReturnId.toString(),
          createdYear: new Date(x.createdDt).getFullYear(),
        };
      }),
      yearOption: yearOption,
      total: res.data.taxReturn.length,
      searchConfig: searchConfig,
    };
  } else {
    return {
      datas: [],
      total: 0,
      searchConfig: searchConfig,
    };
  }
};

const EmployeeTaxReturn = () => {
  // i18n
  const { t } = useTranslation();
  const [ray] = useState(ApiRequest.createRay("EmployeeTaxReturn"));
  const employeeId = useSelector((state) => state.auth.data?.user?.employeeId);
  const dataTable = useRef();
  const downloadRes = useRef();
  const [state, setState] = useState({
    tableConfig: null,
  });

  const mountedRef = useRef(true);
  useEffect(() => {
    return () => {
      mountedRef.current = false;
    };
  }, []);

  useEffect(() => {
    const initPage = async () => {
      if (!mountedRef.current) return null;
      setState({
        searchFn: (searchConfig) =>
          tableSearchFn(searchConfig, ray, employeeId),
        yearFilterOption: (await tableSearchFn(null, ray, employeeId))
          .yearOption,
      });
    };
    initPage();
  }, [ray, t, employeeId]);

  const axiosCallback = (resp) => {
    downloadRes.current = resp;
  };

  // actions
  const onMultiDownload = async (item) => {
    let fileIdList = [];
    item.forEach((element) => {
      fileIdList.push(element.fileId);
    });
    const apiRequestParam = await ApiRequest.createParam();
    var res = await EmpPortalFileService.empPortalFileGetTaxReturnFilesAsZip(
      {
        ray,
        ...apiRequestParam,
        data: fileIdList,
        id: employeeId,
      },
      {
        responseType: "blob",
        callback: axiosCallback,
      }
    );

    if (downloadRes.current) {
      // console.log("--------download---", downloadRes.current);
      var filename = downloadRes.current.headers["x-suggested-filename"];
      if (!filename) {
        filename = "taxReturn.zip";
      }
      saveAs(res, filename);
    }
  };
  const configModel = getBTConfig({
    title: t("Tax-return.title"),
    description: t("Tax-return.description"),
    columns: [
      getColumModel({
        header: t("Tax-return.column.description"),
        key: "taxReturnDescription",
        frozen: true,
        width: 20,
        dataType: "string",
        config: {
          sortable: true,
          filter: true,
          excludeGlobalFilter: true,
        },
        control: getControlModel({
          placeholder: t("Tax-return.filter.description"),
        }),
      }),
      getColumModel({
        header: t("Tax-return.column.createdDate"),
        key: "taxReturnYear",
        render: (row) => <>{t(row?.createdYear)}</>,
        dataType: "string",
        config: {
          sortable: true,
          filter: true,
          excludeGlobalFilter: true,
        },
        control: {
          type: "select",
          placeholder: t("Tax-return.filter.created-year"),
          enum: state.yearFilterOption,
        },
      }),
    ],
    actionsSingle: [
      {
        renderIcon: "pi pi-download",
        clickFn: (item) => {
          onSingleDownload(item);
        },
      },
    ],
    actionsMulti: [
      {
        renderIcon: "pi pi-download",
        className: "p-button-danger",
        title: t("Tax-return.download"),
        kind: "primary",
        clickFn: (item) => {
          onMultiDownload(item);
        },
      },
    ],
    actionWidth: 5,
    hasIndex: false,
    mode: "list",
    defaultMode: "list",
    hasColumnSelector: true,
  });

  const onSingleDownload = async (item) => {
    const apiRequestParam = await ApiRequest.createParam();
    var res = await EmpPortalFileService.empPortalFileDownloadTaxReturnFile(
      {
        ray,
        ...apiRequestParam,
        id: item.fileId,
      },
      {
        responseType: "blob",
        callback: axiosCallback,
      }
    );

    if (downloadRes.current) {
      console.log("--------download---", downloadRes.current);
      var filename = downloadRes.current.headers["x-suggested-filename"];
      if (!filename) {
        filename = "taxReturn.zip";
      }
      saveAs(res, filename);
    }
  };

  return (
    <>
      <Animated
        animationIn="slideInRight"
        animationOut="slideOutRight"
        animationInDuration={200}
        animationOutDuration={200}
        isVisible={true}
      >
        <BaseTable
          ref={dataTable}
          configModel={configModel}
          searchConfig={BT_SEARCH}
          searchFn={state.searchFn}
          isClientSize={true}
        />
      </Animated>
    </>
  );
};

export default EmployeeTaxReturn;
