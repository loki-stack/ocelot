// short hand for exporting sub modules components
export { default as EmployeeHome } from "./employee/home/employee-home";
export { default as EmployeePayRoll } from "./employee/payroll/employee-payroll";
