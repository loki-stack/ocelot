import React, { useEffect } from "react";
import { /* Redirect, */ Route, Switch, useRouteMatch } from "react-router-dom";
import { setGlobal } from "../redux/actions/global";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { MODULE_CODE } from "../constants/index";
// components
import { EmployeeHome, EmployeePayRoll } from "./modules";

function EmployeeRouter() {
  let match = useRouteMatch();
  const routes = [
    {
      path: "/" + MODULE_CODE.EmploymentManagement,
      defaultLayout: "HorizontalLayout",
      component: EmployeeHome,
    },
    {
      path: "/" + MODULE_CODE.Payroll,
      defaultLayout: "HorizontalLayout",
      component: EmployeePayRoll,
    },
  ];
  return (
    <>
      <Switch>
        {routes.map((route, i) => (
          <Route
            key={i}
            path={`${match.path}${route.path}`}
            render={() => {
              return React.createElement(EmployeContent, route);
            }}
          />
        ))}
      </Switch>
    </>
  );
}

export default EmployeeRouter;

const EmployeContent = React.forwardRef((props, ref) => {
  const dispatch = useDispatch();
  const global = useSelector((state) => state.global) || null;
  useEffect(() => {
    if (global?.layout !== props.defaultLayout) {
      dispatch(
        setGlobal({
          layout: props.defaultLayout,
        })
      );
    }
  });
  return React.createElement(props.component, {});
});
