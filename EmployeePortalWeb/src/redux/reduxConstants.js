// constants
export const AUTH_SET_INFO = "AUTH_SET_INFO";
export const SET_LIST_TENANT = "SET_LIST_TENANT";
export const SET_GLOBAL = "SET_GLOBAL";
export const SET_SIDEBAR = "SET_SIDEBAR";

export const SET_BREAD_CRUMB = "SET_BREAD_CRUMB";
export const SET_MODAL = "SET_MODAL";
export const SET_SIDE_MODAL = "SET_SIDE_MODAL";
export const SET_SIDE_MODAL_DYNA = "SET_SIDE_MODAL_DYNA";

export const EMP_UPDATE_INFO = "EMP_UPDATE_INFO";
