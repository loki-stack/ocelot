import { SET_LIST_TENANT } from "./../reduxConstants";
/**
 * set user status
 */
export const setClientInfo = (clientInfo) => {
  return {
    type: SET_LIST_TENANT,
    payload: clientInfo,
  };
};
