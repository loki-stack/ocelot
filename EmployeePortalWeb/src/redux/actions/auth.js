import { AUTH_SET_INFO } from "./../reduxConstants";

/**
 * set user status
 */
export const setAuthInfo = (authInfo) => {
  return {
    type: AUTH_SET_INFO,
    payload: authInfo,
  };
};
