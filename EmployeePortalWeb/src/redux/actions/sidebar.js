import { SET_SIDEBAR } from "./../reduxConstants";

/**
 * set user status
 */
export const setSideBar = (sideBarInfo) => {
  return {
    type: SET_SIDEBAR,
    payload: sideBarInfo,
  };
};
