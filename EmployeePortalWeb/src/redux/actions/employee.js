import { EMP_UPDATE_INFO } from "./../reduxConstants";
/**
 * update employee info
 */
export const updateEmployeeInfo = (empInfo) => {
  return {
    type: EMP_UPDATE_INFO,
    payload: empInfo,
  };
};
