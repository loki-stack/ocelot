import { SET_MODAL } from "./../reduxConstants";

/**
 * openModal
 */
export const openModal = (modal) => {
  return {
    type: SET_MODAL,
    payload: {
      ...modal,
      isOpen: true,
    },
  };
};

/**
 * Close modal
 */
export const closeModal = () => {
  return {
    type: SET_MODAL,
    payload: {
      isOpen: false,
    },
  };
};
