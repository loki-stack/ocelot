import { MAKE_ID } from "../../utils/utils";
import { SET_GLOBAL } from "./../reduxConstants";

export const setGlobal = (globalInfo) => {
  return {
    type: SET_GLOBAL,
    payload: globalInfo,
  };
};

/**
 * set user status
 */
export const reloadTenant = () => {
  return {
    type: SET_GLOBAL,
    payload: {
      loadTennantID: MAKE_ID(),
    },
  };
};
