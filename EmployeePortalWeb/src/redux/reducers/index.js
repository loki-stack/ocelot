import { combineReducers } from "redux";
//
import { persistReducer, createTransform } from "redux-persist";
import storage from "redux-persist/lib/storage";
import { parse, stringify } from "flatted";
//
import auth from "./auth";
import global from "./global";
import modal from "./modal";
import sideModal from "./sideModal";
import sidebar from "./sidebar";
import client from "./client";
import employee from "./employee";

export const transformCircular = createTransform(
  (inboundState, key) => stringify(inboundState),
  (outboundState, key) => parse(outboundState)
);

const rootPersistConfig = {
  // configuration object for redux-persist
  key: "root",
  storage, // define which storage to use
  // transforms: [transformCircular],
  blacklist: ["global", "sidebar"],
};

// https://github.com/rt2zz/redux-persist#nested-persists
const globalPersistConfig = {
  // configuration object for redux-persist
  key: "global",
  storage, // define which storage to use
  // transforms: [transformCircular],
  blacklist: ["toast"],
};

const rootReducer = combineReducers({
  auth,
  global: persistReducer(globalPersistConfig, global),
  modal,
  sideModal,
  sidebar,
  client,
  employee,
});

// const persistedReducer = persistReducer(persistConfig, rootReducer);
export default persistReducer(rootPersistConfig, rootReducer);

// export default combineReducers({
//   auth,
//   global: persistReducer(globalPersistConfig, global),
//   modal,
//   sideModal,
//   sidebar,
//   client,
//   employee,
// });
