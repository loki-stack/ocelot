import { AUTH_SET_INFO } from "../reduxConstants";
const initialState = {};

export default function auth(state = initialState, action) {
  switch (action.type) {
    case AUTH_SET_INFO: {
      return {
        ...state,
        ...action.payload,
      };
    }
    default: {
      return state;
    }
  }
}
