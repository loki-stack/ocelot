import { SET_MODAL } from "../reduxConstants";
const initialState = {
  isOpen: false,
  data: {},
  modals: {},
};

export default function modal(state = initialState, action) {
  switch (action.type) {
    case SET_MODAL: {
      return {
        ...action.payload,
      };
    }
    default: {
      return state;
    }
  }
}
