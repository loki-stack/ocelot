import { SET_SIDEBAR } from "../reduxConstants";
const initialState = {
  isOpenSidebar: true,
  isOpenSidebarMobile: false,
  mustLoad: false,
  data: [],
};

export default function sidebar(state = initialState, action) {
  switch (action.type) {
    case SET_SIDEBAR: {
      return {
        ...state,
        ...action.payload,
      };
    }
    default: {
      return state;
    }
  }
}
