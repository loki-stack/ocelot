import { EMP_UPDATE_INFO } from "../reduxConstants";
const initialState = {
  data: [],
};

export default function employee(state = initialState, action) {
  switch (action.type) {
    case EMP_UPDATE_INFO: {
      return {
        ...state,
        ...action.payload,
      };
    }
    default: {
      return state;
    }
  }
}
