import moment from "moment";
import { FILE_HOST } from "../constants/index";

export const SLEEP = async (ms: any) => {
  return new Promise((resolve) => setTimeout(resolve, ms));
};
export const MAKE_ID = () => {
  return "hris_xxxxxxxxxx".replace(/[xy]/g, (c) => {
    // eslint-disable-next-line no-mixed-operators
    var r = (Math.random() * 16) | 0,
      v = c === "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
};
export const IS_NUMBER = (value: string) => {
  return /^-?\d+$/.test(value);
};
export const IS_DATE = (value: string) => {
  let date_formart = [
    // moment.ISO_8601,
    // moment.RFC_2822,
    "DD/MM/YYYY",
    "MM/DD/YYYY",
    "YYYY/MM/DD",

    "DD/MM/YYYY hh:mm:ss",
    "MM/DD/YYYY hh:mm:ss",
    "YYYY/MM/DD hh:mm:ss",

    "DD/MM/YYYY hh:mm",
    "MM/DD/YYYY hh:mm",
    "YYYY/MM/DD hh:mm",

    "hh:mm:ss DD/MM/YYYY",
    "hh:mm:ss MM/DD/YYYY",
    "hh:mm:ss YYYY/MM/DD",

    "hh:mm DD/MM/YYYY",
    "hh:mm MM/DD/YYYY",
    "hh:mm YYYY/MM/DD",
  ];
  return moment(value, date_formart, true).isValid();
};
export const GET_OBJ_TYPE = (obj: any) => {
  var dataType = typeof obj;
  if (dataType === "string") {
    if (IS_DATE(obj)) {
      return "date";
    }
    if (IS_NUMBER(obj)) {
      return "number";
    }
  }
  return dataType;
};

export const SORT_ARRAY = (list: any[], key: string, isDesc: boolean) => {
  let compare = (prev: any, next: any) => {
    let a: any;
    let b: any;
    if (isDesc) {
      a = next[key];
      b = prev[key];
    } else {
      a = prev[key];
      b = next[key];
    }
    var result;
    let type = GET_OBJ_TYPE(a);
    if (type === "undefined") {
      type = GET_OBJ_TYPE(b);
    }
    switch (type) {
      case "string":
        a = a || "";
        b = b || "";
        result = a.trim().localeCompare(b.trim());
        break;
      case "date":
        a = new Date(a);
        b = new Date(b);
        result = a - b;
        break;
      default:
        result = a - b;
        break;
    }
    return result;
  };
  return list.sort(compare);
};

export const FILTER_SEARCH_ARRAY = (
  list: any[],
  listPropSearch: string[],
  value: string
) => {
  let resultData = list.filter((x) => {
    let result = false;
    listPropSearch.forEach((prop) => {
      result = result || x[prop].includes(value.trim());
    });
    return result;
  });
  return resultData;
};

export const CLONE_EXCLUDE_OBJ = (obj: any, keys: string[]) => {
  var target: any = {};
  for (var i in obj) {
    if (keys.indexOf(i) >= 0) continue;
    if (!Object.prototype.hasOwnProperty.call(obj, i)) continue;
    target[i] = obj[i];
  }
  return target;
};
export const JSON_EQUAL = (a: any, b: any) => {
  return JSON.stringify(a) === JSON.stringify(b);
};
export const IS_OBJECT_EMPTY = (obj: any) => {
  for (var prop in obj) {
    if (obj.hasOwnProperty(prop)) {
      return false;
    }
  }

  return JSON.stringify(obj) === JSON.stringify({});
};
export const CAMELIZE = (str: string) => {
  return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function (match, index) {
    if (+match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
    return index === 0 ? match.toLowerCase() : match.toUpperCase();
  });
};

/**
 * check if the response contains error
 * @param res
 */
export const hasValidateError = (res: any) => {
  let rv = false;
  if (res && res.message) {
    if (res.message.validateResult && res.message.validateResult.length > 0) {
      for (let mi of res.message.validateResult) {
        if (mi.level === "ERROR") {
          return true;
        }
      }
    }
  }
  return rv;
};

/**
 * check if the response contains error
 * @param res
 */
export const partialObj = (obj: any, props: string[]) => {
  let result: any = {};
  for (const key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      if (props.includes(key)) result[key] = obj[key];
    }
  }
  return result;
};

/**
 * Remove obj
 * @param res
 */
export const removeDetailObj = (obj: any) => {
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      if (key.includes("_obj")) {
        delete obj[key];
      }
    }
  }
  return obj;
};

/**
 * BuildFile
 * @param res
 */
export const BuildFileLink = (src: string) => {
  if (!src) {
    return "/assets/images/default.png";
  }
  if (
    src.startsWith("http") ||
    src.startsWith("data:") ||
    src.startsWith("/assets/")
  ) {
    return src;
  } else {
    let result = `${FILE_HOST}${src}`;
    return result;
  }
};

/**
 * BuildFile
 * @param res
 */
export const BuildFileFromId = (
  id: string,
  clientId: string,
  entityId: string
) => {
  if (!id) {
    return "/assets/images/default.png";
  }
  let result = `${FILE_HOST}/v1.0/empl/COM/COM0401/File/${id}?Parameter.ClientId=${clientId}&Parameter.EntityId=${entityId}`;
  return result;
};

/**
 * Get validate array
 * @param res
 */
export const getValidateResultItem = (
  validateResult: any[],
  index: number,
  prefix: string
) => {
  let result: any[] = [];
  validateResult.forEach((element) => {
    if (element.componentId.includes(`${prefix}[${index}]_`)) {
      let newName = element.componentId.replace(`${prefix}[${index}]_`, "");
      result.push({
        ...element,
        componentId: newName,
      });
    }
  });
  return result;
};
