import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
//
import { BrowserRouter as Router } from "react-router-dom";
import { serviceOptions as serviceOptionsEmployee } from "./services/employee/serviceOptions";
import { serviceOptions as serviceOptionsSecurity } from "./services/security/serviceOptions";
import { createBrowserHistory } from "history";

// redux
import { Provider } from "react-redux";

//
import { store, persistor } from "./redux/store";
import { PersistGate } from "redux-persist/integration/react";

//
import { createAxiosApiInstance } from "./services/axiosConfig";

// configure process.env
require("dotenv").config();

// browser history for services call
const history = createBrowserHistory();

// configuring axios for security services
serviceOptionsSecurity.axios = createAxiosApiInstance(
  process.env.REACT_APP_AUTH_API_URL,
  store,
  history
);
serviceOptionsEmployee.axios = createAxiosApiInstance(
  process.env.REACT_APP_EMPLOYEE_API_URL,
  store,
  history
);

ReactDOM.render(
  <Provider store={store}>
    {/* <React.StrictMode> */}
    <PersistGate loading={null} persistor={persistor}>
      <Router>
        <App />
      </Router>
    </PersistGate>
    {/* </React.StrictMode> */}
  </Provider>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
