import {
  IRequestOptions,
  IRequestConfig,
  getConfigs,
  axios,
} from "./serviceOptions";
const basePath = "";

export interface IList<T> extends Array<T> {}
export interface List<T> extends Array<T> {}
export interface IDictionary<TValue> {
  [key: string]: TValue;
}
export interface Dictionary<TValue> extends IDictionary<TValue> {}

export interface IListResult<T> {
  items?: T[];
}

export class ListResultDto<T> implements IListResult<T> {
  items?: T[];
}

export interface IPagedResult<T> extends IListResult<T> {
  totalCount?: number;
  items?: T[];
}

export class PagedResultDto<T> implements IPagedResult<T> {
  totalCount?: number;
  items?: T[];
}

// customer definition
// empty

export class ClientAdminService {
  /**
   * Get all clients
   */
  static clientAdminGetAllClient(options: IRequestOptions = {}): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/CAM/CAM1101/getall";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Get Client by clientId
   */
  static clientAdminGetByClientId(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
      /**  */
      data?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/CAM/CAM1101/getbyclientid";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
        Data: params["data"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Create client
   */
  static clientAdminCreateClient(
    params: {
      /** requestBody */
      body?: TbClientCreateDtoApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/CAM/CAM1102/create";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Update client
   */
  static clientAdminUpdateClient(
    params: {
      /** requestBody */
      body?: TbClientUpdateDtoApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/CAM/CAM1103/update";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Count active employee
   */
  static clientAdminCountActiveEmployee(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/CAM/CAM1201/countactiveemployee";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Get Entity by client id
   */
  static clientAdminGetAllClientEntity(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
      /**  */
      data?: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/CAM/CAM1201/getall";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
        Data: params["data"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Get Entity by client id and entity id
   */
  static clientAdminGetEntityById(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
      /**  */
      data?: string;
      /**  */
      clientId: number;
      /**  */
      entityId: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/CAM/CAM1201/getentity/{clientId}/{entityId}";
      url = url.replace("{clientId}", params["clientId"] + "");
      url = url.replace("{entityId}", params["entityId"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
        Data: params["data"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Create entity
   */
  static clientAdminCreateEntity(
    params: {
      /** requestBody */
      body?: TbClientEntityCreateDtoApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/CAM/CAM1202/create";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Manage entity
   */
  static clientAdminManageEntity(
    params: {
      /** requestBody */
      body?: TbClientEntityManageDtoApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/CAM/CAM1202/manage";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Upload entity logo by entity id
   */
  static clientAdminEntityLogoUpload(
    params: {
      /**  */
      id: number;
      /**  */
      file: any;
      /**  */
      entityCode: string;
      /**  */
      fileCategory: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/CAM/CAM1202/logoupload/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "post",
        "multipart/form-data",
        url,
        options
      );

      let data = null;
      data = new FormData();
      if (params["file"]) {
        if (
          Object.prototype.toString.call(params["file"]) === "[object Array]"
        ) {
          for (const item of params["file"]) {
            data.append("File", item as any);
          }
        } else {
          data.append("File", params["file"] as any);
        }
      }

      if (params["entityCode"]) {
        if (
          Object.prototype.toString.call(params["entityCode"]) ===
          "[object Array]"
        ) {
          for (const item of params["entityCode"]) {
            data.append("EntityCode", item as any);
          }
        } else {
          data.append("EntityCode", params["entityCode"] as any);
        }
      }

      if (params["fileCategory"]) {
        if (
          Object.prototype.toString.call(params["fileCategory"]) ===
          "[object Array]"
        ) {
          for (const item of params["fileCategory"]) {
            data.append("FileCategory", item as any);
          }
        } else {
          data.append("FileCategory", params["fileCategory"] as any);
        }
      }

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Download entity logo by entity id
   */
  static clientAdminEntityLogo(
    params: {
      /**  */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/CAM/CAM1201/logo/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class CostCenterService {
  /**
   *
   */
  static costCenterGetFilter(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
      /**  */
      dataSort?: string;
      /**  */
      dataPage?: number;
      /**  */
      dataSize?: number;
      /**  */
      dataFilter?: string;
      /**  */
      dataFullTextSearch?: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/CAM/CAM0701/CostCenter";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
        "Data.Sort": params["dataSort"],
        "Data.Page": params["dataPage"],
        "Data.Size": params["dataSize"],
        "Data.Filter": params["dataFilter"],
        "Data.FullTextSearch": params["dataFullTextSearch"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Get a specific cost center which id is {id}
   */
  static costCenterGetById(
    params: {
      /** id of a cost center */
      id: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/CAM/CAM0701/CostCenter/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Create a new cost center
   */
  static costCenterCreate(
    params: {
      /** requestBody */
      body?: CreateCostCenterRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/CAM/CAM0702/CostCenter";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Update a cost center
   */
  static costCenterUpdate(
    params: {
      /** id of the cost center which is updated */
      id: number;
      /** requestBody */
      body?: UpdateCostCenterRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/CAM/CAM0703/CostCenter/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "put",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class EmpDataService {
  /**
   * Get employee's brief profile information
   */
  static empDataProfile(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/EMP/EMP5001/Employees/profile/EmpData";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Get employee's all payslips' records
   */
  static empDataGetAllPayslips(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/EMP/EMP5001/Employees/payslips/EmpData";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Get employee's full basic information
   */
  static empDataGetBasicInfo(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/EMP/EMP5001/Employees/BasicInfo/EmpData";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Get employee's full dependent information
   */
  static empDataGetDependentByEmployeeId(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<DependentViewModelListApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/EMP/EMP5001/Employees/Dependent/EmpData";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Get employee's full bank account information
   */
  static empDataGetBankAccountByEmployeeId(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<BankAccountDtoApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/EMP/EMP5001/Employees/BankAccount/EmpData";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Get employee's full employment status
   */
  static empDataGetEmploymentStatusByEmployeeId(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<EmploymentStatusDtoListApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/EMP/EMP5001/Employees/EmploymentStatus/EmpData";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Get employee's full rental address
   */
  static empDataGetRentalAddress(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/EMP/EMP5001/Employees/RentalAddress/EmpData";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Get employee's full tax
   */
  static empDataGetTaxInfo(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/EMP/EMP5001/Employees/Tax/EmpData";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Get employee's full tax return
   */
  static empDataGetAllTaxReturn(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/EMP/EMP5001/Employees/TaxReturn/EmpData";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Get employee's contract
   */
  static empDataGetEmploymentContractByEmployeeId(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<EmploymentContractViewModelListApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/EMP/EMP5001/Employees/Contract/EmpData";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Get employee's movement
   */
  static empDataGetMovementByEmploymentId(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<MovementViewModelListApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/EMP/EMP5001/Employees/Movement/EmpData";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class EmployeeService {
  /**
   *
   */
  static employeeNewHire(
    params: {
      /** requestBody */
      body?: NewHireRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/EMP/EMP0602/Employees/NewHire";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeGetAll(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
      /**  */
      dataSort?: string;
      /**  */
      dataPage?: number;
      /**  */
      dataSize?: number;
      /**  */
      dataFilter?: string;
      /**  */
      dataFullTextSearch?: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/EMP/EMP0601/Employees";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
        "Data.Sort": params["dataSort"],
        "Data.Page": params["dataPage"],
        "Data.Size": params["dataSize"],
        "Data.Filter": params["dataFilter"],
        "Data.FullTextSearch": params["dataFullTextSearch"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeGetProfile(
    params: {
      /**  */
      employeeId: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/EMP/EMP0601/Employees/{employeeId}/Profile";
      url = url.replace("{employeeId}", params["employeeId"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeGetCompanyInfo(
    params: {
      /**  */
      employeeId: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/EMP/EMP0601/Employees/{employeeId}/CompanyInfo";
      url = url.replace("{employeeId}", params["employeeId"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeSaveCompanyInfo(
    params: {
      /**  */
      employeeId: number;
      /** requestBody */
      body?: CompanyInformationApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/EMP/EMP0602/Employees/{employeeId}/CompanyInfo";
      url = url.replace("{employeeId}", params["employeeId"] + "");

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeGetBasicInfo(
    params: {
      /**  */
      employeeId: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/EMP/EMP0601/Employees/{employeeId}/BasicInfo";
      url = url.replace("{employeeId}", params["employeeId"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeSaveBasicInfo(
    params: {
      /**  */
      employeeId: number;
      /** requestBody */
      body?: EmployeeSaveModelApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/EMP/EMP0606/Employees/{employeeId}/BasicInfo";
      url = url.replace("{employeeId}", params["employeeId"] + "");

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeGetTaxInfo(
    params: {
      /**  */
      employeeId: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/EMP/EMP0601/Employees/{employeeId}/Tax";
      url = url.replace("{employeeId}", params["employeeId"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeSaveTaxInfo(
    params: {
      /**  */
      employeeId: number;
      /** requestBody */
      body?: TaxSaveModelApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/EMP/EMP0606/Employees/{employeeId}/Tax";
      url = url.replace("{employeeId}", params["employeeId"] + "");

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeGetMovementByEmploymentId(
    params: {
      /**  */
      employmentId: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<MovementViewModelListApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath +
        "/v1.0/empl/EMP/EMP0601/Employees/Employments/{employmentId}/Movement";
      url = url.replace("{employmentId}", params["employmentId"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeGetMovementById(
    params: {
      /**  */
      employmentId: number;
      /**  */
      id: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<MovementViewModelApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath +
        "/v1.0/empl/EMP/EMP0601/Employees/Employments/{employmentId}/Movement/{id}";
      url = url.replace("{employmentId}", params["employmentId"] + "");
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeSaveMovement(
    params: {
      /**  */
      employmentId: number;
      /** requestBody */
      body?: MovementModelListApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<MovementViewModelApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath +
        "/v1.0/empl/EMP/EMP0606/Employees/Employments/{employmentId}/Movement";
      url = url.replace("{employmentId}", params["employmentId"] + "");

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeGetRentalAddress(
    params: {
      /**  */
      employeeId: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url =
        basePath +
        "/v1.0/empl/EMP/EMP0601/Employees/{employeeId}/RentalAddress";
      url = url.replace("{employeeId}", params["employeeId"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeSaveRentalAddress(
    params: {
      /**  */
      employeeId: number;
      /** requestBody */
      body?: RentalAddressSaveModelApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url =
        basePath +
        "/v1.0/empl/EMP/EMP0606/Employees/{employeeId}/RentalAddress";
      url = url.replace("{employeeId}", params["employeeId"] + "");

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeGetDependentByEmployeeId(
    params: {
      /**  */
      employeeId: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<DependentViewModelListApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/EMP/EMP0601/Employees/{employeeId}/Dependent";
      url = url.replace("{employeeId}", params["employeeId"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeGetDependentById(
    params: {
      /**  */
      employeeId: number;
      /**  */
      id: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<DependentViewModelListApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath +
        "/v1.0/empl/EMP/EMP0601/Employees/{employeeId}/Dependent/{id}";
      url = url.replace("{employeeId}", params["employeeId"] + "");
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeSaveDependent(
    params: {
      /**  */
      employeeId: number;
      /** requestBody */
      body?: DependentSaveModelApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<DependentViewModelApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/EMP/EMP0606/Employees/{employeeId}/Dependent";
      url = url.replace("{employeeId}", params["employeeId"] + "");

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeGetBankAccountByEmployeeId(
    params: {
      /**  */
      employeeId: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<BankAccountDtoApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/EMP/EMP0601/Employees/{employeeId}/BankAccount";
      url = url.replace("{employeeId}", params["employeeId"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeGetBankAccountById(
    params: {
      /**  */
      employeeId: number;
      /**  */
      id: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<BankAccountViewModelListApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath +
        "/v1.0/empl/EMP/EMP0601/Employees/{employeeId}/BankAccount/{id}";
      url = url.replace("{employeeId}", params["employeeId"] + "");
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeSaveBankAccount(
    params: {
      /**  */
      employeeId: number;
      /** requestBody */
      body?: BankAccountSaveModelApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<BankAccountViewModelApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/EMP/EMP0606/Employees/{employeeId}/BankAccount";
      url = url.replace("{employeeId}", params["employeeId"] + "");

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeGetEmploymentByEmployeeId(
    params: {
      /**  */
      employeeId: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<EmploymentContractViewModelListApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/EMP/EMP0601/Employees/{employeeId}/Employment";
      url = url.replace("{employeeId}", params["employeeId"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeGetEmploymentById(
    params: {
      /**  */
      employeeId: number;
      /**  */
      id: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<EmploymentContractDtoApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath +
        "/v1.0/empl/EMP/EMP0601/Employees/{employeeId}/Employment/{id}";
      url = url.replace("{employeeId}", params["employeeId"] + "");
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeGetContractNew(
    params: {
      /**  */
      employeeId: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<EmploymentContractViewModelListApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/EMP/EMP0601/Employees/{employeeId}/ContractNew";
      url = url.replace("{employeeId}", params["employeeId"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeSaveEmployment(
    params: {
      /**  */
      employeeId: number;
      /** requestBody */
      body?: EmploymentContractSaveModelApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<EmploymentContractDtoApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/EMP/EMP0606/Employees/{employeeId}/Employment";
      url = url.replace("{employeeId}", params["employeeId"] + "");

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeCreateEmployment(
    params: {
      /**  */
      employeeId: number;
      /** requestBody */
      body?: EmploymentContractAddModelApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<EmploymentContractDtoApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/EMP/EMP0602/Employees/{employeeId}/Employment";
      url = url.replace("{employeeId}", params["employeeId"] + "");

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeGetEmploymentStatusByEmployeeId(
    params: {
      /**  */
      employeeId: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<EmploymentStatusDtoListApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath +
        "/v1.0/empl/EMP/EMP0601/Employees/{employeeId}/EmploymentStatus";
      url = url.replace("{employeeId}", params["employeeId"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeGetEmploymentStatusById(
    params: {
      /**  */
      id: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<EmploymentStatusViewModelApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/EMP/EMP0601/Employees/EmploymentStatus/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeDeleteEmploymentStatus(
    params: {
      /**  */
      employeeId: number;
      /**  */
      id: number;
      /** requestBody */
      body?: ApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<EmploymentStatusDtoListApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath +
        "/v1.0/empl/EMP/EMP0604/Employees/{employeeId}/EmploymentStatus/{id}";
      url = url.replace("{employeeId}", params["employeeId"] + "");
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "delete",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeSaveEmploymentStatus(
    params: {
      /**  */
      employeeId: number;
      /** requestBody */
      body?: EmploymentStatusSaveModelApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<EmploymentContractDtoApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath +
        "/v1.0/empl/EMP/EMP0606/Employees/{employeeId}/EmploymentStatus";
      url = url.replace("{employeeId}", params["employeeId"] + "");

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeGetStatusConfirmation(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
      /**  */
      dataSort?: string;
      /**  */
      dataPage?: number;
      /**  */
      dataSize?: number;
      /**  */
      dataFilter?: string;
      /**  */
      dataFullTextSearch?: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/EMP/EMP0611/Employees/StatusConfirmation";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
        "Data.Sort": params["dataSort"],
        "Data.Page": params["dataPage"],
        "Data.Size": params["dataSize"],
        "Data.Filter": params["dataFilter"],
        "Data.FullTextSearch": params["dataFullTextSearch"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeUpdateStatusConfirm(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: ApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<Int32NullableApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/EMP/EMP0603/Employees/StatusConfirmation/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "put",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeGetTerminationById(
    params: {
      /**  */
      id: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<TerminationViewModelApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath +
        "/v1.0/empl/EMP/EMP0601/Employees/Employment/Termination/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeGetTerminationByEmploymentId(
    params: {
      /**  */
      employmentId: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<TerminationViewModelApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath +
        "/v1.0/empl/EMP/EMP0601/Employees/Employment/{employmentId}/Termination";
      url = url.replace("{employmentId}", params["employmentId"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static employeeSaveTermination(
    params: {
      /** requestBody */
      body?: TerminationSaveModelApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<TerminationViewModelApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/EMP/EMP0606/Employees/Employment/Termination";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class EmplPublicInfoService {
  /**
   * Get public information of the application
   */
  static emplPublicInfoPublicInfo(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PublicInfoDtoApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/COM/COM9901/EmplPublicInfo";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Create Message Template
   */
  static emplPublicInfoCreateMessageTemplate(
    params: {
      /** requestBody */
      body?: MessageTemplateCreateDtoApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/COM/COM9902/MesageTemplate/EmplPublicInfo";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Create Message Template Language
   */
  static emplPublicInfoCreateMessageTemplateLang(
    params: {
      /** requestBody */
      body?: MessageTemplateLangCreateDtoApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/COM/COM9902/MessageTemplateLang/EmplPublicInfo";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Create Message
   */
  static emplPublicInfoCreateMessage(
    params: {
      /** requestBody */
      body?: MessageCreateDtoApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/COM/COM9902/Message/EmplPublicInfo";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Update Message Template
   */
  static emplPublicInfoUpdateMessageTemplate(
    params: {
      /** requestBody */
      body?: MessageTemplateUpdateDtoApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/COM/COM9903/MesageTemplate/EmplPublicInfo";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Update Message Template Language
   */
  static emplPublicInfoUpdateMessageTemplateLang(
    params: {
      /** requestBody */
      body?: MessageTemplateLangUpdateDtoApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/COM/COM9903/MessageTemplateLang/EmplPublicInfo";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Update Message
   */
  static emplPublicInfoUpdateMessage(
    params: {
      /** requestBody */
      body?: MessageUpdateDtoApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/COM/COM9903/Message/EmplPublicInfo";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Send Email
   */
  static emplPublicInfoSendMail(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/COM/COM9901/sendMail/EmplPublicInfo";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class EmpPortalConfigService {
  /**
   *
   */
  static empPortalConfigGetConfigInfo(
    params: {
      /**  */
      entityCode: string;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/EMP/EMP5001/Config/{entityCode}";
      url = url.replace("{entityCode}", params["entityCode"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Get Support
   */
  static empPortalConfigGetSupport(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/EMP/EMP5001/Support";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Send Email
   */
  static empPortalConfigSendEmail(
    params: {
      /**  */
      entityId?: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/EMP/EMP5001/SendEmail";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        EntityId: params["entityId"],
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class EmpPortalFileService {
  /**
   * Download payslip file
   */
  static empPortalFileDownloadFile(
    params: {
      /**  */
      id: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/EMP/PAY5001/Files/payslip/{id}/EmpPortalFile";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Download payslip zip file
   */
  static empPortalFileGetFilesAsZip(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
      /**  */
      data?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/EMP/PAY5001/Files/payslip/zip/EmpPortalFile";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
        Data: params["data"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Download tax return file
   */
  static empPortalFileDownloadTaxReturnFile(
    params: {
      /**  */
      id: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/EMP/PAY5101/Files/taxreturn/{id}/EmpPortalFile";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Download tax return zip file
   */
  static empPortalFileGetTaxReturnFilesAsZip(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
      /**  */
      data?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/EMP/PAY5101/Files/taxreturn/zip/EmpPortalFile";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
        Data: params["data"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class FileService {
  /**
   *
   */
  static fileDownloadFile(
    params: {
      /**  */
      id: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/COM/COM0401/File/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static fileGetListFileById(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
      /**  */
      data?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<FileViewModelListApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/COM/COM0401/File";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
        Data: params["data"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class JobGradeService {
  /**
   *
   */
  static jobGradeGetFilter(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
      /**  */
      dataSort?: string;
      /**  */
      dataPage?: number;
      /**  */
      dataSize?: number;
      /**  */
      dataFilter?: string;
      /**  */
      dataFullTextSearch?: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/CAM/CAM0601/JobGrade";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
        "Data.Sort": params["dataSort"],
        "Data.Page": params["dataPage"],
        "Data.Size": params["dataSize"],
        "Data.Filter": params["dataFilter"],
        "Data.FullTextSearch": params["dataFullTextSearch"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Get a specific job grade which id is {id}
   */
  static jobGradeGetById(
    params: {
      /**  */
      id: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/CAM/CAM0601/JobGrade/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Create a new job grade
   */
  static jobGradeCreate(
    params: {
      /** requestBody */
      body?: CreateJobGradeRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/CAM/CAM0602/JobGrade";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Update a job grade
   */
  static jobGradeUpdate(
    params: {
      /** id of the job grade which is updated */
      id: number;
      /** requestBody */
      body?: UpdateJobGradeRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/CAM/CAM0603/JobGrade/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "put",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class OrganizationStructureService {
  /**
   *
   */
  static organizationStructureGetAll(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
      /**  */
      dataSort?: string;
      /**  */
      dataPage?: number;
      /**  */
      dataSize?: number;
      /**  */
      dataFilter?: string;
      /**  */
      dataFullTextSearch?: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<OrganizationStructureAllModelApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/CAM/CAM0801/OrganizationStructure";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
        "Data.Sort": params["dataSort"],
        "Data.Page": params["dataPage"],
        "Data.Size": params["dataSize"],
        "Data.Filter": params["dataFilter"],
        "Data.FullTextSearch": params["dataFullTextSearch"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static organizationStructureCreate(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<OrganizationStructureAddModel> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/CAM/CAM0801/OrganizationStructure/Create";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static organizationStructureEdit(
    params: {
      /**  */
      unitId: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/CAM/CAM0801/OrganizationStructure/Edit/{unitId}";
      url = url.replace("{unitId}", params["unitId"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static organizationStructureStore(
    params: {
      /** requestBody */
      body?: OrganizationStructureStoreModelApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<Int32ApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/CAM/CAM0802/OrganizationStructure";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static organizationStructureUpdate(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: OrganizationStructureUpdateModelApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<Int32ApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/CAM/CAM0803/OrganizationStructure/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "put",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static organizationStructureGetOrganizationStructureAsTree(
    params: {
      /**  */
      darId: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<OrganizationStructureDataAccessGroupViewModelApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath +
        "/v1.0/empl/CAM/CFG0501/OrganizationStructure/dataaccess/{darId}";
      url = url.replace("{darId}", params["darId"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class WorkCalendarService {
  /**
   *
   */
  static workCalendarGetFilter(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
      /**  */
      dataSort?: string;
      /**  */
      dataPage?: number;
      /**  */
      dataSize?: number;
      /**  */
      dataFilter?: string;
      /**  */
      dataFullTextSearch?: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<WorkCalendarViewModelPaginationApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/CAM/CAM1001/WorkCalendar/ClientEntity";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
        "Data.Sort": params["dataSort"],
        "Data.Page": params["dataPage"],
        "Data.Size": params["dataSize"],
        "Data.Filter": params["dataFilter"],
        "Data.FullTextSearch": params["dataFullTextSearch"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static workCalendarGetById(
    params: {
      /**  */
      id: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<WorkCalendarViewModelApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/CAM/CAM1001/WorkCalendar/ClientEntity/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static workCalendarDeleteGlobalCalendar(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: ApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<WorkCalendarViewModelApiResponse> {
    return new Promise((resolve, reject) => {
      let url =
        basePath + "/v1.0/empl/CAM/CAM1004/WorkCalendar/ClientEntity/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "delete",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static workCalendarSave(
    params: {
      /** requestBody */
      body?: WorkCalendarDtoApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<WorkCalendarViewModelApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/CAM/CAM1006/WorkCalendar/ClientEntity";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static workCalendarDeleteHoliday(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: ApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<BooleanNullableApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/CAM/CAM1004/WorkCalendar/Holiday/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "delete",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class WorkingScheduleService {
  /**
   *
   */
  static workingScheduleGetAll(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<TbCfgWorkingScheduleListApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/empl/CAM/CAM901/WorkingSchedule";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export interface StdRequestParam {
  /**  */
  locale?: string;

  /**  */
  clientId?: number;

  /**  */
  entityId?: number;

  /**  */
  genericCodeList?: string[];
}

export interface TbClientCreateDto {
  /**  */
  clientNameTxt?: string;

  /**  */
  clientCode?: string;

  /**  */
  contactPerson?: string;

  /**  */
  statusCd?: string;

  /**  */
  remarks?: string;

  /**  */
  internalCode?: string;
}

export interface TbClientCreateDtoApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: TbClientCreateDto;
}

export interface TbClientUpdateDto {
  /**  */
  clientId?: number;

  /**  */
  clientNameTxt?: string;

  /**  */
  clientCode?: string;

  /**  */
  contactPerson?: string;

  /**  */
  statusCd?: string;

  /**  */
  remarks?: string;

  /**  */
  internalCode?: string;
}

export interface TbClientUpdateDtoApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: TbClientUpdateDto;
}

export interface TbClientEntityCreateDto {
  /**  */
  entityNameTxt?: string;

  /**  */
  entityDisplayName?: string;

  /**  */
  entityCode?: string;

  /**  */
  countryCd?: string;

  /**  */
  remarks?: string;

  /**  */
  internalCode?: string;
}

export interface TbClientEntityCreateDtoApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: TbClientEntityCreateDto;
}

export interface TbCamEntityFaqUpdateDto {
  /**  */
  faqId?: number;

  /**  */
  entityId?: number;

  /**  */
  question?: string;

  /**  */
  answer?: string;

  /**  */
  faqOrder?: number;
}

export interface TbCamEntitySupportUpdateDto {
  /**  */
  createdDt?: string;

  /**  */
  supportId?: number;

  /**  */
  entityId?: number;

  /**  */
  supportContactName?: string;

  /**  */
  supportPhoneNumber?: string;

  /**  */
  supportEmail?: string;

  /**  */
  remark?: string;
}

export interface TbClientEntityManageDto {
  /**  */
  entityId?: number;

  /**  */
  parentClientId?: number;

  /**  */
  entityNameTxt?: string;

  /**  */
  entityDisplayName?: string;

  /**  */
  entityCode?: string;

  /**  */
  countryCd?: string;

  /**  */
  remarks?: string;

  /**  */
  dbHost?: string;

  /**  */
  dbName?: string;

  /**  */
  dbPort?: number;

  /**  */
  connStringParam?: string;

  /**  */
  statusCd?: string;

  /**  */
  createdDt?: Date;

  /**  */
  createdBy?: string;

  /**  */
  modifiedDt?: Date;

  /**  */
  modifiedBy?: string;

  /**  */
  businessAddressLine1?: string;

  /**  */
  businessAddressLine2?: string;

  /**  */
  businessAddressLine3?: string;

  /**  */
  businessPhone?: string;

  /**  */
  businessFax?: string;

  /**  */
  contactAddressLine1?: string;

  /**  */
  contactAddressLine2?: string;

  /**  */
  contactAddressLine3?: string;

  /**  */
  contactPhone?: string;

  /**  */
  contactFax?: string;

  /**  */
  contactName?: string;

  /**  */
  contactEmail?: string;

  /**  */
  taxId?: string;

  /**  */
  businessPhoneAreaCd?: string;

  /**  */
  businessPhoneFaxAreaCd?: string;

  /**  */
  contactPhoneAreaCd?: string;

  /**  */
  contactFaxAreaCd?: string;

  /**  */
  internalCode?: string;

  /**  */
  entityNameTxtSecondary?: string;

  /**  */
  entityDisplayNameSecondary?: string;

  /**  */
  businessAddressLine1Secondary?: string;

  /**  */
  businessAddressLine2Secondary?: string;

  /**  */
  businessAddressLine3Secondary?: string;

  /**  */
  contactAddressLine1Secondary?: string;

  /**  */
  contactAddressLine2Secondary?: string;

  /**  */
  contactAddressLine3Secondary?: string;

  /**  */
  faq?: TbCamEntityFaqUpdateDto[];

  /**  */
  support?: TbCamEntitySupportUpdateDto[];
}

export interface TbClientEntityManageDtoApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: TbClientEntityManageDto;
}

export interface MessageItem {
  /**  */
  componentId?: string;

  /**  */
  level?: string;

  /**  */
  labelCode?: string;

  /**  */
  placeholder?: string[];
}

export interface Message {
  /**  */
  toast?: MessageItem[];

  /**  */
  alert?: MessageItem[];

  /**  */
  validateResult?: MessageItem[];
}

export interface StringObjectDictionaryApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: object;

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface CreateCostCenterRequest {
  /**  */
  name?: string;

  /**  */
  code?: string;

  /**  */
  remark?: string;

  /**  */
  status?: string;
}

export interface CreateCostCenterRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: CreateCostCenterRequest;
}

export interface ProblemDetails {
  /**  */
  type?: string;

  /**  */
  title?: string;

  /**  */
  status?: number;

  /**  */
  detail?: string;

  /**  */
  instance?: string;
}

export interface UpdateCostCenterRequest {
  /**  */
  name?: string;

  /**  */
  code?: string;

  /**  */
  remark?: string;

  /**  */
  status?: string;
}

export interface UpdateCostCenterRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: UpdateCostCenterRequest;
}

export interface DependentViewModel {
  /**  */
  dependentId?: number;

  /**  */
  employeeId?: number;

  /**  */
  firstNamePrimary?: string;

  /**  */
  firstNameSecondary?: string;

  /**  */
  middleNamePrimary?: string;

  /**  */
  middleNameSecondary?: string;

  /**  */
  christianNamePrimary?: string;

  /**  */
  christianNameSecondary?: string;

  /**  */
  lastNamePrimary?: string;

  /**  */
  lastNameSecondary?: string;

  /**  */
  gender?: string;

  /**  */
  relationshipToEmployee?: string;

  /**  */
  dateOfBirth?: Date;

  /**  */
  namePrefix?: string;

  /**  */
  citizenIdNo?: string;

  /**  */
  nationality?: string;

  /**  */
  passportIdNo?: string;

  /**  */
  countryOfIssue?: string;

  /**  */
  workVisaHolder?: boolean;

  /**  */
  visaIssueDate?: Date;

  /**  */
  visaLandingDate?: Date;

  /**  */
  remarks?: string;

  /**  */
  address?: string;

  /**  */
  relationshipOther?: string;

  /**  */
  createdBy?: string;

  /**  */
  createdDt?: Date;

  /**  */
  modifiedBy?: string;

  /**  */
  modifiedDt?: Date;

  /**  */
  marriageDate?: Date;
}

export interface DependentViewModelListApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: DependentViewModel[];

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface BankAccountViewModel {
  /**  */
  id?: number;

  /**  */
  eeBankAccountId?: string;

  /**  */
  employeeId?: number;

  /**  */
  accountName?: string;

  /**  */
  accountCurrency?: string;

  /**  */
  bankCode?: string;

  /**  */
  branchCode?: string;

  /**  */
  accountNo?: string;

  /**  */
  transactionReference?: string;

  /**  */
  defaultAccount?: boolean;

  /**  */
  remark?: string;

  /**  */
  createdBy?: string;

  /**  */
  createdDt?: Date;

  /**  */
  modifiedBy?: string;

  /**  */
  modifiedDt?: Date;
}

export interface BankAccountDto {
  /**  */
  remark?: string;

  /**  */
  bankAccounts?: BankAccountViewModel[];
}

export interface BankAccountDtoApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: BankAccountDto;

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface EmploymentStatusViewModel {
  /**  */
  employmentStatusId?: number;

  /**  */
  employmentContractId?: number;

  /**  */
  employmentContractName?: string;

  /**  */
  startDate?: Date;

  /**  */
  status?: string;

  /**  */
  remark?: string;

  /**  */
  confirmed?: boolean;

  /**  */
  createdDt?: Date;

  /**  */
  createdBy?: string;

  /**  */
  modifiedDt?: Date;

  /**  */
  modifiedBy?: string;
}

export interface EmploymentStatusItemModel {
  /**  */
  startDate?: Date;

  /**  */
  employmenStatuses?: EmploymentStatusViewModel[];
}

export interface EmploymentStatusDto {
  /**  */
  remark?: string;

  /**  */
  data?: EmploymentStatusItemModel[];
}

export interface EmploymentStatusDtoListApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: EmploymentStatusDto[];

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface MovementViewModel {
  /**  */
  assignmentId?: number;

  /**  */
  employmentContractId?: number;

  /**  */
  businessUnitId?: number;

  /**  */
  movementName?: string;

  /**  */
  internalJobTitle?: string;

  /**  */
  externalJobTitle?: string;

  /**  */
  jobGrade?: number;

  /**  */
  directManager?: number;

  /**  */
  costCenter?: number;

  /**  */
  assignmentStartDate?: Date;

  /**  */
  assignmentEndDate?: Date;

  /**  */
  remunerationPackage?: number;

  /**  */
  basicSalary?: number;

  /**  */
  baseSalaryUnit?: string;

  /**  */
  defaultSalaryCurrency?: string;

  /**  */
  defaultPaymentCurrency?: string;

  /**  */
  staffTags?: string;

  /**  */
  remarks?: string;

  /**  */
  notificationRequirements?: number;

  /**  */
  workCalendar?: number;

  /**  */
  createdBy?: string;

  /**  */
  createdDt?: Date;

  /**  */
  modifiedBy?: string;

  /**  */
  modifiedDt?: Date;
}

export interface TerminationViewModel {
  /**  */
  id?: number;

  /**  */
  employmentContractId?: number;

  /**  */
  terminationReasonPrimary?: string;

  /**  */
  terminationReasonAlternative?: string;

  /**  */
  terminationReasonMpf?: string;

  /**  */
  terminationCodeTax?: string;

  /**  */
  terminationContractEndDate?: Date;

  /**  */
  lastEmploymentDate?: Date;

  /**  */
  lastWorkingDate?: Date;

  /**  */
  notificationDate?: Date;

  /**  */
  yearServiceStartDate?: Date;

  /**  */
  yearServiceEndDate?: Date;

  /**  */
  leavingHongKong?: boolean;

  /**  */
  holdLastPayment?: boolean;

  /**  */
  remark?: string;

  /**  */
  createdBy?: string;

  /**  */
  createdDt?: Date;

  /**  */
  modifiedBy?: string;

  /**  */
  modifiedDt?: Date;
}

export interface EmploymentContractViewModel {
  /**  */
  employmentContractId?: number;

  /**  */
  employeeId?: number;

  /**  */
  employeeNo?: string;

  /**  */
  globalNo?: string;

  /**  */
  contractNo?: string;

  /**  */
  contractStartDate?: Date;

  /**  */
  contractEndDate?: Date;

  /**  */
  staffType?: string;

  /**  */
  remarks?: string;

  /**  */
  createdBy?: string;

  /**  */
  createdDt?: Date;

  /**  */
  modifiedBy?: string;

  /**  */
  modifiedDt?: Date;

  /**  */
  movements?: MovementViewModel[];

  /**  */
  terminations?: TerminationViewModel[];
}

export interface EmploymentContractViewModelListApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: EmploymentContractViewModel[];

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface MovementViewModelListApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: MovementViewModel[];

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface NewHireRequest {
  /**  */
  firstName?: string;

  /**  */
  middleName?: string;

  /**  */
  lastName?: string;

  /**  */
  displayName?: string;

  /**  */
  staffType?: string;

  /**  */
  businessUnitId?: number;

  /**  */
  contractNo?: string;

  /**  */
  workCalendar?: number;

  /**  */
  defaultSalaryCurrency?: string;

  /**  */
  defaultPaymentCurrency?: string;

  /**  */
  expectedStartDate?: Date;

  /**  */
  probation?: boolean;

  /**  */
  expectedProbationEndDate?: Date;

  /**  */
  acceptingTheOffer?: Date;

  /**  */
  remark?: string;

  /**  */
  photoId?: number;
}

export interface NewHireRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: NewHireRequest;
}

export interface CompanyInformation {
  /**  */
  companyCode?: string;

  /**  */
  clientId?: number;

  /**  */
  clientName?: string;

  /**  */
  companyOfficialName?: string;

  /**  */
  companyDisplayName?: string;

  /**  */
  country?: string;

  /**  */
  functionalGroupFullName?: string;

  /**  */
  functionalGroupDisplayName?: string;

  /**  */
  parentId?: number;

  /**  */
  remarkCompany?: string;
}

export interface CompanyInformationApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: CompanyInformation;
}

export interface EmployeeRequest {
  /**  */
  employeeId?: number;

  /**  */
  firstNamePrimary?: string;

  /**  */
  firstNameSecondary?: string;

  /**  */
  middleNamePrimary?: string;

  /**  */
  middleNameSecondary?: string;

  /**  */
  lastNamePrimary?: string;

  /**  */
  lastNameSecondary?: string;

  /**  */
  christianNamePrimary?: string;

  /**  */
  christianNameSecondary?: string;

  /**  */
  displayNamePrimary?: string;

  /**  */
  displayNameSecondary?: string;

  /**  */
  namePrefix?: string;

  /**  */
  gender?: string;

  /**  */
  photo?: number;

  /**  */
  dateOfBirth?: Date;

  /**  */
  age?: number;

  /**  */
  martialStatus?: string;

  /**  */
  marriageDate?: Date;

  /**  */
  preferredLanguage?: string;

  /**  */
  citizenIdNo?: string;

  /**  */
  nationality?: string;

  /**  */
  passportIdNo?: string;

  /**  */
  countryOfIssue?: string;

  /**  */
  workVisaHolder?: boolean;

  /**  */
  workVisaExpiryDate?: Date;

  /**  */
  workArrivingDate?: Date;

  /**  */
  workVisaIssueDate?: Date;

  /**  */
  workVisaLandingDate?: Date;

  /**  */
  homePhone?: string;

  /**  */
  officePhone?: string;

  /**  */
  mobilePhone?: string;

  /**  */
  workEmail?: string;

  /**  */
  personalEmail?: string;

  /**  */
  residentialAddress1Primary?: string;

  /**  */
  residentialAddress1Secondary?: string;

  /**  */
  residentialAddress2Primary?: string;

  /**  */
  residentialAddress2Secondary?: string;

  /**  */
  residentialAddress3Primary?: string;

  /**  */
  residentialAddress3Secondary?: string;

  /**  */
  residentialDistrictPrimary?: string;

  /**  */
  residentialDistrictSecondary?: string;

  /**  */
  residentialCityPrimary?: string;

  /**  */
  residentialCitySecondary?: string;

  /**  */
  residentialStatePrimary?: string;

  /**  */
  residentialStateSecondary?: string;

  /**  */
  residentialCountry?: string;

  /**  */
  residentialPostalCode?: string;

  /**  */
  residentialHongkongTaxForm?: string;

  /**  */
  postalAddress1Primary?: string;

  /**  */
  postalAddress1Secondary?: string;

  /**  */
  postalAddress2Primary?: string;

  /**  */
  postalAddress2Secondary?: string;

  /**  */
  postalAddress3Primary?: string;

  /**  */
  postalAddress3Secondary?: string;

  /**  */
  postalDistrictPrimary?: string;

  /**  */
  postalDistrictSecondary?: string;

  /**  */
  postalCityPrimary?: string;

  /**  */
  postalCitySecondary?: string;

  /**  */
  postalStatePrimary?: string;

  /**  */
  postalStateSecondary?: string;

  /**  */
  postalCountry?: string;

  /**  */
  postalPostalCode?: string;

  /**  */
  postalHongkongTaxForm?: string;

  /**  */
  remarks?: string;

  /**  */
  remarkContact?: string;

  /**  */
  remarkEmploymentContract?: string;
}

export interface AttachmentRequest {
  /**  */
  attachmentId?: number;

  /**  */
  attachmentFileId?: number;

  /**  */
  attachmentFileName?: string;

  /**  */
  attachmentFileRemark?: string;

  /**  */
  isDeleted?: boolean;
}

export interface EmergencyContactRequest {
  /**  */
  emergencyContactPersonId?: number;

  /**  */
  relationshipToEmployee?: string;

  /**  */
  relationshipOther?: string;

  /**  */
  name?: string;

  /**  */
  mobilePhone?: string;
}

export interface EmployeeSaveModel {
  /**  */
  employee?: EmployeeRequest;

  /**  */
  attachments?: AttachmentRequest[];

  /**  */
  emergencyContacts?: EmergencyContactRequest[];
}

export interface EmployeeSaveModelApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: EmployeeSaveModel;
}

export interface TaxRequest {
  /**  */
  taxId?: number;

  /**  */
  employmentContractId?: number;

  /**  */
  overseasPayment?: boolean;

  /**  */
  companyName?: string;

  /**  */
  address?: string;

  /**  */
  currency?: string;

  /**  */
  amount?: string;

  /**  */
  leavingHkAfterTermination?: boolean;

  /**  */
  holdLastPayment?: boolean;

  /**  */
  ir56bRemarks?: string;

  /**  */
  ir56eMonthlyRateOfAllownace?: string;

  /**  */
  ir56eFlutuatingEmoluments?: string;

  /**  */
  ir56ePreviousEmployerName?: string;

  /**  */
  ir56ePreviousEmployerAddress?: string;

  /**  */
  ir56eShareOptionGranted?: boolean;

  /**  */
  ir56fNewEmployerName?: string;

  /**  */
  ir56fNewEmployerAddress?: string;

  /**  */
  ir56fAddressAfterTermination?: string;

  /**  */
  ir56gDepartureDate?: Date;

  /**  */
  ir56gReasonForDeparture?: string;

  /**  */
  ir56gReasonForDepartureOthers?: string;

  /**  */
  ir56gEmployeeReturningHk?: boolean;

  /**  */
  ir56gEmployeeReturningHkDate?: Date;

  /**  */
  ir56gAddressAfterTermination?: string;

  /**  */
  ir56gEmployerHasHoldMoney?: boolean;

  /**  */
  ir56gHeldAmount?: number;

  /**  */
  ir56gReasonForNotHoldingMoney?: string;

  /**  */
  ir56gTaxBorneByEmployer?: boolean;

  /**  */
  ir56gShareOptionGranted?: boolean;

  /**  */
  ir56gGrantedDate?: Date;

  /**  */
  ir56gNoOfSharesNotYetExercised?: string;

  /**  */
  remarks?: string;
}

export interface OverseasPayment {
  /**  */
  overseasPayments?: boolean;

  /**  */
  companyName?: string;

  /**  */
  address?: string;

  /**  */
  amount?: string;

  /**  */
  currency?: string;
}

export interface TaxSaveModel {
  /**  */
  taxRequests?: TaxRequest[];

  /**  */
  overseasPayment?: OverseasPayment;

  /**  */
  remarkTax?: string;
}

export interface TaxSaveModelApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: TaxSaveModel;
}

export interface MovementViewModelApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: MovementViewModel;

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface MovementModel {
  /**  */
  assignmentId?: number;

  /**  */
  employmentContractId?: number;

  /**  */
  businessUnitId?: number;

  /**  */
  movementName?: string;

  /**  */
  internalJobTitle?: string;

  /**  */
  externalJobTitle?: string;

  /**  */
  jobGrade?: number;

  /**  */
  directManager?: number;

  /**  */
  costCenter?: number;

  /**  */
  assignmentStartDate?: Date;

  /**  */
  remunerationPackage?: number;

  /**  */
  basicSalary?: number;

  /**  */
  baseSalaryUnit?: string;

  /**  */
  defaultSalaryCurrency?: string;

  /**  */
  defaultPaymentCurrency?: string;

  /**  */
  staffTags?: string;

  /**  */
  remarks?: string;

  /**  */
  specificEntityId?: number;

  /**  */
  notificationRequirements?: number;

  /**  */
  workCalendar?: number;
}

export interface MovementModelListApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: MovementModel[];
}

export interface RentalAddressRequest {
  /**  */
  addressId?: number;

  /**  */
  nature?: string;

  /**  */
  address?: string;

  /**  */
  startDate?: Date;

  /**  */
  endDate?: Date;

  /**  */
  rentPaidToLandlordByEmployer?: number;

  /**  */
  rentPaidToLandlordByEmployee?: number;

  /**  */
  rentRefundedToEmployeeByEmployer?: number;

  /**  */
  rentPaidToEmployerByEmployee?: number;
}

export interface RentalAddressSaveModel {
  /**  */
  rentalAddresses?: RentalAddressRequest[];

  /**  */
  remarkRentalAddress?: string;
}

export interface RentalAddressSaveModelApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: RentalAddressSaveModel;
}

export interface DependentModel {
  /**  */
  dependentId?: number;

  /**  */
  firstNamePrimary?: string;

  /**  */
  firstNameSecondary?: string;

  /**  */
  middleNamePrimary?: string;

  /**  */
  middleNameSecondary?: string;

  /**  */
  christianNamePrimary?: string;

  /**  */
  christianNameSecondary?: string;

  /**  */
  lastNamePrimary?: string;

  /**  */
  lastNameSecondary?: string;

  /**  */
  gender?: string;

  /**  */
  relationshipToEmployee?: string;

  /**  */
  dateOfBirth?: Date;

  /**  */
  namePrefix?: string;

  /**  */
  citizenIdNo?: string;

  /**  */
  nationality?: string;

  /**  */
  passportIdNo?: string;

  /**  */
  countryOfIssue?: string;

  /**  */
  workVisaHolder?: boolean;

  /**  */
  remarks?: string;

  /**  */
  address?: string;

  /**  */
  relationshipOther?: string;
}

export interface DependentSaveModel {
  /**  */
  remark?: string;

  /**  */
  dependents?: DependentModel[];
}

export interface DependentSaveModelApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: DependentSaveModel;
}

export interface DependentViewModelApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: DependentViewModel;

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface BankAccountViewModelListApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: BankAccountViewModel[];

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface BankAccountModel {
  /**  */
  id?: number;

  /**  */
  eeBankAccountId?: string;

  /**  */
  accountName?: string;

  /**  */
  accountCurrency?: string;

  /**  */
  bankCode?: string;

  /**  */
  branchCode?: string;

  /**  */
  accountNo?: string;

  /**  */
  transactionReference?: string;

  /**  */
  defaultAccount?: boolean;

  /**  */
  remark?: string;
}

export interface BankAccountSaveModel {
  /**  */
  remark?: string;

  /**  */
  bankAccounts?: BankAccountModel[];
}

export interface BankAccountSaveModelApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: BankAccountSaveModel;
}

export interface BankAccountViewModelApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: BankAccountViewModel;

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface EmploymentContractDto {
  /**  */
  remarkEmploymentContract?: string;

  /**  */
  remarkMovement?: string;

  /**  */
  employmentContracts?: EmploymentContractViewModel[];
}

export interface EmploymentContractDtoApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: EmploymentContractDto;

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface TerminationSaveModel {
  /**  */
  id?: number;

  /**  */
  employmentContractId?: number;

  /**  */
  terminationReasonPrimary?: string;

  /**  */
  terminationReasonAlternative?: string;

  /**  */
  terminationReasonMpf?: string;

  /**  */
  terminationCodeTax?: string;

  /**  */
  terminationContractEndDate?: Date;

  /**  */
  lastEmploymentDate?: Date;

  /**  */
  lastWorkingDate?: Date;

  /**  */
  notificationDate?: Date;

  /**  */
  yearServiceStartDate?: Date;

  /**  */
  yearServiceEndDate?: Date;

  /**  */
  leavingHongKong?: boolean;

  /**  */
  holdLastPayment?: boolean;

  /**  */
  remark?: string;
}

export interface EmploymentContractModel {
  /**  */
  employmentContractId?: number;

  /**  */
  employeeId?: number;

  /**  */
  employeeNo?: string;

  /**  */
  remarks?: string;

  /**  */
  globalNo?: string;

  /**  */
  staffType?: string;

  /**  */
  contractNo?: string;

  /**  */
  termination?: TerminationSaveModel;
}

export interface EmploymentContractSaveModel {
  /**  */
  remarkEmploymentContract?: string;

  /**  */
  remarkMovement?: string;

  /**  */
  employmentContracts?: EmploymentContractModel[];

  /**  */
  movements?: MovementModel[];
}

export interface EmploymentContractSaveModelApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: EmploymentContractSaveModel;
}

export interface EmploymentContractAddModel {
  /**  */
  workCalendar?: number;

  /**  */
  defaultSalaryCurrency?: string;

  /**  */
  defaultPaymentCurrency?: string;

  /**  */
  staffType?: string;

  /**  */
  businessUnitId?: number;

  /**  */
  contractNo?: string;

  /**  */
  expectedStartDate?: Date;

  /**  */
  probation?: boolean;

  /**  */
  expectedProbationEndDate?: Date;

  /**  */
  dateOfAcceptingTheOffer?: Date;

  /**  */
  remark?: string;
}

export interface EmploymentContractAddModelApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: EmploymentContractAddModel;
}

export interface EmploymentStatusViewModelApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: EmploymentStatusViewModel;

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface ApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;
}

export interface EmploymentStatusModel {
  /**  */
  employmentStatusId?: number;

  /**  */
  employmentContractId?: number;

  /**  */
  startDate?: Date;

  /**  */
  status?: string;

  /**  */
  remark?: string;

  /**  */
  confirmed?: boolean;

  /**  */
  statusId?: string;
}

export interface EmploymentStatusSaveModel {
  /**  */
  remark?: string;

  /**  */
  employmentStatuses?: EmploymentStatusModel[];
}

export interface EmploymentStatusSaveModelApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: EmploymentStatusSaveModel;
}

export interface Int32NullableApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: number;

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface TerminationViewModelApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: TerminationViewModel;

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface TerminationSaveModelApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: TerminationSaveModel;
}

export interface PublicInfoDto {
  /**  */
  applicationName?: string;

  /**  */
  environment?: string;

  /**  */
  dbInfo?: string;

  /**  */
  coreServiceGenericCodeUri?: string;

  /**  */
  coreServiceMasterPayItemUri?: string;
}

export interface PublicInfoDtoApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: PublicInfoDto;

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface MessageTemplateCreateDto {
  /**  */
  clientId?: number;

  /**  */
  entityId?: number;

  /**  */
  templateCode?: string;

  /**  */
  templateType?: string;

  /**  */
  flagHtml?: boolean;

  /**  */
  portal?: string;

  /**  */
  status?: string;
}

export interface MessageTemplateCreateDtoApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: MessageTemplateCreateDto;
}

export interface MessageTemplateLangCreateDto {
  /**  */
  templateId?: number;

  /**  */
  langCode?: string;

  /**  */
  title?: string;

  /**  */
  content?: string;

  /**  */
  status?: string;
}

export interface MessageTemplateLangCreateDtoApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: MessageTemplateLangCreateDto;
}

export interface MessageCreateDto {
  /**  */
  templateLangId?: number;

  /**  */
  recipient?: string;

  /**  */
  messageTitle?: string;

  /**  */
  messageContent?: string;

  /**  */
  processedDt?: Date;

  /**  */
  status?: string;

  /**  */
  attemptCount?: number;

  /**  */
  userId?: number;
}

export interface MessageCreateDtoApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: MessageCreateDto;
}

export interface MessageTemplateUpdateDto {
  /**  */
  templateId?: number;

  /**  */
  clientId?: number;

  /**  */
  entityId?: number;

  /**  */
  templateCode?: string;

  /**  */
  templateType?: string;

  /**  */
  flagHtml?: boolean;

  /**  */
  portal?: string;

  /**  */
  status?: string;

  /**  */
  createdBy?: string;
}

export interface MessageTemplateUpdateDtoApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: MessageTemplateUpdateDto;
}

export interface MessageTemplateLangUpdateDto {
  /**  */
  templateLangId?: number;

  /**  */
  templateId?: number;

  /**  */
  langCode?: string;

  /**  */
  title?: string;

  /**  */
  content?: string;

  /**  */
  status?: string;
}

export interface MessageTemplateLangUpdateDtoApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: MessageTemplateLangUpdateDto;
}

export interface MessageUpdateDto {
  /**  */
  messageId?: number;

  /**  */
  templateLangId?: number;

  /**  */
  messageType?: string;

  /**  */
  recipient?: string;

  /**  */
  messageTitle?: string;

  /**  */
  messageContent?: string;

  /**  */
  processedDt?: Date;

  /**  */
  status?: string;

  /**  */
  attemptCount?: number;

  /**  */
  portal?: string;

  /**  */
  flagHtml?: boolean;

  /**  */
  userId?: number;

  /**  */
  createdBy?: string;

  /**  */
  createdDt?: Date;

  /**  */
  modifiedBy?: string;

  /**  */
  modifiedDt?: Date;
}

export interface MessageUpdateDtoApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: MessageUpdateDto;
}

export interface FileViewModel {
  /**  */
  fileId?: number;

  /**  */
  category?: string;

  /**  */
  name?: string;

  /**  */
  isDeleted?: boolean;

  /**  */
  remark?: string;

  /**  */
  src?: string;

  /**  */
  createdDt?: Date;

  /**  */
  createdBy?: string;

  /**  */
  modifiedDt?: Date;

  /**  */
  modifiedBy?: string;
}

export interface FileViewModelListApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: FileViewModel[];

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface CreateJobGradeRequest {
  /**  */
  name?: string;

  /**  */
  level?: number;

  /**  */
  remark?: string;

  /**  */
  status?: string;
}

export interface CreateJobGradeRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: CreateJobGradeRequest;
}

export interface UpdateJobGradeRequest {
  /**  */
  name?: string;

  /**  */
  level?: number;

  /**  */
  remark?: string;

  /**  */
  status?: string;
}

export interface UpdateJobGradeRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: UpdateJobGradeRequest;
}

export interface OrganizationStructureModel {
  /**  */
  unitId?: number;

  /**  */
  parentId?: number;

  /**  */
  clientId?: number;

  /**  */
  entityId?: number;

  /**  */
  codeLevel?: string;

  /**  */
  fullName?: string;

  /**  */
  displayName?: string;

  /**  */
  unitCode?: string;

  /**  */
  statusCd?: string;

  /**  */
  statusText?: string;

  /**  */
  remark?: string;

  /**  */
  employeeCount?: number;
}

export interface OrganizationStructureAllModel {
  /**  */
  data?: OrganizationStructureModel[];

  /**  */
  totalRecord?: number;
}

export interface OrganizationStructureAllModelApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: OrganizationStructureAllModel;

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface OrganizationStructureDisplayNameModel {
  /**  */
  unitId?: number;

  /**  */
  displayName?: string;
}

export interface OrganizationStructureAddModel {
  /**  */
  organizationStructureDisplayName?: OrganizationStructureDisplayNameModel[];
}

export interface OrganizationStructureStoreModel {
  /**  */
  parentId?: number;

  /**  */
  fullName?: string;

  /**  */
  displayName?: string;

  /**  */
  unitCode?: string;

  /**  */
  statusCd?: string;

  /**  */
  remark?: string;
}

export interface OrganizationStructureStoreModelApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: OrganizationStructureStoreModel;
}

export interface Int32ApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: number;

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface OrganizationStructureUpdateModel {
  /**  */
  parentId?: number;

  /**  */
  fullName?: string;

  /**  */
  displayName?: string;

  /**  */
  unitCode?: string;

  /**  */
  statusCd?: string;

  /**  */
  remark?: string;
}

export interface OrganizationStructureUpdateModelApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: OrganizationStructureUpdateModel;
}

export interface OrganizationStructureDataAccessGroupDto {
  /**  */
  itemId?: number;

  /**  */
  itemType?: string;

  /**  */
  itemCode?: string;

  /**  */
  itemDescendantCount?: number;

  /**  */
  itemText?: string;

  /**  */
  isSelected?: boolean;

  /**  */
  itemLevel?: string;

  /**  */
  dataAccessGroupId?: number;
}

export interface OrganizationStructureDataAccessGroupDtoTreeDto {
  /**  */
  key?: string;

  /**  */
  data?: OrganizationStructureDataAccessGroupDto;

  /**  */
  children?: OrganizationStructureDataAccessGroupDtoTreeDto[];

  /**  */
  isSelectable?: boolean;
}

export interface OrganizationStructureDataAccessGroupViewModel {
  /**  */
  isAllCheck?: boolean;

  /**  */
  tree?: OrganizationStructureDataAccessGroupDtoTreeDto[];
}

export interface OrganizationStructureDataAccessGroupViewModelApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: OrganizationStructureDataAccessGroupViewModel;

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface HolidayViewModel {
  /**  */
  holidayDateId?: number;

  /**  */
  date?: Date;

  /**  */
  descriptionPrimaryLanguage?: string;

  /**  */
  descriptionSecondaryLanguage?: string;

  /**  */
  holidayType?: string;

  /**  */
  calendarId?: number;

  /**  */
  isWorkingDay?: boolean;

  /**  */
  createdBy?: string;

  /**  */
  createdDt?: Date;

  /**  */
  modifiedBy?: string;

  /**  */
  modifiedDt?: Date;
}

export interface WorkCalendarViewModel {
  /**  */
  calendarId?: number;

  /**  */
  specificEntityId?: number;

  /**  */
  entityName?: string;

  /**  */
  calendarName?: string;

  /**  */
  parentCalendarId?: number;

  /**  */
  parentName?: string;

  /**  */
  status?: string;

  /**  */
  statusName?: string;

  /**  */
  defaultWorkingDaySunday?: string;

  /**  */
  defaultWorkingDayMonday?: string;

  /**  */
  defaultWorkingDayTuesday?: string;

  /**  */
  defaultWorkingDayWednesday?: string;

  /**  */
  defaultWorkingDayThursday?: string;

  /**  */
  defaultWorkingDayFriday?: string;

  /**  */
  defaultWorkingDaySaturday?: string;

  /**  */
  createdBy?: string;

  /**  */
  createdDt?: Date;

  /**  */
  modifiedBy?: string;

  /**  */
  modifiedDt?: Date;

  /**  */
  calendarDay?: boolean;

  /**  */
  holidays?: HolidayViewModel[];
}

export interface WorkCalendarViewModelPagination {
  /** current page */
  page?: number;

  /** total page */
  totalPages?: number;

  /** total record in page */
  size?: number;

  /** record return */
  numberOfElements?: number;

  /** total record found */
  totalElements?: number;

  /** list data return */
  content?: WorkCalendarViewModel[];
}

export interface WorkCalendarViewModelPaginationApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: WorkCalendarViewModelPagination;

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface WorkCalendarViewModelApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: WorkCalendarViewModel;

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface WorkCalendarModel {
  /**  */
  calendarId?: number;

  /**  */
  specificEntityId?: number;

  /**  */
  calendarName?: string;

  /**  */
  parentCalendarId?: number;

  /**  */
  status?: string;

  /**  */
  defaultWorkingDaySunday?: string;

  /**  */
  defaultWorkingDayMonday?: string;

  /**  */
  defaultWorkingDayTuesday?: string;

  /**  */
  defaultWorkingDayWednesday?: string;

  /**  */
  defaultWorkingDayThursday?: string;

  /**  */
  defaultWorkingDayFriday?: string;

  /**  */
  defaultWorkingDaySaturday?: string;

  /**  */
  calendarDay?: boolean;
}

export interface HolidayModel {
  /**  */
  holidayDateId?: number;

  /**  */
  date?: Date;

  /**  */
  descriptionPrimaryLanguage?: string;

  /**  */
  descriptionSecondaryLanguage?: string;

  /**  */
  holidayType?: string;

  /**  */
  calendarId?: number;

  /**  */
  isWorkingDay?: boolean;
}

export interface WorkCalendarDto {
  /**  */
  workCalendar?: WorkCalendarModel;

  /**  */
  holidays?: HolidayModel[];
}

export interface WorkCalendarDtoApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: WorkCalendarDto;
}

export interface BooleanNullableApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: boolean;

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface TbCfgWorkingSchedule {
  /**  */
  id?: number;

  /**  */
  name?: string;
}

export interface TbCfgWorkingScheduleListApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: TbCfgWorkingSchedule[];

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}
