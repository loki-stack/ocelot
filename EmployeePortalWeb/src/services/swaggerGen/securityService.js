const { codegen } = require("swagger-axios-codegen");
// temporary - avoid conflict with vn team
// will merge before 1.0
codegen({
  methodNameMode: "operationId",
  source: require("./securityService.json"),
  // remoteUrl: "http://172.20.20.18:1234/swagger/docs/v1/employment",
  outputDir: "./src/services/security",
  modelMode: "interface",
  // include: ["Company"],
  multipleFileMode: false,
  sharedServiceOptions: true,
  useHeaderParameters: true,
  strictNullChecks: false,
  useCustomerRequestInstance: true,
  generateValidationModel: true,
});
