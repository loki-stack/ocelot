import { StorageKey, StorageSerive } from "./../storageService/index";

import { MAKE_ID } from "./../../utils/utils";

export class ApiRequest {
  static createRay(batchName: string = "defaultBatch", id: string = MAKE_ID()) {
    return `${batchName}_${Math.floor(Date.now() / 1000)}_${id}`;
  }

  static async createParam() {
    let result = {
      parameterLocale: "en",
      parameterClientId: "",
      parameterEntityId: "",
      parameterGenericCodeList: [],
    };
    let tennantInfo = await StorageSerive.getItemJSON(StorageKey.TENNANT_INFO);
    let userInfo = await StorageSerive.getItemJSON(StorageKey.USER_INFO);
    if (tennantInfo) {
      result.parameterClientId = tennantInfo.data?.client?.clientId || 0;
      result.parameterEntityId = tennantInfo.data?.entity?.entityId || 0;
    }
    if (userInfo) {
      result.parameterLocale = userInfo.primaryLang || "en";
    }
    return result;
  }
  static async createParamBody() {
    let result = {
      locale: "en",
      clientId: 0,
      entityId: 0,
      genericCodeList: [],
    };
    let tennantInfo = await StorageSerive.getItemJSON(StorageKey.TENNANT_INFO);
    let userInfo = await StorageSerive.getItemJSON(StorageKey.USER_INFO);
    if (tennantInfo) {
      result.clientId = tennantInfo.data?.client?.clientId || 0;
      result.entityId = tennantInfo.data?.entity?.entityId || 0;
    }
    if (userInfo) {
      result.locale = userInfo.primaryLang || "en";
    }
    return result;
  }
}
/**
 * Get validate array
 * @param res
 */
export const getValidateResultItem = (
  validateResult: any[],
  index: number,
  prefix: string
) => {
  let result: any[] = [];
  validateResult.forEach((element) => {
    if (element.componentId.includes(`${prefix}[${index}]_`)) {
      let newName = element.componentId.replace(`${prefix}[${index}]_`, "");
      result.push({
        ...element,
        componentId: newName,
      });
    }
  });
  return result;
};
