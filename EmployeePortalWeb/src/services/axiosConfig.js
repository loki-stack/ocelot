import { StorageKey, StorageSerive } from "./storageService";

import { VnAuthenticationService } from "./security";
import axios from "axios";
import i18n from "../i18n";
import qs from "qs";
import { ApiRequest } from "./utils/index";

const refreshAccessToken = async (axiosApiInstance, originalRequest) => {
  console.log("🚀 ~ start refresh");
  const keys = await StorageSerive.getToken();
  if (!keys || !keys.refreshToken) {
    // Dummy ignore error
    gotoLogin();
    return Promise.resolve({
      data: null,
    });
  }

  const ray = ApiRequest.createRay("RefreshToken");
  const apiRequestParam = await ApiRequest.createParamBody();
  // pure request
  var res = await VnAuthenticationService.authenticationRefresh({
    body: {
      ray,
      parameter: {
        ...apiRequestParam,
      },
      data: {
        token: keys.token,
        refreshToken: keys.refreshToken,
      },
    },
  });
  if (res && res.data) {
    // 1) put token to LocalStorage
    await StorageSerive.setToken(res.data);
    const new_keys = await StorageSerive.getToken();
    // 2) Change Authorization header
    axios.defaults.headers.common["Authorization"] = "Bearer " + new_keys.token;

    // 3) return originalRequest object with Axios.
    return axiosApiInstance(originalRequest);
  }
  gotoLogin();
  // Dummy ignore error
  return Promise.resolve({
    data: null,
  });
  // return axios
  //   .post("API_GET_ACCESSTOKEN_FROM_REFRESH_TOKEN", {
  //     token: keys.token,
  //     refreshToken: keys.refreshToken,
  //   })
  //   .then(async (res) => {
  //     if (res.status === 201) {
  //       // 1) put token to LocalStorage
  //       await StorageSerive.setToken(res.data);
  //       const new_keys = await StorageSerive.getToken();
  //       // 2) Change Authorization header
  //       axios.defaults.headers.common["Authorization"] =
  //         "Bearer " + new_keys.refresh_token;

  //       // 3) return originalRequest object with Axios.
  //       return axiosApiInstance(originalRequest);
  //     }
  //   });
};
const gotoLogin = async () => {
  // const portal = await StorageSerive.getItem(StorageKey.PORTAL_CODE);
  const global = await StorageSerive.getItemJSON(StorageKey.GLOBAL);
  // if (!window.location.pathname.includes(`/${portal}/passport`)) {
  //   window.location.href = `/${portal}/passport`;
  //   // history.push({ pathname: `/${portal}/passport` });
  // }

  if (!window.location.pathname.includes("/passport/login")) {
    window.sessionStorage.setItem("path_after_login", window.location.pathname);
  }

  if (global && global.clientInfo) {
    // alert("login---yy");
    window.location.href = `/${global.clientInfo.clientCode}/${global.clientInfo.entityCode}/passport/login`;
  } else {
    // alert("login---xx");
    window.location.href = "/"; // back to root
  }
};
const gotoPage = async (history, page) => {
  const portal = await StorageSerive.getItem(StorageKey.PORTAL_CODE);
  history.push({ pathname: `/${portal}/${page}` });
};

function handlerAPIError(error, store, history) {
  if (!error || !error.response || !error.response.status) {
    let toast = store.getState().global.toast;
    toast.show({
      severity: "error",
      summary: i18n.t("Notification"),
      detail: i18n.t("Network error"),
      life: 3000,
    });
    return error;
  }
  switch (error.response.status) {
    case 500:
      // gotoPage(history, "COM/COM0101/500");
      break;
    case 404:
      // gotoPage(history, "COM/COM0101/404");
      break;
    case 403:
      // gotoPage(history, "COM/COM0101/403");
      break;
    case 401:
      console.log("🚀 ~ goto loginnnnn");
      gotoLogin();

      break;
    default:
      break;
  }

  return error.response;
}

function handlerMessage(response, store, history) {
  let res = response.data;
  let toast = store.getState().global.toast;
  if (res && res.message && res.message.toast && res.message.toast.length > 0) {
    res.message.toast.forEach((element) => {
      const param = {};
      if (element.placeholder) {
        element.placeholder.forEach((element, placeIndex) => {
          param["placeIndex" + placeIndex] = element;
        });
      }
      if (toast) {
        toast.show({
          severity: element.level.toLowerCase(),
          summary: i18n.t("Notification"),
          detail: i18n.t("dynamic:" + element.labelCode, param),
          life: 3000,
        });
      } else {
      }
    });
  }
  if (res && res.redirectUrl) {
    gotoPage(history, res.redirectUrl);
  }
}
export function createAxiosApiInstance(baseURL, store, history) {
  const instance = axios.create({
    baseURL: baseURL,
    timeout: 120000,
    paramsSerializer: function (params) {
      return qs.stringify(params, { arrayFormat: "repeat" });
    },
  });
  // Request interceptor for API calls
  instance.interceptors.request.use(
    async (config) => {
      // get key
      const keys = await StorageSerive.getToken();
      config.headers = {
        Authorization: `Bearer ${keys?.token}`,
      };
      return config;
    },
    (error) => {
      Promise.reject(error);
    }
  );
  // Response interceptor for API calls
  instance.interceptors.response.use(
    (response) => {
      // tweak to get the response object
      if (response.config.callback) {
        response.config.callback(response);
      }

      handlerMessage(response, store, history);
      return response;
    },
    async function (error) {
      const originalRequest = error.config;
      handlerMessage(error, store, history);
      if (
        error &&
        error.response &&
        error.response.status === 401 &&
        !originalRequest._retry
      ) {
        originalRequest._retry = true;
        return refreshAccessToken(instance, originalRequest);
      }
      return handlerAPIError(error, store, history);
    }
  );
  return instance;
}
//#endregion
