import {
  IRequestOptions,
  IRequestConfig,
  getConfigs,
  axios,
} from "./serviceOptions";
const basePath = "";

export interface IList<T> extends Array<T> {}
export interface List<T> extends Array<T> {}
export interface IDictionary<TValue> {
  [key: string]: TValue;
}
export interface Dictionary<TValue> extends IDictionary<TValue> {}

export interface IListResult<T> {
  items?: T[];
}

export class ListResultDto<T> implements IListResult<T> {
  items?: T[];
}

export interface IPagedResult<T> extends IListResult<T> {
  totalCount?: number;
  items?: T[];
}

export class PagedResultDto<T> implements IPagedResult<T> {
  totalCount?: number;
  items?: T[];
}

// customer definition
// empty

export class ClientService {
  /**
   *
   */
  static clientGetClientEntities(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CAM/CAM0401/Client";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class DataAccessGroupsService {
  /**
   *
   */
  static dataAccessGroupsList(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
      /**  */
      data?: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<DarManagementDtoApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC1001/DataAccessGroups/list";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
        Data: params["data"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static dataAccessGroupsCreate(
    params: {
      /** requestBody */
      body?: CreateDataAccessGroupRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC1002/DataAccessGroups";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static dataAccessGroupsGetDarById(
    params: {
      /**  */
      id: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<DarManagementDtoApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC1001/DataAccessGroups/dataaccess/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static dataAccessGroupsGetAllClientEntitiesAsTree(
    params: {
      /** requestBody */
      body?: Int32ApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<DarManagementDtoApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC1004/DataAccessGroups";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static dataAccessGroupsUpdate(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: CreateDataAccessGroupRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC1003/DataAccessGroups/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "put",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class FunctionsService {
  /**
   *
   */
  static functionsGetAll(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
      /**  */
      dataSort?: string;
      /**  */
      dataPage?: number;
      /**  */
      dataSize?: number;
      /**  */
      dataFilter?: string;
      /**  */
      dataFullTextSearch?: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0301/Functions";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
        "Data.Sort": params["dataSort"],
        "Data.Page": params["dataPage"],
        "Data.Size": params["dataSize"],
        "Data.Filter": params["dataFilter"],
        "Data.FullTextSearch": params["dataFullTextSearch"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static functionsGetById(
    params: {
      /**  */
      id: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0301/Functions/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static functionsCreate(
    params: {
      /** requestBody */
      body?: CreateFuncRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0302/Functions";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static functionsUpdate(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: UpdateFuncRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0303/Functions/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "put",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static functionsDelete(
    params: {
      /**  */
      id: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0304/Functions/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "delete",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class GenericCodeService {
  /**
   *
   */
  static genericCodeGetGenericCodes(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
      /**  */
      dataCodeType?: string;
      /**  */
      dataResData?: number;
      /**  */
      dataSort?: string;
      /**  */
      dataPage?: number;
      /**  */
      dataSize?: number;
      /**  */
      dataFilter?: string;
      /**  */
      dataFullTextSearch?: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CFG/CFG0301/GenericCode";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
        "Data.CodeType": params["dataCodeType"],
        "Data.ResData": params["dataResData"],
        "Data.Sort": params["dataSort"],
        "Data.Page": params["dataPage"],
        "Data.Size": params["dataSize"],
        "Data.Filter": params["dataFilter"],
        "Data.FullTextSearch": params["dataFullTextSearch"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static genericCodeGetById(
    params: {
      /**  */
      id: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CFG/CFG0301/GenericCode/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static genericCodeCreate(
    params: {
      /** requestBody */
      body?: CreateGenericCodeRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CFG/CFG0302/GenericCode";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static genericCodeUpdate(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: UpdateGenericCodeRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CFG/CFG0303/GenericCode/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "put",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class GlobalCalendarService {
  /**
   *
   */
  static globalCalendarGetFilter(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
      /**  */
      dataSort?: string;
      /**  */
      dataPage?: number;
      /**  */
      dataSize?: number;
      /**  */
      dataFilter?: string;
      /**  */
      dataFullTextSearch?: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GlobalCalendarViewModelPaginationApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CFG/CFG0601/WorkCalendar/GlobalCalendar";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
        "Data.Sort": params["dataSort"],
        "Data.Page": params["dataPage"],
        "Data.Size": params["dataSize"],
        "Data.Filter": params["dataFilter"],
        "Data.FullTextSearch": params["dataFullTextSearch"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static globalCalendarGetById(
    params: {
      /**  */
      id: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GlobalCalendarViewModelApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CFG/CFG0601/WorkCalendar/GlobalCalendar/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static globalCalendarDeleteGlobalCalendar(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: ApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GlobalCalendarViewModelApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CFG/CFG0604/WorkCalendar/GlobalCalendar/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "delete",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static globalCalendarSave(
    params: {
      /** requestBody */
      body?: GlobalCalendarDtoApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GlobalCalendarViewModelApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CFG/CFG0606/WorkCalendar/GlobalCalendar";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static globalCalendarDeleteHoliday(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: ApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<BooleanNullableApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CFG/CFG0604/WorkCalendar/Holiday/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "delete",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class MenuService {
  /**
   *
   */
  static menuGetMenu(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/COM/COM0301/Menu";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static menuGetBreadCrumbNavigation(
    params: {
      /** requestBody */
      body?: StringListApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/COM/COM0301/Menu";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class PublicInfoService {
  /**
   * Get public information of the application
   */
  static publicInfoPublicInfo(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PublicInfoDtoApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/COM/COM9901/PublicInfo";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class RoleGroupsService {
  /**
   *
   */
  static roleGroupsCreate(
    params: {
      /** requestBody */
      body?: CreateRoleGroupRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0402/RoleGroups";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static roleGroupsUpdate(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: CreateRoleGroupRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0403/RoleGroups/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "put",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static roleGroupsGetAll(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
      /**  */
      dataSort?: string;
      /**  */
      dataPage?: number;
      /**  */
      dataSize?: number;
      /**  */
      dataFilter?: string;
      /**  */
      dataFullTextSearch?: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0401/RoleGroups";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
        "Data.Sort": params["dataSort"],
        "Data.Page": params["dataPage"],
        "Data.Size": params["dataSize"],
        "Data.Filter": params["dataFilter"],
        "Data.FullTextSearch": params["dataFullTextSearch"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static roleGroupsGetById(
    params: {
      /**  */
      id: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0401/RoleGroups/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static roleGroupsDelete(
    params: {
      /**  */
      id: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0404/RoleGroups/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "delete",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static roleGroupsList(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
      /**  */
      data?: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<FarManagementDtoApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0401/RoleGroups/list";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
        Data: params["data"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static roleGroupsTreeInfo(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
      /**  */
      data?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<FarManagementDtoApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0401/RoleGroups/treeInfo";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
        Data: params["data"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class SecurityProfileService {
  /**
   *
   */
  static securityProfileCreate(
    params: {
      /** requestBody */
      body?: CreateSecProfileRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0702/SecurityProfile";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static securityProfileUpdate(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: UpdateSecProfileRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0703/SecurityProfile/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "put",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static securityProfileGetAll(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
      /**  */
      dataSort?: string;
      /**  */
      dataPage?: number;
      /**  */
      dataSize?: number;
      /**  */
      dataFilter?: string;
      /**  */
      dataFullTextSearch?: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0701/SecurityProfile";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
        "Data.Sort": params["dataSort"],
        "Data.Page": params["dataPage"],
        "Data.Size": params["dataSize"],
        "Data.Filter": params["dataFilter"],
        "Data.FullTextSearch": params["dataFullTextSearch"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static securityProfileList(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
      /**  */
      data?: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<SecProfileMngDtoApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0701/SecurityProfile/list";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
        Data: params["data"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static securityProfileSave(
    params: {
      /** requestBody */
      body?: CreateSecProfileRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<BooleanApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0703/SecurityProfile/save";

      const configs: IRequestConfig = getConfigs(
        "put",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static securityProfileGetAllGroups(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
      /**  */
      data?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<SecProfileGroupsDtoApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0701/SecurityProfile/grouplist";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
        Data: params["data"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static securityProfileGetById(
    params: {
      /**  */
      id: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0701/SecurityProfile/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static securityProfileDelete(
    params: {
      /**  */
      id: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0704/SecurityProfile/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "delete",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class SettingService {
  /**
   *
   */
  static settingGetEntitySetting(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CFG/CFG0201/Setting/EntitySetting";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static settingGetEmployeeSetting(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CFG/CFG0201/Setting/EmployeeSetting";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static settingUpdateEmployeeSetting(
    params: {
      /** requestBody */
      body?: SettingUpdateEmployeeRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CFG/CFG0203/Setting/EmployeeSetting";

      const configs: IRequestConfig = getConfigs(
        "put",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static settingGetPdfSetting(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CFG/CFG0201/Setting/PDF";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static settingUpdatePdfSetting(
    params: {
      /** requestBody */
      body?: SettingUpdatePDFRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CFG/CFG0203/Setting/PDF";

      const configs: IRequestConfig = getConfigs(
        "put",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static settingGetLanguageSetting(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CFG/CFG0201/Setting/Language";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static settingUpdateLanguageSetting(
    params: {
      /** requestBody */
      body?: SettingUpdateLanguageRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CFG/CFG0203/Setting/Language";

      const configs: IRequestConfig = getConfigs(
        "put",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static settingGetProbationPeriodSetting(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CFG/CFG0201/Setting/ProbationPeriod";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static settingUpdateProbationPeriodSetting(
    params: {
      /** requestBody */
      body?: SettingUpdateProbationPeriodRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CFG/CFG0203/Setting/ProbationPeriod";

      const configs: IRequestConfig = getConfigs(
        "put",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static settingGetCurrencySetting(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CFG/CFG0201/Setting/Currency";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static settingUpdateCurrencySetting(
    params: {
      /** requestBody */
      body?: SettingUpdateCurrencyRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CFG/CFG0203/Setting/Currency";

      const configs: IRequestConfig = getConfigs(
        "put",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static settingGetWorkCalendarSetting(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CFG/CFG0201/Setting/WorkCalendar";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static settingUpdateWorkCalendarSetting(
    params: {
      /** requestBody */
      body?: SettingUpdateWorkCalendarRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CFG/CFG0203/Setting/WorkCalendar";

      const configs: IRequestConfig = getConfigs(
        "put",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static settingGetSignInSetting(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CFG/CFG0201/Setting/SignInMethod";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static settingUpdateSignInSetting(
    params: {
      /** requestBody */
      body?: SettingSignInMethodRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CFG/CFG0203/Setting/SignInMethod";

      const configs: IRequestConfig = getConfigs(
        "put",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static settingGetLanguages(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
      /**  */
      data?: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CFG/CFG0201/Setting/Languages";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
        Data: params["data"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static settingGetNotificationEmailSetting(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CFG/CFG0201/Setting/NotificationEmail";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static settingUpdateNotificationEmailSetting(
    params: {
      /** requestBody */
      body?: SettingNotificationEmailRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/CFG/CFG0203/Setting/NotificationEmail";

      const configs: IRequestConfig = getConfigs(
        "put",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class UserSecurityProfileService {
  /**
   *
   */
  static userSecurityProfileGetUserSecurityProfile(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
      /**  */
      dataUserId?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url =
        basePath +
        "/v1.0/SEC/SEC0801/UserSecurityProfile/GetUserSecurityProfile";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
        "Data.UserId": params["dataUserId"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static userSecurityProfileCreateUserSecurityProfile(
    params: {
      /** requestBody */
      body?: UserSecurityProfileRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url =
        basePath +
        "/v1.0/SEC/SEC0802/UserSecurityProfile/CreateUserSecurityProfile";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static userSecurityProfileDeleteUserSecurityProfile(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
      /**  */
      dataUserId?: number;
      /**  */
      dataSecurityProfileId?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url =
        basePath +
        "/v1.0/SEC/SEC0804/UserSecurityProfile/DeleteUserSecurityProfile";

      const configs: IRequestConfig = getConfigs(
        "delete",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
        "Data.UserId": params["dataUserId"],
        "Data.SecurityProfileId": params["dataSecurityProfileId"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static userSecurityProfileUpdate(
    params: {
      /**  */
      userId: number;
      /** requestBody */
      body?: UserSecurityProfileDtoApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<StringObjectDictionaryApiResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0803/UserSecurityProfile/{userId}";
      url = url.replace("{userId}", params["userId"] + "");

      const configs: IRequestConfig = getConfigs(
        "put",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class VnAuthenticationService {
  /**
   *
   */
  static authenticationAuthenticate(
    params: {
      /** requestBody */
      body?: AuthenticateRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0201/Authentication";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static authenticationRefresh(
    params: {
      /** requestBody */
      body?: RefreshTokenRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0202/Authentication";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static authenticationEssAuthenticate(
    params: {
      /** requestBody */
      body?: AuthenticateRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC5004/Authentication";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static authenticationEssChangePassword(
    params: {
      /** requestBody */
      body?: ChangePasswordDtoApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC5005/Authentication";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Allow user to mark forgot password. System should send an email to user to reset password
   */
  static authenticationForgotPassword(
    params: {
      /** requestBody */
      body?: StringApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC5003/Authentication";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
 * The link should come from email, if verified, let user to reset the password.
Else show error.
 */
  static authenticationVerifyToken(
    params: {
      /** requestBody */
      body?: StringApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC5001/Authentication";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Reset password - required token, new password, confirm password
   */
  static authenticationResetPassword(
    params: {
      /** requestBody */
      body?: ResetPasswordDtoApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC5103/reset/Authentication";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Create password for user first login.
   */
  static authenticationEssFirstLogin(
    params: {
      /** requestBody */
      body?: ResetPasswordDtoApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC5103/firstLogin/Authentication";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class VnUserService {
  /**
   *
   */
  static usersGetAll(
    params: {
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
      /**  */
      dataSort?: string;
      /**  */
      dataPage?: number;
      /**  */
      dataSize?: number;
      /**  */
      dataFilter?: string;
      /**  */
      dataFullTextSearch?: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0101/Users";

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
        "Data.Sort": params["dataSort"],
        "Data.Page": params["dataPage"],
        "Data.Size": params["dataSize"],
        "Data.Filter": params["dataFilter"],
        "Data.FullTextSearch": params["dataFullTextSearch"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static usersGetById(
    params: {
      /**  */
      id: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
      /**  */
      data?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0101/Users/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
        Data: params["data"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static usersCreate(
    params: {
      /** requestBody */
      body?: CreateUserRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0102/Users";

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static usersUpdate(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: UpdateUserRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0103/Users/{id}";
      url = url.replace("{id}", params["id"] + "");

      const configs: IRequestConfig = getConfigs(
        "put",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static usersUpdatePortalAccess(
    params: {
      /** requestBody */
      body?: UserPortalAccessRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<number> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0103/PortalAccess/Users";

      const configs: IRequestConfig = getConfigs(
        "put",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static usersGetUserByEmployeeId(
    params: {
      /**  */
      employeeId: number;
      /**  */
      ray?: string;
      /**  */
      parameterLocale?: string;
      /**  */
      parameterClientId?: number;
      /**  */
      parameterEntityId?: number;
      /**  */
      parameterGenericCodeList?: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UserViewModel> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0101/User/{employeeId}/Users";
      url = url.replace("{employeeId}", params["employeeId"] + "");

      const configs: IRequestConfig = getConfigs(
        "get",
        "application/json",
        url,
        options
      );
      configs.params = {
        Ray: params["ray"],
        "Parameter.Locale": params["parameterLocale"],
        "Parameter.ClientId": params["parameterClientId"],
        "Parameter.EntityId": params["parameterEntityId"],
        "Parameter.GenericCodeList": params["parameterGenericCodeList"],
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static usersSendInvitation(
    params: {
      /**  */
      userId: number;
      /** requestBody */
      body?: SendInvitationRequestApiRequest;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + "/v1.0/SEC/SEC0102/SendInvitation/{userId}/Users";
      url = url.replace("{userId}", params["userId"] + "");

      const configs: IRequestConfig = getConfigs(
        "post",
        "application/json",
        url,
        options
      );

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export interface MessageItem {
  /**  */
  componentId?: string;

  /**  */
  level?: string;

  /**  */
  labelCode?: string;

  /**  */
  placeholder?: string[];
}

export interface Message {
  /**  */
  toast?: MessageItem[];

  /**  */
  alert?: MessageItem[];

  /**  */
  validateResult?: MessageItem[];
}

export interface StringObjectDictionaryApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: object;

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface ProblemDetails {
  /**  */
  type?: string;

  /**  */
  title?: string;

  /**  */
  status?: number;

  /**  */
  detail?: string;

  /**  */
  instance?: string;
}

export interface DataAccessGradeViewModel {
  /**  */
  dataAccessGroupId?: number;

  /**  */
  dataAccessGradeId?: number;

  /**  */
  minGradeLevelNum?: number;

  /**  */
  maxGradeLevelNum?: number;
}

export interface DataAccessGroupClientEntityDto {
  /**  */
  dataAccessClientEntityId?: number;

  /**  */
  dataAccessGroupId?: number;

  /**  */
  clientId?: number;

  /**  */
  clientLevel?: string;

  /**  */
  clientCode?: string;

  /**  */
  clientName?: string;

  /**  */
  entityId?: number;

  /**  */
  entityLevel?: string;

  /**  */
  entityCode?: string;

  /**  */
  entityName?: string;
}

export interface DataAccessOrganizationStructureViewModel {
  /**  */
  dataAccessGroupId?: number;

  /**  */
  nodeLevel?: string;

  /**  */
  isSelectAllDescendants?: boolean;

  /**  */
  descendantCount?: number;
}

export interface DataAccessGroupViewModel {
  /**  */
  dataAccessGroupId?: number;

  /**  */
  specificClientId?: number;

  /**  */
  specificEntityId?: number;

  /**  */
  dataAccessGroupNameTxt?: string;

  /**  */
  dataAccessGroupDescriptionTxt?: string;

  /**  */
  modifyBy?: string;

  /**  */
  modifyDt?: Date;

  /**  */
  securityProfileMappingCount?: number;

  /**  */
  lastModified?: string;

  /**  */
  grade?: DataAccessGradeViewModel[];

  /**  */
  entities?: DataAccessGroupClientEntityDto[];

  /**  */
  remunerationPackageId?: number[];

  /**  */
  organizationStructure?: DataAccessOrganizationStructureViewModel[];
}

export interface GenericCodeResult {
  /**  */
  codeId?: number;

  /**  */
  codeLevel?: string;

  /**  */
  codeType?: string;

  /**  */
  codeValue?: string;

  /**  */
  displayOrder?: number;

  /**  */
  i18NTextKey?: string;

  /**  */
  statusCd?: string;

  /**  */
  textValue?: string;
}

export interface DarClientEntityDto {
  /**  */
  itemId?: number;

  /**  */
  itemType?: string;

  /**  */
  itemCode?: string;

  /**  */
  itemText?: string;

  /**  */
  isSelected?: boolean;

  /**  */
  itemLevel?: string;

  /**  */
  dataAccessGroupId?: number;
}

export interface DarClientEntityDtoTreeNodeDto {
  /**  */
  key?: string;

  /**  */
  data?: DarClientEntityDto;

  /**  */
  children?: DarClientEntityDtoTreeNodeDto[];

  /**  */
  isSelectable?: boolean;
}

export interface DataAccessOrganizationStructureDto {
  /**  */
  dataAccessGroupId?: number;

  /**  */
  nodeLevel?: string;
}

export interface DarManagementDto {
  /**  */
  selectedDataAccessGroup?: DataAccessGroupViewModel;

  /**  */
  allDataAccessGroup?: DataAccessGroupViewModel[];

  /**  */
  remuneration?: object;

  /**  */
  allClientEntities?: DarClientEntityDtoTreeNodeDto[];

  /**  */
  allOrganizationStructures?: DataAccessOrganizationStructureDto[];

  /**  */
  isSelectAllEntity?: boolean;
}

export interface DarManagementDtoApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: DarManagementDto;

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface StdRequestParam {
  /**  */
  locale?: string;

  /**  */
  clientId?: number;

  /**  */
  entityId?: number;

  /**  */
  genericCodeList?: string[];
}

export interface DataAccessClientEntityViewModel {
  /**  */
  dataAccessClientEntityId?: number;

  /**  */
  dataAccessGroupId?: number;

  /**  */
  clientLevel?: string;

  /**  */
  entityLevel?: string;
}

export interface CreateDataAccessGroupRequest {
  /**  */
  dataAccessGroupId?: number;

  /**  */
  specificClientId?: number;

  /**  */
  specificEntityId?: number;

  /**  */
  dataAccessGroupNameTxt?: string;

  /**  */
  dataAccessGroupDescriptionTxt?: string;

  /**  */
  createBy?: string;

  /**  */
  createDt?: Date;

  /**  */
  modifyBy?: string;

  /**  */
  modifyDt?: Date;

  /**  */
  remunerationPackageId?: number[];

  /**  */
  grade?: DataAccessGradeViewModel[];

  /**  */
  clientEntity?: DataAccessClientEntityViewModel[];

  /**  */
  organizationStructure?: DataAccessOrganizationStructureViewModel[];
}

export interface CreateDataAccessGroupRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: CreateDataAccessGroupRequest;
}

export interface Int32ApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: number;
}

export interface CreateFuncRequest {
  /**  */
  functionId?: number;

  /**  */
  menuCd?: string;

  /**  */
  moduleCd?: string;

  /**  */
  featureCd?: string;

  /**  */
  functionCd?: string;

  /**  */
  roleCd?: string;

  /**  */
  menuDisplayOrder?: number;
}

export interface CreateFuncRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: CreateFuncRequest;
}

export interface UpdateFuncRequest {
  /**  */
  functionId?: number;

  /**  */
  menuCd?: string;

  /**  */
  moduleCd?: string;

  /**  */
  featureCd?: string;

  /**  */
  functionCd?: string;

  /**  */
  roleCd?: string;

  /**  */
  menuDisplayOrder?: number;
}

export interface UpdateFuncRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: UpdateFuncRequest;
}

export interface CreateGenericCodeRequest {
  /**  */
  moduleId?: number;

  /**  */
  parentCodeId?: number;

  /**  */
  codeValue?: string;

  /**  */
  status?: string;

  /**  */
  displayOrder?: number;

  /**  */
  name?: string;

  /**  */
  remark?: string;
}

export interface CreateGenericCodeRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: CreateGenericCodeRequest;
}

export interface UpdateGenericCodeRequest {
  /**  */
  moduleId?: number;

  /**  */
  parentCodeId?: number;

  /**  */
  codeValue?: string;

  /**  */
  status?: string;

  /**  */
  displayOrder?: number;

  /**  */
  name?: string;

  /**  */
  remark?: string;
}

export interface UpdateGenericCodeRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: UpdateGenericCodeRequest;
}

export interface HolidayViewModel {
  /**  */
  holidayDateId?: number;

  /**  */
  date?: Date;

  /**  */
  descriptionPrimaryLanguage?: string;

  /**  */
  descriptionSecondaryLanguage?: string;

  /**  */
  holidayType?: string;

  /**  */
  calendarId?: number;

  /**  */
  isWorkingDay?: boolean;

  /**  */
  createdBy?: string;

  /**  */
  createdDt?: Date;

  /**  */
  modifiedBy?: string;

  /**  */
  modifiedDt?: Date;
}

export interface GlobalCalendarViewModel {
  /**  */
  globalCalendarId?: number;

  /**  */
  globalLevelCalendarName?: string;

  /**  */
  country?: string;

  /**  */
  status?: string;

  /**  */
  statusName?: string;

  /**  */
  createdBy?: string;

  /**  */
  createdDt?: Date;

  /**  */
  modifiedBy?: string;

  /**  */
  modifiedDt?: Date;

  /**  */
  holidays?: HolidayViewModel[];
}

export interface GlobalCalendarViewModelPagination {
  /** current page */
  page?: number;

  /** total page */
  totalPages?: number;

  /** total record in page */
  size?: number;

  /** record return */
  numberOfElements?: number;

  /** total record found */
  totalElements?: number;

  /** list data return */
  content?: GlobalCalendarViewModel[];
}

export interface GlobalCalendarViewModelPaginationApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: GlobalCalendarViewModelPagination;

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface GlobalCalendarViewModelApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: GlobalCalendarViewModel;

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface ApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;
}

export interface GlobalCalendarModel {
  /**  */
  globalCalendarId?: number;

  /**  */
  globalLevelCalendarName?: string;

  /**  */
  country?: string;

  /**  */
  status?: string;
}

export interface HolidayModel {
  /**  */
  holidayDateId?: number;

  /**  */
  date?: Date;

  /**  */
  descriptionPrimaryLanguage?: string;

  /**  */
  descriptionSecondaryLanguage?: string;

  /**  */
  holidayType?: string;

  /**  */
  calendarId?: number;

  /**  */
  isWorkingDay?: boolean;
}

export interface GlobalCalendarDto {
  /**  */
  globalCalendar?: GlobalCalendarModel;

  /**  */
  holidays?: HolidayModel[];
}

export interface GlobalCalendarDtoApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: GlobalCalendarDto;
}

export interface BooleanNullableApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: boolean;

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface StringListApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: string[];
}

export interface PublicInfoDto {
  /**  */
  applicationName?: string;

  /**  */
  environment?: string;

  /**  */
  dbInfo?: string;
}

export interface PublicInfoDtoApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: PublicInfoDto;

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface CreateRoleGroupRequest {
  /**  */
  roleGroupId?: number;

  /**  */
  specificClientId?: number;

  /**  */
  specificEntityId?: number;

  /**  */
  roleGroupNameTxt?: string;

  /**  */
  roleGroupDescriptionTxt?: string;

  /**  */
  bundleGroupFlag?: boolean;

  /**  */
  funcIds?: number[];
}

export interface CreateRoleGroupRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: CreateRoleGroupRequest;
}

export interface RoleFunctionViewModel {
  /**  */
  functionId?: number;

  /**  */
  functionCd?: string;
}

export interface RoleGroupViewModel {
  /**  */
  roleGroupId?: number;

  /**  */
  specificClientId?: number;

  /**  */
  specificEntityId?: number;

  /**  */
  roleGroupNameTxt?: string;

  /**  */
  roleGroupDescriptionTxt?: string;

  /**  */
  createBy?: string;

  /**  */
  createDt?: Date;

  /**  */
  modifyBy?: string;

  /**  */
  modifyDt?: Date;

  /**  */
  funcs?: RoleFunctionViewModel[];

  /**  */
  securityProfileMappingCount?: number;

  /**  */
  lastModified?: string;
}

export interface FuncItemDto {
  /**  */
  itemId?: number;

  /**  */
  itemType?: string;

  /**  */
  itemCode?: string;

  /**  */
  itemText?: string;

  /**  */
  isSelected?: boolean;
}

export interface FuncItemDtoTreeNodeDto {
  /**  */
  key?: string;

  /**  */
  data?: FuncItemDto;

  /**  */
  children?: FuncItemDtoTreeNodeDto[];

  /**  */
  isSelectable?: boolean;
}

export interface FarManagementDto {
  /**  */
  selectedRoleGroup?: RoleGroupViewModel;

  /**  */
  allRoleGroup?: RoleGroupViewModel[];

  /**  */
  selectedFuncList?: FuncItemDtoTreeNodeDto[];

  /**  */
  funcList?: FuncItemDtoTreeNodeDto[];
}

export interface FarManagementDtoApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: FarManagementDto;

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface CreateProfileDetailRequest {
  /**  */
  securityProfileId?: number;

  /**  */
  roleGroupId?: number;

  /**  */
  dataAccessGroupId?: number;
}

export interface CreateSecProfileRequest {
  /**  */
  securityProfileId?: number;

  /**  */
  specificClientId?: number;

  /**  */
  specificEntityId?: number;

  /**  */
  securityProfileNameTxt?: string;

  /**  */
  securityProfileDescriptionTxt?: string;

  /**  */
  createBy?: string;

  /**  */
  createDt?: Date;

  /**  */
  modifyBy?: string;

  /**  */
  modifyDt?: Date;

  /**  */
  lastModified?: string;

  /**  */
  profileDetails?: CreateProfileDetailRequest[];
}

export interface CreateSecProfileRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: CreateSecProfileRequest;
}

export interface SecProfileDetailRequest {
  /**  */
  specificClientId?: number;

  /**  */
  specificEntityId?: number;

  /**  */
  roleGroupId?: number;

  /**  */
  roleGroupNameTxt?: string;

  /**  */
  roleGroupDescriptionTxt?: string;

  /**  */
  dataAccessGroupId?: number;

  /**  */
  dataAccessGroupNameTxt?: string;

  /**  */
  dataAccessGroupDescriptionTxt?: string;
}

export interface UpdateSecProfileRequest {
  /**  */
  name?: string;

  /**  */
  detail?: SecProfileDetailRequest[];
}

export interface UpdateSecProfileRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: UpdateSecProfileRequest;
}

export interface UserSecProfileViewModel {
  /**  */
  userId?: number;

  /**  */
  startDt?: Date;

  /**  */
  endDt?: Date;
}

export interface SecurityProfileViewModel {
  /**  */
  securityProfileId?: number;

  /**  */
  specificClientId?: number;

  /**  */
  specificEntityId?: number;

  /**  */
  securityProfileNameTxt?: string;

  /**  */
  securityProfileDescriptionTxt?: string;

  /**  */
  createBy?: string;

  /**  */
  createDt?: Date;

  /**  */
  modifyBy?: string;

  /**  */
  modifyDt?: Date;

  /**  */
  mappedProfileCount?: number;

  /**  */
  lastModified?: string;

  /**  */
  startDt?: Date;

  /**  */
  endDt?: Date;

  /**  */
  far?: RoleGroupViewModel[];

  /**  */
  dar?: DataAccessGroupViewModel[];

  /**  */
  userSecurityProfile?: UserSecProfileViewModel[];
}

export interface SecProfileMngDto {
  /**  */
  securityProfiles?: SecurityProfileViewModel[];
}

export interface SecProfileMngDtoApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: SecProfileMngDto;

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface BooleanApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: boolean;

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface SecurityProfileMngViewModel {
  /**  */
  securityProfileId?: number;

  /**  */
  roleGroupId?: number;

  /**  */
  roleGroupNameTxt?: string;

  /**  */
  roleGroupDescriptionTxt?: string;

  /**  */
  dataAccessGroupId?: number;

  /**  */
  dataAccessGroupNameTxt?: string;

  /**  */
  dataAccessDescriptionTxt?: string;
}

export interface SecProfileGroupsDto {
  /**  */
  securityProfileGroups?: SecurityProfileMngViewModel[];
}

export interface SecProfileGroupsDtoApiResponse {
  /**  */
  ray?: string;

  /**  */
  parameter?: object;

  /**  */
  data?: SecProfileGroupsDto;

  /**  */
  message?: Message;

  /**  */
  redirectUrl?: string;
}

export interface SettingUpdateEmployeeRequest {
  /**  */
  probationPeriod?: number;

  /**  */
  salaryCurrency?: string;

  /**  */
  paymentCurrency?: string;

  /**  */
  workCalendar?: number;
}

export interface SettingUpdateEmployeeRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: SettingUpdateEmployeeRequest;
}

export interface SettingUpdatePDFRequest {
  /**  */
  pdfStatus?: boolean;
}

export interface SettingUpdatePDFRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: SettingUpdatePDFRequest;
}

export interface SettingUpdateLanguageRequest {
  /**  */
  primaryLanguageCode?: string;

  /**  */
  secondaryLanguageCode?: string;
}

export interface SettingUpdateLanguageRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: SettingUpdateLanguageRequest;
}

export interface SettingUpdateProbationPeriodRequest {
  /**  */
  probationPeriod?: number;
}

export interface SettingUpdateProbationPeriodRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: SettingUpdateProbationPeriodRequest;
}

export interface SettingUpdateCurrencyRequest {
  /**  */
  salaryCurrency?: string;

  /**  */
  paymentCurrency?: string;
}

export interface SettingUpdateCurrencyRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: SettingUpdateCurrencyRequest;
}

export interface SettingUpdateWorkCalendarRequest {
  /**  */
  workCalendar?: number;
}

export interface SettingUpdateWorkCalendarRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: SettingUpdateWorkCalendarRequest;
}

export interface SettingSignInMethodRequest {
  /**  */
  signInMethod?: SignInMethod;
}

export interface SettingSignInMethodRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: SettingSignInMethodRequest;
}

export interface SettingNotificationEmailRequest {
  /**  */
  notificationEmail?: string;
}

export interface SettingNotificationEmailRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: SettingNotificationEmailRequest;
}

export interface UserSecurityProfileRequest {
  /**  */
  userId?: number;

  /**  */
  securityProfileId?: number;
}

export interface UserSecurityProfileRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: UserSecurityProfileRequest;
}

export interface UserSecurityProfileUpdateDto {
  /**  */
  userId?: number;

  /**  */
  securityProfileId?: number;

  /**  */
  startDt?: string;

  /**  */
  endDt?: string;
}

export interface UserSecurityProfileDto {
  /**  */
  fullName?: string;

  /**  */
  userSecurityProfiles?: UserSecurityProfileUpdateDto[];
}

export interface UserSecurityProfileDtoApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: UserSecurityProfileDto;
}

export interface AuthenticateRequest {
  /**  */
  account?: string;

  /**  */
  password?: string;
}

export interface AuthenticateRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: AuthenticateRequest;
}

export interface RefreshTokenRequest {
  /**  */
  token?: string;

  /**  */
  refreshToken?: string;
}

export interface RefreshTokenRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: RefreshTokenRequest;
}

export interface ChangePasswordDto {
  /**  */
  isAuthenticated?: boolean;

  /**  */
  currentPassword?: string;

  /**  */
  username?: string;

  /**  */
  userId?: number;

  /**  */
  newPassword?: string;

  /**  */
  confirmPassword?: string;
}

export interface ChangePasswordDtoApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: ChangePasswordDto;
}

export interface StringApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: string;
}

export interface ResetPasswordDto {
  /**  */
  isVerified?: boolean;

  /**  */
  resetStatus?: boolean;

  /**  */
  token?: string;

  /**  */
  newPassword?: string;

  /**  */
  confirmPassword?: string;

  /**  */
  email?: string;
}

export interface ResetPasswordDtoApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: ResetPasswordDto;
}

export interface CreateUserRequest {
  /**  */
  ssoId?: string;

  /**  */
  username?: string;

  /**  */
  password?: string;

  /**  */
  email?: string;

  /**  */
  avatar?: string;

  /**  */
  fullname?: string;

  /**  */
  entityId?: number;

  /**  */
  roleId?: number;

  /**  */
  languageId?: number;

  /**  */
  clientName?: string;
}

export interface CreateUserRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: CreateUserRequest;
}

export interface UpdateUserRequest {
  /**  */
  email?: string;

  /**  */
  fullname?: string;

  /**  */
  isUpdatePassword?: boolean;

  /**  */
  password?: string;

  /**  */
  statusCD?: string;
}

export interface UpdateUserRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: UpdateUserRequest;
}

export interface UserPortalAccessRequest {
  /**  */
  employeeId?: number;

  /**  */
  startDate?: Date;

  /**  */
  endDate?: Date;

  /**  */
  username?: string;

  /**  */
  singleSignOnId?: string;

  /**  */
  email?: string;

  /**  */
  essAccessFlag?: boolean;
}

export interface UserPortalAccessRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: UserPortalAccessRequest;
}

export interface UserViewModel {
  /**  */
  userId?: number;

  /**  */
  ssoId?: string;

  /**  */
  username?: string;

  /**  */
  email?: string;

  /**  */
  avatar?: string;

  /**  */
  fullname?: string;

  /**  */
  entityId?: number;

  /**  */
  entityName?: string;

  /**  */
  roleId?: number;

  /**  */
  roleName?: string;

  /**  */
  clientId?: number;

  /**  */
  clientName?: string;

  /**  */
  statusCD?: string;

  /**  */
  statusName?: string;

  /**  */
  lastActive?: Date;

  /**  */
  lastActiveStr?: string;

  /**  */
  createdDate?: Date;

  /**  */
  createdBy?: string;

  /**  */
  singleSignOnId?: string;

  /**  */
  essAccessStartDt?: Date;

  /**  */
  essAccessEndDt?: Date;

  /**  */
  essAccessFlag?: boolean;

  /**  */
  employeeId?: number;
}

export interface SendInvitationRequest {
  /**  */
  userName?: string;

  /**  */
  email?: string;
}

export interface SendInvitationRequestApiRequest {
  /**  */
  ray?: string;

  /**  */
  parameter?: StdRequestParam;

  /**  */
  data?: SendInvitationRequest;
}

export type SignInMethod = 1;
