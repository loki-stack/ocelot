﻿using CommonLib.Models;
using CommonLib.Models.Core;
using CommonLib.Validation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO;

namespace TestCommonLib
{
    public class Startup
    {
        public static IServiceCollection CreateServiceCollection()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile($"appsettings.Test.json", optional: true, reloadOnChange: true);
            IConfigurationRoot configuration = builder.Build();
            ServiceCollection serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection, configuration);
            return serviceCollection;
        }
        public static void ConfigureServices(IServiceCollection services, IConfigurationRoot configuration)
        {
            services.AddDbContext<CoreDbContext>(
                options => options.UseSqlServer("Server=172.20.20.18;Database=HRIS_CORE;MultipleActiveResultSets=true;User ID=sa;Password=123456a@",
                conf =>
                {
                    conf.UseHierarchyId();
                })
            );
            services.AddScoped<IValidator, Validator>();
        }
    }
}
