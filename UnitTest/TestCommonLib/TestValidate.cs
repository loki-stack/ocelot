using CommonLib.Validation;
using CoreServiceAPIs.Authentication.Repositories.Impl;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace TestCommonLib
{
    public class TestValidate
    {
        [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
        public class CustomValidate : ValidationAttribute
        {
            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {
                //Get other interfaces here
                //var clientRepository = validationContext.GetService<IClientRepository>();

                //If want standard paramers, use IHttpRequestAnalyzer to get them
                //var hm = validationContext.GetService<IHttpRequestAnalyzer>()

                //Validate property which depends on other property or need data from database

                //Return error if validation fails
                //return new ValidationResult(this.ErrorMessage, new List<string>() { validationContext.MemberName });

                return ValidationResult.Success;
            }
        }

        public class TestValidateClass
        {
            [Required]
            [MaxLength(20, ErrorMessage = "LabelCode01")]
            [ExtraResult(ComponentId = "name", Level = "ERROR", Placeholder = new string[] { "Holder 1", "example 1" })]
            public string Name { get; set; }

            [System.ComponentModel.DataAnnotations.Range(1, 12, ErrorMessage = "LabelCode02")]
            [ExtraResult(ComponentId = "age", Level = "ERROR", Placeholder = new string[] { "Holder 2", "4647" })]
            public int Age { get; set; }

            [CustomValidate(ErrorMessage = "customeTextCode")]
            [ExtraResult(ComponentId = "Family", Level = "ERROR", Placeholder = new string[] { "Holder 3", "example 2" })]
            public string Family { get; set; }

            [System.ComponentModel.DataAnnotations.Range(1, 12)]
            public int Grade { get; set; }
        }

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void ValidateProperty_UseIoC()
        {
            //Arrange
            IServiceCollection serviceCollection = Startup.CreateServiceCollection();
            serviceCollection.AddScoped<IClientRepository, ClientRepository>();
            var logger = new Mock<ILogger<ClientRepository>>();
            serviceCollection.AddScoped<ILogger<ClientRepository>>(prop => logger.Object);

            ServiceProvider serviceProvicder = serviceCollection.BuildServiceProvider();
            var validator = serviceProvicder.GetService<IValidator>();

            TestValidateClass objTest = new TestValidateClass()
            {
                Age = 23,
                Name = "23ertgey4dfbhdfhrerthrth5y56u5gege egfgghergh54y6u",
                Grade = 50
            };

            //Act
            var result = validator.Validate(objTest);

            //Assert
            Assert.IsTrue(result.Any(x => x.LabelCode.Equals("LabelCode01")));
        }

        [Test]
        public void ValidateModel_ClassHas2Errors_Return2Errors()
        {
            //Arrange
            TestValidateClass objTest = new TestValidateClass()
            {
                Age = 23,
                Name = "23ertgey4dfbhdfhrerthrth5y56u5gege egfgghergh54y6u",
                Grade = 50
            };

            //Act

            var result = new CommonLib.Validation.Validator(null).Validate(objTest);
            int numError = result.Count(x => x.Level.Equals("ERROR"));

            //Assert
            Assert.AreEqual(2, numError);
        }

        [Test]
        public void ValidateModel_ClassHasLabelCode_GetTheSameLabelCode()
        {
            //Arrange
            TestValidateClass objTest = new TestValidateClass()
            {
                Age = 23,
                Name = "23ertgey4dfbhdfhrerthrth5y56u5gege egfgghergh54y6u",
                Grade = 50,
                Family = "example family"
            };

            //Act

            var result = new CommonLib.Validation.Validator(null).Validate(objTest);

            //Assert
            Assert.IsTrue(result.Any(x => x.LabelCode.Equals("LabelCode01")));
        }
    }
}