﻿using CommonLib.Excel;
using CommonLib.Excel.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TestCommonLib
{
    public class TestImport
    {
        [Test]
        public void TestImportExcel()
        {
            string filePath = @"..\..\..\Data\import_file.xlsx";
            ExcelUtil excelParser = new ExcelUtil();
            List<Error> errors = new List<Error>();
            List<Type> sheetModels = new List<Type>
            {
                typeof(Transaction),
                typeof(MasterPayrollItem),
                typeof(PayrollCycle),
                typeof(Payroll)
            };

            Configuration configuration = new Configuration() { SheetTypes = sheetModels };
            configuration.BrowserTimeOffset = new TimeSpan(0, 0, 420);

            FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            var results = excelParser.Parse(fs, configuration, ref errors);

            Assert.AreEqual(16, results[typeof(Transaction).Name].Count());
        }

        [Test]
        public void TestExportExcel()
        {
            string filePath = @"..\..\..\Data\export_file" + DateTime.Now.Millisecond + ".xlsx";
            ExcelUtil excelHandler = new ExcelUtil();

            List<Transaction> transactions = new List<Transaction>()
            {
                new Transaction
                {
                    TransactionId = "1",
                    DeleteTransaction = "YES",
                    EmployeeId = "EMP1",
                    ItemCode = "CODE1",
                    DisplayText = "DIS1",
                    CalculationStartDate = DateTime.Now,
                    CalculationEndDate = DateTime.Now,
                    TaxCycle = DateTime.Now,
                    EAO713Cycle = DateTime.Now,
                    MPFCycle = DateTime.Now,
                    BaseAmount = 1,
                    Units = 1,
                    FinalTransactionAmount = 1,
                    GLCode = "GL",
                    CostCenterCode = "COST",
                    InputCurrency = "INPUT",
                    PaymentCurrency = "PAYMENT",
                    ExchangeRate = 0,
                    FirstName = "ABCD",
                    PayrollItemName = "PAYROLL",
                    Sign = "+",
                    Recurrence = "RE",
                    Department = "DEP"
                }
            };
            List<MasterPayrollItem> masterPayrollItems = new List<MasterPayrollItem>
            {
                new MasterPayrollItem { ItemName = "1", PayrollCode = "1", Recurrence = "1", Sign = "1"},
                new MasterPayrollItem { ItemName = "2", PayrollCode = "2", Recurrence = "2", Sign = "2"},
            };

            List<Payroll> payrolls = new List<Payroll>
            {
                new Payroll { ClientId = "clientId1", DefaultPaymentCurrency = "currency1", ExportBy = "export1", ExportTime = DateTime.Now, PayrollRunId = "payrollrunid1"},
                new Payroll { ClientId = "clientId2", DefaultPaymentCurrency = "currency2", ExportBy = "export2", ExportTime = DateTime.Now, PayrollRunId = "payrollrunid2"}
            };

            FileStream fs = new FileStream(filePath, FileMode.CreateNew);
            excelHandler.Export(new Configuration(), fs, transactions, masterPayrollItems, payrolls);
        }
    }
}
