﻿using AutoMapper;
using CommonLib.Dtos;
using CommonLib.Models;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Authentication.Services.Impl;
using CoreServiceAPIs.Common.Dtos;
using CoreServiceAPIs.Common.Mapping;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace TestCoreService
{
    public class TestSecurityProfileService
    {
        private IMapper _mapper;

        [SetUp]
        public void Setup()
        {
            //auto mapper configuration
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperProfile());
            });
            _mapper = mockMapper.CreateMapper();
        }

        [Test]
        public void GetAll_Has2SecurityPofile_Return2SecurityPofile()
        {
            //Arrange
            var repository = new Mock<ISecurityProfileRepository>();
            ApiRequest<PaginationRequest> request = new ApiRequest<PaginationRequest>();
            request.Data = new PaginationRequest();
            repository.Setup(x => x.GetAll(request))
                    .ReturnsAsync(new List<TbSecSecurityProfile> {
                        new TbSecSecurityProfile(),
                        new TbSecSecurityProfile()
                    });
            SecurityProfileService service = new SecurityProfileService(repository.Object, _mapper);

            //Act
            var result = service.GetAll(request).Result;
            var securityProfiles = result.Data["securityPofiles"] as Pagination<SecurityProfileViewModel>;

            //Assert
            Assert.IsNotNull(securityProfiles);
            Assert.AreEqual(2, securityProfiles.Content.Count);
        }

        [Test]
        public void GetSecurityProfileById_HasSecurityPofile_ReturnSecurityPofile()
        {
            //Arrange
            int id = 1;
            ApiRequest request = new ApiRequest();
            var repository = new Mock<ISecurityProfileRepository>();
            repository.Setup(x => x.GetById(id))
                    .ReturnsAsync(new TbSecSecurityProfile() { SecurityProfileId = id });
            SecurityProfileService service = new SecurityProfileService(repository.Object, _mapper);

            //Act
            var result = service.GetById(id, request).Result;
            var securityProfile = result.Data["securityProfile"] as SecurityProfileViewModel;

            //Assert
            Assert.IsNotNull(securityProfile);
            Assert.AreEqual(1, securityProfile.SecurityProfileId);
        }

        [Test]
        public void CreateSecurityProfile_ReturnSecurityProfileId()
        {
            //Arrange
            ApiRequest<CreateSecProfileRequest> request = new ApiRequest<CreateSecProfileRequest>();
            request.Data = new CreateSecProfileRequest();
            string username = "test";
            var repository = new Mock<ISecurityProfileRepository>();
            repository.Setup(x => x.Create(request, username)).ReturnsAsync(1);
            SecurityProfileService service = new SecurityProfileService(repository.Object, _mapper);

            //Act
            var result = service.Create(request, username).Result;
            var securityProfileId = Convert.ToInt32(result.Data["securityProfileId"]);

            //Assert
            Assert.IsNotNull(securityProfileId);
            Assert.AreEqual(1, securityProfileId);
        }

        [Test]
        public void UpdateSecurityProfile_RerturnSuccess()
        {
            //Arrange
            int securityId = 1;
            string username = "test";
            ApiRequest<UpdateSecProfileRequest> request = new ApiRequest<UpdateSecProfileRequest>();
            request.Data = new UpdateSecProfileRequest();
            request.Data.Name = "test";

            var repository = new Mock<ISecurityProfileRepository>();
            repository.Setup(x => x.CheckSecurityProfileById(securityId)).Returns(true);
            repository.Setup(x => x.Update(securityId, username, request)).ReturnsAsync(true);
            SecurityProfileService service = new SecurityProfileService(repository.Object, _mapper);

            //Act
            var result = service.Update(securityId, username, request).Result;

            //Assert
            Assert.IsNotNull(result);
        }

        [Test]
        public void UpdateSecurityProfile_RerturnFail()
        {
            //Arrange
            int securityId = 1;
            string username = "test";
            ApiRequest<UpdateSecProfileRequest> request = new ApiRequest<UpdateSecProfileRequest>();
            request.Data = new UpdateSecProfileRequest();

            var repository = new Mock<ISecurityProfileRepository>();
            repository.Setup(x => x.CheckSecurityProfileById(securityId)).Returns(false);
            repository.Setup(x => x.Update(securityId, username, request)).ReturnsAsync(false);
            SecurityProfileService service = new SecurityProfileService(repository.Object, _mapper);

            //Act
            var result = service.Update(securityId, username, request).Result;
            var data = result.Data;

            //Assert
            Assert.IsNull(data);
        }

        [Test]
        public void DeleteSecurityProfile_ReturnSuccess()
        {
            //Arrange
            int id = 1;
            ApiRequest request = new ApiRequest();
            var repository = new Mock<ISecurityProfileRepository>();
            repository.Setup(x => x.CheckSecurityProfileById(id)).Returns(true);
            repository.Setup(x => x.Delete(id)).ReturnsAsync(true);
            SecurityProfileService service = new SecurityProfileService(repository.Object, _mapper);

            //Act
            var result = service.Delete(id, request).Result;
            var data = result.Data;

            //Assert
            Assert.IsNotNull(data);
        }

        [Test]
        public void DeleteSecurityProfile_ReturnFail()
        {
            //Arrange
            int id = 1;
            ApiRequest request = new ApiRequest();
            var repository = new Mock<ISecurityProfileRepository>();
            repository.Setup(x => x.CheckSecurityProfileById(id)).Returns(false);
            repository.Setup(x => x.Delete(id)).ReturnsAsync(false);
            SecurityProfileService service = new SecurityProfileService(repository.Object, _mapper);

            //Act
            var result = service.Delete(id, request).Result;
            var data = result.Data;

            //Assert
            Assert.IsNull(data);
        }
    }
}