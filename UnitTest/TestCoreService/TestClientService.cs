﻿using AutoMapper;
using EmploymentServiceAPIs.Common.Dtos;
using EmploymentServiceAPIs.Common.Mapping;
using EmploymentServiceAPIs.Common.Repositories.Interfaces;
using EmploymentServiceAPIs.Common.Services.Impl;
using EmploymentServiceAPIs.Common.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace TestCoreService
{
    public class TestClientService
    {
        private IMapper _mapper;

        [SetUp]
        public void Setup()
        {
            //auto mapper configuration
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperProfile());
            });
            _mapper = mockMapper.CreateMapper();
        }

        [Test]
        public void GetClients_Has2Clients_Return2Clients()
        {
            //Arrange
            var clientRepo = new Mock<ITbCamClientRepository>();
            var _httpContextAccessor = new Mock<IHttpContextAccessor>();
            clientRepo.Setup(x => x.GetClients()).ReturnsAsync(new List<ClientViewModel>() { new ClientViewModel(), new ClientViewModel() });
            var logger = new Mock<ILogger<TbCamClientService>>();
            ITbCamClientService clientService = new TbCamClientService(clientRepo.Object, _mapper, _httpContextAccessor.Object, logger.Object);

            //Act
            var clients = clientService.GetClients().Result;
            var result = clients.Data["clients"] as List<ClientViewModel>;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
        }
    }
}