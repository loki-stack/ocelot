﻿using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Authentication.Models;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Authentication.Services.Impl;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using CoreServiceAPIs.Common.Dtos;
using CoreServiceAPIs.Common.Interfaces;
using CoreServiceAPIs.Common.Models;
using CoreServiceAPIs.Common.ShardingUtil;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Moq;
using NUnit.Framework;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading;

namespace TestCoreService
{
    public class TestAuthenticationService
    {
        private readonly string SecretKey = "Thisismytestprivatekey";
        private readonly string TokenExpires = "1";
        private readonly string RefreshTokenExpries = "1";
        [SetUp]
        public void Setup()
        {

        }

        [TestCase("test1", "password1")]
        public void Authenticate_LoginWithAccount_ReturnSuccess(string account, string password)
        {
            //Arrange
            ApiRequest<AuthenticateRequest> apiRequest = new ApiRequest<AuthenticateRequest>();
            apiRequest.Data.Account = account;
            apiRequest.Data.Password = password;
            PaginationRequest request = new PaginationRequest();
            var userRepository = new Mock<IUserRepository>();
            userRepository.Setup(x => x.GetUserByUserName(account))
                .ReturnsAsync(new TbSecUser
                {
                    UserName = account,
                    Password = Utils.MD5Hash(account, password),
                    ClientId = new Random().Next(1, 10)
                });
            var appSettings = new Mock<IOptions<CoreServiceAPIs.Common.Models.AppSettings>>();
            appSettings.Setup(x => x.Value).Returns(new CoreServiceAPIs.Common.Models.AppSettings { Secret = SecretKey, TokenExpires = TokenExpires, RefreshTokenExpries = RefreshTokenExpries });
            var refreshTokenRepository = new Mock<IRefreshTokenRepository>();
            var tokenValidationParameters = new Mock<TokenValidationParameters>();
            var functionRepository = new Mock<IFunctionRepository>();
            var logger = new Mock<ILogger<JwtAuthenticationService>>();
            IJwtAuthenticationService authenticationService = new JwtAuthenticationService(appSettings.Object, userRepository.Object, refreshTokenRepository.Object, tokenValidationParameters.Object, functionRepository.Object, logger.Object);

            //Act
            var result = authenticationService.Authenticate(apiRequest);

            //Assert
            var authen = result.Result.Data["authen"] as AuthenticationResult;
            Assert.IsNotNull(authen);
        }

        [TestCase("test1", "password1")]
        public void Authenticate_LoginWithWrongAccount_ReturnFail(string account, string password)
        {
            //Arrange
            ApiRequest<AuthenticateRequest> apiRequest = new ApiRequest<AuthenticateRequest>();
            apiRequest.Data.Account = account;
            apiRequest.Data.Password = password;
            PaginationRequest request = new PaginationRequest();
            var userRepository = new Mock<IUserRepository>();
            userRepository.Setup(x => x.GetUserByUserName(account))
                .ReturnsAsync(new TbSecUser
                {
                    UserName = account + "test",
                    Password = Utils.MD5Hash(account, password),
                    ClientId = new Random().Next(1, 10)
                });
            var appSettings = new Mock<IOptions<CoreServiceAPIs.Common.Models.AppSettings>>();
            appSettings.Setup(x => x.Value).Returns(new CoreServiceAPIs.Common.Models.AppSettings { Secret = SecretKey, TokenExpires = TokenExpires, RefreshTokenExpries = RefreshTokenExpries });
            var refreshTokenRepository = new Mock<IRefreshTokenRepository>();
            var tokenValidationParameters = new Mock<TokenValidationParameters>();
            var functionRepository = new Mock<IFunctionRepository>();
            var logger = new Mock<ILogger<JwtAuthenticationService>>();
            IJwtAuthenticationService authenticationService = new JwtAuthenticationService(appSettings.Object, userRepository.Object, refreshTokenRepository.Object, tokenValidationParameters.Object, functionRepository.Object, logger.Object);

            //Act
            var result = authenticationService.Authenticate(apiRequest);

            //Assert
            var authen = new AuthenticationResult();
            if (result.Result.Data.ContainsKey("authen"))
            {
                authen = result.Result.Data["authen"] as AuthenticationResult;
            }
            else authen = null;
            Assert.IsNull(authen);
        }

        [TestCase("test1", "password1", Ignore = "Must wait for token expried")]
        public void RefreshToken_ReturnSuccess(string account, string password)
        {
            //Arrange
            ApiRequest<AuthenticateRequest> apiRequest = new ApiRequest<AuthenticateRequest>();
            apiRequest.Data.Account = account;
            apiRequest.Data.Password = password;
            PaginationRequest request = new PaginationRequest();
            var userRepository = new Mock<IUserRepository>();
            var logger = new Mock<ILogger<JwtAuthenticationService>>();
            string token = new Guid().ToString();
            TbSecRefreshToken refreshToken = new TbSecRefreshToken
            {
                Token = token
            };
            var user = new TbSecUser
            {
                UserId = new Random().Next(1, 10),
                UserName = account,
                Password = Utils.MD5Hash(account, password),
                ClientId = new Random().Next(1, 10)
            };
            userRepository.Setup(x => x.GetUserByUserName(account))
                .ReturnsAsync(user);
            userRepository.Setup(x => x.Find(user.UserId)).ReturnsAsync(user);

            var appSettings = new Mock<IOptions<CoreServiceAPIs.Common.Models.AppSettings>>();
            appSettings.Setup(x => x.Value)
                .Returns(new CoreServiceAPIs.Common.Models.AppSettings
                {
                    Secret = SecretKey,
                    TokenExpires = TokenExpires,
                    RefreshTokenExpries = RefreshTokenExpries
                });

            var refreshTokenRepository = new Mock<IRefreshTokenRepository>();
            refreshTokenRepository.Setup(x => x.GetInformation(token))
                .ReturnsAsync(new TbSecRefreshToken
                {
                    Token = token,
                    ExpiryDate = DateTime.Now
                });

            var tokenValidationParameters = new Mock<TokenValidationParameters>();
            tokenValidationParameters.Setup(x => x.Clone()).Returns(new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SecretKey)),
                ValidateIssuer = false,
                ValidateAudience = false,
                RequireExpirationTime = false,
                ValidateLifetime = true
            });
            var functionRepository = new Mock<IFunctionRepository>();
            IJwtAuthenticationService authenticationService = new JwtAuthenticationService(appSettings.Object, userRepository.Object, refreshTokenRepository.Object, tokenValidationParameters.Object, functionRepository.Object, logger.Object);
                ;

            //Act
            var authenResult = authenticationService.Authenticate(apiRequest);
            var authen = authenResult.Result.Data["authen"] as AuthenticationResult;

            //wait token expried
            Thread.Sleep(61000);
            var validatedToken = authenticationService.GetPrincipalFromToken(authen.Token);
            refreshTokenRepository.Setup(x => x.GetInformation(token))
                .ReturnsAsync(new TbSecRefreshToken
                {
                    Token = token,
                    ExpiryDate = DateTime.Now,
                    JwtId = validatedToken.Claims.Single(x => x.Type == JwtRegisteredClaimNames.Jti).Value
                });
            authenticationService = new JwtAuthenticationService(appSettings.Object, userRepository.Object, refreshTokenRepository.Object, tokenValidationParameters.Object, functionRepository.Object, logger.Object);

            ApiRequest<RefreshTokenRequest> refreshRequest = new ApiRequest<RefreshTokenRequest>();
            refreshRequest.Data.Token = authen.Token;
            refreshRequest.Data.RefreshToken = refreshToken.Token;
            var refreshResult = authenticationService.RefreshToken(refreshRequest);
            var refresh = refreshResult.Result.Data["authen"] as AuthenticationResult;

            //Assert
            Assert.IsNotNull(refreshResult);
        }

    }
}
