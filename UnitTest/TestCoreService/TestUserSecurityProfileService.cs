﻿using CommonLib.Dtos;
using CommonLib.Models;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Authentication.Services.Impl;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace TestCoreService
{
    public class TestUserSecurityProfileService
    {
        [Test]
        [TestCase(1, 1)]
        public void GetUserSecurityProfile_ReturnSuccess(int userId, int securityProfileId)
        {
            ApiRequest<GetUserSecurityProfileRequest> request = new ApiRequest<GetUserSecurityProfileRequest>();
            request.Data.UserId = userId;
            var userSecurityProfileRepository = new Mock<IUserSecurityProfileRepository>();
            userSecurityProfileRepository.Setup(x => x.GetUserSecurityProfile(userId, securityProfileId))
                .ReturnsAsync(new TbSecUserSecurityProfile());
            //TODO: Chyi: Please check here again: out is TbSecUserSecurityProfile, not a list. I temparily fix it to build successfully

            //.ReturnsAsync(new List<UserSecurityProfileViewModel>
            //{
            //    new UserSecurityProfileViewModel
            //    {
            //        UserId = userId,
            //        SecurityProfileId = 1,
            //    },
            //    new UserSecurityProfileViewModel
            //    {
            //        UserId = userId,
            //        SecurityProfileId = 2,
            //    },
            //    new UserSecurityProfileViewModel
            //    {
            //        UserId = userId,
            //        SecurityProfileId = 3,
            //    }
            //});
            IUserSecurityProfileService userSecurityProfileService = new UserSecurityProfileService(userSecurityProfileRepository.Object);
            var result = userSecurityProfileService.GetUserSecurityProfile(request);
            var data = result.Result.Data["userSecurityProfiles"] as List<UserSecurityProfileViewModel>;
            Assert.AreEqual(3, data.Count);
        }

        [Test]
        [TestCase(1, 1)]
        public void CreateUserSecurityProfile_ReturnSuccess(int userId, int securityProfileId)
        {
            ApiRequest<UserSecurityProfileRequest> request = new ApiRequest<UserSecurityProfileRequest>();
            request.Data.UserId = userId;
            request.Data.SecurityProfileId = securityProfileId;
            var userSecurityProfileRepository = new Mock<IUserSecurityProfileRepository>();
            userSecurityProfileRepository.Setup(x => x.GetUserSecurityProfile(userId, securityProfileId));
            userSecurityProfileRepository.Setup(x => x.CreateUserSecurityProfile(userId, securityProfileId))
                .ReturnsAsync(new TbSecUserSecurityProfile
                {
                    UserId = userId,
                    SecurityProfileId = securityProfileId
                });
            IUserSecurityProfileService userSecurityProfileService = new UserSecurityProfileService(userSecurityProfileRepository.Object);
            var result = userSecurityProfileService.CreateUserSecurityProfile(request);
            var data = result.Result.Data["userSecurityProfile"] as TbSecUserSecurityProfile;
            Assert.IsNotNull(data);
        }

        [Test]
        [TestCase(1, 1)]
        public void CreateUserSecurityProfile_ReturlFail(int userId, int securityProfileId)
        {
            ApiRequest<UserSecurityProfileRequest> request = new ApiRequest<UserSecurityProfileRequest>();
            request.Data.UserId = userId;
            request.Data.SecurityProfileId = securityProfileId;
            var userSecurityProfileRepository = new Mock<IUserSecurityProfileRepository>();
            userSecurityProfileRepository.Setup(x => x.GetUserSecurityProfile(userId, securityProfileId))
                .ReturnsAsync(new TbSecUserSecurityProfile
                {
                    UserId = userId,
                    SecurityProfileId = securityProfileId
                });
            IUserSecurityProfileService userSecurityProfileService = new UserSecurityProfileService(userSecurityProfileRepository.Object);
            var result = userSecurityProfileService.CreateUserSecurityProfile(request);
            var data = result.Result.Data;
            Assert.IsNull(data);
        }

        [Test]
        [TestCase(1, 1)]
        public void DeleteUserSecurityProfile_ReturnSuccess(int userId, int securityProfileId)
        {
            ApiRequest<UserSecurityProfileRequest> request = new ApiRequest<UserSecurityProfileRequest>();
            request.Data.UserId = userId;
            request.Data.SecurityProfileId = securityProfileId;
            var userSecurityProfileRepository = new Mock<IUserSecurityProfileRepository>();
            userSecurityProfileRepository.Setup(x => x.GetUserSecurityProfile(userId, securityProfileId))
               .ReturnsAsync(new TbSecUserSecurityProfile
               {
                   UserId = userId,
                   SecurityProfileId = securityProfileId
               });
            userSecurityProfileRepository.Setup(x => x.DeleteUserSecurityProfile(userId, securityProfileId))
                .ReturnsAsync(true);
            IUserSecurityProfileService userSecurityProfileService = new UserSecurityProfileService(userSecurityProfileRepository.Object);
            var result = userSecurityProfileService.DeleteUserSecurityProfile(request);
            var data = result.Result.Data;
            Assert.IsNotNull(data);
        }

        [Test]
        [TestCase(1, 1)]
        public void DeleteUserSecurityProfile_ReturnFail(int userId, int securityProfileId)
        {
            ApiRequest<UserSecurityProfileRequest> request = new ApiRequest<UserSecurityProfileRequest>();
            request.Data.UserId = userId;
            request.Data.SecurityProfileId = securityProfileId;
            var userSecurityProfileRepository = new Mock<IUserSecurityProfileRepository>();
            userSecurityProfileRepository.Setup(x => x.GetUserSecurityProfile(userId, securityProfileId));
            IUserSecurityProfileService userSecurityProfileService = new UserSecurityProfileService(userSecurityProfileRepository.Object);
            var result = userSecurityProfileService.DeleteUserSecurityProfile(request);
            var data = result.Result.Data;
            Assert.IsNull(data);
        }
    }
}
