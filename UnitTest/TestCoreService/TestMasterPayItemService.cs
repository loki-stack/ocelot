﻿using CommonLib.Dtos;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace TestCoreService
{
    public class TestMasterPayItemService
    {
        [Test]
        public void GetAll_ReturnSuccess()
        {
            ApiRequest request = new ApiRequest();
            var masterPayItemRepository = new Mock<IMasterPayItemRepository>();
            masterPayItemRepository.Setup(x => x.GetAll())
                .ReturnsAsync(new List<TbCamMasterPayItem>() {
                    new TbCamMasterPayItem { ItemId = 1, ItemCode = "CODE1", Description = "DESCRIPTION1" },
                    new TbCamMasterPayItem { ItemId = 2, ItemCode = "CODE2", Description = "DESCRIPTION2" },
                });
            IMasterPayItemService masterPayItemService = new MasterPayItemService(masterPayItemRepository.Object);

            var result = masterPayItemService.GetAll(request);
            var masterPayItems = result.Result.Data["masterPayItems"] as List<TbCamMasterPayItem>;
            Assert.AreEqual(2, masterPayItems.Count);
        }

        [Test]
        [TestCase("CODE")]
        public void GetByCode_ReturnSuccess(string itemCode)
        {
            ApiRequest request = new ApiRequest();
            var masterPayItemRepository = new Mock<IMasterPayItemRepository>();
            masterPayItemRepository.Setup(x => x.GetByCode(itemCode))
                .ReturnsAsync(new TbCamMasterPayItem { ItemId = 1, ItemCode = itemCode, Description = "DESCRIPTION1" });
            IMasterPayItemService masterPayItemService = new MasterPayItemService(masterPayItemRepository.Object);

            var result = masterPayItemService.GetByCode(itemCode, request);
            var masterPayItem = result.Result.Data["masterPayItem"] as TbCamMasterPayItem;
            Assert.IsNotNull(masterPayItem);
        }

        [Test]
        [TestCase("CODE")]
        public void GetByCode_ReturnFailed(string itemCode)
        {
            ApiRequest request = new ApiRequest();
            var masterPayItemRepository = new Mock<IMasterPayItemRepository>();
            string itemCodeTest = itemCode + "abcd";
            masterPayItemRepository.Setup(x => x.GetByCode(itemCodeTest))
                .ReturnsAsync(new TbCamMasterPayItem { ItemId = 1, ItemCode = itemCodeTest, Description = "DESCRIPTION1" });
            IMasterPayItemService masterPayItemService = new MasterPayItemService(masterPayItemRepository.Object);

            var result = masterPayItemService.GetByCode(itemCode, request);
            Assert.IsNull(result.Result.Data);
        }
    }
}
