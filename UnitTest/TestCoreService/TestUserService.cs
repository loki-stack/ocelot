﻿using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using CommonLib.Services;
using CommonLib.Validation;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Authentication.Services.Impl;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using CoreServiceAPIs.Common.Dtos;
using CoreServiceAPIs.Common.Interfaces;
using CoreServiceAPIs.Common.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;
using System;

namespace TestCoreService
{
    public class TestUserService
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        [TestCase("test1")]
        public void CheckUserExist_HasUser_ReturnExist(string username)
        {
            //Arrange
            var userRepository = new Mock<IUserRepository>();
            var validator = new Mock<IValidator>();
            var smtpSetting = new Mock<IOptions<SmtpSettings>>();
            var mailSetting = new Mock<IOptions<MailSettings>>();
            var logger = new Mock<ILogger<UserService>>();
            var emailService = new Mock<IEmailService>();
            var sendEmailService = new Mock<ISendEmailService>();
            var appSetting = new Mock<IOptions<CoreServiceAPIs.Common.Models.AppSettings>>();
            var clientRepository = new Mock<IClientRepository>();
            userRepository.Setup(x => x.GetUserByUserName(username)).ReturnsAsync(new TbSecUser { UserName = username });
            IUserService userService = new UserService(userRepository.Object, validator.Object, smtpSetting.Object, mailSetting.Object,
                                                        emailService.Object, sendEmailService.Object, appSetting.Object, logger.Object, clientRepository.Object);

            var result = userService.CheckUserExists(username);
            var user = result.Result;

            Assert.IsNotNull(user);
        }

        [Test]
        [TestCase("test1")]
        public void CheckUserExist_HasNotUser_ReturnFail(string username)
        {
            //Arrange
            var userRepository = new Mock<IUserRepository>();
            var validator = new Mock<IValidator>();
            var smtpSetting = new Mock<IOptions<SmtpSettings>>();
            var mailSetting = new Mock<IOptions<MailSettings>>();
            var logger = new Mock<ILogger<UserService>>();
            var emailService = new Mock<IEmailService>();
            var sendEmailService = new Mock<ISendEmailService>();
            var appSetting = new Mock<IOptions<CoreServiceAPIs.Common.Models.AppSettings>>();
            var clientRepository = new Mock<IClientRepository>();
            userRepository.Setup(x => x.GetUserByUserName(username)).ReturnsAsync(new TbSecUser { UserName = username });
            IUserService userService = new UserService(userRepository.Object, validator.Object, smtpSetting.Object, mailSetting.Object,
                                                        emailService.Object, sendEmailService.Object, appSetting.Object, logger.Object, clientRepository.Object);

            var result = userService.CheckUserExists(new Random().ToString());
            var user = result.Result;

            Assert.IsNull(user);
        }

        [Test]
        [TestCase(1)]
        public void GetUserById_HasUser_ReturnUserInfo(int userId)
        {
            //Arrange
            ApiRequest<int> request = new ApiRequest<int>();
            request.Data = userId;
            var userRepository = new Mock<IUserRepository>();
            var validator = new Mock<IValidator>();
            var smtpSetting = new Mock<IOptions<SmtpSettings>>();
            var mailSetting = new Mock<IOptions<MailSettings>>();
            var logger = new Mock<ILogger<UserService>>();
            var emailService = new Mock<IEmailService>();
            var sendEmailService = new Mock<ISendEmailService>();
            var appSetting = new Mock<IOptions<CoreServiceAPIs.Common.Models.AppSettings>>();
            var clientRepository = new Mock<IClientRepository>();
            userRepository.Setup(x => x.GetUserInformation(userId)).ReturnsAsync(new UserViewModel { UserId = userId });
            IUserService userService = new UserService(userRepository.Object, validator.Object, smtpSetting.Object, mailSetting.Object,
                                                        emailService.Object, sendEmailService.Object, appSetting.Object, logger.Object, clientRepository.Object);

            var result = userService.GetById(request);
            var user = result.Result.Data["user"] as UserViewModel;

            Assert.IsNotNull(user);
        }

        [Test]
        public void GetAll_Has2Users_Return2Users()
        {
            //Arrange
            ApiRequest<PaginationRequest> request = new ApiRequest<PaginationRequest>();
            var userRepository = new Mock<IUserRepository>();
            var validator = new Mock<IValidator>();
            var smtpSetting = new Mock<IOptions<SmtpSettings>>();
            var mailSetting = new Mock<IOptions<MailSettings>>();
            var logger = new Mock<ILogger<UserService>>();
            var emailService = new Mock<IEmailService>();
            var sendEmailService = new Mock<ISendEmailService>();
            var appSetting = new Mock<IOptions<CoreServiceAPIs.Common.Models.AppSettings>>();
            var clientRepository = new Mock<IClientRepository>();
            //userRepository.Setup(x => x.GetAll(request.Data)).ReturnsAsync(new List<UserViewModel>() { new UserViewModel(), new UserViewModel() });
            IUserService userService = new UserService(userRepository.Object, validator.Object, smtpSetting.Object, mailSetting.Object,
                                                        emailService.Object, sendEmailService.Object, appSetting.Object, logger.Object, clientRepository.Object);

            var result = userService.GetAll(request);
            var pagination = result.Result.Data["pagination"] as Pagination<UserViewModel>;
            var users = pagination.Content;

            Assert.IsNotNull(users);
            Assert.AreEqual(2, users.Count);
        }

        [Test]
        public void CreateUser_ReturnUserId()
        {
            ApiRequest<CreateUserRequest> request = new ApiRequest<CreateUserRequest>();
            var userRepository = new Mock<IUserRepository>();
            var validator = new Mock<IValidator>();
            var smtpSetting = new Mock<IOptions<SmtpSettings>>();
            var mailSetting = new Mock<IOptions<MailSettings>>();
            var logger = new Mock<ILogger<UserService>>();
            var emailService = new Mock<IEmailService>();
            var sendEmailService = new Mock<ISendEmailService>();
            var appSetting = new Mock<IOptions<CoreServiceAPIs.Common.Models.AppSettings>>();
            var clientRepository = new Mock<IClientRepository>();
            userRepository.Setup(x => x.CreateUser(request.Data, request.Parameter)).ReturnsAsync(new CreateObjectResult { Id = 1, Error = "", IsSuccess = true });
            IUserService userService = new UserService(userRepository.Object, validator.Object, smtpSetting.Object, mailSetting.Object,
                                                        emailService.Object, sendEmailService.Object, appSetting.Object, logger.Object, clientRepository.Object);

            request.Data.SSOId = "SSO test";
            request.Data.Username = "test test";
            request.Data.Password = "test test";
            request.Data.Email = "test@test.test";
            request.Data.Avatar = null;
            request.Data.Fullname = "full name test";
            request.Data.EntityId = 1;
            request.Data.RoleId = 1;
            request.Data.LanguageId = 1;
            request.Data.ClientName = "tenant test";

            var result = userService.CreateUser(request);
            var userId = Convert.ToInt32(result.Result.Data.Value);
            Assert.AreNotEqual(0, userId);
        }

        [Test]
        public void UpdateUser_ReturnSuccess()
        {
            ApiRequest<UpdateUserRequest> request = new ApiRequest<UpdateUserRequest>();
            var userRepository = new Mock<IUserRepository>();
            var validator = new Mock<IValidator>();
            var smtpSetting = new Mock<IOptions<SmtpSettings>>();
            var mailSetting = new Mock<IOptions<MailSettings>>();
            var logger = new Mock<ILogger<UserService>>();
            var emailService = new Mock<IEmailService>();
            var sendEmailService = new Mock<ISendEmailService>();
            var appSetting = new Mock<IOptions<CoreServiceAPIs.Common.Models.AppSettings>>();
            var clientRepository = new Mock<IClientRepository>();
            userRepository.Setup(x => x.UpdateUser(request.Data)).ReturnsAsync(new UpdateObjectResult { Error = "", IsSuccess = true });
            IUserService userService = new UserService(userRepository.Object, validator.Object, smtpSetting.Object, mailSetting.Object,
                                                        emailService.Object, sendEmailService.Object, appSetting.Object, logger.Object, clientRepository.Object);

            request.Data.Password = "test test";
            request.Data.StatusCD = "AT";
            request.Data.Fullname = "full name test";
            request.Data.Email = "test@gmail.com";

            var result = userService.UpdateUser(request);
            var data = result.Result.Data;

            //Assert
            Assert.IsNotNull(data);
        }
    }
}