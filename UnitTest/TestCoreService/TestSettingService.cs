﻿using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using CoreServiceAPIs.Authentication.Dtos;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Authentication.Services.Impl;
using CoreServiceAPIs.Authentication.Services.Interfaces;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System;

namespace TestCoreService
{
    public class TestSettingService
    {
        private readonly ILogger<SettingService> _logger;
        [Test]
        [TestCase(1, 1, true)]
        public void Setting_GetPDFSetting_ReturnSuccess(int clientId, int entityId, bool pdfStatus)
        {
            var settingRepository = new Mock<ISettingRepository>();
            settingRepository.Setup(x => x.GetSettingOfClientEntity(clientId, entityId))
                .ReturnsAsync(new TbCfgSetting
                {
                    SpecificClientId = clientId,
                    SpecificEntityId = entityId,
                    PdfStatus = pdfStatus,
                    PrimaryLanguage = "vi",
                    SecondaryLanguage = "en"
                });
            ISettingService settingService = new SettingService(settingRepository.Object, _logger);

            ApiRequest request = new ApiRequest
            {
                Parameter = new StdRequestParam
                {
                    ClientId = clientId,
                    EntityId = entityId
                }
            };
            var result = settingService.GetPDFSetting(request);
            var setting = result.Result.Data["pdfSetting"];
            Assert.AreEqual(setting, pdfStatus);
        }

        [Test]
        [TestCase(1, 1, true)]
        public void Setting_GetPDFSetting_ReturnFail(int clientId, int entityId, bool pdfStatus)
        {
            var settingRepository = new Mock<ISettingRepository>();
            settingRepository.Setup(x => x.GetSettingOfClientEntity(clientId, entityId))
                .ReturnsAsync(new TbCfgSetting
                {
                    SpecificClientId = clientId,
                    SpecificEntityId = entityId,
                    PdfStatus = !pdfStatus,
                    PrimaryLanguage = new Random().Next(1, 10).ToString(),
                    SecondaryLanguage = new Random().Next(1, 10).ToString()
                });
            ISettingService settingService = new SettingService(settingRepository.Object, _logger);

            ApiRequest request = new ApiRequest
            {
                Parameter = new StdRequestParam
                {
                    ClientId = clientId,
                    EntityId = entityId
                }
            };
            var result = settingService.GetPDFSetting(request);
            var setting = result.Result.Data["pdfSetting"];
            Assert.AreNotEqual(setting, pdfStatus);
        }

        [Test]
        [TestCase(1, 1)]
        public void Setting_GetLanguageSetting_ReturnSuccess(int clientId, int entityId)
        {
            var settingRepository = new Mock<ISettingRepository>();
            settingRepository.Setup(x => x.GetSettingOfClientEntity(clientId, entityId))
                .ReturnsAsync(new TbCfgSetting
                {
                    SpecificClientId = clientId,
                    SpecificEntityId = entityId,
                    PdfStatus = true,
                    PrimaryLanguage = new Random().Next(1, 10).ToString(),
                    SecondaryLanguage = new Random().Next(1, 10).ToString()
                });
            ISettingService settingService = new SettingService(settingRepository.Object, _logger);

            ApiRequest request = new ApiRequest
            {
                Parameter = new StdRequestParam
                {
                    ClientId = clientId,
                    EntityId = entityId
                }
            };
            var result = settingService.GetLanguageSetting(request);
            var primary = result.Result.Data["primaryLanguage"];
            var secondary = result.Result.Data["secondaryLanguage"];
            Assert.AreNotEqual(string.Empty, primary);
            Assert.AreNotEqual(string.Empty, secondary);
        }

        [Test]
        [TestCase(1, 1, true)]
        public void Setting_UpdatePDFSetting_ReturnSuccess(int clientId, int entityId, bool pdfStatus)
        {
            ApiRequest<SettingUpdatePDFRequest> request = new ApiRequest<SettingUpdatePDFRequest>
            {
                Parameter = new StdRequestParam
                {
                    ClientId = clientId,
                    EntityId = entityId
                },
                Data = new SettingUpdatePDFRequest()
                {
                    PDFStatus = pdfStatus
                }
            };

            var settingRepository = new Mock<ISettingRepository>();
            settingRepository.Setup(x => x.UpdatePDFSetting(clientId, entityId, pdfStatus));
            ISettingService settingService = new SettingService(settingRepository.Object, _logger);


            var result = settingService.UpdatePDFSetting(request);
            var data = result.Result.Data;

            //Assert
            Assert.IsNotNull(data);
        }

        [Test]
        [TestCase(1, 1, true)]
        public void Setting_UpdatePDFSetting_ReturnFail(int clientId, int entityId, bool pdfStatus)
        {
            ApiRequest<SettingUpdatePDFRequest> request = new ApiRequest<SettingUpdatePDFRequest>
            {
                Parameter = new StdRequestParam
                {
                    ClientId = clientId,
                    EntityId = entityId
                },
                Data = new SettingUpdatePDFRequest()
                {
                    PDFStatus = pdfStatus
                }
            };

            var settingRepository = new Mock<ISettingRepository>();
            settingRepository.Setup(x => x.UpdatePDFSetting(clientId, entityId, pdfStatus));
            ISettingService settingService = new SettingService(settingRepository.Object, _logger);


            var result = settingService.UpdatePDFSetting(request);
            var data = result.Result.Data;

            //Assert
            Assert.IsNotNull(data);
        }

        [Test]
        [TestCase(1, 1, "vi", "en")]
        public void Setting_UpdateLanguageSetting_ReturnSuccess(int clientId, int entityId, string primaryLanguage, string secondaryLanguage)
        {
            ApiRequest<SettingUpdateLanguageRequest> request = new ApiRequest<SettingUpdateLanguageRequest>
            {
                Parameter = new StdRequestParam
                {
                    ClientId = clientId,
                    EntityId = entityId
                },
                Data = new SettingUpdateLanguageRequest
                {
                    PrimaryLanguageCode = primaryLanguage,
                    SecondaryLanguageCode = secondaryLanguage
                }
            };

            var settingRepository = new Mock<ISettingRepository>();
            settingRepository.Setup(x => x.UpdateLanguageSetting(clientId, entityId, primaryLanguage, secondaryLanguage));
            ISettingService settingService = new SettingService(settingRepository.Object, _logger);

            var result = settingService.UpdateLanguageSetting(request);
            var data = result.Result.Data;

            //Assert
            Assert.IsNotNull(data);
        }

        [Test]
        [TestCase(1, 1, "vi", "en")]
        public void Setting_UpdateLanguageSetting_ReturnFail(int clientId, int entityId, string primaryLanguage, string secondaryLanguage)
        {
            ApiRequest<SettingUpdateLanguageRequest> request = new ApiRequest<SettingUpdateLanguageRequest>
            {
                Parameter = new StdRequestParam
                {
                    ClientId = clientId,
                    EntityId = entityId
                },
                Data = new SettingUpdateLanguageRequest
                {
                    PrimaryLanguageCode = primaryLanguage,
                    SecondaryLanguageCode = secondaryLanguage
                }
            };
            var settingRepository = new Mock<ISettingRepository>();
            settingRepository.Setup(x => x.UpdateLanguageSetting(clientId, entityId, primaryLanguage, secondaryLanguage));
            ISettingService settingService = new SettingService(settingRepository.Object, _logger);

            var result = settingService.UpdateLanguageSetting(request);
            var data = result.Result.Data;

            //Assert
            Assert.IsNotNull(data);
        }
    }
}
