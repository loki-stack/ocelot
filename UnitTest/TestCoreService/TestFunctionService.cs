﻿using AutoMapper;
using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Authentication.Services.Impl;
using CoreServiceAPIs.Common.Dtos;
using CoreServiceAPIs.Common.Mapping;
using CoreServiceAPIs.Common.Repositories.Interfaces;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace TestCoreService
{
    public class TestFunctionService
    {
        private IMapper _mapper;

        [SetUp]
        public void Setup()
        {
            //auto mapper configuration
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperProfile());
            });
            _mapper = mockMapper.CreateMapper();
        }

        [Test]
        public void GetAll_Has2Function_Return2Function()
        {
            //Arrange
            var _functionRepository = new Mock<IFunctionRepository>();
            var _cfgI18nRepository = new Mock<ICfgI18nRepository>();
            ApiRequest<PaginationRequest> request = new ApiRequest<PaginationRequest>();
            request.Data = new PaginationRequest();
            _functionRepository.Setup(x => x.GetAll(request))
                    .ReturnsAsync(new List<FuncViewModel> { new FuncViewModel(), new FuncViewModel() });
            FunctionService service = new FunctionService(_functionRepository.Object, _mapper, _cfgI18nRepository.Object);

            //Act
            var result = service.GetAll(request).Result;
            var functions = result.Data["funcs"] as Pagination<FuncViewModel>;

            //Assert
            Assert.IsNotNull(functions);
            Assert.AreEqual(2, functions.Content.Count);
        }

        [Test]
        public void GetSecurityProfileById_HasSecurityPofile_ReturnSecurityPofile()
        {
            //Arrange
            int id = 1;
            ApiRequest request = new ApiRequest();
            var _functionRepository = new Mock<IFunctionRepository>();
            var _cfgI18nRepository = new Mock<ICfgI18nRepository>();
            _functionRepository.Setup(x => x.GetById(id))
                    .ReturnsAsync(new FuncViewModel() { FunctionId = id });
            FunctionService service = new FunctionService(_functionRepository.Object, _mapper, _cfgI18nRepository.Object);

            //Act
            var result = service.GetById(id, request).Result;
            var func = result.Data["func"] as FuncViewModel;

            //Assert
            Assert.IsNotNull(func);
            Assert.AreEqual(1, func.FunctionId);
        }

        [Test]
        public void CreateFunction_ReturnFunctionId()
        {
            //Arrange
            string username = "test";
            ApiRequest<CreateFuncRequest> request = new ApiRequest<CreateFuncRequest>();
            request.Data = new CreateFuncRequest();
            var _functionRepository = new Mock<IFunctionRepository>();
            var _cfgI18nRepository = new Mock<ICfgI18nRepository>();
            _functionRepository.Setup(x => x.CheckExistFuncByName(request.Data.FunctionCd, 0)).Returns(false);
            _functionRepository.Setup(x => x.Create(request, username)).ReturnsAsync(1);

            FunctionService service = new FunctionService(_functionRepository.Object, _mapper, _cfgI18nRepository.Object);

            //Act
            var result = service.Create(request, username).Result;
            var funcId = Convert.ToInt32(result.Data["funcId"]);

            //Assert
            Assert.IsNotNull(funcId);
            Assert.AreEqual(1, funcId);
        }

        [Test]
        public void UpdateFunction_RerturnSuccess()
        {
            //Arrange
            string username = "test";
            ApiRequest<UpdateFuncRequest> request = new ApiRequest<UpdateFuncRequest>()
            {
                Data = new UpdateFuncRequest { FunctionId = 1 }
            };
            var _functionRepository = new Mock<IFunctionRepository>();
            var _cfgI18nRepository = new Mock<ICfgI18nRepository>();
            _functionRepository.Setup(x => x.CheckExistFuncById(request.Data.FunctionId)).Returns(true);
            _functionRepository.Setup(x => x.CheckExistFuncByName(request.Data.FunctionCd, request.Data.FunctionId)).Returns(false);
            _functionRepository.Setup(x => x.Update(request, username)).ReturnsAsync(true);

            FunctionService service = new FunctionService(_functionRepository.Object, _mapper, _cfgI18nRepository.Object);

            //Act
            var result = service.Update(request, username).Result;
            var data = result.Data;

            //Assert
            Assert.IsNotNull(data);
        }

        [Test]
        public void UpdateFunction_RerturnFail()
        {
            //Arrange
            string username = "test";
            ApiRequest<UpdateFuncRequest> request = new ApiRequest<UpdateFuncRequest>()
            {
                Data = new UpdateFuncRequest { FunctionId = 1 }
            };
            var _functionRepository = new Mock<IFunctionRepository>();
            var _cfgI18nRepository = new Mock<ICfgI18nRepository>();
            _functionRepository.Setup(x => x.CheckExistFuncById(request.Data.FunctionId)).Returns(false);
            _functionRepository.Setup(x => x.CheckExistFuncByName(request.Data.FunctionCd, request.Data.FunctionId)).Returns(true);
            _functionRepository.Setup(x => x.Update(request, username)).ReturnsAsync(true);

            FunctionService service = new FunctionService(_functionRepository.Object, _mapper, _cfgI18nRepository.Object);

            //Act
            var result = service.Update(request, username).Result;
            var data = result.Data;

            //Assert
            Assert.IsNull(data);
        }

        [Test]
        public void DeleteFunction_ReturnSuccess()
        {
            //Arrange
            int id = 1;
            ApiRequest request = new ApiRequest();
            var _functionRepository = new Mock<IFunctionRepository>();
            var _cfgI18nRepository = new Mock<ICfgI18nRepository>();
            _functionRepository.Setup(x => x.CheckExistFuncById(id)).Returns(true);
            _functionRepository.Setup(x => x.Delete(id)).ReturnsAsync(true);
            FunctionService service = new FunctionService(_functionRepository.Object, _mapper, _cfgI18nRepository.Object);

            //Act
            var result = service.Delete(id, request).Result;
            var data = result.Data;

            //Assert
            Assert.IsNotNull(data);
        }

        [Test]
        public void DeleteFunction_ReturnFail()
        {
            //Arrange
            int id = 1;
            ApiRequest request = new ApiRequest();
            var _functionRepository = new Mock<IFunctionRepository>();
            var _cfgI18nRepository = new Mock<ICfgI18nRepository>();
            _functionRepository.Setup(x => x.CheckExistFuncById(id)).Returns(false);
            _functionRepository.Setup(x => x.Delete(id)).ReturnsAsync(false);
            FunctionService service = new FunctionService(_functionRepository.Object, _mapper, _cfgI18nRepository.Object);

            //Act
            var result = service.Delete(id, request).Result;
            var data = result.Data;

            //Assert
            Assert.IsNull(data);
        }
    }
}