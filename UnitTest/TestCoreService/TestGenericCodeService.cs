﻿using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Authentication.Services.Impl;
using CoreServiceAPIs.Common.Dtos;
using CoreServiceAPIs.Common.Models;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace TestCoreService
{
    public class TestGenericCodeService
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void GetDLLGenericCodes_Has2DLLGenericCodes_Return2DLLGenericCodes()
        {
            //Arrange
            //ApiRequest request = new ApiRequest();

            //request.Parameter = new StdRequestParam()
            //{
            //    Locale = "en",
            //    ClientId = 0,
            //    EntityId = 0,
            //    GenericCodeList = new string[0]
            //};
            //List<string> lst = new List<string>();
            //var gcRepository = new Mock<IGenericCodeRepository>();
            //gcRepository.Setup(x => x.GetDDLGenericCodes(request.Parameter.ClientId, request.Parameter.EntityId, request.Parameter.Locale, lst))
            //        .ReturnsAsync(new Dictionary<string, List<GenericCodeResult>>() {
            //            { "country", new List<GenericCodeResult>(){ new GenericCodeResult(), new GenericCodeResult() } },
            //            { "locale", new List<GenericCodeResult>(){ new GenericCodeResult(), new GenericCodeResult() } },
            //        });
            //GenericCodeService service = new GenericCodeService(gcRepository.Object);

            ////Act
            //var genericCodes = service.GetDDLGenericCodes(request).Result;
            //var result = genericCodes.Data["genericCodes"] as Dictionary<string, List<GenericCodeResult>>;

            ////Assert
            //Assert.IsNotNull(result);
            //Assert.AreEqual(2, result["country"].Count);
        }

        [Test]
        public void GetModules_Has2Modules_Return2Modules()
        {
            //Arrange
            //ApiRequest request = new ApiRequest();
            //request.Parameter = new StdRequestParam()
            //{
            //    Locale = "en"
            //};
            //var gcRepository = new Mock<IGenericCodeRepository>();
            //gcRepository.Setup(x => x.GetModules(request.Parameter.Locale))
            //    .ReturnsAsync(new List<ModuleGenericCodeViewModel>() { new ModuleGenericCodeViewModel(), new ModuleGenericCodeViewModel() });
            //GenericCodeService service = new GenericCodeService(gcRepository.Object);

            ////Act
            //var modules = service.GetModules(request).Result;
            //var result = modules.Data["modules"] as List<ModuleGenericCodeViewModel>;

            ////Assert
            //Assert.IsNotNull(result);
            //Assert.AreEqual(2, result.Count);
        }

        [Test]
        public void GetFilterByModule_Has2GenericCodesByModule_Return2GenericCodesByModule()
        {
            //Arrange
            ApiRequest<GetGenericCodeRequest> request = new ApiRequest<GetGenericCodeRequest>();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 0,
                EntityId = 0
            };
            request.Data = new GetGenericCodeRequest()
            {
                CodeType = "COUNTRY",
            };

            var gcRepository = new Mock<IGenericCodeRepository>();
            gcRepository.Setup(x => x.GetFilterByModule(request.Parameter.ClientId, request.Parameter.EntityId, request.Parameter.Locale, request.Data.CodeType))
                                .ReturnsAsync(new List<GenericCodeViewModel> { new GenericCodeViewModel(), new GenericCodeViewModel() });
            GenericCodeService service = new GenericCodeService(gcRepository.Object);

            //Act
            var genericCode = service.GetFilterByModule(request).Result;
            var pagination = genericCode.Data["pagination"] as Pagination<GenericCodeViewModel>;
            var result = pagination.Content;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
        }

        [Test]
        public void GetGenericCodeById_HasGenericCode_ReturnGenericCode()
        {
            //Arrange
            int id = 1;
            ApiRequest request = new ApiRequest();
            request.Parameter = new StdRequestParam();
            request.Parameter.Locale = "en";
            var gcRepository = new Mock<IGenericCodeRepository>();
            gcRepository.Setup(x => x.GetById(id, request.Parameter.Locale))
                                .ReturnsAsync(new GenericCodeViewModel() { Id = id });
            GenericCodeService service = new GenericCodeService(gcRepository.Object);

            //Act
            var genericCode = service.GetById(1, request).Result;
            var result = genericCode.Data["genericCode"] as GenericCodeViewModel;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Id);
        }

        [Test]
        public void CreateGenericCode_ReturnGenericCode()
        {
            //Arrange
            ApiRequest<CreateGenericCodeRequest> request = new ApiRequest<CreateGenericCodeRequest>();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 0,
                EntityId = 0
            };
            request.Data = new CreateGenericCodeRequest()
            {
                ParentCodeId = 40,
                CodeValue = "Test",
                Status = "ACTIVE",
                DisplayOrder = 1,
                Name = "Test",
                Remark = "Test",
            };
            var gcRepository = new Mock<IGenericCodeRepository>();
            gcRepository.Setup(x => x.CheckExistI18N(request.Data.ParentCodeId, request.Parameter.ClientId, request.Parameter.EntityId, request.Parameter.Locale, request.Data.CodeValue, 0, 1))
                                    .Returns(false);
            gcRepository.Setup(x => x.Create(request.Parameter.Locale, request.Parameter.ClientId, request.Parameter.EntityId, request.Data))
                        .ReturnsAsync(new CreateObjectResult<GenericCodeViewModel> { Data = new GenericCodeViewModel() { CodeValue = "Test" } });
            GenericCodeService service = new GenericCodeService(gcRepository.Object);

            //Act
            var genericCode = service.Create(request).Result;
            var result = genericCode.Data["genericCode"] as GenericCodeViewModel;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Test", result.CodeValue);
        }

        [Test]
        public void UpdateGenericCode_ReturnGenericCode()
        {
            //Arrange
            ApiRequest<UpdateGenericCodeRequest> request = new ApiRequest<UpdateGenericCodeRequest>();
            request.Parameter = new StdRequestParam();
            request.Parameter.Locale = "en";
            int clientId = 0;
            int entityId = 0;
            request.Data = new UpdateGenericCodeRequest()
            {
                Id = 1,
                ParentCodeId = 40,
                CodeValue = "Test",
                Status = "ACTIVE",
                DisplayOrder = 1,
                Name = "Test",
                Remark = "Test",
            };
            var gcRepository = new Mock<IGenericCodeRepository>();
            gcRepository.Setup(x => x.CheckExistGenericCodeById(request.Data.Id)).Returns(true);
            gcRepository.Setup(x => x.CheckExistI18N(request.Data.ParentCodeId, clientId, entityId, request.Parameter.Locale, request.Data.CodeValue, 0, 1))
                                    .Returns(false);
            gcRepository.Setup(x => x.GetById(request.Data.Id, request.Parameter.Locale))
                        .ReturnsAsync(new GenericCodeViewModel
                        {
                            ClientId = 0,
                            EntityId = 0
                        }); ;
            gcRepository.Setup(x => x.Update(request.Parameter.Locale, request.Data)).ReturnsAsync(new UpdateObjectResult<GenericCodeViewModel> { Data = new GenericCodeViewModel() { Id = 1, CodeValue = "Test" } });
            GenericCodeService service = new GenericCodeService(gcRepository.Object);

            //Act
            var genericCode = service.Update(request).Result;
            var result = genericCode.Data["genericCode"] as GenericCodeViewModel;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Id);
        }
    }
}