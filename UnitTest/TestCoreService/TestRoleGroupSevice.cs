﻿using AutoMapper;
using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Authentication.Services.Impl;
using CoreServiceAPIs.Common.Dtos;
using CoreServiceAPIs.Common.Mapping;
using CoreServiceAPIs.Common.Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace TestCoreService
{
    public class TestRoleGroupSevice
    {
        private IMapper _mapper;

        [SetUp]
        public void Setup()
        {
            //auto mapper configuration
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperProfile());
            });
            _mapper = mockMapper.CreateMapper();
        }

        [Test]
        public void GetAll_Has2RoleGroup_Return2RoleGroup()
        {
            //Arrange
            var _roleGroupRepository = new Mock<IRoleGroupRepository>();
            var _functionRepository = new Mock<IFunctionRepository>();
            var _cfgI18nRepository = new Mock<ICfgI18nRepository>();
            var _logger = new Mock<ILogger<RoleGroupService>>();
            ApiRequest<PaginationRequest> request = new ApiRequest<PaginationRequest>();
            request.Data = new PaginationRequest();
            _roleGroupRepository.Setup(x => x.GetAll(request))
                    .ReturnsAsync(new List<RoleGroupViewModel> {
                        new RoleGroupViewModel(),
                        new RoleGroupViewModel()
                    });
            RoleGroupService service = new RoleGroupService(_roleGroupRepository.Object, _functionRepository.Object, _cfgI18nRepository.Object, _mapper, _logger.Object);

            //Act
            var result = service.GetAll(request).Result;
            var securityProfiles = result.Data["roleGroups"] as Pagination<RoleGroupViewModel>;

            //Assert
            Assert.IsNotNull(securityProfiles);
            Assert.AreEqual(2, securityProfiles.Content.Count);
        }

        [Test]
        public void GetRoleGroupById_HasRoleGroup_ReturnRoleGroup()
        {
            //Arrange
            int id = 1;
            ApiRequest request = new ApiRequest();
            var _roleGroupRepository = new Mock<IRoleGroupRepository>();
            var _functionRepository = new Mock<IFunctionRepository>();
            var _cfgI18nRepository = new Mock<ICfgI18nRepository>();
            var _logger = new Mock<ILogger<RoleGroupService>>();
            _roleGroupRepository.Setup(x => x.GetById(id))
                    .ReturnsAsync(new RoleGroupViewModel() { RoleGroupId = id });
            RoleGroupService service = new RoleGroupService(_roleGroupRepository.Object, _functionRepository.Object, _cfgI18nRepository.Object, _mapper, _logger.Object);

            //Act
            var result = service.GetById(id, request).Result;
            var roleGroup = result.Data["roleGroup"] as RoleGroupViewModel;

            //Assert
            Assert.IsNotNull(roleGroup);
            Assert.AreEqual(1, roleGroup.RoleGroupId);
        }

        [Test]
        public void CreateRoleGroup_ReturnRoleGroupId()
        {
            //Arrange
            string username = "test";
            ApiRequest<CreateRoleGroupRequest> request = new ApiRequest<CreateRoleGroupRequest>();
            var _roleGroupRepository = new Mock<IRoleGroupRepository>();
            var _functionRepository = new Mock<IFunctionRepository>();
            var _cfgI18nRepository = new Mock<ICfgI18nRepository>();
            var _logger = new Mock<ILogger<RoleGroupService>>();
            _roleGroupRepository.Setup(x => x.Create(request, username)).ReturnsAsync(1);
            RoleGroupService service = new RoleGroupService(_roleGroupRepository.Object, _functionRepository.Object, _cfgI18nRepository.Object, _mapper, _logger.Object);

            //Act
            var result = service.Create(request, username).Result;
            var roleGroupId = Convert.ToInt32(result.Data["roleGroupId"]);

            //Assert
            Assert.IsNotNull(roleGroupId);
            Assert.AreEqual(1, roleGroupId);
        }

        [Test]
        public void UpdateRoleGroup_RerturnSuccess()
        {
            //Arrange
            string username = "test";
            ApiRequest<CreateRoleGroupRequest> request = new ApiRequest<CreateRoleGroupRequest>()
            {
                Data = new CreateRoleGroupRequest { RoleGroupId = 1 }
            };
            var _roleGroupRepository = new Mock<IRoleGroupRepository>();
            var _functionRepository = new Mock<IFunctionRepository>();
            var _cfgI18nRepository = new Mock<ICfgI18nRepository>();
            var _logger = new Mock<ILogger<RoleGroupService>>();
            _roleGroupRepository.Setup(x => x.CheckRoleGroupById(request.Data.RoleGroupId)).Returns(true);
            _roleGroupRepository.Setup(x => x.Update(request, username)).ReturnsAsync(true);
            RoleGroupService service = new RoleGroupService(_roleGroupRepository.Object, _functionRepository.Object, _cfgI18nRepository.Object, _mapper, _logger.Object);

            //Act
            var result = service.Update(request, username).Result;
            var data = result.Data;

            //Assert
            Assert.IsNotNull(data);
        }

        [Test]
        public void UpdateRoleGroup_RerturnFail()
        {
            //Arrange
            string username = "test";
            ApiRequest<CreateRoleGroupRequest> request = new ApiRequest<CreateRoleGroupRequest>()
            {
                Data = new CreateRoleGroupRequest { RoleGroupId = 1 }
            };
            var _roleGroupRepository = new Mock<IRoleGroupRepository>();
            var _functionRepository = new Mock<IFunctionRepository>();
            var _cfgI18nRepository = new Mock<ICfgI18nRepository>();
            var _logger = new Mock<ILogger<RoleGroupService>>();
            _roleGroupRepository.Setup(x => x.CheckRoleGroupById(request.Data.RoleGroupId)).Returns(false);
            _roleGroupRepository.Setup(x => x.Update(request, username)).ReturnsAsync(false);
            RoleGroupService service = new RoleGroupService(_roleGroupRepository.Object, _functionRepository.Object, _cfgI18nRepository.Object, _mapper, _logger.Object);

            //Act
            var result = service.Update(request, username).Result;
            var data = result.Data;

            //Assert
            Assert.IsNull(data);
        }

        [Test]
        public void DeleteRoleGroup_ReturnSuccess()
        {
            //Arrange
            int id = 1;
            ApiRequest request = new ApiRequest();
            var _roleGroupRepository = new Mock<IRoleGroupRepository>();
            var _functionRepository = new Mock<IFunctionRepository>();
            var _cfgI18nRepository = new Mock<ICfgI18nRepository>();
            var _logger = new Mock<ILogger<RoleGroupService>>();
            _roleGroupRepository.Setup(x => x.CheckRoleGroupById(id)).Returns(true);
            _roleGroupRepository.Setup(x => x.Delete(id)).ReturnsAsync(true);
            RoleGroupService service = new RoleGroupService(_roleGroupRepository.Object, _functionRepository.Object, _cfgI18nRepository.Object, _mapper, _logger.Object);

            //Act
            var result = service.Delete(id, request).Result;
            var data = result.Data;

            //Assert
            Assert.IsNotNull(data);
        }

        [Test]
        public void DeleteRoleGroup_ReturnFail()
        {
            //Arrange
            int id = 1;
            ApiRequest request = new ApiRequest();
            var _roleGroupRepository = new Mock<IRoleGroupRepository>();
            var _functionRepository = new Mock<IFunctionRepository>();
            var _cfgI18nRepository = new Mock<ICfgI18nRepository>();
            var _logger = new Mock<ILogger<RoleGroupService>>();
            _roleGroupRepository.Setup(x => x.CheckRoleGroupById(id)).Returns(false);
            _roleGroupRepository.Setup(x => x.Delete(id)).ReturnsAsync(false);
            RoleGroupService service = new RoleGroupService(_roleGroupRepository.Object, _functionRepository.Object, _cfgI18nRepository.Object, _mapper, _logger.Object);

            //Act
            var result = service.Delete(id, request).Result;
            var data = result.Data;

            //Assert
            Assert.IsNull(data);
        }
    }
}