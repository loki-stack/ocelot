﻿using CommonLib.Dtos;
using CoreServiceAPIs.Authentication.Repositories.Interfaces;
using CoreServiceAPIs.Authentication.Services.Impl;
using CoreServiceAPIs.Common.Dtos;
using Moq;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;

namespace TestCoreService
{
    public class TestMenuService
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void GetMenu_Has2Menu_Return2Menu()
        {
            //Arrange
            int userId = 1;
            ApiRequest request = new ApiRequest();
            request.Parameter = new StdRequestParam();
            request.Parameter.ClientId = 0;
            request.Parameter.EntityId = 0;
            var repository = new Mock<IMenuRepository>();
            repository.Setup(x => x.GetMenu(userId, request))
                    .ReturnsAsync(new Dictionary<string, IList>() {
                        { "leftMenu", new List<LeftMenuResult>(){ new LeftMenuResult(), new LeftMenuResult() } },
                        { "rightMenu", new List<RightMenuResult>(){ new RightMenuResult(), new RightMenuResult() } }
                    });
            MenuService service = new MenuService(repository.Object);

            //Act
            var result = service.GetMenu(userId, request).Result;
            var menus = result.Data["menus"] as Dictionary<string, IList>;
            var leftMenu = menus["leftMenu"] as List<LeftMenuViewModel>;
            var rightMenu = menus["rightMenu"] as List<RightMenuViewModel>;

            //Assert
            Assert.IsNotNull(leftMenu);
            Assert.IsNotNull(rightMenu);
            Assert.AreEqual(1, leftMenu.Count);
            Assert.AreEqual(1, rightMenu.Count);
        }
    }
}