﻿using AutoMapper;
using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Client;
using CommonLib.Models.Core;
using CommonLib.Repositories.Interfaces;
using CommonLib.Validation;
using EmploymentServiceAPIs.Common.Mapping;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Common.Repositories.Interfaces;
using EmploymentServiceAPIs.Employee.Dtos.Dependent;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using EmploymentServiceAPIs.Employee.Services.Impl;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using TbCfgI18n = CommonLib.Models.Client.TbCfgI18n;

namespace TestEmploymentService
{
    public class TestDependentService
    {
        private IMapper _mapper;

        [SetUp]
        public void Setup()
        {
            //auto mapper configuration
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperProfile());
            });
            _mapper = mockMapper.CreateMapper();
        }

        [Test]
        public void GetDependentsByEmployeeId_Has2Dependents_Return2Dependents()
        {
            //Arrange
            int employeeId = 1;
            int clientId = 1;
            int entityId = 1;
            string ray = string.Empty;
            string[] includes = { "TbEmpDependent" };
            List<string> textKeys = new List<string>()
            {
                "TextKey1", "TextKey2"
            };
            var dependentRepository = new Mock<IDependentRepository>();
            var employeeRepository = new Mock<IEmployeeRepository>();
            var entityLanguageRepository = new Mock<IEntitySettingRepository>();
            var tbCfgI18NRepository = new Mock<ITbCfgI18NRepository>();
            var validator = new Mock<IValidator>();

            employeeRepository.Setup(x => x.GetSingleByCondition(x => x.EmployeeId == employeeId, includes))
                .ReturnsAsync(new TbEmpEmployee
                {
                    TbEmpDependent = new List<TbEmpDependent>()
                    {
                        new TbEmpDependent(),
                        new TbEmpDependent(),
                    }
                });

            tbCfgI18NRepository.Setup(x => x.GetMulti(x => textKeys.Contains(x.TextKey), null))
                .ReturnsAsync(new List<TbCfgI18n>()
                {
                    new TbCfgI18n(),
                    new TbCfgI18n()
                });

            entityLanguageRepository.Setup(x => x.GetSettingOfEntity(clientId, entityId))
                .ReturnsAsync(new TbCfgSetting());

            DependentService service = new DependentService(dependentRepository.Object, employeeRepository.Object, entityLanguageRepository.Object, tbCfgI18NRepository.Object, _mapper, validator.Object);

            //Act
            var dependents = service.GetByEmployeeId(clientId, entityId, employeeId, ray).Result;

            //Assert
            Assert.IsNotNull(dependents);
            Assert.AreEqual(2, dependents.Data.Dependents.Count);
        }

        [Test]
        public void GetDependentById_HasDependent_ReturnDependent()
        {
            //Arrange
            int id = 1;
            int clientId = 1;
            int entityId = 1;
            string ray = string.Empty;
            List<string> textKeys = new List<string>();
            string[] includes = { "TbEmpDependent" };
            var dependentRepository = new Mock<IDependentRepository>();
            var employeeRepository = new Mock<IEmployeeRepository>();
            var entityLanguageRepository = new Mock<IEntitySettingRepository>();
            var tbCfgI18NRepository = new Mock<ITbCfgI18NRepository>();
            var validator = new Mock<IValidator>();

            dependentRepository.Setup(x => x.GetById(id))
               .ReturnsAsync(new TbEmpDependent { DependentId = id });

            entityLanguageRepository.Setup(x => x.GetSettingOfEntity(clientId, entityId))
                .ReturnsAsync(new TbCfgSetting());

            tbCfgI18NRepository.Setup(x => x.GetMulti(x => textKeys.Contains(x.TextKey), null))
                .ReturnsAsync(new List<TbCfgI18n>()
                {
                    new TbCfgI18n(),
                    new TbCfgI18n()
                });

            entityLanguageRepository.Setup(x => x.GetSettingOfEntity(clientId, entityId))
                .ReturnsAsync(new TbCfgSetting());

            DependentService service = new DependentService(dependentRepository.Object, employeeRepository.Object, entityLanguageRepository.Object, tbCfgI18NRepository.Object, _mapper, validator.Object);

            //Act
            var dependent = service.GetById(clientId, entityId, id, ray).Result;

            //Assert
            Assert.IsNotNull(dependent);
            Assert.AreEqual(1, dependent.Data.DependentId);
        }

        [Test]
        public void SaveDependents_ReturnSuccess()
        {
            //Arrange
            int employeeId = 1;
            int clientId = 1;
            int entityId = 1;
            string ray = string.Empty;
            string username = string.Empty;
            string[] includes = { "TbEmpDependent" };
            TbEmpEmployee employee = new TbEmpEmployee();
            List<TbEmpDependent> lstDependent = new List<TbEmpDependent>();
            List<TbCfgI18n> lstI18N = new List<TbCfgI18n>();
            List<string> textKeys = new List<string>();

            var dependentRepository = new Mock<IDependentRepository>();
            var employeeRepository = new Mock<IEmployeeRepository>();
            var entityLanguageRepository = new Mock<IEntitySettingRepository>();
            var tbCfgI18NRepository = new Mock<ITbCfgI18NRepository>();
            var validator = new Mock<IValidator>();

            DependentSaveModel dependentSM = new DependentSaveModel()
            {
                Remark = "Text1",
                Dependents = new List<DependentModel>()
                {
                    new DependentModel(),
                    new DependentModel()
                }
            };

            validator.Setup(x => x.Validate(dependentSM, null))
              .Returns(new List<MessageItem>());

            foreach (var dependent in dependentSM.Dependents)
            {
                validator.Setup(x => x.Validate(dependent, null))
                    .Returns(new List<MessageItem>());
            }

            employeeRepository.Setup(x => x.CheckExistEmployeeById(employeeId))
                .Returns(true);
            employeeRepository.Setup(x => x.GetById(employeeId))
                .ReturnsAsync(new TbEmpEmployee());
            employeeRepository.Setup(x => x.Edit(employee))
                .ReturnsAsync(new TbEmpEmployee());
            employeeRepository.Setup(x => x.GetSingleByCondition(x => x.EmployeeId == employeeId, includes))
                .ReturnsAsync(new TbEmpEmployee
                {
                    TbEmpDependent = new List<TbEmpDependent>()
                    {
                                    new TbEmpDependent(),
                                    new TbEmpDependent(),
                    }
                });

            dependentRepository.Setup(x => x.Create(lstDependent))
                .ReturnsAsync(true);
            dependentRepository.Setup(x => x.Update(lstDependent))
                .ReturnsAsync(true);
            dependentRepository.Setup(x => x.GetById(1))
                .ReturnsAsync(new TbEmpDependent { });

            entityLanguageRepository.Setup(x => x.GetSettingOfEntity(clientId, entityId))
                .ReturnsAsync(new TbCfgSetting());

            tbCfgI18NRepository.Setup(x => x.GetMulti(x => textKeys.Contains(x.TextKey), null))
                .ReturnsAsync(new List<TbCfgI18n>()
                {
                                new TbCfgI18n(),
                                new TbCfgI18n()
                });
            tbCfgI18NRepository.Setup(x => x.CreateMany(lstI18N))
                 .ReturnsAsync(true);
            tbCfgI18NRepository.Setup(x => x.UpdateMany(lstI18N))
                .ReturnsAsync(true);

            DependentService service = new DependentService(dependentRepository.Object, employeeRepository.Object, entityLanguageRepository.Object, tbCfgI18NRepository.Object, _mapper, validator.Object);

            //Act
            var dependents = service.Save(clientId, entityId, employeeId, ray, username, dependentSM).Result;

            //Assert
            Assert.IsNotNull(dependents.Data);
        }

        [Test]
        public void SaveDependents_ReturnFail()
        {
            //Arrange
            int employeeId = 1;
            int clientId = 1;
            int entityId = 1;
            string ray = string.Empty;
            string username = string.Empty;
            string[] includes = { "TbEmpDependent" };
            TbEmpEmployee employee = new TbEmpEmployee();
            List<TbEmpDependent> lstDependent = new List<TbEmpDependent>();
            List<TbCfgI18n> lstI18N = new List<TbCfgI18n>();
            List<string> textKeys = new List<string>();

            var dependentRepository = new Mock<IDependentRepository>();
            var employeeRepository = new Mock<IEmployeeRepository>();
            var entityLanguageRepository = new Mock<IEntitySettingRepository>();
            var tbCfgI18NRepository = new Mock<ITbCfgI18NRepository>();
            var validator = new Mock<IValidator>();

            DependentSaveModel dependentSM = new DependentSaveModel()
            {
                Remark = "Text1",
                Dependents = new List<DependentModel>()
                {
                    new DependentModel(),
                    new DependentModel()
                }
            };

            validator.Setup(x => x.Validate(dependentSM, null))
              .Returns(new List<MessageItem>()
              {
                   new MessageItem()
              });

            foreach (var dependent in dependentSM.Dependents)
            {
                validator.Setup(x => x.Validate(dependent, null))
                    .Returns(new List<MessageItem>()
                    {
                         new MessageItem()
                    });
            }

            employeeRepository.Setup(x => x.CheckExistEmployeeById(employeeId))
                .Returns(true);
            employeeRepository.Setup(x => x.GetById(employeeId))
                .ReturnsAsync(new TbEmpEmployee());
            employeeRepository.Setup(x => x.Edit(employee))
                .ReturnsAsync(new TbEmpEmployee());
            employeeRepository.Setup(x => x.GetSingleByCondition(x => x.EmployeeId == employeeId, includes))
                .ReturnsAsync(new TbEmpEmployee
                {
                    TbEmpDependent = new List<TbEmpDependent>()
                    {
                                    new TbEmpDependent(),
                                    new TbEmpDependent(),
                    }
                });

            dependentRepository.Setup(x => x.Create(lstDependent))
                .ReturnsAsync(true);
            dependentRepository.Setup(x => x.Update(lstDependent))
                .ReturnsAsync(true);
            dependentRepository.Setup(x => x.GetById(1))
                .ReturnsAsync(new TbEmpDependent { });

            entityLanguageRepository.Setup(x => x.GetSettingOfEntity(clientId, entityId))
                .ReturnsAsync(new TbCfgSetting());

            tbCfgI18NRepository.Setup(x => x.GetMulti(x => textKeys.Contains(x.TextKey), null))
                .ReturnsAsync(new List<TbCfgI18n>()
                {
                                new TbCfgI18n(),
                                new TbCfgI18n()
                });
            tbCfgI18NRepository.Setup(x => x.CreateMany(lstI18N))
                 .ReturnsAsync(true);
            tbCfgI18NRepository.Setup(x => x.UpdateMany(lstI18N))
                .ReturnsAsync(true);

            DependentService service = new DependentService(dependentRepository.Object, employeeRepository.Object, entityLanguageRepository.Object, tbCfgI18NRepository.Object, _mapper, validator.Object);

            //Act
            var dependents = service.Save(clientId, entityId, employeeId, ray, username, dependentSM).Result;

            //Assert
            Assert.IsNull(dependents.Data);
        }
    }
}