using CommonLib.Models.Client;
using CommonLib.Services;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Repositories.Impl;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using EmploymentServiceAPIs.Employee.Services.Impl;
using EmploymentServiceAPIs.Employee.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System;

namespace TestEmploymentService
{
    public class TestEmployee
    {
        public IEmployeeRepository EmployeeRepository { get; set; }

        public Moq.Mock<IConnectionStringContainer> ConnectionStringContainer { get; set; }

        [SetUp]
        public void Setup()
        {
            IServiceCollection serviceCollection = Startup.CreateServiceCollection();

            ConnectionStringContainer = new Moq.Mock<IConnectionStringContainer>();
            ConnectionStringContainer.Setup(x => x.GetConnectionString()).Returns("Server=172.20.20.18;Database=HRIS_TENANT;MultipleActiveResultSets=true;User ID=sa;Password=123456a@");

            serviceCollection.AddScoped<IConnectionStringContainer>(provider =>
            {
                return ConnectionStringContainer.Object;
            });

            serviceCollection.AddScoped<IEmployeeRepository, EmployeeRepository>();
            serviceCollection.AddScoped<IEmployeeService, EmployeeService>();

            ServiceProvider serviceProvicder = serviceCollection.BuildServiceProvider();
            EmployeeRepository = serviceProvicder.GetService<IEmployeeRepository>();
        }

        [Test]
        public void GetHistory_EmployeeIsSingle_ReturnSingleStatus()
        {
            DateTimeOffset time = new DateTimeOffset(2020, 12, 25, 15, 30, 12, new TimeSpan(1, 0, 0));
            TbEmpEmployee employee = new TbEmpEmployee();
            //TbEmpEmployee employee = EmployeeRepository.GetHistory(2, time);
            Assert.AreEqual("SINGLE", employee.MartialStatus);
        }
    }
}