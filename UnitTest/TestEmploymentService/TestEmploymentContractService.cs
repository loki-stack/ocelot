﻿using AutoMapper;
using CommonLib.Dtos;
using CommonLib.Models.Client;
using CommonLib.Validation;
using EmploymentServiceAPIs.Common.Mapping;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Dtos.EmploymentContract;
using EmploymentServiceAPIs.Employee.Dtos.EmploymentStatus;
using EmploymentServiceAPIs.Employee.Dtos.Movement;
using EmploymentServiceAPIs.Employee.Dtos.Termination;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using EmploymentServiceAPIs.Employee.Services.Impl;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace TestEmploymentService
{
    public class TestEmploymentContractService
    {
        private IMapper _mapper;

        [SetUp]
        public void Setup()
        {
            //auto mapper configuration
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperProfile());
            });
            _mapper = mockMapper.CreateMapper();
        }

        [Test]
        public void GetContracts_Has2Contracts_Return2Contracts()
        {
            //Arrange
            int employeeId = 1;
            string ray = string.Empty;

            var ecRepository = new Mock<IEmploymentContractRepository>();
            var movementRepository = new Mock<IMovementRepository>();
            var employeeRepository = new Mock<IEmployeeRepository>();
            var empStatusRepository = new Mock<IEmploymentStatusRepository>();
            var terminationRepository = new Mock<ITerminationRepository>();
            var taxRepository = new Mock<ITaxRepository>();
            var validator = new Mock<IValidator>();

            ecRepository.Setup(x => x.GetByEmployeeId(employeeId))
                .ReturnsAsync(new List<TbEmpEmploymentContract>()
                {
                    new TbEmpEmploymentContract()
                    {
                        TbEmpMovement = new List<TbEmpMovement>(),
                        TbEmpTermination = new List<TbEmpTermination>(),
                    },
                    new TbEmpEmploymentContract()
                    {
                        TbEmpMovement = new List<TbEmpMovement>(),
                        TbEmpTermination = new List<TbEmpTermination>(),
                    }
                });

            empStatusRepository.Setup(x => x.GetByEmployeeId(employeeId))
                .ReturnsAsync(new List<EmploymentStatusViewModel>()
                {
                    new EmploymentStatusViewModel()
                });

            movementRepository.Setup(x => x.GetByEmployeeId(employeeId))
                .ReturnsAsync(new List<TbEmpMovement>
                {
                    new TbEmpMovement(),
                    new TbEmpMovement()
                });

            employeeRepository.Setup(x => x.GetById(employeeId))
                .ReturnsAsync(new TbEmpEmployee());

            EmploymentContractService service = new EmploymentContractService(ecRepository.Object, movementRepository.Object, employeeRepository.Object,
                taxRepository.Object, empStatusRepository.Object, terminationRepository.Object, _mapper, validator.Object);

            //Act
            var contracts = service.GetByEmployeeId(employeeId, ray).Result;

            //Assert
            Assert.IsNotNull(contracts);
            Assert.AreEqual(2, contracts.Data.EmploymentContracts.Count);
        }

        [Test]
        public void GetContract_HasContract_ReturnContract()
        {
            //Arrange
            int id = 1;
            string ray = string.Empty;

            var ecRepository = new Mock<IEmploymentContractRepository>();
            var movementRepository = new Mock<IMovementRepository>();
            var employeeRepository = new Mock<IEmployeeRepository>();
            var empStatusRepository = new Mock<IEmploymentStatusRepository>();
            var terminationRepository = new Mock<ITerminationRepository>();
            var taxRepository = new Mock<ITaxRepository>();
            var validator = new Mock<IValidator>();

            ecRepository.Setup(x => x.GetById(id))
                .ReturnsAsync(new TbEmpEmploymentContract());

            EmploymentContractService service = new EmploymentContractService(ecRepository.Object, movementRepository.Object, employeeRepository.Object,
                taxRepository.Object, empStatusRepository.Object, terminationRepository.Object, _mapper, validator.Object);

            //Act
            var contracts = service.GetById(id, ray).Result;

            //Assert
            Assert.IsNotNull(contracts);
        }

        [Test]
        public void SaveContracts_ReturnSuccess()
        {
            //Arrange
            int employeeId = 1;
            string ray = string.Empty;
            string username = "test";
            EmploymentContractSaveModel employmentContractSM = new EmploymentContractSaveModel()
            {
                RemarkMovement = string.Empty,
                RemarkEmploymentContract = string.Empty,
                EmploymentContracts = new List<EmploymentContractModel>()
                {
                    new EmploymentContractModel()
                    {
                        EmploymentContractId = 1,
                        EmployeeId = employeeId
                    }
                },
                Movements = new List<MovementModel>()
                {
                    new MovementModel()
                    {
                        AssignmentId = 0,
                        BusinessUnitId = 1,
                        WorkCalendar = 1,
                        DefaultSalaryCurrency = "Test",
                        DefaultPaymentCurrency = "Test",
                        EmploymentContractId = 1
                    }
                }
            };

            TbEmpEmployee employee = new TbEmpEmployee();
            List<TbEmpEmploymentContract> contractEntities = new List<TbEmpEmploymentContract>()
            {
                new TbEmpEmploymentContract()
            };

            List<TbEmpMovement> movementEntities = new List<TbEmpMovement>()
            {
                new TbEmpMovement()
            };

            var ecRepository = new Mock<IEmploymentContractRepository>();
            var movementRepository = new Mock<IMovementRepository>();
            var employeeRepository = new Mock<IEmployeeRepository>();
            var empStatusRepository = new Mock<IEmploymentStatusRepository>();
            var terminationRepository = new Mock<ITerminationRepository>();
            var taxRepository = new Mock<ITaxRepository>();
            var validator = new Mock<IValidator>();

            ecRepository.Setup(x => x.GetByEmployeeId(employeeId))
                .ReturnsAsync(new List<TbEmpEmploymentContract>()
                {
                    new TbEmpEmploymentContract()
                    {
                        EmploymentContractId = 1,
                        CreatedBy = "System",
                        CreatedDt = DateTime.Now,
                        EmployeeId = employeeId,
                        TbEmpMovement = new List<TbEmpMovement>(),
                        TbEmpTermination = new List<TbEmpTermination>(),
                    }
                });

            ecRepository.Setup(x => x.Update(contractEntities))
                .ReturnsAsync(true);

            ecRepository.Setup(x => x.GetById(1))
             .ReturnsAsync(new TbEmpEmploymentContract());

            foreach (var contract in employmentContractSM.EmploymentContracts)
            {
                validator.Setup(x => x.Validate(contract, null))
                    .Returns(new List<MessageItem>());

                ecRepository.Setup(x => x.CheckExistEContractById(contract.EmploymentContractId))
                    .Returns(true);
            }

            foreach (var movement in employmentContractSM.Movements)
            {
                validator.Setup(x => x.Validate(movement, null))
                    .Returns(new List<MessageItem>());

                ecRepository.Setup(x => x.CheckExistEContractById(movement.EmploymentContractId.Value))
                    .Returns(true);

                movementRepository.Setup(x => x.CheckExistMovementById(movement.AssignmentId))
                    .Returns(true);
            }

            employeeRepository.Setup(x => x.CheckExistEmployeeById(employeeId))
                .Returns(true);
            employeeRepository.Setup(x => x.Edit(employee))
                .ReturnsAsync(new TbEmpEmployee());
            employeeRepository.Setup(x => x.GetById(employeeId))
                .ReturnsAsync(new TbEmpEmployee());

            movementRepository.Setup(x => x.CreateMany(movementEntities))
                .ReturnsAsync(true);
            movementRepository.Setup(x => x.UpdateMany(movementEntities))
                .ReturnsAsync(true);
            movementRepository.Setup(x => x.GetByEmployeeId(employeeId))
                .ReturnsAsync(new List<TbEmpMovement>
                {
                    new TbEmpMovement()
                    {
                        AssignmentId = 1,
                        EmploymentContractId = 1
                    },
                    new TbEmpMovement()
                    {
                        AssignmentId = 2,
                        EmploymentContractId = 2
                    }
                });

            empStatusRepository.Setup(x => x.GetByEmployeeId(employeeId))
                .ReturnsAsync(new List<EmploymentStatusViewModel>()
                {
                    new EmploymentStatusViewModel()
                });

            employeeRepository.Setup(x => x.GetById(employeeId))
                .ReturnsAsync(new TbEmpEmployee());

            EmploymentContractService service = new EmploymentContractService(ecRepository.Object, movementRepository.Object, employeeRepository.Object,
                taxRepository.Object, empStatusRepository.Object, terminationRepository.Object, _mapper, validator.Object);

            //Act
            var contracts = service.Save(employeeId, ray, username, employmentContractSM).Result;

            //Assert
            Assert.IsNotNull(contracts.Data);
        }

        [Test]
        public void SaveContracts_ReturnFail()
        {
            //Arrange
            int employeeId = 1;
            string ray = string.Empty;
            string username = "test";
            EmploymentContractSaveModel employmentContractSM = new EmploymentContractSaveModel()
            {
                RemarkMovement = string.Empty,
                RemarkEmploymentContract = string.Empty,
                EmploymentContracts = new List<EmploymentContractModel>()
                {
                    new EmploymentContractModel()
                    {
                        EmploymentContractId = 1,
                        EmployeeId = employeeId
                    }
                },
                Movements = new List<MovementModel>()
                {
                    new MovementModel()
                    {
                        AssignmentId = 0,
                        EmploymentContractId = 1
                    }
                }
            };

            TbEmpEmployee employee = new TbEmpEmployee();
            List<TbEmpEmploymentContract> contractEntities = new List<TbEmpEmploymentContract>()
            {
                new TbEmpEmploymentContract()
            };

            List<TbEmpMovement> movementEntities = new List<TbEmpMovement>()
            {
                new TbEmpMovement()
            };

            var ecRepository = new Mock<IEmploymentContractRepository>();
            var movementRepository = new Mock<IMovementRepository>();
            var employeeRepository = new Mock<IEmployeeRepository>();
            var empStatusRepository = new Mock<IEmploymentStatusRepository>();
            var terminationRepository = new Mock<ITerminationRepository>();
            var taxRepository = new Mock<ITaxRepository>();
            var validator = new Mock<IValidator>();

            ecRepository.Setup(x => x.GetByEmployeeId(employeeId))
                .ReturnsAsync(new List<TbEmpEmploymentContract>()
                {
                    new TbEmpEmploymentContract()
                    {
                        EmploymentContractId = 1,
                        CreatedBy = "System",
                        CreatedDt = DateTime.Now,
                        EmployeeId = employeeId,
                        TbEmpMovement = new List<TbEmpMovement>(),
                        TbEmpTermination = new List<TbEmpTermination>(),
                    }
                });

            ecRepository.Setup(x => x.Update(contractEntities))
                .ReturnsAsync(true);

            ecRepository.Setup(x => x.GetById(1))
             .ReturnsAsync(new TbEmpEmploymentContract());

            foreach (var contract in employmentContractSM.EmploymentContracts)
            {
                validator.Setup(x => x.Validate(contract, null))
                    .Returns(new List<MessageItem>());

                ecRepository.Setup(x => x.CheckExistEContractById(contract.EmploymentContractId))
                    .Returns(true);
            }

            foreach (var movement in employmentContractSM.Movements)
            {
                validator.Setup(x => x.Validate(movement, null))
                    .Returns(new List<MessageItem>());

                ecRepository.Setup(x => x.CheckExistEContractById(movement.EmploymentContractId.Value))
                    .Returns(true);

                movementRepository.Setup(x => x.CheckExistMovementById(movement.AssignmentId))
                    .Returns(true);
            }

            employeeRepository.Setup(x => x.CheckExistEmployeeById(employeeId))
                .Returns(true);
            employeeRepository.Setup(x => x.Edit(employee))
                .ReturnsAsync(new TbEmpEmployee());
            employeeRepository.Setup(x => x.GetById(employeeId))
                .ReturnsAsync(new TbEmpEmployee());

            movementRepository.Setup(x => x.CreateMany(movementEntities))
                .ReturnsAsync(true);
            movementRepository.Setup(x => x.UpdateMany(movementEntities))
                .ReturnsAsync(true);
            movementRepository.Setup(x => x.GetByEmployeeId(employeeId))
                .ReturnsAsync(new List<TbEmpMovement>
                {
                    new TbEmpMovement()
                    {
                        AssignmentId = 1,
                        EmploymentContractId = 1
                    },
                    new TbEmpMovement()
                    {
                        AssignmentId = 2,
                        EmploymentContractId = 2
                    }
                });

            empStatusRepository.Setup(x => x.GetByEmployeeId(employeeId))
                .ReturnsAsync(new List<EmploymentStatusViewModel>()
                {
                    new EmploymentStatusViewModel()
                });

            employeeRepository.Setup(x => x.GetById(employeeId))
                .ReturnsAsync(new TbEmpEmployee());

            EmploymentContractService service = new EmploymentContractService(ecRepository.Object, movementRepository.Object, employeeRepository.Object,
                taxRepository.Object, empStatusRepository.Object, terminationRepository.Object, _mapper, validator.Object);

            //Act
            var contracts = service.Save(employeeId, ray, username, employmentContractSM).Result;

            //Assert
            Assert.IsNull(contracts.Data);
        }
    }
}