﻿using AutoMapper;
using CommonLib.Dtos;
using CommonLib.Models.Client;
using CommonLib.Validation;
using EmploymentServiceAPIs.Common.Mapping;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Dtos.ManageFile;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using EmploymentServiceAPIs.Employee.Services.Impl;
using Microsoft.AspNetCore.Http;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace TestEmploymentService
{
    public class TestFileService
    {
        private IMapper _mapper;

        [SetUp]
        public void Setup()
        {
            //auto mapper configuration
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperProfile());
            });
            _mapper = mockMapper.CreateMapper();
        }

        [Test]
        public void GetListFileByIds_Has2Files_Return2Files()
        {
            //Arrange
            int clientId = 1;
            int entityId = 1;
            string ray = string.Empty;
            List<int> ids = new List<int>();
            var fileRepository = new Mock<IFileRepository>();
            var validator = new Mock<IValidator>();

            fileRepository.Setup(x => x.GetListFileByIds(ids))
               .ReturnsAsync(new List<TbComFile>()
               {
                   new TbComFile(),
                   new TbComFile()
               });

            FileService service = new FileService(fileRepository.Object, _mapper, validator.Object);
            //Act
            var files = service.GetListFileByIds(clientId, entityId, ray, ids).Result;

            //Assert
            Assert.IsNotNull(files);
            Assert.AreEqual(2, files.Data.Count);
        }

        [Test]
        public void DownloadFile_ReturnSuccess()
        {
            //Arrange
            int id = 1;
            string ray = string.Empty;
            var fileRepository = new Mock<IFileRepository>();
            var validator = new Mock<IValidator>();

            fileRepository.Setup(x => x.GetById(id))
               .ReturnsAsync(new TbComFile());

            FileService service = new FileService(fileRepository.Object, _mapper, validator.Object);
            //Act
            var file = service.DownloadFile(id, ray).Result;

            //Assert
            Assert.IsNotNull(file);
        }

        [Test]
        public void UploadFile_ReturnSuccess()
        {
            //Arrange
            int clientId = 1;
            int entityId = 1;
            string ray = string.Empty;
            string username = "test";
            List<InforUploadModel> infors = new List<InforUploadModel>();
            IList<IFormFile> files = new List<IFormFile>();
            var fileModels = new List<TbComFile>();

            var fileRepository = new Mock<IFileRepository>();
            var validator = new Mock<IValidator>();

            validator.Setup(x => x.Validate(infors, null))
                .Returns(new List<MessageItem>());
            validator.Setup(x => x.Validate(files, null))
                .Returns(new List<MessageItem>());

            fileRepository.Setup(x => x.Create(fileModels))
                .ReturnsAsync(new List<TbComFile>()
                {
                    new TbComFile(),
                    new TbComFile()
                });

            FileService service = new FileService(fileRepository.Object, _mapper, validator.Object);
            //Act
            var result = service.UploadFile(clientId, entityId, ray, username, infors, files).Result;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Data.Count);
        }

        [Test]
        public void UploadFile_ReturnFail()
        {
            //Arrange
            int clientId = 1;
            int entityId = 1;
            string ray = string.Empty;
            string username = "test";
            List<InforUploadModel> infors = new List<InforUploadModel>();
            IList<IFormFile> files = new List<IFormFile>();
            var fileModels = new List<TbComFile>();

            var fileRepository = new Mock<IFileRepository>();
            var validator = new Mock<IValidator>();

            validator.Setup(x => x.Validate(infors, null))
                .Returns(new List<MessageItem>()
                {
                    new MessageItem()
                });
            validator.Setup(x => x.Validate(files, null))
                .Returns(new List<MessageItem>()
                {
                    new MessageItem()
                });

            fileRepository.Setup(x => x.Create(fileModels))
                .ReturnsAsync(new List<TbComFile>()
                {
                    new TbComFile(),
                    new TbComFile()
                });

            FileService service = new FileService(fileRepository.Object, _mapper, validator.Object);
            //Act
            var result = service.UploadFile(clientId, entityId, ray, username, infors, files).Result;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Data.Count);
        }
    }
}