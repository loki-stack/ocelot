﻿using NUnit.Framework;

namespace TestEmploymentService
{
    internal class TestPayrollProfileService
    {
        [Test]
        public void GetAllPayrollProfile_Has2PayrollProfile_Return2PayrollProfile()
        {
            //string token = string.Empty;
            //List<string> lstGenericCode = new List<string>
            //{
            //    GenericCode.STATUS
            //};
            //ApiRequest<FilterRequest> request = new ApiRequest<FilterRequest>();
            //request.Data = new FilterRequest();

            //request.Parameter = new StdRequestParam()
            //{
            //    Locale = "en",
            //    ClientId = 1,
            //    EntityId = 1
            //};

            //var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            //var genericCodeRepository = new Mock<IGenericCodeRepository>();
            //payrollProfileRepository.Setup(x => x.GetAll(request.Data, request.Parameter.ClientId, request.Parameter.EntityId))
            //        .ReturnsAsync(new List<PayrollProfileViewModel> { new PayrollProfileViewModel(), new PayrollProfileViewModel() });

            //genericCodeRepository.Setup(x => x.GetGenericCodes(lstGenericCode, token, request.Parameter))
            //        .ReturnsAsync(new Dictionary<string, List<GenericCodeResult>>
            //    {
            //        {
            //            GenericCode.STATUS.ToLower(),
            //            new List<GenericCodeResult>
            //            {
            //                new GenericCodeResult { CodeValue = "Active" },
            //                new GenericCodeResult { CodeValue = "Inactive" },
            //                new GenericCodeResult { CodeValue = "Closed" }
            //            }
            //        }
            //    });

            //PayrollProfileService service = new PayrollProfileService(payrollProfileRepository.Object, genericCodeRepository.Object);

            //var result = service.GetAll(request, token).Result;

            //Assert.IsNotNull(result);
            //Assert.AreEqual(2, result.Data.Count);
        }

        [Test]
        public void CreatePayrollProfile_ReturnSuccess()
        {
            //ApiRequest<PayrollProfileStoreModel> request = new ApiRequest<PayrollProfileStoreModel>();
            //request.Parameter = new StdRequestParam();
            //request.Parameter.Locale = "en";
            //request.Data = new PayrollProfileStoreModel()
            //{
            //    ClientId = 1,
            //    EntityId = 1,
            //    ProfileName = "Test",
            //    Remark = "Test",
            //    StatusCd = TableStatusCode.ACTIVE,
            //    CreatedBy = "sysad",
            //    ModifiedBy = "sysad",
            //    PayrollItems = new List<int>() { 1 }
            //};
            //var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            //var genericCodeRepository = new Mock<IGenericCodeRepository>();

            //payrollProfileRepository.Setup(x => x.Store(request.Data))
            //    .ReturnsAsync(1);
            //payrollProfileRepository.Setup(x => x.CheckExistMasterPayrollItem(request.Data.PayrollItems.First()))
            //    .ReturnsAsync(true);
            //payrollProfileRepository.Setup(x => x.StorePayrollProfileItem(1, request.Data.PayrollItems.First(), request.Data.ModifiedBy))
            //    .ReturnsAsync(true);

            //PayrollProfileService service = new PayrollProfileService(payrollProfileRepository.Object, genericCodeRepository.Object);

            //var result = service.Store(request).Result;

            ////Assert
            //Assert.IsNotNull(result);
            //Assert.AreEqual(1, result.Data);
        }

        [Test]
        public void CreatePayrollProfile_AddPayrollProfile_ReturnFail()
        {
            //ApiRequest<PayrollProfileStoreModel> request = new ApiRequest<PayrollProfileStoreModel>();
            //request.Parameter = new StdRequestParam();
            //request.Parameter.Locale = "en";
            //request.Data = new PayrollProfileStoreModel()
            //{
            //    ClientId = 1,
            //    EntityId = 1,
            //    ProfileName = "Test",
            //    Remark = "Test",
            //    StatusCd = TableStatusCode.ACTIVE,
            //    CreatedBy = "sysad",
            //    ModifiedBy = "sysad",
            //    PayrollItems = new List<int>() { 1 }
            //};
            //var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            //var genericCodeRepository = new Mock<IGenericCodeRepository>();

            //payrollProfileRepository.Setup(x => x.Store(request.Data))
            //    .ReturnsAsync(0);
            //payrollProfileRepository.Setup(x => x.CheckExistMasterPayrollItem(request.Data.PayrollItems.First()))
            //    .ReturnsAsync(true);
            //payrollProfileRepository.Setup(x => x.StorePayrollProfileItem(1, request.Data.PayrollItems.First(), request.Data.ModifiedBy))
            //    .ReturnsAsync(true);

            //PayrollProfileService service = new PayrollProfileService(payrollProfileRepository.Object, genericCodeRepository.Object);

            //var result = service.Store(request).Result;

            ////Assert
            //Assert.IsNotNull(result);
            //Assert.AreEqual(0, result.Data);
        }

        [Test]
        public void CreatePayrollProfile_PayrollItemNotExist_ReturnFail()
        {
            //ApiRequest<PayrollProfileStoreModel> request = new ApiRequest<PayrollProfileStoreModel>();
            //request.Parameter = new StdRequestParam();
            //request.Parameter.Locale = "en";
            //request.Data = new PayrollProfileStoreModel()
            //{
            //    ClientId = 1,
            //    EntityId = 1,
            //    ProfileName = "Test",
            //    Remark = "Test",
            //    StatusCd = TableStatusCode.ACTIVE,
            //    CreatedBy = "sysad",
            //    ModifiedBy = "sysad",
            //    PayrollItems = new List<int>() { 1 }
            //};
            //var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            //var genericCodeRepository = new Mock<IGenericCodeRepository>();

            //payrollProfileRepository.Setup(x => x.Store(request.Data))
            //    .ReturnsAsync(1);
            //payrollProfileRepository.Setup(x => x.CheckExistMasterPayrollItem(request.Data.PayrollItems.First()))
            //    .ReturnsAsync(false);
            //payrollProfileRepository.Setup(x => x.StorePayrollProfileItem(1, request.Data.PayrollItems.First(), request.Data.ModifiedBy))
            //    .ReturnsAsync(true);

            //PayrollProfileService service = new PayrollProfileService(payrollProfileRepository.Object, genericCodeRepository.Object);

            //var result = service.Store(request).Result;

            ////Assert
            //Assert.IsNotNull(result);
            //Assert.AreEqual(0, result.Data);
        }

        [Test]
        public void CreatePayrollProfile_AddPayrollProfileItem_ReturnFail()
        {
            //ApiRequest<PayrollProfileStoreModel> request = new ApiRequest<PayrollProfileStoreModel>();
            //request.Parameter = new StdRequestParam();
            //request.Parameter.Locale = "en";
            //request.Data = new PayrollProfileStoreModel()
            //{
            //    ClientId = 1,
            //    EntityId = 1,
            //    ProfileName = "Test",
            //    Remark = "Test",
            //    StatusCd = TableStatusCode.ACTIVE,
            //    CreatedBy = "sysad",
            //    ModifiedBy = "sysad",
            //    PayrollItems = new List<int>() { 1 }
            //};
            //var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            //var genericCodeRepository = new Mock<IGenericCodeRepository>();

            //payrollProfileRepository.Setup(x => x.Store(request.Data))
            //    .ReturnsAsync(1);
            //payrollProfileRepository.Setup(x => x.CheckExistMasterPayrollItem(request.Data.PayrollItems.First()))
            //    .ReturnsAsync(true);
            //payrollProfileRepository.Setup(x => x.StorePayrollProfileItem(1, request.Data.PayrollItems.First(), request.Data.ModifiedBy))
            //    .ReturnsAsync(false);

            //PayrollProfileService service = new PayrollProfileService(payrollProfileRepository.Object, genericCodeRepository.Object);

            //var result = service.Store(request).Result;

            ////Assert
            //Assert.IsNotNull(result);
            //Assert.AreEqual(0, result.Data);
        }

        [Test]
        public void UpdatePayrollProfile_ReturnSuccess()
        {
            //int payrollProfileId = 1;
            //ApiRequest<PayrollProfileStoreModel> request = new ApiRequest<PayrollProfileStoreModel>();
            //request.Parameter = new StdRequestParam();
            //request.Parameter.Locale = "en";
            //request.Data = new PayrollProfileStoreModel()
            //{
            //    ClientId = 1,
            //    EntityId = 1,
            //    ProfileName = "Test",
            //    Remark = "Test",
            //    StatusCd = TableStatusCode.ACTIVE,
            //    CreatedBy = "sysad",
            //    ModifiedBy = "sysad",
            //    PayrollItems = new List<int>() { 2, 3 }
            //};
            //var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            //var genericCodeRepository = new Mock<IGenericCodeRepository>();

            //payrollProfileRepository.Setup(x => x.GetPayrollProfileItem(payrollProfileId))
            //    .ReturnsAsync(new List<PayrollProfileItem>() { new PayrollProfileItem() { PayrollProfileId = payrollProfileId, PayrollItemId = 1 } });
            //payrollProfileRepository.Setup(x => x.GetById(payrollProfileId))
            //    .ReturnsAsync(new PayrollProfileViewModel());
            //payrollProfileRepository.Setup(x => x.Update(payrollProfileId, request.Data))
            //    .ReturnsAsync(1);
            //payrollProfileRepository.Setup(x => x.CheckExistMasterPayrollItem(request.Data.PayrollItems.ElementAt(0)))
            //    .ReturnsAsync(true);
            //payrollProfileRepository.Setup(x => x.CheckExistMasterPayrollItem(request.Data.PayrollItems.ElementAt(1)))
            //    .ReturnsAsync(true);
            //payrollProfileRepository.Setup(x => x.StorePayrollProfileItem(1, request.Data.PayrollItems.ElementAt(0), request.Data.ModifiedBy))
            //    .ReturnsAsync(true);
            //payrollProfileRepository.Setup(x => x.StorePayrollProfileItem(1, request.Data.PayrollItems.ElementAt(1), request.Data.ModifiedBy))
            //    .ReturnsAsync(true);
            //payrollProfileRepository.Setup(x => x.DeletePayrollProfileItem(1, 1, request.Data.ModifiedBy))
            //    .ReturnsAsync(true);

            //PayrollProfileService service = new PayrollProfileService(payrollProfileRepository.Object, genericCodeRepository.Object);

            //var result = service.Update(payrollProfileId, request).Result;

            ////Assert
            //Assert.IsNotNull(result);
            //Assert.AreEqual(1, result.Data);
        }

        [Test]
        public void UpdatePayrollProfile_PayrollProfileNotExit_ReturnFail()
        {
            //int payrollProfileId = 1;
            //ApiRequest<PayrollProfileStoreModel> request = new ApiRequest<PayrollProfileStoreModel>();
            //request.Parameter = new StdRequestParam();
            //request.Parameter.Locale = "en";
            //request.Data = new PayrollProfileStoreModel()
            //{
            //    ClientId = 1,
            //    EntityId = 1,
            //    ProfileName = "Test",
            //    Remark = "Test",
            //    StatusCd = TableStatusCode.ACTIVE,
            //    CreatedBy = "sysad",
            //    ModifiedBy = "sysad",
            //    PayrollItems = new List<int>() { 2, 3 }
            //};
            //var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            //var genericCodeRepository = new Mock<IGenericCodeRepository>();
            //PayrollProfileViewModel value = null;

            //payrollProfileRepository.Setup(x => x.GetPayrollProfileItem(payrollProfileId))
            //    .ReturnsAsync(new List<PayrollProfileItem>() { new PayrollProfileItem() { PayrollProfileId = payrollProfileId, PayrollItemId = 1 } });
            //payrollProfileRepository.Setup(x => x.GetById(payrollProfileId))
            //    .ReturnsAsync(value);
            //payrollProfileRepository.Setup(x => x.Update(payrollProfileId, request.Data))
            //    .ReturnsAsync(1);
            //payrollProfileRepository.Setup(x => x.CheckExistMasterPayrollItem(request.Data.PayrollItems.ElementAt(0)))
            //    .ReturnsAsync(true);
            //payrollProfileRepository.Setup(x => x.CheckExistMasterPayrollItem(request.Data.PayrollItems.ElementAt(1)))
            //    .ReturnsAsync(true);
            //payrollProfileRepository.Setup(x => x.StorePayrollProfileItem(1, request.Data.PayrollItems.ElementAt(0), request.Data.ModifiedBy))
            //    .ReturnsAsync(true);
            //payrollProfileRepository.Setup(x => x.StorePayrollProfileItem(1, request.Data.PayrollItems.ElementAt(1), request.Data.ModifiedBy))
            //    .ReturnsAsync(true);
            //payrollProfileRepository.Setup(x => x.DeletePayrollProfileItem(1, 1, request.Data.ModifiedBy))
            //    .ReturnsAsync(true);

            //PayrollProfileService service = new PayrollProfileService(payrollProfileRepository.Object, genericCodeRepository.Object);

            //var result = service.Update(payrollProfileId, request).Result;

            ////Assert
            //Assert.IsNotNull(result);
            //Assert.AreEqual(0, result.Data);
        }

        [Test]
        public void UpdatePayrollProfile_PayrollItemNotExit_ReturnFail()
        {
            //int payrollProfileId = 1;
            //ApiRequest<PayrollProfileStoreModel> request = new ApiRequest<PayrollProfileStoreModel>();
            //request.Parameter = new StdRequestParam();
            //request.Parameter.Locale = "en";
            //request.Data = new PayrollProfileStoreModel()
            //{
            //    ClientId = 1,
            //    EntityId = 1,
            //    ProfileName = "Test",
            //    Remark = "Test",
            //    StatusCd = TableStatusCode.ACTIVE,
            //    CreatedBy = "sysad",
            //    ModifiedBy = "sysad",
            //    PayrollItems = new List<int>() { 2, 3 }
            //};
            //var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            //var genericCodeRepository = new Mock<IGenericCodeRepository>();

            //payrollProfileRepository.Setup(x => x.GetPayrollProfileItem(payrollProfileId))
            //    .ReturnsAsync(new List<PayrollProfileItem>() { new PayrollProfileItem() { PayrollProfileId = payrollProfileId, PayrollItemId = 1 } });
            //payrollProfileRepository.Setup(x => x.GetById(payrollProfileId))
            //    .ReturnsAsync(new PayrollProfileViewModel());
            //payrollProfileRepository.Setup(x => x.Update(payrollProfileId, request.Data))
            //    .ReturnsAsync(1);
            //payrollProfileRepository.Setup(x => x.CheckExistMasterPayrollItem(request.Data.PayrollItems.ElementAt(0)))
            //    .ReturnsAsync(false);
            //payrollProfileRepository.Setup(x => x.CheckExistMasterPayrollItem(request.Data.PayrollItems.ElementAt(1)))
            //    .ReturnsAsync(true);
            //payrollProfileRepository.Setup(x => x.StorePayrollProfileItem(1, request.Data.PayrollItems.ElementAt(0), request.Data.ModifiedBy))
            //    .ReturnsAsync(true);
            //payrollProfileRepository.Setup(x => x.StorePayrollProfileItem(1, request.Data.PayrollItems.ElementAt(1), request.Data.ModifiedBy))
            //    .ReturnsAsync(true);
            //payrollProfileRepository.Setup(x => x.DeletePayrollProfileItem(1, 1, request.Data.ModifiedBy))
            //    .ReturnsAsync(true);

            //PayrollProfileService service = new PayrollProfileService(payrollProfileRepository.Object, genericCodeRepository.Object);

            //var result = service.Update(payrollProfileId, request).Result;

            ////Assert
            //Assert.IsNotNull(result);
            //Assert.AreEqual(0, result.Data);
        }

        [Test]
        public void GetPayrollItem_ReturnSuccess()
        {
            //int payrollProfileId = 0;
            //ApiRequest request = new ApiRequest();
            //request.Parameter = new StdRequestParam();
            //request.Parameter.Locale = "en";
            //request.Parameter.ClientId = 1;
            //request.Parameter.EntityId = 1;
            //string token = string.Empty;

            //var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            //var genericCodeRepository = new Mock<IGenericCodeRepository>();

            //payrollProfileRepository.Setup(x => x.GetPayrollItem(payrollProfileId, request.Parameter.ClientId, request.Parameter.EntityId))
            //    .ReturnsAsync(new List<PayrollProfileViewItemModel>() { new PayrollProfileViewItemModel() { PayrollProfileId = null, PayrollItemId = 1 }, new PayrollProfileViewItemModel() { PayrollProfileId = payrollProfileId, PayrollItemId = 2 } });

            //Dictionary<string, List<GenericCodeResult>> genericReturn = new Dictionary<string, List<GenericCodeResult>>();
            //genericReturn.Add(EmploymentServiceAPIs.Employee.Constants.GenericCode.PAYROLL_ITEM_FLAG, new List<GenericCodeResult>() {
            //    new GenericCodeResult(){ CodeValue = "1", TextValue = "Pension:MPF MC"},
            //    new GenericCodeResult(){ CodeValue = "2", TextValue = "Pension:MPF VC (EE)"},
            //    new GenericCodeResult(){ CodeValue = "3", TextValue = "Pension:MPF VC (ER)"},
            //    new GenericCodeResult(){ CodeValue = "4", TextValue = "Wage:Wages"},
            //    new GenericCodeResult(){ CodeValue = "5", TextValue = "Wage:Min. Wage"}
            //});
            //genericCodeRepository.Setup(x => x.GetGenericCodes(new List<string>() { EmploymentServiceAPIs.Employee.Constants.GenericCode.PAYROLL_ITEM_FLAG }, token, request.Parameter))
            //    .ReturnsAsync(genericReturn);

            //payrollProfileRepository.Setup(x => x.CountPayrollItemTaxByPayrollItemId(payrollProfileId))
            //    .ReturnsAsync(1);

            //payrollProfileRepository.Setup(x => x.GetPayrollItemFlagsByPayrollItemId(1))
            //    .ReturnsAsync(new List<TbPayMasterPayrollItemFlag>() {
            //        new TbPayMasterPayrollItemFlag(){PayrollItemId = 1, FlagId = 1},
            //        new TbPayMasterPayrollItemFlag(){PayrollItemId = 1, FlagId = 2}
            //    });
            //payrollProfileRepository.Setup(x => x.GetPayrollItemFlagsByPayrollItemId(2))
            //    .ReturnsAsync(new List<TbPayMasterPayrollItemFlag>() {
            //        new TbPayMasterPayrollItemFlag(){PayrollItemId = 1, FlagId = 4},
            //        new TbPayMasterPayrollItemFlag(){PayrollItemId = 1, FlagId = 5}
            //    });

            //PayrollProfileService service = new PayrollProfileService(payrollProfileRepository.Object, genericCodeRepository.Object);

            //var result = service.GetPayrollItem(request, token).Result;

            ////Assert
            //Assert.IsNotNull(result);
            //Assert.AreEqual(5, result.Data.Types.Count);
            //Assert.AreEqual(2, result.Data.Items.Count);
            //Assert.AreEqual(true, result.Data.Items.ElementAt(0).PayrollItemFlags["1"]);
            //Assert.AreEqual(false, result.Data.Items.ElementAt(0).PayrollItemFlags["3"]);
            //Assert.AreEqual(true, result.Data.Items.ElementAt(1).PayrollItemFlags["5"]);
            //Assert.AreEqual(false, result.Data.Items.ElementAt(1).PayrollItemFlags["3"]);
        }

        [Test]
        public void GetPayrollProfileItemConfiguration_ReturnSuccess()
        {
            //int payrollItemId = 1, payrollProfileId = 1;
            //ApiRequest request = new ApiRequest();
            //string token = string.Empty;

            //var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            //var genericCodeRepository = new Mock<IGenericCodeRepository>();

            //Dictionary<string, List<GenericCodeResult>> genericReturn = new Dictionary<string, List<GenericCodeResult>>();
            //genericReturn.Add(EmploymentServiceAPIs.Employee.Constants.GenericCode.DISPLAY_MODEL, new List<GenericCodeResult>() {
            //    new GenericCodeResult(){ CodeValue = "0", TextValue = "Always hidden"},
            //    new GenericCodeResult(){ CodeValue = "1", TextValue = "Always shown"},
            //    new GenericCodeResult(){ CodeValue = "2", TextValue = "Hidden if zero"}
            //});
            //genericCodeRepository.Setup(x => x.GetGenericCodes(new List<string>() { EmploymentServiceAPIs.Employee.Constants.GenericCode.DISPLAY_MODEL }, token, request.Parameter))
            //    .ReturnsAsync(genericReturn);

            //payrollProfileRepository.Setup(x => x.GetPayrollProfileItemConfiguration(payrollProfileId, payrollItemId))
            //    .ReturnsAsync(new PayrollProfileItem() { DisplayName = "Test", DisplayStatus = 1, RecurrentItem = 1, PayrollItemId = payrollItemId, PayrollProfileId = payrollProfileId });

            //PayrollProfileService service = new PayrollProfileService(payrollProfileRepository.Object, genericCodeRepository.Object);

            //var result = service.GetPayrollProfileItemConfiguration(payrollProfileId, payrollItemId, request, token).Result;

            //Assert.IsNotNull(result);
        }

        [Test]
        public void UpdatePayrollProfileItemConfiguration_ReturnSuccess()
        {
            //string strModifiedBy = "Test";
            //int payrollItemId = 1, payrollProfileId = 1;
            //ApiRequest<PayrollProfileItem> request = new ApiRequest<PayrollProfileItem>();
            //request.Parameter = new StdRequestParam();
            //request.Parameter.Locale = "en";
            //request.Data = new PayrollProfileItem();
            //// update payroll profile
            //request.Data.PayrollProfileId = payrollProfileId;
            //request.Data.PayrollItemId = payrollItemId;

            //var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            //var genericCodeRepository = new Mock<IGenericCodeRepository>();

            //payrollProfileRepository.Setup(x => x.UpdatePayrollProfileItem(request.Data, strModifiedBy))
            //    .ReturnsAsync(true);

            //PayrollProfileService service = new PayrollProfileService(payrollProfileRepository.Object, genericCodeRepository.Object);

            //var result = service.UpdatePayrollProfileItemConfiguration(payrollProfileId, payrollItemId, strModifiedBy, request).Result;

            //Assert.IsTrue(result.Data);
        }

        [Test]
        public void UpdatePayrollProfileItemConfiguration_ReturnFailt()
        {
            //string strModifiedBy = "Test";
            //int payrollItemId = 1, payrollProfileId = 1;
            //ApiRequest<PayrollProfileItem> request = new ApiRequest<PayrollProfileItem>();
            //request.Parameter = new StdRequestParam();
            //request.Parameter.Locale = "en";
            //request.Data = new PayrollProfileItem();
            //// update payroll profile
            //request.Data.PayrollProfileId = payrollProfileId;
            //request.Data.PayrollItemId = payrollItemId;

            //var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            //var genericCodeRepository = new Mock<IGenericCodeRepository>();

            //payrollProfileRepository.Setup(x => x.UpdatePayrollProfileItem(request.Data, strModifiedBy))
            //    .ReturnsAsync(false);

            //PayrollProfileService service = new PayrollProfileService(payrollProfileRepository.Object, genericCodeRepository.Object);

            //var result = service.UpdatePayrollProfileItemConfiguration(payrollProfileId, payrollItemId, strModifiedBy, request).Result;

            //Assert.IsFalse(result.Data);
        }
    }
}