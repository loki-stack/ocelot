﻿using CommonLib.Dtos;
using CommonLib.Repositories.Impl;
using CommonLib.Repositories.Interfaces;
using EmploymentServiceAPIs.Common.Constants;
using EmploymentServiceAPIs.Common.Dtos;
using EmploymentServiceAPIs.Common.Repositories.Interfaces;
using EmploymentServiceAPIs.Employee.Constants;
using EmploymentServiceAPIs.ManageEntity.Dtos;
using EmploymentServiceAPIs.ManageEntity.Repositories.Interfaces;
using EmploymentServiceAPIs.ManageEntity.Services.Impl;
using EmploymentServiceAPIs.ManageEntity.Services.Interfaces;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace TestCoreService
{
    internal class TestOrganizationStructureService
    {
        [Test]
        public void GetAll()
        {
            string token = string.Empty;
            var entityId = 1;
            var clientId = 1;
            List<string> lstGenericCode = new List<string>
            {
                GenericCode.STATUS
            };
            ApiRequest<PaginationRequest> request = new ApiRequest<PaginationRequest>()
            {
                Data = new PaginationRequest()
            };
            var organizationStructureRepo = new Mock<IOrganizationStructureRepository>();
            var secDataAccessOrgStructureRepo = new Mock<ITbSecDataAccessOrganizationStructureRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            organizationStructureRepo.Setup(x => x.GetAll(request.Data, clientId, entityId)).ReturnsAsync(new OrganizationStructureAllModel() { Data = new List<OrganizationStructureModel>() { new OrganizationStructureModel(), new OrganizationStructureModel() }, TotalRecord = 2 });
            IOrganizationStructureService organizationStructureService = new OrganizationStructureService(secDataAccessOrgStructureRepo.Object, organizationStructureRepo.Object, genericCodeRepository.Object);

            genericCodeRepository.Setup(x => x.GetGenericCodes(lstGenericCode, token, request.Parameter))
                    .ReturnsAsync(new Dictionary<string, List<GenericCodeResult>>
                {
                    {
                        GenericCode.STATUS.ToLower(),
                        new List<GenericCodeResult>
                        {
                            new GenericCodeResult { CodeValue = "Active" },
                            new GenericCodeResult { CodeValue = "Inactive" },
                            new GenericCodeResult { CodeValue = "Closed" }
                        }
                    }
                });

            var result = organizationStructureService.GetAll(request, clientId, entityId, token);
            var organizationStructureAll = result.Result.Data;

            Assert.AreEqual(organizationStructureAll.Data.Count, organizationStructureAll.TotalRecord);
        }

        [Test]
        public void CreateOrganizationStructure_ReturnId()
        {
            ApiRequest<OrganizationStructureStoreModel> request = new ApiRequest<OrganizationStructureStoreModel>()
            {
                Data = new OrganizationStructureStoreModel()
            };
            var organizationStructureRepo = new Mock<IOrganizationStructureRepository>();
            var secDataAccessOrgStructureRepo = new Mock<ITbSecDataAccessOrganizationStructureRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            organizationStructureRepo.Setup(x => x.Create(request.Data)).ReturnsAsync(1);
            IOrganizationStructureService organizationStructureService = new OrganizationStructureService(secDataAccessOrgStructureRepo.Object, organizationStructureRepo.Object, genericCodeRepository.Object);

            request.Data.ClientId = 1;
            request.Data.EntityId = 1;
            request.Data.ParentId = null;
            request.Data.FullName = "Test";
            request.Data.DisplayName = "Test Display Name";
            request.Data.UnitCode = "20201208";
            request.Data.StatusCd = UnitStatusCode.ACTIVE;
            request.Data.Remark = null;

            var result = organizationStructureService.Store(request);
            var unitId = Convert.ToInt32(result.Result.Data);
            Assert.AreNotEqual(0, unitId);
        }

        [Test]
        public void UpdateOrganizationStructure_ReturnCode()
        {
            ApiRequest<OrganizationStructureUpdateModel> request = new ApiRequest<OrganizationStructureUpdateModel>()
            {
                Data = new OrganizationStructureUpdateModel()
            };
            var organizationStructureRepo = new Mock<IOrganizationStructureRepository>();
            var secDataAccessOrgStructureRepo = new Mock<ITbSecDataAccessOrganizationStructureRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            organizationStructureRepo.Setup(x => x.GetOrganizationStructureById(1)).ReturnsAsync(new OrganizationStructureModel() { UnitId = 1, ClientId = 1, EntityId = 1, EmployeeCount = 1, DisplayName = "TEST", StatusCd = Convert.ToString(UnitStatusCode.ACTIVE), UnitCode = "TEST" });
            organizationStructureRepo.Setup(x => x.Update(request.Data)).ReturnsAsync(1);
            IOrganizationStructureService organizationStructureService = new OrganizationStructureService(secDataAccessOrgStructureRepo.Object, organizationStructureRepo.Object, genericCodeRepository.Object);

            request.Data.UnitId = 1;
            request.Data.FullName = "Test";
            request.Data.DisplayName = "Test Display Name";
            request.Data.UnitCode = "20201208";
            request.Data.StatusCd = UnitStatusCode.ACTIVE;

            var result = organizationStructureService.Update(request);

            Assert.AreEqual(request.Data.UnitId, result.Result.Data);
        }
    }
}