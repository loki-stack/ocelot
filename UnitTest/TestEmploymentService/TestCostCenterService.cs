﻿using AutoMapper;
using CommonLib.Dtos;
using CommonLib.Repositories.Impl;
using CommonLib.Repositories.Interfaces;
using EmploymentServiceAPIs.Common.Mapping;
using EmploymentServiceAPIs.Employee.Constants;
using EmploymentServiceAPIs.Employee.Dtos;
using EmploymentServiceAPIs.ManageEntity.Dtos;
using EmploymentServiceAPIs.ManageEntity.Repositories.Interfaces;
using EmploymentServiceAPIs.ManageEntity.Services.Impl;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace TestEmploymentService
{
    public class TestCostCenterService
    {
        private IMapper _mapper;

        [SetUp]
        public void Setup()
        {
            //auto mapper configuration
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperProfile());
            });
            _mapper = mockMapper.CreateMapper();
        }

        [Test]
        public void GetCostCenters_Has2CostCenters_Return2CostCenters()
        {
            //Arrange
            string token = string.Empty;
            List<string> lstGenericCode = new List<string>
            {
                GenericCode.STATUS
            };
            ApiRequest<BasePaginationRequest> request = new ApiRequest<BasePaginationRequest>();
            request.Data = new BasePaginationRequest();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 0,
                EntityId = 0
            };
            var costCenterRepository = new Mock<ICostCenterRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            costCenterRepository.Setup(x => x.GetFilter())
                    .ReturnsAsync(new List<CostCenterViewModel> { new CostCenterViewModel(), new CostCenterViewModel() });

            genericCodeRepository.Setup(x => x.GetGenericCodes(lstGenericCode, token, request.Parameter))
                    .ReturnsAsync(new Dictionary<string, List<GenericCodeResult>>
                {
                    {
                        GenericCode.STATUS.ToLower(),
                        new List<GenericCodeResult>
                        {
                            new GenericCodeResult { CodeValue = "Active" },
                            new GenericCodeResult { CodeValue = "Inactive" },
                            new GenericCodeResult { CodeValue = "Closed" }
                        }
                    }
                });

            CostCenterService service = new CostCenterService(costCenterRepository.Object, genericCodeRepository.Object, _mapper);

            //Act
            var costCenters = service.GetFilter(token, request).Result;
            var pagination = costCenters.Data["pagination"] as Pagination<CostCenterViewModel>;
            var result = pagination.Content;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
        }

        [Test]
        public void GetCostCenterById_HasCostCenter_ReturnCostCenter()
        {
            //Arrange
            int id = 1;
            ApiRequest request = new ApiRequest();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 0,
                EntityId = 0
            };
            var costCenterRepository = new Mock<ICostCenterRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            costCenterRepository.Setup(x => x.GetById(id))
                                .ReturnsAsync(new CostCenterViewModel() { Id = id });
            CostCenterService service = new CostCenterService(costCenterRepository.Object, genericCodeRepository.Object, _mapper);

            //Act
            var costCenter = service.GetById(1, request).Result;
            var result = costCenter.Data["costCenter"] as CostCenterViewModel;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Id);
        }

        [Test]
        public void CreateCostCenter_ReturnCostCenterSuccess()
        {
            //Arrange
            ApiRequest<CreateCostCenterRequest> request = new ApiRequest<CreateCostCenterRequest>();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 0,
                EntityId = 0
            };
            request.Data = new CreateCostCenterRequest()
            {
                Code = "EG1",
                Status = "ACTIVE",
                Name = "Name 1",
                Remark = "Text",
            };
            var costCenterRepository = new Mock<ICostCenterRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            costCenterRepository.Setup(x => x.CheckExistCode(0, request.Data.Code))
                        .Returns(false);

            costCenterRepository.Setup(x => x.Create(request.Data))
                .ReturnsAsync(new CostCenterViewModel { Id = 1 });

            CostCenterService service = new CostCenterService(costCenterRepository.Object, genericCodeRepository.Object, _mapper);

            //Act
            var costCenter = service.Create(request).Result;
            var result = costCenter.Data["costCenter"] as CostCenterViewModel;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Id);
        }

        [Test]
        public void CreateCostCenter_ReturnCostCenterFail()
        {
            //Arrange
            ApiRequest<CreateCostCenterRequest> request = new ApiRequest<CreateCostCenterRequest>();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 0,
                EntityId = 0
            };
            request.Data = new CreateCostCenterRequest()
            {
                Code = "EG1",
                Status = "ACTIVE",
                Name = "Name 1",
                Remark = "Text"
            };
            var costCenterRepository = new Mock<ICostCenterRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            costCenterRepository.Setup(x => x.CheckExistCode(0, request.Data.Code))
                        .Returns(true);

            costCenterRepository.Setup(x => x.Create(request.Data))
                .ReturnsAsync(new CostCenterViewModel { Id = 1 });

            CostCenterService service = new CostCenterService(costCenterRepository.Object, genericCodeRepository.Object, _mapper);

            //Act
            var result = service.Create(request).Result;

            //Assert
            Assert.IsNull(result.Data);
        }

        [Test]
        public void UpdateCostCenter_ReturnCostCenterSuccess()
        {
            //Arrange
            ApiRequest<UpdateCostCenterRequest> request = new ApiRequest<UpdateCostCenterRequest>();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 0,
                EntityId = 0
            };
            request.Data = new UpdateCostCenterRequest()
            {
                Id = 1,
                Code = "EG1",
                Status = "ACTIVE",
                Name = "Name 1",
                Remark = "Text1"
            };

            var costCenterRepository = new Mock<ICostCenterRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            costCenterRepository.Setup(x => x.CheckCostCenterById(request.Data.Id))
                        .Returns(true);

            costCenterRepository.Setup(x => x.GetById(request.Data.Id))
                        .ReturnsAsync(new CostCenterViewModel());

            costCenterRepository.Setup(x => x.CheckExistCode(request.Data.Id, request.Data.Code))
                        .Returns(false);

            costCenterRepository.Setup(x => x.Update(request.Data))
                        .ReturnsAsync(new CostCenterViewModel { Id = 1 });
            CostCenterService service = new CostCenterService(costCenterRepository.Object, genericCodeRepository.Object, _mapper);
            //Act
            var costCenter = service.Update(request).Result;
            var result = costCenter.Data["costCenter"] as CostCenterViewModel;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Id);
        }

        [Test]
        public void UpdateCostCenter_ReturnCostCenterFail()
        {
            //Arrange
            ApiRequest<UpdateCostCenterRequest> request = new ApiRequest<UpdateCostCenterRequest>();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 0,
                EntityId = 0
            };
            request.Data = new UpdateCostCenterRequest()
            {
                Id = 1,
                Code = "EG1",
                Status = "ACTIVE",
                Name = "Name 1",
                Remark = "Text1"
            };

            var costCenterRepository = new Mock<ICostCenterRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            costCenterRepository.Setup(x => x.CheckCostCenterById(request.Data.Id))
                        .Returns(false);

            costCenterRepository.Setup(x => x.GetById(request.Data.Id))
                        .ReturnsAsync(new CostCenterViewModel());

            costCenterRepository.Setup(x => x.CheckExistCode(request.Data.Id, request.Data.Code))
                        .Returns(true);

            costCenterRepository.Setup(x => x.Update(request.Data))
                        .ReturnsAsync(new CostCenterViewModel { Id = 1 });
            CostCenterService service = new CostCenterService(costCenterRepository.Object, genericCodeRepository.Object, _mapper);
            //Act
            var result = service.Update(request).Result;

            //Assert
            Assert.IsNull(result.Data);
        }
    }
}