﻿using AutoMapper;
using CommonLib.Dtos;
using CommonLib.Repositories.Impl;
using CommonLib.Repositories.Interfaces;
using EmploymentServiceAPIs.Common.Mapping;
using EmploymentServiceAPIs.Employee.Constants;
using EmploymentServiceAPIs.Employee.Dtos;
using EmploymentServiceAPIs.ManageEntity.Dtos;
using EmploymentServiceAPIs.ManageEntity.Repositories.Interfaces;
using EmploymentServiceAPIs.ManageEntity.Services.Impl;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace TestEmploymentService
{
    public class TestJobGradeService
    {
        private IMapper _mapper;

        [SetUp]
        public void Setup()
        {
            //auto mapper configuration
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperProfile());
            });
            _mapper = mockMapper.CreateMapper();
        }

        [Test]
        public void GetJobGrades_Has2JobGrades_Return2JobGrades()
        {
            //Arrange
            string token = string.Empty;
            List<string> lstGenericCode = new List<string>
            {
                GenericCode.STATUS
            };
            ApiRequest<BasePaginationRequest> request = new ApiRequest<BasePaginationRequest>();
            request.Data = new BasePaginationRequest();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 0,
                EntityId = 0
            };
            var jobGradeRepository = new Mock<IJobGradeRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            jobGradeRepository.Setup(x => x.GetFilter())
                    .ReturnsAsync(new List<JobGradeViewModel> { new JobGradeViewModel(), new JobGradeViewModel() });

            genericCodeRepository.Setup(x => x.GetGenericCodes(lstGenericCode, token, request.Parameter))
                    .ReturnsAsync(new Dictionary<string, List<GenericCodeResult>>
                {
                    {
                        GenericCode.STATUS.ToLower(),
                        new List<GenericCodeResult>
                        {
                            new GenericCodeResult { CodeValue = "Active" },
                            new GenericCodeResult { CodeValue = "Inactive" },
                            new GenericCodeResult { CodeValue = "Closed" }
                        }
                    }
                });

            JobGradeService service = new JobGradeService(jobGradeRepository.Object, _mapper, genericCodeRepository.Object);

            //Act
            var jobGrades = service.GetFilter(token, request).Result;
            var pagination = jobGrades.Data["pagination"] as Pagination<JobGradeViewModel>;
            var result = pagination.Content;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
        }

        [Test]
        public void GetCostCenterById_HasCostCenter_ReturnCostCenter()
        {
            //Arrange
            int id = 1;
            ApiRequest request = new ApiRequest();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 0,
                EntityId = 0
            };
            var jobGradeRepository = new Mock<IJobGradeRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            jobGradeRepository.Setup(x => x.GetById(id))
                                .ReturnsAsync(new JobGradeViewModel() { Id = 1 });
            JobGradeService service = new JobGradeService(jobGradeRepository.Object, _mapper, genericCodeRepository.Object);

            //Act
            var jobGrade = service.GetById(1, request).Result;
            var result = jobGrade.Data["jobGrade"] as JobGradeViewModel;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Id);
        }

        [Test]
        public void CreateJobGrade_ReturnJobGradeSuccess()
        {
            //Arrange
            ApiRequest<CreateJobGradeRequest> request = new ApiRequest<CreateJobGradeRequest>();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 0,
                EntityId = 0
            };
            request.Data = new CreateJobGradeRequest()
            {
                Level = 1,
                Status = "ACTIVE",
                Name = "Name 1",
                Remark = "Text",
            };
            var jobGradeRepository = new Mock<IJobGradeRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            jobGradeRepository.Setup(x => x.CheckExistLevel(0, request.Data.Level))
                        .Returns(false);

            jobGradeRepository.Setup(x => x.Create(request.Data))
                .ReturnsAsync(new JobGradeViewModel { Id = 1 });

            JobGradeService service = new JobGradeService(jobGradeRepository.Object, _mapper, genericCodeRepository.Object);

            //Act
            var jobGrade = service.Create(request).Result;
            var result = jobGrade.Data["jobGrade"] as JobGradeViewModel;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Id);
        }

        [Test]
        public void CreateJobGrade_ReturnJobGradeFail()
        {
            //Arrange
            ApiRequest<CreateJobGradeRequest> request = new ApiRequest<CreateJobGradeRequest>();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 0,
                EntityId = 0
            };
            request.Data = new CreateJobGradeRequest()
            {
                Level = 1,
                Status = "ACTIVE",
                Name = "Name 1",
                Remark = "Text"
            };
            var jobGradeRepository = new Mock<IJobGradeRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            jobGradeRepository.Setup(x => x.CheckExistLevel(0, request.Data.Level))
                        .Returns(true);

            jobGradeRepository.Setup(x => x.Create(request.Data))
                .ReturnsAsync(new JobGradeViewModel { Id = 1 });

            JobGradeService service = new JobGradeService(jobGradeRepository.Object, _mapper, genericCodeRepository.Object);

            //Act
            var result = service.Create(request).Result;

            //Assert
            Assert.IsNull(result.Data);
        }

        [Test]
        public void UpdateJobGrade_ReturnJobGradeSucccess()
        {
            //Arrange
            ApiRequest<UpdateJobGradeRequest> request = new ApiRequest<UpdateJobGradeRequest>();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 0,
                EntityId = 0
            };
            request.Data = new UpdateJobGradeRequest()
            {
                Id = 1,
                Level = 1,
                Status = "ACTIVE",
                Name = "Name 1",
                Remark = "Text1"
            };

            var jobGradeRepository = new Mock<IJobGradeRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            jobGradeRepository.Setup(x => x.CheckJobGradeById(request.Data.Id))
                        .Returns(true);

            jobGradeRepository.Setup(x => x.GetById(request.Data.Id))
                        .ReturnsAsync(new JobGradeViewModel() { });

            jobGradeRepository.Setup(x => x.CheckExistLevel(request.Data.Id, request.Data.Level))
                        .Returns(false);

            jobGradeRepository.Setup(x => x.Update(request.Data))
                        .ReturnsAsync(new JobGradeViewModel { Id = 1 });

            JobGradeService service = new JobGradeService(jobGradeRepository.Object, _mapper, genericCodeRepository.Object);
            //Act
            var jobGrade = service.Update(request).Result;
            var result = jobGrade.Data["jobGrade"] as JobGradeViewModel;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Id);
        }

        [Test]
        public void UpdateJobGrade_ReturnJobGradeFaile()
        {
            //Arrange
            ApiRequest<UpdateJobGradeRequest> request = new ApiRequest<UpdateJobGradeRequest>();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 0,
                EntityId = 0
            };
            request.Data = new UpdateJobGradeRequest()
            {
                Id = 1,
                Level = 1,
                Status = "ACTIVE",
                Name = "Name 1",
                Remark = "Text1"
            };

            var jobGradeRepository = new Mock<IJobGradeRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            jobGradeRepository.Setup(x => x.CheckJobGradeById(request.Data.Id))
                        .Returns(false);

            jobGradeRepository.Setup(x => x.GetById(request.Data.Id))
                        .ReturnsAsync(new JobGradeViewModel() { });

            jobGradeRepository.Setup(x => x.CheckExistLevel(request.Data.Id, request.Data.Level))
                        .Returns(true);

            jobGradeRepository.Setup(x => x.Update(request.Data))
                        .ReturnsAsync(new JobGradeViewModel { Id = 1 });

            JobGradeService service = new JobGradeService(jobGradeRepository.Object, _mapper, genericCodeRepository.Object);
            //Act
            var result = service.Update(request).Result;

            //Assert
            Assert.IsNotNull(result.Data);
        }
    }
}