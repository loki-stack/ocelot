using NUnit.Framework;

namespace TestEmploymentService
{
    public class TestMasterPayrollItemService
    {
        [Test]
        public void GetAllMasterPayrollItem_Has2MasterPayrollItem_Return2MasterPayrollIem()
        {
            //int clientId = 1, entityId = 1;
            //int masterPayrollItemId = 1;
            //string token = string.Empty;
            //List<string> lstGenericCode = new List<string>
            //{
            //    GenericCode.STATUS
            //};
            //ApiRequest<PaginationRequest> request = new ApiRequest<PaginationRequest>();
            //ApiRequest<FilterRequest> request1 = new ApiRequest<FilterRequest>();
            //request.Data = new PaginationRequest();
            //request1.Data = new FilterRequest();
            //request.Parameter = new StdRequestParam()
            //{
            //    Locale = "en"
            //};

            //var masterPayrollItemRepository = new Mock<IMasterPayrollItemRepository>();
            //var genericCodeRepository = new Mock<IGenericCodeRepository>();
            // masterPayrollItemRepository.Setup(x=> x.GetAll(request.Data,clientId,entityId))
            // .ReturnsAsync(new List<MasterPayrollItemViewAll>{new MasterPayrollItemViewAll()});
            //masterPayrollItemRepository.Setup(x => x.GetAll(request.Data, clientId, entityId))
            //.ReturnsAsync(new List<MasterPayrollItemViewAll> { new MasterPayrollItemViewAll(), new MasterPayrollItemViewAll() });
            //genericCodeRepository.Setup(x => x.GetGenericCodes(lstGenericCode, token, request.Parameter))
            //        .ReturnsAsync(new Dictionary<string, List<GenericCodeResult>>
            //    {
            //        {
            //            GenericCode.STATUS.ToLower(),
            //            new List<GenericCodeResult>
            //            {
            //                new GenericCodeResult { CodeValue = "Active" },
            //                new GenericCodeResult { CodeValue = "Inactive" },
            //                new GenericCodeResult { CodeValue = "Closed" }
            //            }
            //        }
            //    });

            //masterPayrollItemRepository.Setup(x => x.GetMasterPayrollItem(masterPayrollItemId))
            //.ReturnsAsync(new List<int>());
            //masterPayrollItemRepository.Setup(x => x.CountPayrollItemTaxByPayrollItemId(masterPayrollItemId))
            //.ReturnsAsync(1);
            //MasterPayrollItemService service = new MasterPayrollItemService(masterPayrollItemRepository.Object, genericCodeRepository.Object);
            //var result = service.GetAll(request, clientId, entityId, token).Result;
            //Assert.IsNotNull(result);
            //Assert.AreEqual(2, result.Data);
        }

        [Test]
        public void CreateMasterPayrollItem_ReturnSuccess()
        {
            //int masterPayrollItemId = 1;
            //ApiRequest<MasterPayrollItemStoreModel> request = new ApiRequest<MasterPayrollItemStoreModel>();
            //request.Parameter = new StdRequestParam();
            //request.Parameter.Locale = "en";
            //request.Data = new MasterPayrollItemStoreModel()
            //{
            //    ClientId = 1,
            //    EntityId = 1,
            //    ItemName = "test",
            //    ItemCode = "pass",
            //    Remark = "aaaaa",
            //    StatusCd = TableStatusCode.ACTIVE,
            //    PaymentSign = 1,
            //    CreatedBy = "sas",
            //    ModifiedBy = "aasd",
            //    FlagIds = new List<int>() { 1 },
            //    TaxItemValue = new List<MasterPayrollItemTaxStoreModel>
            //    {new MasterPayrollItemTaxStoreModel{TaxItemId = 1, TaxItemValue = 10},
            //    new MasterPayrollItemTaxStoreModel{TaxItemId = 2, TaxItemValue = 20}}
            //};
            //var masterPayrollItemRepository = new Mock<IMasterPayrollItemRepository>();
            //var genericCodeRepository = new Mock<IGenericCodeRepository>();

            //masterPayrollItemRepository.Setup(x => x.CheckDuplicateCode(null, request.Data.ItemCode)).ReturnsAsync(true);
            //masterPayrollItemRepository.Setup(x => x.Store(request.Data)).ReturnsAsync(1);
            //masterPayrollItemRepository.Setup(x => x.StoreFlag(masterPayrollItemId, request.Data.FlagIds.First(), request.Data.ModifiedBy)).ReturnsAsync(true);
            //masterPayrollItemRepository.Setup(x => x.StoreTaxValue(masterPayrollItemId, request.Data.TaxItemValue.First().TaxItemId, request.Data.TaxItemValue.First().TaxItemValue, request.Data.ModifiedBy)).ReturnsAsync(true);
        }
    }
}