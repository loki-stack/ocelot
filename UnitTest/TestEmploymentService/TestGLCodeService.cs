﻿using CommonLib.Dtos;
using CommonLib.Repositories.Interfaces;
using EmploymentServiceAPIs.Common.Constants;
using EmploymentServiceAPIs.Employee.Constants;
using Moq;
using NUnit.Framework;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.Common.Repositories.Interfaces;
using PayrollServiceAPIs.Common.Services.Impl;
using System.Collections.Generic;
using System.Linq;

namespace TestEmploymentService
{
    internal class TestGLCodeService
    {
        [Test]
        public void GetAllGLCode_Has2GLCode_Return2GLCode()
        {
            string token = string.Empty;
            ApiRequest<FilterRequest> request = new ApiRequest<FilterRequest>();
            request.Data = new FilterRequest();

            request.Parameter = new StdRequestParam()
            {
                Locale = "en"
            };

            var glCodeRepository = new Mock<IGlCodeRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            glCodeRepository.Setup(x => x.GetAll(request.Data))
                    .ReturnsAsync(new List<GlCodeModel>() { new GlCodeModel(), new GlCodeModel() });

            genericCodeRepository.Setup(x => x.GetGenericCodes(new List<string>() { GenericCode.CURRENCY }, token, request.Parameter))
                    .ReturnsAsync(new Dictionary<string, List<GenericCodeResult>>
                {
                    {
                        GenericCode.CURRENCY.ToLower(),
                        new List<GenericCodeResult>
                        {
                            new GenericCodeResult { CodeValue = "USD" },
                            new GenericCodeResult { CodeValue = "HKD" },
                            new GenericCodeResult { CodeValue = "VND" }
                        }
                    }
                });

            GlCodeService service = new GlCodeService(glCodeRepository.Object, genericCodeRepository.Object);

            var result = service.GetAll(request, token).Result;

            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Data.Count);
        }

        [Test]
        public void CreateGLCode_CurrencyNotExist_ReturnFail()
        {
            string token = string.Empty;
            ApiRequest<GlCodeStoreModel> request = new ApiRequest<GlCodeStoreModel>();
            request.Parameter = new StdRequestParam();
            request.Parameter.Locale = "en";
            request.Data = new GlCodeStoreModel()
            {
                AccountType = "Text",
                Currency = "VND",
                GlCode = "GL Code 1",
                Remark = "Test",
                StatusCd = TableStatusCode.ACTIVE
            };
            var glCodeRepository = new Mock<IGlCodeRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();

            genericCodeRepository.Setup(x => x.GetGenericCodes(new List<string>() { GenericCode.CURRENCY }, token, request.Parameter))
                    .ReturnsAsync(new Dictionary<string, List<GenericCodeResult>>
                {
                    {
                        GenericCode.CURRENCY.ToLower(),
                        new List<GenericCodeResult>
                        {
                            new GenericCodeResult { CodeValue = "USD" },
                            new GenericCodeResult { CodeValue = "HKD" }
                        }
                    }
                });

            GlCodeService service = new GlCodeService(glCodeRepository.Object, genericCodeRepository.Object);

            var result = service.Store(request, token).Result;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.Data);
            Assert.IsNotNull(result.Message);
            Assert.IsNotNull(result.Message.Toast);
            Assert.AreEqual(result.Message.Toast.First().LabelCode, EmploymentServiceAPIs.Common.Constants.Label.GLC0102);
        }

        [Test]
        public void CreateGLCode_ReturnSuccess()
        {
            string token = string.Empty;
            ApiRequest<GlCodeStoreModel> request = new ApiRequest<GlCodeStoreModel>();
            request.Parameter = new StdRequestParam();
            request.Parameter.Locale = "en";
            request.Data = new GlCodeStoreModel()
            {
                AccountType = "Text",
                Currency = "VND",
                GlCode = "GL Code 1",
                Remark = "Test",
                StatusCd = TableStatusCode.ACTIVE
            };
            var glCodeRepository = new Mock<IGlCodeRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();

            genericCodeRepository.Setup(x => x.GetGenericCodes(new List<string>() { GenericCode.CURRENCY }, token, request.Parameter))
                    .ReturnsAsync(new Dictionary<string, List<GenericCodeResult>>
                {
                    {
                        GenericCode.CURRENCY.ToLower(),
                        new List<GenericCodeResult>
                        {
                            new GenericCodeResult { CodeValue = "USD" },
                            new GenericCodeResult { CodeValue = "HKD" },
                            new GenericCodeResult { CodeValue = "VND" }
                        }
                    }
                });

            glCodeRepository.Setup(x => x.Store(request.Data)).ReturnsAsync(1);

            GlCodeService service = new GlCodeService(glCodeRepository.Object, genericCodeRepository.Object);

            var result = service.Store(request, token).Result;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Data);
        }

        [Test]
        public void UpdateGLCode_GLCodeNotExits_ReturnFail()
        {
            string token = string.Empty;
            int glCodeId = 1;
            ApiRequest<GlCodeUpdateModel> request = new ApiRequest<GlCodeUpdateModel>();
            request.Parameter = new StdRequestParam();
            request.Parameter.Locale = "en";
            request.Data = new GlCodeUpdateModel()
            {
                AccountType = "Text",
                Currency = "VND",
                GlCode = "GL Code 1",
                Remark = "Test",
                StatusCd = TableStatusCode.ACTIVE,
                ModifiedBy = "System"
            };
            var glCodeRepository = new Mock<IGlCodeRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();

            glCodeRepository.Setup(x => x.GetById(glCodeId)).ReturnsAsync((GlCodeModel)null);

            genericCodeRepository.Setup(x => x.GetGenericCodes(new List<string>() { GenericCode.CURRENCY }, token, request.Parameter))
                    .ReturnsAsync(new Dictionary<string, List<GenericCodeResult>>
                {
                    {
                        GenericCode.CURRENCY.ToLower(),
                        new List<GenericCodeResult>
                        {
                            new GenericCodeResult { CodeValue = "USD" },
                            new GenericCodeResult { CodeValue = "HKD" },
                            new GenericCodeResult { CodeValue = "VND" },
                        }
                    }
                });

            GlCodeService service = new GlCodeService(glCodeRepository.Object, genericCodeRepository.Object);

            var result = service.Update(glCodeId, request, token).Result;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.Data);
            Assert.IsNotNull(result.Message);
            Assert.IsNotNull(result.Message.Toast);
            Assert.AreEqual(result.Message.Toast.First().LabelCode, EmploymentServiceAPIs.Common.Constants.Label.GLC0104);
        }

        [Test]
        public void UpdateGLCode_CurrencyNotExist_ReturnFail()
        {
            int glCodeId = 1;
            string token = string.Empty;
            ApiRequest<GlCodeUpdateModel> request = new ApiRequest<GlCodeUpdateModel>();
            request.Parameter = new StdRequestParam();
            request.Parameter.Locale = "en";
            request.Data = new GlCodeUpdateModel()
            {
                AccountType = "Text",
                Currency = "VND",
                GlCode = "GL Code 1",
                Remark = "Test",
                StatusCd = TableStatusCode.ACTIVE,
                ModifiedBy = "System"
            };
            var glCodeRepository = new Mock<IGlCodeRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();

            glCodeRepository.Setup(x => x.GetById(glCodeId)).ReturnsAsync(new GlCodeModel());

            genericCodeRepository.Setup(x => x.GetGenericCodes(new List<string>() { GenericCode.CURRENCY }, token, request.Parameter))
                    .ReturnsAsync(new Dictionary<string, List<GenericCodeResult>>
                {
                    {
                        GenericCode.CURRENCY.ToLower(),
                        new List<GenericCodeResult>
                        {
                            new GenericCodeResult { CodeValue = "USD" },
                            new GenericCodeResult { CodeValue = "HKD" }
                        }
                    }
                });

            GlCodeService service = new GlCodeService(glCodeRepository.Object, genericCodeRepository.Object);

            var result = service.Update(glCodeId, request, token).Result;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.Data);
            Assert.IsNotNull(result.Message);
            Assert.IsNotNull(result.Message.Toast);
            Assert.AreEqual(result.Message.Toast.First().LabelCode, EmploymentServiceAPIs.Common.Constants.Label.GLC0102);
        }

        [Test]
        public void UpdateGLCode_ReturnSuccess()
        {
            int glCodeId = 1;
            string token = string.Empty;
            ApiRequest<GlCodeUpdateModel> request = new ApiRequest<GlCodeUpdateModel>();
            request.Parameter = new StdRequestParam();
            request.Parameter.Locale = "en";
            request.Data = new GlCodeUpdateModel()
            {
                AccountType = "Text",
                Currency = "VND",
                GlCode = "GL Code 1",
                Remark = "Test",
                StatusCd = TableStatusCode.ACTIVE,
                ModifiedBy = "System"
            };
            var glCodeRepository = new Mock<IGlCodeRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();

            glCodeRepository.Setup(x => x.GetById(glCodeId)).ReturnsAsync(new GlCodeModel());
            glCodeRepository.Setup(x => x.Update(glCodeId, request.Data)).ReturnsAsync(glCodeId);
            genericCodeRepository.Setup(x => x.GetGenericCodes(new List<string>() { GenericCode.CURRENCY }, token, request.Parameter))
                    .ReturnsAsync(new Dictionary<string, List<GenericCodeResult>>
                {
                    {
                        GenericCode.CURRENCY.ToLower(),
                        new List<GenericCodeResult>
                        {
                            new GenericCodeResult { CodeValue = "USD" },
                            new GenericCodeResult { CodeValue = "HKD" },
                            new GenericCodeResult { CodeValue = "VND" },
                        }
                    }
                });

            GlCodeService service = new GlCodeService(glCodeRepository.Object, genericCodeRepository.Object);

            var result = service.Update(glCodeId, request, token).Result;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(glCodeId, result.Data);
        }
    }
}