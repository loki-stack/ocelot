﻿using AutoMapper;
using CommonLib.Dtos;
using CommonLib.Models.Client;
using CommonLib.Repositories.Interfaces;
using CommonLib.Services;
using CommonLib.Validation;
using EmploymentServiceAPIs.Common.Mapping;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Dtos.BankAccount;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using EmploymentServiceAPIs.Employee.Services.Impl;
using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace TestEmploymentService
{
    public class TestBankAccountService
    {
        private IMapper _mapper;

        [SetUp]
        public void Setup()
        {
            //auto mapper configuration
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperProfile());
            });
            _mapper = mockMapper.CreateMapper();
        }

        [Test]
        public void GetBankAccountsByEmployeeId_Has2BankAccounts_Return2BankAccounts()
        {
            //Arrange
            int employeeId = 1;
            string ray = string.Empty;
            string[] includes = { "TbEmpEeBankAccount" };
            var bankAccountRepository = new Mock<IBankAccountRepository>();
            var employeeRepository = new Mock<IEmployeeRepository>();
            var validator = new Mock<IValidator>();
            var entitySettingRepository = new Mock<IEntitySettingRepository>();
            var sendMailService = new Mock<ISendEmailService>();
            var smtpSettings = new Mock<IOptions<SmtpSettings>>();
            var mailSettings = new Mock<IOptions<MailSettings>>();
            employeeRepository.Setup(x => x.GetSingleByCondition(x => x.EmployeeId == employeeId, includes))
                .ReturnsAsync(new TbEmpEmployee
                {
                    TbEmpEeBankAccount = new List<TbEmpEeBankAccount>()
                    {
                        new TbEmpEeBankAccount(),
                        new TbEmpEeBankAccount(),
                    }
                });

            BankAccountService service = new BankAccountService(bankAccountRepository.Object, employeeRepository.Object, _mapper, validator.Object,
                                            entitySettingRepository.Object, sendMailService.Object, smtpSettings.Object, mailSettings.Object);

            //Act
            var bankAccounts = service.GetByEmployeeId(employeeId, ray).Result;

            //Assert
            Assert.IsNotNull(bankAccounts);
            Assert.AreEqual(2, bankAccounts.Data.BankAccounts.Count);
        }

        [Test]
        public void GetBankAccountById_HasBankAccount_ReturnBankAccount()
        {
            //Arrange
            int id = 1;
            string ray = string.Empty;
            var bankAccountRepository = new Mock<IBankAccountRepository>();
            var employeeRepository = new Mock<IEmployeeRepository>();
            var validator = new Mock<IValidator>();
            var entitySettingRepository = new Mock<IEntitySettingRepository>();
            var sendMailService = new Mock<ISendEmailService>();
            var smtpSettings = new Mock<IOptions<SmtpSettings>>();
            var mailSettings = new Mock<IOptions<MailSettings>>();
            bankAccountRepository.Setup(x => x.GetById(id))
                .ReturnsAsync(new TbEmpEeBankAccount { Id = id });

            BankAccountService service = new BankAccountService(bankAccountRepository.Object, employeeRepository.Object, _mapper, validator.Object,
                                          entitySettingRepository.Object, sendMailService.Object, smtpSettings.Object, mailSettings.Object);

            //Act
            var bankAccount = service.GetById(id, ray).Result;

            //Assert
            Assert.IsNotNull(bankAccount);
            Assert.AreEqual(1, bankAccount.Data.Id);
        }

        [Test]
        public void SaveBankAccounts_ReturnSuccess()
        {
            //Arrange
            int employeeId = 1;
            string ray = string.Empty;
            string username = string.Empty;
            TbEmpEmployee employee = new TbEmpEmployee();
            BankAccountSaveModel bankAccountSM = new BankAccountSaveModel()
            {
                Remark = "Text1",
                BankAccounts = new List<BankAccountModel>()
                {
                    new BankAccountModel()
                    {
                        Id = 0,
                        EeBankAccountId = "Account001",
                        EmployeeId = employeeId,
                        AccountName = "Name 1",
                        AccountCurrency = "Currency1",
                        BankCode = "Bank1",
                        BranchCode = "Branch1",
                        TransactionReference = "Transaction1",
                        DefaultAccount = true,
                        Remark = string.Empty,
                        CreatedBy = username,
                        CreatedDt = DateTime.Now,
                        ModifiedBy = username,
                        ModifiedDt = DateTime.Now
                    },
                    new BankAccountModel()
                    {
                        Id = 1,
                        EeBankAccountId = "Account002",
                        EmployeeId = employeeId,
                        AccountName = "Name 2",
                        AccountCurrency = "Currency2",
                        BankCode = "Bank2",
                        BranchCode = "Branch2",
                        TransactionReference = "Transaction2",
                        DefaultAccount = true,
                        Remark = string.Empty,
                        CreatedBy = username,
                        CreatedDt = DateTime.Now,
                        ModifiedBy = username,
                        ModifiedDt = DateTime.Now
                    }
                }
            };
            ApiRequest<BankAccountSaveModel> request = new ApiRequest<BankAccountSaveModel>()
            {
                Ray = ray,
                Data = bankAccountSM,
                Parameter = new StdRequestParam
                {
                    ClientId = 0,
                    EntityId = 0
                }
            };
            List<TbEmpEeBankAccount> lstBankAccount = new List<TbEmpEeBankAccount>();
            var bankAccountRepository = new Mock<IBankAccountRepository>();
            var employeeRepository = new Mock<IEmployeeRepository>();
            var validator = new Mock<IValidator>();
            var entitySettingRepository = new Mock<IEntitySettingRepository>();
            var sendMailService = new Mock<ISendEmailService>();
            var smtpSettings = new Mock<IOptions<SmtpSettings>>();
            var mailSettings = new Mock<IOptions<MailSettings>>();

            validator.Setup(x => x.Validate(bankAccountSM, null))
                .Returns(new List<MessageItem>());
            foreach (var bankAccount in bankAccountSM.BankAccounts)
            {
                validator.Setup(x => x.Validate(bankAccount, null))
                    .Returns(new List<MessageItem>());
            }

            bankAccountRepository.Setup(x => x.Create(lstBankAccount))
                .ReturnsAsync(true);
            bankAccountRepository.Setup(x => x.Update(lstBankAccount))
                .ReturnsAsync(true);
            bankAccountRepository.Setup(x => x.GetById(1))
                .ReturnsAsync(new TbEmpEeBankAccount { });

            employeeRepository.Setup(x => x.CheckExistEmployeeById(employeeId))
                .Returns(true);
            employeeRepository.Setup(x => x.GetById(employeeId))
                .ReturnsAsync(new TbEmpEmployee());
            employeeRepository.Setup(x => x.Edit(employee))
                .ReturnsAsync(new TbEmpEmployee());

            BankAccountService service = new BankAccountService(bankAccountRepository.Object, employeeRepository.Object, _mapper, validator.Object,
                                          entitySettingRepository.Object, sendMailService.Object, smtpSettings.Object, mailSettings.Object);

            //Act
            var bankAccounts = service.Save(employeeId, request, username).Result;

            //Assert
            Assert.IsNotNull(bankAccounts.Data);
        }

        [Test]
        public void SaveBankAccounts_ReturnFail()
        {
            //Arrange
            int employeeId = 1;
            string ray = string.Empty;
            string username = string.Empty;
            TbEmpEmployee employee = new TbEmpEmployee();
            BankAccountSaveModel bankAccountSM = new BankAccountSaveModel()
            {
                Remark = "Text1",
                BankAccounts = new List<BankAccountModel>()
                {
                    new BankAccountModel()
                    {
                        Id = 0,
                        EeBankAccountId = "Account001",
                        EmployeeId = employeeId,
                        AccountName = "Name 1",
                        AccountCurrency = "Currency1",
                        BankCode = "Bank1",
                        BranchCode = "Branch1",
                        TransactionReference = "Transaction1",
                        DefaultAccount = true,
                        Remark = string.Empty,
                        CreatedBy = username,
                        CreatedDt = DateTime.Now,
                        ModifiedBy = username,
                        ModifiedDt = DateTime.Now
                    },
                    new BankAccountModel()
                    {
                        Id = 1,
                        EeBankAccountId = "Account002",
                        EmployeeId = employeeId,
                        AccountName = "Name 2",
                        AccountCurrency = "Currency2",
                        BankCode = "Bank2",
                        BranchCode = "Branch2",
                        TransactionReference = "Transaction2",
                        DefaultAccount = true,
                        Remark = string.Empty,
                        CreatedBy = username,
                        CreatedDt = DateTime.Now,
                        ModifiedBy = username,
                        ModifiedDt = DateTime.Now
                    }
                }
            };
            ApiRequest<BankAccountSaveModel> request = new ApiRequest<BankAccountSaveModel>()
            {
                Ray = ray,
                Data = bankAccountSM,
                Parameter = new StdRequestParam
                {
                    ClientId = 0,
                    EntityId = 0
                }
            };
            List<TbEmpEeBankAccount> lstBankAccount = new List<TbEmpEeBankAccount>();
            var bankAccountRepository = new Mock<IBankAccountRepository>();
            var employeeRepository = new Mock<IEmployeeRepository>();
            var validator = new Mock<IValidator>();
            var entitySettingRepository = new Mock<IEntitySettingRepository>();
            var sendMailService = new Mock<ISendEmailService>();
            var smtpSettings = new Mock<IOptions<SmtpSettings>>();
            var mailSettings = new Mock<IOptions<MailSettings>>();

            validator.Setup(x => x.Validate(bankAccountSM, null))
                .Returns(new List<MessageItem>()
                {
                    new MessageItem()
                });

            foreach (var bankAccount in bankAccountSM.BankAccounts)
            {
                validator.Setup(x => x.Validate(bankAccount, null))
                    .Returns(new List<MessageItem>()
                    {
                        new MessageItem()
                    });
            }

            bankAccountRepository.Setup(x => x.Create(lstBankAccount))
                .ReturnsAsync(true);
            bankAccountRepository.Setup(x => x.Update(lstBankAccount))
                .ReturnsAsync(true);
            bankAccountRepository.Setup(x => x.GetById(1))
                .ReturnsAsync(new TbEmpEeBankAccount { });

            employeeRepository.Setup(x => x.CheckExistEmployeeById(employeeId))
                .Returns(true);
            employeeRepository.Setup(x => x.GetById(employeeId))
                .ReturnsAsync(new TbEmpEmployee());
            employeeRepository.Setup(x => x.Edit(employee))
                .ReturnsAsync(new TbEmpEmployee());

            BankAccountService service = new BankAccountService(bankAccountRepository.Object, employeeRepository.Object, _mapper, validator.Object,
                                          entitySettingRepository.Object, sendMailService.Object, smtpSettings.Object, mailSettings.Object);

            //Act
            var bankAccounts = service.Save(employeeId, request, username).Result;

            //Assert
            Assert.IsNull(bankAccounts.Data);
        }
    }
}