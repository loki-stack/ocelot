﻿using CommonLib.Dtos;
using CommonLib.Models.Core;
using Moq;
using NUnit.Framework;
using PayrollServiceAPIs.PayRoll.Dtos;
using PayrollServiceAPIs.PayRoll.Repositories.Interfaces;
using PayrollServiceAPIs.PayRoll.Services.Impl;
using PayrollServiceAPIs.PayRoll.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestPayrollService
{
    public class TestMPFRegisterdSchemeService
    {
        [Test]
        public void GetAll_HasValue_ReturnSuccess()
        {
            ApiRequest request = new ApiRequest();
            var registeredSchemeRepository = new Mock<IRegisteredSchemeRepository>();
            var mpfTrusteeRepository = new Mock<IMPFTrusteeRepository>();
            List<MPFRegisteredSchemeViewList> mpfRegisterdSchemes = new List<MPFRegisteredSchemeViewList>();
            mpfRegisterdSchemes.Add(new MPFRegisteredSchemeViewList { MpfRegisteredSchemeId = 1 });
            mpfRegisterdSchemes.Add(new MPFRegisteredSchemeViewList { MpfRegisteredSchemeId = 2 });
            registeredSchemeRepository.Setup(x => x.GetAll()).ReturnsAsync(mpfRegisterdSchemes);
            IRegisteredSchemeService registerdSchemeService = new RegisteredSchemeService(registeredSchemeRepository.Object, mpfTrusteeRepository.Object);
            var result = registerdSchemeService.GetAll(request);
            var listMPF = result.Result.Data["mpfRegisteredSchemes"] as List<MPFRegisteredSchemeViewList>;
            Assert.AreEqual(listMPF.Count, mpfRegisterdSchemes.Count);
        }

        [Test]
        [TestCase(1)]
        public void GetById_HasValue_ReturnSuccess(int registeredSchemeId)
        {
            ApiRequest request = new ApiRequest();
            var registeredSchemeRepository = new Mock<IRegisteredSchemeRepository>();
            var mpfTrusteeRepository = new Mock<IMPFTrusteeRepository>();
            registeredSchemeRepository.Setup(x => x.GetById(registeredSchemeId)).ReturnsAsync(new TbPayMpfRegisteredScheme { MpfRegisteredSchemeId = registeredSchemeId });
            IRegisteredSchemeService registerdSchemeService = new RegisteredSchemeService(registeredSchemeRepository.Object, mpfTrusteeRepository.Object);
            var result = registerdSchemeService.GetById(registeredSchemeId, request);
            var mpfRegisteredScheme = result.Result.Data["mpfRegisteredScheme"] as TbPayMpfRegisteredScheme;
            Assert.IsNotNull(mpfRegisteredScheme);
        }

        [Test]
        [TestCase(1)]
        public void GetById_HasValue_ReturnFail(int registeredSchemeId)
        {
            ApiRequest request = new ApiRequest();
            var registeredSchemeRepository = new Mock<IRegisteredSchemeRepository>();
            var mpfTrusteeRepository = new Mock<IMPFTrusteeRepository>();
            registeredSchemeRepository.Setup(x => x.GetById(registeredSchemeId)).ReturnsAsync((TbPayMpfRegisteredScheme)null);
            IRegisteredSchemeService registerdSchemeService = new RegisteredSchemeService(registeredSchemeRepository.Object, mpfTrusteeRepository.Object);
            var result = registerdSchemeService.GetById(registeredSchemeId, request);
            var mpfRegisteredScheme = result.Result.Data;
            Assert.IsNull(mpfRegisteredScheme);
        }
    }
}
