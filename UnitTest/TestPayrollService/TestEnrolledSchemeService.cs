﻿using AutoMapper;
using CommonLib.Dtos;
using CommonLib.Models.Client;
using CommonLib.Repositories.Interfaces;
using Moq;
using NUnit.Framework;
using PayrollServiceAPIs.Common.Constants;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos.EnrolledScheme;
using PayrollServiceAPIs.PayRoll.Repositories.Interfaces;
using PayrollServiceAPIs.PayRoll.Services.Impl;
using PayrollServiceAPIs.PayRoll.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestPayrollService
{
    public class TestEnrolledSchemeService
    {
        [Test]
        [TestCase(1)]
        public void GetByRegisteredSchemeId_ReturnSuccess(int registeredSchemeId)
        {
            ApiRequest<EnrolledSchemeGetAllRequest> request = new ApiRequest<EnrolledSchemeGetAllRequest>();
            var enrolledSchemeRepository = new Mock<IEnrolledSchemeRepository>();
            enrolledSchemeRepository.Setup(x => x.GetByRegisteredSchemeId(registeredSchemeId))
                .ReturnsAsync(new List<EnrolledSchemeViewList>()
                {
                    new EnrolledSchemeViewList { EnrolledSchemeId = 1, ClientId = 1, ClientName = "ClientName1", EntityId = 1, EntityName = "EntityName1" },
                    new EnrolledSchemeViewList { EnrolledSchemeId = 2, ClientId = 1, ClientName = "ClientName1", EntityId = 1, EntityName = "EntityName1" },
                    new EnrolledSchemeViewList { EnrolledSchemeId = 2, ClientId = 1, ClientName = "ClientName1", EntityId = 1, EntityName = "EntityName1" },
                });
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            List<string> genericCodes = new List<string>
            {
                GenericCode.STATUS,
                GenericCode.SCHEME_TYPE
            };
            StdRequestParam requestParam = null;
            string token = "token";
            genericCodeRepository.Setup(x => x.GetGenericCodes(genericCodes, token, requestParam))
                .ReturnsAsync(new Dictionary<string, List<GenericCodeResult>>());

            var payrollItemRepository = new Mock<IPayrollItemRepository>();
            var mapper = new Mock<IMapper>();
            IEnrolledSchemeService enrolledSchemeService = new EnrolledSchemeService(enrolledSchemeRepository.Object, payrollItemRepository.Object, mapper.Object);

            var result = enrolledSchemeService.GetByRegisteredSchemeId(registeredSchemeId, request);
            var pagination = result.Result.Data["pagination"] as Pagination<EnrolledSchemeViewList>;
            Assert.AreEqual(3, pagination.Content.Count);
        }

        [Test]
        [TestCase(1)]
        public void GetByEnrolledSchemeId_ReturnFailed(int enrolledSchemeId)
        {
            ApiRequest request = new ApiRequest();
            var enrolledSchemeRepository = new Mock<IEnrolledSchemeRepository>();
            enrolledSchemeRepository.Setup(x => x.GetByRegisteredSchemeId(enrolledSchemeId)).ReturnsAsync(() => null);

            var payrollItemRepository = new Mock<IPayrollItemRepository>();
            var mapper = new Mock<IMapper>();
            IEnrolledSchemeService enrolledSchemeService = new EnrolledSchemeService(enrolledSchemeRepository.Object, payrollItemRepository.Object, mapper.Object);

            var result = enrolledSchemeService.GetById(enrolledSchemeId, request);
            Assert.IsNull(result.Result.Data);
        }

        [Test]
        [TestCase(1, 1)]
        public void GetByEnrolledSchemeId_ReturnSuccess(int enrolledSchemeId, int entityId)
        {
            ApiRequest request = new ApiRequest();
            var enrolledSchemeRepository = new Mock<IEnrolledSchemeRepository>();
            enrolledSchemeRepository.Setup(x => x.GetByEnrolledSchemeId(enrolledSchemeId, entityId)).ReturnsAsync(new TbPayEnrolledScheme { EnrolledSchemeId = enrolledSchemeId });

            var payrollItemRepository = new Mock<IPayrollItemRepository>();
            var mapper = new Mock<IMapper>();
            IEnrolledSchemeService enrolledSchemeService = new EnrolledSchemeService(enrolledSchemeRepository.Object, payrollItemRepository.Object, mapper.Object);

            var result = enrolledSchemeService.GetById(enrolledSchemeId, request);
            var EnrollScheme = result.Result.Data["EnrollScheme"] as EnrolledSchemeViewDetail;
            Assert.IsNotNull(EnrollScheme);
        }

        [Test]
        [TestCase(1)]
        public void CreateEnrollScheme_ReturnFailed(int entityId)
        {
            ApiRequest<EnrolledSchemeRequest> request = new ApiRequest<EnrolledSchemeRequest>();
            var enrolledSchemeRepository = new Mock<IEnrolledSchemeRepository>();
            string createdBy = "system";
            enrolledSchemeRepository.Setup(x => x.Create(request.Data, entityId, createdBy)).ReturnsAsync(() => null);

            request.Data = new EnrolledSchemeRequest
            {
                MPFRegisteredSchemeId = 1,
                EnrolledSchemeName = "enrolled scheme name",
                EnrolledSchemeCode = "enrolled scheme code",
                EnrolledSchemeType = "enrolled scheme type",
                EnrolledSchemeNo = "enrolled scheme no",
                ContributionCycle = "contribution cycle",
                ContactPerson = "contact person",
                PhoneNumber = "phone number",
                FaxNumber = "fax number",
                ParticipationNo = "participation no",
                PaymentMethod = "payment method",
                Currency = "currency",
                PayCentre = "pay centre",
                RoundType = "round type",
                NearestDec = 0,
                StatusCd = "status",
                ClosedDate = DateTime.Now,
                VcPayrollItemIds = new List<int> { 1, 2 }
            };

            var payrollItemRepository = new Mock<IPayrollItemRepository>();
            var mapper = new Mock<IMapper>();
            IEnrolledSchemeService enrolledSchemeService = new EnrolledSchemeService(enrolledSchemeRepository.Object, payrollItemRepository.Object, mapper.Object);

            var result = enrolledSchemeService.Create(request, createdBy);
            Assert.IsNull(result.Result.Data);
        }

        [Test]
        [TestCase(1)]
        public void CreateEnrollScheme_ReturnSuccess(int entityId)
        {
            ApiRequest<EnrolledSchemeRequest> request = new ApiRequest<EnrolledSchemeRequest>();
            string createdBy = "SYSTEM";
            var enrolledSchemeRepository = new Mock<IEnrolledSchemeRepository>();
            request.Data = new EnrolledSchemeRequest
            {
                MPFRegisteredSchemeId = 1,
                EnrolledSchemeName = "enrolled scheme name",
                EnrolledSchemeCode = "enrolled scheme code",
                EnrolledSchemeType = "enrolled scheme type",
                EnrolledSchemeNo = "enrolled scheme no",
                ContributionCycle = "contribution cycle",
                ContactPerson = "contact person",
                PhoneNumber = "phone number",
                FaxNumber = "fax number",
                ParticipationNo = "participation no",
                PaymentMethod = "payment method",
                Currency = "currency",
                PayCentre = "pay centre",
                RoundType = "round type",
                NearestDec = 0,
                StatusCd = "status",
                ClosedDate = DateTime.Now,
                VcPayrollItemIds = new List<int> { 1, 2 }
            };

            int enrolledSchemeId = new Random().Next(1, 10);
            enrolledSchemeRepository.Setup(x => x.Create(request.Data, entityId, createdBy)).ReturnsAsync(new EnrolledSchemeViewList { EnrolledSchemeId = enrolledSchemeId });

            var payrollItemRepository = new Mock<IPayrollItemRepository>();
            var mapper = new Mock<IMapper>();
            IEnrolledSchemeService enrolledSchemeService = new EnrolledSchemeService(enrolledSchemeRepository.Object, payrollItemRepository.Object, mapper.Object);

            var result = enrolledSchemeService.Create(request, createdBy);
            var EnrollScheme = result.Result.Data["enrolledScheme"] as EnrolledSchemeViewList;
            Assert.AreEqual(enrolledSchemeId, EnrollScheme.EnrolledSchemeId);
        }

        [Test]
        [TestCase(1, 1)]
        public void UpdateEnrolledScheme_ReturnFail(int enrolledSchemeId, int entityId)
        {
            ApiRequest<EnrolledSchemeRequest> request = new ApiRequest<EnrolledSchemeRequest>();
            var enrolledSchemeRepository = new Mock<IEnrolledSchemeRepository>();
            enrolledSchemeRepository.Setup(x => x.CheckExist(enrolledSchemeId, entityId)).ReturnsAsync(false);

            var payrollItemRepository = new Mock<IPayrollItemRepository>();
            var mapper = new Mock<IMapper>();
            IEnrolledSchemeService enrolledSchemeService = new EnrolledSchemeService(enrolledSchemeRepository.Object, payrollItemRepository.Object, mapper.Object);

            var result = enrolledSchemeService.Update(enrolledSchemeId, request, "system");
            Assert.IsNull(result.Result.Data);
        }

        [Test]
        [TestCase(1, 1)]
        public void UpdateEnrollScheme_ReturnSuccess(int enrolledSchemeId, int entityId)
        {
            ApiRequest<EnrolledSchemeRequest> request = new ApiRequest<EnrolledSchemeRequest>();
            string modifiedBy = "SYSTEM";
            var enrolledSchemeRepository = new Mock<IEnrolledSchemeRepository>();
            enrolledSchemeRepository.Setup(x => x.CheckExist(enrolledSchemeId, entityId)).ReturnsAsync(true);

            request.Data = new EnrolledSchemeRequest
            {
                MPFRegisteredSchemeId = 1,
                EnrolledSchemeName = "enrolled scheme name",
                EnrolledSchemeCode = "enrolled scheme code",
                EnrolledSchemeType = "enrolled scheme type",
                EnrolledSchemeNo = "enrolled scheme no",
                ContributionCycle = "contribution cycle",
                ContactPerson = "contact person",
                PhoneNumber = "phone number",
                FaxNumber = "fax number",
                ParticipationNo = "participation no",
                PaymentMethod = "payment method",
                Currency = "currency",
                PayCentre = "pay centre",
                RoundType = "round type",
                NearestDec = 0,
                StatusCd = "status",
                ClosedDate = DateTime.Now,
                VcPayrollItemIds = new List<int> { 1, 2 }
            };

            var payrollItemRepository = new Mock<IPayrollItemRepository>();
            var mapper = new Mock<IMapper>();
            IEnrolledSchemeService enrolledSchemeService = new EnrolledSchemeService(enrolledSchemeRepository.Object, payrollItemRepository.Object, mapper.Object);

            var result = enrolledSchemeService.Update(enrolledSchemeId, request, modifiedBy);
            var EnrollScheme = result.Result.Data["enrolledScheme"] as EnrolledSchemeViewList;
            Assert.AreEqual(enrolledSchemeId, EnrollScheme.EnrolledSchemeId);
        }

        [Test]
        [TestCase(1, 1)]
        public void DeleteEnrollScheme_ReturnFail(int enrolledSchemeId, int entityId)
        {
            ApiRequest request = new ApiRequest();
            var enrolledSchemeRepository = new Mock<IEnrolledSchemeRepository>();
            enrolledSchemeRepository.Setup(x => x.CheckExist(enrolledSchemeId, entityId)).ReturnsAsync(false);

            var payrollItemRepository = new Mock<IPayrollItemRepository>();
            var mapper = new Mock<IMapper>();
            IEnrolledSchemeService enrolledSchemeService = new EnrolledSchemeService(enrolledSchemeRepository.Object, payrollItemRepository.Object, mapper.Object);
            var result = enrolledSchemeService.Delete(enrolledSchemeId, request);
            Assert.IsNull(result.Result.Data);
        }


        [Test]
        public void DeleteManyEnrollScheme_ReturnFail()
        {
            List<EnrolledSchemeEntityRequest> enrolledSchemeEntities = new List<EnrolledSchemeEntityRequest>()
            {
                new EnrolledSchemeEntityRequest { EntityId = 1, EnrolledSchemeId = 1},
                new EnrolledSchemeEntityRequest { EntityId = 1, EnrolledSchemeId = 2},
                new EnrolledSchemeEntityRequest { EntityId = 2, EnrolledSchemeId = 1},
                new EnrolledSchemeEntityRequest { EntityId = 2, EnrolledSchemeId = 2},
            };
            ApiRequest<List<EnrolledSchemeEntityRequest>> request = new ApiRequest<List<EnrolledSchemeEntityRequest>>();
            request.Data = enrolledSchemeEntities;
            var enrolledSchemeRepository = new Mock<IEnrolledSchemeRepository>();
            enrolledSchemeRepository.Setup(x => x.DeleteMany(enrolledSchemeEntities)).ReturnsAsync(false);

            var payrollItemRepository = new Mock<IPayrollItemRepository>();
            var mapper = new Mock<IMapper>();
            IEnrolledSchemeService enrolledSchemeService = new EnrolledSchemeService(enrolledSchemeRepository.Object, payrollItemRepository.Object, mapper.Object);
            var result = enrolledSchemeService.DeleteMany(request);
            Assert.IsNull(result.Result.Data);
        }

        [Test]
        public void DeleteManyEnrollScheme_ReturnSuccess()
        {
            List<EnrolledSchemeEntityRequest> enrolledSchemeEntities = new List<EnrolledSchemeEntityRequest>()
            {
                new EnrolledSchemeEntityRequest { EntityId = 1, EnrolledSchemeId = 1},
                new EnrolledSchemeEntityRequest { EntityId = 1, EnrolledSchemeId = 2},
                new EnrolledSchemeEntityRequest { EntityId = 2, EnrolledSchemeId = 1},
                new EnrolledSchemeEntityRequest { EntityId = 2, EnrolledSchemeId = 2},
            };
            ApiRequest<List<EnrolledSchemeEntityRequest>> request = new ApiRequest<List<EnrolledSchemeEntityRequest>>();
            request.Data = enrolledSchemeEntities;
            var enrolledSchemeRepository = new Mock<IEnrolledSchemeRepository>();
            enrolledSchemeRepository.Setup(x => x.DeleteMany(enrolledSchemeEntities)).ReturnsAsync(true);

            var payrollItemRepository = new Mock<IPayrollItemRepository>();
            var mapper = new Mock<IMapper>();
            IEnrolledSchemeService enrolledSchemeService = new EnrolledSchemeService(enrolledSchemeRepository.Object, payrollItemRepository.Object, mapper.Object);
            var result = enrolledSchemeService.DeleteMany(request);
            Assert.AreEqual(true, result.Result.Data);
        }
    }
}
