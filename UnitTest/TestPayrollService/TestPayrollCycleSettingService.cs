﻿using CommonLib.Dtos;
using CommonLib.Repositories.Interfaces;
using Moq;
using NUnit.Framework;
using PayrollServiceAPIs.PayRoll.Dtos.PayrollCycleSetting;
using PayrollServiceAPIs.PayRoll.Repositories.Interfaces;
using PayrollServiceAPIs.PayRoll.Services.Impl;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestPayrollService
{
    class TestPayrollCycleSettingService
    {
        [Test]
        public void GetDataPayrollCycleSetting_ReturnSuccess()
        {
            ApiRequest request = new ApiRequest()
            {
                Parameter = new StdRequestParam()
                {
                    Locale = "en",
                    ClientId = 1,
                    EntityId = 1
                }
            };
            var payrollCycleSettingRepository = new Mock<IPayrollCycleSettingRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            payrollCycleSettingRepository.Setup(x => x.GetSeting()).ReturnsAsync(new PayrollServiceAPIs.PayRoll.Dtos.PayrollCycleSetting.PayrollCycleSettingModel());
            
            PayrollCycleSettingService service = new PayrollCycleSettingService(payrollCycleSettingRepository.Object, genericCodeRepository.Object);

            var payrollCycleSetting = service.Edit(request).Result;

            //Assert
            Assert.IsNotNull(payrollCycleSetting);
            Assert.IsNotNull(payrollCycleSetting.Data);
        }

        [Test]
        public void UpdatePayrollCycleSetting_ReturnSuccess()
        {
            ApiRequest<PayrollCycleSettingUpdateModel> request = new ApiRequest<PayrollCycleSettingUpdateModel>()
            {
                Parameter = new StdRequestParam()
                {
                    Locale = "en",
                    ClientId = 0,
                    EntityId = 0
                },
                Data = new PayrollCycleSettingUpdateModel()
                {
                    PayrollCyclePrequency = "Monthly",
                    CutOffDay = 10,
                    StartDay = 15
                }
            };

            var payrollCycleSettingRepository = new Mock<IPayrollCycleSettingRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();

            payrollCycleSettingRepository.Setup(x => x.Update(request.Data));

            PayrollCycleSettingService service = new PayrollCycleSettingService(payrollCycleSettingRepository.Object, genericCodeRepository.Object);

            var payrollCycleSetting = service.Update(request).Result;

            //Assert
            Assert.IsNotNull(payrollCycleSetting);
            Assert.AreEqual(true, payrollCycleSetting.Data);
        }
    }
}
