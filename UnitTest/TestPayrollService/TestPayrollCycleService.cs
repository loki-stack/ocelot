using CommonLib.Dtos;
using CommonLib.Repositories.Interfaces;
using Moq;
using NUnit.Framework;
using PayrollServiceAPIs.Common.Constants;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos.PayrollCycle;
using PayrollServiceAPIs.PayRoll.Repositories.Interfaces;
using PayrollServiceAPIs.PayRoll.Services.Impl;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TestPayrollService
{
    public class TestPayrollCycleService
    {
        [Test]
        public void GetPayrollCycle_Has2PayrollCycle_Return2PayrollCycle()
        {
            string token = string.Empty;
            List<string> lstGenericCode = new List<string>
            {
                GenericCode.STATUS
            };
            ApiRequest<FilterRequest> request = new ApiRequest<FilterRequest>();
            request.Data = new FilterRequest();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 1,
                EntityId = 1
            };
            var payrollCycleRepository = new Mock<IPayrollCycleRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            var payrollCycleSettingRepository = new Mock<IPayrollCycleSettingRepository>();
            payrollCycleRepository.Setup(x => x.GetAll())
                    .ReturnsAsync(new List<PayrollCycleViewModel>() { new PayrollCycleViewModel(), new PayrollCycleViewModel() });

            genericCodeRepository.Setup(x => x.GetGenericCodes(lstGenericCode, token, request.Parameter))
                    .ReturnsAsync(new Dictionary<string, List<GenericCodeResult>>
                {
                    {
                        GenericCode.STATUS.ToLower(),
                        new List<GenericCodeResult>
                        {
                            new GenericCodeResult { CodeValue = "Active" },
                            new GenericCodeResult { CodeValue = "Inactive" },
                            new GenericCodeResult { CodeValue = "Closed" }
                        }
                    }
                });

            PayrollCycleService service = new PayrollCycleService(payrollCycleRepository.Object, genericCodeRepository.Object, payrollCycleSettingRepository.Object);

            var payrollCycle = service.GetAll(request, token).Result;

            //Assert
            Assert.IsNotNull(payrollCycle);
            Assert.AreEqual(2, payrollCycle.Data.Count);
        }

        [Test]
        public void StorePayrollCycle_ReturnSuccess()
        {
            string createBy = "admin";
            int cutOfDay = 10, startDay = 15;
            ApiRequest<int> request = new ApiRequest<int>()
            {
                Parameter = new StdRequestParam()
                {
                    Locale = "en",
                    ClientId = 0,
                    EntityId = 0
                },
                Data = 2022
            };
            var payrollCycleRepository = new Mock<IPayrollCycleRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            var payrollCycleSettingRepository = new Mock<IPayrollCycleSettingRepository>();
            for (var i = 1; i <= 12; i++)
            {
                int days = DateTime.DaysInMonth(request.Data, i);
                payrollCycleRepository.Setup(x => x.Store(new PayrollCycleStoreModel()
                {
                    PayrollCycleCode = new DateTime(request.Data, i, startDay <= days ? startDay : days).ToString("yyyyMMdd"),
                    DateStart = new DateTimeOffset(new DateTime(request.Data, i, startDay <= days ? startDay : days)),
                    CutOffDate = new DateTimeOffset(new DateTime(request.Data, i, cutOfDay <= days ? cutOfDay : days)),
                    DateEnd = new DateTimeOffset(new DateTime(request.Data, i, startDay <= days ? startDay : days).AddMonths(1).AddDays(-1)),
                    CreatedBy = createBy,
                    ModifiedBy = createBy
                })).ReturnsAsync(i);
            }

            payrollCycleSettingRepository.Setup(x => x.GetSeting()).ReturnsAsync(new PayrollServiceAPIs.PayRoll.Dtos.PayrollCycleSetting.PayrollCycleSettingModel()
            {
                CutOffDay = 10,
                StartDay = 15,
                PayrollCyclePrequency = "Monthly"
            });


            PayrollCycleService service = new PayrollCycleService(payrollCycleRepository.Object, genericCodeRepository.Object, payrollCycleSettingRepository.Object);

            var payrollCycle = service.Store(request, createBy).Result;

            //Assert
            Assert.IsNotNull(payrollCycle);
            Assert.IsTrue(payrollCycle.Data);
        }

        [Test]
        public void UpdatePayrollCycle_PayrollCycleNotExist_ReturnFail()
        {
            int payrollCycleId = 1;
            ApiRequest<PayrollCycleStoreModel> request = new ApiRequest<PayrollCycleStoreModel>()
            {
                Parameter = new StdRequestParam()
                {
                    Locale = "en",
                    ClientId = 0,
                    EntityId = 0
                },
                Data = new PayrollCycleStoreModel()
                {
                    PayrollCycleCode = "Code 1",
                    DateStart = DateTime.Now,
                    DateEnd = DateTime.Now.AddMonths(1),
                    CutOffDate = DateTime.Now.AddDays(10),
                    CreatedBy = "admin"
                }
            };
            var payrollCycleRepository = new Mock<IPayrollCycleRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            var payrollCycleSettingRepository = new Mock<IPayrollCycleSettingRepository>();
            payrollCycleRepository.Setup(x => x.GetById(payrollCycleId)).ReturnsAsync((PayrollCycleViewModel)null);
            payrollCycleRepository.Setup(x => x.CheckLinkPayrollRun(payrollCycleId)).ReturnsAsync(true);
            PayrollCycleService service = new PayrollCycleService(payrollCycleRepository.Object, genericCodeRepository.Object, payrollCycleSettingRepository.Object);

            var payrollCycle = service.Update(payrollCycleId, request).Result;

            //Assert
            Assert.IsNotNull(payrollCycle);
            Assert.AreEqual(0, payrollCycle.Data);
            Assert.AreEqual(payrollCycle.Message.Toast.First().LabelCode, Label.PAY0804);
        }

        [Test]
        public void UpdatePayrollCycle_DuplicateCode_ReturnFail()
        {
            int payrollCycleId = 1;
            ApiRequest<PayrollCycleStoreModel> request = new ApiRequest<PayrollCycleStoreModel>()
            {
                Parameter = new StdRequestParam()
                {
                    Locale = "en",
                    ClientId = 0,
                    EntityId = 0
                },
                Data = new PayrollCycleStoreModel()
                {
                    PayrollCycleCode = "Code 1",
                    DateStart = DateTime.Now,
                    DateEnd = DateTime.Now.AddMonths(1),
                    CutOffDate = DateTime.Now.AddDays(10),
                    CreatedBy = "admin"
                }
            };
            var payrollCycleRepository = new Mock<IPayrollCycleRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            var payrollCycleSettingRepository = new Mock<IPayrollCycleSettingRepository>();
            payrollCycleRepository.Setup(x => x.GetById(payrollCycleId)).ReturnsAsync(new PayrollCycleViewModel());
            payrollCycleRepository.Setup(x => x.CheckLinkPayrollRun(payrollCycleId)).ReturnsAsync(true);
            payrollCycleRepository.Setup(x => x.GetByCode(request.Data.PayrollCycleCode)).ReturnsAsync(new PayrollCycleViewModel() { PayrollCycleId = 2 });

            PayrollCycleService service = new PayrollCycleService(payrollCycleRepository.Object, genericCodeRepository.Object, payrollCycleSettingRepository.Object);

            var payrollCycle = service.Update(payrollCycleId, request).Result;

            //Assert
            Assert.IsNotNull(payrollCycle);
            Assert.AreEqual(0, payrollCycle.Data);
            Assert.AreEqual(payrollCycle.Message.Toast.First().LabelCode, Label.PAY0801);
        }

        [Test]
        public void UpdatePayrollCycle_ReturnSuccess()
        {
            int payrollCycleId = 1;
            ApiRequest<PayrollCycleStoreModel> request = new ApiRequest<PayrollCycleStoreModel>()
            {
                Parameter = new StdRequestParam()
                {
                    Locale = "en",
                    ClientId = 0,
                    EntityId = 0
                },
                Data = new PayrollCycleStoreModel()
                {
                    PayrollCycleCode = "Code 1",
                    DateStart = DateTime.Now,
                    DateEnd = DateTime.Now.AddMonths(1),
                    CutOffDate = DateTime.Now.AddDays(10),
                    CreatedBy = "admin"
                }
            };
            var payrollCycleRepository = new Mock<IPayrollCycleRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            var payrollCycleSettingRepository = new Mock<IPayrollCycleSettingRepository>();
            payrollCycleRepository.Setup(x => x.GetById(payrollCycleId)).ReturnsAsync(new PayrollCycleViewModel());
            payrollCycleRepository.Setup(x => x.CheckLinkPayrollRun(payrollCycleId)).ReturnsAsync(true);
            payrollCycleRepository.Setup(x => x.GetByCode(request.Data.PayrollCycleCode)).ReturnsAsync(new PayrollCycleViewModel() { PayrollCycleId = payrollCycleId });
            payrollCycleRepository.Setup(x => x.Update(payrollCycleId, request.Data)).ReturnsAsync(payrollCycleId);
            PayrollCycleService service = new PayrollCycleService(payrollCycleRepository.Object, genericCodeRepository.Object, payrollCycleSettingRepository.Object);

            var payrollCycle = service.Update(payrollCycleId, request).Result;

            //Assert
            Assert.IsNotNull(payrollCycle);
            Assert.AreEqual(payrollCycleId, payrollCycle.Data);
        }

        [Test]
        public void DeletePayrollCycle_PayrollCycleNotExist_ReturnFail()
        {
            string modifiedBy = "admin";
            ApiRequest<List<int>> request = new ApiRequest<List<int>>()
            {
                Parameter = new StdRequestParam()
                {
                    Locale = "en",
                    ClientId = 0,
                    EntityId = 0
                },
                Data = new List<int>()
                {
                    1,2
                }
            };

            var payrollCycleRepository = new Mock<IPayrollCycleRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            var payrollCycleSettingRepository = new Mock<IPayrollCycleSettingRepository>();
            payrollCycleRepository.Setup(x => x.Delete(request.Data.ElementAt(0), modifiedBy)).ReturnsAsync(true);
            payrollCycleRepository.Setup(x => x.Delete(request.Data.ElementAt(1), modifiedBy)).ReturnsAsync(false);
            payrollCycleRepository.Setup(x => x.CheckLinkPayrollRun(request.Data.ElementAt(0))).ReturnsAsync(true);
            payrollCycleRepository.Setup(x => x.CheckLinkPayrollRun(request.Data.ElementAt(1))).ReturnsAsync(true);
            PayrollCycleService service = new PayrollCycleService(payrollCycleRepository.Object, genericCodeRepository.Object, payrollCycleSettingRepository.Object);

            var result = service.Delete(request, modifiedBy).Result;
            //Assert
            Assert.IsNotNull(result);
            Assert.IsFalse(result.Data);
            Assert.AreEqual(result.Message.Toast.First().LabelCode, Label.PAY0804);
        }

        [Test]
        public void DeletePayrollCycle_ReturnSuccess()
        {
            string modifiedBy = "admin";
            ApiRequest<List<int>> request = new ApiRequest<List<int>>()
            {
                Parameter = new StdRequestParam()
                {
                    Locale = "en",
                    ClientId = 0,
                    EntityId = 0
                },
                Data = new List<int>()
                {
                    1,2
                }
            };

            var payrollCycleRepository = new Mock<IPayrollCycleRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            var payrollCycleSettingRepository = new Mock<IPayrollCycleSettingRepository>();
            payrollCycleRepository.Setup(x => x.Delete(request.Data.ElementAt(0), modifiedBy)).ReturnsAsync(true);
            payrollCycleRepository.Setup(x => x.Delete(request.Data.ElementAt(1), modifiedBy)).ReturnsAsync(true);
            payrollCycleRepository.Setup(x => x.CheckLinkPayrollRun(request.Data.ElementAt(0))).ReturnsAsync(true);
            payrollCycleRepository.Setup(x => x.CheckLinkPayrollRun(request.Data.ElementAt(1))).ReturnsAsync(true);
            PayrollCycleService service = new PayrollCycleService(payrollCycleRepository.Object, genericCodeRepository.Object, payrollCycleSettingRepository.Object);

            var result = service.Delete(request, modifiedBy).Result;
            //Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Data);
            Assert.AreEqual(result.Message.Toast.First().LabelCode, Label.PAY0807);
        }
    }
}