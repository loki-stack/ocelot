using CommonLib.Dtos;
using CommonLib.Repositories.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Moq;
using NUnit.Framework;
using PayrollServiceAPIs.Common.Constants;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos.PayrollRun;
using PayrollServiceAPIs.PayRoll.Repositories.Interfaces;
using PayrollServiceAPIs.PayRoll.Services.Impl;
using PayrollServiceAPIs.PayRoll.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace TestPayrollService
{
    public class TestPayrollRunService
    {
        [Test]
        public void GetPayrollRun_Has2PayrollRun_Return2PayrollRun()
        {
            string token = string.Empty;
            List<string> lstGenericCode = new List<string>
            {
                GenericCode.PAYROLL_RUN_STATUS
            };
            ApiRequest<FilterRequest> request = new ApiRequest<FilterRequest>();
            request.Data = new FilterRequest();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 1,
                EntityId = 1
            };
            var payrollRunRepository = new Mock<IPayrollRunRepository>();
            var payrollItemRepository = new Mock<IMasterPayrollItemRepository>();
            var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            var formulasRepository = new Mock<IFormulasRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            var employeeService = new Mock<IEmployeeService>();
            var webHostEnvironment = new Mock<IWebHostEnvironment>();
            payrollRunRepository.Setup(x => x.GetAll(request.Data))
                    .ReturnsAsync(new List<PayrollRunViewModel>() { new PayrollRunViewModel(), new PayrollRunViewModel()});

            genericCodeRepository.Setup(x => x.GetGenericCodes(lstGenericCode, token, request.Parameter))
                    .ReturnsAsync(new Dictionary<string, List<GenericCodeResult>>
                {
                    {
                        GenericCode.PAYROLL_RUN_STATUS.ToLower(),
                        new List<GenericCodeResult>
                        {
                            new GenericCodeResult { CodeValue = "Confirm" },
                            new GenericCodeResult { CodeValue = "Draft" },
                            new GenericCodeResult { CodeValue = "Final" },
                            new GenericCodeResult { CodeValue = "Lock" }
                        }
                    }
                });

            PayrollRunService service = new PayrollRunService(payrollRunRepository.Object, payrollItemRepository.Object, payrollProfileRepository.Object,
                formulasRepository.Object, genericCodeRepository.Object, employeeService.Object, webHostEnvironment.Object);

            var payrollRuns = service.GetAll(request, token).Result;

            //Assert
            Assert.IsNotNull(payrollRuns);
            Assert.AreEqual(2, payrollRuns.Data.Count);
        }

        [Test]
        public void GetDataCreate_ReturnDataStatusAndCycle()
        {
            string token = string.Empty;
            List<string> lstGenericCode = new List<string>
            {
                GenericCode.PAYROLL_RUN_STATUS
            };
            ApiRequest request = new ApiRequest();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 1,
                EntityId = 1
            };

            var payrollRunRepository = new Mock<IPayrollRunRepository>();
            var payrollItemRepository = new Mock<IMasterPayrollItemRepository>();
            var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            var formulasRepository = new Mock<IFormulasRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            var employeeService = new Mock<IEmployeeService>();
            var webHostEnvironment = new Mock<IWebHostEnvironment>();
            payrollRunRepository.Setup(x => x.GetPayrollCycle())
                    .ReturnsAsync(new List<CommonLib.Models.Client.TbPayPayrollCycle>() { new CommonLib.Models.Client.TbPayPayrollCycle(), new CommonLib.Models.Client.TbPayPayrollCycle()});

            genericCodeRepository.Setup(x => x.GetGenericCodes(lstGenericCode, token, request.Parameter))
                    .ReturnsAsync(new Dictionary<string, List<GenericCodeResult>>
                {
                    {
                        GenericCode.PAYROLL_RUN_STATUS.ToLower(),
                        new List<GenericCodeResult>
                        {
                            new GenericCodeResult { CodeValue = "Confirm" },
                            new GenericCodeResult { CodeValue = "Draft" },
                            new GenericCodeResult { CodeValue = "Final" },
                            new GenericCodeResult { CodeValue = "Lock" }
                        }
                    }
                });

            PayrollRunService service = new PayrollRunService(payrollRunRepository.Object, payrollItemRepository.Object, payrollProfileRepository.Object,
                formulasRepository.Object, genericCodeRepository.Object, employeeService.Object, webHostEnvironment.Object);

            var dataCreate = service.Create(request, token).Result;

            //Assert
            Assert.IsNotNull(dataCreate);
            Assert.AreEqual(4, dataCreate.Data.Status.Count);
            Assert.AreEqual(2, dataCreate.Data.Cycles.Count);
        }

        [Test]
        public void GetDataEdit_ReturnDataEditAndStatusAndCycle()
        {
            int payrollRunId = 1;
            string token = string.Empty;
            List<string> lstGenericCode = new List<string>
            {
                GenericCode.PAYROLL_RUN_STATUS
            };
            ApiRequest request = new ApiRequest();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 1,
                EntityId = 1
            };

            var payrollRunRepository = new Mock<IPayrollRunRepository>();
            var payrollItemRepository = new Mock<IMasterPayrollItemRepository>();
            var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            var formulasRepository = new Mock<IFormulasRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            var employeeService = new Mock<IEmployeeService>();
            var webHostEnvironment = new Mock<IWebHostEnvironment>();
            payrollRunRepository.Setup(x => x.GetPayrollCycle())
                    .ReturnsAsync(new List<CommonLib.Models.Client.TbPayPayrollCycle>() { new CommonLib.Models.Client.TbPayPayrollCycle(), new CommonLib.Models.Client.TbPayPayrollCycle() });
            payrollRunRepository.Setup(x => x.GetById(payrollRunId))
                    .ReturnsAsync(new PayrollRunViewModel());

            genericCodeRepository.Setup(x => x.GetGenericCodes(lstGenericCode, token, request.Parameter))
                    .ReturnsAsync(new Dictionary<string, List<GenericCodeResult>>
                {
                    {
                        GenericCode.PAYROLL_RUN_STATUS.ToLower(),
                        new List<GenericCodeResult>
                        {
                            new GenericCodeResult { CodeValue = "Confirm" },
                            new GenericCodeResult { CodeValue = "Draft" },
                            new GenericCodeResult { CodeValue = "Final" },
                            new GenericCodeResult { CodeValue = "Lock" }
                        }
                    }
                });

            PayrollRunService service = new PayrollRunService(payrollRunRepository.Object, payrollItemRepository.Object, payrollProfileRepository.Object,
                formulasRepository.Object, genericCodeRepository.Object, employeeService.Object, webHostEnvironment.Object);

            var dataCreate = service.Edit(payrollRunId, request, token).Result;

            //Assert
            Assert.IsNotNull(dataCreate);
            Assert.IsNotNull(dataCreate.Data.Detail);
            Assert.AreEqual(4, dataCreate.Data.Status.Count);
            Assert.AreEqual(2, dataCreate.Data.Cycles.Count);
        }

        [Test]
        public void StorePayrollRun_StatusNotExist_ReturnFail()
        {
            string token = string.Empty;
            List<string> lstGenericCode = new List<string>
            {
                GenericCode.PAYROLL_RUN_STATUS
            };
            ApiRequest<PayrollRunStoreModel> request = new ApiRequest<PayrollRunStoreModel>();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 1,
                EntityId = 1
            };

            request.Data = new PayrollRunStoreModel()
            {
                Name = "Test",
                PayrollCycleId = 1,
                StatusCd = "Active"
            };

            var payrollRunRepository = new Mock<IPayrollRunRepository>();
            var payrollItemRepository = new Mock<IMasterPayrollItemRepository>();
            var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            var formulasRepository = new Mock<IFormulasRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            var employeeService = new Mock<IEmployeeService>();
            var webHostEnvironment = new Mock<IWebHostEnvironment>();

            genericCodeRepository.Setup(x => x.GetGenericCodes(lstGenericCode, token, request.Parameter))
                    .ReturnsAsync(new Dictionary<string, List<GenericCodeResult>>
                {
                    {
                        GenericCode.PAYROLL_RUN_STATUS.ToLower(),
                        new List<GenericCodeResult>
                        {
                            new GenericCodeResult { CodeValue = "Confirm" },
                            new GenericCodeResult { CodeValue = "Draft" },
                            new GenericCodeResult { CodeValue = "Final" },
                            new GenericCodeResult { CodeValue = "Lock" }
                        }
                    }
                });

            PayrollRunService service = new PayrollRunService(payrollRunRepository.Object, payrollItemRepository.Object, payrollProfileRepository.Object,
                formulasRepository.Object, genericCodeRepository.Object, employeeService.Object, webHostEnvironment.Object);

            var response = service.Store(request, token).Result;

            //Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(0, response.Data);
            Assert.AreEqual(Label.PAY0407, response.Message.Toast.First().LabelCode);
        }

        [Test]
        public void StorePayrollRun_PayrollCycleNotExist_ReturnFail()
        {
            string token = string.Empty;
            List<string> lstGenericCode = new List<string>
            {
                GenericCode.PAYROLL_RUN_STATUS
            };
            ApiRequest<PayrollRunStoreModel> request = new ApiRequest<PayrollRunStoreModel>();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 1,
                EntityId = 1
            };

            request.Data = new PayrollRunStoreModel()
            {
                Name = "Test",
                PayrollCycleId = 1,
                StatusCd = "Draft"
            };

            var payrollRunRepository = new Mock<IPayrollRunRepository>();
            var payrollItemRepository = new Mock<IMasterPayrollItemRepository>();
            var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            var formulasRepository = new Mock<IFormulasRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            var employeeService = new Mock<IEmployeeService>();
            var webHostEnvironment = new Mock<IWebHostEnvironment>();

            payrollRunRepository.Setup(x => x.CheckPayrollCycle(request.Data.PayrollCycleId))
                .ReturnsAsync(false);

            genericCodeRepository.Setup(x => x.GetGenericCodes(lstGenericCode, token, request.Parameter))
                    .ReturnsAsync(new Dictionary<string, List<GenericCodeResult>>
                {
                    {
                        GenericCode.PAYROLL_RUN_STATUS.ToLower(),
                        new List<GenericCodeResult>
                        {
                            new GenericCodeResult { CodeValue = "Confirm" },
                            new GenericCodeResult { CodeValue = "Draft" },
                            new GenericCodeResult { CodeValue = "Final" },
                            new GenericCodeResult { CodeValue = "Lock" }
                        }
                    }
                });

            PayrollRunService service = new PayrollRunService(payrollRunRepository.Object, payrollItemRepository.Object, payrollProfileRepository.Object,
                formulasRepository.Object, genericCodeRepository.Object, employeeService.Object, webHostEnvironment.Object);

            var response = service.Store(request, token).Result;

            //Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(0, response.Data);
            Assert.AreEqual(Label.PAY0410, response.Message.Toast.First().LabelCode);
        }

        [Test]
        public void StorePayrollRun_StoreFailt_ReturnFail()
        {
            string token = string.Empty;
            List<string> lstGenericCode = new List<string>
            {
                GenericCode.PAYROLL_RUN_STATUS
            };
            ApiRequest<PayrollRunStoreModel> request = new ApiRequest<PayrollRunStoreModel>();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 1,
                EntityId = 1
            };

            request.Data = new PayrollRunStoreModel()
            {
                Name = "Test",
                PayrollCycleId = 1,
                StatusCd = "Draft"
            };

            var payrollRunRepository = new Mock<IPayrollRunRepository>();
            var payrollItemRepository = new Mock<IMasterPayrollItemRepository>();
            var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            var formulasRepository = new Mock<IFormulasRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            var employeeService = new Mock<IEmployeeService>();
            var webHostEnvironment = new Mock<IWebHostEnvironment>();

            payrollRunRepository.Setup(x => x.CheckPayrollCycle(request.Data.PayrollCycleId))
                .ReturnsAsync(true);

            payrollRunRepository.Setup(x => x.Store(request.Data))
                .ReturnsAsync((int?)null);

            genericCodeRepository.Setup(x => x.GetGenericCodes(lstGenericCode, token, request.Parameter))
                    .ReturnsAsync(new Dictionary<string, List<GenericCodeResult>>
                {
                    {
                        GenericCode.PAYROLL_RUN_STATUS.ToLower(),
                        new List<GenericCodeResult>
                        {
                            new GenericCodeResult { CodeValue = "Confirm" },
                            new GenericCodeResult { CodeValue = "Draft" },
                            new GenericCodeResult { CodeValue = "Final" },
                            new GenericCodeResult { CodeValue = "Lock" }
                        }
                    }
                });

            PayrollRunService service = new PayrollRunService(payrollRunRepository.Object, payrollItemRepository.Object, payrollProfileRepository.Object,
                formulasRepository.Object, genericCodeRepository.Object, employeeService.Object, webHostEnvironment.Object);

            var response = service.Store(request, token).Result;

            //Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(0, response.Data);
            Assert.AreEqual(Label.PAY0401, response.Message.Toast.First().LabelCode);
        }

        [Test]
        public void StorePayrollCycle_ReturnSuccess()
        {
            string token = string.Empty;
            List<string> lstGenericCode = new List<string>
            {
                GenericCode.PAYROLL_RUN_STATUS
            };
            ApiRequest<PayrollRunStoreModel> request = new ApiRequest<PayrollRunStoreModel>();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 1,
                EntityId = 1
            };

            request.Data = new PayrollRunStoreModel()
            {
                Name = "Test",
                PayrollCycleId = 1,
                StatusCd = "Draft"
            };

            var payrollRunRepository = new Mock<IPayrollRunRepository>();
            var payrollItemRepository = new Mock<IMasterPayrollItemRepository>();
            var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            var formulasRepository = new Mock<IFormulasRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            var employeeService = new Mock<IEmployeeService>();
            var webHostEnvironment = new Mock<IWebHostEnvironment>();

            payrollRunRepository.Setup(x => x.CheckPayrollCycle(request.Data.PayrollCycleId))
                .ReturnsAsync(true);

            payrollRunRepository.Setup(x => x.Store(request.Data))
                .ReturnsAsync(1);

            genericCodeRepository.Setup(x => x.GetGenericCodes(lstGenericCode, token, request.Parameter))
                    .ReturnsAsync(new Dictionary<string, List<GenericCodeResult>>
                {
                    {
                        GenericCode.PAYROLL_RUN_STATUS.ToLower(),
                        new List<GenericCodeResult>
                        {
                            new GenericCodeResult { CodeValue = "Confirm" },
                            new GenericCodeResult { CodeValue = "Draft" },
                            new GenericCodeResult { CodeValue = "Final" },
                            new GenericCodeResult { CodeValue = "Lock" }
                        }
                    }
                });

            PayrollRunService service = new PayrollRunService(payrollRunRepository.Object, payrollItemRepository.Object, payrollProfileRepository.Object,
                formulasRepository.Object, genericCodeRepository.Object, employeeService.Object, webHostEnvironment.Object);

            var response = service.Store(request, token).Result;

            //Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(1, response.Data);
        }

        [Test]
        public void UpdatePayrollRun_PayrollRunNotExist_ReturnFail()
        {
            int payrollRunId = 1;
            string token = string.Empty;
            List<string> lstGenericCode = new List<string>
            {
                GenericCode.PAYROLL_RUN_STATUS
            };
            ApiRequest<PayrollRunStoreModel> request = new ApiRequest<PayrollRunStoreModel>();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 1,
                EntityId = 1
            };

            request.Data = new PayrollRunStoreModel()
            {
                Name = "Test",
                PayrollCycleId = 1,
                StatusCd = "Active"
            };

            var payrollRunRepository = new Mock<IPayrollRunRepository>();
            var payrollItemRepository = new Mock<IMasterPayrollItemRepository>();
            var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            var formulasRepository = new Mock<IFormulasRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            var employeeService = new Mock<IEmployeeService>();
            var webHostEnvironment = new Mock<IWebHostEnvironment>();

            payrollRunRepository.Setup(x => x.GetById(payrollRunId))
                .ReturnsAsync((PayrollRunViewModel)null);

            genericCodeRepository.Setup(x => x.GetGenericCodes(lstGenericCode, token, request.Parameter))
                    .ReturnsAsync(new Dictionary<string, List<GenericCodeResult>>
                {
                    {
                        GenericCode.PAYROLL_RUN_STATUS.ToLower(),
                        new List<GenericCodeResult>
                        {
                            new GenericCodeResult { CodeValue = "Confirm" },
                            new GenericCodeResult { CodeValue = "Draft" },
                            new GenericCodeResult { CodeValue = "Final" },
                            new GenericCodeResult { CodeValue = "Lock" }
                        }
                    }
                });

            PayrollRunService service = new PayrollRunService(payrollRunRepository.Object, payrollItemRepository.Object, payrollProfileRepository.Object,
                formulasRepository.Object, genericCodeRepository.Object, employeeService.Object, webHostEnvironment.Object);

            var response = service.Update(payrollRunId, request, token).Result;

            //Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(Label.PAY0403, response.Message.Toast.First().LabelCode);
        }

        [Test]
        public void UpdatePayrollRun_StatusNotExist_ReturnFail()
        {
            int payrollRunId = 1;
            string token = string.Empty;
            List<string> lstGenericCode = new List<string>
            {
                GenericCode.PAYROLL_RUN_STATUS
            };
            ApiRequest<PayrollRunStoreModel> request = new ApiRequest<PayrollRunStoreModel>();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 1,
                EntityId = 1
            };

            request.Data = new PayrollRunStoreModel()
            {
                Name = "Test",
                PayrollCycleId = 1,
                StatusCd = "Active"
            };

            var payrollRunRepository = new Mock<IPayrollRunRepository>();
            var payrollItemRepository = new Mock<IMasterPayrollItemRepository>();
            var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            var formulasRepository = new Mock<IFormulasRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            var employeeService = new Mock<IEmployeeService>();
            var webHostEnvironment = new Mock<IWebHostEnvironment>();

            payrollRunRepository.Setup(x => x.GetById(payrollRunId))
                .ReturnsAsync(new PayrollRunViewModel());

            genericCodeRepository.Setup(x => x.GetGenericCodes(lstGenericCode, token, request.Parameter))
                    .ReturnsAsync(new Dictionary<string, List<GenericCodeResult>>
                {
                    {
                        GenericCode.PAYROLL_RUN_STATUS.ToLower(),
                        new List<GenericCodeResult>
                        {
                            new GenericCodeResult { CodeValue = "Confirm" },
                            new GenericCodeResult { CodeValue = "Draft" },
                            new GenericCodeResult { CodeValue = "Final" },
                            new GenericCodeResult { CodeValue = "Lock" }
                        }
                    }
                });

            PayrollRunService service = new PayrollRunService(payrollRunRepository.Object, payrollItemRepository.Object, payrollProfileRepository.Object,
                formulasRepository.Object, genericCodeRepository.Object, employeeService.Object, webHostEnvironment.Object);

            var response = service.Update(payrollRunId, request, token).Result;

            //Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(0, response.Data);
            Assert.AreEqual(Label.PAY0408, response.Message.Toast.First().LabelCode);
        }

        [Test]
        public void UpdatePayrollRun_PayrollCycleNotExist_ReturnFail()
        {
            int payrollRunId = 1;
            string token = string.Empty;
            List<string> lstGenericCode = new List<string>
            {
                GenericCode.PAYROLL_RUN_STATUS
            };
            ApiRequest<PayrollRunStoreModel> request = new ApiRequest<PayrollRunStoreModel>();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 1,
                EntityId = 1
            };

            request.Data = new PayrollRunStoreModel()
            {
                Name = "Test",
                PayrollCycleId = 1,
                StatusCd = "Draft"
            };

            var payrollRunRepository = new Mock<IPayrollRunRepository>();
            var payrollItemRepository = new Mock<IMasterPayrollItemRepository>();
            var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            var formulasRepository = new Mock<IFormulasRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            var employeeService = new Mock<IEmployeeService>();
            var webHostEnvironment = new Mock<IWebHostEnvironment>();

            payrollRunRepository.Setup(x => x.GetById(payrollRunId))
                .ReturnsAsync(new PayrollRunViewModel());

            payrollRunRepository.Setup(x => x.CheckPayrollCycle(request.Data.PayrollCycleId))
                .ReturnsAsync(false);

            genericCodeRepository.Setup(x => x.GetGenericCodes(lstGenericCode, token, request.Parameter))
                    .ReturnsAsync(new Dictionary<string, List<GenericCodeResult>>
                {
                    {
                        GenericCode.PAYROLL_RUN_STATUS.ToLower(),
                        new List<GenericCodeResult>
                        {
                            new GenericCodeResult { CodeValue = "Confirm" },
                            new GenericCodeResult { CodeValue = "Draft" },
                            new GenericCodeResult { CodeValue = "Final" },
                            new GenericCodeResult { CodeValue = "Lock" }
                        }
                    }
                });

            PayrollRunService service = new PayrollRunService(payrollRunRepository.Object, payrollItemRepository.Object, payrollProfileRepository.Object,
                formulasRepository.Object, genericCodeRepository.Object, employeeService.Object, webHostEnvironment.Object);

            var response = service.Update(payrollRunId, request, token).Result;

            //Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(0, response.Data);
            Assert.AreEqual(Label.PAY0410, response.Message.Toast.First().LabelCode);
        }

        [Test]
        public void UpdatePayrollRun_UpdateFailt_ReturnFail()
        {
            int payrollRunId = 1;
            string token = string.Empty;
            List<string> lstGenericCode = new List<string>
            {
                GenericCode.PAYROLL_RUN_STATUS
            };
            ApiRequest<PayrollRunStoreModel> request = new ApiRequest<PayrollRunStoreModel>();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 1,
                EntityId = 1
            };

            request.Data = new PayrollRunStoreModel()
            {
                Name = "Test",
                PayrollCycleId = 1,
                StatusCd = "Draft"
            };

            var payrollRunRepository = new Mock<IPayrollRunRepository>();
            var payrollItemRepository = new Mock<IMasterPayrollItemRepository>();
            var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            var formulasRepository = new Mock<IFormulasRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            var employeeService = new Mock<IEmployeeService>();
            var webHostEnvironment = new Mock<IWebHostEnvironment>();

            payrollRunRepository.Setup(x => x.GetById(payrollRunId))
                .ReturnsAsync(new PayrollRunViewModel());

            payrollRunRepository.Setup(x => x.CheckPayrollCycle(request.Data.PayrollCycleId))
                .ReturnsAsync(true);

            payrollRunRepository.Setup(x => x.Update(payrollRunId, request.Data))
                .ReturnsAsync((int?)null);

            genericCodeRepository.Setup(x => x.GetGenericCodes(lstGenericCode, token, request.Parameter))
                    .ReturnsAsync(new Dictionary<string, List<GenericCodeResult>>
                {
                    {
                        GenericCode.PAYROLL_RUN_STATUS.ToLower(),
                        new List<GenericCodeResult>
                        {
                            new GenericCodeResult { CodeValue = "Confirm" },
                            new GenericCodeResult { CodeValue = "Draft" },
                            new GenericCodeResult { CodeValue = "Final" },
                            new GenericCodeResult { CodeValue = "Lock" }
                        }
                    }
                });

            PayrollRunService service = new PayrollRunService(payrollRunRepository.Object, payrollItemRepository.Object, payrollProfileRepository.Object,
                formulasRepository.Object, genericCodeRepository.Object, employeeService.Object, webHostEnvironment.Object);

            var response = service.Update(payrollRunId, request, token).Result;

            //Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(Label.PAY0404, response.Message.Toast.First().LabelCode);
        }

        [Test]
        public void UpdatePayrollRun_ReturnSuccess()
        {
            int payrollRunId = 1;
            string token = string.Empty;
            List<string> lstGenericCode = new List<string>
            {
                GenericCode.PAYROLL_RUN_STATUS
            };
            ApiRequest<PayrollRunStoreModel> request = new ApiRequest<PayrollRunStoreModel>();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 1,
                EntityId = 1
            };

            request.Data = new PayrollRunStoreModel()
            {
                Name = "Test",
                PayrollCycleId = 1,
                StatusCd = "Draft"
            };

            var payrollRunRepository = new Mock<IPayrollRunRepository>();
            var payrollItemRepository = new Mock<IMasterPayrollItemRepository>();
            var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            var formulasRepository = new Mock<IFormulasRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            var employeeService = new Mock<IEmployeeService>();
            var webHostEnvironment = new Mock<IWebHostEnvironment>();

            payrollRunRepository.Setup(x => x.GetById(payrollRunId))
                .ReturnsAsync(new PayrollRunViewModel());

            payrollRunRepository.Setup(x => x.CheckPayrollCycle(request.Data.PayrollCycleId))
                .ReturnsAsync(true);

            payrollRunRepository.Setup(x => x.Update(payrollRunId, request.Data))
                .ReturnsAsync(payrollRunId);

            genericCodeRepository.Setup(x => x.GetGenericCodes(lstGenericCode, token, request.Parameter))
                    .ReturnsAsync(new Dictionary<string, List<GenericCodeResult>>
                {
                    {
                        GenericCode.PAYROLL_RUN_STATUS.ToLower(),
                        new List<GenericCodeResult>
                        {
                            new GenericCodeResult { CodeValue = "Confirm" },
                            new GenericCodeResult { CodeValue = "Draft" },
                            new GenericCodeResult { CodeValue = "Final" },
                            new GenericCodeResult { CodeValue = "Lock" }
                        }
                    }
                });

            PayrollRunService service = new PayrollRunService(payrollRunRepository.Object, payrollItemRepository.Object, payrollProfileRepository.Object,
                formulasRepository.Object, genericCodeRepository.Object, employeeService.Object, webHostEnvironment.Object);

            var response = service.Update(payrollRunId, request, token).Result;

            //Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(payrollRunId, response.Data);
        }

        [Test]
        public void DuplicatePayrollRun_PayrollRunNotExist_ReturnFail()
        {
            int payrollRunId = 1;
            string token = string.Empty;
            ApiRequest<PayrollRunStoreModel> request = new ApiRequest<PayrollRunStoreModel>();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 1,
                EntityId = 1
            };

            request.Data = new PayrollRunStoreModel()
            {
                Name = "Test",
                PayrollCycleId = 1,
                StatusCd = "Draft"
            };

            var payrollRunRepository = new Mock<IPayrollRunRepository>();
            var payrollItemRepository = new Mock<IMasterPayrollItemRepository>();
            var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            var formulasRepository = new Mock<IFormulasRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            var employeeService = new Mock<IEmployeeService>();
            var webHostEnvironment = new Mock<IWebHostEnvironment>();

            payrollRunRepository.Setup(x => x.GetById(payrollRunId))
                .ReturnsAsync((PayrollRunViewModel)null);

            PayrollRunService service = new PayrollRunService(payrollRunRepository.Object, payrollItemRepository.Object, payrollProfileRepository.Object,
                formulasRepository.Object, genericCodeRepository.Object, employeeService.Object, webHostEnvironment.Object);

            var response = service.Duplicate(payrollRunId, request).Result;

            //Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(Label.PAY0403, response.Message.Toast.First().LabelCode);
        }

        [Test]
        public void DuplicatePayrollRun_ReturnSuccess()
        {
            int payrollRunId = 1;
            string token = string.Empty;
            ApiRequest<PayrollRunStoreModel> request = new ApiRequest<PayrollRunStoreModel>();
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 1,
                EntityId = 1
            };

            request.Data = new PayrollRunStoreModel()
            {
                Name = "Test",
                PayrollCycleId = 1,
                StatusCd = "Draft"
            };

            var payrollRunRepository = new Mock<IPayrollRunRepository>();
            var payrollItemRepository = new Mock<IMasterPayrollItemRepository>();
            var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            var formulasRepository = new Mock<IFormulasRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            var employeeService = new Mock<IEmployeeService>();
            var webHostEnvironment = new Mock<IWebHostEnvironment>();

            payrollRunRepository.Setup(x => x.GetById(payrollRunId))
                .ReturnsAsync(new PayrollRunViewModel());

            PayrollRunService service = new PayrollRunService(payrollRunRepository.Object, payrollItemRepository.Object, payrollProfileRepository.Object,
                formulasRepository.Object, genericCodeRepository.Object, employeeService.Object, webHostEnvironment.Object);

            var response = service.Duplicate(payrollRunId, request).Result;

            //Assert
            Assert.IsNotNull(response);
            Assert.IsTrue(response.Data);
            Assert.AreEqual(Label.PAY0409, response.Message.Toast.First().LabelCode);
        }

        [Test]
        public void GetPayrollRunSetting_ReturnSuccess()
        {
            int payrollRunId = 1;
            string token = string.Empty;
            ApiRequest request = new ApiRequest();
            List<string> lstGenericCode = new List<string>
            {
                GenericCode.PAYROLL_RUN_STATUS
            };
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 1,
                EntityId = 1
            };

            var payrollRunRepository = new Mock<IPayrollRunRepository>();
            var payrollItemRepository = new Mock<IMasterPayrollItemRepository>();
            var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            var formulasRepository = new Mock<IFormulasRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            var employeeService = new Mock<IEmployeeService>();
            var webHostEnvironment = new Mock<IWebHostEnvironment>();

            payrollRunRepository.Setup(x => x.GetById(payrollRunId))
                .ReturnsAsync(new PayrollRunViewModel());

            payrollRunRepository.Setup(x => x.GetPayrollRunEmployee(payrollRunId, request.Parameter.Locale))
                .ReturnsAsync(new List<PayrollRunEmployeeViewModel>() { new PayrollRunEmployeeViewModel() { EmployeeId = 1}, new PayrollRunEmployeeViewModel() { EmployeeId = 2} });

            payrollRunRepository.Setup(x => x.GetPayrollRunTransaction(payrollRunId, new List<int>() { 1,2}, request.Parameter.Locale))
                .ReturnsAsync(new List<PayrollRunTransactionViewModel>() { new PayrollRunTransactionViewModel(), new PayrollRunTransactionViewModel()});

            PayrollRunService service = new PayrollRunService(payrollRunRepository.Object, payrollItemRepository.Object, payrollProfileRepository.Object,
                formulasRepository.Object, genericCodeRepository.Object, employeeService.Object, webHostEnvironment.Object);

            genericCodeRepository.Setup(x => x.GetGenericCodes(lstGenericCode, token, request.Parameter))
                    .ReturnsAsync(new Dictionary<string, List<GenericCodeResult>>
                {
                    {
                        GenericCode.PAYROLL_RUN_STATUS.ToLower(),
                        new List<GenericCodeResult>
                        {
                            new GenericCodeResult { CodeValue = "Confirm" },
                            new GenericCodeResult { CodeValue = "Draft" },
                            new GenericCodeResult { CodeValue = "Final" },
                            new GenericCodeResult { CodeValue = "Lock" }
                        }
                    }
                });

            formulasRepository.Setup(x => x.GetFormulas(request.Parameter.ClientId, request.Parameter.EntityId))
                .ReturnsAsync(new List<FormulasResult>());

            var response = service.Setting(payrollRunId, request, token).Result;

            //Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Data.Detail);
            Assert.AreEqual(2, response.Data.Employee.Count);
            Assert.AreEqual(2, response.Data.Transaction.Count);
        }

        [Test]
        public void StoreEmployee_ReturnSuccess()
        {
            int payrollRunId = 1;
            string token = string.Empty;
            ApiRequest<PayrollRunEmployeeStoreModel> request = new ApiRequest<PayrollRunEmployeeStoreModel>()
            {
                Data = new PayrollRunEmployeeStoreModel()
                {
                    EmployeeIds = new List<int>() { 1, 2, 3 },
                    CreatedBy = "Admin",
                    RecurrentTransactionFlg = 0,
                    ScheduleTransactionFlg = 0
                }
            };
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 1,
                EntityId = 1
            };

            var payrollRunRepository = new Mock<IPayrollRunRepository>();
            var payrollItemRepository = new Mock<IMasterPayrollItemRepository>();
            var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            var formulasRepository = new Mock<IFormulasRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            var employeeService = new Mock<IEmployeeService>();
            var webHostEnvironment = new Mock<IWebHostEnvironment>();

            payrollRunRepository.Setup(x => x.GetPayrollRunEmployeeId(payrollRunId))
                .ReturnsAsync(new List<int>() { 2 });

            payrollRunRepository.Setup(x => x.DeletePayrollRunEmployee(payrollRunId, request.Data.CreatedBy, new List<int>() { 1}));

            payrollRunRepository.Setup(x => x.StorePayrollRunEmployee(payrollRunId, request.Data));

            PayrollRunService service = new PayrollRunService(payrollRunRepository.Object, payrollItemRepository.Object, payrollProfileRepository.Object,
                formulasRepository.Object, genericCodeRepository.Object, employeeService.Object, webHostEnvironment.Object);

            var response = service.EmployeeStore(payrollRunId, request).Result;

            //Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(Label.PAY0414, response.Message.Toast.First().LabelCode);
            Assert.AreEqual(CommonLib.Constants.Level.SUCCESS, response.Message.Toast.First().Level);
        }

        [Test]
        public void StoreTransaction_NotIntoPayrollProfile_ReturnFail()
        {
            int payrollRunId = 1;
            string token = string.Empty;
            ApiRequest<PayrollRunTransactionStoreModel> request = new ApiRequest<PayrollRunTransactionStoreModel>()
            {
                Data = new PayrollRunTransactionStoreModel()
                {
                    EmployeeIds = new List<int>() { 1,2,3},
                    PayrollItemId = 1,
                    CostCenterId = 1,
                    Currency = "USD"
                }
            };
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 1,
                EntityId = 1
            };

            var payrollRunRepository = new Mock<IPayrollRunRepository>();
            var payrollItemRepository = new Mock<IMasterPayrollItemRepository>();
            var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            var formulasRepository = new Mock<IFormulasRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            var employeeService = new Mock<IEmployeeService>();
            var webHostEnvironment = new Mock<IWebHostEnvironment>();

            payrollRunRepository.Setup(x => x.GetEmployeeNotLinkTransaction(request.Data, request.Parameter.Locale))
                .ReturnsAsync(new List<EmployeeNameErrorModel>() { new EmployeeNameErrorModel()});

            PayrollRunService service = new PayrollRunService(payrollRunRepository.Object, payrollItemRepository.Object, payrollProfileRepository.Object,
                formulasRepository.Object, genericCodeRepository.Object, employeeService.Object, webHostEnvironment.Object);

            var response = service.TransactionStore(payrollRunId, request).Result;

            //Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(Label.PAY0411, response.Message.Toast.First().LabelCode);
        }

        [Test]
        public void StoreTransaction_ReturnSuccess()
        {
            int payrollRunId = 1;
            string token = string.Empty;
            ApiRequest<PayrollRunTransactionStoreModel> request = new ApiRequest<PayrollRunTransactionStoreModel>()
            {
                Data = new PayrollRunTransactionStoreModel()
                {
                    EmployeeIds = new List<int>() { 1, 2, 3 },
                    PayrollItemId = 1,
                    CostCenterId = 1,
                    Currency = "USD"
                }
            };
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 1,
                EntityId = 1
            };

            var payrollRunRepository = new Mock<IPayrollRunRepository>();
            var payrollItemRepository = new Mock<IMasterPayrollItemRepository>();
            var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            var formulasRepository = new Mock<IFormulasRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            var employeeService = new Mock<IEmployeeService>();
            var webHostEnvironment = new Mock<IWebHostEnvironment>();

            payrollRunRepository.Setup(x => x.GetEmployeeNotLinkTransaction(request.Data, request.Parameter.Locale))
                .ReturnsAsync(new List<EmployeeNameErrorModel>());

            payrollRunRepository.Setup(x => x.StorePayrollRunTransaction(payrollRunId, request.Data));

            PayrollRunService service = new PayrollRunService(payrollRunRepository.Object, payrollItemRepository.Object, payrollProfileRepository.Object,
                formulasRepository.Object, genericCodeRepository.Object, employeeService.Object, webHostEnvironment.Object);

            var response = service.TransactionStore(payrollRunId, request).Result;

            //Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(Label.PAY0412, response.Message.Toast.First().LabelCode);
            Assert.AreEqual(CommonLib.Constants.Level.SUCCESS, response.Message.Toast.First().Level);
        }

        [Test]
        public void DeleteTransaction_ReturnSuccess()
        {
            string token = string.Empty;
            ApiRequest<PayrollRunTransactionDeleteModel> request = new ApiRequest<PayrollRunTransactionDeleteModel>()
            {
                Data = new PayrollRunTransactionDeleteModel()
                {
                    PayrollRunTransactionId = new List<int>() { 1,2},
                    ModifiedBy = "Admin"
                }
            };
            request.Parameter = new StdRequestParam()
            {
                Locale = "en",
                ClientId = 1,
                EntityId = 1
            };

            var payrollRunRepository = new Mock<IPayrollRunRepository>();
            var payrollItemRepository = new Mock<IMasterPayrollItemRepository>();
            var payrollProfileRepository = new Mock<IPayrollProfileRepository>();
            var formulasRepository = new Mock<IFormulasRepository>();
            var genericCodeRepository = new Mock<IGenericCodeRepository>();
            var employeeService = new Mock<IEmployeeService>();
            var webHostEnvironment = new Mock<IWebHostEnvironment>();

            payrollRunRepository.Setup(x => x.DeletePayrollRunTransaction(request.Data.ModifiedBy, request.Data.PayrollRunTransactionId));

            PayrollRunService service = new PayrollRunService(payrollRunRepository.Object, payrollItemRepository.Object, payrollProfileRepository.Object,
                formulasRepository.Object, genericCodeRepository.Object, employeeService.Object, webHostEnvironment.Object);

            var response = service.TransactionDelete(request).Result;

            //Assert
            Assert.AreEqual(Label.PAY0413, response.Message.Toast.First().LabelCode);
            Assert.AreEqual(CommonLib.Constants.Level.SUCCESS, response.Message.Toast.First().Level);
        }
    }
}