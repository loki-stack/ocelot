﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace TestUI.Utils
{
    public static class Utils
    {

        public static bool WaitCondition(this IWebDriver driver, By by, int timeout = 10)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout));
                _ = wait.Until(x => x.CheckExiest(by));
                return true;
            }
            catch (Exception)
            {
                Console.WriteLine("Element with locator: '" + by + "' was not found in current context page.");
                return false;
            }
        }
        public static bool WaitCondition(this IWebDriver driver, Func<IWebDriver, bool> condition, int timeout = 10)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout));
                _ = wait.Until(condition);
                return true;
            }
            catch (Exception)
            {
                Console.WriteLine("Element with locator: was not found in current context page.");
                return false;
            }
        }
        public static bool CheckExiest(this IWebDriver driver, By by)
        {
            return driver.FindElements(by).Count > 0;
        }
        public static bool IsDisplayed(this IWebDriver driver, By by, string elementName = "")
        {
            IWebElement element = driver.FindElement(by);
            bool result;
            try
            {
                result = element.Displayed;
                Console.WriteLine(elementName + " is Displayed.");
            }
            catch (Exception)
            {
                result = false;
                Console.WriteLine(elementName + " is not Displayed.");
            }

            return result;
        }
        public static bool IsDisabled(this IWebDriver driver, By by, string elementName = "")
        {
            IWebElement element = driver.FindElement(by);
            bool result;
            try
            {
                result = element.GetAttribute("disabled") == "true";
                Console.WriteLine(elementName + " is disabled.");
            }
            catch (Exception)
            {
                result = false;
                Console.WriteLine(elementName + " is not disabled.");
            }

            return result;
        }
        public static void Click(this IWebDriver driver, By by, string elementName = "")
        {
            IWebElement element = driver.FindElement(by);
            element.Click();
            Console.WriteLine("Clicked on " + elementName);
        }
        public static void EnterText(this IWebDriver driver, By by, string text, string elementName = "")
        {
            IWebElement element = driver.FindElement(by);
            element.Clear();
            element.SendKeys(text);
            element.SendKeys(Keys.Tab);

            Console.WriteLine(text + " entered in the " + elementName + " field.");
        }
        public static void SelectDropdown(this IWebDriver driver, By by, string value, string elementName = "")
        {
            IWebElement element = driver.FindElement(by);
            element.Click();
            driver.FindElement(By.CssSelector(".p-dropdown-panel li.p-dropdown-item[aria-label='" + value + "']")).Click();

            Console.WriteLine(value + " selected in the " + elementName + " field.");
        }


    }

}
