﻿using OpenQA.Selenium;
using System;
using System.Threading;
using Xunit;

namespace TestUI.Utils
{
    public class FormUtils
    {
        public string _formId { get; set; }
        public IWebDriver _driver { get; set; }

        public FormUtils(string formId, IWebDriver driver, int timeout = 10)
        {
            _formId = formId;
            _driver = driver;
            _driver.WaitCondition(By.Id(formId));
            _driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(10);
            _driver.Manage().Timeouts().AsynchronousJavaScript = TimeSpan.FromSeconds(10);
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }
        public void SubmitForm()
        {
            var submitFormElm = _driver.FindElement(By.CssSelector($"#{_formId} button[type='submit']"));
            Assert.True(submitFormElm.GetAttribute("disabled") != "true");
            submitFormElm.Click();
            // Sleep some second
            Thread.Sleep(100);
        }
        public void ResetForm()
        {
            _driver.FindElement(By.CssSelector($"#{_formId} button[type='reset']")).Click();
        }

        public void FillTextData(string controlKey, string value)
        {
            _driver.FindElement(By.CssSelector($"#{_formId}-{controlKey}")).SendKeys(value);
        }

        public void SelectDropDown(string controlKey, string value)
        {
            _driver.FindElement(By.CssSelector($"#{_formId}-{controlKey}")).Click();
            _driver.FindElement(By.CssSelector(".p-dropdown-panel li.p-dropdown-item[aria-label='" + value + "']")).Click();
        }
        public string GetError(string controlKey, string value)
        {
            return _driver.FindElement(By.CssSelector($"#{_formId}-{controlKey}-error")).Text;
        }
        public bool CheckRequired(string controlKey)
        {
            return _driver.FindElements(By.CssSelector($"label[for='{_formId}-{controlKey}']>.required")).Count > 0;
        }

    }
}
