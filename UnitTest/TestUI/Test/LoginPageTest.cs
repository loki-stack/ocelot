using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TestUI.Utils;
using Xunit;

namespace TestUI
{

    public class LoginPageTest : IDisposable
    {
        private readonly IWebDriver _driver;
        private const string URI = "http://172.20.20.21:3002/hris-bs/passport/login";
        public LoginPageTest()
        {
            _driver = new ChromeDriver();
            _driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(10);
            _driver.Manage().Timeouts().AsynchronousJavaScript = TimeSpan.FromSeconds(10);
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

        }


        [Theory()]
        [InlineData("admin", "password1")]
        public void Login_Success(string username, string password)
        {
            // Step 1 Login
            _driver.Navigate().GoToUrl(URI);
            _driver.WaitCondition(By.Id("login"));
            _driver.EnterText(By.Id("login-username"), username);
            _driver.EnterText(By.Id("login-password"), password);
            _driver.Click(By.CssSelector("button[type='submit']"));
            Assert.True(_driver.WaitCondition(x => x.Url.Contains("hris-bs/home") ));
           /* // Step 2 Change Tennant 
            _driver.Click(By.Id("select-tennant"));
            _driver.SelectDropdown(By.Id("modal-client"), "Entity A1");
            _driver.Click(By.CssSelector("button[type='submit']"));
            // GOTO Grade Level
            _driver.Navigate().GoToUrl("http://localhost:3000/hris-bs/CAM/CAM0601/search");
            Assert.True(_driver.WaitCondition(x => x.Title == "Grade Level"));*/


        }


        [Theory()]
        [InlineData("admin2", "password1")]
        [InlineData("admin3", "password1")]
        [InlineData("admin4", "password1")]
        [InlineData("admin5", "password1")]
        [InlineData("admin6", "password1")]
        [InlineData("admin7", "password1")]
        [InlineData("admin8", "password1")]
        [InlineData("admin9", "password1")]
        [InlineData("admin10", "password1")]
        public void Login_Error(string username, string password)
        {
            _driver.Navigate().GoToUrl(URI);
            _driver.WaitCondition(By.Id("login"));
            _driver.EnterText(By.Id("login-username"), username);
            _driver.EnterText(By.Id("login-password"), password);
            _driver.Click(By.CssSelector("button[type='submit']"));
            Assert.False(_driver.WaitCondition(x => x.Url.Contains("hris-bs/home")));
        }
        public void Dispose()
        {
            _driver.Quit();
            _driver.Dispose();
        }
    }
}
