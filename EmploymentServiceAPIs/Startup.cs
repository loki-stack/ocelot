using AutoMapper;
using CommonLib.Dtos;
using CommonLib.Exception;
using CommonLib.Models;
using CommonLib.Models.Core;
using CommonLib.Repositories.Impl;
using CommonLib.Repositories.Interfaces;
using CommonLib.Services;
using CommonLib.Validation;
using EmploymentServiceAPIs.Common.Mapping;
using EmploymentServiceAPIs.Common.Repositories.Impl;
using EmploymentServiceAPIs.Common.Repositories.Interfaces;
using EmploymentServiceAPIs.Common.Services.Impl;
using EmploymentServiceAPIs.Common.Services.Interfaces;
using EmploymentServiceAPIs.Employee.Repositories.Impl;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using EmploymentServiceAPIs.Employee.Services.Impl;
using EmploymentServiceAPIs.Employee.Services.Interfaces;
using EmploymentServiceAPIs.EmployeePortal.Repositories.Impl;
using EmploymentServiceAPIs.EmployeePortal.Repositories.Interfaces;
using EmploymentServiceAPIs.EmployeePortal.Services.Impl;
using EmploymentServiceAPIs.EmployeePortal.Services.Interfaces;
using EmploymentServiceAPIs.ManageEntity.Repositories.Impl;
using EmploymentServiceAPIs.ManageEntity.Repositories.Interfaces;
using EmploymentServiceAPIs.ManageEntity.Services.Impl;
using EmploymentServiceAPIs.ManageEntity.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Text;

namespace EmploymentServiceAPIs
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddHttpClient();
            var secret = Configuration.GetSection("AppSettings").GetSection("Secret").Value;
            var key = Encoding.ASCII.GetBytes(secret);

            var builder = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json");
            var config = builder.Build();

            services.Configure<KestrelServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });
            services.Configure<IISServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });

            var tokenValidationParameters = new TokenValidationParameters
            {
                ClockSkew = TimeSpan.Zero,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false,
                RequireExpirationTime = false,
                ValidateLifetime = true
            };

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = tokenValidationParameters;
            });

            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));
            services.Configure<SmtpSettings>(Configuration.GetSection("SmtpSettings"));
            services.Configure<MailSettings>(Configuration.GetSection("MailSettings"));
            services.AddSingleton(tokenValidationParameters);
            services.AddAutoMapper(typeof(AutoMapperProfile).Assembly);
            services.AddDbContext<CoreDbContext>(options
                => options.UseSqlServer(Configuration.GetConnectionString("CoreConnection"),
                 conf =>
                 {
                     conf.UseHierarchyId(); // enable hierarchyid support
                 })
            );

            //disable automatic model state validation
            services.AddSingleton<IObjectModelValidator, IgnoreAutoValidator>();
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddScoped<IGenericCodeRepository, GenericCodeRepository>();
            services.AddScoped<IGlobalCalendarRepository, GlobalCalendarRepository>();
            services.AddScoped<ICostCenterService, CostCenterService>();
            services.AddScoped<IJobGradeService, JobGradeService>();
            services.AddScoped<IEmployeeService, EmployeeService>();
            services.AddScoped<IDependentService, DependentService>();
            services.AddScoped<IBankAccountService, BankAccountService>();
            services.AddScoped<IEmergencyContactService, EmergencyContactService>();
            services.AddScoped<ITaxService, TaxService>();
            services.AddScoped<IRentalAddressService, RentalAddressService>();
            services.AddScoped<IFileService, FileService>();
            services.AddScoped<IEntitySettingRepository, EntitySettingRepository>();
            services.AddScoped<IEmploymentContractService, EmploymentContractService>();
            services.AddScoped<IMovementService, MovermentService>();
            services.AddScoped<IWorkingScheduleService, WorkingScheduleService>();
            services.AddScoped<IAttachmentService, AttachmentService>();
            services.AddScoped<IEmploymentStatusService, EmploymentStatusService>();
            services.AddScoped<ITerminationService, TerminationService>();
            services.AddScoped<IWorkCalendarService, WorkCalendarService>();

            services.AddScoped<IHttpRequestAnalyzer, HttpRequestAnalyzer>();
            services.AddScoped<IConnectionStringContainer, ConnectionStringContainer>();
            services.AddScoped<IJobGradeRepository, JobGradeRepository>();
            services.AddScoped<ICostCenterRepository, CostCenterRepository>();
            services.AddScoped<IDependentRepository, DependentRepository>();
            services.AddScoped<IMovementRepository, MovementRepository>();
            services.AddScoped<IEmploymentContractRepository, EmploymentContractRepository>();
            services.AddScoped<IBankAccountRepository, BankAccountRepository>();
            services.AddScoped<IEmergencyContactRepository, EmergencyContactRepository>();
            services.AddScoped<IEmployeeRepository, EmployeeRepository>();
            services.AddScoped<ITaxRepository, TaxRepository>();
            services.AddScoped<IRentalAddressRepository, RentalAddressRepository>();
            services.AddScoped<IFileRepository, FileRepository>();
            services.AddScoped<IWorkingSheduleRepository, WorkingScheduleRepository>();
            services.AddScoped<IAttachmentRepository, AttachmentRepository>();
            services.AddScoped<IEmploymentStatusRepository, EmploymentStatusRepository>();
            services.AddScoped<ITerminationRepository, TerminationRepository>();
            services.AddScoped<IPayslipPasswordRepository, PayslipPasswordRepository>();
            services.AddScoped<IHolidayRepository, HolidayRepository>();
            services.AddScoped<IWorkCalendarRepository, WorkCalendarRepository>();
            services.AddScoped<IEntityRepsitory, EntityRepository>();

            //client
            services.AddScoped<ITbCamEntityService, TbCamEntityService>();
            services.AddScoped<ITbCamEntityRepository, TbCamEntityRepository>();
            services.AddScoped<ITbCamClientService, TbCamClientService>();
            services.AddScoped<ITbCamClientRepository, TbCamClientRepository>();
            services.AddScoped<ITbCfgI18NRepository, TbCfgI18Nrepository>();
            services.AddScoped<ITbCamEntityFaqRepository, TbCamEntityFaqRepository>();
            services.AddScoped<ITbCamEntitySupportRepository, TbCamEntitySupportRepository>();

            // organization structure
            services.AddScoped<ITbSecDataAccessOrganizationStructureRepository, TbSecDataAccessOrganizationStructureRepository>();
            services.AddScoped<IOrganizationStructureRepository, OrganizationStructureRepository>();
            services.AddScoped<IOrganizationStructureService, OrganizationStructureService>();
            services.AddScoped<ITbCamEntityFileService, TbCamEntityFileService>();
            services.AddScoped<ITbCamEntityFileRepository, TbCamEntityFileRepository>();
            services.AddScoped<ITbSecDataAccessOrganizationStructureRepository, TbSecDataAccessOrganizationStructureRepository>();

            //validate
            services.AddScoped<IValidator, Validator>();

            // Empolyee Portal - Config
            services.AddScoped<IEntityConfigService, EntityConfigService>();
            services.AddScoped<IEmpDataService, EmpDataService>();
            services.AddScoped<ITaxReturnRepository, TaxReturnRepository>();
            services.AddScoped<IPaySlipRepository, PaySlipRepository>();
            services.AddScoped<IEmpPayslipPasswordService, EmpPayslipPasswordService>();

            //Message
            services.AddScoped<IMessageService, MessageService>();
            services.AddScoped<IMessageRepository, MessageRepository>();
            services.AddScoped<IMessageTemplateLangRepository, MessageTemplateLangRepository>();
            services.AddScoped<IMessageTemplateRepository, MessageTemplateRepository>();
            services.AddScoped<IEmpEmailService, EmpEmailService>();
            // services.AddSingleton<IMessageService, MessageService>();
            services.AddScoped<ISendEmailService, SendEmailService>();

            //disable automatic model state validation
            services.AddSingleton<IObjectModelValidator, IgnoreAutoValidator>();
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            services.AddSwaggerGen(config =>
            {
                config.SwaggerDoc("v1", new OpenApiInfo { Title = "Employment", Version = "v1" });
                config.CustomOperationIds(apiDesc =>
                {
                    string controllerName = apiDesc.ActionDescriptor.RouteValues["controller"];
                    return apiDesc.TryGetMethodInfo(out MethodInfo methodInfo) ? controllerName + "_" + methodInfo.Name : null;
                });
                config.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = @"JWT Authorization header using the Bearer scheme. \r\n\r\n
                      Enter 'Bearer' [space] and then your token in the text input below.
                      \r\n\r\nExample: 'Bearer 12345abcdef'",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                config.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header,
                        },
                        new List<string>()
                    }
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                config.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context => new GlobalExceptionHandler(loggerFactory).Handle(context));
            });

            app.UseSwagger();
            app.UseSwaggerUI(config =>
            {
                config.SwaggerEndpoint("/swagger/v1/swagger.json", "Employment v1");
                config.DisplayOperationId();
                config.RoutePrefix = string.Empty;
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}