﻿using AutoMapper;
using CommonLib.Models;
using CommonLib.Models.Client;
using CommonLib.Models.Core;
using CommonLib.Services;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Dtos.EmergencyContact;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Repositories.Impl
{
    public class EmergencyContactRepository : IEmergencyContactRepository
    {
        protected readonly IConnectionStringContainer _connectionStringRepository;
        protected string _connectionString;
        protected readonly CoreDbContext _coreDbContext;
        protected readonly ILogger<EmergencyContactRepository> _logger;
        protected readonly IMapper _mapper;

        public EmergencyContactRepository(IConnectionStringContainer connectionStringRepository,
                                          CoreDbContext coreDBContext,
                                          ILogger<EmergencyContactRepository> logger,
                                          IMapper mapper)
        {
            _connectionStringRepository = connectionStringRepository;
            _coreDbContext = coreDBContext;
            _logger = logger;
            _mapper = mapper;
            _connectionString = _connectionStringRepository.GetConnectionString();
        }

        protected TenantDBContext CreateContext(string _connectionString)
        {
            return new TenantDBContext(_connectionString);
        }

        public bool CheckExistEmployeeById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                return _context.TbEmpEmergencyContactPerson.Any(x => x.EmergencyContactPersonId == id);
            }
        }

        public async Task<bool> Create(int employeeId, List<EmergencyContactRequest> emergencyContacts, string userName)
        {
            try
            {
                using (var _context = CreateContext(_connectionString))
                {
                    List<TbEmpEmergencyContactPerson> listModel = new List<TbEmpEmergencyContactPerson>();
                    foreach (var item in emergencyContacts)
                    {
                        TbEmpEmergencyContactPerson model = new TbEmpEmergencyContactPerson
                        {
                            EmployeeId = employeeId,
                            RelationshipToEmployee = item.RelationshipToEmployee,
                            RelationshipOther = item.RelationshipOther,
                            Name = item.Name,
                            MobilePhone = item.MobilePhone,
                            CreatedBy = userName,
                            CreatedDt = DateTime.UtcNow
                        };
                        listModel.Add(model);
                    }
                    await _context.TbEmpEmergencyContactPerson.AddRangeAsync(listModel);
                    await _context.SaveChangesAsync();
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return false;
            }
        }

        public async Task<List<TbEmpEmergencyContactPerson>> GetByEmployeeId(int employeeId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var emergencyContacts = await _context.TbEmpEmergencyContactPerson.Where(x => x.EmployeeId == employeeId).ToListAsync();
                return emergencyContacts;
            }
        }

        public async Task<TbEmpEmergencyContactPerson> GetById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var emergencyContact = await _context.TbEmpEmergencyContactPerson.FirstOrDefaultAsync(x => x.EmergencyContactPersonId == id);
                return emergencyContact;
            }
        }

        public async Task<bool> Update(List<EmergencyContactRequest> emergencyContacts, string modifiedBy)
        {
            try
            {
                using (var _context = CreateContext(_connectionString))
                {
                    List<TbEmpEmergencyContactPerson> listModel = new List<TbEmpEmergencyContactPerson>();
                    foreach (var item in emergencyContacts)
                    {
                        TbEmpEmergencyContactPerson model = await _context.TbEmpEmergencyContactPerson.Where(x => x.EmergencyContactPersonId == item.EmergencyContactPersonId).FirstOrDefaultAsync();
                        if (model != null)
                        {
                            model.RelationshipToEmployee = item.RelationshipToEmployee;
                            model.RelationshipOther = item.RelationshipOther;
                            model.Name = item.Name;
                            model.MobilePhone = item.MobilePhone;
                            model.ModifiedBy = modifiedBy;
                            model.ModifiedDt = DateTime.UtcNow;
                            _context.Entry(model).State = EntityState.Modified;
                            listModel.Add(model);
                        }
                    }
                    _context.TbEmpEmergencyContactPerson.UpdateRange(listModel);
                    await _context.SaveChangesAsync();
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return false;
            }
        }
    }
}