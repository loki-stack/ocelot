﻿using CommonLib.Models;
using CommonLib.Models.Client;
using CommonLib.Models.Core;
using CommonLib.Services;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Repositories.Impl
{
    public class WorkingScheduleRepository : IWorkingSheduleRepository
    {
        protected readonly IConnectionStringContainer _connectionStringRepository;
        protected string _connectionString;
        protected readonly CoreDbContext _coreDbContext;

        public WorkingScheduleRepository(IConnectionStringContainer connectionStringRepository, CoreDbContext coreDBContext)
        {
            _connectionStringRepository = connectionStringRepository;
            _coreDbContext = coreDBContext;
            _connectionString = _connectionStringRepository.GetConnectionString();
        }

        protected TenantDBContext CreateContext(string _connectionString)
        {
            return new TenantDBContext(_connectionString);
        }

        public async Task<List<TbCfgWorkingSchedule>> GetAll()
        {
            using (var _context = CreateContext(_connectionString))
            {
                return await _context.TbCfgWorkingSchedule.ToListAsync();
            }
        }
    }
}