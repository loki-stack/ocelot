using CommonLib.Models.Client;
using CommonLib.Services;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Repositories.Impl
{
    public class PayslipPasswordRepository : IPayslipPasswordRepository
    {

        protected readonly IConnectionStringContainer _connectionStringRepository;
        protected string _connectionString;

        public PayslipPasswordRepository(IConnectionStringContainer connectionStringRepository)
        {
            _connectionStringRepository = connectionStringRepository;
            _connectionString = _connectionStringRepository.GetConnectionString();
        }
        protected TenantDBContext CreateContext(string _connectionString)
        {
            return new TenantDBContext(_connectionString);
        }

        public async Task<bool> Create(TbEmpPayslipPassword payslipPassword)
        {
            using (var _context = CreateContext(_connectionString))
            {
                await _context.TbEmpPayslipPassword.AddAsync(payslipPassword);
                await _context.SaveChangesAsync();
                return true;
            }
        }

        public async Task<List<TbEmpPayslipPassword>> GetByEmployeeId(int employeeId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var result = await _context.TbEmpPayslipPassword.Where(x => x.EmployeeId == employeeId).ToListAsync();
                return result;
            }
        }

        public async Task<TbEmpPayslipPassword> GetById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var result = await _context.TbEmpPayslipPassword
                .Where(x => x.PayslipPasswordId == id).FirstOrDefaultAsync();
                return result;
            }
        }

        public async Task<bool> Update(TbEmpPayslipPassword payslipPassword)
        {
            using (var _context = CreateContext(_connectionString))
            {
                _context.TbEmpPayslipPassword.Update(payslipPassword);
                await _context.SaveChangesAsync();
                return true;
            }
        }

        /// <summary>
        /// Return the lastest (current) password of the employee 
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public async Task<TbEmpPayslipPassword> GetLatestByEmployeeId(int employeeId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var result = await _context.TbEmpPayslipPassword
                   .Where(x => x.EmployeeId == employeeId)
                   .OrderByDescending(x => x.CreatedDt)
                   .FirstOrDefaultAsync();
                return result;
            }
        }
    }
}