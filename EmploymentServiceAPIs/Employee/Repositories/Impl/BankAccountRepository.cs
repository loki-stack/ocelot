﻿using CommonLib.Models;
using CommonLib.Models.Client;
using CommonLib.Models.Core;
using CommonLib.Services;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Repositories.Impl
{
    public class BankAccountRepository : IBankAccountRepository
    {
        protected readonly IConnectionStringContainer _connectionStringRepository;
        protected string _connectionString;
        protected readonly CoreDbContext _coreDbContext;

        public BankAccountRepository(IConnectionStringContainer connectionStringRepository, CoreDbContext coreDBContext, ILogger<BankAccountRepository> logger)
        {
            _connectionStringRepository = connectionStringRepository;
            _coreDbContext = coreDBContext;
            _connectionString = _connectionStringRepository.GetConnectionString();
        }

        protected ClientBasicDBContext CreateContext(string _connectionString)
        {
            return new ClientBasicDBContext(_connectionString);
        }

        public async Task<bool> Create(List<TbEmpEeBankAccount> bankAccounts)
        {
            using (var _context = CreateContext(_connectionString))
            {
                await _context.TbEmpEeBankAccount.AddRangeAsync(bankAccounts);
                await _context.SaveChangesAsync();
                return true;
            }
        }

        public async Task<bool> Update(List<TbEmpEeBankAccount> bankAccounts)
        {
            using (var _context = CreateContext(_connectionString))
            {
                _context.TbEmpEeBankAccount.UpdateRange(bankAccounts);
                await _context.SaveChangesAsync();
                return true;
            }
        }

        public async Task<TbEmpEeBankAccount> GetById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var bankAccount = await _context.TbEmpEeBankAccount.FirstOrDefaultAsync(x => x.Id == id);
                return bankAccount;
            }
        }

        public bool CheckExistBankAccountById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                return _context.TbEmpEeBankAccount.Any(x => x.Id == id);
            }
        }

        public async Task<List<TbEmpEeBankAccount>> GetByEmployeeId(int employeeId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var bankAccounts = await _context.TbEmpEeBankAccount.Where(x => x.EmployeeId == employeeId).ToListAsync();
                return bankAccounts;
            }
        }
    }
}