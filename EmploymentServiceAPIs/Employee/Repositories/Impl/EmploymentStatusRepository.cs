﻿using CommonLib.Models;
using CommonLib.Models.Client;
using CommonLib.Models.Core;
using CommonLib.Services;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Constants;
using EmploymentServiceAPIs.Employee.Dtos.EmploymentStatus;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Repositories.Impl
{
    public class EmploymentStatusRepository : IEmploymentStatusRepository
    {
        protected readonly IConnectionStringContainer _connectionStringRepository;
        protected string _connectionString;
        protected readonly CoreDbContext _coreDbContext;

        public EmploymentStatusRepository(IConnectionStringContainer connectionStringRepository, CoreDbContext coreDBContext)
        {
            _connectionStringRepository = connectionStringRepository;
            _coreDbContext = coreDBContext;
            _connectionString = _connectionStringRepository.GetConnectionString();
        }

        protected TenantDBContext CreateContext(string _connectionString)
        {
            return new TenantDBContext(_connectionString);
        }

        public bool CheckExistEmploymentStatusById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                return _context.TbEmpEmploymentStatus.Any(x => x.EmploymentStatusId == id);
            }
        }

        public async Task<bool> CreateMany(List<TbEmpEmploymentStatus> lstEmploymentStatus)
        {
            using (var _context = CreateContext(_connectionString))
            {
                await _context.TbEmpEmploymentStatus.AddRangeAsync(lstEmploymentStatus);
                await _context.SaveChangesAsync();
                return true;
            }
        }

        public async Task<bool> Delete(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var employmentStatus = await _context.TbEmpEmploymentStatus.FirstOrDefaultAsync(x => x.EmploymentStatusId == id);
                _context.TbEmpEmploymentStatus.Remove(employmentStatus);
                await _context.SaveChangesAsync();
                return true;
            }
        }

        public async Task<List<EmploymentStatusViewModel>> GetByEmployeeId(int employeeId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var employmentStatus = await (from ec in _context.TbEmpEmploymentContract
                                              join es in _context.TbEmpEmploymentStatus on ec.EmploymentContractId equals es.EmploymentContractId
                                              where ec.EmployeeId == employeeId
                                              select new EmploymentStatusViewModel
                                              {
                                                  EmploymentStatusId = es.EmploymentStatusId,
                                                  EmploymentContractId = ec.EmploymentContractId,
                                                  EmploymentContractName = ec.ContractNo,
                                                  Status = es.Status,
                                                  Remark = es.Remark,
                                                  Confirmed = es.Confirmed,
                                                  StartDate = es.StartDate,
                                                  CreatedBy = es.CreatedBy,
                                                  CreatedDt = es.CreatedDt,
                                                  ModifiedBy = es.ModifiedBy,
                                                  ModifiedDt = es.ModifiedDt
                                              }).ToListAsync();
                return employmentStatus;
            }
        }

        public async Task<TbEmpEmploymentStatus> GetById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var employmentStatus = await _context.TbEmpEmploymentStatus.FirstOrDefaultAsync(x => x.EmploymentStatusId == id);
                return employmentStatus;
            }
        }

        public async Task<bool> UpdateMany(List<TbEmpEmploymentStatus> lstEmploymentStatus)
        {
            using (var _context = CreateContext(_connectionString))
            {
                _context.TbEmpEmploymentStatus.UpdateRange(lstEmploymentStatus);
                await _context.SaveChangesAsync();
                return true;
            }
        }

        public async Task<List<TbEmpEmploymentStatus>> GetListEmpStatusByStartDateNew(int employeeId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var result = await (from ec in _context.TbEmpEmploymentContract
                                    join es0 in
                                    (
                                       from es1 in _context.TbEmpEmploymentStatus
                                       join es2 in
                                       (
                                           from es3 in _context.TbEmpEmploymentStatus
                                           group es3 by es3.EmploymentContractId into temp
                                           select new { EmploymentContractId = temp.Key, StartDate = temp.Max(x => x.StartDate) }
                                       ) on new { es1.EmploymentContractId, es1.StartDate } equals new { es2.EmploymentContractId, es2.StartDate }
                                       select es1
                                    ) on ec.EmploymentContractId equals es0.EmploymentContractId
                                    where ec.EmployeeId == employeeId
                                    select es0).ToListAsync();

                return result;
            }
        }

        public bool CheckActiveStatusLevel1(int empContractId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var empStatus = (from es1 in _context.TbEmpEmploymentStatus
                                 join es2 in (
                                       from es3 in _context.TbEmpEmploymentStatus
                                       group es3 by es3.EmploymentContractId into temp
                                       select new { EmploymentContractId = temp.Key, StartDate = temp.Max(x => x.StartDate) })
                                 on new { es1.EmploymentContractId, es1.StartDate } equals new { es2.EmploymentContractId, es2.StartDate }
                                 where es1.EmploymentContractId == empContractId
                                 select es1).FirstOrDefault();
                if (empStatus != null)
                {
                    string status = empStatus.Status;
                    if (status == EmploymentStatus.ACTIVE || status == EmploymentStatus.KNOWN_LEAVING
                        || status == EmploymentStatus.SUSPENDED || status == EmploymentStatus.UNDER_PROBATION)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        public async Task<List<EmploymentStatusResult>> GetStatusConfirmation(string locale)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var localeParam = new SqlParameter("@locale", string.IsNullOrEmpty(locale) ? (object)DBNull.Value : locale);

                var result = await _context.Set<EmploymentStatusResult>().FromSqlRaw("EXECUTE SP_GET_STATUS_CONFIRMATION @locale", localeParam).ToListAsync();
                return result;
            }
        }

        public bool CheckExistStartDate(int empContractId, DateTime startDate)
        {
            using (var _context = CreateContext(_connectionString))
            {
                return _context.TbEmpEmploymentStatus.Any(x => x.StartDate == startDate && x.EmploymentContractId == empContractId);
            }
        }

        public async Task<bool> Update(TbEmpEmploymentStatus empStatus)
        {
            using (var _context = CreateContext(_connectionString))
            {
                _context.TbEmpEmploymentStatus.Update(empStatus);
                await _context.SaveChangesAsync();
                return true;
            }
        }
    }
}