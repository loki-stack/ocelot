﻿using CommonLib.Models;
using CommonLib.Models.Client;
using CommonLib.Models.Core;
using CommonLib.Services;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Repositories.Impl
{
    public class EmploymentContractRepository : IEmploymentContractRepository
    {
        protected readonly IConnectionStringContainer _connectionStringRepository;
        protected string _connectionString;
        protected readonly CoreDbContext _coreDbContext;
        protected readonly ILogger<EmploymentContractRepository> _logger;

        public EmploymentContractRepository(IConnectionStringContainer connectionStringRepository,
                                            CoreDbContext coreDBContext,
                                            ILogger<EmploymentContractRepository> logger)
        {
            _connectionStringRepository = connectionStringRepository;
            _coreDbContext = coreDBContext;
            _logger = logger;
            _connectionString = _connectionStringRepository.GetConnectionString();
        }

        protected TenantDBContext CreateContext(string _connectionString)
        {
            return new TenantDBContext(_connectionString);
        }

        public bool CheckExistEContractById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                return _context.TbEmpEmploymentContract.Any(x => x.EmploymentContractId == id);
            }
        }

        public async Task<List<TbEmpEmploymentContract>> CreateMany(List<TbEmpEmploymentContract> employmentContracts)
        {
            using (var _context = CreateContext(_connectionString))
            {
                await _context.TbEmpEmploymentContract.AddRangeAsync(employmentContracts);
                await _context.SaveChangesAsync();
                return employmentContracts;
            }
        }

        public async Task<TbEmpEmploymentContract> GetById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var employmentContract = await _context.TbEmpEmploymentContract
                                                .Include(x => x.TbEmpMovement)
                                                .FirstOrDefaultAsync(x => x.EmploymentContractId == id);
                return employmentContract;
            }
        }

        public async Task<bool> Update(List<TbEmpEmploymentContract> employmentContracts)
        {
            using (var _context = CreateContext(_connectionString))
            {
                _context.TbEmpEmploymentContract.UpdateRange(employmentContracts);
                await _context.SaveChangesAsync();
                return true;
            }
        }

        public bool CheckExistEmployeeNo(int clientId, int entityId, int employeeId, string employeeNo)
        {
            bool isExist = false;
            var entities = _coreDbContext.TbCamEntity.Where(x => x.ParentClientId == clientId).ToList();

            foreach (var entity in entities)
            {
                if (!string.IsNullOrEmpty(entity.ConnStringParam))
                {
                    using (var _context = CreateContext(entity.ConnStringParam))
                    {
                        isExist = _context.TbEmpEmploymentContract
                                               .Any(x => x.EmployeeNo == employeeNo
                                                    && (
                                                         (entity.EntityId == entityId && x.EmployeeId != employeeId)
                                                         || (entity.EntityId != entityId)
                                                       )
                                                   );
                        if (isExist)
                            break;
                    }
                }
            }
            return isExist;
        }

        public bool CheckExistGlobalNo(int employeeId, string globalNo)
        {
            using (var _context = CreateContext(_connectionString))
            {
                return _context.TbEmpEmploymentContract.Any(x => x.GlobalNo == globalNo && x.EmployeeId != employeeId);
            }
        }

        public bool CheckExistContractNo(int employmentContractId, string contractNo)
        {
            using (var _context = CreateContext(_connectionString))
            {
                return _context.TbEmpEmploymentContract.Any(x => (employmentContractId == 0 || x.EmploymentContractId != employmentContractId) && x.ContractNo == contractNo);
            }
        }

        public async Task<List<TbEmpEmploymentContract>> GetByEmployeeId(int employeeId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var employmentContracts = await _context.TbEmpEmploymentContract
                                                .Include(x => x.TbEmpTermination)
                                                .Include(x => x.TbEmpMovement)
                                                .Where(x => x.EmployeeId == employeeId).ToListAsync();
                return employmentContracts;
            }
        }

        public async Task<TbEmpEmployee> GetDataEmployee(int employeeId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var employee = await _context.TbEmpEmployee
                                                .Include(x => x.TbEmpEmploymentContract).ThenInclude(x => x.TbEmpMovement)
                                                .FirstOrDefaultAsync(x => x.EmployeeId == employeeId);
                return employee;
            }
        }

        public async Task<TbEmpEmploymentContract> Create(TbEmpEmploymentContract employmentContract)
        {
            using (var _context = CreateContext(_connectionString))
            {
                try
                {
                    await _context.AddAsync(employmentContract);
                    await _context.SaveChangesAsync();
                    return employmentContract;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    return null;
                }
            }
        }
    }
}