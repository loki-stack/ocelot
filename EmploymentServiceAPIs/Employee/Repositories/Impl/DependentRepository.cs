﻿using CommonLib.Models;
using CommonLib.Models.Client;
using CommonLib.Models.Core;
using CommonLib.Services;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Repositories.Impl
{
    public class DependentRepository : IDependentRepository
    {
        protected readonly IConnectionStringContainer _connectionStringRepository;
        protected string _connectionString;
        protected readonly CoreDbContext _coreDbContext;

        public DependentRepository(IConnectionStringContainer connectionStringRepository, CoreDbContext coreDBContext)
        {
            _connectionStringRepository = connectionStringRepository;
            _coreDbContext = coreDBContext;
            _connectionString = _connectionStringRepository.GetConnectionString();
        }

        protected TenantDBContext CreateContext(string _connectionString)
        {
            return new TenantDBContext(_connectionString);
        }

        public bool CheckExistDependentById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                return _context.TbEmpDependent.Any(x => x.DependentId == id);
            }
        }

        public async Task<bool> Create(List<TbEmpDependent> dependents)
        {
            using (var _context = CreateContext(_connectionString))
            {
                await _context.TbEmpDependent.AddRangeAsync(dependents);
                await _context.SaveChangesAsync();
                return true;
            }
        }

        public async Task<TbEmpDependent> GetById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var dependent = await _context.TbEmpDependent.FirstOrDefaultAsync(x => x.DependentId == id);
                return dependent;
            }
        }

        public async Task<List<TbEmpDependent>> GetByEmployeeId(int employeeId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var dependents = await _context.TbEmpDependent
                                               .Include(x => x.Employee)
                                               .Where(x => x.EmployeeId == employeeId).ToListAsync();
                return dependents;
            }
        }

        public async Task<bool> Update(List<TbEmpDependent> dependents)
        {
            using (var _context = CreateContext(_connectionString))
            {
                _context.TbEmpDependent.UpdateRange(dependents);
                await _context.SaveChangesAsync();
                return true;
            }
        }
    }
}