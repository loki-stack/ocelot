﻿using AutoMapper;
using CommonLib.Dtos;
using CommonLib.Models;
using Client = CommonLib.Models.Client;
using CommonLib.Services;
using EmploymentServiceAPIs.Employee.Dtos.Employee;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CommonLib.Models.Client;
using CommonLib.Models.Core;

namespace EmploymentServiceAPIs.Employee.Repositories.Impl
{
    public class EmployeeRepository : IEmployeeRepository
    {
        protected readonly IConnectionStringContainer _connectionStringRepository;
        protected string _connectionString;
        protected readonly CoreDbContext _coreDbContext;
        protected readonly ILogger<EmployeeRepository> _logger;
        protected readonly IMapper _mapper;

        public EmployeeRepository(IConnectionStringContainer connectionStringRepository,
                                  CoreDbContext coreDBContext,
                                  ILogger<EmployeeRepository> logger,
                                  IMapper mapper)
        {
            _connectionStringRepository = connectionStringRepository;
            _coreDbContext = coreDBContext;
            _connectionString = _connectionStringRepository.GetConnectionString();
            _logger = logger;
            _mapper = mapper;
        }

        protected ClientBasicDBContext CreateContext(string _connectionString)
        {
            return new ClientBasicDBContext(_connectionString);
        }

        public async Task<TbEmpEmployee> Create(EmployeeRequest request, string userName)
        {
            using (var _context = CreateContext(_connectionString))
            {
                TbEmpEmployee employee = _mapper.Map<EmployeeRequest, TbEmpEmployee>(request);
                employee.CreatedBy = userName;
                employee.CreatedDt = DateTime.UtcNow;
                await _context.TbEmpEmployee.AddAsync(employee);
                await _context.SaveChangesAsync();
                return employee;
            }
        }

        public async Task<TbEmpEmployee> Update(int employeeId, EmployeeRequest request, string userName)
        {
            using (var _context = CreateContext(_connectionString))
            {
                TbEmpEmployee employee = await _context.TbEmpEmployee.Where(x => x.EmployeeId == employeeId).FirstOrDefaultAsync();
                employee.FirstName = request.FirstNamePrimary;
                employee.FirstNameSecondary = request.FirstNameSecondary;
                employee.MiddleName = request.MiddleNamePrimary;
                employee.MiddleNameSecondary = request.MiddleNameSecondary;
                employee.LastName = request.LastNamePrimary;
                employee.LastNameSecondary = request.LastNameSecondary;
                employee.ChristianName = request.ChristianNamePrimary;
                employee.ChristianNameSecondary = request.ChristianNameSecondary;
                employee.DisplayName = request.DisplayNamePrimary;
                employee.DisplayNameSecondary = request.DisplayNameSecondary;
                employee.NamePrefix = request.NamePrefix;
                employee.DateOfBirth = request.DateOfBirth;
                employee.Gender = request.Gender;
                employee.Photo = request.Photo;
                employee.Age = request.Age;
                employee.MartialStatus = request.MartialStatus;
                employee.MarriageDate = request.MarriageDate;
                employee.PreferredLanguage = request.PreferredLanguage;
                employee.CitizenIdNo = request.CitizenIdNo;
                employee.Nationality = request.Nationality;
                employee.PassportIdNo = request.PassportIdNo;
                employee.CountryOfIssue = request.CountryOfIssue;
                employee.WorkVisaHolder = request.WorkVisaHolder;
                employee.WorkArrivingDate = request.WorkArrivingDate;
                employee.WorkVisaExpiryDate = request.WorkVisaExpiryDate;
                employee.WorkVisaIssueDate = request.WorkVisaIssueDate;
                employee.WorkVisaLandingDate = request.WorkVisaLandingDate;
                employee.HomePhone = request.HomePhone;
                employee.OfficePhone = request.OfficePhone;
                employee.MobilePhone = request.MobilePhone;
                employee.WorkEmail = request.WorkEmail;
                employee.PersonalEmail = request.PersonalEmail;
                employee.ResidentialAddress1 = request.ResidentialAddress1Primary;
                employee.ResidentialAddress1Secondary = request.ResidentialAddress1Secondary;
                employee.ResidentialAddress2 = request.ResidentialAddress2Primary;
                employee.ResidentialAddress2Secondary = request.ResidentialAddress2Secondary;
                employee.ResidentialAddress3 = request.ResidentialAddress3Primary;
                employee.ResidentialAddress3Secondary = request.ResidentialAddress3Secondary;
                employee.ResidentialCity = request.ResidentialCityPrimary;
                employee.ResidentialCitySecondary = request.ResidentialCitySecondary;
                employee.ResidentialDistrict = request.ResidentialDistrictPrimary;
                employee.ResidentialDistrictSecondary = request.ResidentialDistrictSecondary;
                employee.ResidentialState = request.ResidentialStatePrimary;
                employee.ResidentialStateSecondary = request.ResidentialStateSecondary;
                employee.ResidentialCountry = request.ResidentialCountry;
                employee.ResidentialPostalCode = request.ResidentialPostalCode;
                employee.ResidentialHongkongTaxForm = request.ResidentialHongkongTaxForm;
                employee.PostalAddress1 = request.PostalAddress1Primary;
                employee.PostalAddress1Secondary = request.PostalAddress1Secondary;
                employee.PostalAddress2 = request.PostalAddress2Primary;
                employee.PostalAddress2Secondary = request.PostalAddress2Secondary;
                employee.PostalAddress3 = request.PostalAddress3Primary;
                employee.PostalAddress3Secondary = request.PostalAddress3Secondary;
                employee.PostalCity = request.PostalCityPrimary;
                employee.PostalCitySecondary = request.PostalCitySecondary;
                employee.PostalDistrict = request.PostalDistrictPrimary;
                employee.PostalDistrictSecondary = request.PostalDistrictSecondary;
                employee.PostalState = request.PostalStatePrimary;
                employee.PostalStateSecondary = request.PostalStateSecondary;
                employee.PostalCountry = request.PostalCountry;
                employee.PostalPostalCode = request.PostalPostalCode;
                employee.PostalHongkongTaxForm = request.PostalHongkongTaxForm;
                employee.Remarks = request.Remarks;
                employee.RemarkContact = request.RemarkContact;
                employee.ModifiedBy = userName;
                employee.ModifiedDt = DateTime.UtcNow;
                _context.Entry(employee).State = EntityState.Modified;
                _context.TbEmpEmployee.Update(employee);
                await _context.SaveChangesAsync();
                return employee;
            }
        }

        public async Task<EmployeeViewModel> GetBasicInfo(int employeeId, string primaryLanguage, string secondaryLanguage)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var emp = await _context.TbEmpEmployee.FirstOrDefaultAsync(x => x.EmployeeId == employeeId);
                EmployeeViewModel viewModel = _mapper.Map<TbEmpEmployee, EmployeeViewModel>(emp);
                return viewModel;
            }
        }

        public bool CheckExistEmployeeById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                return _context.TbEmpEmployee.Any(x => x.EmployeeId == id);
            }
        }

        public async Task<List<EmployeeViewList>> GetAll(string languageTag)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var result = await (from e in _context.TbEmpEmployee
                                    join ec in _context.TbEmpEmploymentContract on e.EmployeeId equals ec.EmployeeId into ecs
                                    from ecResult in ecs.DefaultIfEmpty()
                                    join m in _context.TbEmpMovement on ecResult.EmploymentContractId equals m.EmploymentContractId into ms
                                    from mResult in ms.DefaultIfEmpty()
                                    join u in _context.TbCamUnit on mResult.BusinessUnitId equals u.UnitId into us
                                    from usResult in us.DefaultIfEmpty()
                                    join ppe in _context.TbPayPayrollProfileEmployee on e.EmployeeId equals ppe.EmployeeId into ppes
                                    from ppesResult in ppes.DefaultIfEmpty()
                                    join es in _context.TbEmpEmploymentStatus on ecResult.EmploymentContractId equals es.EmploymentContractId into ess
                                    from essResult in ess.DefaultIfEmpty()
                                    join et in _context.TbEmpTermination on ecResult.EmploymentContractId equals et.EmploymentContractId into ets
                                    from etsResult in ets.DefaultIfEmpty()
                                    orderby e.EmployeeId
                                    select new
                                    {
                                        e,
                                        usResult.FullName,
                                        ecResult.EmployeeNo,
                                        mResult.BusinessUnitId,
                                        mResult.JobGrade,
                                        mResult.InternalJobTitle,
                                        mResult.AssignmentStartDate,
                                        essResult.Status,
                                        mResult.RemunerationPackage,
                                        essResult.StartDate,
                                        etsResult.TerminationContractEndDate
                                    }).ToListAsync();
                Dictionary<int, EmployeeViewList> dicEmployee = new Dictionary<int, EmployeeViewList>();
                foreach (var item in result)
                {
                    int employeeId = item.e.EmployeeId;
                    if (!dicEmployee.ContainsKey(employeeId))
                    {
                        EmployeeViewList emp = new EmployeeViewList();
                        emp.EmployeeId = employeeId;
                        emp.StaffId = item.EmployeeNo;
                        if (!string.IsNullOrEmpty(item.e.DisplayName))
                            emp.Name = item.e.DisplayName;
                        else
                        {
                            string firstName = item.e.FirstName;
                            string middleName = item.e.MiddleName;
                            string lastName = item.e.LastName;
                            emp.Name = firstName + (string.IsNullOrEmpty(middleName) ? " " : " " + middleName + " ") + lastName;
                        }
                        var employee = result.Where(x => x.e.EmployeeId == employeeId && x.StartDate != null && x.StartDate <= DateTime.Now).OrderByDescending(x => x.StartDate).FirstOrDefault();
                        if (employee == null)
                        {
                            employee = result.Where(x => x.e.EmployeeId == employeeId).OrderBy(x => x.StartDate).FirstOrDefault();
                        }
                        if (employee != null)
                        {
                            emp.Status = employee.Status;
                            emp.AssignmentStartDate = employee.StartDate;
                        }
                        emp.Role = item.InternalJobTitle;
                        emp.Division = item.BusinessUnitId;
                        emp.DivisionTxt = item.FullName;
                        emp.Grade = item.JobGrade;
                        emp.RemunerationPackage = item.RemunerationPackage;
                        emp.MobilePhone = item.e.MobilePhone;
                        emp.WorkEmail = item.e.WorkEmail;
                        emp.Photo = item.e.Photo;
                        emp.ModifiedDate = item.e.ModifiedDt;
                        emp.TerminationDate = item.TerminationContractEndDate;
                        dicEmployee.Add(employeeId, emp);
                    }
                }
                return dicEmployee.Values.ToList();
            }
        }

        public async Task<TbEmpEmployee> UpdateAfterCreateI18N(TbEmpEmployee employee)
        {
            try
            {
                using (var _context = CreateContext(_connectionString))
                {
                    TbEmpEmployee model = await _context.TbEmpEmployee.Where(x => x.EmployeeId == employee.EmployeeId).FirstOrDefaultAsync();
                    model.FirstName = employee.FirstName;
                    model.LastName = employee.LastName;
                    model.MiddleName = employee.MiddleName;
                    model.DisplayName = employee.DisplayName;
                    model.ChristianName = employee.ChristianName;
                    model.ResidentialAddress1 = employee.ResidentialAddress1;
                    model.ResidentialAddress2 = employee.ResidentialAddress2;
                    model.ResidentialAddress3 = employee.ResidentialAddress3;
                    model.ResidentialDistrict = employee.ResidentialDistrict;
                    model.ResidentialCity = employee.ResidentialCity;
                    model.ResidentialState = employee.ResidentialState;
                    model.PostalAddress1 = employee.PostalAddress1;
                    model.PostalAddress2 = employee.PostalAddress2;
                    model.PostalAddress3 = employee.PostalAddress3;
                    model.PostalDistrict = employee.PostalDistrict;
                    model.PostalCity = employee.PostalCity;
                    model.PostalState = employee.PostalState;
                    _context.Entry(model).State = EntityState.Modified;
                    _context.Update(model);
                    await _context.SaveChangesAsync();
                    return model;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<TbEmpEmployee> Edit(TbEmpEmployee employee)
        {
            using (var _context = CreateContext(_connectionString))
            {
                _context.TbEmpEmployee.Update(employee);
                await _context.SaveChangesAsync();
                return employee;
            }
        }

        public async Task<TbEmpEmployee> GetSingleByCondition(Expression<Func<TbEmpEmployee, bool>> expression, string[] includes = null)
        {
            using (var _context = CreateContext(_connectionString))
            {
                if (includes != null && includes.Count() > 0)
                {
                    var query = _context.TbEmpEmployee.Include(includes.First());
                    foreach (var include in includes.Skip(1))
                        query = query.Include(include);
                    return query.FirstOrDefault(expression);
                }
                return await _context.TbEmpEmployee.FirstOrDefaultAsync(expression);
            }
        }

        public async Task<TbEmpEmployee> GetById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                return await _context.TbEmpEmployee.Where(x => x.EmployeeId == id).FirstOrDefaultAsync();
            }
        }

        public async Task<EmployeeProfile> GetEmployeeProfile(int employeeId, string languageTag)
        {
            using (var _context = CreateContext(_connectionString))
            {
                EmployeeProfile profile = new EmployeeProfile();
                var result = await (from e in _context.TbEmpEmployee
                                    join ec in _context.TbEmpEmploymentContract on e.EmployeeId equals ec.EmployeeId into ecs
                                    from ecResult in ecs.DefaultIfEmpty()
                                    join m in _context.TbEmpMovement on ecResult.EmploymentContractId equals m.EmploymentContractId into ms
                                    from mResult in ms.DefaultIfEmpty()
                                    join u in _context.TbCamUnit on mResult.BusinessUnitId equals u.UnitId into us
                                    from usResult in us.DefaultIfEmpty()
                                    join em in _context.TbEmpEmployee on mResult.DirectManager equals em.EmployeeId into ems
                                    from emsResult in ems.DefaultIfEmpty()
                                    where e.EmployeeId == employeeId
                                    select new
                                    {
                                        e,
                                        usResult.FullName,
                                        ecResult.EmployeeNo,
                                        mResult.JobGrade,
                                        mResult.InternalJobTitle,
                                        mResult.DirectManager,
                                        emsResult
                                    }).FirstOrDefaultAsync();
                if (result != null)
                {
                    profile.Division = result.FullName;
                    profile.StaffId = result.EmployeeNo;
                    profile.Role = result.InternalJobTitle;
                    profile.Email = result.e.WorkEmail;
                    profile.MobileNo = result.e.MobilePhone;
                    profile.Photo = result.e.Photo;

                    //employee name
                    if (!string.IsNullOrEmpty(result.e.DisplayName))
                    {
                        profile.Name = result.e.DisplayName;
                    }
                    else
                    {
                        string firstName = result.e.FirstName;
                        string lastName = result.e.LastName;
                        string middleName = result.e.MiddleName;
                        profile.Name = firstName + (string.IsNullOrEmpty(middleName) ? " " : " " + middleName + " ") + lastName;
                    }

                    //report to
                    if (result.emsResult != null)
                    {
                        if (!string.IsNullOrEmpty(result.emsResult.DisplayName))
                        {
                            profile.ReportTo = result.emsResult.DisplayName;
                        }
                        else
                        {
                            string firstName = result.emsResult.FirstName;
                            string middleName = result.emsResult.MiddleName;
                            string lastName = result.emsResult.LastName;
                            profile.Name = firstName + (string.IsNullOrEmpty(middleName) ? " " : " " + middleName + " ") + lastName;
                        }
                    }
                }
                return profile;
            }
        }

        public async Task<CompanyInformation> GetCompanyInfo(int employeeId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                CompanyInformation companyInfo = new CompanyInformation();
                var result = await (from e in _context.TbEmpEmployee
                                    join ec in _context.TbEmpEmploymentContract on e.EmployeeId equals ec.EmployeeId into ecs
                                    from ecResult in ecs.DefaultIfEmpty()
                                    join m in _context.TbEmpMovement on ecResult.EmploymentContractId equals m.EmploymentContractId into ms
                                    from mResult in ms.DefaultIfEmpty()
                                    join u in _context.TbCamUnit on mResult.BusinessUnitId equals u.UnitId into us
                                    from usResult in us.DefaultIfEmpty()
                                    join em in _context.TbEmpEmployee on mResult.DirectManager equals em.EmployeeId into ems
                                    from emsResult in ems.DefaultIfEmpty()
                                    where e.EmployeeId == employeeId
                                    select new
                                    {
                                        e,
                                        usResult.FullName,
                                        usResult.DisplayName,
                                        usResult.ParentId,
                                        mResult.SpecificEntityId,
                                    }).FirstOrDefaultAsync();
                if (result != null)
                {
                    TbCamEntity entity = await _coreDbContext.TbCamEntity.Where(x => x.EntityId == result.SpecificEntityId).FirstOrDefaultAsync();
                    if (entity != null)
                    {
                        companyInfo.CompanyCode = entity.EntityCode;
                        companyInfo.CompanyOfficialName = entity.EntityNameTxt;
                        companyInfo.CompanyDisplayName = entity.EntityDisplayName;
                        companyInfo.Country = entity.CountryCd;
                        TbCamClient client = await _coreDbContext.TbCamClient.Where(x => x.ClientId == entity.ParentClientId).FirstOrDefaultAsync();
                        if (client != null)
                        {
                            companyInfo.ClientId = client.ClientId;
                            companyInfo.ClientName = client.ClientNameTxt;
                        }
                    }
                    companyInfo.FunctionalGroupFullName = result.FullName;
                    companyInfo.FunctionalGroupDisplayName = result.DisplayName;
                    companyInfo.ParentId = result.ParentId;
                    companyInfo.RemarkCompany = result.e.RemarkCompany;
                }
                return companyInfo;
            }
        }

        public async Task<bool> UpdateCompanyInfo(int employeeId, CompanyInformation companyInfo, string userName)
        {
            using (var _context = CreateContext(_connectionString))
            {
                try
                {
                    TbEmpEmployee employee = await _context.TbEmpEmployee.Where(x => x.EmployeeId == employeeId).FirstOrDefaultAsync();
                    employee.RemarkCompany = companyInfo.RemarkCompany;
                    _context.Entry(employee).State = EntityState.Modified;
                    _context.Update(employee);
                    await _context.SaveChangesAsync();
                    return true;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    return false;
                }
            }
        }

        public async Task<List<TbEmpPayslip>> GetAllPayslips(int employeeId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var result = await _context.TbEmpPayslip.Where(x => x.EmployeeId == employeeId).ToListAsync();
                return result;
            }
        }

        public async Task<List<TbEmpTaxReturn>> GetAllTaxReturn(int employeeId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var result = await _context.TbEmpTaxReturn.Where(x => x.EmployeeId == employeeId).ToListAsync();
                return result;
            }
        }

        public async Task<bool> CreateCoreUser(TbEmpEmployee employee, ApiRequest<NewHireRequest> request)
        {
            try
            {
                TbSecUser user = new TbSecUser
                {
                    EmployeeId = employee.EmployeeId,
                    EntityId = request.Parameter.EntityId,
                    ClientId = request.Parameter.ClientId,
                    EssAccessStartDt = request.Data.ExpectedStartDate,
                    EssAccessEndDt = request.Data.ExpectedProbationEndDate,
                    EssAccessFlag = true,
                    CreatedBy = employee.CreatedBy,
                    CreatedDt = DateTime.UtcNow,
                    FullName = employee.DisplayName,
                    Password = string.Empty,
                    UserName = employee.DisplayName
                };
                await _coreDbContext.TbSecUser.AddAsync(user);
                await _coreDbContext.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return false;
            }
        }
    }
}