﻿using CommonLib.Models;
using CommonLib.Models.Client;
using CommonLib.Models.Core;
using CommonLib.Services;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Repositories.Impl
{
    public class TerminationRepository : ITerminationRepository
    {
        protected readonly IConnectionStringContainer _connectionStringRepository;
        protected string _connectionString;
        protected readonly CoreDbContext _coreDbContext;

        public TerminationRepository(IConnectionStringContainer connectionStringRepository, CoreDbContext coreDBContext)
        {
            _connectionStringRepository = connectionStringRepository;
            _coreDbContext = coreDBContext;
            _connectionString = _connectionStringRepository.GetConnectionString();
        }

        protected TenantDBContext CreateContext(string _connectionString)
        {
            return new TenantDBContext(_connectionString);
        }

        public bool CheckExistTermination(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                return _context.TbEmpTermination.Any(x => x.Id == id);
            }
        }

        public async Task<int?> Create(TbEmpTermination termination)
        {
            using (var _context = CreateContext(_connectionString))
            {
                await _context.TbEmpTermination.AddAsync(termination);
                await _context.SaveChangesAsync();
                return termination.Id;
            }
        }

        public async Task<TbEmpTermination> GetById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var termination = await _context.TbEmpTermination.FirstOrDefaultAsync(x => x.Id == id);
                return termination;
            }
        }

        public async Task<TbEmpTermination> GetByEmpContractId(int empContractId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var termination = await _context.TbEmpTermination.FirstOrDefaultAsync(x => x.EmploymentContractId == empContractId);
                return termination;
            }
        }

        public async Task<int?> Update(TbEmpTermination termination)
        {
            using (var _context = CreateContext(_connectionString))
            {
                _context.TbEmpTermination.Update(termination);
                await _context.SaveChangesAsync();
                return termination.Id;
            }
        }

        public bool CheckExistTerminationOfContract(int empContractId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                return _context.TbEmpTermination.Any(x => x.EmploymentContractId == empContractId);
            }
        }

        public async Task<bool> UpdateMany(List<TbEmpTermination> terminations)
        {
            using (var _context = CreateContext(_connectionString))
            {
                _context.TbEmpTermination.UpdateRange(terminations);
                await _context.SaveChangesAsync();
                return true;
            }
        }

        public async Task<List<TbEmpTermination>> GetByEmployeeId(int employeeId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var result = await (from ec in _context.TbEmpEmploymentContract
                                    join t in _context.TbEmpTermination on ec.EmploymentContractId equals t.EmploymentContractId
                                    where ec.EmployeeId == employeeId
                                    select t).ToListAsync();
                return result;
            }
        }
    }
}