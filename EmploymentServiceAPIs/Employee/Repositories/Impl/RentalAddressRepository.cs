﻿using CommonLib.Models;
using CommonLib.Models.Client;
using CommonLib.Models.Core;
using CommonLib.Services;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Dtos.RentalAddress;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Repositories.Impl
{
    public class RentalAddressRepository : IRentalAddressRepository
    {
        protected readonly IConnectionStringContainer _connectionStringRepository;
        protected string _connectionString;
        protected readonly CoreDbContext _coreDbContext;
        protected readonly ILogger<RentalAddressRepository> _logger;

        public RentalAddressRepository(IConnectionStringContainer connectionStringRepository, CoreDbContext coreDBContext, ILogger<RentalAddressRepository> logger)
        {
            _connectionStringRepository = connectionStringRepository;
            _coreDbContext = coreDBContext;
            _logger = logger;
            _connectionString = _connectionStringRepository.GetConnectionString();
        }

        protected TenantDBContext CreateContext(string _connectionString)
        {
            return new TenantDBContext(_connectionString);
        }

        public bool CheckExistRentalAddressById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                return _context.TbEmpRentalAddress.Any(x => x.AddressId == id);
            }
        }

        public async Task<bool> Create(int employeeId, List<RentalAddressRequest> request, string userName)
        {
            using (var _context = CreateContext(_connectionString))
            {
                try
                {
                    List<TbEmpRentalAddress> rentalAddresses = new List<TbEmpRentalAddress>();
                    foreach (var item in request)
                    {
                        rentalAddresses.Add(new TbEmpRentalAddress
                        {
                            EmployeeId = employeeId,
                            Nature = item.Nature,
                            Address = item.Address,
                            StartDate = item.StartDate,
                            EndDate = item.EndDate,
                            RentPaidToLandlordByEmployer = item.RentPaidToLandlordByEmployer,
                            RentPaidToLandlordByEmployee = item.RentPaidToLandlordByEmployee,
                            RentRefundedToEmployeeByEmployer = item.RentRefundedToEmployeeByEmployer,
                            RentPaidToEmployerByEmployee = item.RentPaidToEmployerByEmployee,
                            CreatedBy = userName,
                            CreatedDt = DateTime.UtcNow
                        });
                    }
                    await _context.TbEmpRentalAddress.AddRangeAsync(rentalAddresses);
                    await _context.SaveChangesAsync();
                    return true;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    return false;
                }
            }
        }

        public async Task<List<TbEmpRentalAddress>> GetByEmployeeId(int employeeId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var rentalAddresses = await _context.TbEmpRentalAddress.Where(x => x.EmployeeId == employeeId).ToListAsync();
                return rentalAddresses;
            }
        }

        public async Task<TbEmpRentalAddress> GetById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var rentalAddress = await _context.TbEmpRentalAddress.FirstOrDefaultAsync(x => x.AddressId == id);
                return rentalAddress;
            }
        }

        public async Task<bool> Update(List<RentalAddressRequest> request, string userName)
        {
            using (var _context = CreateContext(_connectionString))
            {
                try
                {
                    List<TbEmpRentalAddress> rentalAddresses = new List<TbEmpRentalAddress>();
                    foreach (var item in request)
                    {
                        TbEmpRentalAddress model = await _context.TbEmpRentalAddress.Where(x => x.AddressId == item.AddressId).FirstOrDefaultAsync();
                        if (model != null)
                        {
                            model.Nature = item.Nature;
                            model.Address = item.Address;
                            model.StartDate = item.StartDate;
                            model.EndDate = item.EndDate;
                            model.RentPaidToLandlordByEmployer = item.RentPaidToLandlordByEmployer;
                            model.RentPaidToLandlordByEmployee = item.RentPaidToLandlordByEmployee;
                            model.RentRefundedToEmployeeByEmployer = item.RentRefundedToEmployeeByEmployer;
                            model.RentPaidToEmployerByEmployee = item.RentPaidToEmployerByEmployee;
                            model.ModifiedBy = userName;
                            model.ModifiedDt = DateTime.UtcNow;
                            _context.Entry(model).State = EntityState.Modified;
                            rentalAddresses.Add(model);
                        }
                    }
                    _context.TbEmpRentalAddress.UpdateRange(rentalAddresses);
                    await _context.SaveChangesAsync();
                    return true;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    return false;
                }
            }
        }
    }
}