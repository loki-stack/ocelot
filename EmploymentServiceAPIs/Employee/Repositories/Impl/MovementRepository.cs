﻿using CommonLib.Models;
using CommonLib.Models.Client;
using CommonLib.Models.Core;
using CommonLib.Services;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Repositories.Impl
{
    public class MovementRepository : IMovementRepository
    {
        protected readonly IConnectionStringContainer _connectionStringRepository;
        protected string _connectionString;
        protected readonly CoreDbContext _coreDbContext;
        protected readonly ILogger<MovementRepository> _logger;

        public MovementRepository(IConnectionStringContainer connectionStringRepository, CoreDbContext coreDBContext, ILogger<MovementRepository> logger)
        {
            _connectionStringRepository = connectionStringRepository;
            _coreDbContext = coreDBContext;
            _logger = logger;
            _connectionString = _connectionStringRepository.GetConnectionString();
        }

        protected TenantDBContext CreateContext(string _connectionString)
        {
            return new TenantDBContext(_connectionString);
        }

        public bool CheckExistMovementById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                return _context.TbEmpMovement.Any(x => x.AssignmentId == id);
            }
        }

        public async Task<bool> CreateMany(List<TbEmpMovement> movements)
        {
            using (var _context = CreateContext(_connectionString))
            {
                await _context.TbEmpMovement.AddRangeAsync(movements);
                await _context.SaveChangesAsync();
                return true;
            }
        }

        public async Task<List<TbEmpMovement>> GetByEmploymentId(int employmentId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var movements = await _context.TbEmpMovement.Where(x => x.EmploymentContractId == employmentId).ToListAsync();
                return movements;
            }
        }

        public async Task<TbEmpMovement> GetById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var movement = await _context.TbEmpMovement.FirstOrDefaultAsync(x => x.AssignmentId == id);
                return movement;
            }
        }

        public async Task<bool> UpdateMany(List<TbEmpMovement> movements)
        {
            using (var _context = CreateContext(_connectionString))
            {
                _context.TbEmpMovement.UpdateRange(movements);
                await _context.SaveChangesAsync();
                return true;
            }
        }

        public async Task<List<TbEmpMovement>> GetByEmployeeId(int employeeId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var result = await (from ec in _context.TbEmpEmploymentContract
                                    join m in _context.TbEmpMovement on ec.EmploymentContractId equals m.EmploymentContractId
                                    where ec.EmployeeId == employeeId
                                    select m).ToListAsync();
                return result;
            }
        }

        public async Task<TbEmpMovement> Create(TbEmpMovement movement)
        {
            using (var _context = CreateContext(_connectionString))
            {
                try
                {
                    await _context.AddAsync(movement);
                    await _context.SaveChangesAsync();
                    return movement;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    return null;
                }
            }
        }

        public async Task<bool> Update(TbEmpMovement movement)
        {
            using (var _context = CreateContext(_connectionString))
            {
                try
                {
                    _context.Entry(movement).State = EntityState.Modified;
                    _context.Update(movement);
                    await _context.SaveChangesAsync();
                    return true;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    return false;
                }
            }
        }
    }
}