﻿using CommonLib.Models;
using CommonLib.Models.Client;
using CommonLib.Models.Core;
using CommonLib.Services;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Repositories.Impl
{
    public class FileRepository : IFileRepository
    {
        protected readonly IConnectionStringContainer _connectionStringRepository;
        protected string _connectionString;
        protected readonly CoreDbContext _coreDbContext;
        private readonly ILogger<FileRepository> _logger;

        public FileRepository(IConnectionStringContainer connectionStringRepository, CoreDbContext coreDBContext, ILogger<FileRepository> logger)
        {
            _connectionStringRepository = connectionStringRepository;
            _coreDbContext = coreDBContext;
            _connectionString = _connectionStringRepository.GetConnectionString();
            _logger = logger;
        }

        protected TenantDBContext CreateContext(string _connectionString)
        {
            return new TenantDBContext(_connectionString);
        }

        public bool CheckExistFileById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                return _context.TbComFile.Any(x => x.FileId == id);
            }
        }

        public async Task<List<TbComFile>> Create(List<TbComFile> files)
        {
            using (var _context = CreateContext(_connectionString))
            {
                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    try
                    {
                        await _context.TbComFile.AddRangeAsync(files);
                        await _context.SaveChangesAsync();
                        await transaction.CommitAsync();
                        return files;
                    }
                    catch (Exception ex)
                    {
                        await transaction.RollbackAsync();
                        _logger.LogWarning(ex.ToString());
                        throw ex;
                    }
                }
            }
        }

        public async Task<TbComFile> GetById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var file = await _context.TbComFile.FirstOrDefaultAsync(x => x.FileId == id);
                return file;
            }
        }

        public async Task<TbComFile> Update(TbComFile file)
        {
            using (var _context = CreateContext(_connectionString))
            {
                _context.TbComFile.Update(file);
                await _context.SaveChangesAsync();
                return file;
            }
        }

        public async Task<List<TbComFile>> GetListFileByIds(List<int> ids)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var files = await _context.TbComFile.Where(x => ids.Contains(x.FileId)).ToListAsync();
                return files;
            }
        }
    }
}