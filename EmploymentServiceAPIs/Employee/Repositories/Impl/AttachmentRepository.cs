﻿using CommonLib.Models;
using CommonLib.Models.Client;
using CommonLib.Models.Core;
using CommonLib.Services;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Dtos.Attachment;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Repositories.Impl
{
    public class AttachmentRepository : IAttachmentRepository
    {
        protected readonly IConnectionStringContainer _connectionStringRepository;
        protected string _connectionString;
        protected readonly CoreDbContext _coreDbContext;
        protected readonly ILogger<AttachmentRepository> _logger;

        public AttachmentRepository(IConnectionStringContainer connectionStringRepository, CoreDbContext coreDBContext, ILogger<AttachmentRepository> logger)
        {
            _connectionStringRepository = connectionStringRepository;
            _coreDbContext = coreDBContext;
            _logger = logger;
            _connectionString = _connectionStringRepository.GetConnectionString();
        }

        protected TenantDBContext CreateContext(string _connectionString)
        {
            return new TenantDBContext(_connectionString);
        }

        public async Task<List<TbEmpAttachment>> GetByEmployeeId(int employeeId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                return await _context.TbEmpAttachment.Where(x => x.EmployeeId == employeeId).ToListAsync();
            }
        }

        public async Task Create(int employeeId, List<AttachmentRequest> request, string userName)
        {
            using (var _context = CreateContext(_connectionString))
            {
                List<TbEmpAttachment> attachments = new List<TbEmpAttachment>();
                foreach (var item in request)
                {
                    attachments.Add(new TbEmpAttachment
                    {
                        EmployeeId = employeeId,
                        AttachmentFileId = item.AttachmentFileId,
                        AttachmentFileName = item.AttachmentFileName,
                        AttachmentFileRemark = item.AttachmentFileRemark,
                        CreatedBy = userName,
                        CreatedDt = DateTime.UtcNow
                    });
                }
                await _context.AddRangeAsync(attachments);
                await _context.SaveChangesAsync();
            }
        }

        public async Task Update(List<AttachmentRequest> request, string userName)
        {
            using (var _context = CreateContext(_connectionString))
            {
                List<TbEmpAttachment> attachments = new List<TbEmpAttachment>();
                foreach (var item in request)
                {
                    TbEmpAttachment attachment = await _context.TbEmpAttachment.Where(x => x.AttachmentId == item.AttachmentId).FirstOrDefaultAsync();
                    if (attachment != null)
                    {
                        attachment.AttachmentFileId = item.AttachmentFileId;
                        attachment.AttachmentFileName = item.AttachmentFileName;
                        attachment.AttachmentFileRemark = item.AttachmentFileRemark;
                        attachment.ModifiedBy = userName;
                        attachment.ModifiedDt = DateTime.UtcNow;
                    };
                    _context.Entry(attachment).State = EntityState.Modified;
                    attachments.Add(attachment);
                }
                _context.UpdateRange(attachments);
                await _context.SaveChangesAsync();
            }
        }

        public async Task Delete(List<AttachmentRequest> request)
        {
            using (var _context = CreateContext(_connectionString))
            {
                List<int> attachmentIds = request.Select(x => x.AttachmentId).ToList();
                List<TbEmpAttachment> attachments = _context.TbEmpAttachment.Where(x => attachmentIds.Contains(x.AttachmentId)).ToList();
                _context.RemoveRange(attachments);
                await _context.SaveChangesAsync();
            }
        }
    }
}
