﻿using AutoMapper;
using CommonLib.Models;
using CommonLib.Models.Client;
using CommonLib.Models.Core;
using CommonLib.Services;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Dtos.TaxArrangement;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Repositories.Impl
{
    public class TaxRepository : ITaxRepository
    {
        protected readonly IConnectionStringContainer _connectionStringRepository;
        protected string _connectionString;
        protected readonly CoreDbContext _coreDbContext;
        protected readonly IMapper _mapper;
        protected readonly ILogger<TaxRepository> _logger;

        public TaxRepository(IConnectionStringContainer connectionStringRepository, CoreDbContext coreDBContext, IMapper mapper)
        {
            _connectionStringRepository = connectionStringRepository;
            _coreDbContext = coreDBContext;
            _connectionString = _connectionStringRepository.GetConnectionString();
            _mapper = mapper;
        }

        protected TenantDBContext CreateContext(string _connectionString)
        {
            return new TenantDBContext(_connectionString);
        }

        public bool CheckExists(int employmentContractId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                return _context.TbEmpTax.Any(x => x.EmploymentContractId == employmentContractId);
            }
        }

        public async Task<bool> CreateMany(List<TaxRequest> request, string userName)
        {
            using (var _context = CreateContext(_connectionString))
            {
                try
                {
                    List<TbEmpTax> taxes = new List<TbEmpTax>();
                    foreach (var item in request)
                    {
                        TbEmpTax tax = _mapper.Map<TaxRequest, TbEmpTax>(item);
                        tax.CreatedBy = userName;
                        tax.CreatedDt = DateTime.UtcNow;
                        taxes.Add(tax);
                    }
                    await _context.TbEmpTax.AddRangeAsync(taxes);
                    await _context.SaveChangesAsync();
                    return true;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    return false;
                }
            }
        }

        public async Task<TbEmpTax> GetByEmploymentContract(int employmentContractId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var tax = await _context.TbEmpTax.FirstOrDefaultAsync(x => x.EmploymentContractId == employmentContractId);
                return tax;
            }
        }

        public async Task<TbEmpTax> GetById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var tax = await _context.TbEmpTax.FirstOrDefaultAsync(x => x.TaxId == id);
                return tax;
            }
        }

        public async Task<bool> Update(List<TaxRequest> request, string userName)
        {
            using (var _context = CreateContext(_connectionString))
            {
                try
                {
                    List<TbEmpTax> taxes = new List<TbEmpTax>();
                    foreach (var item in request)
                    {
                        TbEmpTax tax = _context.TbEmpTax.Where(x => x.TaxId == item.TaxId).FirstOrDefault();
                        tax.Ir56bRemarks = item.Ir56bRemarks;
                        tax.Ir56eMonthlyRateOfAllownace = item.Ir56eMonthlyRateOfAllownace;
                        tax.Ir56eFlutuatingEmoluments = item.Ir56eFlutuatingEmoluments;
                        tax.Ir56ePreviousEmployerName = item.Ir56ePreviousEmployerName;
                        tax.Ir56ePreviousEmployerAddress = item.Ir56ePreviousEmployerAddress;
                        tax.Ir56eShareOptionGranted = item.Ir56eShareOptionGranted;
                        tax.Ir56fNewEmployerName = item.Ir56fNewEmployerName;
                        tax.Ir56fNewEmployerAddress = item.Ir56fNewEmployerAddress;
                        tax.Ir56fAddressAfterTermination = item.Ir56fAddressAfterTermination;
                        tax.Ir56gDepartureDate = item.Ir56gDepartureDate;
                        tax.Ir56gReasonForDeparture = item.Ir56gReasonForDeparture;
                        tax.Ir56gReasonForDepartureOthers = item.Ir56gReasonForDepartureOthers;
                        tax.Ir56gEmployeeReturningHk = item.Ir56gEmployeeReturningHk;
                        tax.Ir56gEmployeeReturningHkDate = item.Ir56gEmployeeReturningHkDate;
                        tax.Ir56gAddressAfterTermination = item.Ir56gAddressAfterTermination;
                        tax.Ir56gEmployerHasHoldMoney = item.Ir56gEmployerHasHoldMoney;
                        tax.Ir56gHeldAmount = item.Ir56gHeldAmount;
                        tax.Ir56gReasonForNotHoldingMoney = item.Ir56gReasonForNotHoldingMoney;
                        tax.Ir56gTaxBorneByEmployer = item.Ir56gTaxBorneByEmployer;
                        tax.Ir56gShareOptionGranted = item.Ir56gShareOptionGranted;
                        tax.Ir56gGrantedDate = item.Ir56gGrantedDate;
                        tax.Ir56gNoOfSharesNotYetExercised = item.Ir56gNoOfSharesNotYetExercised;
                        tax.ModifiedBy = userName;
                        tax.ModifiedDt = DateTime.UtcNow;
                        taxes.Add(tax);
                    }
                    _context.TbEmpTax.UpdateRange(taxes);
                    await _context.SaveChangesAsync();
                    return true;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    return false;
                }
            }
        }

        public async Task<List<TbEmpTax>> GetByEmployeeId(int employeeId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                List<TbEmpTax> listTax = await (from e in _context.TbEmpEmployee
                                                join ec in _context.TbEmpEmploymentContract on e.EmployeeId equals ec.EmployeeId into ecs
                                                from ecResult in ecs.DefaultIfEmpty()
                                                join t in _context.TbEmpTax on ecResult.EmploymentContractId equals t.EmploymentContractId
                                                where e.EmployeeId == employeeId
                                                select t
                                    ).ToListAsync();
                return listTax;
            }
        }

        public async Task<TbEmpTax> Create(TbEmpTax tax)
        {
            using (var _context = CreateContext(_connectionString))
            {
                await _context.TbEmpTax.AddAsync(tax);
                await _context.SaveChangesAsync();
                return tax;
            }
        }
    }
}