﻿using CommonLib.Models.Client;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Dtos.EmploymentStatus;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Repositories.Intefaces
{
    public interface IEmploymentStatusRepository
    {
        Task<bool> CreateMany(List<TbEmpEmploymentStatus> lstEmploymentStatus);

        Task<bool> UpdateMany(List<TbEmpEmploymentStatus> lstEmploymentStatus);

        Task<bool> Delete(int id);

        Task<TbEmpEmploymentStatus> GetById(int id);

        bool CheckExistEmploymentStatusById(int id);

        Task<List<EmploymentStatusViewModel>> GetByEmployeeId(int employeeId);

        Task<List<TbEmpEmploymentStatus>> GetListEmpStatusByStartDateNew(int employeeId);

        bool CheckActiveStatusLevel1(int empContractId);

        Task<List<EmploymentStatusResult>> GetStatusConfirmation(string locale);

        bool CheckExistStartDate(int empContractId, DateTime startDate);

        Task<bool> Update(TbEmpEmploymentStatus empStatus);
    }
}