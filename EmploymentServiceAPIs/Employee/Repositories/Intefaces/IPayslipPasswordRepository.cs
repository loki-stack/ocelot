﻿using CommonLib.Models.Client;
using EmploymentServiceAPIs.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Repositories.Intefaces
{
    public interface IPayslipPasswordRepository
    {
        Task<bool> Create(TbEmpPayslipPassword payslipPassword);

        Task<bool> Update(TbEmpPayslipPassword payslipPassword);

        Task<TbEmpPayslipPassword> GetById(int id);

        Task<List<TbEmpPayslipPassword>> GetByEmployeeId(int employeeId);

        Task<TbEmpPayslipPassword> GetLatestByEmployeeId(int employeeId);
    }
}