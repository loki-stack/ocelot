﻿using CommonLib.Models.Client;
using EmploymentServiceAPIs.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Repositories.Intefaces
{
    public interface IMovementRepository
    {
        Task<bool> CreateMany(List<TbEmpMovement> movements);

        Task<bool> UpdateMany(List<TbEmpMovement> movements);

        Task<TbEmpMovement> GetById(int id);

        bool CheckExistMovementById(int id);

        Task<List<TbEmpMovement>> GetByEmploymentId(int employmentId);

        Task<List<TbEmpMovement>> GetByEmployeeId(int employeeId);

        Task<TbEmpMovement> Create(TbEmpMovement movement);
        Task<bool> Update(TbEmpMovement movement);
    }
}