﻿using CommonLib.Models.Client;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Dtos.RentalAddress;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Repositories.Intefaces
{
    public interface IRentalAddressRepository
    {
        Task<bool> Create(int employeeId, List<RentalAddressRequest> request, string userName);

        Task<bool> Update(List<RentalAddressRequest> request, string userName);

        Task<TbEmpRentalAddress> GetById(int id);

        bool CheckExistRentalAddressById(int id);

        Task<List<TbEmpRentalAddress>> GetByEmployeeId(int employeeId);
    }
}