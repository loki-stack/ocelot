﻿using CommonLib.Models.Client;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Dtos.TaxArrangement;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Repositories.Intefaces
{
    public interface ITaxRepository
    {
        Task<bool> CreateMany(List<TaxRequest> request, string userName);
        Task<bool> Update(List<TaxRequest> request, string userName);
        Task<TbEmpTax> GetById(int id);
        bool CheckExists(int employmentContractId);
        Task<TbEmpTax> GetByEmploymentContract(int employmentContractId);
        Task<List<TbEmpTax>> GetByEmployeeId(int employeeId);
        Task<TbEmpTax> Create(TbEmpTax tax);
    }
}