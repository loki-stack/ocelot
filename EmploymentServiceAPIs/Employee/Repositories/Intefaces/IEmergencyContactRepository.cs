﻿using CommonLib.Models.Client;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Dtos.EmergencyContact;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Repositories.Intefaces
{
    public interface IEmergencyContactRepository
    {
        Task<bool> Create(int employeeId, List<EmergencyContactRequest> emergencyContacts, string userName);

        Task<bool> Update(List<EmergencyContactRequest> emergencyContacts, string modifiedBy);

        Task<TbEmpEmergencyContactPerson> GetById(int id);

        bool CheckExistEmployeeById(int id);

        Task<List<TbEmpEmergencyContactPerson>> GetByEmployeeId(int employeeId);
    }
}