﻿using CommonLib.Models.Client;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Dtos.Attachment;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Repositories.Intefaces
{
    public interface IAttachmentRepository
    {
        Task<List<TbEmpAttachment>> GetByEmployeeId(int employeeId);
        Task Create(int employeeId, List<AttachmentRequest> request, string userName);
        Task Update(List<AttachmentRequest> request, string userName);
        Task Delete(List<AttachmentRequest> request);
    }
}
