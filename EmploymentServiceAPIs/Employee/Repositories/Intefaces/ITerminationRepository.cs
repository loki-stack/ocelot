﻿using CommonLib.Models.Client;
using EmploymentServiceAPIs.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Repositories.Intefaces
{
    public interface ITerminationRepository
    {
        Task<int?> Create(TbEmpTermination termination);

        Task<int?> Update(TbEmpTermination termination);

        Task<TbEmpTermination> GetById(int id);

        bool CheckExistTermination(int id);

        Task<TbEmpTermination> GetByEmpContractId(int empContractId);

        bool CheckExistTerminationOfContract(int empContractId);

        Task<bool> UpdateMany(List<TbEmpTermination> terminations);

        Task<List<TbEmpTermination>> GetByEmployeeId(int employeeId);
    }
}