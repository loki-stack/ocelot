﻿using CommonLib.Models.Client;
using EmploymentServiceAPIs.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Repositories.Intefaces
{
    public interface IWorkingSheduleRepository
    {
        Task<List<TbCfgWorkingSchedule>> GetAll();
    }
}