﻿using CommonLib.Models.Client;
using EmploymentServiceAPIs.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Repositories.Intefaces
{
    public interface IEmploymentContractRepository
    {
        Task<List<TbEmpEmploymentContract>> CreateMany(List<TbEmpEmploymentContract> employmentContracts);

        Task<TbEmpEmploymentContract> Create(TbEmpEmploymentContract employmentContract);

        Task<bool> Update(List<TbEmpEmploymentContract> employmentContracts);

        Task<TbEmpEmploymentContract> GetById(int id);

        Task<List<TbEmpEmploymentContract>> GetByEmployeeId(int employeeId);

        bool CheckExistEContractById(int id);

        bool CheckExistEmployeeNo(int clientId, int entityId, int employeeId, string employeeNo);

        bool CheckExistGlobalNo(int employeeId, string globalNo);

        bool CheckExistContractNo(int employmentContractId, string contractNo);
    }
}