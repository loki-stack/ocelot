﻿using CommonLib.Models.Client;
using EmploymentServiceAPIs.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Repositories.Intefaces
{
    public interface IBankAccountRepository
    {
        Task<bool> Create(List<TbEmpEeBankAccount> bankAccounts);

        Task<bool> Update(List<TbEmpEeBankAccount> bankAccounts);

        Task<TbEmpEeBankAccount> GetById(int id);

        bool CheckExistBankAccountById(int id);

        Task<List<TbEmpEeBankAccount>> GetByEmployeeId(int employeeId);
    }
}