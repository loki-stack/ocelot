﻿using CommonLib.Models.Client;
using CommonLib.Dtos;
using EmploymentServiceAPIs.Employee.Dtos.Employee;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Repositories.Intefaces
{
    public interface IEmployeeRepository
    {
        Task<List<EmployeeViewList>> GetAll(string languageTag);
        Task<TbEmpEmployee> Create(EmployeeRequest request, string userName);
        Task<TbEmpEmployee> Update(int employeeId, EmployeeRequest request, string userName);
        Task<TbEmpEmployee> UpdateAfterCreateI18N(TbEmpEmployee employee);
        Task<EmployeeViewModel> GetBasicInfo(int employeeId, string primaryLanguage, string secondaryLanguage);
        Task<TbEmpEmployee> GetById(int id);
        bool CheckExistEmployeeById(int id);
        Task<TbEmpEmployee> Edit(TbEmpEmployee employee);
        Task<TbEmpEmployee> GetSingleByCondition(Expression<Func<TbEmpEmployee, bool>> expression, string[] includes = null);
        Task<EmployeeProfile> GetEmployeeProfile(int employeeId, string languageTag);
        Task<CompanyInformation> GetCompanyInfo(int employeeId);
        Task<bool> UpdateCompanyInfo(int employeeId, CompanyInformation companyInfo, string userName);
        Task<List<TbEmpPayslip>> GetAllPayslips(int employeeId);
        Task<List<TbEmpTaxReturn>> GetAllTaxReturn(int employeeId);
        Task<bool> CreateCoreUser(TbEmpEmployee employee, ApiRequest<NewHireRequest> request);
    }
}