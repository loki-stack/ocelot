﻿using CommonLib.Models.Client;
using EmploymentServiceAPIs.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Repositories.Intefaces
{
    public interface IDependentRepository
    {
        Task<bool> Create(List<TbEmpDependent> dependents);

        Task<bool> Update(List<TbEmpDependent> dependents);

        Task<TbEmpDependent> GetById(int id);

        bool CheckExistDependentById(int id);

        Task<List<TbEmpDependent>> GetByEmployeeId(int employeeId);
    }
}