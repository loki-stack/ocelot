﻿using CommonLib.Models.Client;
using EmploymentServiceAPIs.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Repositories.Intefaces
{
    public interface IFileRepository
    {
        Task<List<TbComFile>> Create(List<TbComFile> files);

        Task<TbComFile> Update(TbComFile file);

        Task<TbComFile> GetById(int id);

        bool CheckExistFileById(int id);

        Task<List<TbComFile>> GetListFileByIds(List<int> ids);
    }
}