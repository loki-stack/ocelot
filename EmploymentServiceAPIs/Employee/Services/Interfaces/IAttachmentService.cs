﻿using CommonLib.Dtos;
using EmploymentServiceAPIs.Employee.Dtos.Attachment;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Services.Interfaces
{
    public interface IAttachmentService
    {
        Task<ApiResponse<Dictionary<string, object>>> GetAll(int employeeId, ApiRequest request);
        Task<ApiResponse<Dictionary<string, object>>> SaveAttachment(int employeeId, ApiRequest<List<AttachmentRequest>> request, string userName);
    }
}
