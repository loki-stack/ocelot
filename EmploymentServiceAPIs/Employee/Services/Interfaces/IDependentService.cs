﻿using CommonLib.Dtos;
using EmploymentServiceAPIs.Employee.Dtos.Dependent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Services.Interfaces
{
    public interface IDependentService
    {
        Task<ApiResponse<Dictionary<string, object>>> Save(int clientId, int entityId, int employeeId, string ray, string username, DependentSaveModel dependentSM);

        Task<ApiResponse<DependentViewModel>> GetById(int clientId, int entityId, int id, string ray);

        Task<ApiResponse<DependentDto>> GetByEmployeeId(int clientId, int entityId, int employeeId, string ray);
    }
}