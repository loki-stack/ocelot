﻿using CommonLib.Dtos;
using EmploymentServiceAPIs.Employee.Dtos.Termination;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Services.Interfaces
{
    public interface ITerminationService
    {
        Task<ApiResponse<TerminationViewModel>> Save(string ray, string username, TerminationSaveModel terminationSM);

        Task<ApiResponse<TerminationViewModel>> GetById(int id, string ray);

        Task<ApiResponse<TerminationViewModel>> GetByEmpContractId(int empContractId, string ray);
    }
}