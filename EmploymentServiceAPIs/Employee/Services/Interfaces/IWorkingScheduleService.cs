﻿using CommonLib.Dtos;
using CommonLib.Models.Client;
using EmploymentServiceAPIs.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Services.Interfaces
{
    public interface IWorkingScheduleService
    {
        Task<ApiResponse<List<TbCfgWorkingSchedule>>> GetAll(string ray);
    }
}