﻿using CommonLib.Dtos;
using EmploymentServiceAPIs.Employee.Dtos.EmploymentContract;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Services.Interfaces
{
    public interface IEmploymentContractService
    {
        Task<ApiResponse<Dictionary<string, object>>> Save(int employeeId, string ray, string username, EmploymentContractSaveModel employmentContractDto);

        Task<ApiResponse<EmploymentContractViewModel>> GetById(int id, string ray);

        Task<ApiResponse<EmploymentContractDto>> GetByEmployeeId(int employeeId, string ray);

        Task<ApiResponse<Dictionary<string, object>>> Create(int employeeId, string ray, string username, EmploymentContractAddModel empContractAm);

        Task<ApiResponse<EmploymentContractViewModel>> GetContractNew(int employeeId, string ray);
    }
}