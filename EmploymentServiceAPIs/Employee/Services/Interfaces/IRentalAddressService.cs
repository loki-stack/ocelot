﻿using CommonLib.Dtos;
using EmploymentServiceAPIs.Employee.Dtos.RentalAddress;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Services.Interfaces
{
    public interface IRentalAddressService
    {
        Task<ApiResponse<Dictionary<string, object>>> GetRentalAddress(int employeeId, ApiRequest request);
        Task<ApiResponse<Dictionary<string, object>>> SaveRentalAddress(int employeeId, ApiRequest<RentalAddressSaveModel> request, string userName);
    }
}
