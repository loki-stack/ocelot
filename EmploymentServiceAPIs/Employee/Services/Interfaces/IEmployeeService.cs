﻿using CommonLib.Dtos;
using EmploymentServiceAPIs.Common.Dtos;
using EmploymentServiceAPIs.Employee.Dtos.Employee;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Services.Interfaces
{
    public interface IEmployeeService
    {
        Task<ApiResponse<Dictionary<string, object>>> GetAll(ApiRequest<PaginationRequest> request);
        Task<ApiResponse<Dictionary<string, object>>> GetProfile(int employeeId, ApiRequest request);
        Task<ApiResponse<Dictionary<string, object>>> GetCompanyInfo(int employeeId, ApiRequest request);
        Task<ApiResponse<Dictionary<string, object>>> SaveCompanyInfo(int employeeId, ApiRequest<CompanyInformation> request, string userName);
        Task<ApiResponse<Dictionary<string, object>>> GetBasicInfo(int employeeId, ApiRequest request);
        Task<ApiResponse<Dictionary<string, object>>> SaveBasicInfo(int employeeId, ApiRequest<EmployeeSaveModel> request, string userName);
        Task<ApiResponse<Dictionary<string, object>>> NewHire(ApiRequest<NewHireRequest> request, string userName);
        Task<ApiResponse<int>> CountByStatus(string status);
    }
}
