﻿using CommonLib.Dtos;
using EmploymentServiceAPIs.Employee.Dtos.EmploymentStatus;
using EmploymentServiceAPIs.ManageEntity.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Services.Interfaces
{
    public interface IEmploymentStatusService
    {
        Task<ApiResponse<EmploymentStatusDto>> Save(int employeeId, string ray, string username, EmploymentStatusSaveModel employmentStatusSM);

        Task<ApiResponse<EmploymentStatusViewModel>> GetById(int id, string ray);

        Task<ApiResponse<EmploymentStatusDto>> GetByEmployeeId(int employeeId, string ray);

        Task<ApiResponse<EmploymentStatusDto>> Delete(int id, int employeeId, string ray);

        Task<ApiResponse<Dictionary<string, object>>> GetStatusConfirmation(int clientId, int entityId, string ray, string token, string locale, BasePaginationRequest request);

        Task<ApiResponse<int?>> Update(string username, string ray, int id);
    }
}