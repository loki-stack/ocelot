﻿using CommonLib.Dtos;
using EmploymentServiceAPIs.Employee.Dtos.ManageFile;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.IO.Compression;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Services.Interfaces
{
    public interface IFileService
    {
        Task<ApiResponse<List<FileViewModel>>> UploadFile(int clientId, int entityId, string ray, string username, List<InforUploadModel> infors, IList<IFormFile> files);

        Task<ApiResponse<FileDownloadModel>> DownloadFile(int id, string ray);

        Task<ApiResponse<List<FileViewModel>>> GetListFileByIds(int clientId, int entityId, string ray, List<int> ids);
        /// <summary>
        /// Get files and write to archive object
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="entityId"></param>
        /// <param name="employeeId"></param>
        /// <param name="archive"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task GetFilesAsZip(int clientId, int entityId, int employeeId, ZipArchive archive, List<int> ids);
    }
}