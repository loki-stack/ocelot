﻿using CommonLib.Dtos;
using EmploymentServiceAPIs.Employee.Dtos.EmergencyContact;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Services.Interfaces
{
    public interface IEmergencyContactService
    {
        Task<ApiResponse<Dictionary<string, object>>> GetEmergencyContact(int employeeId, ApiRequest request);
        Task<ApiResponse<Dictionary<string, object>>> SaveEmergencyContact(int employeeId, ApiRequest<List<EmergencyContactRequest>> request, string userName);
    }
}
