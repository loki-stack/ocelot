﻿using CommonLib.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Services.Interfaces
{
    /// <summary>
    /// The interface which is used to call CoreService API
    /// </summary>
    public interface ICoreService
    {
        Task<Dictionary<string, List<GenericCodeResult>>> GetGenericCodes(List<string> genericCodes, string token, StdRequestParam param);
    }
}
