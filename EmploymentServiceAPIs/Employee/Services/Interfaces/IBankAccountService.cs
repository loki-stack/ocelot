﻿using CommonLib.Dtos;
using EmploymentServiceAPIs.Employee.Dtos.BankAccount;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Services.Interfaces
{
    public interface IBankAccountService
    {
        Task<ApiResponse<Dictionary<string, object>>> Save(int employeeId, ApiRequest<BankAccountSaveModel> request, string username);

        Task<ApiResponse<BankAccountViewModel>> GetById(int id, string ray);

        Task<ApiResponse<BankAccountDto>> GetByEmployeeId(int employeeId, string ray);
    }
}