﻿using CommonLib.Dtos;
using EmploymentServiceAPIs.Employee.Dtos.TaxArrangement;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Services.Interfaces
{
    public interface ITaxService
    {
        Task<ApiResponse<Dictionary<string, object>>> GetTaxInfo(int employeeId, ApiRequest request);
        Task<ApiResponse<Dictionary<string, object>>> SaveTaxInfo(int employeeId, ApiRequest<TaxSaveModel> request, string userName);
    }
}
