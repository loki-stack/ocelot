﻿using CommonLib.Dtos;
using EmploymentServiceAPIs.Employee.Dtos.Movement;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Services.Interfaces
{
    public interface IMovementService
    {
        Task<ApiResponse<Dictionary<string, object>>> Save(int employmentId, string ray, string username, List<MovementModel> movementAms);

        Task<ApiResponse<MovementViewModel>> GetById(int id, string ray);

        Task<ApiResponse<List<MovementViewModel>>> GetByEmploymentId(int employmentId, string ray);
    }
}