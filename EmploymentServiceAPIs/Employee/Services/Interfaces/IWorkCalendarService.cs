﻿using CommonLib.Dtos;
using EmploymentServiceAPIs.ManageEntity.Dtos;
using EmploymentServiceAPIs.ManageEntity.Dtos.WorkCalendar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Services.Interfaces
{
    public interface IWorkCalendarService
    {
        Task<ApiResponse<Dictionary<string, object>>> GetFilter(int clientId, int entityId, string locale, string ray, BasePaginationRequest request);

        Task<ApiResponse<WorkCalendarViewModel>> Save(string username, string ray, WorkCalendarDto workCalendarDto);

        Task<ApiResponse<WorkCalendarViewModel>> GetById(int id, string ray);

        Task<ApiResponse<bool?>> DeleteWorkCalendar(int id, string ray);

        Task<ApiResponse<bool?>> DeleteHoliday(int id, string ray);
    }
}