﻿using CommonLib.Constants;
using CommonLib.Dtos;
using CommonLib.Models.Client;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Constants;
using EmploymentServiceAPIs.Employee.Dtos.Attachment;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using EmploymentServiceAPIs.Employee.Services.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Services.Impl
{
    public class AttachmentService : IAttachmentService
    {
        private readonly IAttachmentRepository _attachmentRepository;
        private readonly ILogger<AttachmentService> _logger;
        public AttachmentService(IAttachmentRepository attachmentRepository, ILogger<AttachmentService> logger)
        {
            _attachmentRepository = attachmentRepository;
            _logger = logger;
        }
        public async Task<ApiResponse<Dictionary<string, object>>> GetAll(int employeeId, ApiRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();
            List<TbEmpAttachment> attachments = await _attachmentRepository.GetByEmployeeId(employeeId);
            response.Data.Add("attachments", attachments);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> SaveAttachment(int employeeId, ApiRequest<List<AttachmentRequest>> request, string userName)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();

            List<AttachmentRequest> listCreate = request.Data.Where(x => x.AttachmentId == 0).ToList();
            List<AttachmentRequest> listUpdate = request.Data.Where(x => x.AttachmentId != 0).ToList();
            List<AttachmentRequest> listDelete = request.Data.Where(x => x.AttachmentId != 0 && x.isDeleted == true).ToList();

            List<MessageItem> messageItems = new List<MessageItem>();
            MessageItem itemResult = new MessageItem();
            if (listCreate != null && listCreate.Count > 0)
            {
                itemResult = await CreateAttachment(employeeId, listCreate, userName);
                if (itemResult != null)
                    messageItems.Add(new MessageItem(Level.ERROR, Label.ATT001));
            }
            if (listUpdate != null && listUpdate.Count > 0)
            {
                itemResult = await UpdateAttachment(listUpdate, userName);
                if (itemResult != null)
                    messageItems.Add(new MessageItem(Level.ERROR, Label.ATT002));
            }
            if (listDelete != null && listDelete.Count > 0)
            {
                itemResult = await DeleteAttachment(listDelete);
                if (itemResult != null)
                   messageItems.Add(new MessageItem(Level.ERROR, Label.ATT004));
            }
            if (messageItems.Count > 0)
            {
                response.Data = null;
                response.Message.Toast.AddRange(messageItems);
                return response;
            }
            List<TbEmpAttachment> attachments = await _attachmentRepository.GetByEmployeeId(employeeId);
            response.Data.Add("attachments", attachments);
            response.Message.Toast.Add(new MessageItem(Level.SUCCESS, Label.ATT003));
            return response;
        }

        public async Task<MessageItem> CreateAttachment(int employeeId, List<AttachmentRequest> listCreate, string userName)
        {
            try
            {
                await _attachmentRepository.Create(employeeId, listCreate, userName);
                return null;
            }
            catch (Exception ex)
            {
                return new MessageItem(Level.ERROR, Label.ATT001);
            }
        }

        public async Task<MessageItem> UpdateAttachment(List<AttachmentRequest> listUpdate, string userName)
        {
            try
            {
                await _attachmentRepository.Update(listUpdate, userName);
                return null;
            }
            catch (Exception ex)
            {
                return new MessageItem(Level.ERROR, Label.ATT002);
            }
        }

        public async Task<MessageItem> DeleteAttachment(List<AttachmentRequest> listDelete)
        {
            try
            {
                await _attachmentRepository.Delete(listDelete);
                return null;
            }
            catch (Exception ex)
            {
                return new MessageItem(Level.ERROR, Label.ATT004);
            }
        }
    }
}
