﻿using AutoMapper;
using CommonLib.Constants;
using CommonLib.Dtos;
using CommonLib.Models.Client;
using CommonLib.Models;
using CommonLib.Repositories.Impl;
using CommonLib.Repositories.Interfaces;
using CommonLib.Validation;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Constants;
using EmploymentServiceAPIs.Employee.Dtos;
using EmploymentServiceAPIs.Employee.Services.Interfaces;
using EmploymentServiceAPIs.ManageEntity.Dtos;
using EmploymentServiceAPIs.ManageEntity.Dtos.HolidayCalendar;
using EmploymentServiceAPIs.ManageEntity.Dtos.WorkCalendar;
using EmploymentServiceAPIs.ManageEntity.Repositories.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace EmploymentServiceAPIs.Employee.Services.Impl
{
    public class WorkCalendarService : IWorkCalendarService
    {
        private readonly IWorkCalendarRepository _workCalendarRepository;
        private readonly IHolidayRepository _holidayRepository;
        private readonly IGenericCodeRepository _genericCodeRepository;
        private readonly IGlobalCalendarRepository _globalCalendarRepository;
        private readonly IEntityRepsitory _entityRepsitory;
        private readonly IMapper _mapper;
        private readonly IValidator _validator;

        public WorkCalendarService(IWorkCalendarRepository workCalendarRepository,
                                   IHolidayRepository holidayRepository,
                                   IGenericCodeRepository genericCodeRepository,
                                   IGlobalCalendarRepository globalCalendarRepository,
                                   IEntityRepsitory entityRepsitory,
                                   IMapper mapper,
                                   IValidator validator)
        {
            _workCalendarRepository = workCalendarRepository;
            _holidayRepository = holidayRepository;
            _genericCodeRepository = genericCodeRepository;
            _globalCalendarRepository = globalCalendarRepository;
            _entityRepsitory = entityRepsitory;
            _mapper = mapper;
            _validator = validator;
        }

        public async Task<ApiResponse<bool?>> DeleteHoliday(int id, string ray)
        {
            ApiResponse<bool?> response = new ApiResponse<bool?>
            {
                Ray = ray ?? string.Empty,
                Data = null,
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                }
            };
            //Check exist holiday
            bool isExist = _holidayRepository.CheckExistHolidayById(id);
            if (!isExist)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.HLD0110
                });
                response.Data = null;
                return response;
            }
            await _holidayRepository.Delete(id);
            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.HLD0108
            });
            response.Data = true;
            return response;
        }

        public async Task<ApiResponse<bool?>> DeleteWorkCalendar(int id, string ray)
        {
            ApiResponse<bool?> response = new ApiResponse<bool?>
            {
                Ray = ray ?? string.Empty,
                Data = null,
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                }
            };

            //check exist Calendar
            bool isExist = _workCalendarRepository.CheckExistWorkCalendarById(id);
            if (!isExist)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.WCL0106
                });
                response.Data = null;
                return response;
            }

            //check work calendar link to EE
            bool isExistLinkToEE = await _workCalendarRepository.CheckExistLinkToEE(id);
            if (isExistLinkToEE)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.WCL0127
                });
                response.Data = null;
                return response;
            }

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    //delete holiday
                    var holidays = await _holidayRepository.GetHolidayByCalendarId(id);
                    await _holidayRepository.DeleteMany(holidays);
                    //delete global calendar
                    await _workCalendarRepository.Delete(id);
                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }

            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.WCL0109
            });
            response.Data = true;
            return response;
        }

        public async Task<ApiResponse<WorkCalendarViewModel>> GetById(int id, string ray)
        {
            ApiResponse<WorkCalendarViewModel> response = new ApiResponse<WorkCalendarViewModel>
            {
                Ray = ray ?? string.Empty,
                Data = new WorkCalendarViewModel()
            };
            var workCalendar = await _workCalendarRepository.GetById(id);
            var workCalendarVm = _mapper.Map<TbCamWorkCalendar, WorkCalendarViewModel>(workCalendar);
            response.Data = workCalendarVm;
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetFilter(int clientId, int entityId, string locale, string ray, BasePaginationRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>
            {
                Ray = ray ?? string.Empty,
                Data = new Dictionary<string, object>()
            };

            var workCalendarEntities = await _workCalendarRepository.GetFilter();
            WorkCalendarFilter filter = new WorkCalendarFilter();
            if (!string.IsNullOrEmpty(request.Filter))
            {
                filter = JsonConvert.DeserializeObject<WorkCalendarFilter>(request.Filter);
            }

            //sort by item  from request
            if (!string.IsNullOrEmpty(request.Sort))
            {
                string sortCharacter = request.Sort.Substring(0, 1);
                string sortField = request.Sort.Substring(1, request.Sort.Length - 1);
                if (sortField.ToLower().Contains("calendar"))
                {
                    if (sortCharacter == "-")
                        workCalendarEntities = workCalendarEntities.OrderByDescending(x => x.CalendarName).ToList();
                    else workCalendarEntities = workCalendarEntities.OrderBy(x => x.CalendarName).ToList();
                }
            }

            //search by text
            if (!string.IsNullOrEmpty(request.FullTextSearch))
            {
                workCalendarEntities = workCalendarEntities.Where(x => x.CalendarName.ToLower().Contains(request.FullTextSearch.ToLower())).ToList();
            }

            if (!string.IsNullOrEmpty(filter.Status))
            {
                workCalendarEntities = workCalendarEntities.Where(x => x.Status == filter.Status).ToList();
            }

            StdRequestParam requestParam = new StdRequestParam()
            {
                ClientId = clientId,
                EntityId = entityId,
                Locale = locale
            };

            //Get generic code
            var genericCodes = await _genericCodeRepository.GetGenericCodes(new List<string>() { GenericCode.STATUS }, string.Empty, requestParam);

            List<GenericCodeResult> empStatuses = new List<GenericCodeResult>();

            if (genericCodes.ContainsKey(GenericCode.STATUS.ToLower()))
            {
                empStatuses = genericCodes[GenericCode.STATUS.ToLower()];
            }

            var workCalendarVms = new List<WorkCalendarViewModel>();
            var globalCalendar = await _globalCalendarRepository.GetAll();
            if (clientId != 0 && entityId == 0)
            {
                //Get data work calendar client level
                var dataClientLevel = workCalendarEntities.ToList();
                var entities = await _entityRepsitory.GetAll();
                workCalendarVms = (from w in dataClientLevel
                                   join gc in globalCalendar on w.ParentCalendarId equals gc.GlobalCalendarId into temp
                                   from p in temp.DefaultIfEmpty()
                                   join s in empStatuses on w.Status equals s.CodeValue into temp1
                                   from p1 in temp1.DefaultIfEmpty()
                                   join e in entities on w.SpecificEntityId equals e.EntityId into temp2
                                   from p2 in temp2.DefaultIfEmpty()
                                   select new WorkCalendarViewModel
                                   {
                                       CalendarId = w.CalendarId,
                                       SpecificEntityId = w.SpecificEntityId,
                                       EntityName = p2 != null ? p2.EntityNameTxt : string.Empty,
                                       CalendarName = w.CalendarName,
                                       ParentCalendarId = w.ParentCalendarId,
                                       ParentName = p != null ? p.GlobalLevelCalendarName : string.Empty,
                                       Status = w.Status,
                                       CalendarDay = w.CalendarDay,
                                       StatusName = p1 != null ? p1.TextValue : string.Empty,
                                       DefaultWorkingDaySunday = w.DefaultWorkingDaySunday,
                                       DefaultWorkingDayMonday = w.DefaultWorkingDayMonday,
                                       DefaultWorkingDayTuesday = w.DefaultWorkingDayTuesday,
                                       DefaultWorkingDayWednesday = w.DefaultWorkingDayWednesday,
                                       DefaultWorkingDayThursday = w.DefaultWorkingDayThursday,
                                       DefaultWorkingDayFriday = w.DefaultWorkingDayFriday,
                                       DefaultWorkingDaySaturday = w.DefaultWorkingDaySaturday,
                                       CreatedBy = w.CreatedBy,
                                       CreatedDt = w.CreatedDt,
                                       ModifiedBy = w.ModifiedBy,
                                       ModifiedDt = w.ModifiedDt
                                   }).ToList();
            }
            else if (clientId != 0 && entityId != 0)
            {
                //Get data work calendar entity level
                var dataClientLevel = workCalendarEntities.Where(x => x.SpecificEntityId == entityId).ToList();
                workCalendarVms = (from w1 in dataClientLevel
                                   join gc in globalCalendar on w1.ParentCalendarId equals gc.GlobalCalendarId into temp
                                   from p in temp.DefaultIfEmpty()
                                   join s in empStatuses on w1.Status equals s.CodeValue into temp1
                                   from p1 in temp1.DefaultIfEmpty()
                                   select new WorkCalendarViewModel
                                   {
                                       CalendarId = w1.CalendarId,
                                       SpecificEntityId = w1.SpecificEntityId,
                                       EntityName = string.Empty,
                                       CalendarName = w1.CalendarName,
                                       ParentCalendarId = w1.ParentCalendarId,
                                       ParentName = p != null ? p.GlobalLevelCalendarName : string.Empty,
                                       Status = w1.Status,
                                       CalendarDay = w1.CalendarDay,
                                       StatusName = p1 != null ? p1.TextValue : string.Empty,
                                       DefaultWorkingDaySunday = w1.DefaultWorkingDaySunday,
                                       DefaultWorkingDayMonday = w1.DefaultWorkingDayMonday,
                                       DefaultWorkingDayTuesday = w1.DefaultWorkingDayTuesday,
                                       DefaultWorkingDayWednesday = w1.DefaultWorkingDayWednesday,
                                       DefaultWorkingDayThursday = w1.DefaultWorkingDayThursday,
                                       DefaultWorkingDayFriday = w1.DefaultWorkingDayFriday,
                                       DefaultWorkingDaySaturday = w1.DefaultWorkingDaySaturday,
                                       CreatedBy = w1.CreatedBy,
                                       CreatedDt = w1.CreatedDt,
                                       ModifiedBy = w1.ModifiedBy,
                                       ModifiedDt = w1.ModifiedDt
                                   }).ToList();
            }

            Pagination<WorkCalendarViewModel> pagination = new Pagination<WorkCalendarViewModel>();
            var dataPaging = new List<WorkCalendarViewModel>();
            if (request.Size != null)
            {
                pagination.Size = request.Size.Value;
                pagination.TotalPages = (workCalendarVms.Count + request.Size.Value - 1) / request.Size.Value;
                dataPaging = workCalendarVms.Skip((request.Page.Value - 1) * request.Size.Value).Take(request.Size.Value).ToList();
            }
            else
            {
                dataPaging = workCalendarVms;
            }
            pagination.Page = request.Page.Value;
            pagination.TotalElements = workCalendarVms.Count;
            pagination.NumberOfElements = dataPaging.Count;
            pagination.Content = dataPaging;
            response.Data.Add("pagination", pagination);
            return response;
        }

        public async Task<ApiResponse<WorkCalendarViewModel>> Save(string username, string ray, WorkCalendarDto workCalendarDto)
        {
            ApiResponse<WorkCalendarViewModel> response = new ApiResponse<WorkCalendarViewModel>
            {
                Ray = ray ?? string.Empty,
                Data = new WorkCalendarViewModel(),
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                    ValidateResult = new List<MessageItem>()
                }
            };
            //Validate data
            response = await ValidateData(ray, workCalendarDto);
            if (response.Message.ValidateResult.Count > 0 || response.Message.Toast.Count > 0)
            {
                return response;
            }

            DateTime currentDate = DateTime.Now;
            int id = 0;
            //Save data
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    //Save global calendar
                    id = await SaveWorkCalendar(username, currentDate, workCalendarDto.WorkCalendar);
                    //Save holiday
                    await SaveHoliday(username, currentDate, id, workCalendarDto.WorkCalendar.ParentCalendarId.Value, workCalendarDto.Holidays);
                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }

            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.WCL0104
            });
            var data = await GetById(id, ray);
            response.Data = data.Data;
            return response;
        }

        private async Task<int> SaveWorkCalendar(string username, DateTime currentDate, WorkCalendarModel workCalendar)
        {
            int id = 0;
            if (workCalendar.CalendarId == 0)
            {
                //create work calendar
                var workCalendarCalendarEntity = _mapper.Map<WorkCalendarModel, TbCamWorkCalendar>(workCalendar);
                workCalendarCalendarEntity.CreatedBy = username;
                workCalendarCalendarEntity.CreatedDt = currentDate;
                workCalendarCalendarEntity.ModifiedBy = username;
                workCalendarCalendarEntity.ModifiedDt = currentDate;

                id = await _workCalendarRepository.Create(workCalendarCalendarEntity);
            }
            else
            {
                //update work calendar
                var workCalendarCalendarEntity = await _workCalendarRepository.GetById(workCalendar.CalendarId);
                //workCalendarCalendarEntity.SpecificEntityId = workCalendar.SpecificEntityId
                //workCalendarCalendarEntity.CalendarName = workCalendar.CalendarName;
                workCalendarCalendarEntity.ParentCalendarId = workCalendar.ParentCalendarId;
                workCalendarCalendarEntity.Status = workCalendar.Status;
                workCalendarCalendarEntity.CalendarDay = workCalendar.CalendarDay;
                workCalendarCalendarEntity.DefaultWorkingDaySunday = workCalendar.DefaultWorkingDaySunday;
                workCalendarCalendarEntity.DefaultWorkingDayMonday = workCalendar.DefaultWorkingDayMonday;
                workCalendarCalendarEntity.DefaultWorkingDayTuesday = workCalendar.DefaultWorkingDayTuesday;
                workCalendarCalendarEntity.DefaultWorkingDayWednesday = workCalendar.DefaultWorkingDayWednesday;
                workCalendarCalendarEntity.DefaultWorkingDayThursday = workCalendar.DefaultWorkingDayThursday;
                workCalendarCalendarEntity.DefaultWorkingDayFriday = workCalendar.DefaultWorkingDayFriday;
                workCalendarCalendarEntity.DefaultWorkingDaySaturday = workCalendar.DefaultWorkingDaySaturday;
                workCalendarCalendarEntity.ModifiedBy = username;
                workCalendarCalendarEntity.ModifiedDt = currentDate;

                id = await _workCalendarRepository.Update(workCalendarCalendarEntity);
            }

            return id;
        }

        private async Task SaveHoliday(string username, DateTime currentDate, int calendarId, int parentCalendarId, List<HolidayModel> holidayModels)
        {
            var lstHolidayCreate = holidayModels.Where(x => x.HolidayDateId == 0).ToList();
            var lstHolidayUpdate = holidayModels.Where(x => x.HolidayDateId != 0).ToList();

            //Create holiday
            if (lstHolidayCreate.Count > 0)
            {
                foreach (var holiday in lstHolidayCreate)
                {
                    holiday.CalendarId = calendarId;
                    holiday.CreatedBy = username;
                    holiday.CreatedDt = currentDate;
                    holiday.ModifiedBy = username;
                    holiday.ModifiedDt = currentDate;
                }
                var holidayEntities = _mapper.Map<List<HolidayModel>, List<TbCamHoliday>>(lstHolidayCreate);
                await _holidayRepository.CreateMany(holidayEntities);
            }

            //Update holiday
            if (lstHolidayUpdate.Count > 0)
            {
                if (calendarId != 0)
                {
                    var ids = lstHolidayUpdate.Select(x => x.HolidayDateId).ToList();
                    var holidayEntities = await _holidayRepository.GetByIds(ids);
                    foreach (var holiday in holidayEntities)
                    {
                        var holidayModel = lstHolidayUpdate.FirstOrDefault(x => x.HolidayDateId == holiday.HolidayDateId);
                        if (holidayModel.IsHolidayOfGlobalCalendar.Value)
                        {
                            holiday.IsWorkingDay = holidayModel.IsWorkingDay;
                            holiday.ModifiedBy = username;
                            holiday.ModifiedDt = currentDate;
                        }
                        else
                        {
                            holiday.Date = holidayModel.Date;
                            holiday.DescriptionPrimaryLanguage = holidayModel.DescriptionPrimaryLanguage;
                            holiday.DescriptionSecondaryLanguage = holidayModel.DescriptionSecondaryLanguage;
                            holiday.HolidayType = holidayModel.HolidayType;
                            holiday.IsWorkingDay = holidayModel.IsWorkingDay;
                            holiday.ModifiedBy = username;
                            holiday.ModifiedDt = currentDate;
                        }
                    }
                    await _holidayRepository.UpdateMany(holidayEntities);
                }
            }
        }

        private async Task<ApiResponse<WorkCalendarViewModel>> ValidateData(string ray, WorkCalendarDto workCalendarDto)
        {
            ApiResponse<WorkCalendarViewModel> response = new ApiResponse<WorkCalendarViewModel>
            {
                Ray = ray ?? string.Empty,
                Data = new WorkCalendarViewModel(),
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                    ValidateResult = new List<MessageItem>()
                }
            };

            //validate data
            var messages = _validator.Validate(workCalendarDto.WorkCalendar);
            if (workCalendarDto.Holidays != null)
            {
                for (int i = 0; i < workCalendarDto.Holidays.Count; i++)
                {
                    var message = _validator.Validate(workCalendarDto.Holidays[i]);
                    for (int j = 0; j < message.Count; j++)
                    {
                        message[j].ComponentId = $"workCalendar[{i}]_{message[j].ComponentId}";
                    }
                    messages.AddRange(message);
                }
            }

            //return respone when data invalid
            if (messages.Count > 0)
            {
                //response.Message.Toast.Add(new MessageItem
                //{
                //    Level = Level.ERROR,
                //    LabelCode = Label.WCL0105
                //});
                response.Message.ValidateResult = messages;
                response.Data = null;
                return response;
            }

            //bool isExistActive = await _workCalendarRepository.CheckExistCalendarActiveInEntity(workCalendarDto.WorkCalendar.SpecificEntityId.Value);
            //if (isExistActive && workCalendarDto.WorkCalendar.Status == EmploymentServiceAPIs.Common.Constants.Status.ACTIVE)
            //{
            //    response.Message.Toast.Add(new MessageItem
            //    {
            //        Level = Level.ERROR,
            //        LabelCode = Label.WCL0126
            //    });
            //    response.Data = null;
            //    return response;
            //}

            //check exist global calendar
            if (workCalendarDto.WorkCalendar.CalendarId != 0)
            {
                bool isExist = _workCalendarRepository.CheckExistWorkCalendarById(workCalendarDto.WorkCalendar.CalendarId);
                if (!isExist)
                {
                    response.Message.Toast.Add(new MessageItem
                    {
                        Level = Level.ERROR,
                        LabelCode = Label.WCL0106
                    });
                    response.Data = null;
                    return response;
                }

                messages = new List<MessageItem>();
                var holidayEntities = await _holidayRepository.GetHolidayByCalendarId(workCalendarDto.WorkCalendar.CalendarId);
                //check exist holiday
                if (workCalendarDto.Holidays != null)
                {
                    for (int i = 0; i < workCalendarDto.Holidays.Count; i++)
                    {
                        if (workCalendarDto.Holidays[i].HolidayDateId != 0)
                        {
                            isExist = holidayEntities.Any(x => x.HolidayDateId == workCalendarDto.Holidays[i].HolidayDateId);
                            if (!isExist)
                            {
                                messages.Add(new MessageItem
                                {
                                    ComponentId = $"holiday[{i}]",
                                    Level = Level.ERROR,
                                    LabelCode = Label.HLD0105
                                });
                            }
                        }
                    }

                    if (messages.Count > 0)
                    {
                        response.Message.Toast = messages;
                        response.Data = null;
                        return response;
                    }
                }
            }

            return response;
        }
    }
}