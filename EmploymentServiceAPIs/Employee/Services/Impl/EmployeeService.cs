﻿using AutoMapper;
using CommonLib.Constants;
using CommonLib.Dtos;
using CommonLib.Models;
using Client = CommonLib.Models.Client;
using CommonLib.Repositories.Interfaces;
using CommonLib.Validation;
using EmploymentServiceAPIs.Common.Dtos;
using EmploymentServiceAPIs.Common.Repositories.Interfaces;
using EmploymentServiceAPIs.Employee.Constants;
using EmploymentServiceAPIs.Employee.Dtos;
using EmploymentServiceAPIs.Employee.Dtos.Attachment;
using EmploymentServiceAPIs.Employee.Dtos.EmergencyContact;
using EmploymentServiceAPIs.Employee.Dtos.Employee;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using EmploymentServiceAPIs.Employee.Services.Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using CommonLib.Models.Client;
using Core = CommonLib.Models.Core;

namespace EmploymentServiceAPIs.Employee.Services.Impl
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IDependentRepository _dependentRepository;
        private readonly IMovementRepository _movementRepository;
        private readonly IEntitySettingRepository _entitySettingRepository;
        private readonly ITbCfgI18NRepository _tbCfgI18NRepository;
        private readonly IEmergencyContactRepository _emergencyContactRepository;
        private readonly IAttachmentRepository _attachmentRepository;
        private readonly IValidator _validator;
        private readonly IEmploymentContractRepository _employmentContractRepository;
        private readonly IEmploymentStatusRepository _employmentStatusRepository;
        private readonly ITaxRepository _taxRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<EmployeeService> _logger;

        public EmployeeService(IEmployeeRepository employeeRepository,
                               IDependentRepository dependentRepository,
                               IMovementRepository movementRepository,
                               IEntitySettingRepository entitySettingRepository,
                               ITbCfgI18NRepository tbCfgI18NRepository,
                               IEmergencyContactRepository emergencyContactRepository,
                               IAttachmentRepository attachmentRepository,
                               IValidator validator,
                               IEmploymentContractRepository employmentContractRepository,
                               IEmploymentStatusRepository employmentStatusRepository,
                               ITaxRepository taxRepository,
                               IMapper mapper,
                               ILogger<EmployeeService> logger)
        {
            _employeeRepository = employeeRepository;
            _dependentRepository = dependentRepository;
            _movementRepository = movementRepository;
            _entitySettingRepository = entitySettingRepository;
            _tbCfgI18NRepository = tbCfgI18NRepository;
            _emergencyContactRepository = emergencyContactRepository;
            _attachmentRepository = attachmentRepository;
            _validator = validator;
            _employmentContractRepository = employmentContractRepository;
            _employmentStatusRepository = employmentStatusRepository;
            _taxRepository = taxRepository;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetAll(ApiRequest<PaginationRequest> request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();
            PaginationRequest paginationRequest = request.Data;
            List<EmployeeViewList> employees = await _employeeRepository.GetAll(request.Parameter.Locale);

            EmployeeFilter filter = new EmployeeFilter();

            //get filter from request
            if (!string.IsNullOrEmpty(paginationRequest.Filter))
            {
                filter = JsonConvert.DeserializeObject<EmployeeFilter>(paginationRequest.Filter);
            }

            // sort by item from request
            if (!string.IsNullOrEmpty(paginationRequest.Sort))
            {
                char sortCharacter = paginationRequest.Sort.ElementAt(0);
                string sortField = paginationRequest.Sort.Substring(1, paginationRequest.Sort.Length - 1);
                if (sortField.ToLower().Contains("name"))
                {
                    if (sortCharacter == '-')
                        employees = employees.OrderByDescending(x => x.Name).ToList();
                    else employees = employees.OrderBy(x => x.Name).ToList();
                }
                if (sortField.ToLower().Contains("role"))
                {
                    if (sortCharacter == '-')
                        employees = employees.OrderByDescending(x => x.Role).ToList();
                    else employees = employees.OrderBy(x => x.Role).ToList();
                }
                if (sortField.ToLower().Contains("division"))
                {
                    if (sortCharacter == '-')
                        employees = employees.OrderByDescending(x => x.Division).ToList();
                    else employees = employees.OrderBy(x => x.Division).ToList();
                }
                if (sortField.ToLower().Contains("grade"))
                {
                    if (sortCharacter == '-')
                        employees = employees.OrderByDescending(x => x.Grade).ToList();
                    else employees = employees.OrderBy(x => x.Grade).ToList();
                }
                if (sortField.ToLower().Contains("staffid"))
                {
                    if (sortCharacter == '-')
                        employees = employees.OrderByDescending(x => x.StaffId).ToList();
                    else employees = employees.OrderBy(x => x.StaffId).ToList();
                }
            }
            // search by filter
            if (!string.IsNullOrEmpty(filter.Role))
            {
                employees = employees.Where(x => x.Role == filter.Role).ToList();
            }
            if (!string.IsNullOrEmpty(filter.DivisionTxt))
            {
                employees = employees.Where(x => x.DivisionTxt == filter.DivisionTxt).ToList();
            }
            if (filter.Grade.HasValue)
            {
                employees = employees.Where(x => x.Grade == filter.Grade.Value).ToList();
            }
            if (!string.IsNullOrEmpty(filter.StaffId))
            {
                employees = employees.Where(x => x.StaffId == filter.StaffId).ToList();
            }
            if (!string.IsNullOrEmpty(filter.Status))
            {
                employees = employees.Where(x => x.Status == filter.Status).ToList();
            }
            if (filter.StartDate.HasValue)
            {
                employees = employees.Where(x => x.AssignmentStartDate == filter.StartDate.Value).ToList();
            }

            // search by text
            if (!string.IsNullOrEmpty(paginationRequest.FullTextSearch))
            {
                employees = employees.Where(x => x.Name.ToLower().Contains(paginationRequest.FullTextSearch) ||
                                                    x.Role.ToLower().Contains(paginationRequest.FullTextSearch) ||
                                                    x.DivisionTxt.ToLower().Contains(paginationRequest.FullTextSearch)).ToList();
            }

            // return data
            Pagination<EmployeeViewList> pagination = new Pagination<EmployeeViewList>();
            if (paginationRequest.Size.HasValue)
            {
                pagination.Page = paginationRequest.Page.Value;
                pagination.Size = paginationRequest.Size.Value;
                pagination.TotalPages = (employees.Count + paginationRequest.Size.Value - 1) / paginationRequest.Size.Value;
                pagination.TotalElements = employees.Count;

                employees = employees.Skip((paginationRequest.Page.Value - 1) * paginationRequest.Size.Value).Take(paginationRequest.Size.Value).ToList();
                pagination.NumberOfElements = employees.Count;
                pagination.Content = employees;
            }
            else
            {
                pagination.TotalElements = employees.Count;
                pagination.Content = employees;
            }
            
            response.Data.Add("pagination", pagination);

            return response;
        }

        #region Basic Info

        public async Task<ApiResponse<Dictionary<string, object>>> GetBasicInfo(int employeeId, ApiRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();
            bool isExist = _employeeRepository.CheckExistEmployeeById(employeeId);
            if (!isExist)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.EMP001
                });
            }
            Core.TbCfgSetting entitySetting = await _entitySettingRepository.GetSettingOfEntity(request.Parameter.ClientId, request.Parameter.EntityId);
            EmployeeViewModel employee = await _employeeRepository.GetBasicInfo(employeeId, entitySetting.PrimaryLanguage, entitySetting.SecondaryLanguage);
            response.Data.Add("employee", employee);

            List<TbEmpAttachment> attachments = await _attachmentRepository.GetByEmployeeId(employee.EmployeeId);
            response.Data.Add("attachments", attachments);

            List<TbEmpEmergencyContactPerson> emergencyContacts = await _emergencyContactRepository.GetByEmployeeId(employee.EmployeeId);
            response.Data.Add("emergencyContacts", emergencyContacts);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> SaveBasicInfo(int employeeId, ApiRequest<EmployeeSaveModel> request, string userName)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();

            List<MessageItem> messages = new List<MessageItem>();
            messages = _validator.Validate<EmployeeRequest>(request.Data.Employee);
            if (messages.Count > 0)
            {
                response.Data = null;
                response.Message.Toast.AddRange(messages);
                return response;
            }

            Core.TbCfgSetting entitySetting = await _entitySettingRepository.GetSettingOfEntity(request.Parameter.ClientId, request.Parameter.EntityId);

            if (employeeId == 0)
            {
                return await CreateBasicInfo(response, request, userName, entitySetting);
            }
            else if (employeeId != 0)
            {
                bool isExist = _employeeRepository.CheckExistEmployeeById(employeeId);
                if (!isExist)
                {
                    response.Message.Toast.Add(new MessageItem
                    {
                        Level = Level.ERROR,
                        LabelCode = Label.EMP001
                    });
                    response.Data = null;
                    return response;
                }
                return await UpdateBasicInfo(employeeId, response, request, userName, entitySetting);
            }
            return response;
        }

        private async Task<ApiResponse<Dictionary<string, object>>> CreateBasicInfo(ApiResponse<Dictionary<string, object>> response,
                                                                    ApiRequest<EmployeeSaveModel> request,
                                                                    string userName,
                                                                    Core.TbCfgSetting entityLanguage)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    TbEmpEmployee employee = await _employeeRepository.Create(request.Data.Employee, userName);

                    List<MessageItem> messageItems = new List<MessageItem>();
                    if (request.Data.Attachments != null && request.Data.Attachments.Count > 0)
                    {
                        // create attachment
                        messageItems = await SaveAttachment(employee.EmployeeId, request.Data.Attachments, userName);
                        if (messageItems.Count > 0)
                        {
                            response.Message.Toast.AddRange(messageItems);
                            response.Data = null;
                            return response;
                        }
                    }

                    if (request.Data.EmergencyContacts != null && request.Data.EmergencyContacts.Count > 0)
                    {
                        //create mergency contact
                        messageItems = await SaveEmergencyContact(employee.EmployeeId, request.Data.EmergencyContacts, userName);
                        if (messageItems.Count > 0)
                        {
                            response.Message.Toast.AddRange(messageItems);
                            response.Data = null;
                            return response;
                        }
                    }

                    request.Data.Employee.EmployeeId = employee.EmployeeId;
                    response.Data.Add("employee", request.Data.Employee);

                    List<TbEmpAttachment> attachments = await _attachmentRepository.GetByEmployeeId(employee.EmployeeId);
                    response.Data.Add("attachments", attachments);

                    List<TbEmpEmergencyContactPerson> emergencyContacts = await _emergencyContactRepository.GetByEmployeeId(employee.EmployeeId);
                    response.Data.Add("emergencyContacts", emergencyContacts);
                    response.Message.Toast.Add(new MessageItem
                    {
                        Level = Level.SUCCESS,
                        LabelCode = Label.EMP003
                    });
                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    _logger.LogError(ex.Message);
                    response.Message.Toast.Add(new MessageItem
                    {
                        Level = Level.ERROR,
                        LabelCode = Label.EMP002
                    });
                    response.Data = null;
                }
            }
            return response;
        }

        private async Task<ApiResponse<Dictionary<string, object>>> UpdateBasicInfo(int employeeId,
                                                                    ApiResponse<Dictionary<string, object>> response,
                                                                    ApiRequest<EmployeeSaveModel> request,
                                                                    string userName,
                                                                    Core.TbCfgSetting entitySetting)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    TbEmpEmployee employee = await _employeeRepository.Update(employeeId, request.Data.Employee, userName);

                    List<MessageItem> messageItems = new List<MessageItem>();
                    if (request.Data.Attachments != null && request.Data.Attachments.Count > 0)
                    {
                        //update attachment
                        messageItems = await SaveAttachment(employee.EmployeeId, request.Data.Attachments, userName);
                        if (messageItems.Count > 0)
                        {
                            response.Message.Toast.AddRange(messageItems);
                            response.Data = null;
                            return response;
                        }
                    }

                    if (request.Data.EmergencyContacts != null && request.Data.EmergencyContacts.Count > 0)
                    {
                        //update emergency contact
                        messageItems = await SaveEmergencyContact(employee.EmployeeId, request.Data.EmergencyContacts, userName);
                        if (messageItems.Count > 0)
                        {
                            response.Message.Toast.AddRange(messageItems);
                            response.Data = null;
                            return response;
                        }
                    }

                    request.Data.Employee.EmployeeId = employee.EmployeeId;
                    response.Data.Add("employee", request.Data.Employee);

                    List<TbEmpAttachment> attachments = await _attachmentRepository.GetByEmployeeId(employee.EmployeeId);
                    response.Data.Add("attachments", attachments);

                    List<TbEmpEmergencyContactPerson> emergencyContacts = await _emergencyContactRepository.GetByEmployeeId(employee.EmployeeId);
                    response.Data.Add("emergencyContacts", emergencyContacts);
                    response.Message.Toast.Add(new MessageItem
                    {
                        Level = Level.SUCCESS,
                        LabelCode = Label.EMP005
                    });
                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    _logger.LogError(ex.Message);
                    response.Message.Toast.Add(new MessageItem
                    {
                        Level = Level.ERROR,
                        LabelCode = Label.EMP004
                    });
                    response.Data = null;
                }
            }
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetProfile(int employeeId, ApiRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();
            EmployeeProfile employeeProfile = await _employeeRepository.GetEmployeeProfile(employeeId, request.Parameter.Locale);
            response.Data.Add("employeeProfile", employeeProfile);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetCompanyInfo(int employeeId, ApiRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();
            CompanyInformation employeeProfile = await _employeeRepository.GetCompanyInfo(employeeId);
            response.Data.Add("companyInfo", employeeProfile);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> SaveCompanyInfo(int employeeId, ApiRequest<CompanyInformation> request, string userName)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();
            bool isSave = await _employeeRepository.UpdateCompanyInfo(employeeId, request.Data, userName);
            if (!isSave)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.COM001
                });
                response.Data = null;
                return response;
            }
            response.Data.Add("companyInfo", request.Data);
            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.COM002
            });
            return response;
        }

        #endregion Basic Info

        public async Task<List<MessageItem>> SaveAttachment(int employeeId, List<AttachmentRequest> attachmentRequests, string userName)
        {
            List<MessageItem> messageItems = new List<MessageItem>();
            MessageItem itemResult = new MessageItem();
            List<AttachmentRequest> listCreate = attachmentRequests.Where(x => x.AttachmentId == 0).ToList();
            List<AttachmentRequest> listUpdate = attachmentRequests.Where(x => x.AttachmentId != 0).ToList();
            List<AttachmentRequest> listDelete = attachmentRequests.Where(x => x.AttachmentId != 0 && x.isDeleted == true).ToList();

            bool isCreate = true;
            bool isUpdate = true;
            bool isDelete = true;
            if (listCreate != null && listCreate.Count > 0)
            {
                
                itemResult = await CreateAttachment(employeeId, listCreate, userName);
                if (itemResult != null)
                    messageItems.Add(itemResult);
            }
            if (listUpdate != null && listUpdate.Count > 0)
            {
                itemResult = await UpdateAttachment(listUpdate, userName);
                if (itemResult != null)
                    messageItems.Add(itemResult);
            }
            if (listDelete != null && listDelete.Count > 0)
            {
                itemResult = await DeleteAttachment(listDelete);
                if (itemResult != null)
                    messageItems.Add(itemResult);
            }
            return messageItems;
        }

        public async Task<MessageItem> CreateAttachment(int employeeId, List<AttachmentRequest> listCreate, string userName)
        {
            try
            {
                await _attachmentRepository.Create(employeeId, listCreate, userName);
                return null;
            }
            catch (Exception ex)
            {
                return new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.ATT001
                };
            }
        }

        public async Task<MessageItem> UpdateAttachment(List<AttachmentRequest> listUpdate, string userName)
        {
            try
            {
                await _attachmentRepository.Update(listUpdate, userName);
                return null;
            }
            catch (Exception ex)
            {
                return new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.ATT002
                };
            }
        }

        public async Task<MessageItem> DeleteAttachment(List<AttachmentRequest> listDelete)
        {
            try
            {
                await _attachmentRepository.Delete(listDelete);
                return null;
            }
            catch (Exception ex)
            {
                return new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.ATT004
                };
            }
        }

        public async Task<List<MessageItem>> SaveEmergencyContact(int employeeId, List<EmergencyContactRequest> emergencyContacts, string userName)
        {
            List<MessageItem> messageItems = new List<MessageItem>();
            string prefixComponent = "emergencyContacts";

            //validate employee property
            for (int i = 0; i < emergencyContacts.Count; i++)
            {
                var resultCheck = new CommonLib.Validation.Validator(null).Validate(emergencyContacts[i], prefixComponent + "[" + i + "].");
                messageItems.AddRange(resultCheck);
            }
            if (messageItems.Count > 0)
            {
                return messageItems;
            }

            List<EmergencyContactRequest> listCreate = emergencyContacts.Where(x => x.EmergencyContactPersonId == 0).ToList();
            List<EmergencyContactRequest> listUpdate = emergencyContacts.Where(x => x.EmergencyContactPersonId != 0).ToList();

            bool isCreate = true;
            bool isUpdate = true;
            if (listCreate != null && listCreate.Count > 0)
            {
                isCreate = await _emergencyContactRepository.Create(employeeId, listCreate, userName);
                if (!isCreate)
                {
                    messageItems.Add(new MessageItem
                    {
                        Level = Level.ERROR,
                        LabelCode = Label.EMER001
                    });
                }
            }
            if (listUpdate != null && listUpdate.Count > 0)
            {
                isUpdate = await _emergencyContactRepository.Update(listUpdate, userName);
                if (!isUpdate)
                {
                    messageItems.Add(new MessageItem
                    {
                        Level = Level.ERROR,
                        LabelCode = Label.EMER004
                    });
                }
            }
            return messageItems;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> NewHire(ApiRequest<NewHireRequest> request, string userName)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();
            List<MessageItem> listValidate = _validator.Validate<NewHireRequest>(request.Data);
            Core.TbCfgSetting entitySetting = await _entitySettingRepository.GetSettingOfEntity(request.Parameter.ClientId, request.Parameter.EntityId);

            if (request.Data.AcceptingTheOffer != null)
            {
                if (request.Data.AcceptingTheOffer.Value > DateTime.Now)
                {
                    listValidate.Add(new MessageItem
                    {
                        Level = Level.ERROR,
                        LabelCode = Label.EMP006
                    });
                }
            }

            if (request.Data.Probation)
            {
                if (request.Data.ExpectedProbationEndDate < (request.Data.ExpectedStartDate.AddDays(entitySetting.ProbationPeriod)))
                {
                    listValidate.Add(new MessageItem
                    {
                        Level = Level.ERROR,
                        LabelCode = Label.EMP007
                    });
                }
            }

            if (listValidate.Count > 0)
            {
                response.Message.Toast.AddRange(listValidate);
                response.Data = null;
                return response;
            }

            TbEmpEmployee employee = new TbEmpEmployee();
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    //create employee
                    EmployeeRequest employeeRequest = new EmployeeRequest
                    {
                        FirstNamePrimary = request.Data.FirstName,
                        MiddleNamePrimary = request.Data.MiddleName,
                        LastNamePrimary = request.Data.LastName,
                        DisplayNamePrimary = request.Data.DisplayName,
                        RemarkEmploymentContract = request.Data.Remark,
                        Photo = request.Data.PhotoId
                    };

                    employee = await _employeeRepository.Create(employeeRequest, userName);
                    if (employee == null)
                    {
                        response.Message.Toast.Add(new MessageItem
                        {
                            Level = Level.ERROR,
                            LabelCode = Label.EMP002
                        });
                        scope.Dispose();
                        response.Data = null;
                        return response;
                    }

                    //create employment contract
                    TbEmpEmploymentContract employmentContractRequest = new TbEmpEmploymentContract
                    {
                        EmployeeId = employee.EmployeeId,
                        StaffType = request.Data.StaffType,
                        ContractNo = request.Data.ContractNo,
                        CreatedBy = userName,
                        CreatedDt = DateTime.UtcNow
                    };
                    TbEmpEmploymentContract employmentContract = await _employmentContractRepository.Create(employmentContractRequest);
                    if (employmentContract == null)
                    {
                        response.Message.Toast.Add(new MessageItem
                        {
                            Level = Level.ERROR,
                            LabelCode = Label.EC0102
                        });
                        scope.Dispose();
                        response.Data = null;
                        return response;
                    }

                    //create tax
                    TbEmpTax taxRequest = new TbEmpTax
                    {
                        EmploymentContractId = employmentContract.EmploymentContractId,
                        CreatedBy = userName,
                        CreatedDt = DateTime.UtcNow
                    };
                    TbEmpTax tax = await _taxRepository.Create(taxRequest);
                    if (tax == null)
                    {
                        response.Message.Toast.Add(new MessageItem
                        {
                            Level = Level.ERROR,
                            LabelCode = Label.TAX002
                        });
                        scope.Dispose();
                        response.Data = null;
                        return response;
                    }

                    //create movement
                    TbEmpMovement movementRequest = new TbEmpMovement
                    {
                        EmploymentContractId = employmentContract.EmploymentContractId,
                        BusinessUnitId = request.Data.BusinessUnitId,
                        WorkCalendar = request.Data.WorkCalendar,
                        DefaultSalaryCurrency = request.Data.DefaultSalaryCurrency,
                        DefaultPaymentCurrency = request.Data.DefaultPaymentCurrency,
                        MovementName = "First movement",
                        AssignmentStartDate = request.Data.ExpectedStartDate,
                        CreatedBy = userName,
                        CreatedDt = DateTime.UtcNow
                    };
                    TbEmpMovement movement = await _movementRepository.Create(movementRequest);
                    if (movement == null)
                    {
                        response.Message.Toast.Add(new MessageItem
                        {
                            Level = Level.ERROR,
                            LabelCode = Label.MO0125
                        });
                        scope.Dispose();
                        response.Data = null;
                        return response;
                    }

                    //create employment status
                    TbEmpEmploymentStatus acceptedOfferStatus = null;
                    if (request.Data.AcceptingTheOffer != null)
                    {
                        acceptedOfferStatus = new TbEmpEmploymentStatus
                        {
                            EmploymentContractId = employmentContract.EmploymentContractId,
                            StartDate = request.Data.AcceptingTheOffer.Value,
                            Confirmed = true,
                            CreatedBy = userName,
                            CreatedDt = DateTime.UtcNow,
                            Status = EmploymentStatus.ACCEPTED_OFFER
                        };
                    }

                    TbEmpEmploymentStatus underProbationStatus = null;
                    TbEmpEmploymentStatus activeStatus = null;
                    if (!request.Data.Probation)
                    {
                        activeStatus = new TbEmpEmploymentStatus
                        {
                            EmploymentContractId = employmentContract.EmploymentContractId,
                            StartDate = request.Data.ExpectedStartDate,
                            CreatedBy = userName,
                            CreatedDt = DateTime.UtcNow,
                            Status = EmploymentStatus.ACTIVE
                        };
                        if (request.Data.ExpectedStartDate <= DateTime.Today)
                        {
                            activeStatus.Confirmed = true;
                        }
                        else activeStatus.Confirmed = false;
                    }
                    else
                    {
                        underProbationStatus = new TbEmpEmploymentStatus
                        {
                            EmploymentContractId = employmentContract.EmploymentContractId,
                            StartDate = request.Data.ExpectedStartDate,
                            CreatedBy = userName,
                            CreatedDt = DateTime.UtcNow,
                            Status = EmploymentStatus.UNDER_PROBATION
                        };
                        if (request.Data.ExpectedStartDate <= DateTime.Today)
                        {
                            underProbationStatus.Confirmed = true;
                        }
                        else underProbationStatus.Confirmed = false;

                        activeStatus = new TbEmpEmploymentStatus
                        {
                            EmploymentContractId = employmentContract.EmploymentContractId,
                            StartDate = request.Data.ExpectedProbationEndDate.AddDays(1),
                            CreatedBy = userName,
                            CreatedDt = DateTime.UtcNow,
                            Status = EmploymentStatus.ACTIVE
                        };
                        if (request.Data.ExpectedProbationEndDate <= DateTime.Today)
                        {
                            activeStatus.Confirmed = true;
                        }
                        else activeStatus.Confirmed = false;
                    }

                    List<TbEmpEmploymentStatus> employmentStatuses = new List<TbEmpEmploymentStatus>();
                    if (acceptedOfferStatus != null) employmentStatuses.Add(acceptedOfferStatus);
                    if (underProbationStatus != null) employmentStatuses.Add(underProbationStatus);
                    if (activeStatus != null) employmentStatuses.Add(activeStatus);
                    bool isCreated = await _employmentStatusRepository.CreateMany(employmentStatuses);
                    if (!isCreated)
                    {
                        response.Message.Toast.Add(new MessageItem
                        {
                            Level = Level.ERROR,
                            LabelCode = Label.ES0102
                        });
                        scope.Dispose();
                        response.Data = null;
                        return response;
                    }

                    scope.Complete();
                }
                catch (Exception)
                {
                    scope.Dispose();
                    response.Message.Toast.Add(new MessageItem
                    {
                        Level = Level.ERROR,
                        LabelCode = Label.EMP002
                    });
                    scope.Dispose();
                    response.Data = null;
                }
            }

            //create user
            bool isCreatedUser = await _employeeRepository.CreateCoreUser(employee, request);
            if (!isCreatedUser)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.USR0103
                });
                response.Data = null;
                return response;
            }
            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.EMP003
            });
            response.Data.Add("employeeId", employee.EmployeeId);
            return response;
        }
        public async Task<ApiResponse<int>> CountByStatus(string status)
        {
            var response = new ApiResponse<int>();
            string language = "en";
            int count = 0;
            var employeeViewList = await _employeeRepository.GetAll(language);
            count = employeeViewList.Count(x => x.Status == status);
            response.Data = count;
            return response;
        }
    }
}