﻿using AutoMapper;
using CommonLib.Constants;
using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Client;
using CommonLib.Models.Core;
using CommonLib.Services;
using CommonLib.Validation;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Constants;
using EmploymentServiceAPIs.Employee.Dtos.Movement;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using EmploymentServiceAPIs.Employee.Services.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Services.Impl
{
    public class MovermentService : IMovementService
    {
        private readonly IMovementRepository _movementRepository;
        private readonly IEmploymentContractRepository _ecRepository;
        private readonly IMapper _mapper;
        private readonly IValidator _validator;
        protected readonly IConnectionStringContainer _connectionStringRepository;
        protected string _connectionString;
        protected readonly CoreDbContext _coreDbContext;
        private readonly ILogger<MovermentService> _logger;

        public MovermentService(IEmploymentContractRepository ecRepository, IMovementRepository movementRepository, IMapper mapper, IValidator validator, IConnectionStringContainer connectionStringRepository, CoreDbContext coreDBContext, ILogger<EmploymentContractService> logger)
        {
            _ecRepository = ecRepository;
            _movementRepository = movementRepository;
            _connectionStringRepository = connectionStringRepository;
            _mapper = mapper;
            _validator = validator;
            _connectionString = _connectionStringRepository.GetConnectionString();
        }

        protected TenantDBContext CreateContext(string _connectionString)
        {
            return new TenantDBContext(_connectionString);
        }

        public async Task<ApiResponse<Dictionary<string, object>>> Save(int employmentId, string ray, string username, List<MovementModel> movementAms)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>
            {
                Ray = ray ?? string.Empty,
                Data = new Dictionary<string, object>(),
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                    ValidateResult = new List<MessageItem>()
                }
            };

            //check exist employment
            var isExist = _ecRepository.CheckExistEContractById(employmentId);
            if (!isExist)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.MO0125
                });
                response.Message.ValidateResult.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.EC0122
                });
                response.Data = null;
                return response;
            }

            List<MessageItem> messages = new List<MessageItem>();

            for (int i = 0; i < movementAms.Count; i++)
            {
                var messageMovement = _validator.Validate(movementAms[i]);
                for (int j = 0; j < messageMovement.Count; j++)
                {
                    messageMovement[j].ComponentId = $"Movement[{i}]_{messageMovement[j].ComponentId}";
                }
                messages.AddRange(messageMovement);
            }

            if (messages.Count > 0)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.MO0125
                });
                response.Message.ValidateResult = messages;
                response.Data = null;
                return response;
            }
            using (var _context = CreateContext(_connectionString))
            {
                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    try
                    {
                        var lstIdNotExist = new List<int>();
                        var lstMovementCreate = movementAms.Where(x => x.AssignmentId == 0).ToList();
                        var lstMovementUpdate = movementAms.Where(x => x.AssignmentId != 0).ToList();
                        DateTime currentDate = DateTime.Now;
                        //create movements
                        if (lstMovementCreate.Count > 0)
                        {
                            for (int i = 0; i < lstMovementCreate.Count; i++)
                            {
                                lstMovementCreate[i].EmploymentContractId = employmentId;
                                lstMovementCreate[i].CreatedBy = username;
                                lstMovementCreate[i].CreatedDt = currentDate;
                                lstMovementCreate[i].ModifiedBy = username;
                                lstMovementCreate[i].ModifiedDt = currentDate;
                            }
                            var movements = _mapper.Map<List<MovementModel>, List<TbEmpMovement>>(lstMovementCreate);
                            //await _movementRepository.Create(movements, _context);
                        }
                        //update movements
                        if (lstMovementUpdate.Count > 0)
                        {
                            for (int i = 0; i < lstMovementUpdate.Count; i++)
                            {
                                var movementEntity = await _movementRepository.GetById(lstMovementUpdate[i].AssignmentId);
                                if (movementEntity == null)
                                {
                                    lstIdNotExist.Add(lstMovementUpdate[i].AssignmentId);
                                }
                                else
                                {
                                    lstMovementCreate[i].EmploymentContractId = movementEntity.EmploymentContractId;
                                    lstMovementCreate[i].CreatedBy = movementEntity.CreatedBy;
                                    lstMovementCreate[i].CreatedDt = movementEntity.CreatedDt;
                                    lstMovementCreate[i].ModifiedBy = username;
                                    lstMovementCreate[i].ModifiedDt = currentDate;
                                }
                            }

                            if (lstIdNotExist.Count > 0)
                            {
                                await transaction.RollbackAsync();
                                response.Message.Toast.Add(new MessageItem
                                {
                                    Level = Level.ERROR,
                                    LabelCode = Label.MO0125
                                });

                                response.Message.ValidateResult.Add(new MessageItem
                                {
                                    Level = Level.ERROR,
                                    LabelCode = Label.MO0128
                                });

                                response.Data.Add("ids", lstIdNotExist);
                                return response;
                            }
                            var movements = _mapper.Map<List<MovementModel>, List<TbEmpMovement>>(lstMovementUpdate);
                            //await _movementRepository.Update(movements, _context);
                        }
                        await transaction.CommitAsync();

                        response.Message.Toast.Add(new MessageItem
                        {
                            Level = Level.SUCCESS,
                            LabelCode = Label.MO0126
                        });

                        var movementVms = await GetByEmploymentId(employmentId, ray);
                        response.Data.Add("movements", movementVms.Data);

                        return response;
                    }
                    catch (Exception ex)
                    {
                        await transaction.RollbackAsync();
                        _logger.LogWarning(ex.ToString());
                        throw ex;
                    }
                }
            }
        }

        public async Task<ApiResponse<List<MovementViewModel>>> GetByEmploymentId(int employmentId, string ray)
        {
            ApiResponse<List<MovementViewModel>> response = new ApiResponse<List<MovementViewModel>>
            {
                Ray = ray ?? string.Empty,
                Data = new List<MovementViewModel>()
            };
            var movements = await _movementRepository.GetByEmploymentId(employmentId);
            var movementVms = _mapper.Map<List<TbEmpMovement>, List<MovementViewModel>>(movements);
            response.Data = movementVms;
            return response;
        }

        public async Task<ApiResponse<MovementViewModel>> GetById(int id, string ray)
        {
            ApiResponse<MovementViewModel> response = new ApiResponse<MovementViewModel>
            {
                Ray = ray ?? string.Empty,
                Data = new MovementViewModel()
            };
            var movement = await _movementRepository.GetById(id);
            var movementVm = _mapper.Map<TbEmpMovement, MovementViewModel>(movement);
            response.Data = movementVm;
            return response;
        }
    }
}