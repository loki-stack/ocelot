﻿using AutoMapper;
using CommonLib.Constants;
using CommonLib.Dtos;
using CommonLib.EmailTemplate;
using CommonLib.Models;
using CommonLib.Repositories.Interfaces;
using CommonLib.Services;
using CommonLib.Models.Client;
using CommonLib.Validation;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Constants;
using EmploymentServiceAPIs.Employee.Dtos.BankAccount;
using EmploymentServiceAPIs.Employee.Dtos.Employee;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using EmploymentServiceAPIs.Employee.Services.Interfaces;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using CommonLib.Models.Core;

namespace EmploymentServiceAPIs.Employee.Services.Impl
{
    public class BankAccountService : IBankAccountService
    {
        private readonly IBankAccountRepository _bankAccountRepository;
        private readonly IEntitySettingRepository _entitySettingRepository;
        private readonly ISendEmailService _sendEmailService;
        private readonly IMapper _mapper;
        private readonly IValidator _validator;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly SmtpSettings _smtpSettings;
        private readonly MailSettings _mailSettings;

        public BankAccountService(IBankAccountRepository bankAccountRepository, 
                                  IEmployeeRepository employeeRepository, 
                                  IMapper mapper, 
                                  IValidator validator,
                                  IEntitySettingRepository entitySettingRepository,
                                  ISendEmailService sendEmailService,
                                  IOptions<SmtpSettings> smtpSettings,
                                  IOptions<MailSettings> mailSettings)
        {
            _bankAccountRepository = bankAccountRepository;
            _employeeRepository = employeeRepository;
            _mapper = mapper;
            _validator = validator;
            _entitySettingRepository = entitySettingRepository;
            _sendEmailService = sendEmailService;
            _smtpSettings = smtpSettings.Value;
            _mailSettings = mailSettings.Value;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> Save(int employeeId, ApiRequest<BankAccountSaveModel> request, string username)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new Dictionary<string, object>(),
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                    ValidateResult = new List<MessageItem>()
                }
            };

            BankAccountSaveModel bankAccountSM = request.Data;

            //validate data
            response = Validate(request.Ray, employeeId, bankAccountSM);
            if (response.Message.ValidateResult.Count > 0)
            {
                return response;
            }

            var lstIdNotExist = new List<int>();
            var lstBankAccountCreate = bankAccountSM.BankAccounts.Where(x => x.Id == 0).ToList();
            var lstBankAccountUpdate = bankAccountSM.BankAccounts.Where(x => x.Id != 0).ToList();
            DateTime currentDate = DateTime.Now;
            bool isSendEmail = false;
            TbCfgSetting setting = await _entitySettingRepository.GetSettingOfEntity(request.Parameter.ClientId, request.Parameter.EntityId);

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    //Update remark bank account in Employee
                    var employee = await _employeeRepository.GetById(employeeId);
                    employee.RemarkBankAccount = bankAccountSM.Remark;
                    employee.ModifiedBy = username;
                    employee.ModifiedDt = currentDate;
                    await _employeeRepository.Edit(employee);

                    //Create bank account
                    if (lstBankAccountCreate.Count > 0)
                    {
                        for (int i = 0; i < lstBankAccountCreate.Count; i++)
                        {
                            lstBankAccountCreate[i].EmployeeId = employeeId;
                            lstBankAccountCreate[i].CreatedBy = username;
                            lstBankAccountCreate[i].CreatedDt = currentDate;
                            lstBankAccountCreate[i].ModifiedBy = username;
                            lstBankAccountCreate[i].ModifiedDt = currentDate;
                        }
                        var bankAccounts = _mapper.Map<List<BankAccountModel>, List<TbEmpEeBankAccount>>(lstBankAccountCreate);
                        await _bankAccountRepository.Create(bankAccounts);
                        isSendEmail = true;
                    }

                    //Update bank account
                    if (lstBankAccountUpdate.Count > 0)
                    {
                        for (int i = 0; i < lstBankAccountUpdate.Count; i++)
                        {
                            var bankAccountEntity = await _bankAccountRepository.GetById(lstBankAccountUpdate[i].Id);
                            if (bankAccountEntity == null)
                            {
                                lstIdNotExist.Add(lstBankAccountUpdate[i].Id);
                            }
                            else
                            {
                                lstBankAccountUpdate[i].EmployeeId = bankAccountEntity.EmployeeId;
                                lstBankAccountUpdate[i].CreatedBy = bankAccountEntity.CreatedBy;
                                lstBankAccountUpdate[i].CreatedDt = bankAccountEntity.CreatedDt;
                                lstBankAccountUpdate[i].ModifiedBy = username;
                                lstBankAccountUpdate[i].ModifiedDt = currentDate;
                            }
                        }

                        if (lstIdNotExist.Count > 0)
                        {
                            response.Message.Toast.Add(new MessageItem
                            {
                                Level = Level.ERROR,
                                LabelCode = Label.BA0116
                            });

                            response.Message.ValidateResult.Add(new MessageItem
                            {
                                Level = Level.ERROR,
                                LabelCode = Label.BA0120
                            });

                            scope.Dispose();

                            response.Data.Add("ids", lstIdNotExist);
                            return response;
                        }
                        var bankAccounts = _mapper.Map<List<BankAccountModel>, List<TbEmpEeBankAccount>>(lstBankAccountUpdate);
                        await _bankAccountRepository.Update(bankAccounts);
                        isSendEmail = true;
                    }

                    response.Message.Toast.Add(new MessageItem
                    {
                        Level = Level.SUCCESS,
                        LabelCode = Label.BA0117
                    });

                    var bankAccountVms = await GetByEmployeeId(employeeId, request.Ray);
                    response.Data.Add("bankAccounts", bankAccountVms.Data);
                    scope.Complete();     
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }

            if (setting != null)
            {
                if (!string.IsNullOrEmpty(setting.NotificationEmail) && isSendEmail)
                {
                    var isSuccess = await SendEmailToNotice(setting.NotificationEmail, username, employeeId, request);
                    if (!isSuccess)
                    {
                        response.Message.Toast.Add(new MessageItem
                        {
                            Level = Level.ERROR,
                            LabelCode = Label.BA0121
                        });
                    }
                }
            }
            return response;
        }

        public async Task<ApiResponse<BankAccountDto>> GetByEmployeeId(int employeeId, string ray)
        {
            ApiResponse<BankAccountDto> response = new ApiResponse<BankAccountDto>
            {
                Ray = ray ?? string.Empty,
                Data = new BankAccountDto()
            };
            string[] includes = { "TbEmpEeBankAccount" };
            var employee = await _employeeRepository.GetSingleByCondition(x => x.EmployeeId == employeeId, includes);
            var bankAccountVms = _mapper.Map<TbEmpEmployee, BankAccountDto>(employee);
            response.Data = bankAccountVms;
            return response;
        }

        public async Task<ApiResponse<BankAccountViewModel>> GetById(int id, string ray)
        {
            ApiResponse<BankAccountViewModel> response = new ApiResponse<BankAccountViewModel>
            {
                Ray = ray ?? string.Empty,
                Data = new BankAccountViewModel()
            };
            var bankAccount = await _bankAccountRepository.GetById(id);
            var bankAccountVm = _mapper.Map<TbEmpEeBankAccount, BankAccountViewModel>(bankAccount);
            response.Data = bankAccountVm;
            return response;
        }

        private ApiResponse<Dictionary<string, object>> Validate(string ray, int employeeId, BankAccountSaveModel bankAccountSM)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>
            {
                Ray = ray ?? string.Empty,
                Data = new Dictionary<string, object>(),
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                    ValidateResult = new List<MessageItem>()
                }
            };
            //check exist employee
            var isExist = _employeeRepository.CheckExistEmployeeById(employeeId);
            if (!isExist)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.BA0116
                });
                response.Message.ValidateResult.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.EMP001
                });
                response.Data = null;
                return response;
            }

            //validate data
            List<MessageItem> messages = new List<MessageItem>();
            messages = _validator.Validate(bankAccountSM);

            for (int i = 0; i < bankAccountSM.BankAccounts.Count; i++)
            {
                var messageBankAccount = _validator.Validate(bankAccountSM.BankAccounts[i]);
                for (int j = 0; j < messageBankAccount.Count; j++)
                {
                    messageBankAccount[j].ComponentId = $"bankAccount[{i}]_{messageBankAccount[j].ComponentId}";
                }
                messages.AddRange(messageBankAccount);
            }

            //return respone when data invalid
            if (messages.Count > 0)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.BA0116
                });
                response.Message.ValidateResult = messages;
                response.Data = null;
                return response;
            }

            return response;
        }

        private async Task<bool> SendEmailToNotice(string notificationEmail, string userName, int employeeId, ApiRequest<BankAccountSaveModel> request)
        {
            TbMsgMessage message = new TbMsgMessage
            {
                MessageType = CommonLib.Constants.Messages.MessageTemplateType.Email,
                Recipient = notificationEmail,
                ProcessedDt = DateTime.UtcNow,
                Status = CommonLib.Constants.Messages.MessageStatus.NEW,
                Portal = CommonLib.Constants.Messages.Portal.EE,
                FlagHtml = true,
                AttemptCount = 0,
                UserId = employeeId,
                CreatedBy = userName,
                CreatedDt = DateTime.UtcNow
            };
            EmployeeProfile employee = await _employeeRepository.GetEmployeeProfile(employeeId, request.Parameter.Locale);
            EML002 eml002 = new EML002
            {
                EmployeeName = employee.Name
            };
            var isSuccess = await _sendEmailService.Send(eml002, _smtpSettings, _mailSettings, request.Parameter, message);
            return isSuccess;
        }
    }
}