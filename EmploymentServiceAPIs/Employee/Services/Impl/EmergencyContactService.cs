﻿using AutoMapper;
using CommonLib.Constants;
using CommonLib.Dtos;
using CommonLib.Models.Client;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Constants;
using EmploymentServiceAPIs.Employee.Dtos.EmergencyContact;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using EmploymentServiceAPIs.Employee.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Services.Impl
{
    public class EmergencyContactService : IEmergencyContactService
    {
        private readonly IEmergencyContactRepository _emergencyContactRepository;

        public EmergencyContactService(IEmergencyContactRepository emergencyContactRepository, IMapper mapper)
        {
            _emergencyContactRepository = emergencyContactRepository;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetEmergencyContact(int employeeId, ApiRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();
            List<TbEmpEmergencyContactPerson> emergencyContacts = await _emergencyContactRepository.GetByEmployeeId(employeeId);
            response.Data.Add("emergencyContacts", emergencyContacts);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> SaveEmergencyContact(int employeeId, ApiRequest<List<EmergencyContactRequest>> request, string userName)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();
            string prefixComponent = "emergencyContacts";
            List<MessageItem> listResultCheck = new List<MessageItem>();

            //validate employee property
            for (int i = 0; i < request.Data.Count; i++)
            {
                var resultCheck = new CommonLib.Validation.Validator(null).Validate(request.Data[i], prefixComponent + "[" + i + "].");
                listResultCheck.AddRange(resultCheck);
            }
            if (listResultCheck.Count > 0)
            {
                response.Message.Toast.AddRange(listResultCheck);
                response.Data = null;
                return response;
            }

            List<EmergencyContactRequest> listCreate = request.Data.Where(x => x.EmergencyContactPersonId == 0).ToList();
            List<EmergencyContactRequest> listUpdate = request.Data.Where(x => x.EmergencyContactPersonId != 0).ToList();

            bool isCreate = true;
            bool isUpdate = true;
            if (listCreate != null && listCreate.Count > 0)
            {
                isCreate = await _emergencyContactRepository.Create(employeeId, listCreate, userName);
                if (!isCreate)
                {
                    response.Message.Toast.Add(new MessageItem
                    {
                        Level = Level.ERROR,
                        LabelCode = Label.EMER001
                    });
                }
            }
            if (listUpdate != null && listUpdate.Count > 0)
            {
                isUpdate = await _emergencyContactRepository.Update(listUpdate, userName);
                if (!isUpdate)
                {
                    response.Message.Toast.Add(new MessageItem
                    {
                        Level = Level.ERROR,
                        LabelCode = Label.EMER004
                    });
                }
            }
            if (!isCreate || !isUpdate)
            {
                response.Data = null;
                return response;
            }
            List<TbEmpEmergencyContactPerson> emergencyContacts = await _emergencyContactRepository.GetByEmployeeId(employeeId);
            response.Data.Add("emergencyContacts", emergencyContacts);
            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.EMER006
            });
            return response;
        }

    }
}
