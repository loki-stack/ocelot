﻿using AutoMapper;
using CommonLib.Constants;
using CommonLib.Dtos;
using CommonLib.Models.Client;
using CommonLib.Validation;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Constants;
using EmploymentServiceAPIs.Employee.Dtos.EmploymentContract;
using EmploymentServiceAPIs.Employee.Dtos.Movement;
using EmploymentServiceAPIs.Employee.Dtos.TaxArrangement;
using EmploymentServiceAPIs.Employee.Dtos.Termination;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using EmploymentServiceAPIs.Employee.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace EmploymentServiceAPIs.Employee.Services.Impl
{
    public class EmploymentContractService : IEmploymentContractService
    {
        private readonly IEmploymentContractRepository _ecRepository;
        private readonly IMovementRepository _movementRepository;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly ITaxRepository _taxRepository;
        private readonly IEmploymentStatusRepository _employmentStatusRepository;
        private readonly ITerminationRepository _terminationRepository;
        private readonly IMapper _mapper;
        private readonly IValidator _validator;

        public EmploymentContractService(IEmploymentContractRepository ecRepository,
                                         IMovementRepository movementRepository,
                                         IEmployeeRepository employeeRepository,
                                         ITaxRepository taxRepository,
                                         IEmploymentStatusRepository employmentStatusRepository,
                                         ITerminationRepository terminationRepository,
                                         IMapper mapper,
                                         IValidator validator)
        {
            _ecRepository = ecRepository;
            _movementRepository = movementRepository;
            _employeeRepository = employeeRepository;
            _taxRepository = taxRepository;
            _employmentStatusRepository = employmentStatusRepository;
            _terminationRepository = terminationRepository;
            _mapper = mapper;
            _validator = validator;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> Save(int employeeId, string ray, string username, EmploymentContractSaveModel employmentContractSM)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>
            {
                Ray = ray ?? string.Empty,
                Data = new Dictionary<string, object>(),
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                    Alert = new List<MessageItem>(),
                    ValidateResult = new List<MessageItem>()
                }
            };
            //Validate data
            response = await Validate(ray, employeeId, employmentContractSM);
            if (response.Message.ValidateResult.Count > 0 || response.Message.Alert.Count > 0)
            {
                return response;
            }

            //var lstContractCreate = employmentContractSM.EmploymentContracts.Where(x => x.EmploymentContractId == 0).ToList();
            //var lstContractUpdate = employmentContractSM.EmploymentContracts.Where(x => x.EmploymentContractId != 0).ToList();
            //var lstMovementCreate = employmentContractSM.Movements.Where(x => x.AssignmentId == 0).ToList();
            //var lstMovementUpdate = employmentContractSM.Movements.Where(x => x.AssignmentId != 0).ToList();

            //var lstMovementCreateHaveContract = lstMovementCreate.Where(x => x.EmploymentContractId != 0).ToList();
            //var lstMovementCreateNonContract = lstMovementCreate.Where(x => x.EmploymentContractId == 0).ToList();

            //var lstMovementUpdateHaveContract = lstMovementUpdate.Where(x => x.EmploymentContractId != 0).ToList();
            //var lstMovementUpdateNonContract = lstMovementUpdate.Where(x => x.EmploymentContractId == 0).ToList();

            //DateTime currentDate = DateTime.Now;
            ////Update remark contract, movement in Employee
            //var employee = await _employeeRepository.GetById(employeeId);
            //employee.RemarkEmploymentContract = employmentContractSM.RemarkEmploymentContract;
            //employee.RemarkEmploymentMovement = employmentContractSM.RemarkMovement;
            //employee.ModifiedBy = username;
            //employee.ModifiedDt = currentDate;
            //await _employeeRepository.Edit(employee);

            //create employment contract
            //if (lstContractCreate.Count > 0)
            //{
            //    await CreateContract(employeeId, username, currentDate, lstContractCreate);
            //}

            //update employment contract
            //if (lstContractUpdate.Count > 0)
            //{
            //    await UpdateContract(employeeId, username, currentDate, lstContractUpdate);
            //}

            //get contracts by employeeId
            //var contracts = await _ecRepository.GetByEmployeeId(employeeId);

            //create movement have contract
            //if (lstMovementCreateHaveContract.Count > 0)
            //{
            //    await CreateMovement(username, currentDate, lstMovementCreateHaveContract);
            //}

            //update movement have contract
            //if (lstMovementUpdateHaveContract.Count > 0)
            //{
            //    await UpdateMovement(username, currentDate, lstMovementUpdateHaveContract);
            //}

            //create movement non contract
            //if (lstMovementCreateNonContract.Count > 0)
            //{
            //    foreach (var item in lstMovementCreateNonContract)
            //    {
            //        item.EmploymentContractId = contracts.FirstOrDefault(x => x.ContractNo == item.ContractNo).EmploymentContractId;
            //    }

            //    await CreateMovement(username, currentDate, lstMovementCreateNonContract);
            //}

            //update movement non contract
            //if (lstMovementUpdateNonContract.Count > 0)
            //{
            //    foreach (var item in lstMovementUpdateNonContract)
            //    {
            //        item.EmploymentContractId = contracts.FirstOrDefault(x => x.ContractNo == item.ContractNo).EmploymentContractId;
            //    }

            //    await UpdateMovement(username, currentDate, lstMovementUpdateNonContract);
            //}

            var empContractEntities = await _ecRepository.GetByEmployeeId(employeeId);
            var terminationEntities = await _terminationRepository.GetByEmployeeId(employeeId);
            var movementEntities = await _movementRepository.GetByEmployeeId(employeeId);
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    DateTime currentDate = DateTime.Now;
                    //Update remark contract, movement in Employee
                    var employee = await _employeeRepository.GetById(employeeId);
                    employee.RemarkEmploymentContract = employmentContractSM.RemarkEmploymentContract;
                    employee.RemarkEmploymentMovement = employmentContractSM.RemarkMovement;
                    employee.ModifiedBy = username;
                    employee.ModifiedDt = currentDate;
                    await _employeeRepository.Edit(employee);

                    List<TbEmpEmploymentContract> contracts = new List<TbEmpEmploymentContract>();
                    List<TbEmpTermination> terminations = new List<TbEmpTermination>();
                    foreach (var contractSM in employmentContractSM.EmploymentContracts)
                    {
                        var contractEntity = empContractEntities.FirstOrDefault(x => x.EmploymentContractId == contractSM.EmploymentContractId);
                        var contract = _mapper.Map<EmploymentContractModel, TbEmpEmploymentContract>(contractSM);
                        contract.CreatedBy = contractEntity.CreatedBy;
                        contract.CreatedDt = contractEntity.CreatedDt;
                        contract.EmployeeId = contractEntity.EmployeeId;
                        contract.ModifiedBy = username;
                        contract.ModifiedDt = currentDate;
                        contracts.Add(contract);

                        if (contractSM.Termination != null)
                        {
                            var terminationEntity = terminationEntities.FirstOrDefault(x => x.Id == contractSM.Termination.Id);
                            contractSM.Termination.EmploymentContractId = terminationEntity.EmploymentContractId;
                            contractSM.Termination.CreatedBy = terminationEntity.CreatedBy;
                            contractSM.Termination.CreatedDt = terminationEntity.CreatedDt;
                            contractSM.Termination.ModifiedBy = username;
                            contractSM.Termination.ModifiedDt = currentDate;
                            var termination = _mapper.Map<TerminationSaveModel, TbEmpTermination>(contractSM.Termination);

                            terminations.Add(termination);
                        }
                    }
                    //update employment contract
                    await _ecRepository.Update(contracts);
                    //update termination
                    await _terminationRepository.UpdateMany(terminations);
                    //create update movement
                    var lstMovementCreate = employmentContractSM.Movements.Where(x => x.AssignmentId == 0).ToList();
                    var lstMovementUpdate = employmentContractSM.Movements.Where(x => x.AssignmentId != 0).ToList();
                    //create movement
                    if (lstMovementCreate.Count > 0)
                    {
                        List<TbEmpMovement> movementCreates = new List<TbEmpMovement>();
                        foreach (var movementSM in lstMovementCreate)
                        {
                            var movement = _mapper.Map<MovementModel, TbEmpMovement>(movementSM);
                            movement.EmploymentContractId = movementSM.EmploymentContractId;
                            movement.CreatedBy = username;
                            movement.CreatedDt = currentDate;
                            movement.ModifiedBy = username;
                            movement.ModifiedDt = currentDate;
                            movementCreates.Add(movement);
                        }
                        await _movementRepository.CreateMany(movementCreates);
                    }

                    //update movement
                    if (lstMovementUpdate.Count > 0)
                    {
                        List<TbEmpMovement> movementUpdates = new List<TbEmpMovement>();
                        foreach (var movementSM in lstMovementUpdate)
                        {
                            var movementEntity = movementEntities.FirstOrDefault(x => x.AssignmentId == movementSM.AssignmentId);
                            var movement = _mapper.Map<MovementModel, TbEmpMovement>(movementSM);
                            movement.EmploymentContractId = movementEntity.EmploymentContractId;
                            movement.CreatedBy = movementEntity.CreatedBy;
                            movement.CreatedDt = movementEntity.CreatedDt;
                            movement.ModifiedBy = username;
                            movement.ModifiedDt = currentDate;
                            movementUpdates.Add(movement);
                        }
                        await _movementRepository.UpdateMany(movementUpdates);
                    }

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }

            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.EC0101
            });

            var contractVms = await GetByEmployeeId(employeeId, ray);
            response.Data.Add("contracts", contractVms.Data);

            return response;
        }

        private async Task<ApiResponse<Dictionary<string, object>>> Validate(string ray, int employeeId, EmploymentContractSaveModel employmentContractSM)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>
            {
                Ray = ray ?? string.Empty,
                Data = new Dictionary<string, object>(),
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                    Alert = new List<MessageItem>(),
                    ValidateResult = new List<MessageItem>()
                }
            };

            List<MessageItem> messages = new List<MessageItem>();
            //validate employment contract
            for (int i = 0; i < employmentContractSM.EmploymentContracts.Count; i++)
            {
                var messageContract = _validator.Validate(employmentContractSM.EmploymentContracts[i]);
                if (messageContract != null)
                {
                    for (int j = 0; j < messageContract.Count; j++)
                    {
                        messageContract[j].ComponentId = $"employmentContract[{i}]_{messageContract[j].ComponentId}";
                    }
                    messages.AddRange(messageContract);
                }

                //validate termination
                if (employmentContractSM.EmploymentContracts[i].Termination != null)
                {
                    var messageTermination = _validator.Validate(employmentContractSM.EmploymentContracts[i].Termination);
                    if (messageTermination != null)
                    {
                        for (int j = 0; j < messageTermination.Count; j++)
                        {
                            messageTermination[j].ComponentId = $"employmentContract[{i}]_{messageTermination[j].ComponentId}";
                        }
                        messages.AddRange(messageTermination);
                    }
                }
            }

            //validate movement
            for (int i = 0; i < employmentContractSM.Movements.Count; i++)
            {
                var message = _validator.Validate(employmentContractSM.Movements[i]);
                if (message != null)
                {
                    for (int j = 0; j < message.Count; j++)
                    {
                        message[j].ComponentId = $"movement[{i}]_{message[j].ComponentId}";
                    }
                    messages.AddRange(message);
                }

                if (i == 0)
                {
                    if (employmentContractSM.Movements[i].BusinessUnitId == null)
                    {
                        messages.Add(new MessageItem()
                        {
                            ComponentId = $"movement[{i}]_{Component.BUSINESS_UNIT_ID}",
                            Level = Level.ERROR,
                            LabelCode = Label.MO0101
                        });
                    }

                    if (employmentContractSM.Movements[i].WorkCalendar == null)
                    {
                        messages.Add(new MessageItem()
                        {
                            ComponentId = $"movement[{i}]_{Component.WORK_CALENDAR}",
                            Level = Level.ERROR,
                            LabelCode = Label.MO0111
                        });
                    }

                    if (string.IsNullOrEmpty(employmentContractSM.Movements[i].DefaultSalaryCurrency))
                    {
                        messages.Add(new MessageItem()
                        {
                            ComponentId = $"movement[{i}]_{Component.DEFAULT_SALARY_CURRENCY}",
                            Level = Level.ERROR,
                            LabelCode = Label.MO0118
                        });
                    }

                    if (string.IsNullOrEmpty(employmentContractSM.Movements[i].DefaultPaymentCurrency))
                    {
                        messages.Add(new MessageItem()
                        {
                            ComponentId = $"movement[{i}]_{Component.DEFAULT_PAYMENT_CURRENCY}",
                            Level = Level.ERROR,
                            LabelCode = Label.MO0120
                        });
                    }
                }
            }

            //return respone when data invalid
            if (messages.Count > 0)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.EC0102
                });
                response.Message.ValidateResult = messages;
                response.Data = null;
                return response;
            }

            //check exist employee
            var isExist = _employeeRepository.CheckExistEmployeeById(employeeId);
            if (!isExist)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.EMP001
                });
                response.Data = null;
                return response;
            }

            List<MessageItem> alert = new List<MessageItem>();
            //check exist employement contract
            for (int i = 0; i < employmentContractSM.EmploymentContracts.Count; i++)
            {
                if (employmentContractSM.EmploymentContracts[i].EmploymentContractId != 0)
                {
                    isExist = _ecRepository.CheckExistEContractById(employmentContractSM.EmploymentContracts[i].EmploymentContractId);
                    if (!isExist)
                    {
                        alert.Add(new MessageItem
                        {
                            ComponentId = $"employmentContract[{i}]",
                            Level = Level.ERROR,
                            LabelCode = Label.EMP0143,
                            Placeholder = new string[] { $"{i}" }
                        });
                    }
                }
                if (employmentContractSM.EmploymentContracts[i].Termination != null)
                {
                    var termination = employmentContractSM.EmploymentContracts[i].Termination;
                    isExist = _terminationRepository.CheckExistTermination(termination.Id);
                    if (!isExist)
                    {
                        alert.Add(new MessageItem
                        {
                            ComponentId = $"employmentContract[{i}]",
                            Level = Level.ERROR,
                            LabelCode = Label.TE0103,
                            Placeholder = new string[] { $"{i}" }
                        });
                    }
                }
            }

            //check exist movement
            for (int i = 0; i < employmentContractSM.Movements.Count; i++)
            {
                if (employmentContractSM.Movements[i].AssignmentId != 0)
                {
                    isExist = _movementRepository.CheckExistMovementById(employmentContractSM.Movements[i].AssignmentId);
                    if (!isExist)
                    {
                        alert.Add(new MessageItem
                        {
                            ComponentId = $"movement[{i}]",
                            Level = Level.ERROR,
                            LabelCode = Label.MO0129,
                            Placeholder = new string[] { $"{i}" }
                        });
                    }
                }
                else
                {
                    isExist = _ecRepository.CheckExistEContractById(employmentContractSM.Movements[i].EmploymentContractId.Value);
                    if (!isExist)
                    {
                        alert.Add(new MessageItem
                        {
                            ComponentId = $"movement[{i}]",
                            Level = Level.ERROR,
                            LabelCode = Label.EMP0143,
                            Placeholder = new string[] { $"{i}" }
                        });
                    }
                }
            }

            if (alert.Count > 0)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.EC0102
                });
                response.Message.Alert = alert;
                response.Data = null;
                return response;
            }

            //alert = new List<MessageItem>();
            //check employment contract have movement
            //for (int i = 0; i < employmentContractSM.EmploymentContracts.Count; i++)
            //{
            //    if (employmentContractSM.EmploymentContracts[i].EmploymentContractId != 0)
            //    {
            //        var movementModel = employmentContractSM.Movements.Where(x => x.EmploymentContractId == employmentContractSM.EmploymentContracts[i].EmploymentContractId).ToList();
            //        var movementEntities = await _movementRepository.GetByEmploymentId(employmentContractSM.EmploymentContracts[i].EmploymentContractId);
            //        if (movementModel.Count == 0 && movementEntities.Count == 0)
            //        {
            //            alert.Add(new MessageItem
            //            {
            //                ComponentId = $"employmentContract[{i}]",
            //                Level = Level.ERROR,
            //                LabelCode = Label.EMP0144,
            //                Placeholder = new string[] { $"{i}" }
            //            });
            //        }
            //    }
            //    else
            //    {
            //        var movementModel = employmentContractSM.Movements.Where(x => x.ContractNo == employmentContractSM.EmploymentContracts[i].ContractNo).ToList();

            //        if (movementModel.Count == 0)
            //        {
            //            alert.Add(new MessageItem
            //            {
            //                ComponentId = $"employmentContract[{i}]",
            //                Level = Level.ERROR,
            //                LabelCode = Label.EMP0144,
            //                Placeholder = new string[] { $"{i}" }
            //            });
            //        }
            //    }
            //}

            //if (alert.Count > 0)
            //{
            //    response.Message.Toast.Add(new MessageItem
            //    {
            //        Level = Level.ERROR,
            //        LabelCode = Label.EC0102
            //    });
            //    response.Message.Alert = alert;
            //    response.Data = null;
            //    return response;
            //}

            return response;
        }

        private async Task CreateContract(int employeeId, string username, DateTime currentDate, List<EmploymentContractModel> employmentContractModels)
        {
            foreach (var item in employmentContractModels)
            {
                item.EmployeeId = employeeId;
                item.CreatedBy = username;
                item.CreatedDt = currentDate;
                item.ModifiedBy = username;
                item.ModifiedDt = currentDate;
            }

            var contracts = _mapper.Map<List<EmploymentContractModel>, List<TbEmpEmploymentContract>>(employmentContractModels);
            contracts = await _ecRepository.CreateMany(contracts);

            List<TaxRequest> taxs = new List<TaxRequest>();
            foreach (var contract in contracts)
            {
                TaxRequest tax = new TaxRequest();
                tax.EmploymentContractId = contract.EmploymentContractId;
                taxs.Add(tax);
            }

            if (taxs.Count > 0)
            {
                _taxRepository.CreateMany(taxs, username);
            }
        }

        private async Task UpdateContract(int employeeId, string username, DateTime currentDate, List<EmploymentContractModel> employmentContractModels)
        {
            foreach (var item in employmentContractModels)
            {
                var contractEntity = await _ecRepository.GetById(item.EmploymentContractId);
                item.EmployeeId = contractEntity.EmployeeId;
                item.CreatedBy = contractEntity.CreatedBy;
                item.CreatedDt = contractEntity.CreatedDt;
                item.ModifiedBy = username;
                item.ModifiedDt = currentDate;
            }

            var contracts = _mapper.Map<List<EmploymentContractModel>, List<TbEmpEmploymentContract>>(employmentContractModels);
            await _ecRepository.Update(contracts);
        }

        private async Task CreateMovement(string username, DateTime currentDate, List<MovementModel> movementModels)
        {
            foreach (var item in movementModels)
            {
                item.CreatedBy = username;
                item.CreatedDt = currentDate;
                item.ModifiedBy = username;
                item.ModifiedDt = currentDate;
            }

            var movements = _mapper.Map<List<MovementModel>, List<TbEmpMovement>>(movementModels);
            await _movementRepository.CreateMany(movements);
        }

        private async Task UpdateMovement(string username, DateTime currentDate, List<MovementModel> movementModels)
        {
            foreach (var item in movementModels)
            {
                var movementEntity = await _movementRepository.GetById(item.AssignmentId);
                item.CreatedBy = movementEntity.CreatedBy;
                item.CreatedDt = movementEntity.CreatedDt;
                item.ModifiedBy = username;
                item.ModifiedDt = currentDate;
            }

            var movements = _mapper.Map<List<MovementModel>, List<TbEmpMovement>>(movementModels);
            await _movementRepository.UpdateMany(movements);
        }

        public async Task<ApiResponse<EmploymentContractDto>> GetByEmployeeId(int employeeId, string ray)
        {
            ApiResponse<EmploymentContractDto> response = new ApiResponse<EmploymentContractDto>
            {
                Ray = ray ?? string.Empty,
                Data = new EmploymentContractDto()
            };

            var contracts = await _ecRepository.GetByEmployeeId(employeeId);
            var employeeStatuses = await _employmentStatusRepository.GetByEmployeeId(employeeId);
            var movementEntities = await _movementRepository.GetByEmployeeId(employeeId);
            var employee = await _employeeRepository.GetById(employeeId);

            var contractVms = new EmploymentContractDto();
            contractVms.RemarkEmploymentContract = employee.RemarkEmploymentContract;
            contractVms.RemarkMovement = employee.RemarkEmploymentMovement;
            contractVms.EmploymentContracts = _mapper.Map<List<TbEmpEmploymentContract>, List<EmploymentContractViewModel>>(contracts);

            foreach (var item in contractVms.EmploymentContracts)
            {
                foreach (var movement in item.Movements)
                {
                    var temp = (from m1 in movementEntities
                                join m2 in
                                (
                                    from m3 in
                                    (
                                        from m4 in movementEntities
                                        where m4.AssignmentStartDate >= movement.AssignmentStartDate
                                                   && m4.AssignmentId != movement.AssignmentId
                                                   && m4.EmploymentContractId == movement.EmploymentContractId
                                        select m4
                                    )
                                    group m3 by m3.EmploymentContractId into t1
                                    select new { EmploymentContractId = t1.Key, AssignmentStartDate = t1.Min(x => x.AssignmentStartDate) }
                                )
                                on new { m1.EmploymentContractId, m1.AssignmentStartDate } equals new { m2.EmploymentContractId, m2.AssignmentStartDate }
                                select m1).FirstOrDefault();
                    if (temp != null)
                    {
                        movement.AssignmentEndDate = temp.AssignmentStartDate;
                    }
                }
                var lstStatus = employeeStatuses.Where(x => x.EmploymentContractId == item.EmploymentContractId).ToList();
                var statusAC = lstStatus.FirstOrDefault(x => x.Status == EmploymentStatus.ACTIVE);
                var statusUP = lstStatus.FirstOrDefault(x => x.Status == EmploymentStatus.UNDER_PROBATION);
                var statusTE = lstStatus.FirstOrDefault(x => x.Status == EmploymentStatus.TERMINATED);

                if (statusTE != null)
                    item.ContractEndDate = statusTE.StartDate;

                if (statusAC != null && statusUP == null)
                    item.ContractStartDate = statusAC.StartDate;
                else if (statusAC == null && statusUP != null)
                    item.ContractStartDate = statusUP.StartDate;
                else if (statusAC != null && statusUP != null)
                {
                    if (statusAC.StartDate <= statusUP.StartDate)
                        item.ContractStartDate = statusAC.StartDate;
                    else
                        item.ContractStartDate = statusUP.StartDate;
                }
            }

            response.Data = contractVms;
            return response;
        }

        public async Task<ApiResponse<EmploymentContractViewModel>> GetById(int id, string ray)
        {
            ApiResponse<EmploymentContractViewModel> response = new ApiResponse<EmploymentContractViewModel>
            {
                Ray = ray ?? string.Empty,
                Data = new EmploymentContractViewModel()
            };
            var employment = await _ecRepository.GetById(id);
            var employmentVm = _mapper.Map<TbEmpEmploymentContract, EmploymentContractViewModel>(employment);
            response.Data = employmentVm;
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> Create(int employeeId, string ray, string username, EmploymentContractAddModel empContractAm)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>
            {
                Ray = ray ?? string.Empty,
                Data = new Dictionary<string, object>(),
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                    Alert = new List<MessageItem>(),
                    ValidateResult = new List<MessageItem>()
                }
            };

            //validate data
            response = await ValidateCreate(ray, employeeId, empContractAm);
            if (response.Message.ValidateResult.Count > 0 || response.Message.Alert.Count > 0)
            {
                return response;
            }

            DateTime currentDate = DateTime.Now;

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    //Update remark contract in Employee
                    var employee = await _employeeRepository.GetById(employeeId);
                    employee.RemarkEmploymentContract = empContractAm.Remark;
                    employee.ModifiedBy = username;
                    employee.ModifiedDt = currentDate;
                    await _employeeRepository.Edit(employee);

                    //Create contract
                    List<TbEmpEmploymentContract> contracts = new List<TbEmpEmploymentContract>();
                    TbEmpEmploymentContract contract = new TbEmpEmploymentContract();
                    contract.ContractNo = empContractAm.ContractNo;
                    contract.StaffType = empContractAm.StaffType;
                    contract.EmployeeId = employeeId;
                    contract.CreatedBy = username;
                    contract.CreatedDt = currentDate;
                    contract.ModifiedBy = username;
                    contract.ModifiedDt = currentDate;
                    contracts.Add(contract);
                    await _ecRepository.CreateMany(contracts);

                    //create movement
                    List<TbEmpMovement> movements = new List<TbEmpMovement>();
                    TbEmpMovement movement = new TbEmpMovement();
                    movement.WorkCalendar = empContractAm.WorkCalendar;
                    movement.DefaultSalaryCurrency = empContractAm.DefaultSalaryCurrency;
                    movement.DefaultPaymentCurrency = empContractAm.DefaultPaymentCurrency;
                    movement.BusinessUnitId = empContractAm.BusinessUnitId;
                    movement.AssignmentStartDate = empContractAm.ExpectedStartDate;
                    movement.EmploymentContractId = contract.EmploymentContractId;
                    movement.CreatedBy = username;
                    movement.CreatedDt = currentDate;
                    movement.ModifiedBy = username;
                    movement.ModifiedDt = currentDate;
                    movements.Add(movement);
                    await _movementRepository.CreateMany(movements);

                    //create tax
                    TbEmpTax tax = new TbEmpTax();
                    tax.EmploymentContractId = contract.EmploymentContractId;
                    tax.CreatedBy = username;
                    tax.CreatedDt = currentDate;
                    movement.ModifiedBy = username;
                    movement.ModifiedDt = currentDate;
                    await _taxRepository.Create(tax);

                    //create employment status
                    List<TbEmpEmploymentStatus> empStatuses = new List<TbEmpEmploymentStatus>();
                    TbEmpEmploymentStatus empStatus = new TbEmpEmploymentStatus();

                    if (empContractAm.DateOfAcceptingTheOffer != null)
                    {
                        if (empContractAm.DateOfAcceptingTheOffer.Value.Date.CompareTo(currentDate.Date) <= 0)
                            empStatus.Confirmed = true;
                        else
                            empStatus.Confirmed = false;
                        empStatus.StartDate = empContractAm.DateOfAcceptingTheOffer.Value;
                        empStatus.Status = EmploymentStatus.ACCEPTED_OFFER;
                        empStatus.EmploymentContractId = contract.EmploymentContractId;
                        empStatus.CreatedBy = username;
                        empStatus.CreatedDt = currentDate;
                        empStatus.ModifiedBy = username;
                        empStatus.ModifiedDt = currentDate;
                        empStatuses.Add(empStatus);
                    }

                    if (empContractAm.ExpectedStartDate != null && empContractAm.ExpectedProbationEndDate == null)
                    {
                        empStatus = new TbEmpEmploymentStatus();
                        if (empContractAm.ExpectedStartDate.Value.Date.CompareTo(currentDate.Date) <= 0)
                            empStatus.Confirmed = true;
                        else
                            empStatus.Confirmed = false;
                        empStatus.StartDate = empContractAm.ExpectedStartDate.Value;
                        empStatus.Status = EmploymentStatus.ACTIVE;
                        empStatus.EmploymentContractId = contract.EmploymentContractId;
                        empStatus.CreatedBy = username;
                        empStatus.CreatedDt = currentDate;
                        empStatus.ModifiedBy = username;
                        empStatus.ModifiedDt = currentDate;
                        empStatuses.Add(empStatus);
                    }

                    if (empContractAm.ExpectedStartDate != null && empContractAm.ExpectedProbationEndDate != null)
                    {
                        empStatus = new TbEmpEmploymentStatus();
                        if (empContractAm.ExpectedStartDate.Value.Date.CompareTo(currentDate.Date) <= 0)
                            empStatus.Confirmed = true;
                        else
                            empStatus.Confirmed = false;
                        empStatus.StartDate = empContractAm.ExpectedStartDate.Value;
                        empStatus.Status = EmploymentStatus.UNDER_PROBATION;
                        empStatus.EmploymentContractId = contract.EmploymentContractId;
                        empStatus.CreatedBy = username;
                        empStatus.CreatedDt = currentDate;
                        empStatus.ModifiedBy = username;
                        empStatus.ModifiedDt = currentDate;
                        empStatuses.Add(empStatus);

                        empStatus = new TbEmpEmploymentStatus();
                        if (empContractAm.ExpectedProbationEndDate.Value.Date.CompareTo(currentDate.Date) <= 0)
                            empStatus.Confirmed = true;
                        else
                            empStatus.Confirmed = false;
                        empStatus.StartDate = empContractAm.ExpectedProbationEndDate.Value.AddDays(1);
                        empStatus.Status = EmploymentStatus.ACTIVE;
                        empStatus.EmploymentContractId = contract.EmploymentContractId;
                        empStatus.CreatedBy = username;
                        empStatus.CreatedDt = currentDate;
                        empStatus.ModifiedBy = username;
                        empStatus.ModifiedDt = currentDate;
                        empStatuses.Add(empStatus);
                    }
                    await _employmentStatusRepository.CreateMany(empStatuses);
                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }
            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.EC0134
            });
            var contractVms = await GetByEmployeeId(employeeId, ray);
            response.Data.Add("contracts", contractVms);
            return response;
        }

        private async Task<ApiResponse<Dictionary<string, object>>> ValidateCreate(string ray, int employeeId, EmploymentContractAddModel empContractAm)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>
            {
                Ray = ray ?? string.Empty,
                Data = new Dictionary<string, object>(),
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                    Alert = new List<MessageItem>(),
                    ValidateResult = new List<MessageItem>()
                }
            };

            List<MessageItem> messages = new List<MessageItem>();
            messages = _validator.Validate(empContractAm);
            if (messages.Count > 0)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.EC0135
                });
                response.Message.ValidateResult = messages;
                response.Data = null;
                return response;
            }

            return response;
        }

        public async Task<ApiResponse<EmploymentContractViewModel>> GetContractNew(int employeeId, string ray)
        {
            ApiResponse<EmploymentContractViewModel> response = new ApiResponse<EmploymentContractViewModel>
            {
                Ray = ray ?? string.Empty,
                Data = new EmploymentContractViewModel()
            };
            var contracts = await _ecRepository.GetByEmployeeId(employeeId);
            var employeeStatuses = await _employmentStatusRepository.GetByEmployeeId(employeeId);
            var contractVms = _mapper.Map<List<TbEmpEmploymentContract>, List<EmploymentContractViewModel>>(contracts);

            foreach (var item in contractVms)
            {
                var lstStatus = employeeStatuses.Where(x => x.EmploymentContractId == item.EmploymentContractId).ToList();
                var statusAC = lstStatus.FirstOrDefault(x => x.Status == EmploymentStatus.ACTIVE);
                var statusUP = lstStatus.FirstOrDefault(x => x.Status == EmploymentStatus.UNDER_PROBATION);
                var statusTE = lstStatus.FirstOrDefault(x => x.Status == EmploymentStatus.TERMINATED);

                if (statusTE != null)
                    item.ContractEndDate = statusTE.StartDate;

                if (statusAC != null && statusUP == null)
                    item.ContractStartDate = statusAC.StartDate;
                else if (statusAC == null && statusUP != null)
                    item.ContractStartDate = statusUP.StartDate;
                else if (statusAC != null && statusUP != null)
                {
                    if (statusAC.StartDate <= statusUP.StartDate)
                        item.ContractStartDate = statusAC.StartDate;
                    else
                        item.ContractStartDate = statusUP.StartDate;
                }
            }

            var contractVm = contractVms.OrderByDescending(x => x.ContractStartDate).FirstOrDefault();
            response.Data = contractVm;
            return response;
        }
    }
}