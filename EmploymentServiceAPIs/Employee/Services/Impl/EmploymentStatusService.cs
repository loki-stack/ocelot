﻿using AutoMapper;
using CommonLib.Constants;
using CommonLib.Dtos;
using CommonLib.Models.Client;
using CommonLib.Repositories.Impl;
using CommonLib.Repositories.Interfaces;
using CommonLib.Validation;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Constants;
using EmploymentServiceAPIs.Employee.Dtos;
using EmploymentServiceAPIs.Employee.Dtos.EmploymentStatus;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using EmploymentServiceAPIs.Employee.Services.Interfaces;
using EmploymentServiceAPIs.ManageEntity.Dtos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace EmploymentServiceAPIs.Employee.Services.Impl
{
    public class EmploymentStatusService : IEmploymentStatusService
    {
        private readonly IEmploymentStatusRepository _employmentStatusRepository;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IEmploymentContractRepository _employmentContractRepository;
        private readonly IEntitySettingRepository _entitySettingRepository;
        private readonly IGenericCodeRepository _genericCodeRepository;
        private readonly IMapper _mapper;
        private readonly IValidator _validator;

        public EmploymentStatusService(IEmploymentStatusRepository employmentStatusRepository,
                                        IEmployeeRepository employeeRepository,
                                        IEmploymentContractRepository employmentContractRepository,
                                        IEntitySettingRepository entitySettingRepository,
                                        IGenericCodeRepository genericCodeRepository,
                                        IMapper mapper,
                                        IValidator validator)
        {
            _employmentStatusRepository = employmentStatusRepository;
            _employeeRepository = employeeRepository;
            _employmentContractRepository = employmentContractRepository;
            _entitySettingRepository = entitySettingRepository;
            _genericCodeRepository = genericCodeRepository;
            _mapper = mapper;
            _validator = validator;
        }

        public async Task<ApiResponse<EmploymentStatusDto>> Delete(int id, int employeeId, string ray)
        {
            ApiResponse<EmploymentStatusDto> response = new ApiResponse<EmploymentStatusDto>
            {
                Ray = ray ?? string.Empty,
                Data = new EmploymentStatusDto(),
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                    ValidateResult = new List<MessageItem>()
                }
            };

            // check exist employment status
            var isExist = _employmentStatusRepository.CheckExistEmploymentStatusById(id);
            if (!isExist)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.ES0109
                });
                response.Message.ValidateResult.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.ES0107
                });
                response.Data = null;
                return response;
            }

            await _employmentStatusRepository.Delete(id);

            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.ES0108
            });
            var employmentStatusDto = await GetByEmployeeId(employeeId, ray);
            response.Data = employmentStatusDto.Data;
            return response;
        }

        public async Task<ApiResponse<EmploymentStatusDto>> GetByEmployeeId(int employeeId, string ray)
        {
            ApiResponse<EmploymentStatusDto> response = new ApiResponse<EmploymentStatusDto>
            {
                Ray = ray ?? string.Empty,
                Data = new EmploymentStatusDto()
            };
            var lstEmploymentStatus = await _employmentStatusRepository.GetByEmployeeId(employeeId);
            var esGroupBy = lstEmploymentStatus
                            .GroupBy(x => new { x.StartDate })
                            .Select(x => new EmploymentStatusItemModel
                            {
                                StartDate = x.Key.StartDate,
                                EmploymenStatuses = x.Select(y => new EmploymentStatusViewModel
                                {
                                    EmploymentStatusId = y.EmploymentStatusId,
                                    EmploymentContractId = y.EmploymentContractId,
                                    EmploymentContractName = y.EmploymentContractName,
                                    Status = y.Status,
                                    Remark = y.Remark,
                                    Confirmed = y.Confirmed,
                                    StartDate = y.StartDate,
                                    CreatedBy = y.CreatedBy,
                                    CreatedDt = y.CreatedDt,
                                    ModifiedBy = y.ModifiedBy,
                                    ModifiedDt = y.ModifiedDt
                                }).OrderBy(z => z.StartDate).ToList()
                            }).OrderByDescending(x => x.StartDate).ToList();

            var employee = await _employeeRepository.GetById(employeeId);
            var data = new EmploymentStatusDto()
            {
                Remark = employee.RemarkEmploymentStatus,
                Data = esGroupBy
            };
            response.Data = data;
            return response;
        }

        public async Task<ApiResponse<EmploymentStatusViewModel>> GetById(int id, string ray)
        {
            ApiResponse<EmploymentStatusViewModel> response = new ApiResponse<EmploymentStatusViewModel>
            {
                Ray = ray ?? string.Empty,
                Data = new EmploymentStatusViewModel()
            };
            var employmentStatus = await _employmentStatusRepository.GetById(id);
            var employmentStatusVm = _mapper.Map<TbEmpEmploymentStatus, EmploymentStatusViewModel>(employmentStatus);
            response.Data = employmentStatusVm;
            return response;
        }

        public async Task<ApiResponse<EmploymentStatusDto>> Save(int employeeId, string ray, string username, EmploymentStatusSaveModel employmentStatusSM)
        {
            ApiResponse<EmploymentStatusDto> response = new ApiResponse<EmploymentStatusDto>
            {
                Ray = ray ?? string.Empty,
                Data = new EmploymentStatusDto(),
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                    Alert = new List<MessageItem>(),
                    ValidateResult = new List<MessageItem>()
                }
            };

            //validate data
            response = await Validate(employeeId, ray, employmentStatusSM);
            if (response.Message.ValidateResult.Count > 0
                || response.Message.Alert.Count > 0
                || response.Message.Toast.Count > 0)
            {
                return response;
            }

            //Save data
            var lstEmploymentStatusCreate = employmentStatusSM.EmploymentStatuses.Where(x => x.EmploymentStatusId == 0).ToList();
            var lstEmploymentStatusUpdate = employmentStatusSM.EmploymentStatuses.Where(x => x.EmploymentStatusId != 0).ToList();
            DateTime currentDate = DateTime.Now;

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    //Update remark employment status in Employee
                    var employee = await _employeeRepository.GetById(employeeId);
                    employee.RemarkEmploymentStatus = employmentStatusSM.Remark;
                    employee.ModifiedBy = username;
                    employee.ModifiedDt = currentDate;
                    await _employeeRepository.Edit(employee);

                    //Create employee status
                    if (lstEmploymentStatusCreate.Count > 0)
                    {
                        for (int i = 0; i < lstEmploymentStatusCreate.Count; i++)
                        {
                            lstEmploymentStatusCreate[i].CreatedBy = username;
                            lstEmploymentStatusCreate[i].CreatedDt = currentDate;
                            lstEmploymentStatusCreate[i].ModifiedBy = username;
                            lstEmploymentStatusCreate[i].ModifiedDt = currentDate;
                        }

                        var lstEmploymentStatus = _mapper.Map<List<EmploymentStatusModel>, List<TbEmpEmploymentStatus>>(lstEmploymentStatusCreate);
                        await _employmentStatusRepository.CreateMany(lstEmploymentStatus);
                    }
                    //Update employee status
                    if (lstEmploymentStatusUpdate.Count > 0)
                    {
                        for (int i = 0; i < lstEmploymentStatusUpdate.Count; i++)
                        {
                            var employeeStatusEntity = await _employmentStatusRepository.GetById(lstEmploymentStatusUpdate[i].EmploymentStatusId);
                            lstEmploymentStatusUpdate[i].CreatedBy = employeeStatusEntity.CreatedBy;
                            lstEmploymentStatusUpdate[i].CreatedDt = employeeStatusEntity.CreatedDt;
                            lstEmploymentStatusUpdate[i].ModifiedBy = username;
                            lstEmploymentStatusUpdate[i].ModifiedDt = currentDate;
                        }

                        var lstEmploymentStatus = _mapper.Map<List<EmploymentStatusModel>, List<TbEmpEmploymentStatus>>(lstEmploymentStatusUpdate);
                        await _employmentStatusRepository.UpdateMany(lstEmploymentStatus);
                    }

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }

            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.ES0101
            });

            var employmentStatusDto = await GetByEmployeeId(employeeId, ray);
            response.Data = employmentStatusDto.Data;
            return response;
        }

        private async Task<ApiResponse<EmploymentStatusDto>> Validate(int employeeId, string ray, EmploymentStatusSaveModel employmentStatusSM)
        {
            ApiResponse<EmploymentStatusDto> response = new ApiResponse<EmploymentStatusDto>
            {
                Ray = ray ?? string.Empty,
                Data = new EmploymentStatusDto(),
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                    Alert = new List<MessageItem>(),
                    ValidateResult = new List<MessageItem>()
                }
            };

            //validate data
            List<MessageItem> messages = new List<MessageItem>();
            messages = _validator.Validate(employmentStatusSM);

            for (int i = 0; i < employmentStatusSM.EmploymentStatuses.Count; i++)
            {
                var message = _validator.Validate(employmentStatusSM.EmploymentStatuses[i]);
                for (int j = 0; j < message.Count; j++)
                {
                    message[j].ComponentId = $"employmentStatus[{i}]_{message[j].ComponentId}";
                }
                messages.AddRange(message);
            }

            //return respone when data invalid
            if (messages.Count > 0)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.ES0102
                });
                response.Message.ValidateResult = messages;
                response.Data = null;
                return response;
            }

            List<MessageItem> alert = new List<MessageItem>();
            //check exist employment status
            for (int i = 0; i < employmentStatusSM.EmploymentStatuses.Count; i++)
            {
                bool isExist = _employmentContractRepository.CheckExistEContractById(employmentStatusSM.EmploymentStatuses[i].EmploymentContractId);
                if (!isExist)
                {
                    alert.Add(new MessageItem
                    {
                        ComponentId = $"employmentStatus[{i}]",
                        Level = Level.ERROR,
                        LabelCode = Label.EMP0143
                    });
                }

                if (employmentStatusSM.EmploymentStatuses[i].EmploymentStatusId != 0)
                {
                    isExist = _employmentStatusRepository.CheckExistEmploymentStatusById(employmentStatusSM.EmploymentStatuses[i].EmploymentStatusId);
                    if (!isExist)
                    {
                        alert.Add(new MessageItem
                        {
                            ComponentId = $"employmentStatus[{i}]",
                            Level = Level.ERROR,
                            LabelCode = Label.ES0107
                        });
                    }
                }
            }

            if (alert.Count > 0)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.ES0102
                });
                response.Message.Alert = alert;
                response.Data = null;
                return response;
            }

            var empStatusVms = await _employmentStatusRepository.GetByEmployeeId(employeeId);
            var empContracts = await _employmentContractRepository.GetByEmployeeId(employeeId);
            var empStatuses = (from es1 in employmentStatusSM.EmploymentStatuses
                               join es2 in (from es3 in (from es4 in employmentStatusSM.EmploymentStatuses
                                                         join es5 in employmentStatusSM.EmploymentStatuses on es4.EmploymentContractId equals es5.EmploymentContractId
                                                         where es4.StartDate < es5.StartDate && (es5.EmploymentStatusId == 0 || es4.EmploymentStatusId != es5.EmploymentStatusId)
                                                         select new
                                                         {
                                                             es5.EmploymentStatusId,
                                                             es5.EmploymentContractId,
                                                             es4.Status,
                                                             es4.StartDate
                                                         })
                                            group es3 by new { es3.EmploymentStatusId, es3.EmploymentContractId } into temp
                                            select new
                                            {
                                                EmploymentStatusId = temp.Key.EmploymentStatusId,
                                                EmploymentContractId = temp.Key.EmploymentContractId,
                                                StartDate = temp.Max(x => x.StartDate)
                                            }) on es1.EmploymentContractId equals es2.EmploymentContractId
                               where es2.StartDate == es1.StartDate
                               select new EmploymentStatusViewModel
                               {
                                   EmploymentStatusId = es2.EmploymentStatusId,
                                   EmploymentContractId = es2.EmploymentContractId,
                                   Status = es1.Status,
                                   StartDate = es2.StartDate
                               }).ToList();

            //validate start date exist
            messages = new List<MessageItem>();
            for (int i = 0; i < employmentStatusSM.EmploymentStatuses.Count; i++)
            {
                bool isExist = CheckExistStartDate(employmentStatusSM.EmploymentStatuses[i].EmploymentStatusId,
                                        employmentStatusSM.EmploymentStatuses[i].EmploymentContractId, employmentStatusSM.EmploymentStatuses[i].StatusId,
                                        employmentStatusSM.EmploymentStatuses[i].StartDate, empStatusVms, employmentStatusSM.EmploymentStatuses);
                if (isExist)
                {
                    messages.Add(new MessageItem
                    {
                        ComponentId = $"employmentStatus[{i}]_startDate",
                        Level = Level.ERROR,
                        LabelCode = Label.ES0111
                    });
                }
            }

            if (messages.Count > 0)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.ES0102
                });
                response.Message.ValidateResult = messages;
                response.Data = null;
                return response;
            }

            //validate flow change status
            List<MessageItem> toast = ValidateFlowChangeStatus(employmentStatusSM, empStatuses, empContracts);
            if (toast.Count > 0)
            {
                response.Message.Toast = toast;
                response.Data = null;
                return response;
            }

            return response;
        }

        private List<MessageItem> ValidateFlowChangeStatus(EmploymentStatusSaveModel employmentStatusSM,
                                                            List<EmploymentStatusViewModel> empStatuses,
                                                            List<TbEmpEmploymentContract> empContracts)
        {
            List<MessageItem> toast = new List<MessageItem>();
            for (int i = 0; i < employmentStatusSM.EmploymentStatuses.Count; i++)
            {
                var empStatus = empStatuses.FirstOrDefault(x => x.EmploymentStatusId == employmentStatusSM.EmploymentStatuses[i].EmploymentStatusId);
                var empContract = empContracts.FirstOrDefault(x => x.EmploymentContractId == employmentStatusSM.EmploymentStatuses[i].EmploymentContractId);
                if (empStatus != null)
                {
                    string status = empStatus.Status;
                    if (status == EmploymentStatus.NO_SHOW
                        || status == EmploymentStatus.DECLINED_OFFER
                        || status == EmploymentStatus.TERMINATED
                        || (status == EmploymentStatus.ACCEPTED_OFFER
                            && employmentStatusSM.EmploymentStatuses[i].Status != EmploymentStatus.UNDER_PROBATION
                            && employmentStatusSM.EmploymentStatuses[i].Status != EmploymentStatus.ACTIVE
                            && employmentStatusSM.EmploymentStatuses[i].Status != EmploymentStatus.DECLINED_OFFER
                            && employmentStatusSM.EmploymentStatuses[i].Status != EmploymentStatus.NO_SHOW)
                        || (status == EmploymentStatus.UNDER_PROBATION
                            && employmentStatusSM.EmploymentStatuses[i].Status != EmploymentStatus.ACTIVE
                            && employmentStatusSM.EmploymentStatuses[i].Status != EmploymentStatus.KNOWN_LEAVING
                            && employmentStatusSM.EmploymentStatuses[i].Status != EmploymentStatus.UNDER_PROBATION
                            && employmentStatusSM.EmploymentStatuses[i].Status != EmploymentStatus.TERMINATED)
                        || (status == EmploymentStatus.ACTIVE
                            && employmentStatusSM.EmploymentStatuses[i].Status != EmploymentStatus.SUSPENDED
                            && employmentStatusSM.EmploymentStatuses[i].Status != EmploymentStatus.KNOWN_LEAVING
                            && employmentStatusSM.EmploymentStatuses[i].Status != EmploymentStatus.TERMINATED)
                        || (status == EmploymentStatus.SUSPENDED
                            && employmentStatusSM.EmploymentStatuses[i].Status != EmploymentStatus.ACTIVE
                            && employmentStatusSM.EmploymentStatuses[i].Status != EmploymentStatus.KNOWN_LEAVING
                            && employmentStatusSM.EmploymentStatuses[i].Status != EmploymentStatus.TERMINATED)
                       )
                    {
                        toast.Add(new MessageItem
                        {
                            Level = Level.ERROR,
                            LabelCode = Label.ES0110,
                            Placeholder = new string[] { $"{empContract.ContractNo}", $"{status}", $"{employmentStatusSM.EmploymentStatuses[i].Status}" }
                        });
                    }

                    if (status == EmploymentStatus.KNOWN_LEAVING)
                    {
                        string statusOld = GetEmployementStatusOld(employmentStatusSM.EmploymentStatuses[i].EmploymentStatusId
                                                        , employmentStatusSM.EmploymentStatuses[i].EmploymentContractId
                                                        , empStatus.StartDate
                                                        , empStatuses);
                        if (!string.IsNullOrEmpty(statusOld) &&
                            (
                                (statusOld == EmploymentStatus.UNDER_PROBATION && employmentStatusSM.EmploymentStatuses[i].Status != EmploymentStatus.TERMINATED)
                                || (
                                      (statusOld == EmploymentStatus.ACTIVE || statusOld == EmploymentStatus.SUSPENDED)
                                      && (
                                            employmentStatusSM.EmploymentStatuses[i].Status != EmploymentStatus.ACTIVE
                                            && employmentStatusSM.EmploymentStatuses[i].Status != EmploymentStatus.SUSPENDED
                                            && employmentStatusSM.EmploymentStatuses[i].Status != EmploymentStatus.TERMINATED
                                         )
                                   )
                            )
                        )
                        {
                            toast.Add(new MessageItem
                            {
                                Level = Level.ERROR,
                                LabelCode = Label.ES0110,
                                Placeholder = new string[] { $"{empContract.ContractNo}", $"{status}", $"{employmentStatusSM.EmploymentStatuses[i].Status}" }
                            });
                        }
                    }
                }
            }

            return toast;
        }

        private bool CheckExistStartDate(int empStatusId, int empContractId, Guid StatusId, DateTime startDate,
                                        List<EmploymentStatusViewModel> empStatuses, List<EmploymentStatusModel> empStatusMs)
        {
            bool isExistServer = empStatuses.Any(x => x.EmploymentContractId == empContractId && x.StartDate == startDate && (empStatusId == 0 || x.EmploymentStatusId != empStatusId));
            bool isExistClient = empStatusMs.Any(x => x.EmploymentContractId == empContractId && x.StartDate == startDate && ((empStatusId == 0 && x.StatusId != StatusId) || x.EmploymentStatusId != empStatusId));
            bool isExist = false;
            if (isExistServer || isExistClient)
                isExist = true;
            return isExist;
        }

        private string GetEmployementStatusOld(int empStatusId, int empContractId, DateTime startDate, List<EmploymentStatusViewModel> empStatuses)
        {
            string status = string.Empty;
            var empStatus = (from es1 in empStatuses
                             join es2 in
                             (
                                 from es3 in
                                 (
                                     from es4 in empStatuses
                                     where es4.StartDate < startDate && (empStatusId == 0 || es4.EmploymentStatusId != empStatusId)
                                     select es4
                                 )
                                 group es3 by es3.EmploymentContractId into temp
                                 select new { EmploymentContractId = temp.Key, StartDate = temp.Max(x => x.StartDate) }
                             )
                             on new { es1.EmploymentContractId, es1.StartDate } equals new { es2.EmploymentContractId, es2.StartDate }
                             select es1).FirstOrDefault(x => x.EmploymentContractId == empContractId);
            status = empStatus != null ? empStatus.Status : string.Empty;
            return status;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetStatusConfirmation(int clientId, int entityId, string ray, string token, string locale, BasePaginationRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>
            {
                Ray = ray ?? string.Empty,
                Data = new Dictionary<string, object>()
            };
            StdRequestParam requestParam = new StdRequestParam()
            {
                ClientId = clientId,
                EntityId = entityId,
                Locale = locale
            };
            //Get status confirmation
            var empStatusConfirmation = await _employmentStatusRepository.GetStatusConfirmation(locale);

            //Get generic code
            var genericCodes = await _genericCodeRepository.GetGenericCodes(new List<string>() { GenericCode.EMPLOYMENT_STATUS }, token, requestParam);

            List<GenericCodeResult> empStatuses = new List<GenericCodeResult>();

            if (genericCodes.ContainsKey(GenericCode.EMPLOYMENT_STATUS.ToLower()))
            {
                empStatuses = genericCodes[GenericCode.EMPLOYMENT_STATUS.ToLower()];
            }

            var lstStatus = (from esc in empStatusConfirmation
                             join es1 in empStatuses on esc.LastStatus equals es1.CodeValue into temp1
                             from p1 in temp1.DefaultIfEmpty()
                             join es2 in empStatuses on esc.NewStatus equals es2.CodeValue into temp2
                             from p2 in temp2.DefaultIfEmpty()
                             select new EmploymentStatusResult
                             {
                                 FullName = esc.FullName,
                                 EmployeeId = esc.EmployeeId,
                                 BusinessUnit = esc.BusinessUnit,
                                 EmployeeNo = esc.EmployeeNo,
                                 EmploymentContractId = esc.EmploymentContractId,
                                 EmploymentStatusId = esc.EmploymentStatusId,
                                 StartDate = esc.StartDate,
                                 LastStatus = p1 != null ? p1.TextValue : string.Empty,
                                 NewStatus = p2 != null ? p2.TextValue : string.Empty,
                                 Confirmed = esc.Confirmed
                             }).ToList();

            EmploymentStatusFilter filter = new EmploymentStatusFilter();
            if (!string.IsNullOrEmpty(request.Filter))
            {
                filter = JsonConvert.DeserializeObject<EmploymentStatusFilter>(request.Filter);
            }
            //sort by item from request
            if (!string.IsNullOrEmpty(request.Sort))
            {
                string sortCharacter = request.Sort.Substring(0, 1);
                string sortField = request.Sort.Substring(1, request.Sort.Length - 1);
                if (sortField.ToLower().Contains("fullname"))
                {
                    if (sortCharacter == "-")
                        lstStatus = lstStatus.OrderByDescending(x => x.FullName).ToList();
                    else lstStatus = lstStatus.OrderBy(x => x.FullName).ToList();
                }

                if (sortField.ToLower().Contains("employeeno"))
                {
                    if (sortCharacter == "-")
                        lstStatus = lstStatus.OrderByDescending(x => x.EmployeeNo).ToList();
                    else lstStatus = lstStatus.OrderBy(x => x.EmployeeNo).ToList();
                }

                if (sortField.ToLower().Contains("businessunit"))
                {
                    if (sortCharacter == "-")
                        lstStatus = lstStatus.OrderByDescending(x => x.BusinessUnit).ToList();
                    else lstStatus = lstStatus.OrderBy(x => x.BusinessUnit).ToList();
                }

                if (sortField.ToLower().Contains("laststatus"))
                {
                    if (sortCharacter == "-")
                        lstStatus = lstStatus.OrderByDescending(x => x.LastStatus).ToList();
                    else lstStatus = lstStatus.OrderBy(x => x.LastStatus).ToList();
                }

                if (sortField.ToLower().Contains("newstatus"))
                {
                    if (sortCharacter == "-")
                        lstStatus = lstStatus.OrderByDescending(x => x.NewStatus).ToList();
                    else lstStatus = lstStatus.OrderBy(x => x.NewStatus).ToList();
                }

                if (sortField.ToLower().Contains("startdate"))
                {
                    if (sortCharacter == "-")
                        lstStatus = lstStatus.OrderByDescending(x => x.StartDate).ToList();
                    else lstStatus = lstStatus.OrderBy(x => x.StartDate).ToList();
                }
            }
            //search by text
            if (!string.IsNullOrEmpty(filter.NewStatus))
            {
                lstStatus = lstStatus.Where(x => x.NewStatus == filter.NewStatus).ToList();
            }

            if (!string.IsNullOrEmpty(filter.LastStatus))
            {
                lstStatus = lstStatus.Where(x => x.LastStatus == filter.LastStatus).ToList();
            }

            if (!string.IsNullOrEmpty(request.FullTextSearch))
            {
                lstStatus = lstStatus.Where(x => x.FullName.ToLower().Contains(request.FullTextSearch.ToLower())).ToList();
            }

            Pagination<EmploymentStatusResult> pagination = new Pagination<EmploymentStatusResult>();
            var dataPaging = new List<EmploymentStatusResult>();
            if (request.Size != null)
            {
                pagination.Size = request.Size.Value;
                pagination.TotalPages = (lstStatus.Count + request.Size.Value - 1) / request.Size.Value;
                dataPaging = lstStatus.Skip((request.Page.Value - 1) * request.Size.Value).Take(request.Size.Value).ToList();
            }
            else
            {
                dataPaging = lstStatus;
            }
            pagination.Page = request.Page.Value;
            pagination.TotalElements = lstStatus.Count;
            pagination.NumberOfElements = dataPaging.Count;
            pagination.Content = dataPaging;
            response.Data.Add("pagination", pagination);
            return response;
        }

        public async Task<ApiResponse<int?>> Update(string username, string ray, int id)
        {
            ApiResponse<int?> response = new ApiResponse<int?>
            {
                Ray = ray ?? string.Empty,
                Data = null,
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                    ValidateResult = new List<MessageItem>()
                }
            };
            //check exist
            bool isExist = _employmentStatusRepository.CheckExistEmploymentStatusById(id);
            if (!isExist)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.ES0107
                });

                return response;
            }

            var empStatus = await _employmentStatusRepository.GetById(id);
            empStatus.Confirmed = true;
            empStatus.ModifiedBy = username;
            empStatus.ModifiedDt = DateTime.Now;
            await _employmentStatusRepository.Update(empStatus);

            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.ES0112
            });
            response.Data = id;

            return response;
        }
    }
}