﻿using AutoMapper;
using CommonLib.Constants;
using CommonLib.Dtos;
using CommonLib.Models.Client;
using CommonLib.Validation;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Constants;
using EmploymentServiceAPIs.Employee.Dtos.ManageFile;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using EmploymentServiceAPIs.Employee.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Services.Impl
{
    public class FileService : IFileService
    {
        private readonly IFileRepository _manageFileRepository;
        private readonly IMapper _mapper;
        private readonly IValidator _validator;
        private static readonly string[] _validExtensions = { ".jpg", ".png" };

        public FileService(IFileRepository manageFileRepository, IMapper mapper, IValidator validator)
        {
            _manageFileRepository = manageFileRepository;
            _mapper = mapper;
            _validator = validator;
        }

        public async Task<ApiResponse<List<FileViewModel>>> UploadFile(int clientId, int entityId, string ray, string username, List<InforUploadModel> infors, IList<IFormFile> files)
        {
            ApiResponse<List<FileViewModel>> response = new ApiResponse<List<FileViewModel>>
            {
                Ray = ray ?? string.Empty,
                Data = new List<FileViewModel>(),
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                    ValidateResult = new List<MessageItem>()
                }
            };

            //validate data
            response = await Validate(ray, infors, files);
            if (response.Message.ValidateResult.Count > 0)
            {
                return response;
            }

            var fileModels = new List<TbComFile>();

            for (int i = 0; i < files.Count(); i++)
            {
                using (var memoryStream = new MemoryStream())
                {
                    await files[i].CopyToAsync(memoryStream);
                    if (memoryStream.Length <= 1048576)
                    {
                        TbComFile fileModel = new TbComFile();
                        fileModel.Name = !string.IsNullOrEmpty(infors[i].Name) ? infors[i].Name : files[i].FileName;
                        fileModel.Category = infors[i].Category;
                        fileModel.Content = memoryStream.ToArray();
                        fileModel.IsDeleted = false;
                        fileModel.Remark = infors[i].Remark;
                        fileModel.CreatedBy = username;
                        fileModel.CreatedDt = DateTime.Now;
                        fileModel.ModifiedBy = username;
                        fileModel.ModifiedDt = DateTime.Now;

                        fileModels.Add(fileModel);
                    }
                }
            }

            var result = await _manageFileRepository.Create(fileModels);
            var fileVms = _mapper.Map<List<TbComFile>, List<FileViewModel>>(result);
            for (int i = 0; i < fileVms.Count; i++)
            {
                fileVms[i].Src = $"/v1.0/empl/COM/COM0401/File/{fileVms[i].FileId}?Parameter.ClientId={clientId}&Parameter.EntityId={entityId}";
            }
            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.MF0101
            });
            response.Data = fileVms;
            return response;
        }

        public async Task<ApiResponse<List<FileViewModel>>> GetListFileByIds(int clientId, int entityId, string ray, List<int> ids)
        {
            ApiResponse<List<FileViewModel>> response = new ApiResponse<List<FileViewModel>>
            {
                Ray = ray ?? string.Empty,
                Data = new List<FileViewModel>(),
            };
            var files = await _manageFileRepository.GetListFileByIds(ids);
            var fileVms = _mapper.Map<List<TbComFile>, List<FileViewModel>>(files);
            for (int i = 0; i < fileVms.Count; i++)
            {
                fileVms[i].Src = $"/v1.0/empl/COM/COM0401/File/{fileVms[i].FileId}?Parameter.ClientId={clientId}&Parameter.EntityId={entityId}";
            }
            response.Data = fileVms;
            return response;
        }

        public async Task GetFilesAsZip(int clientId, int entityId, int employeeId, ZipArchive archive, List<int> ids)
        {
            // TODO - query TB_EMP_PAYSLIP that the payslip is belongs to the employeeId
            List<int> validIds = new List<int>(ids); // comment this later
            // List<int> validIds = _payslipRepo.getFileIdByEmployeeId(employeeId);

            var files = await _manageFileRepository.GetListFileByIds(validIds);
            foreach (var file in files)
            {
                var zipArchiveEntry = archive.CreateEntry(file.Name, CompressionLevel.Fastest);
                using (var zipStream = zipArchiveEntry.Open())
                {
                    await zipStream.WriteAsync(file.Content);
                }
            }
        }

        public async Task<ApiResponse<FileDownloadModel>> DownloadFile(int id, string ray)
        {
            ApiResponse<FileDownloadModel> response = new ApiResponse<FileDownloadModel>
            {
                Ray = ray ?? string.Empty,
                Data = new FileDownloadModel(),
            };
            var fileEntities = await _manageFileRepository.GetById(id);
            var result = new FileDownloadModel();
            if (fileEntities != null && fileEntities.Content != null)
            {
                var stream = new MemoryStream(fileEntities.Content);
                IFormFile file = new FormFile(stream, 0, fileEntities.Content.Length, "File", fileEntities.Name);
                result.File = file;
                result.FileContent = fileEntities.Content;
                result.FileName = fileEntities.Name;
            }
            response.Data = result;
            return response;
        }

        private async Task<ApiResponse<List<FileViewModel>>> Validate(string ray, List<InforUploadModel> infors, IList<IFormFile> files)
        {
            ApiResponse<List<FileViewModel>> response = new ApiResponse<List<FileViewModel>>
            {
                Ray = ray ?? string.Empty,
                Data = new List<FileViewModel>(),
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                    ValidateResult = new List<MessageItem>()
                }
            };

            if (files == null)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.MF0102
                });
                response.Message.ValidateResult.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.MF0105
                });
                response.Data = null;
                return response;
            }

            if (infors == null)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.MF0102
                });
                response.Message.ValidateResult.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.MF0107
                });
                response.Data = null;
                return response;
            }

            if (files.Count != infors.Count)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.MF0102
                });
                response.Message.ValidateResult.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.MF0105
                });
                response.Data = null;
                return response;
            }

            List<MessageItem> messages = new List<MessageItem>();

            for (int i = 0; i < infors.Count; i++)
            {
                //validate common
                var message = _validator.Validate(infors[i]);
                for (int j = 0; j < message.Count; j++)
                {
                    message[j].ComponentId = $"File[{i}]_{message[j].ComponentId}";
                }
                messages.AddRange(message);
            }

            if (messages.Count > 0)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.MF0102
                });
                response.Message.ValidateResult = messages;
                response.Data = null;
                return response;
            }

            messages = new List<MessageItem>();
            for (int i = 0; i < infors.Count; i++)
            {
                if (infors[i].Category == CategoryImage.LOGO || infors[i].Category == CategoryImage.PHOTO)
                {
                    //validate extension
                    string extension = Path.GetExtension(files[i].FileName);

                    if (!IsImageExtension(extension))
                    {
                        messages.Add(new MessageItem()
                        {
                            ComponentId = $"File[{i}]",
                            Level = Level.ERROR,
                            LabelCode = Label.MF0109
                        });
                    }
                    else
                    {
                        //validate resolution
                        using (var memoryStream = new MemoryStream())
                        {
                            await files[i].CopyToAsync(memoryStream);
                            if (!ValidateResolution(memoryStream))
                            {
                                messages.Add(new MessageItem()
                                {
                                    ComponentId = $"File[{i}]",
                                    Level = Level.ERROR,
                                    LabelCode = Label.MF0110
                                });
                            }
                        }
                    }
                }

                //validate file size
                if (files[i].Length > 1048576)
                {
                    messages.Add(new MessageItem()
                    {
                        ComponentId = $"File[{i}]",
                        Level = Level.ERROR,
                        LabelCode = Label.MF0111
                    });
                }
            }

            if (messages.Count > 0)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.MF0102
                });
                response.Message.ValidateResult = messages;
                response.Data = null;
                return response;
            }

            return response;
        }

        private bool IsImageExtension(string extension)
        {
            return _validExtensions.Contains(extension.ToLower());
        }

        private bool ValidateResolution(MemoryStream stream)
        {
            using (Image image = Image.FromStream(stream))
            {
                return (image.Height <= 1200 && image.Width <= 1200);
            }
        }
    }
}