﻿using CommonLib.Constants;
using CommonLib.Dtos;
using CommonLib.Models.Client;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Constants;
using EmploymentServiceAPIs.Employee.Dtos.RentalAddress;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using EmploymentServiceAPIs.Employee.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Services.Impl
{
    public class RentalAddressService : IRentalAddressService
    {
        private readonly IRentalAddressRepository _rentalAddressRepository;
        private readonly IEmployeeRepository _employeeRepository;
        public RentalAddressService(IRentalAddressRepository rentalAddressRepository, IEmployeeRepository employeeRepository)
        {
            _rentalAddressRepository = rentalAddressRepository;
            _employeeRepository = employeeRepository;
        }
        public async Task<ApiResponse<Dictionary<string, object>>> GetRentalAddress(int employeeId, ApiRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();
            List<TbEmpRentalAddress> rentalAddresses = await _rentalAddressRepository.GetByEmployeeId(employeeId);
            response.Data.Add("rentalAddresses", rentalAddresses);
            TbEmpEmployee employee = await _employeeRepository.GetById(employeeId);
            response.Data.Add("remarkRentalAddress", employee.RemarkRentalAddress);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> SaveRentalAddress(int employeeId, ApiRequest<RentalAddressSaveModel> request, string userName)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();
            string prefixComponent = "rentalAddresses";
            List<MessageItem> listResultCheck = new List<MessageItem>();

            //validate rental address property
            for (int i = 0; i < request.Data.RentalAddresses.Count; i++)
            {
                var resultCheck = new CommonLib.Validation.Validator(null).Validate(request.Data.RentalAddresses[i], prefixComponent + "[" + i + "].");
                listResultCheck.AddRange(resultCheck);
            }
            if (listResultCheck.Count > 0)
            {
                response.Message.Toast.AddRange(listResultCheck);
                response.Data = null;
                return response;
            }

            List<RentalAddressRequest> listCreate = request.Data.RentalAddresses.Where(x => x.AddressId == 0).ToList();
            List<RentalAddressRequest> listUpdate = request.Data.RentalAddresses.Where(x => x.AddressId != 0).ToList();

            bool isCreate = true;
            bool isUpdate = true;
            if (listCreate != null && listCreate.Count > 0)
            {
                isCreate = await _rentalAddressRepository.Create(employeeId, listCreate, userName);
                if (!isCreate)
                {
                    response.Message.Toast.Add(new MessageItem
                    {
                        Level = Level.ERROR,
                        LabelCode = Label.REN001
                    });
                }
            }
            if (listUpdate != null && listUpdate.Count > 0)
            {
                isUpdate = await _rentalAddressRepository.Update(listUpdate, userName);
                if (!isUpdate)
                {
                    response.Message.Toast.Add(new MessageItem
                    {
                        Level = Level.ERROR,
                        LabelCode = Label.REN002
                    });
                }
            }
            TbEmpEmployee employee = await _employeeRepository.GetById(employeeId);
            employee.RemarkRentalAddress = request.Data.RemarkRentalAddress;
            await _employeeRepository.Edit(employee);
            if (!isCreate || !isUpdate)
            {
                response.Data = null;
                return response;
            }
            List<TbEmpRentalAddress> rentalAddresses = await _rentalAddressRepository.GetByEmployeeId(employeeId);
            response.Data.Add("rentalAddresses", rentalAddresses);
            response.Data.Add("remarkRentalAddress", request.Data.RemarkRentalAddress);
            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.REN003
            });
            return response;
        }
    }
}
