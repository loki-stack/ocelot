﻿using CommonLib.Constants;
using CommonLib.Dtos;
using CommonLib.Models.Client;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Constants;
using EmploymentServiceAPIs.Employee.Dtos.Employee;
using EmploymentServiceAPIs.Employee.Dtos.TaxArrangement;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using EmploymentServiceAPIs.Employee.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Services.Impl
{
    public class TaxService : ITaxService
    {
        protected readonly ITaxRepository _taxRepository;
        protected readonly IEmployeeRepository _employeeRepository;
        public TaxService(ITaxRepository taxRepository, IEmployeeRepository employeeRepository)
        {
            _taxRepository = taxRepository;
            _employeeRepository = employeeRepository;
        }
        public async Task<ApiResponse<Dictionary<string, object>>> GetTaxInfo(int employeeId, ApiRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();
            List<TbEmpTax> taxes = await _taxRepository.GetByEmployeeId(employeeId);
            TbEmpEmployee employee = await _employeeRepository.GetById(employeeId);
            response.Data.Add("taxes", taxes);
            OverseasPayment overseasPayment = new OverseasPayment
            {
                OverseasPayments = employee.OverseasPayments,
                CompanyName = employee.CompanyName,
                Address = employee.Address,
                Amount = employee.Amount,
                Currency = employee.Currency
            };
            response.Data.Add("overseasPayment", overseasPayment);
            response.Data.Add("remarkTax", employee.RemarkTax);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> SaveTaxInfo(int employeeId, ApiRequest<TaxSaveModel> request, string userName)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();
            List<MessageItem> listResultCheck = new List<MessageItem>();

            bool isUpdate = true;
            if (request.Data != null && request.Data.TaxRequests.Count > 0)
            {
                isUpdate = await _taxRepository.Update(request.Data.TaxRequests, userName);
                if (!isUpdate)
                {
                    response.Message.Toast.Add(new MessageItem
                    {
                        Level = Level.ERROR,
                        LabelCode = Label.TAX003
                    });
                }
            }
            TbEmpEmployee employee = await _employeeRepository.GetById(employeeId);
            employee.RemarkTax = request.Data.RemarkTax;
            employee.OverseasPayments = request.Data.OverseasPayment.OverseasPayments;
            employee.CompanyName = request.Data.OverseasPayment.CompanyName;
            employee.Address = request.Data.OverseasPayment.Address;
            employee.Amount = request.Data.OverseasPayment.Amount;
            employee.Currency = request.Data.OverseasPayment.Currency;
            await _employeeRepository.Edit(employee);
            if (!isUpdate)
            {
                response.Data = null;
                return response;
            }
            List<TbEmpTax> taxes = await _taxRepository.GetByEmployeeId(employeeId);
            response.Data.Add("taxes", taxes);
            response.Data.Add("overseasPayment", request.Data.OverseasPayment);
            response.Data.Add("remarkTax", request.Data.RemarkTax);
            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.TAX004
            });
            return response;
        }
    }
}
