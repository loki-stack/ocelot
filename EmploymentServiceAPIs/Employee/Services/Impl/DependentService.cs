﻿using AutoMapper;
using CommonLib.Constants;
using CommonLib.Dtos;
using CommonLib.Models;
using Client = CommonLib.Models.Client;
using CommonLib.Repositories.Interfaces;
using CommonLib.Validation;
using EmploymentServiceAPIs.Common.Dtos;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Common.Repositories.Interfaces;
using EmploymentServiceAPIs.Employee.Constants;
using EmploymentServiceAPIs.Employee.Dtos.Dependent;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using EmploymentServiceAPIs.Employee.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using CommonLib.Models.Client;
using Core = CommonLib.Models.Core;

namespace EmploymentServiceAPIs.Employee.Services.Impl
{
    public class DependentService : IDependentService
    {
        private readonly IDependentRepository _dependentRepository;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IEntitySettingRepository _entitySettingRepository;
        private readonly ITbCfgI18NRepository _tbCfgI18NRepository;
        private readonly IMapper _mapper;
        private readonly IValidator _validator;

        public DependentService(IDependentRepository dependentRepository,
                                IEmployeeRepository employeeRepository,
                                IEntitySettingRepository entitySettingRepository,
                                ITbCfgI18NRepository tbCfgI18NRepository,
                                IMapper mapper,
                                IValidator validator)
        {
            _dependentRepository = dependentRepository;
            _employeeRepository = employeeRepository;
            _entitySettingRepository = entitySettingRepository;
            _tbCfgI18NRepository = tbCfgI18NRepository;
            _mapper = mapper;
            _validator = validator;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> Save(int clientId, int entityId, int employeeId, string ray, string username, DependentSaveModel dependentSM)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>
            {
                Ray = ray ?? string.Empty,
                Data = new Dictionary<string, object>(),
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                    ValidateResult = new List<MessageItem>()
                }
            };

            //validate data
            response = Validate(ray, employeeId, dependentSM);
            if (response.Message.ValidateResult.Count > 0)
            {
                return response;
            }

            var lstDependentCreate = dependentSM.Dependents.Where(x => x.DependentId == 0).ToList();
            var lstDependentUpdate = dependentSM.Dependents.Where(x => x.DependentId != 0).ToList();
            DateTime currentDate = DateTime.Now;
            Core.TbCfgSetting entityLanguage = await _entitySettingRepository.GetSettingOfEntity(clientId, entityId);

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    //Update remark dependent in Employee
                    var employee = await _employeeRepository.GetById(employeeId);
                    employee.RemarkDependent = dependentSM.Remark;
                    employee.ModifiedBy = username;
                    employee.ModifiedDt = currentDate;
                    await _employeeRepository.Edit(employee);

                    //create dependent
                    if (lstDependentCreate.Count > 0)
                    {
                        response = await Create(ray, employeeId, username, currentDate, entityLanguage, scope, lstDependentCreate);

                        if (response.Message.ValidateResult.Count > 0)
                        {
                            return response;
                        }
                    }

                    //update dependent
                    if (lstDependentUpdate.Count > 0)
                    {
                        response = await Update(ray, employeeId, username, currentDate, entityLanguage, scope, lstDependentUpdate);

                        if (response.Message.ValidateResult.Count > 0)
                        {
                            return response;
                        }
                    }

                    response.Message.Toast.Add(new MessageItem
                    {
                        Level = Level.SUCCESS,
                        LabelCode = Label.DE0123
                    });

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }

            var dependentVms = await GetByEmployeeId(clientId, entityId, employeeId, ray);
            response.Data.Add("dependents", dependentVms.Data);
            return response;
        }

        public async Task<ApiResponse<DependentDto>> GetByEmployeeId(int clientId, int entityId, int employeeId, string ray)
        {
            ApiResponse<DependentDto> response = new ApiResponse<DependentDto>
            {
                Ray = ray ?? string.Empty,
                Data = new DependentDto()
            };
            string[] includes = { "TbEmpDependent" };
            var employee = await _employeeRepository.GetSingleByCondition(x => x.EmployeeId == employeeId, includes);
            var dependentVms = _mapper.Map<TbEmpEmployee, DependentDto>(employee);

            //Get firstname, lastname...
            //if (dependentVms.Dependents.Count > 0)
            //{
            //    List<string> textKeys = new List<string>();
            //    foreach (var dependent in dependentVms.Dependents)
            //    {
            //        textKeys.Add(dependent.FirstName);
            //        textKeys.Add(dependent.MiddleName);
            //        textKeys.Add(dependent.ChristianName);
            //        textKeys.Add(dependent.LastName);
            //    }

            //    var listI18N = await _tbCfgI18NRepository.GetMulti(x => textKeys.Contains(x.TextKey));
            //    TbCfgSetting entityLanguage = await _entitySettingRepository.GetSettingOfEntity(clientId, entityId);

            //    if (listI18N != null)
            //    {
            //        foreach (var dependent in dependentVms.Dependents)
            //        {
            //            GetNameDependent(dependent, entityLanguage, listI18N);
            //        }
            //    }
            //}

            response.Data = dependentVms;
            return response;
        }

        public async Task<ApiResponse<DependentViewModel>> GetById(int clientId, int entityId, int id, string ray)
        {
            ApiResponse<DependentViewModel> response = new ApiResponse<DependentViewModel>
            {
                Ray = ray ?? string.Empty,
                Data = new DependentViewModel()
            };
            var dependent = await _dependentRepository.GetById(id);

            var dependentVm = _mapper.Map<TbEmpDependent, DependentViewModel>(dependent);
            //if (dependentVm != null)
            //{
            //    //Get firstname, lastname...
            //    List<string> textKeys = new List<string>();
            //    textKeys.Add(dependentVm.FirstName);
            //    textKeys.Add(dependentVm.MiddleName);
            //    textKeys.Add(dependentVm.ChristianName);
            //    textKeys.Add(dependentVm.LastName);
            //    var listI18N = await _tbCfgI18NRepository.GetMulti(x => textKeys.Contains(x.TextKey));
            //    TbCfgSetting entityLanguage = await _entitySettingRepository.GetSettingOfEntity(clientId, entityId);

            //    if (listI18N != null)
            //    {
            //        dependentVm = GetNameDependent(dependentVm, entityLanguage, listI18N);
            //    }
            //}

            response.Data = dependentVm;
            return response;
        }

        private DependentViewModel GetNameDependent(DependentViewModel dependent, Core.TbCfgSetting entityLanguage, List<Client.TbCfgI18n> listI18N)
        {
            //Get data firstname
            Client.TbCfgI18n i18N = listI18N.Where(x => x.TextKey == dependent.FirstName && x.LanguageTag == entityLanguage.PrimaryLanguage).FirstOrDefault();

            Client.TbCfgI18n i18NDefault = listI18N.Where(x => x.TextKey == dependent.FirstName && x.DefaultFlag == true).FirstOrDefault();

            dependent.FirstNamePrimary = i18N != null ? i18N.TextValue : (i18NDefault != null ? i18NDefault.TextValue : string.Empty);

            i18N = listI18N.Where(x => x.TextKey == dependent.FirstName && x.LanguageTag == entityLanguage.SecondaryLanguage).FirstOrDefault();

            dependent.FirstNameSecondary = i18N != null ? i18N.TextValue : (i18NDefault != null ? i18NDefault.TextValue : string.Empty);

            //Get data middlename
            i18N = listI18N.Where(x => x.TextKey == dependent.MiddleName && x.LanguageTag == entityLanguage.PrimaryLanguage).FirstOrDefault();
            i18NDefault = listI18N.Where(x => x.TextKey == dependent.MiddleName && x.DefaultFlag == true).FirstOrDefault();

            dependent.MiddleNamePrimary = i18N != null ? i18N.TextValue : (i18NDefault != null ? i18NDefault.TextValue : string.Empty);

            i18N = listI18N.Where(x => x.TextKey == dependent.MiddleName && x.LanguageTag == entityLanguage.SecondaryLanguage).FirstOrDefault();

            dependent.MiddleNameSecondary = i18N != null ? i18N.TextValue : (i18NDefault != null ? i18NDefault.TextValue : string.Empty);

            //Get data christianname
            i18N = listI18N.Where(x => x.TextKey == dependent.ChristianName && x.LanguageTag == entityLanguage.PrimaryLanguage).FirstOrDefault();
            i18NDefault = listI18N.Where(x => x.TextKey == dependent.ChristianName && x.DefaultFlag == true).FirstOrDefault();

            dependent.ChristianNamePrimary = i18N != null ? i18N.TextValue : (i18NDefault != null ? i18NDefault.TextValue : string.Empty);

            i18N = listI18N.Where(x => x.TextKey == dependent.ChristianName && x.LanguageTag == entityLanguage.SecondaryLanguage).FirstOrDefault();

            dependent.ChristianNameSecondary = i18N != null ? i18N.TextValue : (i18NDefault != null ? i18NDefault.TextValue : string.Empty);

            //Get data lastname
            i18N = listI18N.Where(x => x.TextKey == dependent.LastName && x.LanguageTag == entityLanguage.PrimaryLanguage).FirstOrDefault();
            i18NDefault = listI18N.Where(x => x.TextKey == dependent.ChristianName && x.DefaultFlag == true).FirstOrDefault();

            dependent.LastNamePrimary = i18N != null ? i18N.TextValue : (i18NDefault != null ? i18NDefault.TextValue : string.Empty);

            i18N = listI18N.Where(x => x.TextKey == dependent.LastName && x.LanguageTag == entityLanguage.SecondaryLanguage).FirstOrDefault();

            dependent.LastNameSecondary = i18N != null ? i18N.TextValue : (i18NDefault != null ? i18NDefault.TextValue : string.Empty);

            return dependent;
        }

        private ApiResponse<Dictionary<string, object>> Validate(string ray, int employeeId, DependentSaveModel dependentSM)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>
            {
                Ray = ray ?? string.Empty,
                Data = new Dictionary<string, object>(),
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                    ValidateResult = new List<MessageItem>()
                }
            };
            //check exist employee
            var isExist = _employeeRepository.CheckExistEmployeeById(employeeId);
            if (!isExist)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.DE0122
                });
                response.Message.ValidateResult.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.EMP001
                });
                response.Data = null;
                return response;
            }

            //validate data
            List<MessageItem> messages = new List<MessageItem>();
            messages = _validator.Validate(dependentSM);

            for (int i = 0; i < dependentSM.Dependents.Count; i++)
            {
                var messageDependent = _validator.Validate(dependentSM.Dependents[i]);
                for (int j = 0; j < messageDependent.Count; j++)
                {
                    messageDependent[j].ComponentId = $"dependent[{i}]_{messageDependent[j].ComponentId}";
                }
                messages.AddRange(messageDependent);
            }

            //return respone when data invalid
            if (messages.Count > 0)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.DE0122
                });
                response.Message.ValidateResult = messages;
                response.Data = null;
                return response;
            }

            return response;
        }

        private async Task<ApiResponse<Dictionary<string, object>>> Create(string ray, int employeeId, string username, DateTime currentDate, Core.TbCfgSetting entityLanguage, TransactionScope scope, List<DependentModel> dependentModels)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>
            {
                Ray = ray ?? string.Empty,
                Data = new Dictionary<string, object>(),
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                    ValidateResult = new List<MessageItem>()
                }
            };

            for (int i = 0; i < dependentModels.Count; i++)
            {
                dependentModels[i].EmployeeId = employeeId;
                dependentModels[i].CreatedBy = username;
                dependentModels[i].CreatedDt = currentDate;
                dependentModels[i].ModifiedBy = username;
                dependentModels[i].ModifiedDt = currentDate;
            }
            var dependents = _mapper.Map<List<DependentModel>, List<TbEmpDependent>>(dependentModels);
            await _dependentRepository.Create(dependents);

            //update primary, secondary langugage
            //for (int i = 0; i < dependents.Count; i++)
            //{
            //    dependents[i].FirstName = TenantObject.DEPENDENT + dependents[i].DependentId + "_" + Component.FIRST_NAME;
            //    dependents[i].MiddleName = TenantObject.DEPENDENT + dependents[i].DependentId + "_" + Component.MIDDLE_NAME;
            //    dependents[i].LastName = TenantObject.DEPENDENT + dependents[i].DependentId + "_" + Component.LAST_NAME;
            //    dependents[i].ChristianName = TenantObject.DEPENDENT + dependents[i].DependentId + "_" + Component.CHRISTIAN_NAME;
            //}

            //await _dependentRepository.Update(dependents);

            //create
            //await CreateUpdateI18NForDependent(username, entityLanguage, dependentModels, dependents, true);

            return response;
        }

        private async Task<ApiResponse<Dictionary<string, object>>> Update(string ray, int employeeId, string username, DateTime currentDate, Core.TbCfgSetting entityLanguage, TransactionScope scope, List<DependentModel> dependentModels)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>
            {
                Ray = ray ?? string.Empty,
                Data = new Dictionary<string, object>(),
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                    ValidateResult = new List<MessageItem>()
                }
            };
            var lstIdNotExist = new List<int>();

            for (int i = 0; i < dependentModels.Count; i++)
            {
                var dependentEntity = await _dependentRepository.GetById(dependentModels[i].DependentId);
                if (dependentEntity == null)
                {
                    lstIdNotExist.Add(dependentModels[i].DependentId);
                }
                else
                {
                    dependentModels[i].EmployeeId = dependentEntity.EmployeeId;
                    dependentModels[i].CreatedBy = dependentEntity.CreatedBy;
                    dependentModels[i].CreatedDt = dependentEntity.CreatedDt;
                    dependentModels[i].ModifiedBy = username;
                    dependentModels[i].ModifiedDt = currentDate;
                }
            }

            if (lstIdNotExist.Count > 0)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.DE0122
                });

                response.Message.ValidateResult.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.DE0124
                });

                scope.Dispose();

                response.Data.Add("ids", lstIdNotExist);
                return response;
            }
            var dependents = _mapper.Map<List<DependentModel>, List<TbEmpDependent>>(dependentModels);
            //foreach (var dependent in dependents)
            //{
            //    var dependentEntity = await _dependentRepository.GetById(dependent.DependentId);
            //    dependent.FirstName = dependentEntity.FirstName;
            //    dependent.MiddleName = dependentEntity.MiddleName;
            //    dependent.ChristianName = dependentEntity.ChristianName;
            //    dependent.LastName = dependentEntity.LastName;
            //}
            await _dependentRepository.Update(dependents);

            //update i18N
            //await CreateUpdateI18NForDependent(username, entityLanguage, dependentModels, dependents, false);

            return response;
        }

        private async Task CreateUpdateI18NForDependent(string username, Core.TbCfgSetting entityLanguage, List<DependentModel> dependentModels, List<TbEmpDependent> dependents, bool isCreate)
        {
            List<CommonLib.Models.Client.TbCfgI18n> lstI18N = new List<CommonLib.Models.Client.TbCfgI18n>();
            for (int i = 0; i < dependents.Count; i++)
            {
                lstI18N.AddRange(ProcessI18NData(dependents[i], dependentModels[i], entityLanguage, username, isCreate));
            }

            if (isCreate)
                await _tbCfgI18NRepository.CreateMany(lstI18N);
            else
                await _tbCfgI18NRepository.UpdateMany(lstI18N);
        }

        private List<CommonLib.Models.Client.TbCfgI18n> ProcessI18NData(TbEmpDependent dependent, DependentModel dependentModel, Core.TbCfgSetting entityLanguage, string username, bool isCreate)
        {
            List<TbCfgI18NDto> listCfgI18NDto = new List<TbCfgI18NDto>();
            //firstName
            listCfgI18NDto.Add(new TbCfgI18NDto(entityLanguage.PrimaryLanguage, dependent.FirstName, dependentModel.FirstNamePrimary, true, username, isCreate));
            listCfgI18NDto.Add(new TbCfgI18NDto(entityLanguage.SecondaryLanguage, dependent.FirstName, dependentModel.FirstNameSecondary, false, username, isCreate));

            //middleName
            listCfgI18NDto.Add(new TbCfgI18NDto(entityLanguage.PrimaryLanguage, dependent.MiddleName, dependentModel.MiddleNamePrimary, true, username, isCreate));
            listCfgI18NDto.Add(new TbCfgI18NDto(entityLanguage.SecondaryLanguage, dependent.MiddleName, dependentModel.MiddleNameSecondary, false, username, isCreate));

            //lastName
            listCfgI18NDto.Add(new TbCfgI18NDto(entityLanguage.PrimaryLanguage, dependent.LastName, dependentModel.LastNamePrimary, true, username, isCreate));
            listCfgI18NDto.Add(new TbCfgI18NDto(entityLanguage.SecondaryLanguage, dependent.LastName, dependentModel.LastNameSecondary, false, username, isCreate));

            //christianName
            listCfgI18NDto.Add(new TbCfgI18NDto(entityLanguage.PrimaryLanguage, dependent.ChristianName, dependentModel.ChristianNamePrimary, true, username, isCreate));
            listCfgI18NDto.Add(new TbCfgI18NDto(entityLanguage.SecondaryLanguage, dependent.ChristianName, dependentModel.ChristianNameSecondary, false, username, isCreate));

            List<CommonLib.Models.Client.TbCfgI18n> listCfgI18N = _mapper.Map<List<TbCfgI18NDto>, List<CommonLib.Models.Client.TbCfgI18n>>(listCfgI18NDto);

            return listCfgI18N;
        }
    }
}