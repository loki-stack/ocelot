﻿using AutoMapper;
using CommonLib.Constants;
using CommonLib.Dtos;
using CommonLib.Models.Client;
using CommonLib.Validation;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Constants;
using EmploymentServiceAPIs.Employee.Dtos.Termination;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using EmploymentServiceAPIs.Employee.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Transactions;

namespace EmploymentServiceAPIs.Employee.Services.Impl
{
    public class TerminationService : ITerminationService
    {
        private readonly ITerminationRepository _terminationRepository;
        private readonly IEmploymentContractRepository _empContractRepository;
        private readonly IEmploymentStatusRepository _employmentStatusRepository;
        private readonly IMapper _mapper;
        private readonly IValidator _validator;

        public TerminationService(ITerminationRepository terminationRepository,
                                  IEmploymentContractRepository empContractRepository,
                                  IEmploymentStatusRepository employmentStatusRepository,
                                  IMapper mapper,
                                  IValidator validator)
        {
            _terminationRepository = terminationRepository;
            _empContractRepository = empContractRepository;
            _employmentStatusRepository = employmentStatusRepository;
            _mapper = mapper;
            _validator = validator;
        }

        public async Task<ApiResponse<TerminationViewModel>> GetById(int id, string ray)
        {
            ApiResponse<TerminationViewModel> response = new ApiResponse<TerminationViewModel>
            {
                Ray = ray ?? string.Empty,
                Data = new TerminationViewModel()
            };

            var termination = await _terminationRepository.GetById(id);
            var terminationVm = _mapper.Map<TbEmpTermination, TerminationViewModel>(termination);
            response.Data = terminationVm;
            return response;
        }

        public async Task<ApiResponse<TerminationViewModel>> GetByEmpContractId(int empContractId, string ray)
        {
            ApiResponse<TerminationViewModel> response = new ApiResponse<TerminationViewModel>
            {
                Ray = ray ?? string.Empty,
                Data = new TerminationViewModel()
            };

            var termination = await _terminationRepository.GetByEmpContractId(empContractId);
            var terminationVm = _mapper.Map<TbEmpTermination, TerminationViewModel>(termination);
            response.Data = terminationVm;
            return response;
        }

        public async Task<ApiResponse<TerminationViewModel>> Save(string ray, string username, TerminationSaveModel terminationSM)
        {
            ApiResponse<TerminationViewModel> response = new ApiResponse<TerminationViewModel>
            {
                Ray = ray ?? string.Empty,
                Data = new TerminationViewModel(),
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                    ValidateResult = new List<MessageItem>()
                }
            };

            //validate data
            response = Validate(ray, terminationSM);
            if (response.Message.ValidateResult.Count > 0 || response.Message.Toast.Count > 0)
            {
                return response;
            }

            DateTime currentDate = DateTime.Now;
            int? id = 0;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    //create, update termination
                    if (terminationSM.Id == 0)
                    {
                        terminationSM.CreatedBy = username;
                        terminationSM.CreatedDt = currentDate;
                        terminationSM.ModifiedBy = username;
                        terminationSM.ModifiedDt = currentDate;
                        var termination = _mapper.Map<TerminationSaveModel, TbEmpTermination>(terminationSM);
                        id = await _terminationRepository.Create(termination);
                    }
                    else
                    {
                        var termination = await _terminationRepository.GetById(terminationSM.Id);
                        terminationSM.EmploymentContractId = termination.EmploymentContractId;
                        terminationSM.CreatedBy = termination.CreatedBy;
                        terminationSM.CreatedDt = termination.CreatedDt;
                        terminationSM.ModifiedBy = username;
                        terminationSM.ModifiedDt = currentDate;
                        termination = _mapper.Map<TerminationSaveModel, TbEmpTermination>(terminationSM);
                        id = await _terminationRepository.Update(termination);
                    }

                    //create employment status
                    List<TbEmpEmploymentStatus> empStatuses = new List<TbEmpEmploymentStatus>();
                    TbEmpEmploymentStatus empStatus = new TbEmpEmploymentStatus();
                    if (terminationSM.TerminationContractEndDate.Value.Date.CompareTo(currentDate.Date) <= 0)
                        empStatus.Confirmed = true;
                    else
                        empStatus.Confirmed = false;
                    empStatus.StartDate = terminationSM.TerminationContractEndDate.Value;
                    empStatus.Status = EmploymentStatus.TERMINATED;
                    empStatus.EmploymentContractId = terminationSM.EmploymentContractId.Value;
                    empStatus.CreatedBy = username;
                    empStatus.CreatedDt = currentDate;
                    empStatus.ModifiedBy = username;
                    empStatus.ModifiedDt = currentDate;
                    empStatuses.Add(empStatus);

                    await _employmentStatusRepository.CreateMany(empStatuses);
                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }

            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.TE0101
            });

            var terminationVms = await GetById(id.Value, ray);
            response.Data = terminationVms.Data;
            return response;
        }

        private ApiResponse<TerminationViewModel> Validate(string ray, TerminationSaveModel terminationSM)
        {
            ApiResponse<TerminationViewModel> response = new ApiResponse<TerminationViewModel>
            {
                Ray = ray ?? string.Empty,
                Data = new TerminationViewModel(),
                Message = new Message()
                {
                    Toast = new List<MessageItem>(),
                    ValidateResult = new List<MessageItem>()
                }
            };
            //check exist employment contract
            var isExist = _empContractRepository.CheckExistEContractById(terminationSM.EmploymentContractId.Value);
            if (!isExist)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.EMP0143,
                });
                response.Data = null;
                return response;
            }

            if (terminationSM.Id != 0)
            {
                //check exist termination
                isExist = _terminationRepository.CheckExistTermination(terminationSM.Id);
                if (!isExist)
                {
                    response.Message.Toast.Add(new MessageItem
                    {
                        Level = Level.ERROR,
                        LabelCode = Label.TE0103
                    });
                    response.Data = null;
                    return response;
                }
            }

            //validate data
            List<MessageItem> messages = new List<MessageItem>();
            messages = _validator.Validate(terminationSM);

            //return respone when data invalid
            if (messages.Count > 0)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.TE0102
                });
                response.Message.ValidateResult = messages;
                response.Data = null;
                return response;
            }

            if (terminationSM.Id == 0)
            {
                //check exist termination of contract
                isExist = _terminationRepository.CheckExistTerminationOfContract(terminationSM.EmploymentContractId.Value);
                if (isExist)
                {
                    response.Message.Toast.Add(new MessageItem
                    {
                        Level = Level.ERROR,
                        LabelCode = Label.TE0106
                    });
                    response.Data = null;
                    return response;
                }
            }

            return response;
        }
    }
}