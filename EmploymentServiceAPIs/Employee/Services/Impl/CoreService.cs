﻿using CommonLib.Dtos;
using EmploymentServiceAPIs.Employee.Services.Interfaces;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Services.Impl
{
    public class CoreService : ICoreService
    {
        private readonly AppSettings _appSettings;

        public CoreService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public async Task<Dictionary<string, List<GenericCodeResult>>> GetGenericCodes(List<string> genericCodeStrs, string token, StdRequestParam requestParam)
        {
            Dictionary<string, List<GenericCodeResult>> result = new Dictionary<string, List<GenericCodeResult>>();
            try
            {
                var myUri = new Uri(_appSettings.CoreServiceGenericCode);
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                string locale = requestParam == null ? "" : requestParam.Locale;
                int clientId = requestParam == null ? 0 : requestParam.ClientId;
                int entityId = requestParam == null ? 0 : requestParam.EntityId;

                string param = string.Empty;
                param = param + "Parameter.Locale=" + locale + "&";
                param = param + "Data.ClientId=" + clientId + "&";
                param = param + "Data.EntityId=" + entityId + "&";
                foreach (var code in genericCodeStrs)
                {
                    param = param + "Parameter.GenericCodeList=" + code + "&";
                }
                param = param + "&Data.ResData=1";
                HttpResponseMessage response = await client.GetAsync(myUri + "?" + param);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                ApiResponse<Dictionary<string, object>> responseObj = JsonConvert.DeserializeObject<ApiResponse<Dictionary<string, object>>>(responseBody);
                var genericCodes = responseObj.Data["genericCodes"];
                result = JsonConvert.DeserializeObject<Dictionary<string, List<GenericCodeResult>>>(genericCodes.ToString());
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}
