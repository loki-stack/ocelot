﻿using CommonLib.Dtos;
using CommonLib.Models.Client;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using EmploymentServiceAPIs.Employee.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Services.Impl
{
    public class WorkingScheduleService : IWorkingScheduleService
    {
        private readonly IWorkingSheduleRepository _workingSheduleRepository;

        public WorkingScheduleService(IWorkingSheduleRepository workingSheduleRepository)
        {
            _workingSheduleRepository = workingSheduleRepository;
        }

        public async Task<ApiResponse<List<TbCfgWorkingSchedule>>> GetAll(string ray)
        {
            ApiResponse<List<TbCfgWorkingSchedule>> response = new ApiResponse<List<TbCfgWorkingSchedule>>
            {
                Ray = ray ?? string.Empty,
                Data = new List<TbCfgWorkingSchedule>(),
            };
            response.Data = await _workingSheduleRepository.GetAll();
            return response;
        }
    }
}