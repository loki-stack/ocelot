﻿using CommonLib.Dtos;
using EmploymentServiceAPIs.Common.Constants;
using EmploymentServiceAPIs.Common.Dtos;
using EmploymentServiceAPIs.Employee.Dtos.BankAccount;
using EmploymentServiceAPIs.Employee.Dtos.Dependent;
using EmploymentServiceAPIs.Employee.Dtos.Employee;
using EmploymentServiceAPIs.Employee.Dtos.EmploymentContract;
using EmploymentServiceAPIs.Employee.Dtos.EmploymentStatus;
using EmploymentServiceAPIs.Employee.Dtos.Movement;
using EmploymentServiceAPIs.Employee.Dtos.RentalAddress;
using EmploymentServiceAPIs.Employee.Dtos.TaxArrangement;
using EmploymentServiceAPIs.Employee.Dtos.Termination;
using EmploymentServiceAPIs.Employee.Services.Interfaces;
using EmploymentServiceAPIs.ManageEntity.Dtos;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Controllers
{
    [Route("api/EMP/[action]")]
    [ApiController]
    [Authorize]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;
        private readonly IDependentService _dependentService;
        private readonly IBankAccountService _bankAccountService;
        private readonly IEmploymentContractService _employmentContractService;
        private readonly IMovementService _movementService;
        private readonly IEmergencyContactService _emergencyContactService;
        private readonly ITaxService _taxService;
        private readonly IRentalAddressService _rentalAddressService;
        private readonly IAttachmentService _attachmentService;
        private readonly IEmploymentStatusService _employmentStatusService;
        private readonly ITerminationService _terminationService;
        private readonly ILogger<EmployeeController> _logger;

        public EmployeeController(IEmployeeService employeeService,
                                  IDependentService dependentService,
                                  IBankAccountService bankAccountService,
                                  IEmergencyContactService emergencyContactService,
                                  ITaxService taxService,
                                  IEmploymentContractService employmentContractService,
                                  IMovementService movementService,
                                  IRentalAddressService rentalAddressService,
                                  IAttachmentService attachmentService,
                                  IEmploymentStatusService employmentStatusService,
                                  ITerminationService terminationService,
                                  ILogger<EmployeeController> logger)
        {
            _employeeService = employeeService;
            _dependentService = dependentService;
            _bankAccountService = bankAccountService;
            _employmentContractService = employmentContractService;
            _movementService = movementService;
            _emergencyContactService = emergencyContactService;
            _taxService = taxService;
            _rentalAddressService = rentalAddressService;
            _attachmentService = attachmentService;
            _employmentStatusService = employmentStatusService;
            _terminationService = terminationService;
            _logger = logger;
        }

        #region New Hire

        [HttpPost()]
        [Authorize(Roles = "EMP0602")]
        [ActionName("EMP0602/Employees/NewHire")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> NewHire([FromBody] ApiRequest<NewHireRequest> request)
        {
            try
            {
                string userName = User.FindFirst(ClaimTypes.Name).Value;
                var response = await _employeeService.NewHire(request, userName);
                if (response == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        #endregion New Hire

        #region Get All

        [HttpGet()]
        [Authorize(Roles = "EMP0601")]
        [ActionName("EMP0601/Employees")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAll([FromQuery] ApiRequest<PaginationRequest> request)
        {
            try
            {
                if (request.Data == null) request.Data = new PaginationRequest();
                var response = await _employeeService.GetAll(request);
                if (response == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet()]
        [Authorize(Roles = "EMP0601")]
        [ActionName("EMP0601/Employees/{employeeId}/Profile")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetProfile([FromRoute] int employeeId, [FromQuery] ApiRequest request)
        {
            try
            {
                var response = await _employeeService.GetProfile(employeeId, request);
                if (response == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet]
        [Authorize(Roles = "EMP0601")]
        [ActionName("EMP0601/Employees/{employeeId}/CompanyInfo")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetCompanyInfo([FromRoute] int employeeId, [FromQuery] ApiRequest request)
        {
            try
            {
                var response = await _employeeService.GetCompanyInfo(employeeId, request);
                if (response == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        [Authorize(Roles = "EMP0602")]
        [ActionName("EMP0602/Employees/{employeeId}/CompanyInfo")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> SaveCompanyInfo([FromRoute] int employeeId, [FromBody] ApiRequest<CompanyInformation> request)
        {
            try
            {
                string userName = User.FindFirst(ClaimTypes.Name).Value;
                var response = await _employeeService.SaveCompanyInfo(employeeId, request, userName);
                if (response == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        #endregion Get All

        #region Basic Info

        [HttpGet()]
        [Authorize(Roles = "EMP0601")]
        [ActionName("EMP0601/Employees/{employeeId}/BasicInfo")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetBasicInfo([FromRoute] int employeeId, [FromQuery] ApiRequest request)
        {
            try
            {
                var response = await _employeeService.GetBasicInfo(employeeId, request);
                if (response == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost()]
        [Authorize(Roles = "EMP0606")]
        [ActionName("EMP0606/Employees/{employeeId}/BasicInfo")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> SaveBasicInfo([FromRoute] int employeeId, [FromBody] ApiRequest<EmployeeSaveModel> request)
        {
            try
            {
                string userName = User.FindFirst(ClaimTypes.Name).Value;
                var response = await _employeeService.SaveBasicInfo(employeeId, request, userName);
                if (response == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        #endregion Basic Info

        #region Tax

        [HttpGet()]
        [Authorize(Roles = "EMP0601")]
        [ActionName("EMP0601/Employees/{employeeId}/Tax")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetTaxInfo([FromRoute] int employeeId, [FromQuery] ApiRequest request)
        {
            try
            {
                var response = await _taxService.GetTaxInfo(employeeId, request);
                if (response == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost()]
        [Authorize(Roles = "EMP0606")]
        [ActionName("EMP0606/Employees/{employeeId}/Tax")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> SaveTaxInfo([FromRoute] int employeeId, [FromBody] ApiRequest<TaxSaveModel> request)
        {
            try
            {
                string userName = User.FindFirst(ClaimTypes.Name).Value;
                var response = await _taxService.SaveTaxInfo(employeeId, request, userName);
                if (response == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        #endregion Tax

        #region Movement

        [HttpGet()]
        [Authorize(Roles = "EMP0601")]
        [ActionName("EMP0601/Employees/Employments/{employmentId}/Movement")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<List<MovementViewModel>>>> GetMovementByEmploymentId([FromRoute] int employmentId, [FromQuery] ApiRequest request)
        {
            try
            {
                var result = await _movementService.GetByEmploymentId(employmentId, request.Ray);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet()]
        [Authorize(Roles = "EMP0601")]
        [ActionName("EMP0601/Employees/Employments/{employmentId}/Movement/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<MovementViewModel>>> GetMovementById([FromRoute] int employmentId, [FromRoute] int id, [FromQuery] ApiRequest request)
        {
            try
            {
                var result = await _movementService.GetById(id, request.Ray);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        [Authorize(Roles = "EMP0606")]
        [ActionName("EMP0606/Employees/Employments/{employmentId}/Movement")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<MovementViewModel>>> SaveMovement([FromRoute] int employmentId, [FromBody] ApiRequest<List<MovementModel>> request)
        {
            try
            {
                string username = User.FindFirst(ClaimTypes.Name).Value;
                var response = await _movementService.Save(employmentId, request.Ray, username, request.Data);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        #endregion Movement

        #region Rental Address

        [HttpGet()]
        [Authorize(Roles = "EMP0606")]
        [ActionName("EMP0601/Employees/{employeeId}/RentalAddress")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetRentalAddress([FromRoute] int employeeId, [FromQuery] ApiRequest request)
        {
            try
            {
                var response = await _rentalAddressService.GetRentalAddress(employeeId, request);
                if (response == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost()]
        [Authorize(Roles = "EMP0606")]
        [ActionName("EMP0606/Employees/{employeeId}/RentalAddress")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> SaveRentalAddress([FromRoute] int employeeId, [FromBody] ApiRequest<RentalAddressSaveModel> request)
        {
            try
            {
                string userName = User.FindFirst(ClaimTypes.Name).Value;
                var response = await _rentalAddressService.SaveRentalAddress(employeeId, request, userName);
                if (response == null)
                {
                    return BadRequest(response);
                }
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        #endregion Rental Address

        #region Dependent

        [HttpGet]
        [Authorize(Roles = "EMP0601")]
        [ActionName("EMP0601/Employees/{employeeId}/Dependent")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<List<DependentViewModel>>>> GetDependentByEmployeeId([FromRoute] int employeeId, [FromQuery] ApiRequest request)
        {
            try
            {
                var result = await _dependentService.GetByEmployeeId(request.Parameter.ClientId, request.Parameter.EntityId, employeeId, request.Ray);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet]
        [Authorize(Roles = "EMP0601")]
        [ActionName("EMP0601/Employees/{employeeId}/Dependent/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<List<DependentViewModel>>>> GetDependentById([FromRoute] int employeeId, [FromRoute] int id, [FromQuery] ApiRequest request)
        {
            try
            {
                var result = await _dependentService.GetById(request.Parameter.ClientId, request.Parameter.EntityId, id, request.Ray);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        [Authorize(Roles = "EMP0606")]
        [ActionName("EMP0606/Employees/{employeeId}/Dependent")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<DependentViewModel>>> SaveDependent([FromRoute] int employeeId, [FromBody] ApiRequest<DependentSaveModel> request)
        {
            try
            {
                string username = User.FindFirst(ClaimTypes.Name).Value;
                var response = await _dependentService.Save(request.Parameter.ClientId, request.Parameter.EntityId, employeeId, request.Ray, username, request.Data);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        #endregion Dependent

        #region Bank Account

        [HttpGet()]
        [Authorize(Roles = "EMP0601")]
        [ActionName("EMP0601/Employees/{employeeId}/BankAccount")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<BankAccountDto>>> GetBankAccountByEmployeeId([FromRoute] int employeeId, [FromQuery] ApiRequest request)
        {
            try
            {
                var result = await _bankAccountService.GetByEmployeeId(employeeId, request.Ray);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet()]
        [Authorize(Roles = "EMP0601")]
        [ActionName("EMP0601/Employees/{employeeId}/BankAccount/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<List<BankAccountViewModel>>>> GetBankAccountById([FromRoute] int employeeId, [FromRoute] int id, [FromQuery] ApiRequest request)
        {
            try
            {
                var result = await _bankAccountService.GetById(id, request.Ray);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        [Authorize(Roles = "EMP0606")]
        [ActionName("EMP0606/Employees/{employeeId}/BankAccount")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<BankAccountViewModel>>> SaveBankAccount([FromRoute] int employeeId, [FromBody] ApiRequest<BankAccountSaveModel> request)
        {
            try
            {
                string username = User.FindFirst(ClaimTypes.Name).Value;
                var response = await _bankAccountService.Save(employeeId, request, username);
                if (response.Message.ValidateResult.Count > 0)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        #endregion Bank Account

        #region Employment Contract

        [HttpGet()]
        [Authorize(Roles = "EMP0601")]
        [ActionName("EMP0601/Employees/{employeeId}/Employment")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<List<EmploymentContractViewModel>>>> GetEmploymentByEmployeeId([FromRoute] int employeeId, [FromQuery] ApiRequest request)
        {
            try
            {
                var result = await _employmentContractService.GetByEmployeeId(employeeId, request.Ray);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet()]
        [Authorize(Roles = "EMP0601")]
        [ActionName("EMP0601/Employees/{employeeId}/Employment/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<EmploymentContractDto>>> GetEmploymentById([FromRoute] int employeeId, [FromRoute] int id, [FromQuery] ApiRequest request)
        {
            try
            {
                var result = await _employmentContractService.GetById(id, request.Ray);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet()]
        [Authorize(Roles = "EMP0601")]
        [ActionName("EMP0601/Employees/{employeeId}/ContractNew")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<List<EmploymentContractViewModel>>>> GetContractNew([FromRoute] int employeeId, [FromQuery] ApiRequest request)
        {
            try
            {
                var result = await _employmentContractService.GetContractNew(employeeId, request.Ray);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        [Authorize(Roles = "EMP0606")]
        [ActionName("EMP0606/Employees/{employeeId}/Employment")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<EmploymentContractDto>>> SaveEmployment([FromRoute] int employeeId, [FromBody] ApiRequest<EmploymentContractSaveModel> request)
        {
            try
            {
                string username = User.FindFirst(ClaimTypes.Name).Value;
                foreach (var contract in request.Data.EmploymentContracts)
                {
                    contract.EmployeeId = employeeId;
                    contract.ClientId = request.Parameter.ClientId;
                    contract.EntityId = request.Parameter.EntityId;
                }

                var response = await _employmentContractService.Save(employeeId, request.Ray, username, request.Data);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        [Authorize(Roles = "EMP0602")]
        [ActionName("EMP0602/Employees/{employeeId}/Employment")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<EmploymentContractDto>>> CreateEmployment([FromRoute] int employeeId, [FromBody] ApiRequest<EmploymentContractAddModel> request)
        {
            try
            {
                string username = User.FindFirst(ClaimTypes.Name).Value;
                var response = await _employmentContractService.Create(employeeId, request.Ray, username, request.Data);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        #endregion Employment Contract

        #region Employment Status

        [HttpGet()]
        [Authorize(Roles = "EMP0601")]
        [ActionName("EMP0601/Employees/{employeeId}/EmploymentStatus")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<List<EmploymentStatusDto>>>> GetEmploymentStatusByEmployeeId([FromRoute] int employeeId, [FromQuery] ApiRequest request)
        {
            try
            {
                var result = await _employmentStatusService.GetByEmployeeId(employeeId, request.Ray);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet()]
        [Authorize(Roles = "EMP0601")]
        [ActionName("EMP0601/Employees/EmploymentStatus/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<EmploymentStatusViewModel>>> GetEmploymentStatusById([FromRoute] int id, [FromQuery] ApiRequest request)
        {
            try
            {
                var result = await _employmentStatusService.GetById(id, request.Ray);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "EMP0604")]
        [ActionName("EMP0604/Employees/{employeeId}/EmploymentStatus")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<List<EmploymentStatusDto>>>> DeleteEmploymentStatus([FromRoute] int employeeId, [FromRoute] int id, [FromBody] ApiRequest request)
        {
            try
            {
                var result = await _employmentStatusService.Delete(id, employeeId, request.Ray);
                if (result.Data == null)
                    BadRequest(result);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        [Authorize(Roles = "EMP0606")]
        [ActionName("EMP0606/Employees/{employeeId}/EmploymentStatus")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<EmploymentContractDto>>> SaveEmploymentStatus([FromRoute] int employeeId, [FromBody] ApiRequest<EmploymentStatusSaveModel> request)
        {
            try
            {
                string username = User.FindFirst(ClaimTypes.Name).Value;

                var response = await _employmentStatusService.Save(employeeId, request.Ray, username, request.Data);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet]
        [Authorize(Roles = "EMP0611")]
        [ActionName("EMP0611/Employees/StatusConfirmation")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> GetStatusConfirmation([FromQuery] ApiRequest<BasePaginationRequest> request)
        {
            try
            {
                string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);
                if (request.Data == null)
                    request.Data = new BasePaginationRequest();
                var result = await _employmentStatusService.GetStatusConfirmation(request.Parameter.ClientId, request.Parameter.EntityId, request.Ray, token, request.Parameter.Locale, request.Data);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "EMP0603")]
        [ActionName("EMP0603/Employees/StatusConfirmation")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<int?>>> UpdateStatusConfirm([FromRoute] int id, [FromBody] ApiRequest request)
        {
            try
            {
                string username = User.FindFirst(ClaimTypes.Name).Value;

                var response = await _employmentStatusService.Update(username, request.Ray, id);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        #endregion Employment Status

        #region Termination

        [HttpGet]
        [Authorize(Roles = "EMP0601")]
        [ActionName("EMP0601/Employees/Employment/Termination/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<TerminationViewModel>>> GetTerminationById([FromRoute] int id, [FromQuery] ApiRequest request)
        {
            try
            {
                var result = await _terminationService.GetById(id, request.Ray);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet]
        [Authorize(Roles = "EMP0601")]
        [ActionName("EMP0601/Employees/Employment/{employmentId}/Termination")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<TerminationViewModel>>> GetTerminationByEmploymentId([FromRoute] int employmentId, [FromQuery] ApiRequest request)
        {
            try
            {
                var result = await _terminationService.GetByEmpContractId(employmentId, request.Ray);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        [Authorize(Roles = "EMP0606")]
        [ActionName("EMP0606/Employees/Employment/Termination")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<TerminationViewModel>>> SaveTermination([FromBody] ApiRequest<TerminationSaveModel> request)
        {
            try
            {
                string username = User.FindFirst(ClaimTypes.Name).Value;

                var response = await _terminationService.Save(request.Ray, username, request.Data);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        #endregion Termination
    }
}