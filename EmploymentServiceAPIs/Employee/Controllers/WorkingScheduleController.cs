﻿using CommonLib.Dtos;
using CommonLib.Models.Client;
using EmploymentServiceAPIs.Employee.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Controllers
{
    [Route("api/CAM/[action]/[controller]")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class WorkingScheduleController : ControllerBase
    {
        private readonly IWorkingScheduleService _workingScheduleService;
        private readonly ILogger<WorkingScheduleController> _logger;

        public WorkingScheduleController(IWorkingScheduleService workingScheduleService, ILogger<WorkingScheduleController> logger)
        {
            _workingScheduleService = workingScheduleService;
            _logger = logger;
        }

        [HttpGet]
        [ActionName("CAM901")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<List<TbCfgWorkingSchedule>>>> GetAll([FromQuery] ApiRequest request)
        {
            try
            {
                var response = await _workingScheduleService.GetAll(request.Ray);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}