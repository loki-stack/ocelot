﻿using CommonLib.Dtos;
using EmploymentServiceAPIs.Employee.Dtos.ManageFile;
using EmploymentServiceAPIs.Employee.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Mime;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Employee.Controllers
{
    [Route("api/COM/[action]/[controller]")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FileController : ControllerBase
    {
        private readonly IFileService _manageFileService;
        private readonly ILogger<FileController> _logger;

        public FileController(IFileService manageFileService, ILogger<FileController> logger)
        {
            _manageFileService = manageFileService;
            _logger = logger;
        }

        [HttpGet("{id}")]
        [ActionName("COM0401")]
        [Authorize(Roles = "COM0401")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [AllowAnonymous]
        public async Task<IActionResult> DownloadFile(int id, [FromQuery] ApiRequest request)
        {
            try
            {
                var result = await _manageFileService.DownloadFile(id, request.Ray);
                if (result != null && result.Data != null)
                {
                    ContentDisposition cd = new ContentDisposition
                    {
                        FileName = result.Data.FileName,
                        Inline = true  // false = prompt the user for downloading;  true = browser to try to show the file inline
                    };
                    Response.Headers.Add("Content-Disposition", cd.ToString());
                    Response.Headers.Add("X-Content-Type-Options", "nosniff");
                    return File(result.Data.FileContent, "application/octet-stream");
                }
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet]
        [ActionName("COM0401")]
        [Authorize(Roles = "COM0401")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<List<FileViewModel>>>> GetListFileById([FromQuery] ApiRequest<List<int>> request)
        {
            try
            {
                var response = await _manageFileService.GetListFileByIds(request.Parameter.ClientId, request.Parameter.EntityId, request.Ray, request.Data);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        [ActionName("COM0402")]
        [Authorize(Roles = "COM0402")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<List<FileViewModel>>>> UploadFile([FromForm] ApiRequest<string> request, IList<IFormFile> files)
        {
            try
            {
                string username = User.FindFirst(ClaimTypes.Name).Value;
                List<InforUploadModel> infors = null;
                if (!string.IsNullOrEmpty(request.Data))
                {
                    infors = JsonConvert.DeserializeObject<List<InforUploadModel>>(request.Data);
                }
                var response = await _manageFileService.UploadFile(request.Parameter.ClientId, request.Parameter.EntityId, request.Ray, username, infors, files);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}