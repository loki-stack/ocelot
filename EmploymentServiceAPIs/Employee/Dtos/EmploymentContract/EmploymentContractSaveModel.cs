﻿using EmploymentServiceAPIs.Employee.Dtos.Movement;
using System.Collections.Generic;

namespace EmploymentServiceAPIs.Employee.Dtos.EmploymentContract
{
    public class EmploymentContractSaveModel
    {
        public string RemarkEmploymentContract { get; set; }
        public string RemarkMovement { get; set; }
        public List<EmploymentContractModel> EmploymentContracts { get; set; }
        public List<MovementModel> Movements { get; set; }
    }
}