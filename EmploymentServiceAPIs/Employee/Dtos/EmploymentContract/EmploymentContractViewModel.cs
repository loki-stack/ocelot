﻿using EmploymentServiceAPIs.Employee.Dtos.Movement;
using EmploymentServiceAPIs.Employee.Dtos.Termination;
using System;
using System.Collections.Generic;

namespace EmploymentServiceAPIs.Employee.Dtos.EmploymentContract
{
    public class EmploymentContractViewModel
    {
        public int EmploymentContractId { get; set; }
        public int? EmployeeId { get; set; }
        public string EmployeeNo { get; set; }
        public string GlobalNo { get; set; }
        public string ContractNo { get; set; }
        public DateTime? ContractStartDate { get; set; }
        public DateTime? ContractEndDate { get; set; }
        public string StaffType { get; set; }
        public string Remarks { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
        public List<MovementViewModel> Movements { get; set; }
        public List<TerminationViewModel> Terminations { get; set; }
    }
}