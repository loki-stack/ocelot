﻿using System.Collections.Generic;

namespace EmploymentServiceAPIs.Employee.Dtos.EmploymentContract
{
    public class EmploymentContractDto
    {
        public string RemarkEmploymentContract { get; set; }
        public string RemarkMovement { get; set; }
        public List<EmploymentContractViewModel> EmploymentContracts { get; set; }
    }
}