﻿using CommonLib.Constants;
using CommonLib.Validation;
using EmploymentServiceAPIs.Common.Validation;
using EmploymentServiceAPIs.Employee.Constants;
using EmploymentServiceAPIs.Employee.Dtos.Termination;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace EmploymentServiceAPIs.Employee.Dtos.EmploymentContract
{
    [System.ComponentModel.DisplayName("EmploymentContract")]
    public class EmploymentContractModel
    {
        public int EmploymentContractId { get; set; }
        public int? EmployeeId { get; set; }

        [RequiredEmployeeNoValidate(ErrorMessage = Label.EC0103)]
        [MaxLength(30, ErrorMessage = Label.EC0104)]
        [UniqueEmployeeNoValidate(ErrorMessage = Label.EC0105)]
        [ExtraResult(ComponentId = Component.EMPLOYEE_NO, Level = Level.ERROR, Placeholder = new string[] { })]
        public string EmployeeNo { get; set; }

        [MaxLength(500, ErrorMessage = Label.EC0111)]
        [ExtraResult(ComponentId = Component.REMARK, Level = Level.ERROR, Placeholder = new string[] { })]
        public string Remarks { get; set; }

        [MaxLength(30, ErrorMessage = Label.EC0112)]
        [UniqueGlobalNoValidate(ErrorMessage = Label.EC0110)]
        [ExtraResult(ComponentId = Component.GLOBAL_NO, Level = Level.ERROR, Placeholder = new string[] { })]
        public string GlobalNo { get; set; }

        [Required(ErrorMessage = Label.EC0125)]
        [MaxLength(20, ErrorMessage = Label.EC0126)]
        public string StaffType { get; set; }

        [MaxLength(30, ErrorMessage = Label.EC0128)]
        [UniqueContractNoValidate(ErrorMessage = Label.EC0129)]
        [ExtraResult(ComponentId = Component.CONTRACT_NO, Level = Level.ERROR, Placeholder = new string[] { })]
        public string ContractNo { get; set; }

        [JsonIgnore]
        public int ClientId { get; set; }

        [JsonIgnore]
        public int EntityId { get; set; }

        [JsonIgnore]
        public string CreatedBy { get; set; }

        [JsonIgnore]
        public DateTimeOffset? CreatedDt { get; set; }

        [JsonIgnore]
        public string ModifiedBy { get; set; }

        [JsonIgnore]
        public DateTimeOffset? ModifiedDt { get; set; }

        public TerminationSaveModel Termination { get; set; }
    }
}