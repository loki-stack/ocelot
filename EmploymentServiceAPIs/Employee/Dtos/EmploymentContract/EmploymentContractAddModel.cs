﻿using CommonLib.Constants;
using CommonLib.Validation;
using EmploymentServiceAPIs.Common.Validation;
using EmploymentServiceAPIs.Employee.Constants;
using System;
using System.ComponentModel.DataAnnotations;

namespace EmploymentServiceAPIs.Employee.Dtos.EmploymentContract
{
    public class EmploymentContractAddModel
    {
        [Required(ErrorMessage = Label.MO0111)]
        [ExtraResult(ComponentId = Component.WORK_CALENDAR, Level = Level.ERROR, Placeholder = new string[] { })]
        public int? WorkCalendar { get; set; }

        [Required(ErrorMessage = Label.MO0118)]
        [MaxLength(20, ErrorMessage = Label.MO0119)]
        [ExtraResult(ComponentId = Component.DEFAULT_SALARY_CURRENCY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string DefaultSalaryCurrency { get; set; }

        [Required(ErrorMessage = Label.MO0120)]
        [MaxLength(20, ErrorMessage = Label.MO0121)]
        [ExtraResult(ComponentId = Component.DEFAULT_PAYMENT_CURRENCY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string DefaultPaymentCurrency { get; set; }

        [MaxLength(20, ErrorMessage = Label.EC0126)]
        [ExtraResult(ComponentId = Component.STAFF_TYPE, Level = Level.ERROR, Placeholder = new string[] { })]
        public string StaffType { get; set; }

        [Required(ErrorMessage = Label.MO0101)]
        [ExtraResult(ComponentId = Component.BUSINESS_UNIT_ID, Level = Level.ERROR, Placeholder = new string[] { })]
        public int? BusinessUnitId { get; set; }

        [MaxLength(30, ErrorMessage = Label.EC0128)]
        [UniqueContractNoCreateValidate(ErrorMessage = Label.EC0129)]
        [ExtraResult(ComponentId = Component.CONTRACT_NO, Level = Level.ERROR, Placeholder = new string[] { })]
        public string ContractNo { get; set; }

        [Required(ErrorMessage = Label.EC0131)]
        [ExpectedStartDateValidate(ErrorMessage = Label.EC0139)]
        [ExtraResult(ComponentId = Component.EXPECTED_START_DATE, Level = Level.ERROR, Placeholder = new string[] { })]
        public DateTime? ExpectedStartDate { get; set; }

        [Required(ErrorMessage = Label.EC0132)]
        [ExtraResult(ComponentId = Component.PROBATION, Level = Level.ERROR, Placeholder = new string[] { })]
        public bool Probation { get; set; }

        [RequiredExpectedProbationEndDate(ErrorMessage = Label.EC0133)]
        [CompareExpectedProbationEndDateValidate(ErrorMessage = Label.EC0136)]
        [ExtraResult(ComponentId = Component.EXPECTED_PROBATION_END_DATE, Level = Level.ERROR, Placeholder = new string[] { })]
        public DateTime? ExpectedProbationEndDate { get; set; }

        [DateOfAcceptingTheOfferValidate(ErrorMessage = Label.EC0138)]
        [ExtraResult(ComponentId = Component.DATE_OF_ACCEPTING_THE_OFFER, Level = Level.ERROR, Placeholder = new string[] { })]
        public DateTime? DateOfAcceptingTheOffer { get; set; }

        [MaxLength(500, ErrorMessage = Label.EC0111)]
        [ExtraResult(ComponentId = Component.REMARK, Level = Level.ERROR, Placeholder = new string[] { })]
        public string Remark { get; set; }
    }
}