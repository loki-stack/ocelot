﻿using System.Collections.Generic;

namespace EmploymentServiceAPIs.Employee.Dtos.Dependent
{
    public class DependentDto
    {
        public string Remark { get; set; }
        public List<DependentViewModel> Dependents { get; set; }
    }
}