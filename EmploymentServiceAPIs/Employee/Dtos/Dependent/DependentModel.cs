﻿using CommonLib.Constants;
using CommonLib.Validation;
using EmploymentServiceAPIs.Common.Validation;
using EmploymentServiceAPIs.Employee.Constants;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace EmploymentServiceAPIs.Employee.Dtos.Dependent
{
    [System.ComponentModel.DisplayName("Dependent")]
    public class DependentModel
    {
        public int DependentId { get; set; }

        [JsonIgnore]
        public int? EmployeeId { get; set; }

        [Required(ErrorMessage = Label.DE0101)]
        [MaxLength(55, ErrorMessage = Label.DE0102)]
        [ExtraResult(ComponentId = Component.FIRST_NAME_PRIMARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string FirstNamePrimary { get; set; }

        [MaxLength(55, ErrorMessage = Label.DE0127)]
        [ExtraResult(ComponentId = Component.FIRST_NAME_SECONDARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string FirstNameSecondary { get; set; } = string.Empty;

        [MaxLength(55, ErrorMessage = Label.DE0104)]
        [ExtraResult(ComponentId = Component.MIDDLE_NAME_PRIMARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string MiddleNamePrimary { get; set; } = string.Empty;

        [MaxLength(55, ErrorMessage = Label.DE0128)]
        [ExtraResult(ComponentId = Component.MIDDLE_NAME_SECONDARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string MiddleNameSecondary { get; set; } = string.Empty;

        [MaxLength(55, ErrorMessage = Label.DE0106)]
        [ExtraResult(ComponentId = Component.CHRISTIAN_NAME_PRIMARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string ChristianNamePrimary { get; set; } = string.Empty;

        [MaxLength(55, ErrorMessage = Label.DE0129)]
        [ExtraResult(ComponentId = Component.CHRISTIAN_NAME_SECONDARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string ChristianNameSecondary { get; set; } = string.Empty;

        [Required(ErrorMessage = Label.DE0107)]
        [MaxLength(55, ErrorMessage = Label.DE0108)]
        [ExtraResult(ComponentId = Component.LAST_NAME_PRIMARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string LastNamePrimary { get; set; }

        [MaxLength(55, ErrorMessage = Label.DE0130)]
        [ExtraResult(ComponentId = Component.LAST_NAME_SECONDARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string LastNameSecondary { get; set; } = string.Empty;

        [MaxLength(20, ErrorMessage = Label.EMP0132)]
        [ExtraResult(ComponentId = Component.GENDER, Level = Level.ERROR, Placeholder = new string[] { })]
        public string Gender { get; set; }

        [MaxLength(20, ErrorMessage = Label.DE0133)]
        [ExtraResult(ComponentId = Component.RELATIONSHIP_TO_EMPLOYEE, Level = Level.ERROR, Placeholder = new string[] { })]
        public string RelationshipToEmployee { get; set; }

        public DateTime? DateOfBirth { get; set; } = null;

        [MaxLength(20, ErrorMessage = Label.DE0134)]
        [ExtraResult(ComponentId = Component.NAMEPREFIX, Level = Level.ERROR, Placeholder = new string[] { })]
        public string NamePrefix { get; set; }

        [RequiredCitizenIDNoValidate(ErrorMessage = Label.DE0113)]
        [MaxLength(70, ErrorMessage = Label.DE0114)]
        [ExtraResult(ComponentId = Component.CITIZEN_ID_NO, Level = Level.ERROR, Placeholder = new string[] { })]
        public string CitizenIdNo { get; set; }

        [MaxLength(20, ErrorMessage = Label.DE0135)]
        [ExtraResult(ComponentId = Component.NATIONALITY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string Nationality { get; set; }

        [RequiredPassportIdNoValidate(ErrorMessage = Label.DE0136)]
        [MaxLength(70, ErrorMessage = Label.DE0117)]
        [ExtraResult(ComponentId = Component.PASSPORT_ID_NO, Level = Level.ERROR, Placeholder = new string[] { })]
        public string PassportIdNo { get; set; }

        [MaxLength(20, ErrorMessage = Label.DE0131)]
        [RequiredCountryOfIssueOfDependent(ErrorMessage = Label.DE0118)]
        [ExtraResult(ComponentId = Component.COUNTRY_OF_ISSUE, Level = Level.ERROR, Placeholder = new string[] { })]
        public string CountryOfIssue { get; set; }

        [ExtraResult(ComponentId = Component.WORK_VISA_HOLDER, Level = Level.ERROR, Placeholder = new string[] { })]
        public bool? WorkVisaHolder { get; set; }

        [JsonIgnore]
        public DateTime? VisaIssueDate { get; set; } = null;

        [JsonIgnore]
        public DateTime? VisaLandingDate { get; set; } = null;

        [MaxLength(500, ErrorMessage = Label.EC0111)]
        [ExtraResult(ComponentId = Component.REMARK, Level = Level.ERROR, Placeholder = new string[] { })]
        public string Remarks { get; set; }

        [MaxLength(200, ErrorMessage = Label.DE0121)]
        [ExtraResult(ComponentId = Component.ADDRESS, Level = Level.ERROR, Placeholder = new string[] { })]
        public string Address { get; set; }

        public string RelationshipOther { get; set; }

        [JsonIgnore]
        public string CreatedBy { get; set; }

        [JsonIgnore]
        public DateTimeOffset? CreatedDt { get; set; }

        [JsonIgnore]
        public string ModifiedBy { get; set; }

        [JsonIgnore]
        public DateTimeOffset? ModifiedDt { get; set; }
    }
}