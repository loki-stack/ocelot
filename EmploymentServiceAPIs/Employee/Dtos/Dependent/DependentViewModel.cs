﻿using System;
using System.ComponentModel;
using System.Text.Json.Serialization;

namespace EmploymentServiceAPIs.Employee.Dtos.Dependent
{
    [DisplayName("Dependent")]
    public class DependentViewModel
    {
        public int DependentId { get; set; }
        public int? EmployeeId { get; set; }

        [JsonIgnore]
        public string FirstName { get; set; }

        [JsonIgnore]
        public string MiddleName { get; set; }

        [JsonIgnore]
        public string ChristianName { get; set; }

        [JsonIgnore]
        public string LastName { get; set; }

        public string FirstNamePrimary { get; set; }
        public string FirstNameSecondary { get; set; }
        public string MiddleNamePrimary { get; set; }
        public string MiddleNameSecondary { get; set; }
        public string ChristianNamePrimary { get; set; }
        public string ChristianNameSecondary { get; set; }
        public string LastNamePrimary { get; set; }
        public string LastNameSecondary { get; set; }
        public string Gender { get; set; }
        public string RelationshipToEmployee { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string NamePrefix { get; set; }
        public string CitizenIdNo { get; set; }
        public string Nationality { get; set; }
        public string PassportIdNo { get; set; }
        public string CountryOfIssue { get; set; }
        public bool? WorkVisaHolder { get; set; }
        public DateTime? VisaIssueDate { get; set; }
        public DateTime? VisaLandingDate { get; set; }
        public string Remarks { get; set; }
        public string Address { get; set; }
        public string RelationshipOther { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
        public DateTime? MarriageDate { get; set; }
    }
}