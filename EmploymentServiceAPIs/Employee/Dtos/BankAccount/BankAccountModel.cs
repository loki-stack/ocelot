﻿using CommonLib.Constants;
using CommonLib.Validation;
using EmploymentServiceAPIs.Employee.Constants;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace EmploymentServiceAPIs.Employee.Dtos.BankAccount
{
    [System.ComponentModel.DisplayName("BankAccount")]
    public class BankAccountModel
    {
        public int Id { get; set; }

        [MaxLength(100, ErrorMessage = Label.BA0102)]
        [ExtraResult(ComponentId = Component.BANK_ACCOUNT_ID, Level = Level.ERROR, Placeholder = new string[] { })]
        public string EeBankAccountId { get; set; }

        [JsonIgnore]
        public int? EmployeeId { get; set; }

        [Required(ErrorMessage = Label.BA0103)]
        [MaxLength(100, ErrorMessage = Label.BA0104)]
        [ExtraResult(ComponentId = Component.BANK_ACCOUNT_NAME, Level = Level.ERROR, Placeholder = new string[] { })]
        public string AccountName { get; set; }

        [Required(ErrorMessage = Label.BA0105)]
        [MaxLength(20, ErrorMessage = Label.BA0106)]
        [ExtraResult(ComponentId = Component.ACCOUNT_CURRENCY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string AccountCurrency { get; set; }

        [MaxLength(20, ErrorMessage = Label.BA0108)]
        [ExtraResult(ComponentId = Component.BANK_CODE, Level = Level.ERROR, Placeholder = new string[] { })]
        public string BankCode { get; set; }

        [MaxLength(20, ErrorMessage = Label.BA0110)]
        [ExtraResult(ComponentId = Component.BRANCH_CODE, Level = Level.ERROR, Placeholder = new string[] { })]
        public string BranchCode { get; set; }

        [Required(ErrorMessage = Label.BA0111)]
        [MaxLength(30, ErrorMessage = Label.BA0112)]
        [ExtraResult(ComponentId = Component.ACCOUNT_NO, Level = Level.ERROR, Placeholder = new string[] { })]
        public string AccountNo { get; set; }

        [MaxLength(100, ErrorMessage = Label.BA0114)]
        [ExtraResult(ComponentId = Component.TRANSACTION_REFERENCE, Level = Level.ERROR, Placeholder = new string[] { })]
        public string TransactionReference { get; set; }

        [Required(ErrorMessage = Label.BA0115)]
        [ExtraResult(ComponentId = Component.DEFAULT_ACCOUNT, Level = Level.ERROR, Placeholder = new string[] { })]
        public bool? DefaultAccount { get; set; }

        [MaxLength(500, ErrorMessage = Label.EC0111)]
        [ExtraResult(ComponentId = Component.REMARK, Level = Level.ERROR, Placeholder = new string[] { })]
        public string Remark { get; set; }

        [JsonIgnore]
        public string CreatedBy { get; set; }

        [JsonIgnore]
        public DateTimeOffset? CreatedDt { get; set; }

        [JsonIgnore]
        public string ModifiedBy { get; set; }

        [JsonIgnore]
        public DateTimeOffset? ModifiedDt { get; set; }
    }
}