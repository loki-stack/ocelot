﻿using System.Collections.Generic;

namespace EmploymentServiceAPIs.Employee.Dtos.BankAccount
{
    public class BankAccountDto
    {
        public string Remark { get; set; }
        public List<BankAccountViewModel> BankAccounts { get; set; }
    }
}