﻿using CommonLib.Constants;
using CommonLib.Validation;
using EmploymentServiceAPIs.Common.Validation;
using EmploymentServiceAPIs.Employee.Constants;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace EmploymentServiceAPIs.Employee.Dtos.Termination
{
    public class TerminationSaveModel
    {
        public int Id { get; set; }
        public int? EmploymentContractId { get; set; }

        [Required(ErrorMessage = Label.EC0137)]
        [MaxLength(20, ErrorMessage = Label.EC0113)]
        [ExtraResult(ComponentId = Component.TERMINATION_REASON_PRIMARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string TerminationReasonPrimary { get; set; }

        [MaxLength(20, ErrorMessage = Label.EC0116)]
        [ExtraResult(ComponentId = Component.TERMINATION_REASON_ALTERNATIVE, Level = Level.ERROR, Placeholder = new string[] { })]
        public string TerminationReasonAlternative { get; set; }

        [MaxLength(20, ErrorMessage = Label.EC0118)]
        [ExtraResult(ComponentId = Component.TERMINATION_REASON_MPF, Level = Level.ERROR, Placeholder = new string[] { })]
        public string TerminationReasonMpf { get; set; }

        [MaxLength(20, ErrorMessage = Label.EC0117)]
        [ExtraResult(ComponentId = Component.TERMINATION_CODE_TAX, Level = Level.ERROR, Placeholder = new string[] { })]
        public string TerminationCodeTax { get; set; }

        [Required(ErrorMessage = Label.EC0107)]
        [ExtraResult(ComponentId = Component.CONTRACT_END_DATE, Level = Level.ERROR, Placeholder = new string[] { })]
        public DateTime? TerminationContractEndDate { get; set; }

        [LastEmploymentDateValidate(ErrorMessage = Label.TE0107)]
        [ExtraResult(ComponentId = Component.LAST_EMPLOYMENT_DATE, Level = Level.ERROR, Placeholder = new string[] { })]
        public DateTime? LastEmploymentDate { get; set; }

        [LastWorkingDateValidate(ErrorMessage = Label.TE0107)]
        [ExtraResult(ComponentId = Component.LAST_WORKING_DATE, Level = Level.ERROR, Placeholder = new string[] { })]
        public DateTime? LastWorkingDate { get; set; }

        [NotificationDateValidate(ErrorMessage = Label.TE0107)]
        [ExtraResult(ComponentId = Component.NOTIFICATION_DATE, Level = Level.ERROR, Placeholder = new string[] { })]
        public DateTime? NotificationDate { get; set; }

        [YearServiceValidate(ErrorMessage = Label.EC0124)]
        [ExtraResult(ComponentId = Component.YEAR_SERVICE_START_DATE, Level = Level.ERROR, Placeholder = new string[] { })]
        public DateTime? YearServiceStartDate { get; set; }

        public DateTime? YearServiceEndDate { get; set; }
        public bool? LeavingHongKong { get; set; }

        [ExtraResult(ComponentId = Component.HOLD_LAST_PAYMENT, Level = Level.ERROR, Placeholder = new string[] { })]
        public bool? HoldLastPayment { get; set; }

        [MaxLength(500, ErrorMessage = Label.EC0111)]
        [ExtraResult(ComponentId = Component.REMARK, Level = Level.ERROR, Placeholder = new string[] { })]
        public string Remark { get; set; }

        [JsonIgnore]
        public string CreatedBy { get; set; }

        [JsonIgnore]
        public DateTimeOffset? CreatedDt { get; set; }

        [JsonIgnore]
        public string ModifiedBy { get; set; }

        [JsonIgnore]
        public DateTimeOffset? ModifiedDt { get; set; }
    }
}