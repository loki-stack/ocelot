﻿using System;

namespace EmploymentServiceAPIs.Employee.Dtos.Termination
{
    public class TerminationViewModel
    {
        public int Id { get; set; }
        public int? EmploymentContractId { get; set; }
        public string TerminationReasonPrimary { get; set; }
        public string TerminationReasonAlternative { get; set; }
        public string TerminationReasonMpf { get; set; }
        public string TerminationCodeTax { get; set; }
        public DateTime? TerminationContractEndDate { get; set; }
        public DateTime? LastEmploymentDate { get; set; }
        public DateTime? LastWorkingDate { get; set; }
        public DateTime? NotificationDate { get; set; }
        public DateTime? YearServiceStartDate { get; set; }
        public DateTime? YearServiceEndDate { get; set; }
        public bool? LeavingHongKong { get; set; }
        public bool? HoldLastPayment { get; set; }
        public string Remark { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
    }
}