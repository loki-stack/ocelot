﻿using CommonLib.Constants;
using CommonLib.Validation;
using EmploymentServiceAPIs.Employee.Constants;
using System;
using System.ComponentModel.DataAnnotations;

namespace EmploymentServiceAPIs.Employee.Dtos.RentalAddress
{
    [System.ComponentModel.DisplayName("RentalAddress")]
    public class RentalAddressRequest
    {
        public int AddressId { get; set; }

        [MaxLength(20, ErrorMessage = Label.REN0101)]
        [ExtraResult(ComponentId = "nature", Level = Level.ERROR, Placeholder = new string[] { })]
        public string Nature { get; set; }

        [MaxLength(200, ErrorMessage = Label.REN0102)]
        [ExtraResult(ComponentId = "address", Level = Level.ERROR, Placeholder = new string[] { })]
        public string Address { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? RentPaidToLandlordByEmployer { get; set; }
        public int? RentPaidToLandlordByEmployee { get; set; }
        public int? RentRefundedToEmployeeByEmployer { get; set; }
        public int? RentPaidToEmployerByEmployee { get; set; }
    }
}