﻿using System.Collections.Generic;

namespace EmploymentServiceAPIs.Employee.Dtos.RentalAddress
{
    public class RentalAddressSaveModel
    {
        public List<RentalAddressRequest> RentalAddresses { get; set; }
        public string RemarkRentalAddress { get; set; }
    }
}
