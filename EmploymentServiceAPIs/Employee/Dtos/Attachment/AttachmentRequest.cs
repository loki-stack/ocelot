﻿namespace EmploymentServiceAPIs.Employee.Dtos.Attachment
{
    public class AttachmentRequest
    {
        public int AttachmentId { get; set; }
        public int AttachmentFileId { get; set; }
        public string AttachmentFileName { get; set; }
        public string AttachmentFileRemark { get; set; }
        public bool isDeleted { get; set; } = false;
    }
}
