﻿using System;

namespace EmploymentServiceAPIs.Employee.Dtos.Movement
{
    public class MovementViewModel
    {
        public int AssignmentId { get; set; }
        public int? EmploymentContractId { get; set; }
        public int? BusinessUnitId { get; set; }
        public string MovementName { get; set; }
        public string InternalJobTitle { get; set; }
        public string ExternalJobTitle { get; set; }
        public int? JobGrade { get; set; }
        public int? DirectManager { get; set; }
        public int? CostCenter { get; set; }
        public DateTime? AssignmentStartDate { get; set; }
        public DateTime? AssignmentEndDate { get; set; }
        public int? RemunerationPackage { get; set; }
        public int? BasicSalary { get; set; }
        public string BaseSalaryUnit { get; set; }
        public string DefaultSalaryCurrency { get; set; }
        public string DefaultPaymentCurrency { get; set; }
        public string StaffTags { get; set; }
        public string Remarks { get; set; }
        public int? NotificationRequirements { get; set; }
        public int? WorkCalendar { get; set; }

        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
    }
}