﻿using CommonLib.Constants;
using CommonLib.Validation;
using EmploymentServiceAPIs.Employee.Constants;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace EmploymentServiceAPIs.Employee.Dtos.Movement
{
    [System.ComponentModel.DisplayName("Movement")]
    public class MovementModel
    {
        public int AssignmentId { get; set; }
        public int? EmploymentContractId { get; set; }
        public int? BusinessUnitId { get; set; }

        [MaxLength(100, ErrorMessage = Label.MO0103)]
        [ExtraResult(ComponentId = Component.MOVEMENT_NAME, Level = Level.ERROR, Placeholder = new string[] { })]
        public string MovementName { get; set; }

        [MaxLength(40, ErrorMessage = Label.MO0105)]
        [ExtraResult(ComponentId = Component.INTERNAL_JOB_TITLE, Level = Level.ERROR, Placeholder = new string[] { })]
        public string InternalJobTitle { get; set; }

        [MaxLength(40, ErrorMessage = Label.MO0107)]
        [ExtraResult(ComponentId = Component.EXTERNAL_JOB_TITLE, Level = Level.ERROR, Placeholder = new string[] { })]
        public string ExternalJobTitle { get; set; }

        public int? JobGrade { get; set; }

        public int? DirectManager { get; set; }
        public int? CostCenter { get; set; }

        [Required(ErrorMessage = Label.MO0112)]
        [ExtraResult(ComponentId = Component.ASSIGNMENT_START_DATE, Level = Level.ERROR, Placeholder = new string[] { })]
        public DateTime? AssignmentStartDate { get; set; }

        public int? RemunerationPackage { get; set; }
        public int? BasicSalary { get; set; }

        [MaxLength(20, ErrorMessage = Label.MO0117)]
        [ExtraResult(ComponentId = Component.BASE_SALARY_UNIT, Level = Level.ERROR, Placeholder = new string[] { })]
        public string BaseSalaryUnit { get; set; }

        [MaxLength(20, ErrorMessage = Label.MO0119)]
        [ExtraResult(ComponentId = Component.DEFAULT_SALARY_CURRENCY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string DefaultSalaryCurrency { get; set; }

        [MaxLength(20, ErrorMessage = Label.MO0121)]
        [ExtraResult(ComponentId = Component.DEFAULT_PAYMENT_CURRENCY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string DefaultPaymentCurrency { get; set; }

        public string StaffTags { get; set; }

        [MaxLength(500, ErrorMessage = Label.MO0123)]
        [ExtraResult(ComponentId = Component.REMARK, Level = Level.ERROR, Placeholder = new string[] { })]
        public string Remarks { get; set; }

        [Required(ErrorMessage = Label.MO0130)]
        [ExtraResult(ComponentId = Component.SPECIFIC_ENTITY_ID, Level = Level.ERROR, Placeholder = new string[] { })]
        public int? SpecificEntityId { get; set; }

        public int? NotificationRequirements { get; set; }
        public int? WorkCalendar { get; set; }

        [JsonIgnore]
        public string CreatedBy { get; set; }

        [JsonIgnore]
        public DateTimeOffset? CreatedDt { get; set; }

        [JsonIgnore]
        public string ModifiedBy { get; set; }

        [JsonIgnore]
        public DateTimeOffset? ModifiedDt { get; set; }
    }
}