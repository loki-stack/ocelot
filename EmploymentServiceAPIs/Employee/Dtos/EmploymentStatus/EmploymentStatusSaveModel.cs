﻿using CommonLib.Constants;
using CommonLib.Validation;
using EmploymentServiceAPIs.Employee.Constants;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EmploymentServiceAPIs.Employee.Dtos.EmploymentStatus
{
    public class EmploymentStatusSaveModel
    {
        [MaxLength(500, ErrorMessage = Label.EMP0141)]
        [ExtraResult(ComponentId = Component.REMARK, Level = Level.ERROR, Placeholder = new string[] { })]
        public string Remark { get; set; }

        public List<EmploymentStatusModel> EmploymentStatuses { get; set; }
    }
}