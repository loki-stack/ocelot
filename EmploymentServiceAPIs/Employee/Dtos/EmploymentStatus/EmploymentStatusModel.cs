﻿using CommonLib.Constants;
using CommonLib.Validation;
using EmploymentServiceAPIs.Employee.Constants;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace EmploymentServiceAPIs.Employee.Dtos.EmploymentStatus
{
    public class EmploymentStatusModel
    {
        public int EmploymentStatusId { get; set; }

        [Required(ErrorMessage = Label.ES0103)]
        [ExtraResult(ComponentId = Component.EMPLOYMENT_CONTRACT_ID, Level = Level.ERROR, Placeholder = new string[] { })]
        public int EmploymentContractId { get; set; }

        [Required(ErrorMessage = Label.ES0104)]
        [ExtraResult(ComponentId = Component.START_DATE, Level = Level.ERROR, Placeholder = new string[] { })]
        public DateTime StartDate { get; set; }

        [Required(ErrorMessage = Label.ES0105)]
        [MaxLength(20, ErrorMessage = Label.ES0106)]
        [ExtraResult(ComponentId = Component.STATUS, Level = Level.ERROR, Placeholder = new string[] { })]
        public string Status { get; set; }

        [MaxLength(500, ErrorMessage = Label.EMP0141)]
        [ExtraResult(ComponentId = Component.REMARK, Level = Level.ERROR, Placeholder = new string[] { })]
        public string Remark { get; set; }

        public bool? Confirmed { get; set; } = false;
        public Guid StatusId { get; set; } = Guid.NewGuid();

        [JsonIgnore]
        public DateTimeOffset? CreatedDt { get; set; }

        [JsonIgnore]
        public string CreatedBy { get; set; }

        [JsonIgnore]
        public DateTimeOffset? ModifiedDt { get; set; }

        [JsonIgnore]
        public string ModifiedBy { get; set; }
    }
}