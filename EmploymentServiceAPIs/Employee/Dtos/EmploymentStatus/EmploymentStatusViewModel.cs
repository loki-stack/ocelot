﻿using System;

namespace EmploymentServiceAPIs.Employee.Dtos.EmploymentStatus
{
    public class EmploymentStatusViewModel
    {
        public int EmploymentStatusId { get; set; }
        public int EmploymentContractId { get; set; }
        public string EmploymentContractName { get; set; }
        public DateTime StartDate { get; set; }
        public string Status { get; set; }
        public string Remark { get; set; }
        public bool? Confirmed { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
        public string ModifiedBy { get; set; }
    }
}