﻿using System;

namespace EmploymentServiceAPIs.Employee.Dtos.EmploymentStatus
{
    public class EmploymentStatusResult
    {
        public string FullName { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeNo { get; set; }
        public string BusinessUnit { get; set; }
        public int? EmploymentContractId { get; set; }
        public int? EmploymentStatusId { get; set; }
        public string LastStatus { get; set; }
        public string NewStatus { get; set; }
        public DateTime StartDate { get; set; }
        public bool Confirmed { get; set; }
    }
}