﻿using System;
using System.Collections.Generic;

namespace EmploymentServiceAPIs.Employee.Dtos.EmploymentStatus
{
    public class EmploymentStatusItemModel
    {
        public DateTime StartDate { get; set; }
        public List<EmploymentStatusViewModel> EmploymenStatuses { get; set; }
    }
}