﻿using System.Collections.Generic;

namespace EmploymentServiceAPIs.Employee.Dtos.EmploymentStatus
{
    public class EmploymentStatusDto
    {
        public string Remark { get; set; }
        public List<EmploymentStatusItemModel> Data { get; set; }
    }
}