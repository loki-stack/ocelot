﻿namespace EmploymentServiceAPIs.Employee.Dtos.EmploymentStatus
{
    public class EmploymentStatusFilter
    {
        public string NewStatus { get; set; }
        public string LastStatus { get; set; }
    }
}