﻿using EmploymentServiceAPIs.Employee.Dtos.Employee;
using System.Collections.Generic;

namespace EmploymentServiceAPIs.Employee.Dtos.TaxArrangement
{
    public class TaxSaveModel
    {
        public List<TaxRequest> TaxRequests { get; set; }
        public OverseasPayment OverseasPayment { get; set; }
        public string RemarkTax { get; set; }
    }
}
