﻿using CommonLib.Constants;
using CommonLib.Validation;
using EmploymentServiceAPIs.Employee.Constants;
using System;
using System.ComponentModel.DataAnnotations;

namespace EmploymentServiceAPIs.Employee.Dtos.TaxArrangement
{
    [System.ComponentModel.DisplayName("Tax")]
    public class TaxRequest
    {
        public int TaxId { get; set; }

        [Required(ErrorMessage = Label.TAX0101)]
        [ExtraResult(ComponentId = "employmentContractId", Level = Level.ERROR, Placeholder = new string[] { })]
        public int EmploymentContractId { get; set; }

        public bool? OverseasPayment { get; set; }

        [MaxLength(100, ErrorMessage = Label.TAX0102)]
        [ExtraResult(ComponentId = "companyName", Level = Level.ERROR, Placeholder = new string[] { })]
        public string CompanyName { get; set; }

        [MaxLength(100, ErrorMessage = Label.TAX0103)]
        [ExtraResult(ComponentId = "address", Level = Level.ERROR, Placeholder = new string[] { })]
        public string Address { get; set; }

        [MaxLength(20, ErrorMessage = Label.TAX0104)]
        [ExtraResult(ComponentId = "currency", Level = Level.ERROR, Placeholder = new string[] { })]
        public string Currency { get; set; }

        [MaxLength(100, ErrorMessage = Label.TAX0105)]
        [ExtraResult(ComponentId = "amount", Level = Level.ERROR, Placeholder = new string[] { })]
        public string Amount { get; set; }

        public bool? LeavingHkAfterTermination { get; set; }

        public bool? HoldLastPayment { get; set; }

        [MaxLength(500, ErrorMessage = Label.TAX0106)]
        [ExtraResult(ComponentId = "ir56bRemarks", Level = Level.ERROR, Placeholder = new string[] { })]
        public string Ir56bRemarks { get; set; }

        [MaxLength(30, ErrorMessage = Label.TAX0107)]
        [ExtraResult(ComponentId = "ir56eMonthlyRateOfAllownace", Level = Level.ERROR, Placeholder = new string[] { })]
        public string Ir56eMonthlyRateOfAllownace { get; set; }

        [MaxLength(30, ErrorMessage = Label.TAX0108)]
        [ExtraResult(ComponentId = "ir56eFlutuatingEmoluments", Level = Level.ERROR, Placeholder = new string[] { })]
        public string Ir56eFlutuatingEmoluments { get; set; }

        [MaxLength(50, ErrorMessage = Label.TAX0109)]
        [ExtraResult(ComponentId = "ir56ePreviousEmployerName", Level = Level.ERROR, Placeholder = new string[] { })]
        public string Ir56ePreviousEmployerName { get; set; }

        [MaxLength(100, ErrorMessage = Label.TAX0110)]
        [ExtraResult(ComponentId = "ir56ePreviousEmployerAddress", Level = Level.ERROR, Placeholder = new string[] { })]
        public string Ir56ePreviousEmployerAddress { get; set; }

        public bool? Ir56eShareOptionGranted { get; set; }

        [MaxLength(50, ErrorMessage = Label.TAX0111)]
        [ExtraResult(ComponentId = "ir56fNewEmployerName", Level = Level.ERROR, Placeholder = new string[] { })]
        public string Ir56fNewEmployerName { get; set; }

        [MaxLength(100, ErrorMessage = Label.TAX0112)]
        [ExtraResult(ComponentId = "ir56fNewEmployerAddress", Level = Level.ERROR, Placeholder = new string[] { })]
        public string Ir56fNewEmployerAddress { get; set; }

        [MaxLength(100, ErrorMessage = Label.TAX0113)]
        [ExtraResult(ComponentId = "ir56fAddressAfterTermination", Level = Level.ERROR, Placeholder = new string[] { })]
        public string Ir56fAddressAfterTermination { get; set; }

        public DateTime? Ir56gDepartureDate { get; set; }

        [MaxLength(20, ErrorMessage = Label.TAX0115)]
        [ExtraResult(ComponentId = "ir56gReasonForDeparture", Level = Level.ERROR, Placeholder = new string[] { })]
        public string Ir56gReasonForDeparture { get; set; }
        public string Ir56gReasonForDepartureOthers { get; set; }
        public bool? Ir56gEmployeeReturningHk { get; set; }
        public DateTime? Ir56gEmployeeReturningHkDate { get; set; }

        [MaxLength(100, ErrorMessage = Label.TAX0116)]
        [ExtraResult(ComponentId = "ir56gAddressAfterTermination", Level = Level.ERROR, Placeholder = new string[] { })]
        public string Ir56gAddressAfterTermination { get; set; }
        public bool? Ir56gEmployerHasHoldMoney { get; set; }
        public int? Ir56gHeldAmount { get; set; }

        [MaxLength(100, ErrorMessage = Label.TAX0117)]
        [ExtraResult(ComponentId = "ir56gReasonForNotHoldingMoney", Level = Level.ERROR, Placeholder = new string[] { })]
        public string Ir56gReasonForNotHoldingMoney { get; set; }
        public bool? Ir56gTaxBorneByEmployer { get; set; }

        public bool? Ir56gShareOptionGranted { get; set; }
        public DateTime? Ir56gGrantedDate { get; set; }

        [MaxLength(30, ErrorMessage = Label.TAX0119)]
        [ExtraResult(ComponentId = "ir56gNoOfSharesNotYetExercised", Level = Level.ERROR, Placeholder = new string[] { })]
        public string Ir56gNoOfSharesNotYetExercised { get; set; }


        [MaxLength(500, ErrorMessage = Label.TAX0118)]
        [ExtraResult(ComponentId = "remarks", Level = Level.ERROR, Placeholder = new string[] { })]
        public string Remarks { get; set; }


    }
}