﻿using CommonLib.Constants;
using CommonLib.Validation;
using EmploymentServiceAPIs.Employee.Constants;
using System.ComponentModel.DataAnnotations;

namespace EmploymentServiceAPIs.Employee.Dtos.EmergencyContact
{
    [System.ComponentModel.DisplayName("EmergencyContactPerson")]
    public class EmergencyContactRequest
    {
        public int EmergencyContactPersonId { get; set; }

        [MaxLength(20, ErrorMessage = Label.EMER0101)]
        [ExtraResult(ComponentId = "relationshipToEmployee", Level = Level.ERROR, Placeholder = new string[] { })]
        public string RelationshipToEmployee { get; set; }

        [MaxLength(100, ErrorMessage = Label.EMER0102)]
        [ExtraResult(ComponentId = "relationshipOther", Level = Level.ERROR, Placeholder = new string[] { })]
        public string RelationshipOther { get; set; }

        [MaxLength(100, ErrorMessage = Label.EMER0103)]
        [ExtraResult(ComponentId = "name", Level = Level.ERROR, Placeholder = new string[] { })]
        public string Name { get; set; }

        [MaxLength(50, ErrorMessage = Label.EMER0104)]
        [ExtraResult(ComponentId = "mobilePhone", Level = Level.ERROR, Placeholder = new string[] { })]
        public string MobilePhone { get; set; }
    }
}
