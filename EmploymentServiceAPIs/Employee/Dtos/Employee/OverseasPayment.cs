﻿using CommonLib.Constants;
using CommonLib.Validation;
using EmploymentServiceAPIs.Employee.Constants;
using System.ComponentModel.DataAnnotations;

namespace EmploymentServiceAPIs.Employee.Dtos.Employee
{
    [System.ComponentModel.DisplayName("OverseasPayment")]
    public class OverseasPayment
    {
        public bool? OverseasPayments { get; set; }

        [MaxLength(30, ErrorMessage = Label.EMP0145)]
        [ExtraResult(ComponentId = Component.COMPANY_NAME, Level = Level.ERROR, Placeholder = new string[] { })]
        public string CompanyName { get; set; }

        [MaxLength(30, ErrorMessage = Label.EMP0146)]
        [ExtraResult(ComponentId = Component.ADDRESS, Level = Level.ERROR, Placeholder = new string[] { })]
        public string Address { get; set; }

        [MaxLength(30, ErrorMessage = Label.EMP0147)]
        [ExtraResult(ComponentId = Component.AMOUNT, Level = Level.ERROR, Placeholder = new string[] { })]
        public string Amount { get; set; }

        [MaxLength(20, ErrorMessage = Label.EMP0148)]
        [ExtraResult(ComponentId = Component.CURRENCY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string Currency { get; set; }
    }
}
