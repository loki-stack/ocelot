﻿using CommonLib.Constants;
using CommonLib.Validation;
using EmploymentServiceAPIs.Employee.Constants;
using System;
using System.ComponentModel.DataAnnotations;

namespace EmploymentServiceAPIs.Employee.Dtos.Employee
{
    [System.ComponentModel.DisplayName("NewHire")]
    public class NewHireRequest
    {
        [Required(ErrorMessage = Label.EMP0101)]
        [MaxLength(100, ErrorMessage = Label.EMP0102)]
        [ExtraResult(ComponentId = "firstName", Level = Level.ERROR, Placeholder = new string[] { })]
        public string FirstName { get; set; }

        [MaxLength(100, ErrorMessage = Label.EMP0104)]
        [ExtraResult(ComponentId = "middleName", Level = Level.ERROR, Placeholder = new string[] { })]
        public string MiddleName { get; set; } = "";

        [Required(ErrorMessage = Label.EMP0105)]
        [MaxLength(100, ErrorMessage = Label.EMP0106)]
        [ExtraResult(ComponentId = "lastName", Level = Level.ERROR, Placeholder = new string[] { })]
        public string LastName { get; set; }

        [Required(ErrorMessage = Label.EMP0149)]
        [MaxLength(100, ErrorMessage = Label.EMP0108)]
        [ExtraResult(ComponentId = "displayName", Level = Level.ERROR, Placeholder = new string[] { })]
        public string DisplayName { get; set; }

        [Required(ErrorMessage = Label.EC0125)]
        [MaxLength(20, ErrorMessage = Label.EC0126)]
        [ExtraResult(ComponentId = "staffType", Level = Level.ERROR, Placeholder = new string[] { })]
        public string StaffType { get; set; }

        [Required(ErrorMessage = Label.MO0101)]
        [ExtraResult(ComponentId = "businessUnitId", Level = Level.ERROR, Placeholder = new string[] { })]
        public int BusinessUnitId { get; set; }

        [MaxLength(30, ErrorMessage = Label.EC0128)]
        [ExtraResult(ComponentId = "contractNo", Level = Level.ERROR, Placeholder = new string[] { })]
        public string ContractNo { get; set; }

        [Required(ErrorMessage = Label.MO0111)]
        [ExtraResult(ComponentId = "workCalendar", Level = Level.ERROR, Placeholder = new string[] { })]
        public int WorkCalendar { get; set; }

        [Required(ErrorMessage = Label.MO0118)]
        [MaxLength(20, ErrorMessage = Label.MO0119)]
        [ExtraResult(ComponentId = "defaultSlaryCurrency", Level = Level.ERROR, Placeholder = new string[] { })]
        public string DefaultSalaryCurrency { get; set; }

        [Required(ErrorMessage = Label.MO0120)]
        [MaxLength(20, ErrorMessage = Label.MO0121)]
        [ExtraResult(ComponentId = "defaultPaymentCurrency", Level = Level.ERROR, Placeholder = new string[] { })]
        public string DefaultPaymentCurrency { get; set; }

        [Required(ErrorMessage = Label.EMP0150)]
        [ExtraResult(ComponentId = "expectedStartDate", Level = Level.ERROR, Placeholder = new string[] { })]
        public DateTime ExpectedStartDate { get; set; }
        public bool Probation { get; set; }

        public DateTime ExpectedProbationEndDate { get; set; }
        public DateTime? AcceptingTheOffer { get; set; }

        [MaxLength(500, ErrorMessage = Label.EMP0141)]
        [ExtraResult(ComponentId = "remark", Level = Level.ERROR, Placeholder = new string[] { })]
        public string Remark { get; set; }

        public int PhotoId { get; set; }
    }
}
