﻿using System;

namespace EmploymentServiceAPIs.Employee.Dtos.Employee
{
    public class EmployeeFilter
    {
        public string Role { get; set; }
        public int Division { get; set; }
        public string DivisionTxt { get; set; }
        public int? Grade { get; set; }
        public string StaffId { get; set; }
        public string Status { get; set; }
        public DateTime? StartDate { get; set; }
    }
}
