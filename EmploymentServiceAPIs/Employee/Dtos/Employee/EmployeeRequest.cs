﻿using CommonLib.Constants;
using CommonLib.Validation;
using EmploymentServiceAPIs.Employee.Constants;
using System;
using System.ComponentModel.DataAnnotations;

namespace EmploymentServiceAPIs.Employee.Dtos.Employee
{
    [System.ComponentModel.DisplayName("EmployeeRequest")]
    public class EmployeeRequest
    {
        public int EmployeeId { get; set; }

        [Required(ErrorMessage = Label.EMP0101)]
        [MaxLength(100, ErrorMessage = Label.EMP0102)]
        [ExtraResult(ComponentId = Component.FIRST_NAME_PRIMARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string FirstNamePrimary { get; set; }

        [MaxLength(100, ErrorMessage = Label.EMP0102)]
        [ExtraResult(ComponentId = Component.FIRST_NAME_SECONDARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string FirstNameSecondary { get; set; } = "";

        [MaxLength(100, ErrorMessage = Label.EMP0104)]
        [ExtraResult(ComponentId = Component.MIDDLE_NAME_PRIMARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string MiddleNamePrimary { get; set; } = "";

        [MaxLength(100, ErrorMessage = Label.EMP0104)]
        [ExtraResult(ComponentId = Component.MIDDLE_NAME_SECONDARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string MiddleNameSecondary { get; set; } = "";

        [Required(ErrorMessage = Label.EMP0105)]
        [MaxLength(100, ErrorMessage = Label.EMP0106)]
        [ExtraResult(ComponentId = Component.LAST_NAME_PRIMARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string LastNamePrimary { get; set; }

        [MaxLength(100, ErrorMessage = Label.EMP0106)]
        [ExtraResult(ComponentId = Component.LAST_NAME_SECONDARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string LastNameSecondary { get; set; } = "";

        [MaxLength(100, ErrorMessage = Label.EMP0107)]
        [ExtraResult(ComponentId = Component.CHRISTIAN_NAME_PRIMARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string ChristianNamePrimary { get; set; } = "";

        [MaxLength(100, ErrorMessage = Label.EMP0107)]
        [ExtraResult(ComponentId = Component.CHRISTIAN_NAME_SECONDARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string ChristianNameSecondary { get; set; } = "";

        [MaxLength(200, ErrorMessage = Label.EMP0108)]
        [ExtraResult(ComponentId = Component.DISPLAY_NAME_PRIMARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string DisplayNamePrimary { get; set; } = "";

        [MaxLength(200, ErrorMessage = Label.EMP0108)]
        [ExtraResult(ComponentId = Component.DISPLAY_NAME_SECONDARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string DisplayNameSecondary { get; set; } = "";

        [MaxLength(20, ErrorMessage = Label.EMP0109)]
        [ExtraResult(ComponentId = Component.NAME_PREFIX, Level = Level.ERROR, Placeholder = new string[] { })]
        public string NamePrefix { get; set; }

        [MaxLength(20, ErrorMessage = Label.EMP0110)]
        [ExtraResult(ComponentId = Component.GENDER, Level = Level.ERROR, Placeholder = new string[] { })]
        public string Gender { get; set; }
        public int? Photo { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int? Age { get; set; }

        [MaxLength(20, ErrorMessage = Label.EMP0111)]
        [ExtraResult(ComponentId = Component.MARTIAL_STATUS, Level = Level.ERROR, Placeholder = new string[] { })]
        public string MartialStatus { get; set; }

        public DateTime? MarriageDate { get; set; }

        [MaxLength(20, ErrorMessage = Label.EMP0112)]
        [ExtraResult(ComponentId = Component.PREFERRED_LANGUAGE, Level = Level.ERROR, Placeholder = new string[] { })]
        public string PreferredLanguage { get; set; }

        [MaxLength(70, ErrorMessage = Label.EMP0113)]
        [ExtraResult(ComponentId = Component.CITIZEN_ID_NO, Level = Level.ERROR, Placeholder = new string[] { })]
        public string CitizenIdNo { get; set; }

        [MaxLength(20, ErrorMessage = Label.EMP0114)]
        [ExtraResult(ComponentId = Component.NATIONALITY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string Nationality { get; set; }

        [MaxLength(70, ErrorMessage = Label.EMP0115)]
        [ExtraResult(ComponentId = Component.PASSPORT_ID_NO, Level = Level.ERROR, Placeholder = new string[] { })]
        public string PassportIdNo { get; set; }

        [MaxLength(20, ErrorMessage = Label.EMP0116)]
        [ExtraResult(ComponentId = Component.COUNTRY_OF_ISSUE, Level = Level.ERROR, Placeholder = new string[] { })]
        public string CountryOfIssue { get; set; }

        public bool? WorkVisaHolder { get; set; }
        public DateTime? WorkVisaExpiryDate { get; set; }
        public DateTime? WorkArrivingDate { get; set; }
        public DateTime? WorkVisaIssueDate { get; set; }
        public DateTime? WorkVisaLandingDate { get; set; }

        [MaxLength(50, ErrorMessage = Label.EMP0118)]
        [ExtraResult(ComponentId = Component.HOME_PHONE, Level = Level.ERROR, Placeholder = new string[] { })]
        public string HomePhone { get; set; }

        [MaxLength(50, ErrorMessage = Label.EMP0119)]
        [ExtraResult(ComponentId = Component.OFFICE_PHONE, Level = Level.ERROR, Placeholder = new string[] { })]
        public string OfficePhone { get; set; }

        [MaxLength(50, ErrorMessage = Label.EMP0120)]
        [ExtraResult(ComponentId = Component.MOBILE_PHONE, Level = Level.ERROR, Placeholder = new string[] { })]
        public string MobilePhone { get; set; }

        [MaxLength(100, ErrorMessage = Label.EMP0121)]
        [ExtraResult(ComponentId = Component.WORK_EMAIL, Level = Level.ERROR, Placeholder = new string[] { })]
        public string WorkEmail { get; set; }

        [MaxLength(100, ErrorMessage = Label.EMP0122)]
        [ExtraResult(ComponentId = Component.PERSONAL_EMAIL, Level = Level.ERROR, Placeholder = new string[] { })]
        public string PersonalEmail { get; set; }

        [MaxLength(30, ErrorMessage = Label.EMP0123)]
        [ExtraResult(ComponentId = Component.RESIDENTIAL_ADDRESS_1_PRIMARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string ResidentialAddress1Primary { get; set; } = "";

        [MaxLength(30, ErrorMessage = Label.EMP0123)]
        [ExtraResult(ComponentId = Component.RESIDENTIAL_ADDRESS_1_SECONDARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string ResidentialAddress1Secondary { get; set; } = "";

        [MaxLength(30, ErrorMessage = Label.EMP0124)]
        [ExtraResult(ComponentId = Component.RESIDENTIAL_ADDRESS_2_PRIMARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string ResidentialAddress2Primary { get; set; } = "";

        [MaxLength(30, ErrorMessage = Label.EMP0124)]
        [ExtraResult(ComponentId = Component.RESIDENTIAL_ADDRESS_2_SECONDARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string ResidentialAddress2Secondary { get; set; } = "";

        [MaxLength(30, ErrorMessage = Label.EMP0125)]
        [ExtraResult(ComponentId = Component.RESIDENTIAL_ADDRESS_3_PRIMARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string ResidentialAddress3Primary { get; set; } = "";

        [MaxLength(30, ErrorMessage = Label.EMP0125)]
        [ExtraResult(ComponentId = Component.RESIDENTIAL_ADDRESS_3_SECONDARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string ResidentialAddress3Secondary { get; set; } = "";

        [MaxLength(30, ErrorMessage = Label.EMP0126)]
        [ExtraResult(ComponentId = Component.RESIDENTIAL_DISTRICT_PRIMARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string ResidentialDistrictPrimary { get; set; } = "";

        [MaxLength(30, ErrorMessage = Label.EMP0126)]
        [ExtraResult(ComponentId = Component.RESIDENTIAL_DISTRICT_SECONDARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string ResidentialDistrictSecondary { get; set; } = "";

        [MaxLength(30, ErrorMessage = Label.EMP0127)]
        [ExtraResult(ComponentId = Component.RESIDENTIAL_CITY_PRIMARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string ResidentialCityPrimary { get; set; } = "";

        [MaxLength(30, ErrorMessage = Label.EMP0127)]
        [ExtraResult(ComponentId = Component.RESIDENTIAL_CITY_SECONDARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string ResidentialCitySecondary { get; set; } = "";

        [MaxLength(30, ErrorMessage = Label.EMP0128)]
        [ExtraResult(ComponentId = Component.RESIDENTIAL_STATE_PRIMARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string ResidentialStatePrimary { get; set; } = "";

        [MaxLength(30, ErrorMessage = Label.EMP0128)]
        [ExtraResult(ComponentId = Component.RESIDENTIAL_STATE_SECONDARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string ResidentialStateSecondary { get; set; } = "";

        [MaxLength(30, ErrorMessage = Label.EMP0129)]
        [ExtraResult(ComponentId = Component.RESIDENTIAL_COUNTRY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string ResidentialCountry { get; set; }

        [MaxLength(30, ErrorMessage = Label.EMP0130)]
        [ExtraResult(ComponentId = Component.RESIDENTIAL_POSTAL_CODE, Level = Level.ERROR, Placeholder = new string[] { })]
        public string ResidentialPostalCode { get; set; }

        [MaxLength(30, ErrorMessage = Label.EMP0131)]
        [ExtraResult(ComponentId = Component.RESIDENTIAL_HONGKONG_TAX_FORM, Level = Level.ERROR, Placeholder = new string[] { })]
        public string ResidentialHongkongTaxForm { get; set; }

        [MaxLength(30, ErrorMessage = Label.EMP0132)]
        [ExtraResult(ComponentId = Component.POSTAL_ADDRESS_1_PRIMARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string PostalAddress1Primary { get; set; } = "";

        [MaxLength(30, ErrorMessage = Label.EMP0132)]
        [ExtraResult(ComponentId = Component.POSTAL_ADDRESS_1_SECONDARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string PostalAddress1Secondary { get; set; } = "";

        [MaxLength(30, ErrorMessage = Label.EMP0133)]
        [ExtraResult(ComponentId = Component.POSTAL_ADDRESS_2_PRIMARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string PostalAddress2Primary { get; set; } = "";

        [MaxLength(30, ErrorMessage = Label.EMP0133)]
        [ExtraResult(ComponentId = Component.POSTAL_ADDRESS_2_SECONDARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string PostalAddress2Secondary { get; set; } = "";

        [MaxLength(30, ErrorMessage = Label.EMP0134)]
        [ExtraResult(ComponentId = Component.POSTAL_ADDRESS_3_PRIMARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string PostalAddress3Primary { get; set; } = "";

        [MaxLength(30, ErrorMessage = Label.EMP0134)]
        [ExtraResult(ComponentId = Component.POSTAL_ADDRESS_3_SECONDARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string PostalAddress3Secondary { get; set; } = "";

        [MaxLength(30, ErrorMessage = Label.EMP0135)]
        [ExtraResult(ComponentId = Component.POSTAL_DISTRICT_PRIMARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string PostalDistrictPrimary { get; set; } = "";

        [MaxLength(30, ErrorMessage = Label.EMP0135)]
        [ExtraResult(ComponentId = Component.POSTAL_DISTRICT_SECONDARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string PostalDistrictSecondary { get; set; } = "";

        [MaxLength(30, ErrorMessage = Label.EMP0136)]
        [ExtraResult(ComponentId = Component.POSTAL_CITY_PRIMARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string PostalCityPrimary { get; set; } = "";

        [MaxLength(30, ErrorMessage = Label.EMP0136)]
        [ExtraResult(ComponentId = Component.POSTAL_CITY_SECONDARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string PostalCitySecondary { get; set; } = "";

        [MaxLength(30, ErrorMessage = Label.EMP0137)]
        [ExtraResult(ComponentId = Component.POSTAL_STATE_PRIMARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string PostalStatePrimary { get; set; } = "";

        [MaxLength(30, ErrorMessage = Label.EMP0137)]
        [ExtraResult(ComponentId = Component.POSTAL_STATE_SECONDARY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string PostalStateSecondary { get; set; } = "";

        [MaxLength(30, ErrorMessage = Label.EMP0138)]
        [ExtraResult(ComponentId = Component.POSTAL_COUNTRY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string PostalCountry { get; set; }

        [MaxLength(30, ErrorMessage = Label.EMP0139)]
        [ExtraResult(ComponentId = Component.POSTAL_POSTAL_CODE, Level = Level.ERROR, Placeholder = new string[] { })]
        public string PostalPostalCode { get; set; }

        [MaxLength(30, ErrorMessage = Label.EMP0140)]
        [ExtraResult(ComponentId = Component.POSTAL_HONGKONG_TAX_FORM, Level = Level.ERROR, Placeholder = new string[] { })]
        public string PostalHongkongTaxForm { get; set; }

        [MaxLength(500, ErrorMessage = Label.EMP0141)]
        [ExtraResult(ComponentId = Component.REMARKS, Level = Level.ERROR, Placeholder = new string[] { })]
        public string Remarks { get; set; }

        [MaxLength(500, ErrorMessage = Label.EMP0142)]
        [ExtraResult(ComponentId = Component.REMARK_CONTACT, Level = Level.ERROR, Placeholder = new string[] { })]
        public string RemarkContact { get; set; }

        [MaxLength(500, ErrorMessage = Label.EMP0152)]
        [ExtraResult(ComponentId = Component.REMARK_EMPLOYMENT_CONTRACT, Level = Level.ERROR, Placeholder = new string[] { })]
        public string RemarkEmploymentContract { get; set; }
    }
}