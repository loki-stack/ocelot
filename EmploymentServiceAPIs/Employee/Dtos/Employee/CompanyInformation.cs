﻿namespace EmploymentServiceAPIs.Employee.Dtos.Employee
{
    public class CompanyInformation
    {
        public string CompanyCode { get; set; }
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public string CompanyOfficialName { get; set; }
        public string CompanyDisplayName { get; set; }
        public string Country { get; set; }
        public string FunctionalGroupFullName { get; set; }
        public string FunctionalGroupDisplayName { get; set; }
        public int? ParentId { get; set; }
        public string RemarkCompany { get; set; }
    }
}
