﻿using EmploymentServiceAPIs.Employee.Dtos.Attachment;
using EmploymentServiceAPIs.Employee.Dtos.EmergencyContact;
using System.Collections.Generic;

namespace EmploymentServiceAPIs.Employee.Dtos.Employee
{
    public class EmployeeSaveModel
    {
        public EmployeeRequest Employee { get; set; }
        public List<AttachmentRequest> Attachments { get; set; }
        public List<EmergencyContactRequest> EmergencyContacts { get; set; }
    }
}
