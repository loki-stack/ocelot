﻿using System;

namespace EmploymentServiceAPIs.Employee.Dtos.Employee
{
    public class EmployeeViewList
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
        public int? Division { get; set; }
        public string DivisionTxt { get; set; }
        public int? Grade { get; set; }
        public string StaffId { get; set; }
        public string Status { get; set; }
        public string WorkEmail { get; set; }
        public string MobilePhone { get; set; }
        public DateTime? AssignmentStartDate { get; set; }
        public DateTime? AssignmentEndDate { get; set; }
        public int? RemunerationPackage { get; set; }
        public DateTime? TerminationDate { get; set; }
        public int? Photo { get; set; }
        public DateTimeOffset? ModifiedDate { get; set; }
    }
}
