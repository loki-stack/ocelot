﻿using System;

namespace EmploymentServiceAPIs.Employee.Dtos.Employee
{
    public class EmployeeViewModel
    {
        public int EmployeeId { get; set; }
        public string FirstNamePrimary { get; set; }
        public string FirstNameSecondary { get; set; }
        public string MiddleNamePrimary { get; set; }
        public string MiddleNameSecondary { get; set; }
        public string LastNamePrimary { get; set; }
        public string LastNameSecondary { get; set; }
        public string ChristianNamePrimary { get; set; }
        public string ChristianNameSecondary { get; set; }
        public string DisplayNamePrimary { get; set; }
        public string DisplayNameSecondary { get; set; }
        public string NamePrefix { get; set; }
        public string Gender { get; set; }
        public int Photo { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int? Age { get; set; }
        public string MartialStatus { get; set; }
        public DateTime? MarriageDate { get; set; }
        public string PreferredLanguage { get; set; }
        public string CitizenIdNo { get; set; }
        public string Nationality { get; set; }
        public string PassportIdNo { get; set; }
        public string CountryOfIssue { get; set; }
        public bool WorkVisaHolder { get; set; }
        public DateTime? WorkVisaExpiryDate { get; set; }
        public DateTime? WorkVisaIssueDate { get; set; }
        public DateTime? WorkVisaLandingDate { get; set; }
        public string HomePhoneArea { get; set; }
        public string HomePhone { get; set; }
        public string OfficePhoneArea { get; set; }
        public string OfficePhone { get; set; }
        public string MobilePhoneArea { get; set; }
        public string MobilePhone { get; set; }
        public string WorkEmail { get; set; }
        public string PersonalEmail { get; set; }
        public string ResidentialAddress1Primary { get; set; }
        public string ResidentialAddress1Secondary { get; set; }
        public string ResidentialAddress2Primary { get; set; }
        public string ResidentialAddress2Secondary { get; set; }
        public string ResidentialAddress3Primary { get; set; }
        public string ResidentialAddress3Secondary { get; set; }
        public string ResidentialDistrictPrimary { get; set; }
        public string ResidentialDistrictSecondary { get; set; }
        public string ResidentialCityPrimary { get; set; }
        public string ResidentialCitySecondary { get; set; }
        public string ResidentialStatePrimary { get; set; }
        public string ResidentialStateSecondary { get; set; }
        public string ResidentialCountry { get; set; }
        public string ResidentialPostalCode { get; set; }
        public string ResidentialHongkongTaxForm { get; set; }
        public string PostalAddress1Primary { get; set; }
        public string PostalAddress1Secondary { get; set; }
        public string PostalAddress2Primary { get; set; }
        public string PostalAddress2Secondary { get; set; }
        public string PostalAddress3Primary { get; set; }
        public string PostalAddress3Secondary { get; set; }
        public string PostalDistrictPrimary { get; set; }
        public string PostalDistrictSecondary { get; set; }
        public string PostalCityPrimary { get; set; }
        public string PostalCitySecondary { get; set; }
        public string PostalStatePrimary { get; set; }
        public string PostalStateSecondary { get; set; }
        public string PostalCountry { get; set; }
        public string PostalPostalCode { get; set; }
        public string PostalHongkongTaxForm { get; set; }
        public DateTime? WorkArrivingDate { get; set; }
        public string Remarks { get; set; }
        public string RemarkContact { get; set; }
        public bool OverseasPayments { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string Amount { get; set; }
    }
}