﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel;
using System.Text.Json.Serialization;

namespace EmploymentServiceAPIs.Employee.Dtos.ManageFile
{
    [DisplayName("File")]
    public class FileEditModel
    {
        [JsonIgnore]
        public int FileId { get; set; }

        [JsonIgnore]
        public string Category { get; set; }

        public IFormFile File { get; set; }

        [JsonIgnore]
        public string Name { get; set; }

        [JsonIgnore]
        public byte[] Content { get; set; }

        public bool IsDeleted { get; set; }
        public string Remark { get; set; }

        [JsonIgnore]
        public DateTimeOffset CreatedDt { get; set; }

        [JsonIgnore]
        public string CreatedBy { get; set; }

        [JsonIgnore]
        public DateTimeOffset ModifiedDt { get; set; }

        [JsonIgnore]
        public string ModifiedBy { get; set; }
    }
}