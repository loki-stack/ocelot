﻿using Microsoft.AspNetCore.Http;

namespace EmploymentServiceAPIs.Employee.Dtos.ManageFile
{
    public class FileDownloadModel
    {
        public string FileName { get; set; }
        public IFormFile File { get; set; }
        public byte[] FileContent { get; set; }
        public string ContentType { get; set; }
    }
}