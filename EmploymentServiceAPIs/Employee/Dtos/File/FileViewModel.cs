﻿using System;
using System.ComponentModel;

namespace EmploymentServiceAPIs.Employee.Dtos.ManageFile
{
    [DisplayName("File")]
    public class FileViewModel
    {
        public int FileId { get; set; }
        public string Category { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public string Remark { get; set; }
        public string Src { get; set; }
        public DateTimeOffset CreatedDt { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset ModifiedDt { get; set; }
        public string ModifiedBy { get; set; }
    }
}