﻿using CommonLib.Constants;
using CommonLib.Validation;
using EmploymentServiceAPIs.Employee.Constants;
using System.ComponentModel.DataAnnotations;

namespace EmploymentServiceAPIs.Employee.Dtos.ManageFile
{
    public class InforUploadModel
    {
        [MaxLength(200, ErrorMessage = Label.MF0104)]
        [ExtraResult(ComponentId = Component.NAME, Level = Level.ERROR, Placeholder = new string[] { })]
        public string Name { get; set; }

        [MaxLength(500, ErrorMessage = Label.MF0103)]
        [ExtraResult(ComponentId = Component.REMARK, Level = Level.ERROR, Placeholder = new string[] { })]
        public string Remark { get; set; }

        [MaxLength(20, ErrorMessage = Label.MF0108)]
        [ExtraResult(ComponentId = Component.CATEGORY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string Category { get; set; }
    }
}