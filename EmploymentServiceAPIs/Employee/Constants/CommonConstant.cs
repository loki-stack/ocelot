﻿namespace EmploymentServiceAPIs.Employee.Constants
{
    public struct GenericCode
    {
        public const string SCHEME_TYPE = "SCHEMETYPE";
        public const string CONTRIBUTION_CYCLE = "CONTRIBUTION";
        public const string PAYMENT_METHOD = "PAYMENTMETHOD";
        public const string CURRENCY = "CURRENCY";
        public const string STATUS = "STATUS";
        public const string FEE_UNIT = "FEEUNIT";
        public const string ROUNDING_TYPE = "ROUNDINGTYPE";
        public const string SCHEME_TYPE_MPF = "MPF";
        public const string SCHEME_TYPE_ORSO = "ORSO";
        public const string DISPLAY_MODEL = "00006";
        public const string PAYROLL_ITEM_FLAG = "00001";
        public const string PAYROLL_ITEM_TAX = "00005";
        public const string EMPLOYMENT_STATUS = "EESTATUS";
    }

    public struct SaveType
    {
        public const int CREATE = 1;
        public const int UPDATE = 2;
        public const int DELETE = 3;
    }

    public struct EmploymentStatus
    {
        public const string ACTIVE = "AC";
        public const string UNDER_PROBATION = "UP";
        public const string TERMINATED = "TE";
        public const string NO_SHOW = "NS";
        public const string DECLINED_OFFER = "DO";
        public const string KNOWN_LEAVING = "KL";
        public const string SUSPENDED = "SU";
        public const string ACCEPTED_OFFER = "AO";
    }
}