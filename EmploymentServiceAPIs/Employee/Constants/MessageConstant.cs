namespace EmploymentServiceAPIs.Employee.Constants

{
    public struct Toast
    {
        public const string FORM = "FORM";
        public const string NOTIFICATION = "NOTIFICATION";
        public const string TOAST = "toast";
    }

    public struct Component
    {
        public const string NAME = "name";
        public const string CODE = "code";
        public const string REMARK = "remark";
        public const string STATUS = "status";
        public const string DISPLAYORDER = "displayOrder";
        public const string UNIT_CODE = "unitCode";
        public const string ACCOUNT_CODE = "accountCode";
        public const string LEVEL = "level";
        public const string RELATIONSHIP_TO_EMPLOYEE = "relationshipToEmployee";
        public const string NAMEPREFIX = "namePrefix";
        public const string ADDRESS = "address";
        public const string BANK_ACCOUNT_ID = "bankAccountId";
        public const string BANK_ACCOUNT_NAME = "bankAccountName";
        public const string ACCOUNT_CURRENCY = "accountCurrency";
        public const string BANK_CODE = "bankCode";
        public const string BRANCH_CODE = "branchCode";
        public const string ACCOUNT_NO = "accountNo";
        public const string TRANSACTION_REFERENCE = "transactionReference";
        public const string DEFAULT_ACCOUNT = "defaultAccount";
        public const string EMPLOYEE_NO = "employeeNo";
        public const string CONTRACT_START_DATE = "contractStartDate";
        public const string CONTRACT_END_DATE = "contractEndDate";
        public const string YEAR_SERVICE_START_DATE = "yearServiceStartDate";
        public const string YEAR_SERVICE_END_DATE = "yearServiceEndDate";
        public const string LAST_WORKING_DATE = "lastWorkingDate";
        public const string GLOBAL_NO = "globalNo";
        public const string TERMINATION_REASON_PRIMARY = "terminationReasonPrimary";
        public const string TERMINATION_REASON_ALTERNATIVE = "terminationReasonAlternative";
        public const string TERMINATION_CODE_TAX = "terminationCodeTax";
        public const string TERMINATION_REASON_MPF = "terminationReasonMPF";
        public const string BUSINESS_UNIT_ID = "businessUnitId";
        public const string MOVEMENT_NAME = "movementName";
        public const string INTERNAL_JOB_TITLE = "internalJobTitle";
        public const string EXTERNAL_JOB_TITLE = "externalJobTitle";
        public const string JOB_GRADE = "jobGrade";
        public const string DIRECT_MANAGER = "directManager";
        public const string COST_CENTER = "costCenter";
        public const string WORK_CALENDAR = "workCalendar";
        public const string ASSIGNMENT_START_DATE = "assignmentStartDate";
        public const string ASSIGNMENT_END_DATE = "assignmentEndDate";
        public const string PAYROLL_PROFILE = "payrollProfile";
        public const string BASIC_SALARY = "basicSalary";
        public const string BASE_SALARY_UNIT = "baseSalaryUnit";
        public const string DEFAULT_SALARY_CURRENCY = "defaultSalaryCurrency";
        public const string DEFAULT_PAYMENT_CURRENCY = "defaultPaymentCurrency";
        public const string STAFF_TAGS = "staffTags";
        public const string SPECIFIC_ENTITY_ID = "entityId";
        public const string REMUNERATION_PACKAGE = "remunerationPackage";
        public const string CATEGORY = "category";
        public const string EMPLOYMENT_CONTRACT_ID = "employmentContractId";
        public const string START_DATE = "startDate";
        public const string CONTRACT_NO = "contractNo";
        public const string HOLD_LAST_PAYMENT = "holdLastPayment";
        public const string STAFF_TYPE = "staffType";
        public const string EXPECTED_START_DATE = "expectedStartDate";
        public const string PROBATION = "probation";
        public const string EXPECTED_PROBATION_END_DATE = "expectedProbationEndDate";
        public const string NOTIFICATION_DATE = "notificationDate";
        public const string LAST_EMPLOYMENT_DATE = "lastEmploymentDate";
        public const string DATE_OF_ACCEPTING_THE_OFFER = "dateOfAcceptingTheOffer";
        public const string GLOBAL_LEVEL_CALENDAR_NAME = "globalLevelCalendarName";
        public const string COUNTRY = "country";
        public const string DATE = "date";
        public const string DESCRIPTION_PRIMARY_LANGUAGE = "descriptionPrimaryLanguage";
        public const string HOLIDAY_TYPE = "holidayType";
        public const string IS_WORKING_DAY = "isWorkingDay";
        public const string CALENDAR_NAME = "calendarName";
        public const string WORKING_SUNDAY = "sunday";
        public const string WORKING_MONDAY = "monday";
        public const string WORKING_TUESDAY = "tuesday";
        public const string WORKING_WEDNESDAY = "wednesday";
        public const string WORKING_THURSDAY = "thursday";
        public const string WORKING_FRIDAY = "friday";
        public const string WORKING_SATURDAY = "saturday";

        #region employee

        public const string FIRST_NAME = "firstName";
        public const string FIRST_NAME_PRIMARY = "firstNamePrimary";
        public const string FIRST_NAME_SECONDARY = "firstNameSecondary";

        public const string MIDDLE_NAME = "middleName";
        public const string MIDDLE_NAME_PRIMARY = "middleNamePrimary";
        public const string MIDDLE_NAME_SECONDARY = "middleNameSecondary";

        public const string LAST_NAME = "lastName";
        public const string LAST_NAME_PRIMARY = "lastNamePrimary";
        public const string LAST_NAME_SECONDARY = "lastNameSecondary";

        public const string CHRISTIAN_NAME = "christianName";
        public const string CHRISTIAN_NAME_PRIMARY = "christianNamePrimary";
        public const string CHRISTIAN_NAME_SECONDARY = "christianNameSecondary";

        public const string DISPLAY_NAME = "displayName";
        public const string DISPLAY_NAME_PRIMARY = "displayNamePrimary";
        public const string DISPLAY_NAME_SECONDARY = "displayNameSecondary";

        public const string NAME_PREFIX = "namePrefix";
        public const string DATE_OF_BIRTH = "dateOfBirth";
        public const string GENDER = "gender";
        public const string PHOTO = "photo";
        public const string AGE = "age";
        public const string MARTIAL_STATUS = "martialStatus";
        public const string PREFERRED_LANGUAGE = "preferredLanguage";
        public const string CITIZEN_ID_NO = "citizenIdNo";
        public const string NATIONALITY = "nationality";
        public const string PASSPORT_ID_NO = "passportIdNo";
        public const string COUNTRY_OF_ISSUE = "countryOfIssue";
        public const string WORK_VISA_HOLDER = "workVisaHolder";
        public const string WORK_ARRIVING_DATE = "workArrivingDate";
        public const string WORK_VISA_EXPIRY_DATE = "workVisaExpiryDate";
        public const string WORK_VISA_ISSUE_DATE = "workVisaIssueDate";
        public const string WORK_VISA_LANDING_DATE = "workVisaLandingDate";
        public const string HOME_PHONE_AREA = "homePhoneArea";
        public const string HOME_PHONE = "homePhone";
        public const string OFFICE_PHONE_AREA = "officePhoneArea";
        public const string OFFICE_PHONE = "officePhone";
        public const string MOBILE_PHONE_AREA = "officePhoneArea";
        public const string MOBILE_PHONE = "mobilePhone";
        public const string WORK_EMAIL = "workEmail";
        public const string PERSONAL_EMAIL = "personalEmail";

        public const string RESIDENTIAL_ADDRESS_1 = "residentialAddress1";
        public const string RESIDENTIAL_ADDRESS_1_PRIMARY = "residentialAddress1Primary";
        public const string RESIDENTIAL_ADDRESS_1_SECONDARY = "residentialAddress1Secondary";

        public const string RESIDENTIAL_ADDRESS_2 = "residentialAddress2";
        public const string RESIDENTIAL_ADDRESS_2_PRIMARY = "residentialAddress2Primary";
        public const string RESIDENTIAL_ADDRESS_2_SECONDARY = "residentialAddress2Secondary";

        public const string RESIDENTIAL_ADDRESS_3 = "residentialAddress3";
        public const string RESIDENTIAL_ADDRESS_3_PRIMARY = "residentialAddress3Primary";
        public const string RESIDENTIAL_ADDRESS_3_SECONDARY = "residentialAddress3Secondary";

        public const string RESIDENTIAL_DISTRICT = "residentialDistrict";
        public const string RESIDENTIAL_DISTRICT_PRIMARY = "residentialDistrictPrimary";
        public const string RESIDENTIAL_DISTRICT_SECONDARY = "residentialDistrictSecondary";

        public const string RESIDENTIAL_CITY = "residentialCity";
        public const string RESIDENTIAL_CITY_PRIMARY = "residentialCityPrimary";
        public const string RESIDENTIAL_CITY_SECONDARY = "residentialCitySecondary";

        public const string RESIDENTIAL_STATE = "residentitalState";
        public const string RESIDENTIAL_STATE_PRIMARY = "residentialStatePrimary";
        public const string RESIDENTIAL_STATE_SECONDARY = "residentialStateSecondary";

        public const string RESIDENTIAL_COUNTRY = "residentialCountry";
        public const string RESIDENTIAL_POSTAL_CODE = "residentialPostalCode";
        public const string RESIDENTIAL_HONGKONG_TAX_FORM = "residentialHongkongTaxForm";

        public const string POSTAL_ADDRESS_1 = "postalAddress1";
        public const string POSTAL_ADDRESS_1_PRIMARY = "postalAddress1Primary";
        public const string POSTAL_ADDRESS_1_SECONDARY = "postalAddress1Secondary";

        public const string POSTAL_ADDRESS_2 = "postalAddress2";
        public const string POSTAL_ADDRESS_2_PRIMARY = "postalAddress2Primary";
        public const string POSTAL_ADDRESS_2_SECONDARY = "postalAddress2Secondary";

        public const string POSTAL_ADDRESS_3 = "postalAddress3";
        public const string POSTAL_ADDRESS_3_PRIMARY = "postalAddress3Primary";
        public const string POSTAL_ADDRESS_3_SECONDARY = "postalAddress3Secondary";

        public const string POSTAL_DISTRICT = "postalDistrict";
        public const string POSTAL_DISTRICT_PRIMARY = "postalDistrictPrimary";
        public const string POSTAL_DISTRICT_SECONDARY = "postalDistrictSecondary";

        public const string POSTAL_CITY = "postalCity";
        public const string POSTAL_CITY_PRIMARY = "postalCityPrimary";
        public const string POSTAL_CITY_SECONDARY = "postalCitySecondary";

        public const string POSTAL_STATE = "postalState";
        public const string POSTAL_STATE_PRIMARY = "postalStatePrimary";
        public const string POSTAL_STATE_SECONDARY = "postalStateSecondary";
        public const string POSTAL_COUNTRY = "postalCountry";
        public const string POSTAL_POSTAL_CODE = "postalPostalCode";
        public const string POSTAL_HONGKONG_TAX_FORM = "postalHongkongTaxForm";
        public const string REMARKS = "remarks";
        public const string REMARK_CONTACT = "remarkContact";
        public const string COMPANY_NAME = "companyName";
        public const string AMOUNT = "amount";
        public const string CURRENCY = "currency";
        public const string REMARK_EMPLOYMENT_CONTRACT = "remarkEmploymentContract";

        #endregion employee
    }

    public struct Label
    {
        public const string PS0101 = "PS0101";
        public const string PS0102 = "PS0102";
        public const string PS0103 = "PS0103";
        public const string PS0104 = "PS0104";
        public const string PS0105 = "PS0105";
        public const string PS0106 = "PS0106";
        public const string PS0107 = "PS0107";
        public const string PS0108 = "PS0108";
        public const string PS0109 = "PS0109";
        public const string PS0110 = "PS0110";
        public const string PS0111 = "PS0111";
        public const string PS0112 = "PS0112";
        public const string PS0113 = "PS0113";
        public const string PS0114 = "PS0114";

        #region Cost Center

        public const string CC01E001 = "CC01E001";
        public const string CC01E002 = "CC01E002";
        public const string CC01E005 = "CC01E005";
        public const string CC01E006 = "CC01E006";
        public const string CC01E007 = "CC01E007";
        public const string CC01E008 = "CC01E008";
        public const string CC0101 = "CC0101";
        public const string CC0102 = "CC0102";
        public const string CC0103 = "CC0103";
        public const string CC0104 = "CC0104";

        #endregion Cost Center

        #region Job Grade

        public const string JG01E001 = "JG01E001";
        public const string JG01E002 = "JG01E002";
        public const string JG01E003 = "JG01E003";
        public const string JG01E004 = "JG01E004";
        public const string JG01E005 = "JG01E005";
        public const string JG01E006 = "JG01E006";
        public const string JG0101 = "JG0101";
        public const string JG0102 = "JG0102";
        public const string JG0103 = "JG0103";
        public const string JG0104 = "JG0104";

        #endregion Job Grade

        #region ORSO Scheme

        public const string ORSO001 = "ORSO001";
        public const string ORSO002 = "ORSO002";
        public const string ORSO003 = "ORSO003";
        public const string ORSO004 = "ORSO004";

        #endregion ORSO Scheme

        #region Employee

        public const string EMP001 = "EMP001";
        public const string EMP002 = "EMP002";
        public const string EMP003 = "EMP003";
        public const string EMP004 = "EMP004";
        public const string EMP005 = "EMP005";
        public const string EMP006 = "EMP006";
        public const string EMP007 = "EMP007";
        public const string EMP008 = "EMP008";
        public const string EMP0101 = "EMP0101";
        public const string EMP0102 = "EMP0102";
        public const string EMP0103 = "EMP0103";
        public const string EMP0104 = "EMP0104";
        public const string EMP0105 = "EMP0105";
        public const string EMP0106 = "EMP0106";
        public const string EMP0107 = "EMP0107";
        public const string EMP0108 = "EMP0108";
        public const string EMP0109 = "EMP0109";
        public const string EMP0110 = "EMP0110";
        public const string EMP0111 = "EMP0111";
        public const string EMP0112 = "EMP0112";
        public const string EMP0113 = "EMP0113";
        public const string EMP0114 = "EMP0114";
        public const string EMP0115 = "EMP0115";
        public const string EMP0116 = "EMP0116";
        public const string EMP0117 = "EMP0117";
        public const string EMP0118 = "EMP0118";
        public const string EMP0119 = "EMP0119";
        public const string EMP0120 = "EMP0120";
        public const string EMP0121 = "EMP0121";
        public const string EMP0122 = "EMP0122";
        public const string EMP0123 = "EMP0123";
        public const string EMP0124 = "EMP0124";
        public const string EMP0125 = "EMP0125";
        public const string EMP0126 = "EMP0126";
        public const string EMP0127 = "EMP0127";
        public const string EMP0128 = "EMP0128";
        public const string EMP0129 = "EMP0129";
        public const string EMP0130 = "EMP0130";
        public const string EMP0131 = "EMP0131";
        public const string EMP0132 = "EMP0132";
        public const string EMP0133 = "EMP0133";
        public const string EMP0134 = "EMP0134";
        public const string EMP0135 = "EMP0135";
        public const string EMP0136 = "EMP0136";
        public const string EMP0137 = "EMP0137";
        public const string EMP0138 = "EMP0138";
        public const string EMP0139 = "EMP0139";
        public const string EMP0140 = "EMP0140";
        public const string EMP0141 = "EMP0141";
        public const string EMP0142 = "EMP0142";
        public const string EMP0143 = "EMP0143";
        public const string EMP0144 = "EMP0144";
        public const string EMP0145 = "EMP0145";
        public const string EMP0146 = "EMP0146";
        public const string EMP0147 = "EMP0147";
        public const string EMP0148 = "EMP0148";
        public const string EMP0149 = "EMP0149";
        public const string EMP0150 = "EMP0150";
        public const string EMP0151 = "EMP0151";
        public const string EMP0152 = "EMP0152";
        public const string EMP0153 = "EMP0153";
        public const string EMP0156 = "EMP0156";

        #endregion Employee

        #region Dependent

        public const string DE0101 = "DE0101";
        public const string DE0102 = "DE0102";
        public const string DE0103 = "DE0103";
        public const string DE0104 = "DE0104";
        public const string DE0105 = "DE0105";
        public const string DE0106 = "DE0106";
        public const string DE0107 = "DE0107";
        public const string DE0108 = "DE0108";
        public const string DE0109 = "DE0109";
        public const string DE0110 = "DE0110";
        public const string DE0111 = "DE0111";
        public const string DE0112 = "DE0112";
        public const string DE0113 = "DE0113";
        public const string DE0114 = "DE0114";
        public const string DE0115 = "DE0115";
        public const string DE0116 = "DE0116";
        public const string DE0117 = "DE0117";
        public const string DE0118 = "DE0118";
        public const string DE0119 = "DE0119";
        public const string DE0120 = "DE0120";
        public const string DE0121 = "DE0121";
        public const string DE0122 = "DE0122";
        public const string DE0123 = "DE0123";
        public const string DE0124 = "DE0124";
        public const string DE0125 = "DE0125";
        public const string DE0126 = "DE0126";
        public const string DE0127 = "DE0127";
        public const string DE0128 = "DE0128";
        public const string DE0129 = "DE0129";
        public const string DE0130 = "DE0130";
        public const string DE0131 = "DE0131";
        public const string DE0132 = "DE0132";
        public const string DE0133 = "DE0133";
        public const string DE0134 = "DE0134";
        public const string DE0135 = "DE0135";
        public const string DE0136 = "DE0136";

        #endregion Dependent

        #region Bank Account

        public const string BA0101 = "BA0101";
        public const string BA0102 = "BA0102";
        public const string BA0103 = "BA0103";
        public const string BA0104 = "BA0104";
        public const string BA0105 = "BA0105";
        public const string BA0106 = "BA0106";
        public const string BA0107 = "BA0107";
        public const string BA0108 = "BA0108";
        public const string BA0109 = "BA0109";
        public const string BA0110 = "BA0110";
        public const string BA0111 = "BA0111";
        public const string BA0112 = "BA0112";
        public const string BA0113 = "BA0113";
        public const string BA0114 = "BA0114";
        public const string BA0115 = "BA0115";
        public const string BA0116 = "BA0116";
        public const string BA0117 = "BA0117";
        public const string BA0118 = "BA0118";
        public const string BA0119 = "BA0119";
        public const string BA0120 = "BA0120";
        public const string BA0121 = "BA0121";

        #endregion Bank Account

        #region Emergency Contact

        public const string EMER001 = "EMER001";
        public const string EMER002 = "EMER002";
        public const string EMER003 = "EMER003";
        public const string EMER004 = "EMER004";
        public const string EMER005 = "EMER005";
        public const string EMER006 = "EMER006";
        public const string EMER0101 = "EMER0101";
        public const string EMER0102 = "EMER0102";
        public const string EMER0103 = "EMER0103";
        public const string EMER0104 = "EMER0104";
        public const string EMER0105 = "EMER0105";

        #endregion Emergency Contact

        #region Manage File

        public const string MF0101 = "MF0101";
        public const string MF0102 = "MF0102";
        public const string MF0103 = "MF0103";
        public const string MF0104 = "MF0104";
        public const string MF0105 = "MF0105";
        public const string MF0106 = "MF0106";
        public const string MF0107 = "MF0107";
        public const string MF0108 = "MF0108";
        public const string MF0109 = "MF0109";
        public const string MF0110 = "MF0110";
        public const string MF0111 = "MF0111";

        #endregion Manage File

        #region Tax

        public const string TAX001 = "TAX001";
        public const string TAX002 = "TAX002";
        public const string TAX003 = "TAX003";
        public const string TAX004 = "TAX004";
        public const string TAX0101 = "TAX0101";
        public const string TAX0102 = "TAX0102";
        public const string TAX0103 = "TAX0103";
        public const string TAX0104 = "TAX0104";
        public const string TAX0105 = "TAX0105";
        public const string TAX0106 = "TAX0106";
        public const string TAX0107 = "TAX0107";
        public const string TAX0108 = "TAX0108";
        public const string TAX0109 = "TAX0109";
        public const string TAX0110 = "TAX0110";
        public const string TAX0111 = "TAX0111";
        public const string TAX0112 = "TAX0112";
        public const string TAX0113 = "TAX0113";
        public const string TAX0114 = "TAX0114";
        public const string TAX0115 = "TAX0115";
        public const string TAX0116 = "TAX0116";
        public const string TAX0117 = "TAX0117";
        public const string TAX0118 = "TAX0118";
        public const string TAX0119 = "TAX0119";

        #endregion Tax

        #region Rental Address

        public const string REN001 = "REN001";
        public const string REN002 = "REN002";
        public const string REN003 = "REN003";
        public const string REN0101 = "REN0101";
        public const string REN0102 = "REN0102";
        public const string REN0103 = "REN0103";

        #endregion Rental Address

        #region Employment Contract

        public const string EC0101 = "EC0101";
        public const string EC0102 = "EC0102";
        public const string EC0103 = "EC0103";
        public const string EC0104 = "EC0104";
        public const string EC0105 = "EC0105";
        public const string EC0106 = "EC0106";
        public const string EC0107 = "EC0107";
        public const string EC0108 = "EC0108";
        public const string EC0109 = "EC0109";
        public const string EC0110 = "EC0110";
        public const string EC0111 = "EC0111";
        public const string EC0112 = "EC0112";
        public const string EC0113 = "EC0113";
        public const string EC0114 = "EC0114";
        public const string EC0115 = "EC0115";
        public const string EC0116 = "EC0116";
        public const string EC0117 = "EC0117";
        public const string EC0118 = "EC0118";
        public const string EC0119 = "EC0119";
        public const string EC0120 = "EC0120";
        public const string EC0121 = "EC0121";
        public const string EC0122 = "EC0122";
        public const string EC0123 = "EC0123";
        public const string EC0124 = "EC0124";
        public const string EC0125 = "EC0125";
        public const string EC0126 = "EC0126";
        public const string EC0127 = "EC0127";
        public const string EC0128 = "EC0128";
        public const string EC0129 = "EC0129";
        public const string EC0130 = "EC0130";
        public const string EC0131 = "EC0131";
        public const string EC0132 = "EC0132";
        public const string EC0133 = "EC0133";
        public const string EC0134 = "EC0134";
        public const string EC0135 = "EC0135";
        public const string EC0136 = "EC0136";
        public const string EC0137 = "EC0137";
        public const string EC0138 = "EC0138";
        public const string EC0139 = "EC0139";

        #endregion Employment Contract

        #region Movement

        public const string MO0101 = "MO0101";
        public const string MO0102 = "MO0102";
        public const string MO0103 = "MO0103";
        public const string MO0104 = "MO0104";
        public const string MO0105 = "MO0105";
        public const string MO0106 = "MO0106";
        public const string MO0107 = "MO0107";
        public const string MO0108 = "MO0108";
        public const string MO0109 = "MO0109";
        public const string MO0110 = "MO0110";
        public const string MO0111 = "MO0111";
        public const string MO0112 = "MO0112";
        public const string MO0113 = "MO0113";
        public const string MO0114 = "MO0114";
        public const string MO0115 = "MO0115";
        public const string MO0116 = "MO0116";
        public const string MO0117 = "MO0117";
        public const string MO0118 = "MO0118";
        public const string MO0119 = "MO0119";
        public const string MO0120 = "MO0120";
        public const string MO0121 = "MO0121";
        public const string MO0122 = "MO0122";
        public const string MO0123 = "MO0123";
        public const string MO0124 = "MO0124";
        public const string MO0125 = "MO0125";
        public const string MO0126 = "MO0126";
        public const string MO0127 = "MO0127";
        public const string MO0128 = "MO0128";
        public const string MO0129 = "MO0129";
        public const string MO0130 = "MO0130";
        public const string MO0131 = "MO0131";
        public const string MO0132 = "MO0132";
        public const string MO0133 = "MO0133";
        public const string MO0134 = "MO0134";

        #endregion Movement

        #region Company Infomation

        public const string COM001 = "COM001";
        public const string COM002 = "COM002";

        #endregion Company Infomation

        #region Attachment

        public const string ATT001 = "ATT001";
        public const string ATT002 = "ATT002";
        public const string ATT003 = "ATT003";
        public const string ATT004 = "ATT004";

        #endregion Attachment

        #region Employment Status

        public const string ES0101 = "ES0101";
        public const string ES0102 = "ES0102";
        public const string ES0103 = "ES0103";
        public const string ES0104 = "ES0104";
        public const string ES0105 = "ES0105";
        public const string ES0106 = "ES0106";
        public const string ES0107 = "ES0107";
        public const string ES0108 = "ES0108";
        public const string ES0109 = "ES0109";
        public const string ES0110 = "ES0110";
        public const string ES0111 = "ES0111";
        public const string ES0112 = "ES0112";
        public const string ES0113 = "ES0113";

        #endregion Employment Status

        #region Termination

        public const string TE0101 = "TE0101";
        public const string TE0102 = "TE0102";
        public const string TE0103 = "TE0103";
        public const string TE0104 = "TE0104";
        public const string TE0105 = "TE0105";
        public const string TE0106 = "TE0106";
        public const string TE0107 = "TE0107";

        #endregion Termination

        #region Core User

        public const string USR0103 = "USR0103";

        #endregion Core User

        #region Holiday

        public const string HLD0101 = "HLD0101";
        public const string HLD0102 = "HLD0102";
        public const string HLD0103 = "HLD0103";
        public const string HLD0104 = "HLD0104";
        public const string HLD0105 = "HLD0105";
        public const string HLD0106 = "HLD0106";
        public const string HLD0107 = "HLD0107";
        public const string HLD0108 = "HLD0108";
        public const string HLD0109 = "HLD0109";
        public const string HLD0110 = "HLD0110";

        #endregion Holiday

        #region Work Calendar

        public const string WCL0101 = "WCL0101";
        public const string WCL0102 = "WCL0102";
        public const string WCL0103 = "WCL0103";
        public const string WCL0104 = "WCL0104";
        public const string WCL0105 = "WCL0105";
        public const string WCL0106 = "WCL0106";
        public const string WCL0107 = "WCL0107";
        public const string WCL0108 = "WCL0108";
        public const string WCL0109 = "WCL0109";
        public const string WCL0110 = "WCL0110";
        public const string WCL0111 = "WCL0111";
        public const string WCL0112 = "WCL0112";
        public const string WCL0113 = "WCL0113";
        public const string WCL0114 = "WCL0114";
        public const string WCL0115 = "WCL0115";
        public const string WCL0116 = "WCL0116";
        public const string WCL0117 = "WCL0117";
        public const string WCL0118 = "WCL0118";
        public const string WCL0119 = "WCL0119";
        public const string WCL0120 = "WCL0120";
        public const string WCL0121 = "WCL0121";
        public const string WCL0122 = "WCL0122";
        public const string WCL0123 = "WCL0123";
        public const string WCL0124 = "WCL0124";
        public const string WCL0125 = "WCL0125";
        public const string WCL0126 = "WCL0126";
        public const string WCL0127 = "WCL0127";

        #endregion Work Calendar
    }

    public struct TenantObject
    {
        public const string EMPLOYEE = "EMP";
        public const string DEPENDENT = "DEP";
    }

    public struct TextKey
    {
        public const string FIRST_NAME = "FIRST_NAME";
        public const string MIDDLE_NAME = "MIDDLE_NAME";
        public const string CHRISTIAN_NAME = "CHRISTIAN_NAME";
        public const string LAST_NAME = "LAST_NAME";
    }

    public struct CategoryImage
    {
        public const string LOGO = "logo";
        public const string PHOTO = "photo";
    }
}