﻿using AutoMapper;
using CommonLib.Models;
using CommonLib.Models.Client;
using CommonLib.Models.Core;
using EmploymentServiceAPIs.Common.Dtos;
using EmploymentServiceAPIs.Employee.Dtos.BankAccount;
using EmploymentServiceAPIs.Employee.Dtos.Dependent;
using EmploymentServiceAPIs.Employee.Dtos.EmergencyContact;
using EmploymentServiceAPIs.Employee.Dtos.Employee;
using EmploymentServiceAPIs.Employee.Dtos.EmploymentContract;
using EmploymentServiceAPIs.Employee.Dtos.EmploymentStatus;
using EmploymentServiceAPIs.Employee.Dtos.ManageFile;
using EmploymentServiceAPIs.Employee.Dtos.Movement;
using EmploymentServiceAPIs.Employee.Dtos.TaxArrangement;
using EmploymentServiceAPIs.Employee.Dtos.Termination;
using EmploymentServiceAPIs.EmployeePortal.Dtos;
using EmploymentServiceAPIs.ManageEntity.Dtos;
using EmploymentServiceAPIs.ManageEntity.Dtos.HolidayCalendar;
using EmploymentServiceAPIs.ManageEntity.Dtos.WorkCalendar;
using System;

namespace EmploymentServiceAPIs.Common.Mapping
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<TbCamClient, TbClientDto>();
            CreateMap<TbClientCreateDto, TbCamClient>()
                .ForMember(s => s.ClientCode, t => t.MapFrom(x => x.ClientCode))
                .ForMember(s => s.ClientNameTxt, t => t.MapFrom(x => x.ClientNameTxt))
                .ForMember(s => s.StatusCd, t => t.MapFrom(x => x.StatusCd))
                .ForMember(s => s.ContactPerson, t => t.MapFrom(x => x.ContactPerson));
            CreateMap<TbClientUpdateDto, TbCamClient>()
                .ForMember(s => s.ClientId, t => t.MapFrom(x => x.ClientId))
                .ForMember(s => s.ClientCode, t => t.MapFrom(x => x.ClientCode))
                .ForMember(s => s.ClientNameTxt, t => t.MapFrom(x => x.ClientNameTxt))
                .ForMember(s => s.StatusCd, t => t.MapFrom(x => x.StatusCd))
                .ForMember(s => s.ContactPerson, t => t.MapFrom(x => x.ContactPerson));
            CreateMap<TbClientEntityCreateDto, TbCamEntity>().ReverseMap();
            CreateMap<TbClientEntityManageDto, TbCamEntity>().ReverseMap();
            CreateMap<TbCamEntity, TbClientEntityDto>();
            CreateMap<TbCamEntitySupportUpdateDto, TbCamEntitySupport>()
                .ForMember(s => s.CreatedDt, t => t.MapFrom(x => DateTimeOffset.Parse(x.CreatedDt))).ReverseMap();
            CreateMap<TbClientDto, TbCamClient>().ReverseMap();
            CreateMap<TbSecDataAccessOrganizationStructure, DataAccessOrganizationStructureDto>().ReverseMap();
            CreateMap<TbCamEntitySupportDto, TbCamEntitySupport>().ReverseMap();
            CreateMap<TbCamEntityFaqDto, TbCamEntityFaq>().ReverseMap();

            CreateMap<TbCfgCostCenter, CostCenterViewModel>()
                .ForMember(s => s.Id, t => t.MapFrom(x => x.CostCenterId))
                .ForMember(s => s.Name, t => t.MapFrom(x => x.CostCenterName))
                .ForMember(s => s.Code, t => t.MapFrom(x => x.CostCenterCd))
                .ForMember(s => s.Status, t => t.MapFrom(x => x.StatusCd));

            CreateMap<TbCfgJobGrade, JobGradeViewModel>()
                .ForMember(s => s.Id, t => t.MapFrom(x => x.JobGradeId))
                .ForMember(s => s.Name, t => t.MapFrom(x => x.JobGradeName))
                .ForMember(s => s.Level, t => t.MapFrom(x => x.JobGradeLevel))
                .ForMember(s => s.Status, t => t.MapFrom(x => x.StatusCd));

            CreateMap<EmployeeRequest, TbEmpEmployee>()
                .ForMember(s => s.FirstName, t => t.MapFrom(x => x.FirstNamePrimary))
                .ForMember(s => s.MiddleName, t => t.MapFrom(x => x.MiddleNamePrimary))
                .ForMember(s => s.LastName, t => t.MapFrom(x => x.LastNamePrimary))
                .ForMember(s => s.ChristianName, t => t.MapFrom(x => x.ChristianNamePrimary))
                .ForMember(s => s.DisplayName, t => t.MapFrom(x => x.DisplayNamePrimary))
                .ForMember(s => s.ResidentialAddress1, t => t.MapFrom(x => x.ResidentialAddress1Primary))
                .ForMember(s => s.ResidentialAddress2, t => t.MapFrom(x => x.ResidentialAddress2Primary))
                .ForMember(s => s.ResidentialAddress3, t => t.MapFrom(x => x.ResidentialAddress3Primary))
                .ForMember(s => s.ResidentialCity, t => t.MapFrom(x => x.ResidentialCityPrimary))
                .ForMember(s => s.ResidentialState, t => t.MapFrom(x => x.ResidentialStatePrimary))
                .ForMember(s => s.ResidentialDistrict, t => t.MapFrom(x => x.ResidentialDistrictPrimary))
                .ForMember(s => s.PostalAddress1, t => t.MapFrom(x => x.PostalAddress1Primary))
                .ForMember(s => s.PostalAddress2, t => t.MapFrom(x => x.PostalAddress2Primary))
                .ForMember(s => s.PostalAddress3, t => t.MapFrom(x => x.PostalAddress3Primary))
                .ForMember(s => s.PostalCity, t => t.MapFrom(x => x.PostalCityPrimary))
                .ForMember(s => s.PostalState, t => t.MapFrom(x => x.PostalStatePrimary))
                .ForMember(s => s.PostalDistrict, t => t.MapFrom(x => x.PostalDistrictPrimary));

            CreateMap<DependentModel, TbEmpDependent>()
                .ForMember(s => s.FirstName, t => t.MapFrom(x => x.FirstNamePrimary))
                .ForMember(s => s.MiddleName, t => t.MapFrom(x => x.MiddleNamePrimary))
                .ForMember(s => s.LastName, t => t.MapFrom(x => x.LastNamePrimary))
                .ForMember(s => s.ChristianName, t => t.MapFrom(x => x.ChristianNamePrimary));
            CreateMap<TbEmpDependent, DependentViewModel>()
                .ForMember(s => s.FirstNamePrimary, t => t.MapFrom(x => x.FirstName))
                .ForMember(s => s.MiddleNamePrimary, t => t.MapFrom(x => x.MiddleName))
                .ForMember(s => s.ChristianNamePrimary, t => t.MapFrom(x => x.ChristianName))
                .ForMember(s => s.LastNamePrimary, t => t.MapFrom(x => x.LastName));
            CreateMap<DependentSaveModel, TbEmpDependent>();
            CreateMap<BankAccountModel, TbEmpEeBankAccount>();
            CreateMap<TbEmpEeBankAccount, BankAccountViewModel>();
            CreateMap<EmergencyContactRequest, TbEmpEmergencyContactPerson>();

            CreateMap<TbComFile, FileViewModel>();
            CreateMap<FileViewModel, TbComFile>();
            CreateMap<InforUploadModel, TbComFile>();
            CreateMap<FileEditModel, TbComFile>();
            CreateMap<TbCfgI18NDto, CommonLib.Models.Client.TbCfgI18n>();

            CreateMap<MovementViewModel, TbEmpMovement>();
            CreateMap<TbEmpMovement, MovementViewModel>();
            CreateMap<MovementModel, TbEmpMovement>();
            CreateMap<TbEmpTermination, TerminationViewModel>();

            CreateMap<EmploymentContractModel, TbEmpEmploymentContract>();
            CreateMap<TbEmpEmploymentContract, EmploymentContractViewModel>()
                .ForMember(s => s.Movements, t => t.MapFrom(x => x.TbEmpMovement))
                .ForMember(s => s.Terminations, t => t.MapFrom(x => x.TbEmpTermination))
                .ReverseMap();
            CreateMap<TaxRequest, TbEmpTax>();
            CreateMap<TbEmpEmployee, EmployeeViewModel>()
                .ForMember(s => s.FirstNamePrimary, t => t.MapFrom(x => x.FirstName))
                .ForMember(s => s.MiddleNamePrimary, t => t.MapFrom(x => x.MiddleName))
                .ForMember(s => s.LastNamePrimary, t => t.MapFrom(x => x.LastName))
                .ForMember(s => s.ChristianNamePrimary, t => t.MapFrom(x => x.ChristianName))
                .ForMember(s => s.DisplayNamePrimary, t => t.MapFrom(x => x.DisplayName))
                .ForMember(s => s.ResidentialAddress1Primary, t => t.MapFrom(x => x.ResidentialAddress1))
                .ForMember(s => s.ResidentialAddress2Primary, t => t.MapFrom(x => x.ResidentialAddress2))
                .ForMember(s => s.ResidentialAddress3Primary, t => t.MapFrom(x => x.ResidentialAddress3))
                .ForMember(s => s.ResidentialCityPrimary, t => t.MapFrom(x => x.ResidentialCity))
                .ForMember(s => s.ResidentialStatePrimary, t => t.MapFrom(x => x.ResidentialState))
                .ForMember(s => s.ResidentialDistrictPrimary, t => t.MapFrom(x => x.ResidentialDistrict))
                .ForMember(s => s.PostalAddress1Primary, t => t.MapFrom(x => x.PostalAddress1))
                .ForMember(s => s.PostalAddress2Primary, t => t.MapFrom(x => x.PostalAddress2))
                .ForMember(s => s.PostalAddress3Primary, t => t.MapFrom(x => x.PostalAddress3))
                .ForMember(s => s.PostalCityPrimary, t => t.MapFrom(x => x.PostalCity))
                .ForMember(s => s.PostalStatePrimary, t => t.MapFrom(x => x.PostalState))
                .ForMember(s => s.PostalDistrictPrimary, t => t.MapFrom(x => x.PostalDistrict));

            CreateMap<TbEmpEmployee, BankAccountDto>()
                 .ForMember(s => s.Remark, t => t.MapFrom(x => x.RemarkBankAccount))
                 .ForMember(s => s.BankAccounts, t => t.MapFrom(x => x.TbEmpEeBankAccount))
                .ReverseMap();

            CreateMap<TbEmpEmployee, DependentDto>()
                 .ForMember(s => s.Remark, t => t.MapFrom(x => x.RemarkDependent))
                 .ForMember(s => s.Dependents, t => t.MapFrom(x => x.TbEmpDependent))
                .ReverseMap();

            CreateMap<TbEmpEmployee, EmploymentContractDto>()
                 .ForMember(s => s.RemarkMovement, t => t.MapFrom(x => x.RemarkEmploymentMovement))
                 .ForMember(s => s.RemarkEmploymentContract, t => t.MapFrom(x => x.RemarkEmploymentContract))
                 .ForMember(s => s.EmploymentContracts, t => t.MapFrom(x => x.TbEmpEmploymentContract))
                .ReverseMap();

            //
            CreateMap<TbEmpEmployee, EmpDetailDto>();

            CreateMap<TbEmpEmploymentStatus, EmploymentStatusViewModel>();
            CreateMap<EmploymentStatusModel, TbEmpEmploymentStatus>();

            CreateMap<TerminationSaveModel, TbEmpTermination>();

            CreateMap<HolidayModel, TbCamHoliday>();
            CreateMap<TbCamHoliday, HolidayViewModel>();
            CreateMap<WorkCalendarModel, TbCamWorkCalendar>();
            CreateMap<TbCamWorkCalendar, WorkCalendarViewModel>()
                 .ForMember(s => s.Holidays, t => t.MapFrom(x => x.TbCamHoliday))
                .ReverseMap(); ;

            CreateMap<TbCfgHoliday, HolidayModel>();
        }
    }
}