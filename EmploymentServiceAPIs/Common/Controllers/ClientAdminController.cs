using CommonLib.Dtos;
using EmploymentServiceAPIs.Common.Dtos;
using EmploymentServiceAPIs.Common.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;
using EmploymentServiceAPIs.Employee.Services.Interfaces;
using EmploymentServiceAPIs.Common.Constants;

namespace EmploymentServiceAPIs.Common.Controllers
{
    // All Client related APIs to be processed here
    [Route("api/CAM")]
    [ApiController]
    [Authorize]
    public class ClientAdminController : ControllerBase
    {
        private readonly ILogger<ClientAdminController> _logger;
        private readonly ITbCamClientService _clientService;
        private readonly ITbCamEntityService _clientEntityService;
        private readonly ITbCamEntityFileService _entityFileService;
        private readonly IEmployeeService _employmentService;

        /// <param name="clientService"></param>
        /// <param name="logger"></param>
        /// <param name="clientEntityService"></param>
        /// <param name="entityFileService"></param>
        /// <param name="employmentStatusRepository"></param>
        /// <returns></returns>
        public ClientAdminController(
            ITbCamClientService clientService,
            ILogger<ClientAdminController> logger,
            ITbCamEntityService clientEntityService,
            IEmployeeService employmentStatusRepository,
            ITbCamEntityFileService entityFileService)
        {
            _entityFileService = entityFileService;
            _clientService = clientService;
            _logger = logger;
            _clientEntityService = clientEntityService;
            _employmentService = employmentStatusRepository;
        }

        #region client

        /// <summary>
        /// Get all clients
        /// </summary>
        /// <returns></returns>
        [HttpGet("CAM1101/getall")]
        [Authorize(Roles = "CAM1101")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAllClient()
        {
            try
            {
                _logger.LogInformation("GetAllClient.");
                var response = await _clientService.GetAll();
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Get Client by clientId
        /// </summary>
        /// <param name="request"></param>
        [HttpGet("CAM1101/getbyclientid")]
        [Authorize(Roles = "CAM1101")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetByClientId([FromQuery] ApiRequest<int> request)
        {
            try
            {
                _logger.LogInformation("GetByClientId - clientID {id}", request.Data);
                var response = await _clientService.GetClientById(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Create client
        /// </summary>
        /// <param name="request"></param>
        [HttpPost]
        [Route("CAM1102/create")]
        [Authorize(Roles = "CAM1102")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreateClient([FromBody] ApiRequest<TbClientCreateDto> request)
        {
            try
            {
                string username = User.FindFirst(ClaimTypes.Name).Value;
                _logger.LogInformation($"CreateClient - Username: {username}");
                var response = await _clientService.Create(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Update client
        /// </summary>
        /// <param name="request"></param>
        [HttpPost]
        [Authorize(Roles = "CAM1103")]
        [Route("CAM1103/update")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateClient([FromBody] ApiRequest<TbClientUpdateDto> request)
        {
            try
            {
                string username = User.FindFirst(ClaimTypes.Name).Value;
                _logger.LogInformation($"UpdateClient - Username: {username}");
                var response = await _clientService.Update(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Count active employee
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet("CAM1201/countactiveemployee")]
        [Authorize(Roles = "CAM1201")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CountActiveEmployee([FromQuery] ApiRequest request)
        {
            //TODO: Change ClientCode, EntityCode to ClientId, EntityId
            //_logger.LogInformation("CountActiveEmplyee {entityCode}", request.Parameter.EntityCode);
            try
            {
                _logger.LogInformation($"CountActiveEmployee - {request.Parameter.EntityId}");
                var response = await _employmentService.CountByStatus(EmployeeStatus.ACTIVE);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        #endregion client

        #region entity

        // -------------- Manage Entities CAM12xx-------------------------------------------
        /// <summary>
        /// Get Entity by client id
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet("CAM1201/getall")]
        [Authorize(Roles = "CAM1201")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAllClientEntity([FromQuery] ApiRequest<string> request)
        {
            try
            {
                _logger.LogInformation($"GetAllClientEntity - {request.Data}");
                var response = await _clientEntityService.GetAll(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Get Entity by client id and entity id
        /// </summary>
        /// <param name="request"></param>
        /// <param name="clientId"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpGet("CAM1201/getentity/{clientId}/{entityId}")]
        [Authorize(Roles = "CAM1201")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetEntityById([FromQuery] ApiRequest<string> request, int clientId, int entityId)
        {
            try
            {
                _logger.LogInformation($"GetEntityById --- ClientId: {clientId} , EntityId: {entityId}");
                var response = await _clientEntityService.GetById(request.Ray, clientId, entityId);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Create entity
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CAM1202/create")]
        [Authorize(Roles = "CAM1202")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreateEntity([FromBody] ApiRequest<TbClientEntityCreateDto> request)
        {
            try
            {
                string username = User.FindFirst(ClaimTypes.Name).Value;
                _logger.LogInformation($"CreateEntity - {request}", JsonSerializer.Serialize(request));
                var response = await _clientEntityService.Create(request, username);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Manage entity
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CAM1202/manage")]
        [Authorize(Roles = "CAM1202")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ManageEntity([FromBody] ApiRequest<TbClientEntityManageDto> request)
        {
            try
            {
                _logger.LogInformation($"ManageEntity - {request}", JsonSerializer.Serialize(request));
                string username = User.FindFirst(ClaimTypes.Name).Value;
                var response = await _clientEntityService.Update(request, username);
                if (response.Data == null && response.Message.Toast[0].Level == Level.ERROR)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        #endregion entity

        #region entity file

        /// <summary>
        /// Upload entity logo by entity id
        /// </summary>
        /// <param name="request"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CAM1202/logoupload/{id}")]
        [Authorize(Roles = "CAM1202")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> EntityLogoUpload([FromForm] EntityFileUploadDto request, int id)
        {
            try
            {
                _logger.LogInformation($"EntityLogoUpload - {id}");
                var response = await _entityFileService.UploadEntityFile(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Download entity logo by entity id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("CAM1201/logo/{id}")]
        [Authorize(Roles = "CAM1201")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> EntityLogo(int id)
        {
            try
            {
                _logger.LogInformation($"EntityLogo - {id}");
                var image = await _entityFileService.DownloadFileByEntityId(id);

                if (image.Data.FileContent != null)
                {
                    return File(image.Data.FileContent, "application/octet-stream", image.Data.File.FileName);
                }
                else
                {
                    return Ok(image);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /* [HttpGet("getclients")]
        public async Task<IActionResult> GetClients()
        {
            var clients = await _clientService.GetClients();
            return Ok(clients);
        } */
        #endregion entity file
    }
}