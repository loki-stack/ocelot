﻿using Antlr4.StringTemplate;
using CommonLib.Dtos;
using CommonLib.Services;
using EmploymentServiceAPIs.Common.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Common.Controllers
{
    /// <summary>
    /// Return public information about the application
    /// </summary>
    [Route("api/COM/[action]/[controller]")]
    [ApiController]
    [Authorize]
    public class EmplPublicInfoController : ControllerBase
    {
        private readonly ILogger<EmplPublicInfoController> _logger;
        private readonly IConfiguration _config;
        private readonly IWebHostEnvironment _env;
        private readonly AppSettings _appSettings;
        private readonly IMessageService _messageService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="config"></param>
        /// <param name="env"></param>
        /// <param name="messageService"></param>
        /// <param name="appSettings"></param>
        public EmplPublicInfoController(
            ILogger<EmplPublicInfoController> logger,
            IConfiguration config,
            IWebHostEnvironment env,
            IMessageService messageService,
            IOptions<AppSettings> appSettings)
        {
            this._logger = logger;
            this._config = config;
            this._env = env;
            this._appSettings = appSettings.Value;
            this._messageService = messageService;
        }

        /// <summary>
        /// Get public information of the application
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [ActionName("COM9901")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<PublicInfoDto>>> PublicInfo([FromQuery] ApiRequest request)
        {
            _logger.LogInformation($"PublicInfo");
            ApiResponse<PublicInfoDto> apiResponse = new ApiResponse<PublicInfoDto>();
            PublicInfoDto data = new PublicInfoDto();

            await Task.Run(
                () =>
                {
                    data.Environment = this._env.EnvironmentName;
                    data.ApplicationName = this._env.ApplicationName;
                    data.DbInfo = GetDbBaseInfo(this._config["ConnectionStrings:CoreConnection"]);
                    data.CoreServiceGenericCodeUri = this._appSettings.CoreServiceGenericCode;
                    // test string template
                    // data.TestInfo = TestTemplate();
                    // TODO - in future - get GitInfo, version, build timestamp
                }
            );
            apiResponse.Data = data;
            return apiResponse;
        }

        private string GetDbBaseInfo(string input)
        {
            string[] subs = input.Split(';');
            string[] vals = new string[2];

            foreach (var sub in subs)
            {
                if (sub.Trim().ToLower().StartsWith("Username"))
                {
                    vals[0] = sub;
                }
                else if (sub.Trim().ToLower().StartsWith("Password"))
                {
                    vals[1] = sub;
                }

            }
            return string.Join(';', vals);
        }

        // private string TestTemplate()
        // {

        //     // TODO - cannot support nested dictionary
        //     string json = "{\"data\": \"json text here\", \"inner\":{\"data\": \"json text here\"}}";
        //     // Dictionary<string, dynamic> jsonDict = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(json);
        //     //JsonSerializer 

        //     ClientViewModel clientObj = new ClientViewModel();
        //     clientObj.ClientName = "client name here";
        //     Type dynaType = Type.GetType("EmploymentServiceAPIs.Common.Dtos.ClientViewModel");
        //     string jsonClient = JsonSerializer.Serialize(clientObj);

        //     var client = JsonSerializer.Deserialize(jsonClient, dynaType);


        //     Dictionary<string, dynamic> jsonDict = JsonSerializer.Deserialize<Dictionary<string, dynamic>>(json);
        //     string templateText = "test $data$. This is json - $json.data$. Test escape \\$dd\\$"
        //     + ". Test dyna serialize: $client.ClientName$";


        //     Template template = new Template(templateText, '$', '$');

        //     template.Add("data", "it is great");
        //     template.Add("json", jsonDict);
        //     template.Add("client", client);
        //     return template.Render();
        // }

        #region Testing Purpose Only

        /// <summary>
        /// Create Message Template 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ActionName("COM9902/MesageTemplate")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> CreateMessageTemplate([FromBody] ApiRequest<MessageTemplateCreateDto> request)
        {
            try
            {
                _logger.LogInformation($"CreateMessageTemplate");
                string username = "SYSTEM";
                var response = await _messageService.Create(request.Data, username);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Create Message Template Language
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ActionName("COM9902/MessageTemplateLang")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> CreateMessageTemplateLang([FromBody] ApiRequest<MessageTemplateLangCreateDto> request)
        {
            try
            {
                // string username = User.FindFirst(ClaimTypes.Name).Value;
                _logger.LogInformation($"CreateMessageTemplateLang");
                string username = "SYSTEM";
                var response = await _messageService.Create(request.Data, username);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Create Message
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ActionName("COM9902/Message")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> CreateMessage([FromBody] ApiRequest<MessageCreateDto> request)
        {
            try
            {
                _logger.LogInformation($"CreateMessage");
                // string username = User.FindFirst(ClaimTypes.Name).Value;
                string username = "SYSTEM";
                var response = await _messageService.Create(request.Data, username);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Update Message Template 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ActionName("COM9903/MesageTemplate")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> UpdateMessageTemplate([FromBody] ApiRequest<MessageTemplateUpdateDto> request)
        {
            try
            {
                _logger.LogInformation($"UpdateMessageTemplate");
                // string username = User.FindFirst(ClaimTypes.Name).Value;
                string username = "SYSTEM";
                var response = await _messageService.Update(request.Data, username);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Update Message Template Language
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ActionName("COM9903/MessageTemplateLang")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> UpdateMessageTemplateLang([FromBody] ApiRequest<MessageTemplateLangUpdateDto> request)
        {
            try
            {
                _logger.LogInformation($"UpdateMessageTemplateLang");
                // string username = User.FindFirst(ClaimTypes.Name).Value;
                string username = "SYSTEM";
                var response = await _messageService.Update(request.Data, username);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Update Message
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ActionName("COM9903/Message")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> UpdateMessage([FromBody] ApiRequest<MessageUpdateDto> request)
        {
            try
            {
                _logger.LogInformation($"UpdateMessage");
                // string username = User.FindFirst(ClaimTypes.Name).Value;
                string username = "SYSTEM";
                var response = await _messageService.Update(request.Data, username);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        #endregion Testing Purpose Only

        /// <summary>
        /// Send Email
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [ActionName("COM9901/sendMail")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> SendMail([FromQuery] ApiRequest request)
        {
            try
            {
                _logger.LogInformation($"SendMail");
                // string username = User.FindFirst(ClaimTypes.Name).Value;
                string username = "SYSTEM";
                var response = await _messageService.ProcessEmails(username);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }


}