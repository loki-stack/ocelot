using CommonLib.Dtos;
using EmploymentServiceAPIs.Common.Dtos;
using EmploymentServiceAPIs.Common.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Text.Json;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Common.Controllers
{
    // to be removed - for simplicity - all api/CAM - will be handled by
    // ClientManagementController
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ManageClientController : ControllerBase
    {
        private readonly ILogger<ManageClientController> _logger;
        private readonly ITbCamClientService _clientService;

        public ManageClientController(ITbCamClientService clientService, ILogger<ManageClientController> logger)
        {
            _clientService = clientService;
            _logger = logger;
        }

        [HttpGet("getall")]
        public async Task<IActionResult> GetAllClient()
        {
            _logger.LogInformation("GetAllClient with request.");
            var client = await _clientService.GetAll();
            return Ok(client);
        }

        [HttpGet("getbyclientid")]
        public async Task<IActionResult> GetByClientId([FromQuery] ApiRequest<int> request)
        {
            _logger.LogInformation("GetByClientId with clientID - " + request.Data);
            var client = await _clientService.GetClientById(request);
            return Ok(client);
        }

        [HttpPost("create")]
        public async Task<IActionResult> Create([FromBody] ApiRequest<TbClientCreateDto> request)
        {
            _logger.LogInformation("CREATE New Client [{request}].", JsonSerializer.Serialize(request));
            var client = await _clientService.Create(request);
            return Ok(client);
        }

        [HttpPost("update")]
        public async Task<IActionResult> Update([FromBody] ApiRequest<TbClientUpdateDto> request)
        {
            _logger.LogInformation("UPDATE Client [{request}].", JsonSerializer.Serialize(request));
            var client = await _clientService.Update(request);
            return Ok(client);
        }

        [HttpGet("getclients")]
        public async Task<IActionResult> GetClients()
        {
            var clients = await _clientService.GetClients();
            return Ok(clients);
        }
    }
}