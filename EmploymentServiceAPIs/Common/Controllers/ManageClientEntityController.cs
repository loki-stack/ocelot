using CommonLib.Dtos;
using EmploymentServiceAPIs.Common.Dtos;
using EmploymentServiceAPIs.Common.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Common.Controllers
{
    // to be removed - for simplicity - all api/CAM - will be handled by
    // ClientManagementController
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ManageClientEntityController : ControllerBase
    {
        private readonly ILogger<ManageClientEntityController> _logger;
        private readonly ITbCamEntityService _clientEntityService;

        public ManageClientEntityController(ITbCamEntityService clientEntityService,
            ILogger<ManageClientEntityController> logger)
        {
            _clientEntityService = clientEntityService;
            _logger = logger;
        }

        [HttpGet("getall")]
        public async Task<IActionResult> GetAllClientEntity([FromQuery] ApiRequest<string> request)
        {
            _logger.LogInformation("GetAllClientEntity with request [{request}].", JsonSerializer.Serialize(request));
            var clientEntity = await _clientEntityService.GetAll(request);
            return Ok(clientEntity);
        }

        // [HttpGet("getbycliententitycode")]
        // public async Task<IActionResult> GetClientEntityByCode([FromQuery] ApiRequest<string> request)
        // {
        //     _logger.LogInformation("GetClientEntityByCode with request [{request}].", JsonSerializer.Serialize(request));
        //     var clientEntity = await _clientEntityService.GetClientEntityByCode(request);
        //     return Ok(clientEntity);
        // }

        // [HttpPost("create")]
        // public async Task<IActionResult> Create([FromBody] ApiRequest<TbClientEntityCreateDto> newClientEntity)
        // {
        //     _logger.LogInformation("CREATE New ClientEntity [{newClient}].", JsonSerializer.Serialize(newClientEntity));
        //     var clientEntity = await _clientEntityService.Create(newClientEntity);
        //     return Ok(clientEntity);
        //}

        // [HttpPost("update")]
        // [ProducesResponseType(StatusCodes.Status200OK)]
        // [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        // public async Task<IActionResult> Update([FromBody] ApiRequest<TbClientEntityUpdateDto> updateClientEntity)
        // {
        //     try
        //     {
        //         _logger.LogInformation($"Update {updateClientEntity}", JsonSerializer.Serialize(updateClientEntity));
        //         var response = await _clientEntityService.Update(updateClientEntity);
        //         return Ok(response);
        //     }
        //     catch (Exception ex)
        //     {
        //         _logger.LogError(ex, ex.Message);
        //         return StatusCode(StatusCodes.Status500InternalServerError);
        //     }
        // }
    }
}