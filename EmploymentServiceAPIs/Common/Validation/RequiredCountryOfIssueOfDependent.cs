﻿using EmploymentServiceAPIs.Employee.Dtos.Dependent;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EmploymentServiceAPIs.Common.Validation
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class RequiredCountryOfIssueOfDependent : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var instance = validationContext.ObjectInstance as DependentModel;

            if (!string.IsNullOrEmpty(instance.PassportIdNo) && string.IsNullOrEmpty((string)value))
            {
                return new ValidationResult(this.ErrorMessage, new List<string>() { validationContext.MemberName });
            }
            return ValidationResult.Success;
        }
    }
}