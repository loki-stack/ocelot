﻿using EmploymentServiceAPIs.Employee.Dtos.Termination;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EmploymentServiceAPIs.Common.Validation
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class LastEmploymentDateValidate : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var instance = validationContext.ObjectInstance as TerminationSaveModel;
            if (instance.LastEmploymentDate != null && (instance.TerminationContractEndDate != null && (instance.LastEmploymentDate > instance.TerminationContractEndDate)))
            {
                return new ValidationResult(this.ErrorMessage, new List<string>() { validationContext.MemberName });
            }
            return ValidationResult.Success;
        }
    }
}