﻿using EmploymentServiceAPIs.Employee.Dtos.EmploymentContract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EmploymentServiceAPIs.Common.Validation
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class RequiredExpectedProbationEndDate : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var instance = validationContext.ObjectInstance as EmploymentContractAddModel;

            if (instance.Probation && instance.ExpectedProbationEndDate == null)
            {
                return new ValidationResult(this.ErrorMessage, new List<string>() { validationContext.MemberName });
            }
            return ValidationResult.Success;
        }
    }
}