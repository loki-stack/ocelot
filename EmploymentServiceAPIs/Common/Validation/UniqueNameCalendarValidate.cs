﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using EmploymentServiceAPIs.ManageEntity.Dtos.WorkCalendar;
using EmploymentServiceAPIs.ManageEntity.Repositories.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace EmploymentServiceAPIs.Common.Validation
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class UniqueNameCalendarValidate : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var workCalendarRepository = validationContext.GetService<IWorkCalendarRepository>();
            var instance = validationContext.ObjectInstance as WorkCalendarModel;

            if (workCalendarRepository.CheckExistNameCalendarInEntity(instance.SpecificEntityId.Value, (string)value, instance.CalendarId))
            {
                return new ValidationResult(this.ErrorMessage, new List<string>() { validationContext.MemberName });
            }
            return ValidationResult.Success;
        }
    }
}