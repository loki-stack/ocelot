﻿using EmploymentServiceAPIs.Employee.Dtos.EmploymentContract;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EmploymentServiceAPIs.Common.Validation
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class UniqueEmployeeNoValidate : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var ecRepository = validationContext.GetService<IEmploymentContractRepository>();

            var instance = validationContext.ObjectInstance as EmploymentContractModel;

            if (!string.IsNullOrEmpty((string)value) && ecRepository.CheckExistEmployeeNo(instance.ClientId, instance.EntityId, instance.EmployeeId.Value, (string)value))
            {
                return new ValidationResult(this.ErrorMessage, new List<string>() { validationContext.MemberName });
            }
            return ValidationResult.Success;
        }
    }
}