﻿using EmploymentServiceAPIs.Employee.Dtos.EmploymentContract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EmploymentServiceAPIs.Common.Validation
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class DateOfAcceptingTheOfferValidate : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var instance = validationContext.ObjectInstance as EmploymentContractAddModel;

            if (instance.DateOfAcceptingTheOffer != null
                && (instance.DateOfAcceptingTheOffer.Value.Date.CompareTo(DateTime.Now.Date) > 0))
            {
                return new ValidationResult(this.ErrorMessage, new List<string>() { validationContext.MemberName });
            }
            return ValidationResult.Success;
        }
    }
}