﻿using EmploymentServiceAPIs.Employee.Dtos.EmploymentContract;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EmploymentServiceAPIs.Common.Validation
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class RequiredEmployeeNoValidate : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var empStatusRepository = validationContext.GetService<IEmploymentStatusRepository>();
            var instance = validationContext.ObjectInstance as EmploymentContractModel;
            var isCheck = empStatusRepository.CheckActiveStatusLevel1(instance.EmploymentContractId);
            if (!isCheck && string.IsNullOrEmpty((string)value))
            {
                return new ValidationResult(this.ErrorMessage, new List<string>() { validationContext.MemberName });
            }
            return ValidationResult.Success;
        }
    }
}