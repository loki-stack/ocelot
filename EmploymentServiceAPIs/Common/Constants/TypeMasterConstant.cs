﻿namespace EmploymentServiceAPIs.Common.Constants
{
    public struct TypeMasterConstant
    {
        /// <summary>
        /// Payroll item flags
        /// </summary>
        public const string PAYROLL_ITEM_FLAGS_CODE = "00001";
        /// <summary>
        /// Payroll item configuration display mode
        /// </summary>
        public const string DISPLAY_MODE_CODE = "00006";
    }

    public struct PayrollListTaxSelecteMaster
    {
        public const string TAX_CATEGORIES_ITEM_IR56B = "00002";
        public const string TAX_CATEGORIES_ITEM_IR56F = "00003";
        public const string TAX_CATEGORIES_ITEM_IR56G = "00004";
    }

    public struct PayrollTaxSelecteMaster
    {
        public const string PAYROLL_TAX_SELECT_IR56x = "00005";
    }
}
