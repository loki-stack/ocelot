﻿namespace EmploymentServiceAPIs.Common.Constants
{
    public struct UserStatus
    {
        public const string ACTIVE_CD = "AT";
        public const string INACTIVE_CD = "IT";
        public const string ACTIVE_VALUE = "Active";
        public const string INACTIVE_VALUE = "Inactive";
    }

    public enum StatusCode
    {
        SUCCESS = 0,
        INVALID = -1,
        ACCOUNT_LOCKED = -2
    }

    public struct UnitStatusCode
    {
        public const string INACTIVE = "INACTIVE";
        public const string ACTIVE = "ACTIVE";
        public const string ACTIVE_CD = "AT";
        public const string INACTIVE_CD = "IT";
    }

    public struct TableStatusCode
    {
        public const string INACTIVE = "INACTIVE";
        public const string ACTIVE = "ACTIVE";
    }

    public struct JWTKey
    {
        public const string CLIENT_ID = "ClientId";
        public const string ENTITY_ID = "EntityId";
        public const string TOKEN_KEY = "access_token";
    }

    /// <summary>
    /// Display Model
    /// </summary>
    public struct DisplayModel
    {
        public const byte ALWAYS_HIDDEN = 0;
        public const byte ALWAYS_SHOWN = 1;
        public const byte HIDDEN_IF_ZERO = 2;
    }

    /// <summary>
    /// Entity Status
    /// </summary>
    public struct EntityStatus
    {
        /// <summary>
        /// ACTIVE Status code
        /// </summary>
        public const string ACTIVE = "AC";
    }

    /// <summary>
    /// Employee Status
    /// </summary>
    public struct EmployeeStatus
    {
        /// <summary>
        /// ACTIVE Status code
        /// </summary>
        public const string ACTIVE = "AC";
    }

    /// <summary>
    /// Client Status
    /// </summary>
    public struct ClientStatus
    {
        /// <summary>
        /// PENDING Status code
        /// </summary>
        public const string PENDING = "STATUS_CLT.PD";
    }

    public struct Status
    {
        public const string ACTIVE = "ACTIVE";
        public const string INACTIVE = "INACTIVE";
    }
}