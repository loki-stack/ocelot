﻿namespace EmploymentServiceAPIs.Common.Constants
{
    public struct Level
    {
        public const string ERROR = "ERROR";
        public const string SUCCESS = "SUCCESS";
        public const string INFO = "INFO";
    }

    public struct Label
    {
        #region Organization Structure
        // Organization Structure Not Exists
        public const string OS0101 = "OS0101";
        // Organization Structure Cannot InActive. Organization Structure have employee
        public const string OS0102 = "OS0102";
        // Organization Structure Add Success
        public const string OS0103 = "OS0103";
        // Organization Structure Add Fail
        public const string OS0104 = "OS0104";
        // Organization Structure Edit Success
        public const string OS0105 = "OS0105";
        // Organization Structure Edit Fail
        public const string OS0106 = "OS0106";

        /// <summary>
        /// Fail to retrieve Structure Organization. Please try again.
        /// </summary>
        public const string OS0107 = "OS0107";
        #endregion

        #region Payroll Profile
        /// <summary>
        /// Add Payroll Profile Fail
        /// </summary>
        public const string PAY0201 = "PAY0201";
        /// <summary>
        /// Add Payroll Profile Item Fail
        /// </summary>
        public const string PAY0202 = "PAY0202";
        /// <summary>
        /// Add Payroll Profile Success
        /// </summary>
        public const string PAY0203 = "PAY0203";
        /// <summary>
        /// Payroll Profile Not Exist
        /// </summary>
        public const string PAY0204 = "PAY0204";
        /// <summary>
        /// Update Payroll Profile Fail
        /// </summary>
        public const string PAY0205 = "PAY0205";
        /// <summary>
        /// Delete Payroll Profile Item Fail
        /// </summary>
        public const string PAY0206 = "PAY0206";
        /// <summary>
        /// Update Payroll Profile Success
        /// </summary>
        public const string PAY0207 = "PAY0207";
        /// <summary>
        /// Update Payroll Profile Item Configuration Fail
        /// </summary>
        public const string PAY0208 = "PAY0208";
        /// <summary>
        /// Update Payroll Profile Item Configuration Success
        /// </summary>
        public const string PAY0209 = "PAY0209";
        /// <summary>
        /// Master Payroll Item Not Exist
        /// </summary>
        public const string PAY0210 = "PAY0210";
        /// <summary>
        /// Duplicate Payroll Profile Fail
        /// </summary>
        public const string PAY0211 = "PAY0211";
        /// <summary>
        /// Duplicate Payroll Profile Success
        /// </summary>
        public const string PAY0212 = "PAY0212";

        #endregion

        #region "GL Code"
        /// <summary>
        /// Currency Not Found
        /// </summary>
        public const string GLC0101 = "GLC0101";
        /// <summary>
        /// Add GL Code Fail
        /// </summary>
        public const string GLC0102 = "GLC0102";
        /// <summary>
        /// Add GL Code Success
        /// </summary>
        public const string GLC0103 = "GLC0103";
        /// <summary>
        /// GL Code Not Exist
        /// </summary>
        public const string GLC0104 = "GLC0104";
        /// <summary>
        /// GL Code Update Fail
        /// </summary>
        public const string GLC0105 = "GLC0105";
        /// <summary>
        /// GL Code Update Success
        /// </summary>
        public const string GLC0106 = "GLC0106";
        #endregion

        #region Entity

        /// <summary>
        /// Entity Update Success
        /// </summary>
        public const string CAM1201 = "CAM1201";

        /// <summary>
        /// Entity Update Fail
        /// </summary>
        public const string CAM1202 = "CAM1202";

        #endregion Entity

        #region Employee Portal

        /// <summary>
        /// Please try again.
        /// </summary>
        public const string ESS50_E_006 = "ESS50_E_006";

        #endregion Employee Portal

    }

    public struct MasterPayrollItemLabel
    {
        #region MasterPayrollItem
        // PayrollItem Not Exists
        public const string MPRI0101 = "PRI0101";
        // PayrollItem Cannot InActive. PayrollItem not display
        public const string MPRI0102 = "PRI0102";
        // PayrollItem Add Success
        public const string MPRI0103 = "PRI0103";
        // PayrollItem Add Fail
        public const string MPRI0104 = "PRI0104";
        // PayrollItem Update Success
        public const string MPRI0105 = "PRI0105";
        // PayrollItem Update Fail
        public const string MPRI0106 = "PRI0106";
        /// <summary>
        /// Duplicate Payroll Item Code
        /// </summary>
        public const string MPRI0107 = "PRI0107";
        //Delete Payroll Item Code
        public const string MPRI0108 = "PRI0108";
        //Duplicate Payroll Item Code Success
        public const string MPRI0109 = "PRI0109";
        #endregion
    }

    public struct ValMsg
    {
        public const string VAL_REQUIRED = "VAL_REQUIRED";
        public const string VAL_MAXLENGTH = "VAL_MAXLENGTH";
        public const string VAL_DUPLICATE = "VAL_DUPLICATE";
    }
    public struct EmployeePortalLabel
    {
        #region Payslip
        /// <summary>
        /// Download PaySlip fail
        /// </summary>
        public const string PAY5001 = "PAY5001";

        #endregion Payslip

        #region TaxReturn
        /// <summary>
        /// Download PaySlip fail
        /// </summary>
        public const string PAY5101 = "PAY5101";

        #endregion TaxReturn
    }
}