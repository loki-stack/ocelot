﻿using CommonLib.Models.Client;
using CommonLib.Utils;
using EmploymentServiceAPIs.Employee.Dtos.EmploymentStatus;
using EmploymentServiceAPIs.ManageEntity.Dtos;
using Microsoft.EntityFrameworkCore;

namespace EmploymentServiceAPIs.Common.Models
{
    public class TenantDBContext : ClientBasicDBContext
    {
        public virtual DbSet<OrganizationStructureModel> OrganizationStructures { get; set; }
        public virtual DbSet<OrganizationStructureDisplayNameModel> OrganizationStructureDisplayNames { get; set; }
        public virtual DbSet<EmploymentStatusResult> EmploymentStatusResults { get; set; }

        public override void OnModelCreatingExtention(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OrganizationStructureModel>().HasNoKey();
            modelBuilder.Entity<OrganizationStructureDisplayNameModel>().HasNoKey();
            modelBuilder.Entity<EmploymentStatusResult>().HasNoKey();
        }

        public TenantDBContext(string connectionStr) : base(DbContextUtil.CreateDDRConnection<TenantDBContext>(connectionStr))
        {
        }
    }
}