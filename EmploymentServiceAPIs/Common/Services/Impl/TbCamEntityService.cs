using Antlr4.StringTemplate;
using AutoMapper;
using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using CommonLib.Repositories.Interfaces;
using CommonLib.Services;
using CommonLib.Validation;
using EmploymentServiceAPIs.Common.Constants;
using EmploymentServiceAPIs.Common.Dtos;
using EmploymentServiceAPIs.Common.Repositories.Interfaces;
using EmploymentServiceAPIs.Common.Services.Interfaces;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;


namespace EmploymentServiceAPIs.Common.Services.Impl
{
    /// <summary>
    /// Impl
    /// </summary>
    public class TbCamEntityService : ITbCamEntityService
    {
        private readonly ILogger<TbCamEntityService> _logger;
        private readonly ITbCamEntityFaqRepository _faqRepository;
        private readonly ITbCamEntitySupportRepository _supportRepository;
        private readonly ITbCamEntityRepository _clientEntityRepository;
        private readonly IMapper _mapper;
        private readonly IValidator _validator;
        protected readonly IMessageTemplateRepository _messageTemplateRepository;
        protected readonly MailSettings _mailSettings;
        protected readonly IEmpEmailService _empEmailService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="clientEntityRepository"></param>
        /// <param name="mapper"></param>
        /// <param name="faqRepository"></param>
        /// <param name="supportRepository"></param>
        /// <param name="logger"></param>
        /// <param name="validator"></param>
        /// <param name="messageTemplateRepository"></param>
        /// <param name="mailSettings"></param>
        /// <param name="empEmailService"></param>
        /// <returns></returns>
        public TbCamEntityService(
            ITbCamEntityRepository clientEntityRepository
            , IMapper mapper
            , ITbCamEntityFaqRepository faqRepository
            , ITbCamEntitySupportRepository supportRepository
            , ILogger<TbCamEntityService> logger
            , IValidator validator
            , IMessageTemplateRepository messageTemplateRepository
            , IOptions<MailSettings> mailSettings
            , IEmpEmailService empEmailService
            )
        {
            _clientEntityRepository = clientEntityRepository;
            _mapper = mapper;
            _logger = logger;
            _faqRepository = faqRepository;
            _supportRepository = supportRepository;
            _validator = validator;
            _messageTemplateRepository = messageTemplateRepository;
            _mailSettings = mailSettings.Value;
            _empEmailService = empEmailService;
        }

        /// <summary>
        /// Get All Entity
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<List<TbClientEntityDto>>> GetAll(ApiRequest<string> request)
        {
            _logger.LogInformation($"GetAll - {request.Parameter.ClientId}.");
            var response = new ApiResponse<List<TbClientEntityDto>>();
            response.Ray = request.Ray;
            try
            {
                var result = await _clientEntityRepository.GetAll(request.Parameter.ClientId);
                response.Data = result;
                return response;

            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return response;
            }
        }

        /// <summary>
        /// Quick Create New Entity
        /// </summary>
        /// <param name="request"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        public async Task<ApiResponse<int>> Create(ApiRequest<TbClientEntityCreateDto> request, string username)
        {
            _logger.LogInformation($"Create - {request.Data.EntityCode}.");
            ApiResponse<int> response = new ApiResponse<int>();
            response.Ray = request.Ray;
            try
            {
                bool isValid = await validate(request.Data, response.Message.ValidateResult, true, request.Data.EntityCode, 0);
                if (isValid)
                {
                    TbCamEntity newEntity = _mapper.Map<TbClientEntityCreateDto, TbCamEntity>(request.Data);
                    int newEntityId = await _clientEntityRepository.Create(newEntity, request.Parameter.ClientId);
                    if (newEntityId > 0)
                    {
                        _logger.LogInformation($"Success {newEntityId} ");
                        response.Data = newEntityId;

                        var templateCode = CommonLib.Constants.Messages.MessageTemplateCode.EM_ENTITY_NEW;
                        var templateType = CommonLib.Constants.Messages.MessageTemplateType.Email;
                        var template = await _messageTemplateRepository.GetById(
                                    0, //SYSTEM DEFAULT 
                                    templateCode,
                                    templateType);
                        if (template != null)
                        {
                            var templateLang = template.TbMsgTemplateLang.ToList().Where(x => x.LangCode == request.Parameter.Locale).FirstOrDefault();
                            if (templateLang != null)
                            {
                                var emailRecordData = new EmploymentServiceAPIs.Common.Dtos.MessageDtos.MessageCreateEntity
                                {
                                    EntityDisplayName = request.Data.EntityDisplayName,
                                    EntityNameTxt = request.Data.EntityNameTxt,
                                    EntityCode = request.Data.EntityCode,
                                    Link = _mailSettings.TricorPortalBaseUrl,
                                };

                                Template templateTitle = new Template(templateLang.Title, '$', '$');
                                templateTitle.Group.RegisterRenderer(typeof(string), new StringRenderer());
                                templateTitle.Add("data", emailRecordData);
                                string title = templateTitle.Render();

                                Template templateContent = new Template(templateLang.Content, '$', '$');
                                templateContent.Group.RegisterRenderer(typeof(string), new StringRenderer());
                                templateContent.Add("data", emailRecordData);
                                string content = templateContent.Render();
                                var msg = await _empEmailService.InsertEmailRecord(
                                    templateLang,
                                    username,
                                    title,
                                    content,
                                    _mailSettings.SupportEmail
                                );

                            }

                        }
                    }
                }
                return response;

            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return response;
            }
        }

        /// <summary>
        /// Update Entity
        /// </summary>
        /// <param name="request"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        public async Task<ApiResponse<TbClientEntityDto>> Update(ApiRequest<TbClientEntityManageDto> request, string username)
        {
            _logger.LogInformation($"Update - {request.Data.EntityId}.");
            var response = new ApiResponse<TbClientEntityDto>();
            response.Ray = request.Ray;
            try
            {

                TbCamEntity updateEntity = _mapper.Map<TbClientEntityManageDto, TbCamEntity>(request.Data);

                bool isValid = await validate(request.Data, response.Message.ValidateResult, false, request.Data.EntityCode, request.Data.EntityId);
                if (!isValid)
                {
                    var data = await _clientEntityRepository.GetById(request.Data.ParentClientId, request.Data.EntityId);
                    response.Data = data;

                    response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                    {
                        Level = Level.ERROR,
                        LabelCode = Label.CAM1202, // update fail
                    });
                    return response;
                }

                bool resultFlag = await _clientEntityRepository.Update(updateEntity, request.Data.ParentClientId, username);
                if (!resultFlag)
                {
                    _logger.LogWarning("Fail to update Entity");
                    var data = await _clientEntityRepository.GetById(request.Data.ParentClientId, request.Data.EntityId);
                    response.Data = data;

                    response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                    {
                        Level = Level.ERROR,
                        LabelCode = Label.CAM1202, // update fail
                    });
                    return response;
                }

                //validate support
                if (request.Data.Support != null)
                {
                    bool isSupportValid = await validate(request.Data.Support, response.Message.ValidateResult, false, "0", 0);
                    if (!isSupportValid)
                    {
                        var data = await _clientEntityRepository.GetById(request.Data.ParentClientId, request.Data.EntityId);
                        response.Data = data;

                        response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                        {
                            Level = Level.ERROR,
                            LabelCode = Label.CAM1202, // update fail
                        });
                        return response;
                    }


                    //update support
                    var orderSuppData = request.Data.Support.OrderBy(x => x.CreatedDt).ToList();

                    var suppData = _mapper.Map<List<TbCamEntitySupportUpdateDto>, List<TbCamEntitySupport>>(orderSuppData);

                    resultFlag = await _supportRepository.UpdateByEntityId(suppData, request.Data.EntityId);
                    if (!resultFlag)
                    {
                        _logger.LogWarning("Fail to update support");
                        var data = await _clientEntityRepository.GetById(request.Data.ParentClientId, request.Data.EntityId);
                        response.Data = data;

                        response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                        {
                            Level = Level.ERROR,
                            LabelCode = Label.CAM1202, // update fail
                        });
                        return response;
                    }
                }

                //validate faq
                if (request.Data.FAQ != null)
                {
                    bool isFAQValid = await validate(request.Data.FAQ, response.Message.ValidateResult, false, "0", 0);
                    if (!isFAQValid)
                    {
                        var data = await _clientEntityRepository.GetById(request.Data.ParentClientId, request.Data.EntityId);
                        response.Data = data;

                        response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                        {
                            Level = Level.ERROR,
                            LabelCode = Label.CAM1202, // update fail
                        });
                        return response;
                    }

                    //update faq
                    resultFlag = await _faqRepository.UpdateByEntityId(request.Data.FAQ, request.Data.EntityId);
                    if (!resultFlag)
                    {
                        _logger.LogWarning("Fail to update faq");
                        var data = await _clientEntityRepository.GetById(request.Data.ParentClientId, request.Data.EntityId);
                        response.Data = data;

                        response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                        {
                            Level = Level.ERROR,
                            LabelCode = Label.CAM1202, // update fail
                        });
                        return response;
                    }
                }

                var result = await _clientEntityRepository.GetById(request.Data.ParentClientId, request.Data.EntityId);
                response.Data = result;

                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.SUCCESS,
                    LabelCode = Label.CAM1201 // update successfully
                });
                return response;
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return response;
            }

        }

        /// <summary>
        /// Get Entity By ClientEntity Id
        /// </summary>
        /// <param name="ray"></param>
        /// <param name="clientId"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public async Task<ApiResponse<TbClientEntityDto>> GetById(string ray, int clientId, int entityId)
        {
            _logger.LogInformation($"GetEntity - {clientId}, {entityId}.");
            var response = new ApiResponse<TbClientEntityDto>();
            response.Ray = ray;
            try
            {
                var result = await _clientEntityRepository.GetById(clientId, entityId);
                response.Data = result;
                return response;
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return response;
            }
        }

        /// <summary>
        /// Get Entity By ClientEntity Code
        /// </summary>
        /// <param name="entityCode"></param>
        /// <returns></returns>
        public async Task<ApiResponse<TbClientEntityDto>> GetEntityByCode(string entityCode)
        {
            _logger.LogInformation($"GetEntity - {entityCode}.");
            var response = new ApiResponse<TbClientEntityDto>();
            try
            {
                var result = await _clientEntityRepository.GetByCode(entityCode);
                var data = _mapper.Map<TbCamEntity, TbClientEntityDto>(result);
                response.Data = data;
                return response;
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return response;
            }
        }

        private MessageItem msg(string componentId, string level, string labelCode, string[] placeHolder)
        {
            return new MessageItem
            {
                ComponentId = componentId,
                Level = level,
                LabelCode = labelCode,
                Placeholder = placeHolder
            };
        }

        /// <summary>
        /// Validate api object
        /// </summary>
        /// <param name="model"></param>
        /// <param name="add"></param>
        /// <param name="entityCode"></param>
        /// <param name="entityId"></param>
        /// <param name="msgItem"></param>
        /// <returns></returns>
        public async Task<bool> validate<TModel>(
            TModel model,
            List<MessageItem> msgItem,
            bool add,
            string entityCode,
            int entityId) where TModel : class
        {
            bool rv = true;
            List<MessageItem> valResult = msgItem;

            // test
            List<MessageItem> vrs = _validator.Validate(model);
            if (vrs.Count > 0)
            {
                rv = false;
                //valResult.Concat(vrs); // add to vrs list
                vrs.ForEach((item) =>
                {
                    valResult.Add(item);
                });
            }

            if (!add && entityId == 0)
            {
                return rv;
            }

            TbCamEntity entity = await _clientEntityRepository.GetByCode(entityCode);
            if (add)
            {
                if (entity != null)
                {
                    rv = false;
                    valResult.Add(msg("entityCode", "ERROR", ValMsg.VAL_DUPLICATE, new string[] { "clientEntityModule.info.entityCode" }));
                }
            }
            else if (entityId > 0)
            {
                if (entity != null && entity.EntityId != entityId) //a client found with same client code
                {
                    rv = false;
                    valResult.Add(msg("entityCode", "ERROR", ValMsg.VAL_DUPLICATE, new string[] { "clientEntityModule.info.entityCode" }));
                }
            }

            return rv;
        }

    }
}