using AutoMapper;
using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using EmploymentServiceAPIs.Common.Dtos;
using EmploymentServiceAPIs.Common.Repositories.Interfaces;
using EmploymentServiceAPIs.Common.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Common.Services.Impl
{
    public class TbCamEntityFileService : ITbCamEntityFileService
    {
        private readonly ILogger<TbCamEntityFileService> _logger;
        private readonly ITbCamEntityService _entityService;
        private readonly ITbCamEntityFileRepository _entityFileRepository;
        private readonly IMapper _mapper;

        public TbCamEntityFileService(ITbCamEntityFileRepository entityFileRepository, IMapper mapper, ILogger<TbCamEntityFileService> logger, ITbCamEntityService entityService)
        {
            _entityService = entityService;
            _entityFileRepository = entityFileRepository;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<ApiResponse<string>> UploadEntityFile(EntityFileUploadDto request)
        {
            var response = new ApiResponse<string>();
            try
            {
                _logger.LogInformation($"UploadEntityFile - file {request.File.FileName}");
                var entity = await _entityService.GetEntityByCode(request.EntityCode); // query for entity information
                TbCamEntityFile query = await _entityFileRepository.Upload(request, entity.Data);
                //EntityFileUploadDto result = new EntityFileUploadDto();
                //result.File = request.File;
                //result.FileContent = query.FileContent;
                //response.Data = result;
                response.Data = "OK";

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return response;
            }
        }
        public async Task<ApiResponse<EntityFileDownloadDto>> DownloadFileByEntityId(int entityId)
        {
            var response = new ApiResponse<EntityFileDownloadDto>();
            try
            {
                _logger.LogInformation($"DownloadFileByEntityId - entity Id {entityId}");
                var query = await _entityFileRepository.DownloadFileByEntityId(entityId);
                EntityFileDownloadDto result = new EntityFileDownloadDto();
                if (query != null && query.FileContent != null)
                {

                    var stream = new MemoryStream(query.FileContent);
                    IFormFile file = new FormFile(stream, 0, query.FileContent.Length, "File", "fileName");
                    result.File = file;
                    result.FileContent = query.FileContent;

                }
                response.Data = result;

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return response;
            }

        }

    }
}