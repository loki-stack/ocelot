using AutoMapper;
using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using CommonLib.Validation;
using EmploymentServiceAPIs.Common.Constants;
using EmploymentServiceAPIs.Common.Dtos;
using EmploymentServiceAPIs.Common.Repositories.Interfaces;
using EmploymentServiceAPIs.Common.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;


namespace EmploymentServiceAPIs.Common.Services.Impl
{
    public class TbCamClientService : ITbCamClientService
    {
        private readonly ILogger<TbCamClientService> _logger;
        private readonly ITbCamClientRepository _clientRepository;
        private readonly IValidator _validator;
        private readonly IMapper _mapper;

        public TbCamClientService(
            ITbCamClientRepository clientRepository,
            IMapper mapper,
            IHttpContextAccessor httpContextAccessor,
            ILogger<TbCamClientService> logger,
            IValidator validator
            )
        {
            _clientRepository = clientRepository;
            _mapper = mapper;
            _logger = logger;
            _validator = validator;
        }

        public async Task<ApiResponse<List<TbClientDto>>> GetAll()
        {
            var response = new ApiResponse<List<TbClientDto>>();
            _logger.LogInformation("GetAll.");
            try
            {

                var result = await _clientRepository.GetAllWithEntityCount();
                response.Data = result;
                return response;

            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return response;
            }
        }

        public async Task<ApiResponse<List<TbClientDto>>> Create(ApiRequest<TbClientCreateDto> request)
        {
            _logger.LogInformation($"Create.");
            ApiResponse<List<TbClientDto>> response = new ApiResponse<List<TbClientDto>>();

            try
            {

                bool isValid = await validate(request.Data, response.Message.ValidateResult, true, request.Data.ClientCode, 0);
                if (!isValid)
                {
                    return response;
                }

                TbCamClient newClient = _mapper.Map<TbClientCreateDto, TbCamClient>(request.Data);
                int newClientId = await _clientRepository.Create(newClient);
                if (newClientId > 0)
                {
                    _logger.LogInformation($"Success create - {newClientId}");
                    return response;
                }
                else
                {
                    _logger.LogInformation($"Fail to create - {newClientId}");
                    return response;
                }

            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return response;
            }
        }

        public async Task<ApiResponse<List<TbClientDto>>> Update(ApiRequest<TbClientUpdateDto> request)
        {
            _logger.LogInformation($"Update - {request.Data.ClientId}.");

            ApiResponse<List<TbClientDto>> response = new ApiResponse<List<TbClientDto>>();
            try
            {

                bool isValid = await validate(request.Data, response.Message.ValidateResult, false, request.Data.ClientCode, request.Data.ClientId);
                if (!isValid)
                {
                    return response;
                }

                TbCamClient updateClient = _mapper.Map<TbClientUpdateDto, TbCamClient>(request.Data);
                bool isUpdated = await _clientRepository.Update(updateClient);
                if (isUpdated)
                {
                    var allClient = await _clientRepository.GetAll();
                    var result = _mapper.Map<List<TbCamClient>, List<TbClientDto>>(allClient);
                    _logger.LogInformation($"Success Update - {request.Data.ClientId}");
                    response.Data = result;
                }
                return response;

            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return response;
            }
        }

        public async Task<ApiResponse<TbClientDto>> GetClientById(ApiRequest<int> request)
        {
            _logger.LogInformation($"GetClientById - {request.Data}.");
            var response = new ApiResponse<TbClientDto>();
            try
            {
                var client = await _clientRepository.GetById(request.Data);
                var result = _mapper.Map<TbCamClient, TbClientDto>(client);
                response.Data = result;
                return response;

            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return response;
            }
        }

        public async Task<ResponseModel> GetClients()
        {
            _logger.LogInformation("GetClientById.");
            ResponseModel response = new ResponseModel();
            response.Ray = "Get Clients";
            response.Data = new Dictionary<string, object>();
            response.WebMessage = new WebMessage();
            var result = await _clientRepository.GetClients();
            response.Data.Add("clients", result);
            response.Code = Convert.ToString((int)StatusCode.SUCCESS);
            return response;
        }

        public async Task<ClientResult> GetClientEntityId(int clientId, int entityId)
        {
            return await _clientRepository.GetClientEntityId(clientId, entityId);
        }
        private MessageItem msg(string componentId, string level, string labelCode, string[] placeHolder)
        {
            return new MessageItem
            {
                ComponentId = componentId,
                Level = level,
                LabelCode = labelCode,
                Placeholder = placeHolder
            };
        }
        public async Task<bool> validate<TModel>(TModel data, List<MessageItem> msgItem, bool add, string clientCode, int clientId) where TModel : class
        {
            bool rv = true;
            List<MessageItem> valResult = msgItem;

            // test
            List<MessageItem> vrs = _validator.Validate(data);
            if (vrs.Count > 0)
            {
                rv = false;
                //valResult.Concat(vrs); // add to vrs list
                vrs.ForEach((item) =>
                {
                    valResult.Add(item);
                });
            }

            TbCamClient client = await _clientRepository.GetClientByCode(clientCode);

            // check duplicate client code
            if (add)
            {
                if (client != null)
                {
                    rv = false;
                    valResult.Add(msg("clientCode", "ERROR", ValMsg.VAL_DUPLICATE, new string[] { "clientModule.Profile Code" }));
                }
            }
            else
            {
                if (client != null && client.ClientId != clientId) //a client found with same client code
                {
                    rv = false;
                    valResult.Add(msg("clientCode", "ERROR", ValMsg.VAL_DUPLICATE, new string[] { "clientModule.Profile Code" }));
                }
            }

            return rv;
        }
    }
}