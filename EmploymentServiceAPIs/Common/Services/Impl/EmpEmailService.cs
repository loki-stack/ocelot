using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using CommonLib.Services;
using EmploymentServiceAPIs.Common.Constants;
using EmploymentServiceAPIs.Common.Dtos;
using EmploymentServiceAPIs.Common.Repositories.Interfaces;
using EmploymentServiceAPIs.Common.Services.Interfaces;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Common.Services.Impl
{
    /// <summary>
    /// Impl
    /// </summary>
    public class EmpEmailService : IEmpEmailService
    {

        private readonly ILogger<EmpEmailService> _logger;
        private readonly IMessageService _messageService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="messageService"></param>
        /// <returns></returns>
        public EmpEmailService(
            ILogger<EmpEmailService> logger,
            IMessageService messageService
            )
        {
            _logger = logger;
            _messageService = messageService;
        }

        /// <summary>
        /// Insert New Message
        /// </summary>
        /// <param name="templateLang"></param>
        /// <param name="username"></param>
        /// <param name="title"></param>
        /// <param name="content"></param>
        /// <param name="recipient"></param>
        /// <returns></returns>
        public async Task<bool> InsertEmailRecord(
            TbMsgTemplateLang templateLang,
            string username,
            string title,
            string content,
            string recipient
            )
        {
            _logger.LogInformation($"InsertEmailRecord");
            try
            {
                MessageCreateDto msg = new MessageCreateDto
                {
                    TemplateLangId = templateLang.TemplateLangId,
                    Recipient = recipient,
                    Status = CommonLib.Constants.Messages.MessageStatus.NEW,
                };
                msg.MessageTitle = title;
                msg.MessageContent = content;


                var entityMsg = await _messageService.Create(msg, username);
                _logger.LogInformation($"InsertEmailRecord - success");
                return true;
            }

            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return false;
            }

        }

    }
}