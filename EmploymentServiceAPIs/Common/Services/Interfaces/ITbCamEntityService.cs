using CommonLib.Dtos;
using EmploymentServiceAPIs.Common.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Common.Services.Interfaces
{
    public interface ITbCamEntityService
    {
        /// <summary>
        /// Get All Client Entity
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<ApiResponse<List<TbClientEntityDto>>> GetAll(ApiRequest<string> request);


        /// <summary>
        /// Quick Create New Client Entity
        /// </summary>
        /// <param name="request"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        Task<ApiResponse<int>> Create(ApiRequest<TbClientEntityCreateDto> request, string username);


        /// <summary>
        /// Update Client Entity (Manage Mode)
        /// </summary>
        /// <param name="request"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        Task<ApiResponse<TbClientEntityDto>> Update(ApiRequest<TbClientEntityManageDto> request, string username);

        /// <summary>
        /// Get Client Entity By Id
        /// </summary>
        /// <param name="ray"></param>
        /// <param name="clientId"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        Task<ApiResponse<TbClientEntityDto>> GetById(string ray, int clientId, int entityId);

        // Task<ApiResponse<string>> UploadEntityFile(EntityFileUploadDto request);


        /// <summary>
        /// Get Client Entity By ClientEntity Code
        /// </summary>
        /// <param name="entityCode"></param>
        /// <returns></returns>
        Task<ApiResponse<TbClientEntityDto>> GetEntityByCode(string entityCode);

    }
}