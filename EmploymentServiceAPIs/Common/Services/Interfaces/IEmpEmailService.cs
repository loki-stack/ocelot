using CommonLib.Models;
using CommonLib.Models.Core;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Common.Services.Interfaces
{
    /// <summary>
    /// Impl
    /// </summary>
    public interface IEmpEmailService
    {
        /// <summary>
        /// Insert New Message
        /// </summary>
        /// <param name="templateLang"></param>
        /// <param name="username"></param>
        /// <param name="title"></param>
        /// <param name="content"></param>
        /// <param name="recipient"></param>
        /// <returns></returns>
        Task<bool> InsertEmailRecord(TbMsgTemplateLang templateLang, string username, string title, string content, string recipient);
    }
}