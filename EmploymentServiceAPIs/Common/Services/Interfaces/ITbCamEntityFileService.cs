using CommonLib.Dtos;
using EmploymentServiceAPIs.Common.Dtos;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Common.Services.Interfaces
{
    public interface ITbCamEntityFileService
    {
        Task<ApiResponse<string>> UploadEntityFile(EntityFileUploadDto request);
        Task<ApiResponse<EntityFileDownloadDto>> DownloadFileByEntityId(int entityId);

    }
}