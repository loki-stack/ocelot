using CommonLib.Dtos;
using EmploymentServiceAPIs.Common.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Common.Services.Interfaces
{
    public interface ITbCamClientService
    {
        Task<ApiResponse<List<TbClientDto>>> GetAll();
        Task<ResponseModel> GetClients();
        Task<ApiResponse<List<TbClientDto>>> Create(ApiRequest<TbClientCreateDto> request);
        Task<ApiResponse<List<TbClientDto>>> Update(ApiRequest<TbClientUpdateDto> request);
        Task<ApiResponse<TbClientDto>> GetClientById(ApiRequest<int> request);

        Task<ClientResult> GetClientEntityId(int clientId, int entityId);

        // Task<ApiResponse<int>> GetActiveEmployeeCount(int clientId, int entityId);

    }
}