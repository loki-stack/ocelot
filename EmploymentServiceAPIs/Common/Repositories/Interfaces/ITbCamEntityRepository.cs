using CommonLib.Models;
using CommonLib.Models.Core;
using EmploymentServiceAPIs.Common.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Common.Repositories.Interfaces
{
    public interface ITbCamEntityRepository
    {
        Task<List<TbClientEntityDto>> GetAll(int clientId);
        Task<List<TbCamEntity>> GetAll();
        Task<TbCamEntity> GetByCode(string code);
        Task<TbClientEntityDto> GetById(int clientId, int entityId);
        Task<int> Create(TbCamEntity clientCompany, int clientId);
        Task<bool> Update(TbCamEntity clientCompany, int clientId, string username);

    }
}