using EmploymentServiceAPIs.Common.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Common.Repositories.Interfaces
{
    /// <summary>
    /// Interface
    /// </summary>
    public interface ITbCamEntityFaqRepository
    {
        /// <summary>
        /// Update Faq
        /// </summary>
        /// <param name="FAQ"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        Task<bool> UpdateByEntityId(List<TbCamEntityFaqUpdateDto> FAQ, int entityId);

        /// <summary>
        /// Update Faq
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        Task<List<TbCamEntityFaqDto>> GetByEntityId(int entityId);

        /// <summary>
        /// Check if contain Faq
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns>bool</returns>
        Task<bool> CheckFAQ(int entityId);
    }
}