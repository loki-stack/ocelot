using CommonLib.Models;
using CommonLib.Models.Core;
using EmploymentServiceAPIs.Common.Dtos;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Common.Repositories.Interfaces
{
    public interface ITbCamEntityFileRepository
    {
        Task<TbCamEntityFile> Upload(EntityFileUploadDto File, TbClientEntityDto entity);
        
        Task<TbCamEntityFile> DownloadFileByEntityId(int entityId);

    }
}