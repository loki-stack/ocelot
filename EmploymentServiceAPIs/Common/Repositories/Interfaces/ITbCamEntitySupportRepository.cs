using CommonLib.Models;
using CommonLib.Models.Core;
using EmploymentServiceAPIs.Common.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Common.Repositories.Interfaces
{
    /// <summary>
    /// Interface
    /// </summary>
    public interface ITbCamEntitySupportRepository
    {
        /// <summary>
        /// Update Support
        /// </summary>
        /// <param name="supports"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        Task<bool> UpdateByEntityId(List<TbCamEntitySupport> supports, int entityId);
        /// <summary>
        /// Get Support
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        Task<List<TbCamEntitySupportDto>> GetByEntityId(int entityId);

        /// <summary>
        /// Check if contain Support and FAQ
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        Task<bool> CheckSupport(int entityId);
    }
}