using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using EmploymentServiceAPIs.Common.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Common.Repositories.Interfaces
{
    public interface ITbCamClientRepository
    {
        Task<List<TbCamClient>> GetAll();

        Task<int> Create(TbCamClient client);

        Task<bool> Update(TbCamClient client);

        Task<List<ClientViewModel>> GetClients();
        Task<TbCamClient> GetById(int id);
        Task<TbCamClient> GetClientByCode(string code);

        Task<List<TbClientDto>> GetAllWithEntityCount();
        Task<ClientResult> GetClientEntityId(int clientId, int entityId);
    }
}