﻿using CommonLib.Models.Client;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Common.Repositories.Interfaces
{
    public interface ITbCfgI18NRepository
    {
        Task<bool> Create(TbCfgI18n i18n);

        Task<bool> CreateMany(List<TbCfgI18n> listI18n);

        Task<bool> Update(TbCfgI18n i18n);

        Task<bool> UpdateMany(List<TbCfgI18n> listI18n);

        Task<List<TbCfgI18n>> GetMulti(Expression<Func<TbCfgI18n, bool>> predicate, string[] includes = null);
    }
}