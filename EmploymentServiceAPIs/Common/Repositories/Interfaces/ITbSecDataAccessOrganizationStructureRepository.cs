using EmploymentServiceAPIs.ManageEntity.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace EmploymentServiceAPIs.Common.Repositories.Interfaces
{
    public interface ITbSecDataAccessOrganizationStructureRepository
    {
        Task<List<DataAccessOrganizationStructureDto>> GetById(int dataAccessGroupId);
        Task<bool> IsAllCheck(int dataAccessGroupId);
    }
}