using AutoMapper;
using CommonLib.Models;
using CommonLib.Models.Core;
using EmploymentServiceAPIs.Common.Dtos;
using EmploymentServiceAPIs.Common.Repositories.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Common.Repositories.Impl
{
    public class TbCamEntityRepository : ITbCamEntityRepository
    {
        private readonly ILogger<TbCamEntityRepository> _logger;
        private readonly IMapper _mapper;
        private readonly CoreDbContext _coreDBContext;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public TbCamEntityRepository(
            CoreDbContext coreDBContext,
            IMapper mapper,
            IHttpContextAccessor httpContextAccessor,
            ILogger<TbCamEntityRepository> logger)
        {
            _coreDBContext = coreDBContext;
            _httpContextAccessor = httpContextAccessor;
            _logger = logger;
            _mapper = mapper;
        }
        private string GetUserName()
        {
            try
            {
                return _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.Name);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return "";
            }
        }
        public async Task<List<TbCamEntity>> GetAll()
        {
            _logger.LogInformation("GetAll");
            try
            {
                List<TbCamEntity> clientEntity = await _coreDBContext.TbCamEntity
                                                        .Include(c => c.ParentClient)
                                                        .ToListAsync();
                return clientEntity;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }
        public async Task<List<TbClientEntityDto>> GetAll(int clientId)
        {
            _logger.LogInformation($"GetAll By clientId - {clientId}.");
            try
            {
                List<TbCamEntity> clientEntity = await _coreDBContext.TbCamEntity
                                                        .Include(c => c.TbCamEntityFaq)
                                                        .Include(c => c.TbCamEntitySupport)
                                                        .Include(c => c.ParentClient)
                                                        .Where(x => x.ParentClient.ClientId == clientId)
                                                        .ToListAsync();
                var result = new List<TbClientEntityDto>();
                foreach (var entity in clientEntity)
                {
                    var res = new TbClientEntityDto();
                    res = _mapper.Map<TbCamEntity, TbClientEntityDto>(entity);
                    res.FAQ = _mapper.Map<List<TbCamEntityFaq>, List<TbCamEntityFaqDto>>(entity.TbCamEntityFaq.ToList());
                    res.Support = _mapper.Map<List<TbCamEntitySupport>, List<TbCamEntitySupportDto>>(entity.TbCamEntitySupport.ToList());
                    result.Add(res);
                }
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<TbCamEntity> GetByCode(string code)
        {
            _logger.LogInformation($"GetByCode - EntityCode - {code}.");
            try
            {
                TbCamEntity clientEntity = await _coreDBContext.TbCamEntity
                                                        .Include(c => c.ParentClient)
                                                        .FirstOrDefaultAsync(x => x.EntityCode == code);
                return clientEntity;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }


        public async Task<TbClientEntityDto> GetById(int clientId, int entityId)
        {
            _logger.LogInformation($"GetById - clientId - {clientId} & entityId - {entityId}.");
            try
            {
                TbCamEntity clientEntity = await _coreDBContext.TbCamEntity
                                                        .Include(c => c.TbCamEntityFaq)
                                                        .Include(c => c.TbCamEntitySupport)
                                                        .Include(c => c.ParentClient)
                                                        .FirstOrDefaultAsync(x => x.EntityId == entityId && x.ParentClientId == clientId);
                var res = new TbClientEntityDto();

                if (clientEntity != null)
                {

                    res = _mapper.Map<TbCamEntity, TbClientEntityDto>(clientEntity);
                    if (clientEntity.TbCamEntityFaq.Count > 0)
                    {
                        res.FAQ = _mapper.Map<List<TbCamEntityFaq>, List<TbCamEntityFaqDto>>(clientEntity.TbCamEntityFaq.ToList());
                    }
                    else
                    {
                        res.FAQ = new List<TbCamEntityFaqDto>();
                    }

                    if (clientEntity.TbCamEntitySupport.Count > 0)
                    {
                        res.Support = _mapper.Map<List<TbCamEntitySupport>, List<TbCamEntitySupportDto>>(clientEntity.TbCamEntitySupport.ToList());
                    }
                    else
                    {
                        res.Support = new List<TbCamEntitySupportDto>();
                    }
                }
                return res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<int> Create(TbCamEntity clientEntity, int clientId)
        {
            _logger.LogInformation($"Create - EntityCode- {clientEntity.EntityCode}");

            using (var transaction = await _coreDBContext.Database.BeginTransactionAsync())
            {
                try
                {


                    clientEntity.CreatedBy = GetUserName();
                    clientEntity.CreatedDt = DateTimeOffset.Now;
                    clientEntity.StatusCd = "AC";
                    if (clientEntity.ModifiedBy == null)
                    {
                        clientEntity.ModifiedBy = string.Empty;
                    }
                    _logger.LogInformation($"Find Client by clientId - {clientId}.");
                    clientEntity.ParentClient = await _coreDBContext.TbCamClient.FirstOrDefaultAsync(x => x.ClientId == clientId);
                    clientEntity.ParentClientId = clientEntity.ParentClient.ClientId;


                    //initial value
                    clientEntity.ConnStringParam = string.Empty;
                    clientEntity.DbHost = string.Empty;
                    clientEntity.DbName = string.Empty;
                    //

                    await _coreDBContext.TbCamEntity.AddAsync(clientEntity);
                    await _coreDBContext.SaveChangesAsync();

                    // populate level
                    HierarchyId level = HierarchyId.Parse($"/{ clientEntity.ParentClientId}/{clientEntity.EntityId}/");
                    clientEntity.EntityLevel = level;
                    _coreDBContext.Entry(clientEntity).State = EntityState.Modified;
                    await _coreDBContext.SaveChangesAsync();
                    await transaction.CommitAsync();

                    return clientEntity.EntityId;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                    await transaction.RollbackAsync();
                    return 0;
                }
            }
        }

        private string checkIsNullOrEmpty(string str)
        {
            return string.IsNullOrEmpty(str) ? "" : str;
        }

        public async Task<bool> Update(TbCamEntity clientEntity, int clientId, string username)
        {
            _logger.LogInformation($"Update - EntityId - {clientEntity.EntityId}");
            using (var transaction = await _coreDBContext.Database.BeginTransactionAsync())
            {

                try
                {
                    var entity = _coreDBContext.TbCamEntity.Find(clientEntity.EntityId);
                    _logger.LogInformation($"Update - find entity id - {clientEntity.EntityId}.");
                    if (entity != null)
                    {
                        entity.ModifiedBy = GetUserName();
                        entity.ModifiedDt = DateTimeOffset.Now;
                        entity.EntityCode = clientEntity.EntityCode;
                        entity.EntityNameTxt = clientEntity.EntityNameTxt;
                        entity.EntityDisplayName = clientEntity.EntityDisplayName;
                        entity.CountryCd = clientEntity.CountryCd;
                        entity.Remarks = clientEntity.Remarks;
                        _logger.LogInformation($"Update - clientId - {clientId}");
                        entity.ParentClient = await _coreDBContext.TbCamClient.FirstOrDefaultAsync(x => x.ClientId == clientId);
                        entity.DbName = clientEntity.DbName;
                        entity.DbHost = clientEntity.DbHost;
                        entity.DbPort = clientEntity.DbPort;
                        entity.ConnStringParam = clientEntity.ConnStringParam;
                        entity.StatusCd = clientEntity.StatusCd;

                        entity.BusinessAddressLine1 = clientEntity.BusinessAddressLine1;
                        entity.BusinessAddressLine2 = checkIsNullOrEmpty(clientEntity.BusinessAddressLine2);
                        entity.BusinessAddressLine3 = checkIsNullOrEmpty(clientEntity.BusinessAddressLine3);;
                        entity.BusinessPhone = clientEntity.BusinessPhone;
                        entity.BusinessFax = clientEntity.BusinessFax;
                        entity.ContactAddressLine1 = clientEntity.ContactAddressLine1;
                        entity.ContactAddressLine2 = checkIsNullOrEmpty(clientEntity.ContactAddressLine2);
                        entity.ContactAddressLine3 = checkIsNullOrEmpty(clientEntity.ContactAddressLine3);

                        entity.ContactPhone = checkIsNullOrEmpty(clientEntity.ContactPhone);
                        entity.ContactFax = checkIsNullOrEmpty(clientEntity.ContactFax);
                        entity.ContactName = checkIsNullOrEmpty(clientEntity.ContactName);
                        entity.ContactEmail = checkIsNullOrEmpty(clientEntity.ContactEmail);
                        entity.InternalCode = clientEntity.InternalCode;
                        entity.TaxId = checkIsNullOrEmpty(clientEntity.TaxId);

                        entity.BusinessPhoneAreaCd = checkIsNullOrEmpty(clientEntity.BusinessPhoneAreaCd);
                        entity.BusinessPhoneFaxAreaCd = checkIsNullOrEmpty(clientEntity.BusinessPhoneFaxAreaCd);
                        entity.ContactEmail = checkIsNullOrEmpty(clientEntity.ContactEmail);
                        entity.ContactPhoneAreaCd = checkIsNullOrEmpty(clientEntity.ContactPhoneAreaCd);
                        entity.ContactFaxAreaCd = checkIsNullOrEmpty(clientEntity.ContactFaxAreaCd);

                        //secondary
                        entity.BusinessAddressLine1Secondary = clientEntity.BusinessAddressLine1Secondary;
                        entity.BusinessAddressLine2Secondary = clientEntity.BusinessAddressLine2Secondary;
                        entity.BusinessAddressLine3Secondary = clientEntity.BusinessAddressLine3Secondary;
                        entity.ContactAddressLine1Secondary = clientEntity.ContactAddressLine1Secondary;
                        entity.ContactAddressLine2Secondary = clientEntity.ContactAddressLine2Secondary;
                        entity.ContactAddressLine3Secondary = clientEntity.ContactAddressLine3Secondary;
                        entity.EntityDisplayNameSecondary = clientEntity.EntityDisplayNameSecondary;
                        entity.EntityNameTxtSecondary = clientEntity.EntityNameTxtSecondary;

                        _coreDBContext.Entry(entity).State = EntityState.Modified;
                        await _coreDBContext.SaveChangesAsync();
                        await transaction.CommitAsync();
                        return true;
                    }
                    else
                    {
                        _logger.LogWarning("Manage exception - no such entity in database.");
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                    await transaction.RollbackAsync();
                    return false;
                }
            }
        }
    }
}