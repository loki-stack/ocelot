using AutoMapper;
using CommonLib.Models;
using CommonLib.Models.Core;
using EmploymentServiceAPIs.Common.Repositories.Interfaces;
using EmploymentServiceAPIs.ManageEntity.Dtos;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Common.Repositories.Impl
{
    /// <summary>
    /// 
    /// </summary>
    public class TbSecDataAccessOrganizationStructureRepository : ITbSecDataAccessOrganizationStructureRepository
    {
        private readonly ILogger<TbSecDataAccessOrganizationStructureRepository> _logger;
        private readonly CoreDbContext _coreDBContext;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMapper _mapper;

        /// <summary>
        /// TbSecDataAccessOrganizationStructureRepository Constructor
        /// </summary>
        /// <param name="mapper">mapper</param>
        /// <param name="coreDBContext">catalogDBContext</param>
        /// <param name="httpContextAccessor">httpContextAccessor</param>
        /// <returns></returns>
        public TbSecDataAccessOrganizationStructureRepository(IMapper mapper, CoreDbContext coreDBContext, IHttpContextAccessor httpContextAccessor, ILogger<TbSecDataAccessOrganizationStructureRepository> logger)
        {
            _coreDBContext = coreDBContext;
            _httpContextAccessor = httpContextAccessor;
            _logger = logger;
            _mapper = mapper;
        }
        /// <summary>
        /// Get DataAccessOrganizationStructure's relationship by dataAccessId
        /// </summary>
        /// <param name="dataAccessGroupId">dataAccessId</param>
        /// <returns>DataAccessOrganizationStructureDto</returns>
        public async Task<List<DataAccessOrganizationStructureDto>> GetById(int dataAccessGroupId)
        {
            _logger.LogInformation($"GetById - dataAccessGroupId- {dataAccessGroupId}");
            try
            {
                var result = await (from g in _coreDBContext.TbSecDataAccessOrganizationStructure
                                    where g.DataAccessGroupId == dataAccessGroupId
                                    select g).ToListAsync();

                return _mapper.Map<List<TbSecDataAccessOrganizationStructure>, List<DataAccessOrganizationStructureDto>>(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Return boolean result whether Data Access Organization Structure's node level is at root level
        /// </summary>
        /// <param name="dataAccessGroupId">dataAccessGroupId</param>
        /// <returns>bool</returns>
        public async Task<bool> IsAllCheck(int dataAccessGroupId)
        {
            _logger.LogInformation($"IsAllCheck - dataAccessGroupId- {dataAccessGroupId}");
            try
            {
                bool result = await _coreDBContext.TbSecDataAccessOrganizationStructure.AnyAsync(x => x.DataAccessGroupId == dataAccessGroupId && x.NodeLevel.GetLevel() == 0);

                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return false;
            }
        }

    }
}