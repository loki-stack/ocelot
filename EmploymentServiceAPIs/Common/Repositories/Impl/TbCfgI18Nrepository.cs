﻿using CommonLib.Models.Client;
using CommonLib.Services;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.Common.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Common.Repositories.Impl
{
    public class TbCfgI18Nrepository : ITbCfgI18NRepository
    {
        protected readonly IConnectionStringContainer _connectionStringRepository;
        protected string connectionString;

        public TbCfgI18Nrepository(IConnectionStringContainer connectionStringRepository)
        {
            _connectionStringRepository = connectionStringRepository;
            connectionString = _connectionStringRepository.GetConnectionString();
        }

        protected TenantDBContext CreateContext(string _connectionString)
        {
            return new TenantDBContext(_connectionString);
        }

        public Task<bool> Create(TbCfgI18n i18n)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> CreateMany(List<TbCfgI18n> listI18n)
        {
            using (var _context = CreateContext(connectionString))
            {
                await _context.AddRangeAsync(listI18n);
                await _context.SaveChangesAsync();
                return true;
            }
        }

        public Task<bool> Update(TbCfgI18n i18n)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> UpdateMany(List<TbCfgI18n> listI18n)
        {
            using (var _context = CreateContext(connectionString))
            {
                List<TbCfgI18n> listUpdate = new List<TbCfgI18n>();
                List<TbCfgI18n> listCreate = new List<TbCfgI18n>();
                foreach (var item in listI18n)
                {
                    TbCfgI18n model = _context.TbCfgI18n.Where(x => x.LanguageTag == item.LanguageTag && x.TextKey == item.TextKey).FirstOrDefault();
                    if (model != null)
                    {
                        model.TextValue = item.TextValue;
                        model.ModifyBy = item.ModifyBy;
                        model.ModifyDt = item.ModifyDt;
                        _context.Entry(model).State = EntityState.Modified;
                        listUpdate.Add(model);
                    }
                    else
                    {
                        listCreate.Add(new TbCfgI18n
                        {
                            LanguageTag = item.LanguageTag,
                            TextKey = item.TextKey,
                            TextValue = item.TextValue,
                            DefaultFlag = false,
                            CreateBy = item.ModifyBy,
                            CreateDt = DateTime.UtcNow
                        });
                    }
                }
                if (listUpdate.Count > 0)
                    _context.UpdateRange(listUpdate);
                if (listCreate.Count > 0)
                    await _context.AddRangeAsync(listCreate);
                await _context.SaveChangesAsync();
            }
            return true;
        }

        public async Task<List<TbCfgI18n>> GetMulti(Expression<Func<TbCfgI18n, bool>> predicate, string[] includes = null)
        {
            using (var _context = CreateContext(connectionString))
            {
                if (includes != null && includes.Count() > 0)
                {
                    var query = _context.TbCfgI18n.Include(includes.First());
                    foreach (var include in includes.Skip(1))
                        query = query.Include(include);
                    return await query.Where<TbCfgI18n>(predicate).ToListAsync();
                }

                return await _context.TbCfgI18n.Where<TbCfgI18n>(predicate).ToListAsync();
            }
        }
    }
}