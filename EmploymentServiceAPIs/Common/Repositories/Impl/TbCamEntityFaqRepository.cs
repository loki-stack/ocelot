using AutoMapper;
using CommonLib.Models;
using CommonLib.Models.Core;
using EmploymentServiceAPIs.Common.Dtos;
using EmploymentServiceAPIs.Common.Repositories.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Common.Repositories.Impl
{
    /// <summary>
    /// Impl
    /// </summary>
    public class TbCamEntityFaqRepository : ITbCamEntityFaqRepository
    {
        private readonly ILogger<TbCamEntityFaqRepository> _logger;
        private readonly IMapper _mapper;
        private readonly CoreDbContext _coreDBContext;
        private readonly IHttpContextAccessor _httpContextAccessor;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="coreDBContext"></param>
        /// <param name="mapper"></param>
        /// <param name="httpContextAccessor"></param>
        /// <param name="logger"></param>
        public TbCamEntityFaqRepository(
            CoreDbContext coreDBContext,
            IMapper mapper,
            IHttpContextAccessor httpContextAccessor,
            ILogger<TbCamEntityFaqRepository> logger)
        {
            _coreDBContext = coreDBContext;
            _httpContextAccessor = httpContextAccessor;
            _logger = logger;
            _mapper = mapper;
        }
        private string GetUserName()
        {
            try
            {
                return _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.Name);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return "";
            }
        }

        /// <summary>
        /// Update Faq
        /// </summary>
        /// <param name="FAQ"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public async Task<bool> UpdateByEntityId(List<TbCamEntityFaqUpdateDto> FAQ, int entityId)
        {
            _logger.LogInformation($"Update faqs - entityId -{entityId}");
            using (var transaction = _coreDBContext.Database.BeginTransaction())
            {
                try
                {
                    var newFaqsData = new List<TbCamEntityFaq>();
                    var faqsData = new List<TbCamEntityFaq>();
                    if (FAQ.Count > 0)
                    {
                        foreach (var faq in FAQ)
                        {
                            if (faq.EntityId == 0)
                            {
                                TbCamEntityFaq model = new TbCamEntityFaq();
                                model.CreatedBy = GetUserName();
                                model.CreatedDt = DateTimeOffset.Now;
                                model.EntityId = entityId;
                                model.Answer = faq.Answer;
                                model.Question = faq.Question;
                                model.FaqOrder = faq.FaqOrder;
                                newFaqsData.Add(model);
                            }
                            else
                            {
                                TbCamEntityFaq model = await _coreDBContext.TbCamEntityFaq.Where(x => x.FaqId == faq.FaqId).FirstOrDefaultAsync();
                                faqsData.Add(model);
                                model.ModifiedBy = GetUserName();
                                model.ModifiedDt = DateTimeOffset.Now;
                                model.Answer = faq.Answer;
                                model.Question = faq.Question;
                                model.FaqOrder = faq.FaqOrder;
                                _coreDBContext.TbCamEntityFaq.UpdateRange(model);
                            }
                        }
                        await _coreDBContext.TbCamEntityFaq.AddRangeAsync(newFaqsData);
                    }

                    List<TbCamEntityFaq> faqModel = await _coreDBContext.TbCamEntityFaq.Where(x => x.EntityId == entityId).ToListAsync();
                    if (faqModel.Except(faqsData).Any())
                    {
                        var diff = faqModel.Except(faqsData);
                        _coreDBContext.TbCamEntityFaq.RemoveRange(diff);
                    }
                    await _coreDBContext.SaveChangesAsync();
                    await transaction.CommitAsync();
                    return true;

                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                    return false;
                }
            }
        }

        /// <summary>
        /// Get FAQ
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public async Task<List<TbCamEntityFaqDto>> GetByEntityId(int entityId)
        {
            _logger.LogInformation($"GetByEntityId - entityId -{entityId}.");
            try
            {
                List<TbCamEntityFaq> faqs = await _coreDBContext.TbCamEntityFaq.Where(x => x.EntityId == entityId).ToListAsync();
                var res = _mapper.Map<List<TbCamEntityFaq>, List<TbCamEntityFaqDto>>(faqs);
                return res;


            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Check if contain FAQ
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns>bool</returns>
        public async Task<bool> CheckFAQ(int entityId)
        {
            _logger.LogInformation($"CheckFAQ - entityId- {entityId}.");
            try
            {
                bool result = await _coreDBContext.TbCamEntityFaq.AnyAsync(x => x.EntityId == entityId);
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return false;
            }
        }
    }
}