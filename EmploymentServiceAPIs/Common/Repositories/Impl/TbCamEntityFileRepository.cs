using AutoMapper;
using CommonLib.Models;
using CommonLib.Models.Core;
using EmploymentServiceAPIs.Common.Dtos;
using EmploymentServiceAPIs.Common.Repositories.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Common.Repositories.Impl
{
    public class TbCamEntityFileRepository : ITbCamEntityFileRepository
    {
        private readonly ILogger<TbCamEntityFileRepository> _logger;
        private readonly CoreDbContext _coreDBContext;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMapper _mapper;

        public TbCamEntityFileRepository(CoreDbContext coreDBContext, IMapper mapper, IHttpContextAccessor httpContextAccessor, ILogger<TbCamEntityFileRepository> logger)
        {
            _coreDBContext = coreDBContext;
            _httpContextAccessor = httpContextAccessor;
            _logger = logger;
            _mapper = mapper;
        }
        private string GetUserName()
        {
            try
            {
                return _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.Name);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return "";
            }
        }

        public async Task<TbCamEntityFile> Upload(EntityFileUploadDto File, TbClientEntityDto Entity)
        {
            TbCamEntityFile newFile = new TbCamEntityFile();
            newFile.ModifiedBy = GetUserName();
            newFile.ModifiedDt = DateTimeOffset.Now;
            newFile.CreatedBy = GetUserName();
            newFile.CreatedDt = DateTimeOffset.Now;
            newFile.IsDeleted = false;
            newFile.FileName = File.File.FileName;
            newFile.FileCategory = File.FileCategory;
            newFile.EntityId = Entity.EntityId;

            _logger.LogInformation($"Upload - EntityCode - {File.EntityCode}");
            TbCamEntityFile retrieveFile = await _coreDBContext.TbCamEntityFile.FirstOrDefaultAsync(x => x.EntityId == Entity.EntityId);

            using (var transaction = _coreDBContext.Database.BeginTransaction())
            {
                try
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        await File.File.CopyToAsync(memoryStream);
                        _logger.LogInformation("CopyToAsync");
                        if (memoryStream.Length < 2097152)
                        {

                            if (retrieveFile == null)
                            {
                                _logger.LogInformation("new logo");
                                newFile.FileRemark = "new";
                                _logger.LogInformation("Start Creating File in repo.");
                                newFile.FileContent = memoryStream.ToArray();

                                _coreDBContext.TbCamEntityFile.Add(newFile);

                                await _coreDBContext.SaveChangesAsync();
                                await transaction.CommitAsync();

                                return newFile;

                            }
                            else
                            {
                                _logger.LogInformation("update logo");
                                retrieveFile.FileCategory = File.FileCategory;
                                retrieveFile.FileRemark = "update";
                                retrieveFile.FileContent = memoryStream.ToArray();
                                _logger.LogInformation("Start Updating File in repo.");
                                //_catalogDBContext.TbCamEntityFile.Update(retrieveFile);
                                _coreDBContext.Entry(retrieveFile).State = EntityState.Modified;
                                await _coreDBContext.SaveChangesAsync();
                                await transaction.CommitAsync();

                                return retrieveFile;
                            }

                        }

                        return retrieveFile;
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                    return null;
                }

            }
        }

        public async Task<TbCamEntityFile> DownloadFileByEntityId(int entityId)
        {

            _logger.LogInformation($"GetFileId - {entityId}");
            try
            {
                TbCamEntityFile retrieveFile = await _coreDBContext.TbCamEntityFile
                                                        .Include(c => c.Entity)
                                                        .FirstOrDefaultAsync(x => x.EntityId == entityId);
                return retrieveFile;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }
    }
}