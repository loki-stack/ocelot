using AutoMapper;
using CommonLib.Models;
using CommonLib.Models.Core;
using EmploymentServiceAPIs.Common.Dtos;
using EmploymentServiceAPIs.Common.Repositories.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Common.Repositories.Impl
{
    /// <summary>
    /// Impl
    /// </summary>
    public class TbCamEntitySupportRepository : ITbCamEntitySupportRepository
    {
        private readonly ILogger<TbCamEntitySupportRepository> _logger;
        private readonly IMapper _mapper;
        private readonly CoreDbContext _coreDBContext;
        private readonly IHttpContextAccessor _httpContextAccessor;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="coreDBContext"></param>
        /// <param name="mapper"></param>
        /// <param name="httpContextAccessor"></param>
        /// <param name="logger"></param>
        public TbCamEntitySupportRepository(
            CoreDbContext coreDBContext,
            IMapper mapper,
            IHttpContextAccessor httpContextAccessor,
            ILogger<TbCamEntitySupportRepository> logger)
        {
            _coreDBContext = coreDBContext;
            _httpContextAccessor = httpContextAccessor;
            _logger = logger;
            _mapper = mapper;
        }
        private string GetUserName()
        {
            try
            {
                _logger.LogInformation("GetUserName");
                return _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.Name);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return "";
            }
        }

        /// <summary>
        /// Update Support
        /// </summary>
        /// <param name="supports"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public async Task<bool> UpdateByEntityId(List<TbCamEntitySupport> supports, int entityId)
        {
            using (var transaction = _coreDBContext.Database.BeginTransaction())
            {
                try
                {
                    var newSuppdata = new List<TbCamEntitySupport>();
                    var suppData = new List<TbCamEntitySupport>();
                    if (supports.Count > 0)
                    {
                        foreach (var support in supports)
                        {
                            _logger.LogInformation($"Update support --- SupportId: {support.SupportId}");
                            if (support.EntityId == 0)
                            {
                                TbCamEntitySupport model = new TbCamEntitySupport();
                                model.CreatedBy = GetUserName();
                                model.CreatedDt = support.CreatedDt;
                                // model.Remark = support.Remark;
                                model.EntityId = entityId;
                                model.SupportContactName = support.SupportContactName;
                                model.SupportEmail = support.SupportEmail;
                                model.SupportPhoneNumber = support.SupportPhoneNumber;
                                newSuppdata.Add(model);
                            }
                            else
                            {
                                TbCamEntitySupport model = await _coreDBContext.TbCamEntitySupport.Where(x => x.SupportId == support.SupportId).FirstOrDefaultAsync();
                                suppData.Add(model);
                                model.ModifiedBy = GetUserName();
                                model.ModifiedDt = DateTimeOffset.Now;
                                // model.Remark = support.Remark;
                                model.SupportContactName = support.SupportContactName;
                                model.SupportEmail = support.SupportEmail;
                                model.SupportPhoneNumber = support.SupportPhoneNumber;
                                _coreDBContext.TbCamEntitySupport.UpdateRange(model);
                            }
                        }
                        await _coreDBContext.TbCamEntitySupport.AddRangeAsync(newSuppdata);
                    }

                    List<TbCamEntitySupport> supportModel = await _coreDBContext.TbCamEntitySupport.Where(x => x.EntityId == entityId).ToListAsync();
                    if (supportModel.Except(suppData).Any())
                    {
                        var diff = supportModel.Except(suppData);
                        _coreDBContext.TbCamEntitySupport.RemoveRange(diff);
                    }

                    await _coreDBContext.SaveChangesAsync();
                    await transaction.CommitAsync();
                    return true;

                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                    return false;
                }
            }
        }

        /// <summary>
        /// Get Support
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public async Task<List<TbCamEntitySupportDto>> GetByEntityId(int entityId)
        {
            _logger.LogInformation($"GetSupport --- ByEntityId: {entityId}.");
            try
            {
                List<TbCamEntitySupport> support = await _coreDBContext.TbCamEntitySupport.Where(x => x.EntityId == entityId).ToListAsync();
                var res = _mapper.Map<List<TbCamEntitySupport>, List<TbCamEntitySupportDto>>(support);
                return res;


            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Check if contain Support
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns>bool</returns>
        public async Task<bool> CheckSupport(int entityId)
        {
            try
            {
                _logger.LogInformation($"Check Support --- EntityId: {entityId}");
                bool result = await _coreDBContext.TbCamEntitySupport.AnyAsync(x => x.EntityId == entityId);
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return false;
            }
        }

    }
}