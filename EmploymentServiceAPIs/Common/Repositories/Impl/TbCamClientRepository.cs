using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using EmploymentServiceAPIs.Common.Constants;
using EmploymentServiceAPIs.Common.Dtos;
using EmploymentServiceAPIs.Common.Repositories.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.Common.Repositories.Impl
{
    public class TbCamClientRepository : ITbCamClientRepository
    {
        private readonly ILogger<TbCamClientRepository> _logger;
        private readonly CoreDbContext _coreDBContext;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public TbCamClientRepository(CoreDbContext coreDBContext, IHttpContextAccessor httpContextAccessor, ILogger<TbCamClientRepository> logger)
        {
            _coreDBContext = coreDBContext;
            _httpContextAccessor = httpContextAccessor;
            _logger = logger;
        }

        private string GetUserName()
        {
            try
            {
                return _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.Name);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return "";
            }
        }

        public async Task<List<TbCamClient>> GetAll()
        {
            _logger.LogInformation("GetAll.");
            try
            {
                List<TbCamClient> client = await _coreDBContext.TbCamClient
                    .Where(c => c.ClientId > 0)
                    .OrderBy(c => c.ClientNameTxt)
                    .ToListAsync();
                return client;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<TbCamClient> GetById(int id)
        {
            _logger.LogInformation($"GetById - id - {id}");
            try
            {
                TbCamClient client = await _coreDBContext.TbCamClient.FirstOrDefaultAsync(x => x.ClientId == id);
                return client;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<int> Create(TbCamClient client)
        {
            _logger.LogInformation($"Create - {client.ClientCode}");
            try
            {
                client.CreatedBy = GetUserName();
                client.ModifiedBy = GetUserName();
                client.StatusCd = ClientStatus.PENDING;
                client.CreatedDt = DateTimeOffset.Now;
                client.ModifiedDt = DateTimeOffset.Now;
                await _coreDBContext.TbCamClient.AddAsync(client);
                _coreDBContext.SaveChanges();

                // hierarchy id
                HierarchyId newLevel = HierarchyId.Parse("/" + client.ClientId + "/");
                client.ClientLevel = newLevel;
                _coreDBContext.Entry(client).State = EntityState.Modified;
                await _coreDBContext.SaveChangesAsync();

                return client.ClientId;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return 0;
            }
        }

        public async Task<bool> Update(TbCamClient tbclient)
        {
            _logger.LogInformation($"Update client - {tbclient.ClientId}");
            try
            {
                var client = _coreDBContext.TbCamClient.Find(tbclient.ClientId);
                if (client != null)
                {
                    client.ModifiedBy = GetUserName();
                    client.ModifiedDt = DateTimeOffset.Now;
                    client.ClientNameTxt = tbclient.ClientNameTxt;
                    client.ContactPerson = tbclient.ContactPerson;
                    client.StatusCd = tbclient.StatusCd;
                    client.ClientCode = tbclient.ClientCode;
                    client.InternalCode = tbclient.InternalCode;
                    client.Remarks = tbclient.Remarks;
                    _coreDBContext.Entry(client).State = EntityState.Modified;
                    await _coreDBContext.SaveChangesAsync();
                    return true;
                }
                else
                {
                    _logger.LogInformation($"No such client - {tbclient.ClientId} in database.");
                    return false;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return false;
            }
        }

        public async Task<List<ClientViewModel>> GetClients()
        {
            _logger.LogInformation($"GetClients");
            try
            {
                var clients = await _coreDBContext.TbCamClient
                              .Select(c => new ClientViewModel()
                              {
                                  ClientId = c.ClientId,
                                  ClientName = c.ClientNameTxt,
                                  Entities = c.TbCamEntity.Select(x => new ClientEntityViewModel
                                  {
                                      ClientEntityId = x.EntityId,
                                      EntityName = x.EntityNameTxt
                                  }).OrderBy(x => x.EntityName).ToList()
                              }).OrderBy(x => x.ClientName).ToListAsync();
                return clients;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<TbCamClient> GetClientByCode(string code)
        {
            _logger.LogInformation($"GetClientByCode - ClientCode - {code}.");
            try
            {
                TbCamClient client = await _coreDBContext.TbCamClient.FirstOrDefaultAsync(x => x.ClientCode == code);
                return client;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<List<TbClientDto>> GetAllWithEntityCount()
        {
            _logger.LogInformation("GetAllWithEntityCount");
            try
            {
                var list = await _coreDBContext.TbCamClient
                    .Include(c => c.TbCamEntity)
                    .Where(c => c.ClientId > 0)
                    .OrderBy(c => c.ClientNameTxt)
                    .Select(c => new TbClientDto
                    {
                        ClientId = c.ClientId,
                        ClientNameTxt = c.ClientNameTxt,
                        ClientCode = c.ClientCode,
                        ContactPerson = c.ContactPerson,
                        CreatedBy = c.CreatedBy,
                        CreatedDt = c.CreatedDt,
                        StatusCd = c.StatusCd,
                        NoOfEntities = c.TbCamEntity.Count()
                    })
                    .ToListAsync();
                return list;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<ClientResult> GetClientEntityId(int clientId, int entityId)
        {
            _logger.LogInformation($"GetClientEntityId - {clientId} & {entityId}");
            try
            {
                ClientResult result = new ClientResult();

                result.ClientId = clientId;
                result.EntityId = entityId;

                // client info
                TbCamClient client = await _coreDBContext.TbCamClient
                    .Where(e => e.ClientId == clientId)
                    .FirstOrDefaultAsync();

                if (client != null)
                {
                    result.ClientCode = client.ClientCode;
                    result.ClientName = client.ClientNameTxt;
                    result.ClientLevel = client.ClientLevel.ToString();
                }
                else
                {
                    result.ClientId = -1;
                }

                // entity info
                TbCamEntity entity = await _coreDBContext.TbCamEntity
                        .Where(e => e.EntityId == entityId)
                        .FirstOrDefaultAsync();

                if (entity != null)
                {
                    result.EntityCode = entity.EntityCode;
                    result.EntityName = entity.EntityNameTxt;
                    result.EntityLevel = entity.EntityLevel.ToString();
                }
                else
                {
                    if (result.ClientId == 0)
                    {
                        result.EntityId = 0;
                    }
                    else
                    {
                        result.EntityId = -1;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }
    }
}