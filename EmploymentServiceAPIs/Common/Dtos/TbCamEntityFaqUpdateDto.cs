using CommonLib.Validation;
using EmploymentServiceAPIs.Common.Constants;
using System.ComponentModel.DataAnnotations;

namespace EmploymentServiceAPIs.Common.Dtos
{
    public class TbCamEntityFaqUpdateDto
    {
        public int FaqId { get; set; }
        public int EntityId { get; set; }

        [MaxLength(500, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "question", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.question", "500" })]
        public string Question { get; set; }

        [MaxLength(500, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "answer", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.answer", "500" })]
        public string Answer { get; set; }
        public int FaqOrder { get; set; }
    }
}