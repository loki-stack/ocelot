using CommonLib.Validation;
using EmploymentServiceAPIs.Common.Constants;
using System.ComponentModel.DataAnnotations;

namespace EmploymentServiceAPIs.Common.Dtos
{
    public class TbClientEntityCreateDto
    {

        [Required(ErrorMessage = ValMsg.VAL_REQUIRED)]
        [MaxLength(200, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "entityNameTxt", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.entityName", "200" })]
        public string EntityNameTxt { get; set; }

        [Required(ErrorMessage = ValMsg.VAL_REQUIRED)]
        [MaxLength(200, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "entityDisplayName", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.entityDisplayName", "200" })]
        public string EntityDisplayName { get; set; }

        [Required(ErrorMessage = ValMsg.VAL_REQUIRED)]
        [MaxLength(10, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "entityCode", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.entityCode", "10" })]
        public string EntityCode { get; set; }

        [Required(ErrorMessage = ValMsg.VAL_REQUIRED)]
        [MaxLength(41, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "countryCd", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.entityCountry"})]
        public string CountryCd { get; set; }
        public string Remarks { get; set; }

        [MaxLength(50, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "internalCode", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.Internal Code"})]
        public string InternalCode { get; set; }
    }
}