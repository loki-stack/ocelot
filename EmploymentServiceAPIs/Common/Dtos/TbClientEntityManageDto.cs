using System;
using System.Collections.Generic;
using EmploymentServiceAPIs.Common.Constants;
using System.ComponentModel.DataAnnotations;
using CommonLib.Validation;

namespace EmploymentServiceAPIs.Common.Dtos
{
    public class TbClientEntityManageDto
    {

        public int EntityId { get; set; }
        public int ParentClientId { get; set; }
        //public HierarchyId EntityLevel { get; set; }

        [Required(ErrorMessage = ValMsg.VAL_REQUIRED)]
        [MaxLength(200, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "entityNameTxt", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.entityName", "200" })]
        public string EntityNameTxt { get; set; }

        [Required(ErrorMessage = ValMsg.VAL_REQUIRED)]
        [MaxLength(200, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "entityDisplayName", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.entityDisplayName", "200" })]
        public string EntityDisplayName { get; set; }

        [Required(ErrorMessage = ValMsg.VAL_REQUIRED)]
        [MaxLength(10, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "entityCode", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.entityCode", "10" })]
        public string EntityCode { get; set; }

        [Required(ErrorMessage = ValMsg.VAL_REQUIRED)]
        [MaxLength(41, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "countryCd", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.entityCountry", "41"})]
        public string CountryCd { get; set; }
        public string Remarks { get; set; }
        public string DbHost { get; set; }
        public string DbName { get; set; }
        public int DbPort { get; set; }
        public string ConnStringParam { get; set; }

        [Required(ErrorMessage = ValMsg.VAL_REQUIRED)]
        [MaxLength(41, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "statusCd", Level = Level.ERROR, Placeholder = new string[] { "common.status"})]
        public string StatusCd { get; set; }
        public DateTimeOffset CreatedDt { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset ModifiedDt { get; set; }
        public string ModifiedBy { get; set; }

        [MaxLength(200, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "businessAddressLine1", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.addressLine1", "200" })]
        public string BusinessAddressLine1 { get; set; }

        [MaxLength(200, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "businessAddressLine2", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.addressLine2", "200" })]
        public string BusinessAddressLine2 { get; set; }

        [MaxLength(200, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "businessAddressLine3", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.addressLine3", "200" })]
        public string BusinessAddressLine3 { get; set; }
        public string BusinessPhone { get; set; }
        public string BusinessFax { get; set; }

        [MaxLength(200, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "contactAddressLine1", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.addressLine1", "200" })]
        public string ContactAddressLine1 { get; set; }

        [MaxLength(200, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "contactAddressLine2", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.addressLine2", "200" })]
        public string ContactAddressLine2 { get; set; }

        [MaxLength(200, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "contactAddressLine3", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.addressLine3", "200" })]
        public string ContactAddressLine3 { get; set; }

        [MaxLength(50, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "contactPhone", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.contactPhone", "50" })]
        public string ContactPhone { get; set; }
        public string ContactFax { get; set; }

        [MaxLength(200, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "contactName", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.contactName", "200" })]
        public string ContactName { get; set; }

        [MaxLength(255, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "contactEmail", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.contactEmail", "255" })]
        public string ContactEmail { get; set; }
        public string TaxId { get; set; }
        public string BusinessPhoneAreaCd { get; set; }
        public string BusinessPhoneFaxAreaCd { get; set; }
        public string ContactPhoneAreaCd { get; set; }
        public string ContactFaxAreaCd { get; set; }

        [MaxLength(50, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "internalCode", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.Internal Code", "50"})]
        public string InternalCode { get; set; }

        [MaxLength(200, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "entityNameTxtSecondary", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.legalEntity", "200" })]
        public string EntityNameTxtSecondary { get; set; }

        [MaxLength(200, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "entityDisplayNameSecondary", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.entityDisplayName", "200" })]
        public string EntityDisplayNameSecondary { get; set; }

        [MaxLength(200, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "businessAddressLine1Secondary", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.addressLine1", "200" })]
        public string BusinessAddressLine1Secondary { get; set; }

        [MaxLength(200, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "businessAddressLine2Secondary", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.addressLine2", "200" })]
        public string BusinessAddressLine2Secondary { get; set; }

        [MaxLength(200, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "businessAddressLine3Secondary", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.addressLine3", "200" })]
        public string BusinessAddressLine3Secondary { get; set; }

        [MaxLength(200, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "contactAddressLine1Secondary", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.addressLine1", "200" })]
        public string ContactAddressLine1Secondary { get; set; }

        [MaxLength(200, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "contactAddressLine2Secondary", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.addressLine2", "200" })]
        public string ContactAddressLine2Secondary { get; set; }

        [MaxLength(200, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "contactAddressLine3Secondary", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.addressLine3", "200" })]
        public string ContactAddressLine3Secondary { get; set; }
        public List<TbCamEntityFaqUpdateDto> FAQ { get; set; }
        public List<TbCamEntitySupportUpdateDto> Support { get; set; }
    }
}