﻿using System;

namespace EmploymentServiceAPIs.Common.Dtos
{
    public class TbClientViewModel
    {
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public string ContactPerson { get; set; }
        public string StatusCd { get; set; }
        public DateTime CreatedDt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDt { get; set; }
        public string ModifiedBy { get; set; }
    }
}
