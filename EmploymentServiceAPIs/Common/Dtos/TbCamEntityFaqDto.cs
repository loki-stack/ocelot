using System;

namespace EmploymentServiceAPIs.Common.Dtos
{
    public class TbCamEntityFaqDto
    {
        public int FaqId { get; set; }
        public int EntityId { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public int FaqOrder { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
    }
}