﻿namespace EmploymentServiceAPIs.Common.Dtos
{
    public class UpdateObjectResult
    {
        public bool IsSuceess { get; set; }
        public string Error { get; set; }
    }

    public class UpdateObjectResult<T>
    {
        public T Data { get; set; }
        public bool IsSucess { get; set; }
        public string Error { get; set; }
    }
}