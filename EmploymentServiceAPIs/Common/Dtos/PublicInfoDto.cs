#pragma warning disable 1591

namespace EmploymentServiceAPIs.Common.Dtos
{
    public class PublicInfoDto
    {
        public string ApplicationName { get; set; }
        public string Environment { get; set; }
        public string DbInfo { get; set; }
        public string CoreServiceGenericCodeUri { get; set; }
        public string CoreServiceMasterPayItemUri { get; set; }

    }
}

#pragma warning restore 1591