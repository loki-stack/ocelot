using System;

namespace EmploymentServiceAPIs.Common.Dtos
{
    public class TbClientDto
    {
        public int ClientId { get; set; }
        public string ClientNameTxt { get; set; }

        public string ClientCode { get; set; }

        public string ContactPerson { get; set; }

        public string StatusCd { get; set; }
        public DateTimeOffset CreatedDt { get; set; }
        public string CreatedBy { get; set; }

        public int NoOfEntities { get; set; }

        public string Remarks { get; set; }

        public string InternalCode { get; set; }
    }
}