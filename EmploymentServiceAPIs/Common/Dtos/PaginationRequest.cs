﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EmploymentServiceAPIs.Common.Dtos
{
    public class PaginationRequest
    {
        public string Sort { get; set; }
        [Range(1, int.MaxValue)]
        public int? Page { get; set; }
        [Range(1, int.MaxValue)]
        public int? Size { get; set; }
        public string Filter { get; set; }
        public string FullTextSearch { get; set; }
    }
}
