using CommonLib.Validation;
using EmploymentServiceAPIs.Common.Constants;
using System.ComponentModel.DataAnnotations;

namespace EmploymentServiceAPIs.Common.Dtos
{
    public class TbClientUpdateDto
    {
        public int ClientId { get; set; }

        [Required(ErrorMessage = ValMsg.VAL_REQUIRED)]
        [MaxLength(200, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "clientNameTxt", Level = Level.ERROR, Placeholder = new string[] { "common.name", "100" })]
        public string ClientNameTxt { get; set; }

        [Required(ErrorMessage = ValMsg.VAL_REQUIRED)]
        [MaxLength(10, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "clientCode", Level = Level.ERROR, Placeholder = new string[] { "clientModule.Profile Code", "10" })]
        public string ClientCode { get; set; }

        [MaxLength(200, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "contactPerson", Level = Level.ERROR, Placeholder = new string[] { "common.director", "200" })]
        public string ContactPerson { get; set; }

        [Required(ErrorMessage = ValMsg.VAL_REQUIRED)]
        [MaxLength(41, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "statusCd", Level = Level.ERROR, Placeholder = new string[] { "common.status", "2" })]
        public string StatusCd { get; set; }
        public string Remarks { get; set; }

        [MaxLength(50, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "internalCode", Level = Level.ERROR, Placeholder = new string[] { "clientModule.Internal Code", "50" })]
        public string InternalCode { get; set; }
    }
}