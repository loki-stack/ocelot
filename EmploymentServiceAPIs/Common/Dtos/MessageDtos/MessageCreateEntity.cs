namespace EmploymentServiceAPIs.Common.Dtos.MessageDtos
{
    public class MessageCreateEntity
    {
        public string EntityNameTxt { get; set; }
        public string EntityDisplayName { get; set; }
        public string EntityCode { get; set; }
        public string Link { get; set; }
    }
}