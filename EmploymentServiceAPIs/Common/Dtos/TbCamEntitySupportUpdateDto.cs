using CommonLib.Validation;
using EmploymentServiceAPIs.Common.Constants;
using System;
using System.ComponentModel.DataAnnotations;
namespace EmploymentServiceAPIs.Common.Dtos
{
    public class TbCamEntitySupportUpdateDto
    {
        public string CreatedDt { get; set; }
        public int SupportId { get; set; }
        public int EntityId { get; set; }

        [Required(ErrorMessage = ValMsg.VAL_REQUIRED)]
        [MaxLength(100, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "supportContactName", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.supportContactName", "100" })]
        public string SupportContactName { get; set; }
        
        [ExtraResult(ComponentId = "supportPhoneNumber", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.supportPhoneNumber", "50" })]
        public string SupportPhoneNumber { get; set; }
        
        [ExtraResult(ComponentId = "supportEmail", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.supportEmail", "50" })]
        public string SupportEmail { get; set; }
    }
}