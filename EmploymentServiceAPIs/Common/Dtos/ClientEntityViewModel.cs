﻿namespace EmploymentServiceAPIs.Common.Dtos
{
    public class ClientEntityViewModel
    {
        public int ClientEntityId { get; set; }
        public string EntityName { get; set; }
    }
}