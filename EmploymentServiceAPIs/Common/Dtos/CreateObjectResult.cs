﻿namespace EmploymentServiceAPIs.Common.Dtos
{
    public class CreateObjectResult
    {
        public int Id { get; set; }
        public bool IsSuccess { get; set; }
        public string Error { get; set; }
    }

    public class CreateObjectResult<T>
    {
        public T Data { get; set; }
        public bool IsSuccess { get; set; }
        public string Error { get; set; }
    }
}