﻿using System.Collections.Generic;

namespace EmploymentServiceAPIs.Common.Dtos
{
    public class ClientViewModel
    {
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public string ClientCode { get; set; }
        public List<ClientEntityViewModel> Entities { get; set; }
    }
}