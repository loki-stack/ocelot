﻿using EmploymentServiceAPIs.Common.Dtos;
using EmploymentServiceAPIs.ManageEntity.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.ManageEntity.Repositories.Interfaces
{
    public interface IOrganizationStructureRepository
    {
        Task<OrganizationStructureAllModel> GetAll(PaginationRequest filter, int clientId, int entityId);
        Task<int?> Create(OrganizationStructureStoreModel request);
        Task<int?> Update(OrganizationStructureUpdateModel request);
        Task<OrganizationStructureModel> GetOrganizationStructureById(int unitId);
        Task<List<OrganizationStructureDisplayNameModel>> GetOrganizationStructureDisplayName(int? unitId);
        Task<List<TreeDto<OrganizationStructureDataAccessGroupDto>>> GetOrganizationStructureAsTree(int clientId, int entityId, string entityCode, int darId, List<DataAccessOrganizationStructureDto> daOrgStruc);
    }
}
