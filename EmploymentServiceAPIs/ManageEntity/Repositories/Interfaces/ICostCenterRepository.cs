﻿using EmploymentServiceAPIs.ManageEntity.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.ManageEntity.Repositories.Interfaces
{
    public interface ICostCenterRepository
    {
        Task<CostCenterViewModel> Create(CreateCostCenterRequest request);

        Task<CostCenterViewModel> Update(UpdateCostCenterRequest request);

        Task<List<CostCenterViewModel>> GetFilter();

        Task<CostCenterViewModel> GetById(int id);

        bool CheckCostCenterById(int id);

        bool CheckExistCode(int costId, string code);
    }
}