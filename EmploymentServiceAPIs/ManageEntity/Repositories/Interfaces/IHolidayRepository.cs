﻿using CommonLib.Models.Client;
using EmploymentServiceAPIs.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.ManageEntity.Repositories.Interfaces
{
    public interface IHolidayRepository
    {
        Task<bool> Create(TbCamHoliday holiday);

        Task<bool> CreateMany(List<TbCamHoliday> holidays);

        Task<bool> Update(TbCamHoliday holiday);

        Task<bool> UpdateMany(List<TbCamHoliday> holidays);

        Task<TbCamHoliday> GetById(int id);

        bool CheckExistHolidayById(int id);

        Task<List<TbCamHoliday>> GetHolidayByCalendarId(int calendarId);

        Task<bool> Delete(int id);

        Task<bool> DeleteMany(List<TbCamHoliday> holidays);

        Task<List<TbCamHoliday>> GetByIds(List<int> ids);
    }
}