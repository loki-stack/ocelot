﻿using EmploymentServiceAPIs.ManageEntity.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.ManageEntity.Repositories.Interfaces
{
    public interface IJobGradeRepository
    {
        Task<JobGradeViewModel> Create(CreateJobGradeRequest request);

        Task<JobGradeViewModel> Update(UpdateJobGradeRequest request);

        Task<List<JobGradeViewModel>> GetFilter();

        Task<JobGradeViewModel> GetById(int id);

        bool CheckJobGradeById(int id);

        bool CheckExistLevel(int jobGradeId, int level);
    }
}