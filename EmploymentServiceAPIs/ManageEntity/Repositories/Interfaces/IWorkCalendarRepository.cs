﻿using CommonLib.Models.Client;
using EmploymentServiceAPIs.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.ManageEntity.Repositories.Interfaces
{
    public interface IWorkCalendarRepository
    {
        Task<int> Create(TbCamWorkCalendar workCalendar);

        Task<int> Update(TbCamWorkCalendar workCalendar);

        Task<TbCamWorkCalendar> GetById(int id);

        bool CheckExistWorkCalendarById(int id);

        Task<List<TbCamWorkCalendar>> GetFilter();

        Task<bool> Delete(int id);

        bool CheckExistNameCalendarInEntity(int entityId, string calendarName, int calendarId);

        Task<bool> CheckExistCalendarActiveInEntity(int entityId);

        Task<bool> CheckExistLinkToEE(int calendarId);
    }
}