﻿using AutoMapper;
using CommonLib.Models;
using CommonLib.Models.Client;
using CommonLib.Models.Core;
using CommonLib.Services;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.ManageEntity.Dtos;
using EmploymentServiceAPIs.ManageEntity.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.ManageEntity.Repositories.Impl
{
    public class CostCenterRepository : ICostCenterRepository
    {
        protected readonly IConnectionStringContainer _connectionStringContainer;
        protected string _connectionString;
        protected readonly CoreDbContext _coreDbContext;
        private readonly IMapper _mapper;

        public CostCenterRepository(IConnectionStringContainer connectionStringContainer, CoreDbContext coreDBContext, IMapper mapper)
        {
            _connectionStringContainer = connectionStringContainer;
            _coreDbContext = coreDBContext;
            _connectionString = _connectionStringContainer.GetConnectionString();
            _mapper = mapper;
        }

        protected TenantDBContext CreateContext(string _connectionString)
        {
            return new TenantDBContext(_connectionString);
        }

        public bool CheckCostCenterById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                return _context.TbCfgCostCenter.Any(x => x.CostCenterId == id);
            }
            return false;
        }

        public async Task<CostCenterViewModel> Create(CreateCostCenterRequest request)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var model = new TbCfgCostCenter();
                model.CostCenterCd = request.Code;
                model.CostCenterName = request.Name;
                model.StatusCd = request.Status;
                model.Remark = request.Remark;
                model.CreatedBy = request.CreatedBy;
                model.CreatedDt = request.CreatedDt;
                model.ModifiedBy = request.ModifiedBy;
                model.ModifiedDt = request.ModifiedDt;

                await _context.TbCfgCostCenter.AddAsync(model);
                await _context.SaveChangesAsync();
                var result = await GetById(model.CostCenterId);
                return result;
            }
            return null;
        }

        public async Task<CostCenterViewModel> Update(UpdateCostCenterRequest request)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var model = await _context.TbCfgCostCenter.FirstOrDefaultAsync(x => x.CostCenterId == request.Id);
                model.CostCenterCd = request.Code;
                model.CostCenterName = request.Name;
                model.StatusCd = request.Status;
                model.Remark = request.Remark;
                model.ModifiedBy = request.ModifiedBy;
                model.ModifiedDt = request.ModifiedDt;

                _context.TbCfgCostCenter.Update(model);
                await _context.SaveChangesAsync();
                var result = await GetById(model.CostCenterId);
                return result;
            }
            return null;
        }

        public async Task<List<CostCenterViewModel>> GetFilter()
        {
            var viewList = new List<CostCenterViewModel>();
            using (var _context = CreateContext(_connectionString))
            {
                var costCenters = await _context.TbCfgCostCenter.ToListAsync();
                viewList = _mapper.Map<List<TbCfgCostCenter>, List<CostCenterViewModel>>(costCenters);
            }
            return viewList;
        }

        public async Task<CostCenterViewModel> GetById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var costCenter = await _context.TbCfgCostCenter.FirstOrDefaultAsync(x => x.CostCenterId == id);
                var costCenterVm = _mapper.Map<TbCfgCostCenter, CostCenterViewModel>(costCenter);
                return costCenterVm;
            }
            return null;
        }

        public bool CheckExistCode(int costId, string code)
        {
            using (var _context = CreateContext(_connectionString))
            {
                return _context.TbCfgCostCenter
                .Any(x => x.CostCenterCd == code && (costId == 0 || x.CostCenterId != costId));
            }
            return false;
        }
    }
}