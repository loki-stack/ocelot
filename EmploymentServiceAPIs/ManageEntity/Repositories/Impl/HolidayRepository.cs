﻿using CommonLib.Models.Client;
using CommonLib.Services;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.ManageEntity.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.ManageEntity.Repositories.Impl
{
    public class HolidayRepository : IHolidayRepository
    {
        protected readonly IConnectionStringContainer _connectionStringRepository;
        protected string _connectionString;

        public HolidayRepository(IConnectionStringContainer connectionStringRepository)
        {
            _connectionStringRepository = connectionStringRepository;
            _connectionString = _connectionStringRepository.GetListConnectionString().FirstOrDefault().Value;
        }

        protected TenantDBContext CreateContext(string _connectionString)
        {
            return new TenantDBContext(_connectionString);
        }

        public bool CheckExistHolidayById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                return _context.TbCamHoliday.Any(x => x.HolidayDateId == id);
            }
        }

        public async Task<bool> Create(TbCamHoliday holiday)
        {
            using (var _context = CreateContext(_connectionString))
            {
                await _context.TbCamHoliday.AddAsync(holiday);
                await _context.SaveChangesAsync();
                return true;
            }
        }

        public async Task<bool> CreateMany(List<TbCamHoliday> holidays)
        {
            using (var _context = CreateContext(_connectionString))
            {
                await _context.TbCamHoliday.AddRangeAsync(holidays);
                await _context.SaveChangesAsync();
                return true;
            }
        }

        public async Task<bool> Delete(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var holiday = await _context.TbCamHoliday.FirstOrDefaultAsync(x => x.HolidayDateId == id);
                _context.TbCamHoliday.Remove(holiday);
                await _context.SaveChangesAsync();
                return true;
            }
        }

        public async Task<bool> DeleteMany(List<TbCamHoliday> holidays)
        {
            using (var _context = CreateContext(_connectionString))
            {
                _context.TbCamHoliday.RemoveRange(holidays);
                await _context.SaveChangesAsync();
                return true;
            }
        }

        public async Task<TbCamHoliday> GetById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var holiday = await _context.TbCamHoliday.AsNoTracking().FirstOrDefaultAsync(x => x.HolidayDateId == id);
                return holiday;
            }
        }

        public async Task<List<TbCamHoliday>> GetByIds(List<int> ids)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var holidays = await _context.TbCamHoliday.Where(x => ids.Contains(x.HolidayDateId)).ToListAsync();
                return holidays;
            }
        }

        public async Task<List<TbCamHoliday>> GetHolidayByCalendarId(int calendarId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var holidays = await _context.TbCamHoliday.Where(x => x.CalendarId == calendarId).AsNoTracking().ToListAsync();
                return holidays;
            }
        }

        public async Task<bool> Update(TbCamHoliday holiday)
        {
            using (var _context = CreateContext(_connectionString))
            {
                _context.TbCamHoliday.Update(holiday);
                await _context.SaveChangesAsync();
                return true;
            }
        }

        public async Task<bool> UpdateMany(List<TbCamHoliday> holidays)
        {
            using (var _context = CreateContext(_connectionString))
            {
                _context.TbCamHoliday.UpdateRange(holidays);
                await _context.SaveChangesAsync();
                return true;
            }
        }
    }
}