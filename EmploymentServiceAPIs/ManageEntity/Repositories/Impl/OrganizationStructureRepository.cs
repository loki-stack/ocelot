﻿using CommonLib.Models.Client;
using CommonLib.Services;
using EmploymentServiceAPIs.Common.Constants;
using EmploymentServiceAPIs.Common.Dtos;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.ManageEntity.Dtos;
using EmploymentServiceAPIs.ManageEntity.Repositories.Interfaces;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.ManageEntity.Repositories.Impl
{
    public class OrganizationStructureRepository : IOrganizationStructureRepository
    {
        private readonly TenantDBContext _context;
        private readonly ILogger<OrganizationStructureRepository> _logger;

        public OrganizationStructureRepository(IConnectionStringContainer connectionStringContainer,
            ILogger<OrganizationStructureRepository> logger)
        {
            _context = new TenantDBContext(connectionStringContainer.GetConnectionString());
            _logger = logger;
        }

        /// <summary>
        /// Get ALl Organization Structure
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<OrganizationStructureAllModel> GetAll(PaginationRequest filter, int clientId, int entityId)
        {
            OrganizationStructureAllModel result = new OrganizationStructureAllModel();
            var clientIdParam = new SqlParameter("@clientId", clientId);
            var entityIdParam = new SqlParameter("@entityId", entityId);
            var sortParam = new SqlParameter("@sort", filter.Sort);
            var pageParam = new SqlParameter("@page", filter.Page.HasValue ? filter.Page : (object)DBNull.Value);
            var sizeParam = new SqlParameter("@size", filter.Size.HasValue ? filter.Size : (object)DBNull.Value);
            var filterParam = new SqlParameter("@filter", string.IsNullOrEmpty(filter.Filter) ? (object)DBNull.Value : filter.Filter);
            var fullTextSearchParam = new SqlParameter("@fullTextSearch", string.IsNullOrEmpty(filter.FullTextSearch) ? (object)DBNull.Value : filter.FullTextSearch);
            var countDataParam = new SqlParameter("@count", System.Data.SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
            result.Data = await _context.Set<OrganizationStructureModel>().FromSqlRaw("EXECUTE SP_GET_ALL_ORGANIZATION_STRUCTURE @clientId, @entityId, @sort, @page, @size, @filter, @fullTextSearch, @count OUT", clientIdParam, entityIdParam, sortParam, pageParam, sizeParam, filterParam, fullTextSearchParam, countDataParam).ToListAsync();
            result.TotalRecord = (int)countDataParam.Value;
            return result;
        }

        /// <summary>
        /// Get ALL Organization Structure in Tree Format
        /// </summary>
        /// <param name="entityCode"></param>
        /// <param name="clientId"></param>
        /// <param name="entityId"></param>
        /// <param name="darId"></param>
        /// <returns>OrganizationStructureModel</returns>
        public async Task<List<TreeDto<OrganizationStructureDataAccessGroupDto>>> GetOrganizationStructureAsTree(int clientId, int entityId, string entityCode, int darId, List<DataAccessOrganizationStructureDto> daOrgStructure)
        {
            try
            {
                var parents = await GetParent(clientId, entityId, 1);
                var children = await GetChildren(clientId, entityId, 1, darId, daOrgStructure);


                var tree = parents.Select(x => new TreeDto<OrganizationStructureDataAccessGroupDto>
                {
                    Key = x.UnitId.ToString(),
                    Data = new OrganizationStructureDataAccessGroupDto
                    {
                        ItemId = x.UnitId,
                        ItemType = "Entity",
                        ItemCode = x.UnitCode,
                        ItemDescendantCount = _context.TbCamUnit
                                                            .Where(p => p.CodeLevel.IsDescendantOf(x.CodeLevel))
                                                            .Where(p => p.ClientId == clientId)
                                                            .Where(p => p.EntityId == entityId)
                                                            .Count(),
                        ItemLevel = x.CodeLevel.ToString(),
                        ItemText = x.DisplayName,
                        DataAccessGroupId = darId,
                        IsSelected = daOrgStructure.Count <= 0 ? false : daOrgStructure
                                                    .Where(p =>
                                                        p.DataAccessGroupId == darId &&
                                                        (p.NodeLevel == x.CodeLevel))
                                                    .Any()
                    },
                    Children = (x.CodeLevel.GetLevel() > 0 ? GetTreeMethod(clientId, entityId, x.CodeLevel, x.UnitId, children) : null),
                }).ToList();
                return tree;
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex.ToString());
                return null;
            }
        }

        private List<TreeDto<OrganizationStructureDataAccessGroupDto>> GetTreeMethod(int clientId, int entityId, HierarchyId node, int unitId, List<OrganizationStructureDataAccessGroupDto> lstCollection)
        {
            List<TreeDto<OrganizationStructureDataAccessGroupDto>> lst = new List<TreeDto<OrganizationStructureDataAccessGroupDto>>();
            var lastItemInCurrentLevel = GetChilds(clientId, entityId, node, unitId, lstCollection);

            foreach (var item in lastItemInCurrentLevel)
            {
                TreeDto<OrganizationStructureDataAccessGroupDto> tr = new TreeDto<OrganizationStructureDataAccessGroupDto>
                {
                    Key = item.Key,
                    Data = item.Data,

                };
                tr.Children = GetTreeMethod(clientId, entityId, HierarchyId.Parse(item.Data.ItemLevel), unitId, lstCollection);
                lst.Add(tr);
            }

            return lst;

        }

        private List<TreeDto<OrganizationStructureDataAccessGroupDto>> GetChilds(int clientId, int entityId, HierarchyId node, int unitId, List<OrganizationStructureDataAccessGroupDto> lstCollection)
        {
            List<TreeDto<OrganizationStructureDataAccessGroupDto>> child = lstCollection
                                                    .Where(x => HierarchyId.Parse(x.ItemLevel).GetAncestor(1) == node)
                                                    .Select(q => new TreeDto<OrganizationStructureDataAccessGroupDto>
                                                    {
                                                        Key = q.ItemId.ToString(),
                                                        Data = new OrganizationStructureDataAccessGroupDto
                                                        {
                                                            ItemId = q.ItemId,
                                                            ItemType = q.ItemType,
                                                            ItemDescendantCount = _context.TbCamUnit
                                                                                            .Where(p => p.CodeLevel.IsDescendantOf(HierarchyId.Parse(q.ItemLevel)))
                                                                                            .Where(p => p.ClientId == clientId)
                                                                                            .Where(p => p.EntityId == entityId)
                                                                                            .Count(),
                                                            ItemCode = q.ItemCode,
                                                            IsSelected = q.IsSelected,
                                                            ItemText = q.ItemText,
                                                            DataAccessGroupId = q.DataAccessGroupId,
                                                            ItemLevel = q.ItemLevel,
                                                        }
                                                    }).ToList();
            return child;
        }
        private async Task<HashSet<TbCamUnit>> GetParent(int clientId, int entityId, int level)
        {
            HashSet<TbCamUnit> parents = (await (from g in _context.TbCamUnit
                                                 where (g.CodeLevel.GetLevel() == level && g.StatusCd == UnitStatusCode.ACTIVE)
                                                 where (g.ClientId == clientId && g.EntityId == entityId)
                                                 select g
                                                            /*select new OrganizationStructureDataAccessGroupDto {
                                                            ItemId = g.UnitId,
                                                            //ItemType = "",
                                                            ItemCode = g.UnitCode,
                                                            //IsSelected = g.IsSelected,
                                                            ItemText = g.DisplayName,
                                                            //DataAccessGroupId = q.DataAccessGroupId,
                                                            ItemLevel = g.CodeLevel

                                                            }*/).ToListAsync()).ToHashSet();
            return parents;
        }
        private async Task<List<OrganizationStructureDataAccessGroupDto>> GetChildren(int clientId, int entityId, int level, int darId, List<DataAccessOrganizationStructureDto> daOrgStructure)
        {
            var children = await (from g in _context.TbCamUnit
                                  where (g.CodeLevel.GetLevel() > level && g.StatusCd == UnitStatusCode.ACTIVE)
                                  where (g.ClientId == clientId && g.EntityId == entityId)
                                  select g).ToListAsync();
            var child = children
                        .Select(g => new OrganizationStructureDataAccessGroupDto
                        {
                            ItemId = g.UnitId,
                            //ItemType = daOrgStructure[0].NodeLevel.ToString(),
                            ItemCode = g.UnitCode,
                            //IsSelected = false,
                            ItemDescendantCount = children
                                                    .Where(p => p.CodeLevel.IsDescendantOf(g.CodeLevel))
                                                    .Count(),
                            IsSelected = (daOrgStructure.Count > 0) ? (
                                daOrgStructure.Where(x => x.NodeLevel == g.CodeLevel).Any() ||
                                daOrgStructure
                                    .Any(p =>
                                    p.IsSelectAllDescendants == true &&
                                    p.NodeLevel.GetLevel() < g.CodeLevel.GetLevel() &&
                                    g.CodeLevel.IsDescendantOf(p.NodeLevel)
                                    )
                                )
                             : false,
                            ItemText = g.DisplayName,
                            DataAccessGroupId = darId,
                            ItemLevel = g.CodeLevel.ToString(),
                        }).ToList();
            /*select new OrganizationStructureDataAccessGroupDto {
            ItemId = g.UnitId,
            ItemType = daOrgStructure[0].NodeLevel.ToString(),
            ItemCode = g.UnitCode,
            //IsSelected = false,
            IsSelected = (daOrgStructure.Count > 0) ? daOrgStructure.Where(x => x.NodeLevel == g.CodeLevel).Any() : false,
            ItemText = g.DisplayName,
            DataAccessGroupId = darId,
            ItemLevel = g.CodeLevel.ToString(),
            }).ToListAsync();*/

            return child;
        }

        /// <summary>
        /// Add Organization Structure
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<int?> Create(OrganizationStructureStoreModel request)
        {
            if (request.ParentId == 0)
                request.ParentId = null;
            TbCamUnit model = new TbCamUnit
            {
                ParentId = request.ParentId,
                ClientId = request.ClientId,
                EntityId = request.EntityId,
                FullName = request.FullName,
                DisplayName = request.DisplayName,
                UnitCode = request.UnitCode,
                StatusCd = request.StatusCd,
                Remark = request.Remark,
                CreatedDt = DateTimeOffset.Now,
                CreatedBy = request.CreatedBy,
                ModifiedDt = DateTimeOffset.Now,
                ModifiedBy = request.CreatedBy
            };
            await _context.AddAsync(model);
            await _context.SaveChangesAsync();
            return model.UnitId;
        }

        public async Task<int?> Update(OrganizationStructureUpdateModel request)
        {
            if (request.ParentId == 0)
                request.ParentId = null;
            TbCamUnit model = new TbCamUnit();
            model = await _context.TbCamUnit.FindAsync(request.UnitId);
            if (model != null)
            {
                model.UnitCode = request.UnitCode;
                model.ParentId = request.ParentId;
                model.FullName = request.FullName;
                model.DisplayName = request.DisplayName;
                model.StatusCd = request.StatusCd;
                model.Remark = request.Remark;
                model.ModifiedDt = DateTimeOffset.Now;
                model.ModifiedBy = request.ModifyBy;
            }
            else
            {
                return null;
            }
            _context.Entry(model).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return request.UnitId;
        }

        /// <summary>
        /// Get Organization Structure by Id
        /// </summary>
        /// <param name="unitId"></param>
        /// <returns></returns>
        public async Task<OrganizationStructureModel> GetOrganizationStructureById(int unitId)
        {
            var unitIdParam = new SqlParameter("@unitId", unitId);
            var listData = await _context.Set<OrganizationStructureModel>().FromSqlRaw("EXECUTE SP_GET_ORGANIZATION_STRUCTURE_BY_ID @unitId", unitIdParam).ToListAsync();
            if (listData.Count > 0)
                return listData.First();
            return null;
        }

        /// <summary>
        /// Find display name of Organization Structure
        /// </summary>
        /// <param name="unitId"></param>
        /// <returns></returns>
        public async Task<List<OrganizationStructureDisplayNameModel>> GetOrganizationStructureDisplayName(int? unitId)
        {
            if (!unitId.HasValue)
            {
                return await _context.TbCamUnit.Where(n => n.StatusCd == UnitStatusCode.ACTIVE).
                Select(n => new OrganizationStructureDisplayNameModel() { DisplayName = n.DisplayName, UnitId = n.UnitId }).ToListAsync();
            }
            else
            {
                var unitIdParam = new SqlParameter("@unitId", unitId);
                return await _context.Set<OrganizationStructureDisplayNameModel>().FromSqlRaw("EXECUTE SP_GET_ORGANIATION_STRUCTURE_PARENT_EDIT @unitId", unitIdParam).ToListAsync();
            }
        }
    }
}
