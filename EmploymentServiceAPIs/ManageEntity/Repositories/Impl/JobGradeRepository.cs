﻿using AutoMapper;
using CommonLib.Models;
using CommonLib.Models.Client;
using CommonLib.Models.Core;
using CommonLib.Services;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.ManageEntity.Dtos;
using EmploymentServiceAPIs.ManageEntity.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.ManageEntity.Repositories.Impl
{
    public class JobGradeRepository : IJobGradeRepository
    {
        protected readonly IConnectionStringContainer _connectionStringContainer;
        protected string _connectionString;
        protected readonly CoreDbContext _coreDbContext;
        private readonly IMapper _mapper;

        public JobGradeRepository(IConnectionStringContainer connectionStringContainer, CoreDbContext coreDBContext, IMapper mapper)
        {
            _connectionStringContainer = connectionStringContainer;
            _coreDbContext = coreDBContext;
            _connectionString = _connectionStringContainer.GetConnectionString();
            _mapper = mapper;
        }

        protected TenantDBContext CreateContext(string _connectionString)
        {
            return new TenantDBContext(_connectionString);
        }

        public bool CheckJobGradeById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                return _context.TbCfgJobGrade.Any(x => x.JobGradeId == id);
            }
            return false;
        }

        public bool CheckExistLevel(int jobGradeId, int level)
        {
            using (var _context = CreateContext(_connectionString))
            {
                return _context.TbCfgJobGrade
                .Any(x => x.JobGradeLevel == level && (jobGradeId == 0 || x.JobGradeId != jobGradeId));
            }
            return false;
        }

        public async Task<JobGradeViewModel> Create(CreateJobGradeRequest request)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var model = new TbCfgJobGrade();
                model.JobGradeLevel = request.Level;
                model.JobGradeName = request.Name;
                model.StatusCd = request.Status;
                model.Remark = request.Remark;
                model.CreatedBy = request.CreatedBy;
                model.CreatedDt = request.CreatedDt;
                model.ModifiedBy = request.ModifiedBy;
                model.ModifiedDt = request.ModifiedDt;

                await _context.TbCfgJobGrade.AddAsync(model);
                await _context.SaveChangesAsync();
                var result = await GetById(model.JobGradeId);
                return result;
            }
            return null;
        }

        public async Task<JobGradeViewModel> GetById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var jobGrade = await _context.TbCfgJobGrade.FirstOrDefaultAsync(x => x.JobGradeId == id);
                var jobGradeVm = _mapper.Map<TbCfgJobGrade, JobGradeViewModel>(jobGrade);
                return jobGradeVm;
            }
            return null;
        }

        public async Task<List<JobGradeViewModel>> GetFilter()
        {
            var viewList = new List<JobGradeViewModel>();
            using (var _context = CreateContext(_connectionString))
            {
                var jobGrades = await _context.TbCfgJobGrade.ToListAsync();
                viewList = _mapper.Map<List<TbCfgJobGrade>, List<JobGradeViewModel>>(jobGrades);
            }
            return viewList;
        }

        public async Task<JobGradeViewModel> Update(UpdateJobGradeRequest request)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var model = await _context.TbCfgJobGrade.FirstOrDefaultAsync(x => x.JobGradeId == request.Id);
                model.JobGradeLevel = request.Level;
                model.JobGradeName = request.Name;
                model.StatusCd = request.Status;
                model.Remark = request.Remark;
                model.ModifiedBy = request.ModifiedBy;
                model.ModifiedDt = request.ModifiedDt;

                _context.TbCfgJobGrade.Update(model);
                await _context.SaveChangesAsync();
                var result = await GetById(model.JobGradeId);
                return result;
            }
            return null;
        }
    }
}