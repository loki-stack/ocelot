﻿using CommonLib.Models.Client;
using CommonLib.Services;
using EmploymentServiceAPIs.Common.Constants;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.ManageEntity.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.ManageEntity.Repositories.Impl
{
    public class WorkCalendarRepository : IWorkCalendarRepository
    {
        protected readonly IConnectionStringContainer _connectionStringRepository;
        protected string _connectionString;

        public WorkCalendarRepository(IConnectionStringContainer connectionStringRepository)
        {
            _connectionStringRepository = connectionStringRepository;
            _connectionString = _connectionStringRepository.GetListConnectionString().FirstOrDefault().Value;
        }

        protected TenantDBContext CreateContext(string _connectionString)
        {
            return new TenantDBContext(_connectionString);
        }

        public bool CheckExistWorkCalendarById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                return _context.TbCamWorkCalendar.Any(x => x.CalendarId == id);
            }
        }

        public async Task<int> Create(TbCamWorkCalendar workCalendar)
        {
            using (var _context = CreateContext(_connectionString))
            {
                await _context.TbCamWorkCalendar.AddAsync(workCalendar);
                await _context.SaveChangesAsync();
                return workCalendar.CalendarId;
            }
        }

        public async Task<bool> Delete(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var workCalendar = await _context.TbCamWorkCalendar.FirstOrDefaultAsync(x => x.CalendarId == id);
                _context.TbCamWorkCalendar.Remove(workCalendar);
                await _context.SaveChangesAsync();
                return true;
            }
        }

        public async Task<TbCamWorkCalendar> GetById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var workCalendar = await _context.TbCamWorkCalendar.Include(x => x.TbCamHoliday).FirstOrDefaultAsync(x => x.CalendarId == id);
                return workCalendar;
            }
        }

        public async Task<List<TbCamWorkCalendar>> GetFilter()
        {
            using (var _context = CreateContext(_connectionString))
            {
                return await _context.TbCamWorkCalendar
                        .Include(x => x.TbCamHoliday)
                        .ToListAsync();
            }
        }

        public async Task<int> Update(TbCamWorkCalendar workCalendar)
        {
            using (var _context = CreateContext(_connectionString))
            {
                _context.TbCamWorkCalendar.Update(workCalendar);
                await _context.SaveChangesAsync();
                return workCalendar.CalendarId;
            }
        }

        public bool CheckExistNameCalendarInEntity(int entityId, string calendarName, int calendarId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                return _context.TbCamWorkCalendar.Any(x => x.SpecificEntityId == entityId && x.CalendarName == calendarName && (calendarId == 0 || x.CalendarId != calendarId));
            }
        }

        public async Task<bool> CheckExistCalendarActiveInEntity(int entityId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                return await _context.TbCamWorkCalendar.AnyAsync(x => x.SpecificEntityId == entityId && x.Status == Status.ACTIVE);
            }
        }

        public async Task<bool> CheckExistLinkToEE(int calendarId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                return await _context.TbEmpMovement.AnyAsync(x => x.WorkCalendar == calendarId);
            }
        }
    }
}