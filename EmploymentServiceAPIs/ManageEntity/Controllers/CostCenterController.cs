﻿using CommonLib.Dtos;
using EmploymentServiceAPIs.ManageEntity.Dtos;
using EmploymentServiceAPIs.ManageEntity.Services.Interfaces;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.ManageEntity.Controllers
{
    [Route("api/CAM/[action]/[controller]")]
    [ApiController]
    [Authorize]
    public class CostCenterController : ControllerBase
    {
        private readonly ICostCenterService _costCenterService;
        private readonly ILogger<CostCenterController> _logger;

        public CostCenterController(ICostCenterService costCenterService, ILogger<CostCenterController> logger)
        {
            _costCenterService = costCenterService;
            _logger = logger;
        }

        [HttpGet]
        [Authorize(Roles = "CAM0701")]
        [ActionName("CAM0701")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> GetFilter([FromQuery] ApiRequest<BasePaginationRequest> request)
        {
            try
            {
                string token = await HttpContext.GetTokenAsync("access_token");
                if (request.Data == null)
                    request.Data = new BasePaginationRequest();
                var result = await _costCenterService.GetFilter(token, request);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Get cost centers which are filterd by: " + JsonConvert.SerializeObject(request));
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Get a specific cost center which id is {<paramref name="id"/>}
        /// </summary>
        /// <param name="id"> id of a cost center </param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Authorize(Roles = "CAM0701")]
        [ActionName("CAM0701")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> GetById(int id, [FromQuery] ApiRequest request)
        {
            try
            {
                var result = await _costCenterService.GetById(id, request);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Get the cost center which id is {id}. Additional parameters: {JsonConvert.SerializeObject(request)}");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Create a new cost center
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "CAM0702")]
        [ActionName("CAM0702")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> Create([FromBody] ApiRequest<CreateCostCenterRequest> request)
        {
            try
            {
                string username = User.FindFirst(ClaimTypes.Name).Value;
                request.Data.CreatedBy = username;
                request.Data.CreatedDt = DateTime.Now;
                request.Data.ModifiedBy = username;
                request.Data.ModifiedDt = DateTime.Now;

                var response = await _costCenterService.Create(request);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Update a cost center
        /// </summary>
        /// <param name="id"> id of the cost center which is updated </param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize(Roles = "CAM0703")]
        [ActionName("CAM0703")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> Update(int id, [FromBody] ApiRequest<UpdateCostCenterRequest> request)
        {
            try
            {
                request.Data.Id = id;
                string username = User.FindFirst(ClaimTypes.Name).Value;
                request.Data.ModifiedBy = username;
                request.Data.ModifiedDt = DateTime.Now;

                var response = await _costCenterService.Update(request);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}