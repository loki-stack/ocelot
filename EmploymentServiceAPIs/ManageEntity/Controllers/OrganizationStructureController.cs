﻿using CommonLib.Dtos;
using EmploymentServiceAPIs.Common.Dtos;
using EmploymentServiceAPIs.Common.Services.Interfaces;
using EmploymentServiceAPIs.ManageEntity.Dtos;
using EmploymentServiceAPIs.ManageEntity.Services.Interfaces;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.ManageEntity.Controllers
{
    [Authorize]
    [Route("api/CAM/[action]/[controller]")]
    [ApiController]
    public class OrganizationStructureController : ControllerBase
    {
        private readonly IOrganizationStructureService _organizationStructureService;
        private readonly ILogger<OrganizationStructureController> _logger;
        private readonly ITbCamClientService _clientService;
        public OrganizationStructureController(IOrganizationStructureService organizationStructureService, ITbCamClientService clientService, ILogger<OrganizationStructureController> logger)
        {
            _organizationStructureService = organizationStructureService;
            _logger = logger;
            _clientService = clientService;
        }

        [HttpGet()]
        [Authorize(Roles = "CAM0801")]
        [ActionName("CAM0801")]
        public async Task<ActionResult<ApiResponse<OrganizationStructureAllModel>>> GetAll([FromQuery] ApiRequest<PaginationRequest> request)
        {
            string token = await HttpContext.GetTokenAsync("access_token");
            var response = await _organizationStructureService.GetAll(request, request.Parameter.ClientId, request.Parameter.EntityId, token);
            return Ok(response);
        }

        [HttpGet("Create")]
        [Authorize(Roles = "CAM0801")]
        [ActionName("CAM0801")]
        public async Task<ActionResult<OrganizationStructureAddModel>> Create([FromQuery] ApiRequest request)
        {
            var response = await _organizationStructureService.GetOrganizationStructureAddInformation(request);
            return Ok(response);
        }

        [HttpGet("Edit/{unitId}")]
        [Authorize(Roles = "CAM0801")]
        [ActionName("CAM0801")]
        public async Task<IActionResult> Edit(int unitId, [FromQuery] ApiRequest request)
        {
            var response = await _organizationStructureService.GetOrganizationStructureEditInformation(unitId, request);
            return Ok(response);
        }

        [HttpPost()]
        [ActionName("CAM0802")]
        [Authorize(Roles = "CAM0802")]
        public async Task<ActionResult<ApiResponse<int>>> Store([FromBody] ApiRequest<OrganizationStructureStoreModel> request)
        {
            var resultCheck = new CommonLib.Validation.Validator(null).Validate(request.Data);
            if (resultCheck.Any())
            {
                return StatusCode(StatusCodes.Status400BadRequest, resultCheck);
            }

            request.Data.ClientId = request.Parameter.ClientId;
            request.Data.EntityId = request.Parameter.EntityId;
            request.Data.CreatedBy = User.FindFirst(ClaimTypes.Name).Value;
            var response = await _organizationStructureService.Store(request);
            return Ok(response);
        }

        [HttpPut("{id}")]
        [ActionName("CAM0803")]
        [Authorize(Roles = "CAM0803")]
        public async Task<ActionResult<ApiResponse<int>>> Update(int id, [FromBody] ApiRequest<OrganizationStructureUpdateModel> request)
        {
            var resultCheck = new CommonLib.Validation.Validator(null).Validate(request.Data);
            if (resultCheck.Any())
            {
                return StatusCode(StatusCodes.Status400BadRequest, resultCheck);
            }

            request.Data.UnitId = id;
            request.Data.ModifyBy = User.FindFirst(ClaimTypes.Name).Value;
            var response = await _organizationStructureService.Update(request);
            return Ok(response);
        }

        [HttpGet("dataaccess/{darId}")]
        [ActionName("CFG0501")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        //public async Task<ActionResult<ApiResponse<List<TreeDto<OrganizationStructureDataAccessGroupDto>>>>> GetOrganizationStructureAsTree(int darId, [FromBody]ApiRequest<DataAccessOrganizationStructureRequest> request)
        public async Task<ActionResult<ApiResponse<OrganizationStructureDataAccessGroupViewModel>>> GetOrganizationStructureAsTree(int darId, [FromQuery] ApiRequest request)
        {
            try
            {

                _logger.LogInformation("List");
                var response = new ApiResponse<OrganizationStructureDataAccessGroupViewModel>();

                // TODO
                StdRequestParam param = request.Parameter;
                //DarManagementDto data = new DarManagementDto();
                ClientResult clientInfo = await _clientService.GetClientEntityId(param.ClientId, param.EntityId);
                response = await _organizationStructureService.GetOrganizationStructureAsTree(clientInfo.ClientId, clientInfo.EntityId, clientInfo.EntityCode, darId);
                _logger.LogDebug($"-----");
                //data.AllDataAccessGroup = await _dataAccessGroupService.ListAll(clientInfo);

                //response.Data = data;

                // if (response.Data == null)
                //     return NotFound();
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

    }
}
