﻿using CommonLib.Dtos;
using EmploymentServiceAPIs.Employee.Dtos;
using EmploymentServiceAPIs.Employee.Services.Interfaces;
using EmploymentServiceAPIs.ManageEntity.Dtos;
using EmploymentServiceAPIs.ManageEntity.Dtos.WorkCalendar;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.ManageEntity.Controllers
{
    [Route("api/CAM/[action]/")]
    [ApiController]
    [Authorize]
    public class WorkCalendarController : ControllerBase
    {
        private readonly IWorkCalendarService _workCalendarService;
        private readonly ILogger<WorkCalendarController> _logger;

        public WorkCalendarController(IWorkCalendarService workCalendarService,
                                      ILogger<WorkCalendarController> logger)
        {
            _workCalendarService = workCalendarService;
            _logger = logger;
        }

        [HttpGet]
        [Authorize(Roles = "CAM1001")]
        [ActionName("CAM1001/WorkCalendar/ClientEntity")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Pagination<WorkCalendarViewModel>>>> GetFilter([FromQuery] ApiRequest<BasePaginationRequest> request)
        {
            try
            {
                if (request.Data == null)
                    request.Data = new BasePaginationRequest();
                var result = await _workCalendarService.GetFilter(request.Parameter.ClientId, request.Parameter.EntityId, request.Parameter.Locale, request.Ray, request.Data);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("{id}")]
        [Authorize(Roles = "CAM1001")]
        [ActionName("CAM1001/WorkCalendar/ClientEntity")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<WorkCalendarViewModel>>> GetById(int id, [FromQuery] ApiRequest request)
        {
            try
            {
                var result = await _workCalendarService.GetById(id, request.Ray);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "CAM1004")]
        [ActionName("CAM1004/WorkCalendar/ClientEntity")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<WorkCalendarViewModel>>> DeleteGlobalCalendar([FromRoute] int id, [FromBody] ApiRequest request)
        {
            try
            {
                var result = await _workCalendarService.DeleteWorkCalendar(id, request.Ray);
                if (result.Data == null)
                    return BadRequest(result);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        [Authorize(Roles = "CAM1006")]
        [ActionName("CAM1006/WorkCalendar/ClientEntity")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<WorkCalendarViewModel>>> Save([FromBody] ApiRequest<WorkCalendarDto> request)
        {
            try
            {
                string username = User.FindFirst(ClaimTypes.Name).Value;
                var result = await _workCalendarService.Save(username, request.Ray, request.Data);
                if (result.Data == null)
                    return BadRequest(result);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "CAM1004")]
        [ActionName("CAM1004/WorkCalendar/Holiday")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<bool?>>> DeleteHoliday([FromRoute] int id, [FromBody] ApiRequest request)
        {
            try
            {
                var result = await _workCalendarService.DeleteHoliday(id, request.Ray);
                if (result.Data == null)
                    return BadRequest(result);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}