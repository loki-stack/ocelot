﻿using CommonLib.Dtos;
using EmploymentServiceAPIs.ManageEntity.Dtos;
using EmploymentServiceAPIs.ManageEntity.Services.Interfaces;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.ManageEntity.Controllers
{
    [Route("api/CAM/[action]/[controller]")]
    [ApiController]
    [Authorize]
    public class JobGradeController : ControllerBase
    {
        private readonly IJobGradeService _jobGradeService;
        private readonly ILogger<JobGradeController> _logger;

        public JobGradeController(IJobGradeService jobGradeService, ILogger<JobGradeController> logger)
        {
            _jobGradeService = jobGradeService;
            _logger = logger;
        }

        [HttpGet]
        [Authorize(Roles = "CAM0601")]
        [ActionName("CAM0601")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> GetFilter([FromQuery] ApiRequest<BasePaginationRequest> request)
        {
            try
            {
                string token = await HttpContext.GetTokenAsync("access_token");
                if (request.Data == null)
                    request.Data = new BasePaginationRequest();
                var result = await _jobGradeService.GetFilter(token, request);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Get cost centers which are filterd by: " + JsonConvert.SerializeObject(request));
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        ///  Get a specific job grade which id is {<paramref name="id"/>}
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Authorize(Roles = "CAM0601")]
        [ActionName("CAM0601")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> GetById(int id, [FromQuery] ApiRequest request)
        {
            try
            {
                var result = await _jobGradeService.GetById(id, request);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Get the job grade which id is {id}. Additional parameters: {JsonConvert.SerializeObject(request)}");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Create a new job grade
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "CAM0602")]
        [ActionName("CAM0602")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> Create([FromBody] ApiRequest<CreateJobGradeRequest> request)
        {
            try
            {
                string username = User.FindFirst(ClaimTypes.Name).Value;
                request.Data.CreatedBy = username;
                request.Data.CreatedDt = DateTime.Now;
                request.Data.ModifiedBy = username;
                request.Data.ModifiedDt = DateTime.Now;

                var response = await _jobGradeService.Create(request);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Update a job grade
        /// </summary>
        /// <param name="id"> id of the job grade which is updated </param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize(Roles = "CAM0603")]
        [ActionName("CAM0603")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<Dictionary<string, object>>>> Update(int id, [FromBody] ApiRequest<UpdateJobGradeRequest> request)
        {
            try
            {
                request.Data.Id = id;
                string username = User.FindFirst(ClaimTypes.Name).Value;
                request.Data.ModifiedBy = username;
                request.Data.ModifiedDt = DateTime.Now;

                var response = await _jobGradeService.Update(request);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}