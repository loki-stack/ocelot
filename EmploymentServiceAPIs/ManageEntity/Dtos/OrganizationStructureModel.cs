﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EmploymentServiceAPIs.ManageEntity.Dtos
{
    public class OrganizationStructureModel
    {
        public int UnitId { get; set; }
        public int? ParentId { get; set; }
        public int ClientId { get; set; }
        public int EntityId { get; set; }
        public string CodeLevel { get; set; }
        public string FullName { get; set; }
        public string DisplayName { get; set; }
        public string UnitCode { get; set; }
        public string StatusCd { get; set; }
        [NotMapped]
        public string StatusText { get; set; }
        public string Remark { get; set; }
        public int EmployeeCount { get; set; }
    }
}
