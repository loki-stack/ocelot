﻿using CommonLib.Validation;
using EmploymentServiceAPIs.Common.Constants;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace EmploymentServiceAPIs.ManageEntity.Dtos
{
    public class OrganizationStructureUpdateModel
    {
        [JsonIgnore]
        public int UnitId { get; set; }
        public int? ParentId { get; set; }
        [MaxLength(100)]
        [Required]
        [ExtraResult(ComponentId = "FullName", Level = Level.ERROR, Placeholder = new string[] { })]
        public string FullName { get; set; }
        [MaxLength(100)]
        [Required]
        [ExtraResult(ComponentId = "DisplayName", Level = Level.ERROR, Placeholder = new string[] { })]
        public string DisplayName { get; set; }
        [MaxLength(20)]
        [Required]
        [ExtraResult(ComponentId = "UnitCode", Level = Level.ERROR, Placeholder = new string[] { })]
        public string UnitCode { get; set; }
        [RegularExpression("INACTIVE|ACTIVE", ErrorMessage = "The StatusCd must be either 'INACTIVE' or 'ACTIVE' only.")]
        [ExtraResult(ComponentId = "StatusCd", Level = Level.ERROR, Placeholder = new string[] { })]
        public string StatusCd { get; set; }
        public string Remark { get; set; }
        [JsonIgnore]
        public string ModifyBy { get; set; }
    }
}
