﻿using System.Collections.Generic;

namespace EmploymentServiceAPIs.ManageEntity.Dtos
{
    public class OrganizationStructureAllModel
    {
        public List<OrganizationStructureModel> Data { get; set; }
        public int TotalRecord { get; set; }
    }
}
