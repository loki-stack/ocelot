﻿namespace EmploymentServiceAPIs.ManageEntity.Dtos
{
    public class OrganizationStructureDisplayNameModel
    {
        public int UnitId { get; set; }
        public string DisplayName { get; set; }
    }
}
