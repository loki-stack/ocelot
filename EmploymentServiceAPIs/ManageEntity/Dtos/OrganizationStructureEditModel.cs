﻿using System.Collections.Generic;

namespace EmploymentServiceAPIs.ManageEntity.Dtos
{
    public class OrganizationStructureEditModel
    {
        public List<OrganizationStructureDisplayNameModel> OrganizationStructureDisplayName { get; set; }
        public OrganizationStructureModel OrganizationStructure { get; set; }
    }
}
