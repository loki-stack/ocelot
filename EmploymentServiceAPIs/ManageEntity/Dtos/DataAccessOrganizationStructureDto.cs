using Microsoft.EntityFrameworkCore;
namespace EmploymentServiceAPIs.ManageEntity.Dtos
{
    public class DataAccessOrganizationStructureDto
    {
        public int DataAccessGroupId { get; set; }
        public HierarchyId NodeLevel { get; set; }
        public bool IsSelectAllDescendants { get; set; }
    }
}