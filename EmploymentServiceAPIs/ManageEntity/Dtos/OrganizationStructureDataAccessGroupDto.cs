namespace EmploymentServiceAPIs.ManageEntity.Dtos
{
    public class OrganizationStructureDataAccessGroupDto
    {
        public int ItemId { get; set; }
        public string ItemType { get; set; }
        public string ItemCode { get; set; }
        public int ItemDescendantCount { get; set; }
        public string ItemText { get; set; }
        public bool IsSelected { get; set; }

        public string ItemLevel { get; set; }
        public int DataAccessGroupId { get; set; }
    }
}