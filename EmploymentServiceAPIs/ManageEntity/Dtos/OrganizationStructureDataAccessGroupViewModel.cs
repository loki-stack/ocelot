using System.Collections.Generic;
namespace EmploymentServiceAPIs.ManageEntity.Dtos
{
    public class OrganizationStructureDataAccessGroupViewModel
    {
        public bool IsAllCheck { get; set; }
        public List<TreeDto<OrganizationStructureDataAccessGroupDto>> Tree { get; set; }
    }
}