﻿using System;

namespace EmploymentServiceAPIs.ManageEntity.Dtos
{
    public class CostCenterViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Remark { get; set; }
        public string Status { get; set; }
        public string StatusTxt { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedDt { get; set; }
    }
}