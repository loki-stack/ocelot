﻿using System;
using System.Text.Json.Serialization;

namespace EmploymentServiceAPIs.ManageEntity.Dtos
{
    public class UpdateCostCenterRequest
    {
        [JsonIgnore]
        public int Id { get; set; }

        public string Name { get; set; }
        public string Code { get; set; }
        public string Remark { get; set; }
        public string Status { get; set; }

        [JsonIgnore]
        public string ModifiedBy { get; set; }

        [JsonIgnore]
        public DateTimeOffset ModifiedDt { get; set; }
    }
}