﻿using EmploymentServiceAPIs.ManageEntity.Dtos.HolidayCalendar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.ManageEntity.Dtos.WorkCalendar
{
    public class WorkCalendarViewModel
    {
        public int CalendarId { get; set; }
        public int? SpecificEntityId { get; set; }
        public string EntityName { get; set; }
        public string CalendarName { get; set; }
        public int? ParentCalendarId { get; set; }
        public string ParentName { get; set; }
        public string Status { get; set; }
        public string StatusName { get; set; }
        public string DefaultWorkingDaySunday { get; set; }
        public string DefaultWorkingDayMonday { get; set; }
        public string DefaultWorkingDayTuesday { get; set; }
        public string DefaultWorkingDayWednesday { get; set; }
        public string DefaultWorkingDayThursday { get; set; }
        public string DefaultWorkingDayFriday { get; set; }
        public string DefaultWorkingDaySaturday { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
        public bool? CalendarDay { get; set; }
        public List<HolidayViewModel> Holidays { get; set; }
    }
}