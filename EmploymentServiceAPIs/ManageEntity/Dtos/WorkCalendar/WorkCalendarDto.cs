﻿using EmploymentServiceAPIs.ManageEntity.Dtos.HolidayCalendar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.ManageEntity.Dtos.WorkCalendar
{
    public class WorkCalendarDto
    {
        public WorkCalendarModel WorkCalendar { get; set; }
        public List<HolidayModel> Holidays { get; set; }
    }
}