﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.ManageEntity.Dtos.WorkCalendar
{
    public class WorkCalendarFilter
    {
        public string Status { get; set; }
    }
}