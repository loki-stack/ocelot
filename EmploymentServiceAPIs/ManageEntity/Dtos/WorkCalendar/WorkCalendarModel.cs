﻿using CommonLib.Constants;
using CommonLib.Validation;
using EmploymentServiceAPIs.Common.Validation;
using EmploymentServiceAPIs.Employee.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.ManageEntity.Dtos.WorkCalendar
{
    public class WorkCalendarModel
    {
        public int CalendarId { get; set; }
        public int? SpecificEntityId { get; set; } = 0;

        [Required(ErrorMessage = Label.WCL0101)]
        [UniqueNameCalendarValidate(ErrorMessage = Label.WCL0125)]
        [MaxLength(200, ErrorMessage = Label.WCL0107)]
        [ExtraResult(ComponentId = Component.CALENDAR_NAME, Level = Level.ERROR, Placeholder = new string[] { })]
        public string CalendarName { get; set; }

        public int? ParentCalendarId { get; set; }

        [Required(ErrorMessage = Label.WCL0103)]
        [MaxLength(20, ErrorMessage = Label.WCL0110)]
        [ExtraResult(ComponentId = Component.STATUS, Level = Level.ERROR, Placeholder = new string[] { })]
        public string Status { get; set; }

        [Required(ErrorMessage = Label.WCL0111)]
        [MaxLength(20, ErrorMessage = Label.WCL0112)]
        [ExtraResult(ComponentId = Component.WORKING_SUNDAY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string DefaultWorkingDaySunday { get; set; }

        [Required(ErrorMessage = Label.WCL0113)]
        [MaxLength(20, ErrorMessage = Label.WCL0114)]
        [ExtraResult(ComponentId = Component.WORKING_MONDAY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string DefaultWorkingDayMonday { get; set; }

        [Required(ErrorMessage = Label.WCL0115)]
        [MaxLength(20, ErrorMessage = Label.WCL0116)]
        [ExtraResult(ComponentId = Component.WORKING_TUESDAY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string DefaultWorkingDayTuesday { get; set; }

        [Required(ErrorMessage = Label.WCL0117)]
        [MaxLength(20, ErrorMessage = Label.WCL0118)]
        [ExtraResult(ComponentId = Component.WORKING_WEDNESDAY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string DefaultWorkingDayWednesday { get; set; }

        [Required(ErrorMessage = Label.WCL0119)]
        [MaxLength(20, ErrorMessage = Label.WCL0120)]
        [ExtraResult(ComponentId = Component.WORKING_THURSDAY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string DefaultWorkingDayThursday { get; set; }

        [Required(ErrorMessage = Label.WCL0121)]
        [MaxLength(20, ErrorMessage = Label.WCL0122)]
        [ExtraResult(ComponentId = Component.WORKING_FRIDAY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string DefaultWorkingDayFriday { get; set; }

        [Required(ErrorMessage = Label.WCL0123)]
        [MaxLength(20, ErrorMessage = Label.WCL0124)]
        [ExtraResult(ComponentId = Component.WORKING_SATURDAY, Level = Level.ERROR, Placeholder = new string[] { })]
        public string DefaultWorkingDaySaturday { get; set; }

        [JsonIgnore]
        public string CreatedBy { get; set; }

        [JsonIgnore]
        public DateTimeOffset? CreatedDt { get; set; }

        [JsonIgnore]
        public string ModifiedBy { get; set; }

        [JsonIgnore]
        public DateTimeOffset? ModifiedDt { get; set; }

        public bool? CalendarDay { get; set; }
    }
}