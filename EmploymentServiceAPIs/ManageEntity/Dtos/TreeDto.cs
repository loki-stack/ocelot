using System.Collections.Generic;
namespace EmploymentServiceAPIs.ManageEntity.Dtos
{
    public class TreeDto<T>
    {
        public TreeDto()
        {
            Children = new List<TreeDto<T>>();
        }

        public string Key { get; set; }
        public T Data { get; set; }
        public List<TreeDto<T>> Children { get; set; }
        public bool IsSelectable { get; set; }

    }
}