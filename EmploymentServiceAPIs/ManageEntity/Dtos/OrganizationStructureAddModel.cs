﻿using System.Collections.Generic;

namespace EmploymentServiceAPIs.ManageEntity.Dtos
{
    public class OrganizationStructureAddModel
    {
        public List<OrganizationStructureDisplayNameModel> OrganizationStructureDisplayName { get; set; }
    }
}
