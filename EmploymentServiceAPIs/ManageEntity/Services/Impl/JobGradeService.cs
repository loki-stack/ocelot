﻿using AutoMapper;
using CommonLib.Constants;
using CommonLib.Dtos;
using CommonLib.Repositories.Interfaces;
using EmploymentServiceAPIs.Employee.Constants;
using EmploymentServiceAPIs.Employee.Dtos;
using EmploymentServiceAPIs.ManageEntity.Dtos;
using EmploymentServiceAPIs.ManageEntity.Repositories.Interfaces;
using EmploymentServiceAPIs.ManageEntity.Services.Interfaces;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.ManageEntity.Services.Impl
{
    public class JobGradeService : IJobGradeService
    {
        private readonly IJobGradeRepository _jobGradeRepository;
        private readonly IGenericCodeRepository _genericCodeRepository;
        private readonly IMapper _mapper;

        public JobGradeService(IJobGradeRepository jobGradeRepository, IMapper mapper, IGenericCodeRepository genericCodeRepository)
        {
            _jobGradeRepository = jobGradeRepository;
            _genericCodeRepository = genericCodeRepository;
            _mapper = mapper;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> Create(ApiRequest<CreateJobGradeRequest> request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new Dictionary<string, object>(),
                Message = ValidateCreateUpdate(request.Data.Name, request.Data.Remark, request.Data.Status, 0, request.Data.Level)
            };
            if (response.Message.Alert.Count > 0 || response.Message.ValidateResult.Count > 0)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.JG0101
                });
                response.Data = null;
                return response;
            }
            var jobGrade = await _jobGradeRepository.Create(request.Data);
            if (jobGrade == null)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.JG0101
                });
                response.Data = null;
                return response;
            }
            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.JG0102
            });
            response.Data.Add("jobGrade", jobGrade);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetById(int id, ApiRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new Dictionary<string, object>()
            };
            var jobGrade = await _jobGradeRepository.GetById(id);
            response.Data.Add("jobGrade", jobGrade);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetFilter(string token, ApiRequest<BasePaginationRequest> request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new Dictionary<string, object>()
            };
            var jobGrades = await _jobGradeRepository.GetFilter();
            JobGradeFilter filter = new JobGradeFilter();
            if (!string.IsNullOrEmpty(request.Data.Filter))
            {
                filter = JsonConvert.DeserializeObject<JobGradeFilter>(request.Data.Filter);
            }

            // sort by item from request
            if (!string.IsNullOrEmpty(request.Data.Sort))
            {
                string sortCharacter = request.Data.Sort.Substring(0, 1);
                string sortField = request.Data.Sort.Substring(1, request.Data.Sort.Length - 1);
                if (sortField.ToLower().Contains("name"))
                {
                    if (sortCharacter == "-")
                        jobGrades = jobGrades.OrderByDescending(x => x.Name).ToList();
                    else jobGrades = jobGrades.OrderBy(x => x.Name).ToList();
                }

                if (sortField.ToLower().Contains("level"))
                {
                    if (sortCharacter == "-")
                        jobGrades = jobGrades.OrderByDescending(x => x.Level).ToList();
                    else jobGrades = jobGrades.OrderBy(x => x.Level).ToList();
                }

                if (sortField.ToLower().Contains("status"))
                {
                    if (sortCharacter == "-")
                        jobGrades = jobGrades.OrderByDescending(x => x.Status).ToList();
                    else jobGrades = jobGrades.OrderBy(x => x.Status).ToList();
                }

                if (sortField.ToLower().Contains("remark"))
                {
                    if (sortCharacter == "-")
                        jobGrades = jobGrades.OrderByDescending(x => x.Remark).ToList();
                    else jobGrades = jobGrades.OrderBy(x => x.Remark).ToList();
                }
            }

            // search by text
            if (!string.IsNullOrEmpty(filter.Status))
            {
                jobGrades = jobGrades.Where(x => x.Status == filter.Status).ToList();
            }

            if (!string.IsNullOrEmpty(request.Data.FullTextSearch))
            {
                jobGrades = jobGrades.Where(x => x.Name.ToLower().Contains(request.Data.FullTextSearch.ToLower()))
                                         .ToList();
            }

            List<string> genericCodeList = new List<string>();
            genericCodeList.Add(Employee.Constants.GenericCode.STATUS);
            var genericCodes = await _genericCodeRepository.GetGenericCodes(genericCodeList, token, request.Parameter);
            List<GenericCodeResult> statuses = new List<GenericCodeResult>();
            if (genericCodes.ContainsKey(Employee.Constants.GenericCode.STATUS.ToLower()))
            {
                statuses = genericCodes[Employee.Constants.GenericCode.STATUS.ToLower()];
            }

            var result = (from d in jobGrades
                          join s in statuses on d.Status equals s.CodeValue into temp
                          from t in temp.DefaultIfEmpty()
                          select new JobGradeViewModel
                          {
                              Id = d.Id,
                              Name = d.Name,
                              Level = d.Level,
                              Remark = d.Remark,
                              Status = t != null ? t.CodeValue : string.Empty,
                              StatusTxt = t != null ? t.TextValue : string.Empty,
                              CreatedDt = d.CreatedDt,
                              CreatedBy = d.CreatedBy
                          }).ToList();

            Pagination<JobGradeViewModel> pagination = new Pagination<JobGradeViewModel>();
            var dataPaging = new List<JobGradeViewModel>();
            if (request.Data.Size != null)
            {
                pagination.Size = request.Data.Size.Value;
                pagination.TotalPages = (result.Count + request.Data.Size.Value - 1) / request.Data.Size.Value;
                dataPaging = result.Skip((request.Data.Page.Value - 1) * request.Data.Size.Value).Take(request.Data.Size.Value).ToList();
            }
            else
            {
                dataPaging = result;
            }
            pagination.Page = request.Data.Page.Value;
            pagination.TotalElements = result.Count;
            pagination.NumberOfElements = dataPaging.Count;
            pagination.Content = dataPaging;
            response.Data.Add("pagination", pagination);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> Update(ApiRequest<UpdateJobGradeRequest> request)
        {
            var response = new ApiResponse<Dictionary<string, object>>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new Dictionary<string, object>()
            };

            if (!_jobGradeRepository.CheckJobGradeById(request.Data.Id))
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.JG0103
                });

                response.Message.Alert.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.JG01E006
                });
                return response;
            }

            var model = await _jobGradeRepository.GetById(request.Data.Id);

            response.Message = ValidateCreateUpdate(request.Data.Name, request.Data.Remark, request.Data.Status, request.Data.Id, request.Data.Level);

            if (response.Message.Alert.Count > 0 || response.Message.ValidateResult.Count > 0)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.JG0103
                });
                response.Data = null;
                return response;
            }
            var jobGrade = await _jobGradeRepository.Update(request.Data);
            if (jobGrade == null)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.JG0103
                });
                response.Data = null;
                return response;
            }
            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.JG0104
            });
            response.Data.Add("jobGrade", jobGrade);
            return response;
        }

        private List<MessageItem> ValidateCommon(int jobGradeLevel, string jobGradeName, string remark, string status)
        {
            List<MessageItem> validateResult = new List<MessageItem>();
            if (string.IsNullOrEmpty(jobGradeName))
            {
                validateResult.Add(new MessageItem
                {
                    ComponentId = Component.NAME,
                    Level = Level.ERROR,
                    LabelCode = Label.JG01E001
                });
            }

            if (jobGradeLevel < 0 || jobGradeLevel > 999)
            {
                validateResult.Add(new MessageItem
                {
                    ComponentId = Component.LEVEL,
                    Level = Level.ERROR,
                    LabelCode = Label.JG01E002
                });
            }

            if (string.IsNullOrEmpty(status))
            {
                validateResult.Add(new MessageItem
                {
                    ComponentId = Component.STATUS,
                    Level = Level.ERROR,
                    LabelCode = Label.JG01E004
                });
            }

            return validateResult;
        }

        private Message ValidateCreateUpdate(string jobGradeName, string remark, string status, int jobGradeId, int jobGradeLevel)
        {
            var message = new Message
            {
                Toast = new List<MessageItem>(),
                Alert = new List<MessageItem>(),
                ValidateResult = new List<MessageItem>()
            };
            List<MessageItem> validateResult = ValidateCommon(jobGradeLevel, jobGradeName, remark, status);

            if (validateResult.Count > 0)
            {
                message.ValidateResult = validateResult;
                return message;
            }

            bool isExist = _jobGradeRepository.CheckExistLevel(jobGradeId, jobGradeLevel);
            if (isExist)
            {
                message.ValidateResult.Add(new MessageItem
                {
                    ComponentId = Component.LEVEL,
                    Level = Level.ERROR,
                    LabelCode = Label.JG01E005
                });
            }
            return message;
        }
    }
}