﻿using AutoMapper;
using CommonLib.Constants;
using CommonLib.Dtos;
using CommonLib.Repositories.Interfaces;
using EmploymentServiceAPIs.Employee.Constants;
using EmploymentServiceAPIs.Employee.Dtos;
using EmploymentServiceAPIs.ManageEntity.Dtos;
using EmploymentServiceAPIs.ManageEntity.Repositories.Interfaces;
using EmploymentServiceAPIs.ManageEntity.Services.Interfaces;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.ManageEntity.Services.Impl
{
    public class CostCenterService : ICostCenterService
    {
        private readonly ICostCenterRepository _costCenterRepository;
        private readonly IGenericCodeRepository _genericCodeRepository;
        private readonly IMapper _mapper;

        public CostCenterService(ICostCenterRepository costCenterRepository, IGenericCodeRepository genericCodeRepository, IMapper mapper)

        {
            _costCenterRepository = costCenterRepository;
            _genericCodeRepository = genericCodeRepository;
            _mapper = mapper;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> Create(ApiRequest<CreateCostCenterRequest> request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new Dictionary<string, object>(),
                Message = ValidateCreateUpdate(request.Data.Name, request.Data.Code, request.Data.Remark, request.Data.Status, 0)
            };
            if (response.Message.Alert.Count > 0 || response.Message.ValidateResult.Count > 0)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.CC0101
                });
                response.Data = null;
                return response;
            }
            var costCenter = await _costCenterRepository.Create(request.Data);
            if (costCenter == null)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.CC0101
                });
                response.Data = null;
                return response;
            }
            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.CC0102
            });
            response.Data.Add("costCenter", costCenter);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetById(int id, ApiRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new Dictionary<string, object>()
            };
            var costCenter = await _costCenterRepository.GetById(id);
            response.Data.Add("costCenter", costCenter);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetFilter(string token, ApiRequest<BasePaginationRequest> request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new Dictionary<string, object>()
            };
            var costCenters = await _costCenterRepository.GetFilter();
            CostCenterFilter filter = new CostCenterFilter();
            if (!string.IsNullOrEmpty(request.Data.Filter))
            {
                filter = JsonConvert.DeserializeObject<CostCenterFilter>(request.Data.Filter);
            }

            // sort by item from request
            if (!string.IsNullOrEmpty(request.Data.Sort))
            {
                string sortCharacter = request.Data.Sort.Substring(0, 1);
                string sortField = request.Data.Sort.Substring(1, request.Data.Sort.Length - 1);
                if (sortField.ToLower().Contains("name"))
                {
                    if (sortCharacter == "-")
                        costCenters = costCenters.OrderByDescending(x => x.Name).ToList();
                    else costCenters = costCenters.OrderBy(x => x.Name).ToList();
                }

                if (sortField.ToLower().Contains("code"))
                {
                    if (sortCharacter == "-")
                        costCenters = costCenters.OrderByDescending(x => x.Code).ToList();
                    else costCenters = costCenters.OrderBy(x => x.Code).ToList();
                }

                if (sortField.ToLower().Contains("status"))
                {
                    if (sortCharacter == "-")
                        costCenters = costCenters.OrderByDescending(x => x.Status).ToList();
                    else costCenters = costCenters.OrderBy(x => x.Status).ToList();
                }

                if (sortField.ToLower().Contains("remark"))
                {
                    if (sortCharacter == "-")
                        costCenters = costCenters.OrderByDescending(x => x.Remark).ToList();
                    else costCenters = costCenters.OrderBy(x => x.Remark).ToList();
                }
            }

            // search by text
            if (!string.IsNullOrEmpty(filter.Status))
            {
                costCenters = costCenters.Where(x => x.Status == filter.Status).ToList();
            }

            if (!string.IsNullOrEmpty(request.Data.FullTextSearch))
            {
                costCenters = costCenters.Where(x => x.Code.ToLower().Contains(request.Data.FullTextSearch.ToLower())
                                                    || x.Name.ToLower().Contains(request.Data.FullTextSearch.ToLower()))
                                         .ToList();
            }

            List<string> genericCodeList = new List<string>();
            genericCodeList.Add(Employee.Constants.GenericCode.STATUS);
            var genericCodes = await _genericCodeRepository.GetGenericCodes(genericCodeList, token, request.Parameter);
            List<GenericCodeResult> statuses = new List<GenericCodeResult>();
            if (genericCodes.ContainsKey(Employee.Constants.GenericCode.STATUS.ToLower()))
            {
                statuses = genericCodes[Employee.Constants.GenericCode.STATUS.ToLower()];
            }

            var result = (from d in costCenters
                          join s in statuses on d.Status equals s.CodeValue into temp
                          from t in temp.DefaultIfEmpty()
                          select new CostCenterViewModel
                          {
                              Id = d.Id,
                              Name = d.Name,
                              Code = d.Code,
                              Remark = d.Remark,
                              Status = t != null ? t.CodeValue : string.Empty,
                              StatusTxt = t != null ? t.TextValue : string.Empty,
                              CreatedDt = d.CreatedDt,
                              CreatedBy = d.CreatedBy
                          }).ToList();

            Pagination<CostCenterViewModel> pagination = new Pagination<CostCenterViewModel>();
            var dataPaging = new List<CostCenterViewModel>();
            if (request.Data.Size != null)
            {
                pagination.Size = request.Data.Size.Value;
                pagination.TotalPages = (result.Count + request.Data.Size.Value - 1) / request.Data.Size.Value;
                dataPaging = result.Skip((request.Data.Page.Value - 1) * request.Data.Size.Value).Take(request.Data.Size.Value).ToList();
            }
            else
            {
                dataPaging = result;
            }

            pagination.Page = request.Data.Page.Value;
            pagination.TotalElements = result.Count;
            pagination.NumberOfElements = dataPaging.Count;
            pagination.Content = dataPaging;
            response.Data.Add("pagination", pagination);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> Update(ApiRequest<UpdateCostCenterRequest> request)
        {
            var response = new ApiResponse<Dictionary<string, object>>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new Dictionary<string, object>()
            };

            if (!_costCenterRepository.CheckCostCenterById(request.Data.Id))
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.CC0103
                });

                response.Message.Alert.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.CC01E007
                });
                response.Data = null;
                return response;
            }

            var viewModel = await _costCenterRepository.GetById(request.Data.Id);

            response.Message = ValidateCreateUpdate(request.Data.Name, request.Data.Code, request.Data.Remark, request.Data.Status, request.Data.Id);

            if (response.Message.Alert.Count > 0 || response.Message.ValidateResult.Count > 0)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.CC0103
                });
                response.Data = null;
                return response;
            }
            var costCenter = await _costCenterRepository.Update(request.Data);
            if (costCenter == null)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.CC0103
                });
                response.Data = null;
                return response;
            }
            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.CC0104
            });
            response.Data.Add("costCenter", costCenter);
            return response;
        }

        private List<MessageItem> ValidateCommon(string costCenterName, string costCenterCode, string remark, string status)
        {
            List<MessageItem> validateResult = new List<MessageItem>();
            if (string.IsNullOrEmpty(costCenterName))
            {
                validateResult.Add(new MessageItem
                {
                    ComponentId = Component.NAME,
                    Level = Level.ERROR,
                    LabelCode = Label.CC01E001
                });
            }

            if (string.IsNullOrEmpty(costCenterCode))
            {
                validateResult.Add(new MessageItem
                {
                    ComponentId = Component.CODE,
                    Level = Level.ERROR,
                    LabelCode = Label.CC01E002
                });
            }

            if (string.IsNullOrEmpty(status))
            {
                validateResult.Add(new MessageItem
                {
                    ComponentId = Component.STATUS,
                    Level = Level.ERROR,
                    LabelCode = Label.CC01E006
                });
            }

            return validateResult;
        }

        private Message ValidateCreateUpdate(string costCenterName, string costCenterCode, string remark, string status, int costId)
        {
            var message = new Message
            {
                Toast = new List<MessageItem>(),
                Alert = new List<MessageItem>(),
                ValidateResult = new List<MessageItem>()
            };
            List<MessageItem> validateResult = ValidateCommon(costCenterName, costCenterCode, remark, status);

            if (validateResult.Count > 0)
            {
                message.ValidateResult = validateResult;
                return message;
            }

            bool isExist = _costCenterRepository.CheckExistCode(costId, costCenterCode);
            if (isExist)
            {
                message.ValidateResult.Add(new MessageItem
                {
                    ComponentId = Component.CODE,
                    Level = Level.ERROR,
                    LabelCode = Label.CC01E008
                });
            }
            return message;
        }
    }
}