﻿using CommonLib.Dtos;
using CommonLib.Repositories.Interfaces;
using EmploymentServiceAPIs.Common.Constants;
using EmploymentServiceAPIs.Common.Dtos;
using EmploymentServiceAPIs.Common.Repositories.Interfaces;
using EmploymentServiceAPIs.ManageEntity.Dtos;
using EmploymentServiceAPIs.ManageEntity.Repositories.Interfaces;
using EmploymentServiceAPIs.ManageEntity.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.ManageEntity.Services.Impl
{
    public class OrganizationStructureService : IOrganizationStructureService
    {
        private readonly IOrganizationStructureRepository _organizationStructureRepository;
        private readonly ITbSecDataAccessOrganizationStructureRepository _dataAccessOrganizationStructureRepository;
        private readonly IGenericCodeRepository _genericCodeRepository;
        public OrganizationStructureService(ITbSecDataAccessOrganizationStructureRepository dataAccessOrganizationStructureRepository, IOrganizationStructureRepository organizationStructureRepository,
            IGenericCodeRepository genericCodeRepository)
        {
            _organizationStructureRepository = organizationStructureRepository;
            _dataAccessOrganizationStructureRepository = dataAccessOrganizationStructureRepository;
            _genericCodeRepository = genericCodeRepository;
        }

        /// <summary>
        /// Get All Organization Structure
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<OrganizationStructureAllModel>> GetAll(ApiRequest<PaginationRequest> request, int clientId, int entityId, string token)
        {
            ApiResponse<OrganizationStructureAllModel> response = new ApiResponse<OrganizationStructureAllModel>()
            {
                Ray = request.Ray ?? string.Empty,
                Data = new OrganizationStructureAllModel()
            };

            if (request.Data == null)
            {
                request.Data = new PaginationRequest();
            }

            if (request.Data.Page <= 0)
                request.Data.Page = 1;
            if (request.Data.Size <= 0)
                request.Data.Page = 10;
            if (string.IsNullOrEmpty(request.Data.Sort))
                request.Data.Sort = "CODE_LEVEL";

            response.Data = await _organizationStructureRepository.GetAll(request.Data, clientId, entityId);

            if (response.Data.Data != null)
            {
                List<string> genericCodeList = new List<string>();
                genericCodeList.Add(Employee.Constants.GenericCode.STATUS);
                var genericCodes = await _genericCodeRepository.GetGenericCodes(genericCodeList, token, request.Parameter);
                List<GenericCodeResult> statuses = new List<GenericCodeResult>();
                if (genericCodes.ContainsKey(Employee.Constants.GenericCode.STATUS.ToLower()))
                {
                    statuses = genericCodes[Employee.Constants.GenericCode.STATUS.ToLower()];
                }

                foreach (var item in response.Data.Data)
                {
                    if (statuses.Any(n => n.CodeValue == item.StatusCd))
                    {
                        item.StatusText = statuses.First(n => n.CodeValue == item.StatusCd).TextValue;
                    }
                }
            }

            return response;
        }

        /// <summary>
        /// Get All Organization Structure in Tree Format
        /// </summary>
        /// <param name="clientId">clientId</param>
        /// <param name="entityId">entityId</param>
        /// <param name="entityCode">entityCode</param>
        /// <param name="darId">darId</param>
        /// <returns>OrganizationStructureAllModel</returns>
        public async Task<ApiResponse<OrganizationStructureDataAccessGroupViewModel>> GetOrganizationStructureAsTree(int clientId, int entityId, string entityCode, int darId)
        {
            ApiResponse<OrganizationStructureDataAccessGroupViewModel> result = new ApiResponse<OrganizationStructureDataAccessGroupViewModel>();
            List<DataAccessOrganizationStructureDto> daOrgStruct = await _dataAccessOrganizationStructureRepository.GetById(darId);
            if (daOrgStruct == null)
            {
                result.Data = null;
                result.Message.Toast.Add(new MessageItem()
                {
                    Level = Level.ERROR,
                    LabelCode = Label.OS0101,
                    Placeholder = new string[] { "OS0107", "" },
                });
            }

            List<TreeDto<OrganizationStructureDataAccessGroupDto>> tree = await _organizationStructureRepository.GetOrganizationStructureAsTree(clientId, entityId, entityCode, darId, daOrgStruct);
            if (tree == null)
            {
                result.Data = null;
                result.Message.Toast.Add(new MessageItem()
                {
                    Level = Level.ERROR,
                    LabelCode = Label.OS0107,
                    Placeholder = new string[] { "OS0107", "" },
                });
            }

            bool isAllCheck = await _dataAccessOrganizationStructureRepository.IsAllCheck(darId);
            OrganizationStructureDataAccessGroupViewModel data = new OrganizationStructureDataAccessGroupViewModel();
            data.Tree = tree;
            data.IsAllCheck = isAllCheck;
            result.Data = data;
            return result;
        }

        /// <summary>
        /// Create Organization Structure
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<int>> Store(ApiRequest<OrganizationStructureStoreModel> request)
        {
            ApiResponse<int> result = new ApiResponse<int>()
            {
                Ray = request.Ray ?? string.Empty,
                Data = 0
            };
            int? newId = await _organizationStructureRepository.Create(request.Data);
            if (!newId.HasValue)
            {
                result.Message.Toast.Add(new MessageItem()
                {
                    Level = Level.ERROR,
                    LabelCode = Label.OS0104
                });
            }
            else
            {
                result.Data = newId.Value;
                result.Message.Toast.Add(new MessageItem()
                {
                    Level = Level.SUCCESS,
                    LabelCode = Label.OS0103
                });
            }
            return result;
        }

        /// <summary>
        /// Update Organization Structure
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<int>> Update(ApiRequest<OrganizationStructureUpdateModel> request)
        {
            ApiResponse<int> result = new ApiResponse<int>()
            {
                Ray = request.Ray ?? string.Empty,
                Data = 0
            };
            OrganizationStructureModel objOrganizationStructure = await _organizationStructureRepository.GetOrganizationStructureById(request.Data.UnitId);

            // check exits
            if (objOrganizationStructure == null)
            {
                result.Message.Toast.Add(new MessageItem()
                {
                    Level = Level.ERROR,
                    LabelCode = Label.OS0101
                });
                return result;
            }

            // check change to inactive
            if (objOrganizationStructure.StatusCd != request.Data.StatusCd && request.Data.StatusCd == UnitStatusCode.INACTIVE && objOrganizationStructure.EmployeeCount > 0)
            {
                result.Message.Toast.Add(new MessageItem()
                {
                    Level = Level.ERROR,
                    LabelCode = Label.OS0102
                });
                return result;
            }

            int? updateId = await _organizationStructureRepository.Update(request.Data);
            if (!updateId.HasValue)
            {
                result.Message.Toast.Add(new MessageItem()
                {
                    Level = Level.ERROR,
                    LabelCode = Label.OS0105
                });
                return result;
            }
            result.Data = request.Data.UnitId;
            result.Message.Toast.Add(new MessageItem()
            {
                Level = Level.SUCCESS,
                LabelCode = Label.OS0106
            });
            return result;
        }

        /// <summary>
        /// Get Organization Structure Edit Information 
        /// </summary>
        /// <param name="unitId"></param>
        /// param name="request">
        /// <returns></returns>
        public async Task<ApiResponse<OrganizationStructureEditModel>> GetOrganizationStructureEditInformation(int unitId, ApiRequest request)
        {
            ApiResponse<OrganizationStructureEditModel> response = new ApiResponse<OrganizationStructureEditModel>()
            {
                Ray = request.Ray ?? string.Empty,
                Data = new OrganizationStructureEditModel()
            };
            response.Data.OrganizationStructureDisplayName = await _organizationStructureRepository.GetOrganizationStructureDisplayName(unitId);
            response.Data.OrganizationStructure = await _organizationStructureRepository.GetOrganizationStructureById(unitId);

            return response;
        }

        /// <summary>
        /// Get Organization Structure Add Information 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<OrganizationStructureAddModel>> GetOrganizationStructureAddInformation(ApiRequest request)
        {
            ApiResponse<OrganizationStructureAddModel> response = new ApiResponse<OrganizationStructureAddModel>()
            {
                Ray = request.Ray ?? string.Empty,
                Data = new OrganizationStructureAddModel()
            };
            response.Data.OrganizationStructureDisplayName = await _organizationStructureRepository.GetOrganizationStructureDisplayName(null);
            return response;
        }
    }
}
