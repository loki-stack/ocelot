﻿using CommonLib.Dtos;
using EmploymentServiceAPIs.ManageEntity.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.ManageEntity.Services.Interfaces
{
    public interface ICostCenterService
    {
        Task<ApiResponse<Dictionary<string, object>>> GetFilter(string token, ApiRequest<BasePaginationRequest> request);

        Task<ApiResponse<Dictionary<string, object>>> Create(ApiRequest<CreateCostCenterRequest> request);

        Task<ApiResponse<Dictionary<string, object>>> Update(ApiRequest<UpdateCostCenterRequest> request);

        Task<ApiResponse<Dictionary<string, object>>> GetById(int id, ApiRequest request);
    }
}