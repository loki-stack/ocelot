﻿using CommonLib.Dtos;
using EmploymentServiceAPIs.ManageEntity.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.ManageEntity.Services.Interfaces
{
    public interface IJobGradeService
    {
        Task<ApiResponse<Dictionary<string, object>>> GetFilter(string token, ApiRequest<BasePaginationRequest> request);

        Task<ApiResponse<Dictionary<string, object>>> Create(ApiRequest<CreateJobGradeRequest> request);

        Task<ApiResponse<Dictionary<string, object>>> Update(ApiRequest<UpdateJobGradeRequest> request);

        Task<ApiResponse<Dictionary<string, object>>> GetById(int id, ApiRequest request);
    }
}