﻿using CommonLib.Dtos;
using EmploymentServiceAPIs.Common.Dtos;
using EmploymentServiceAPIs.ManageEntity.Dtos;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.ManageEntity.Services.Interfaces
{
    public interface IOrganizationStructureService
    {
        Task<ApiResponse<OrganizationStructureAllModel>> GetAll(ApiRequest<PaginationRequest> request, int clientId, int entityId, string token);
        Task<ApiResponse<int>> Store(ApiRequest<OrganizationStructureStoreModel> request);
        Task<ApiResponse<int>> Update(ApiRequest<OrganizationStructureUpdateModel> request);
        Task<ApiResponse<OrganizationStructureEditModel>> GetOrganizationStructureEditInformation(int unitId, ApiRequest request);
        Task<ApiResponse<OrganizationStructureAddModel>> GetOrganizationStructureAddInformation(ApiRequest request);
        Task<ApiResponse<OrganizationStructureDataAccessGroupViewModel>> GetOrganizationStructureAsTree(int clientId, int entityId, string entityCode, int darId);
    }
}
