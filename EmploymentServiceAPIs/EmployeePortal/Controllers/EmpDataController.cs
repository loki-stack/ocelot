using CommonLib.Dtos;
using EmploymentServiceAPIs.Employee.Dtos.BankAccount;
using EmploymentServiceAPIs.Employee.Dtos.Dependent;
using EmploymentServiceAPIs.Employee.Dtos.EmploymentContract;
using EmploymentServiceAPIs.Employee.Dtos.EmploymentStatus;
using EmploymentServiceAPIs.Employee.Dtos.Movement;
using EmploymentServiceAPIs.Employee.Services.Interfaces;
using EmploymentServiceAPIs.EmployeePortal.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.EmployeePortal.Controllers
{
    /// <summary>
    /// For processing employee data for ESS portal
    /// </summary>
    [ApiController]
    [Route("api/EMP/[action]/[controller]")]
    [Authorize]
    public class EmpDataController : ControllerBase
    {
        private readonly ILogger<EmpDataController> _logger;
        private readonly IEmploymentContractService _employmentContractService;
        private readonly IEmpDataService _empDataService;
        private readonly IEmployeeService _employeeService;
        private readonly IDependentService _dependentService;
        private readonly IBankAccountService _bankAccountService;
        private readonly IEmploymentStatusService _employmentStatusService;
        private readonly ITaxService _taxService;
        private readonly IRentalAddressService _rentalAddressService;
        private readonly IMovementService _movementService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="empDataService"></param>
        /// <param name="dependentService"></param>
        /// <param name="employeeService"></param>
        /// <param name="bankAccountService"></param>
        /// <param name="employmentStatusService"></param>
        /// <param name="rentalAddressService"></param>
        /// <param name="taxService"></param>
        /// <param name="employmentContractService"></param>
        /// <param name="movementService"></param>
        public EmpDataController(ILogger<EmpDataController> logger,
        IEmpDataService empDataService,
        IDependentService dependentService,
        IBankAccountService bankAccountService,
        IEmploymentStatusService employmentStatusService,
        IEmploymentContractService employmentContractService,
        IRentalAddressService rentalAddressService,
        IMovementService movementService,
        ITaxService taxService,
        IEmployeeService employeeService)
        {
            this._logger = logger;
            _dependentService = dependentService;
            this._empDataService = empDataService;
            _employeeService = employeeService;
            _movementService = movementService;
            _bankAccountService = bankAccountService;
            _employmentStatusService = employmentStatusService;
            _rentalAddressService = rentalAddressService;
            _employmentContractService = employmentContractService;
            _taxService = taxService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private int GetEdIdFromClaims()
        {
            var eeId = User.FindFirstValue("EmployeeId");
            _logger.LogInformation($"Employee ID from claim---{eeId}");
            if (eeId == null)
            {
                return -1;
            }

            return Int32.Parse(eeId);
        }

        /// <summary>
        /// Get employee's brief profile information
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet()]
        [ActionName("EMP5001/Employees/profile")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Profile([FromQuery] ApiRequest request)
        {
            try
            {
                int eeId = GetEdIdFromClaims();
                _logger.LogInformation($"GetEmployeeProfile --- EmployeeId: {eeId}");
                if (eeId < 0)
                {
                    return BadRequest();
                }

                //var response = await _empDataService.GetEmployeeProfile(request.Ray, employeeId, request.Parameter.Locale);
                var response = await _empDataService.GetEmployeeProfile(request.Ray, eeId, request.Parameter.Locale);
                if (response.Data == null)
                    return NotFound();
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        #region PaySlip

        /// <summary>
        /// Get employee's all payslips' records
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet()]
        [ActionName("EMP5001/Employees/payslips")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAllPayslips([FromQuery] ApiRequest request)
        {
            try
            {
                int eeId = GetEdIdFromClaims();
                _logger.LogInformation($"GetAllPayslips --- EmployeeId: {eeId}");
                if (eeId < 0)
                {
                    return BadRequest();
                }

                var response = await _empDataService.GetAllPayslips(request.Ray, eeId);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        #endregion PaySlip

        #region Basic Info

        /// <summary>
        /// Get employee's full basic information
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>

        [HttpGet()]
        [Authorize]
        [ActionName("EMP5001/Employees/BasicInfo")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetBasicInfo([FromQuery] ApiRequest request)
        {
            try
            {
                int eeId = GetEdIdFromClaims();
                _logger.LogInformation($"GetBasicInfo --- EmployeeId: {eeId}");
                if (eeId < 0)
                {
                    return BadRequest();
                }
                var response = await _employeeService.GetBasicInfo(eeId, request);
                if (response == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        #endregion Basic Info

        #region Dependent

        /// <summary>
        /// Get employee's full dependent information
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>

        [HttpGet]
        [Authorize]
        [ActionName("EMP5001/Employees/Dependent")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<List<DependentViewModel>>>> GetDependentByEmployeeId([FromQuery] ApiRequest request)
        {
            try
            {
                int eeId = GetEdIdFromClaims();
                _logger.LogInformation($"GetDependent --- ByEmployeeId: {eeId}");
                if (eeId < 0)
                {
                    return BadRequest();
                }
                var result = await _dependentService.GetByEmployeeId(request.Parameter.ClientId, request.Parameter.EntityId, eeId, request.Ray);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        #endregion Dependent

        #region Bank Account

        /// <summary>
        /// Get employee's full bank account information
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>

        [HttpGet]
        [Authorize]
        [ActionName("EMP5001/Employees/BankAccount")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<BankAccountDto>>> GetBankAccountByEmployeeId([FromQuery] ApiRequest request)
        {
            try
            {
                int eeId = GetEdIdFromClaims();
                _logger.LogInformation($"GetBankAccount --- ByEmployeeId: {eeId}");
                if (eeId < 0)
                {
                    return BadRequest();
                }
                var result = await _bankAccountService.GetByEmployeeId(eeId, request.Ray);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        #endregion Bank Account

        #region Employment Status

        /// <summary>
        /// Get employee's full employment status
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>

        [HttpGet]
        [Authorize]
        [ActionName("EMP5001/Employees/EmploymentStatus")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<List<EmploymentStatusDto>>>> GetEmploymentStatusByEmployeeId([FromQuery] ApiRequest request)
        {
            try
            {
                int eeId = GetEdIdFromClaims();
                _logger.LogInformation($"GetEmploymentStatus --- ByEmployeeId: {eeId}");
                if (eeId < 0)
                {
                    return BadRequest();
                }
                var result = await _employmentStatusService.GetByEmployeeId(eeId, request.Ray);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        #endregion Employment Status

        #region Rental Address

        /// <summary>
        /// Get employee's full rental address
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>

        [HttpGet]
        [Authorize]
        [ActionName("EMP5001/Employees/RentalAddress")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetRentalAddress([FromQuery] ApiRequest request)
        {
            try
            {
                int eeId = GetEdIdFromClaims();
                _logger.LogInformation($"GetRentalAddress --- EmployeeId: {eeId}");
                if (eeId < 0)
                {
                    return BadRequest();
                }
                var response = await _rentalAddressService.GetRentalAddress(eeId, request);
                if (response == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        #endregion Rental Address

        #region Tax

        /// <summary>
        /// Get employee's full tax
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>

        [HttpGet]
        [Authorize]
        [ActionName("EMP5001/Employees/Tax")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetTaxInfo([FromQuery] ApiRequest request)
        {
            try
            {
                int eeId = GetEdIdFromClaims();
                _logger.LogInformation($"GetTaxInfo --- EmployeeId: {eeId}");
                if (eeId < 0)
                {
                    return BadRequest();
                }
                var response = await _taxService.GetTaxInfo(eeId, request);
                if (response == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        #endregion Tax

        #region TaxReturn

        /// <summary>
        /// Get employee's full tax return
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet()]
        [ActionName("EMP5001/Employees/TaxReturn")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAllTaxReturn([FromQuery] ApiRequest request)
        {
            try
            {
                int eeId = GetEdIdFromClaims();
                _logger.LogInformation($"GetAllTaxReturn --- EmployeeId: {eeId}");
                if (eeId < 0)
                {
                    return BadRequest();
                }

                var response = await _empDataService.GetAllTaxReturn(request.Ray, eeId);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        #endregion TaxReturn

        #region Employment Contract

        /// <summary>
        /// Get employee's contract
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet()]
        [ActionName("EMP5001/Employees/Contract")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<List<EmploymentContractViewModel>>>> GetEmploymentContractByEmployeeId([FromQuery] ApiRequest request)
        {
            try
            {
                int eeId = GetEdIdFromClaims();
                _logger.LogInformation($"GetEmploymentContract --- ByEmployeeId: {eeId}");
                if (eeId < 0)
                {
                    return BadRequest();
                }
                var result = await _employmentContractService.GetByEmployeeId(eeId, request.Ray);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        #endregion Employment Contract

        #region Movement

        /// <summary>
        /// Get employee's movement
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet()]
        [ActionName("EMP5001/Employees/Movement")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ApiResponse<List<MovementViewModel>>>> GetMovementByEmploymentId([FromQuery] ApiRequest request)
        {
            try
            {
                int eeId = GetEdIdFromClaims();
                _logger.LogInformation($"GetMovement --- ByEmployeeId: {eeId}");
                if (eeId < 0)
                {
                    return BadRequest();
                }
                var result = await _movementService.GetByEmploymentId(eeId, request.Ray);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        #endregion Movement

    }
}