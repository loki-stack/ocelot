using CommonLib.Dtos;
using EmploymentServiceAPIs.Employee.Services.Interfaces;
using EmploymentServiceAPIs.EmployeePortal.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Security.Claims;
using System.Threading.Tasks;


namespace EmploymentServiceAPIs.EmployeePortal.Controllers
{
    [Route("api/EMP/[action]/[controller]")]
    [ApiController]
    [Authorize]
    public class EmpPortalFileController : ControllerBase
    {
        private readonly IFileService _manageFileService;
        private readonly IEmpDataService _empDataService;
        private readonly ILogger<EmpPortalFileController> _logger;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="manageFileService"></param>
        /// <param name="empDataService"></param>
        /// <param name="logger"></param>
        public EmpPortalFileController(
            IFileService manageFileService,
            IEmpDataService empDataService,
            ILogger<EmpPortalFileController> logger)
        {
            _manageFileService = manageFileService;
            this._empDataService = empDataService;
            _logger = logger;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private int GetEdIdFromClaims()
        {
            var eeId = User.FindFirstValue("EmployeeId");
            _logger.LogInformation($"Employee ID from claim---{eeId}");
            if (eeId == null)
            {
                return -1;
            }

            return Int32.Parse(eeId);
        }


        #region payslip
        /// <summary>
        ///  Download payslip file
        /// </summary>
        /// <param name="request"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet()]
        [ActionName("PAY5001/Files/payslip/{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DownloadFile([FromRoute] int id, [FromQuery] ApiRequest request)
        {
            try
            {
                int eeId = GetEdIdFromClaims();
                if (eeId < 0)
                {
                    return BadRequest();
                }
                var requestedfileIds = new List<int>();
                requestedfileIds.Add(id);
                var valid = await _empDataService.ValidatePaySlipByFileIds(requestedfileIds, eeId, request.Ray);
                if (valid.Data.Count == 0)
                {
                    return BadRequest(valid);
                }

                var result = await _manageFileService.DownloadFile(id, request.Ray);
                if (result != null && result.Data != null)
                {
                    Response.Headers["Access-Control-Expose-Headers"] = "Content-Disposition,X-Suggested-Filename";
                    Response.Headers["X-Suggested-Filename"] = result.Data.FileName;
                    return File(result.Data.FileContent, "application/octet-stream");
                }
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }


        /// <summary>
        ///  Download payslip zip file
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet()]
        [ActionName("PAY5001/Files/payslip/zip")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> GetFilesAsZip([FromQuery] ApiRequest<List<int>> request)
        {
            try
            {
                int eeId = GetEdIdFromClaims();
                if (eeId < 0)
                {
                    return BadRequest();
                }
                var valid = await _empDataService.ValidatePaySlipByFileIds(request.Data, eeId, request.Ray);
                if (valid.Data.Count == 0)
                {
                    return BadRequest(valid);
                }

                using (MemoryStream ms = new MemoryStream())
                {
                    using (var archive = new ZipArchive(ms, ZipArchiveMode.Create, true))
                    {
                        int employeeId = eeId; // TODO - get from JWT token

                        await _manageFileService.GetFilesAsZip(request.Parameter.ClientId, request.Parameter.EntityId,
                            employeeId, archive, valid.Data);
                    }
                    Response.Headers["Access-Control-Expose-Headers"] = "Content-Disposition,X-Suggested-Filename";
                    Response.Headers["X-Suggested-Filename"] = "payslip_" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + ".zip";
                    return File(ms.ToArray(), "application/zip");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        #endregion payslip

        #region taxreturn

        /// <summary>
        ///  Download tax return file
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet()]
        [ActionName("PAY5101/Files/taxreturn/{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DownloadTaxReturnFile([FromRoute] int id, [FromQuery] ApiRequest request)
        {
            try
            {
                int eeId = GetEdIdFromClaims();
                if (eeId < 0)
                {
                    return BadRequest();
                }
                var requestedfileIds = new List<int>();
                requestedfileIds.Add(id);
                var valid = await _empDataService.ValidateTaxReturnByFileIds(requestedfileIds, eeId, request.Ray);
                if (valid.Data.Count == 0)
                {
                    return BadRequest(valid);
                }

                var result = await _manageFileService.DownloadFile(id, request.Ray);
                if (result != null && result.Data != null)
                {
                    Response.Headers["Access-Control-Expose-Headers"] = "Content-Disposition,X-Suggested-Filename";
                    Response.Headers["X-Suggested-Filename"] = result.Data.FileName;
                    return File(result.Data.FileContent, "application/octet-stream");
                }
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        ///  Download tax return zip file
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet()]
        [ActionName("PAY5101/Files/taxreturn/zip")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> GetTaxReturnFilesAsZip([FromQuery] ApiRequest<List<int>> request)
        {
            try
            {
                int eeId = GetEdIdFromClaims();
                if (eeId < 0)
                {
                    return BadRequest();
                }

                var valid = await _empDataService.ValidateTaxReturnByFileIds(request.Data, eeId, request.Ray);
                if (valid.Data.Count == 0)
                {
                    return BadRequest(valid);
                }

                using (MemoryStream ms = new MemoryStream())
                {
                    using (var archive = new ZipArchive(ms, ZipArchiveMode.Create, true))
                    {
                        int employeeId = eeId; // TODO - get from JWT token

                        await _manageFileService.GetFilesAsZip(request.Parameter.ClientId, request.Parameter.EntityId,
                            employeeId, archive, valid.Data);
                    }
                    Response.Headers["Access-Control-Expose-Headers"] = "Content-Disposition,X-Suggested-Filename";
                    Response.Headers["X-Suggested-Filename"] = "taxreturn_" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + ".zip";
                    return File(ms.ToArray(), "application/zip");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        #endregion taxreturn
    }
}