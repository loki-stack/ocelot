using CommonLib.Dtos;
using EmploymentServiceAPIs.EmployeePortal.Dtos;
using EmploymentServiceAPIs.EmployeePortal.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.EmployeePortal.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/EMP/")]
    [ApiController]
    [Authorize]
    public class EmpPortalConfigController : ControllerBase
    {
        private readonly ILogger<EmpPortalConfigController> _logger;

        private readonly IEntityConfigService _entityConfigService;
        private readonly SmtpSettings _smtpSettings;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="entityConfigService"></param>
        /// <param name="smtp"></param>
        public EmpPortalConfigController(ILogger<EmpPortalConfigController> logger,
        IEntityConfigService entityConfigService,
        IOptions<SmtpSettings> smtp
        )
        {
            this._logger = logger;
            this._entityConfigService = entityConfigService;
            _smtpSettings = smtp.Value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityCode"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet("EMP5001/Config/{entityCode}")]
        [AllowAnonymous]
        //[ActionName("EMP5001/Config/{entityCode}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetConfigInfo([FromRoute] string entityCode, [FromQuery] ApiRequest request)
        {
            try
            {
                _logger.LogInformation($"GetConfigInfo --- EntityCode: {entityCode}");
                ApiResponse<EntityConfigDto> response = new ApiResponse<EntityConfigDto>();
                var data = await _entityConfigService.GetConfig(entityCode);
                if (data == null)
                {
                    return BadRequest(response);
                }

                response.Data = data;

                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Get Support
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet("EMP5001/Support")]
        [AllowAnonymous]
        //[ActionName("EMP5001/Config/{entityCode}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetSupport([FromQuery] ApiRequest request)
        {
            try
            {
                _logger.LogInformation($"GetSupport --- EntityId: {request.Parameter.EntityId}");
                var data = await _entityConfigService.GetSupport(request.Parameter.EntityId, request.Ray);
                return Ok(data);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }

        /// <summary>
        /// Send Email
        /// </summary>
        /// <param name="request"></param>
        /// <param name="EntityId"></param>
        /// <returns></returns>
        [HttpGet("EMP5001/SendEmail")]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task SendEmail(int EntityId, [FromQuery] ApiRequest request)
        {
            _logger.LogInformation($"SendEmail --- EntityId: {EntityId}");
            var smtpClient = new SmtpClient(_smtpSettings.Host)
            {
                Port = _smtpSettings.Port,
                Credentials = new NetworkCredential(_smtpSettings.Username, _smtpSettings.Password),
                EnableSsl = true,
            };

            var mailMessage = new MailMessage
            {
                From = new MailAddress("chertest1198@gmail.com"),
                Subject = "Send Email Test",
                Body = "<h4>This is body. Test test.</h4>",
                IsBodyHtml = true,
            };
            mailMessage.To.Add("chertest1198@gmail.com");

            await smtpClient.SendMailAsync(mailMessage);
        }

    }

}