using CommonLib.Dtos;
using EmploymentServiceAPIs.EmployeePortal.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.EmployeePortal.Services.Interfaces
{
    /// <summary>
    /// For getting Employee Data
    /// </summary>
    public interface IEmpDataService
    {
        /// <summary>
        /// Get basic employee information
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        Task<EmpDataDto> GetInfo(int employeeId);

        /// <summary>
        /// Get basic employee profile
        /// </summary>
        /// <param name="ray"></param>
        /// <param name="employeeId"></param>
        /// <param name="languageTag"></param>
        /// <returns></returns>
        Task<ApiResponse<Dictionary<string, object>>> GetEmployeeProfile(string ray, int employeeId, string languageTag);

        /// <summary>
        /// Get All Employee Payslips
        /// </summary>
        /// <param name="ray"></param>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        Task<ApiResponse<Dictionary<string, object>>> GetAllPayslips(string ray, int employeeId);

        /// <summary>
        /// Get All Employee Tax Return
        /// </summary>
        /// <param name="ray"></param>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        Task<ApiResponse<Dictionary<string, object>>> GetAllTaxReturn(string ray, int employeeId);

        /// <summary>
        /// Validate payslip by fileId
        /// </summary>
        /// <param name="ray"></param>
        /// <param name="employeeId"></param>
        /// <param name="fileId"></param>
        /// <returns></returns>
        Task<ApiResponse<List<int>>> ValidatePaySlipByFileIds(List<int> fileId, int employeeId, string ray);

        /// <summary>
        /// Validate tax return by fileId
        /// </summary>
        /// <param name="ray"></param>
        /// <param name="employeeId"></param>
        /// <param name="fileId"></param>
        /// <returns></returns>
        Task<ApiResponse<List<int>>> ValidateTaxReturnByFileIds(List<int> fileId, int employeeId, string ray);
    }
}