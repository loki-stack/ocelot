using CommonLib.Dtos;
using EmploymentServiceAPIs.EmployeePortal.Dtos;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.EmployeePortal.Services.Interfaces
{
    /// <summary>
    /// For employee self service portal to retrieve entity specific information 
    /// to display at entity specific login page
    /// </summary>
    public interface IEntityConfigService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityCode"></param>
        /// <returns></returns>
        Task<EntityConfigDto> GetConfig(string entityCode);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="ray"></param>
        /// <returns></returns>
        Task<ApiResponse<EmpPortalSupportDto>> GetSupport(int entityId, string ray);
    }
}