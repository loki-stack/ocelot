

using CommonLib.Dtos;
using EmploymentServiceAPIs.EmployeePortal.Dtos;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.EmployeePortal.Services.Interfaces
{
    public interface IEmpPayslipPasswordService
    {
        /// <summary>
        /// Get the employee current password
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        Task<ApiResponse<EmpPayslipPasswordDto>> GetCurrentPassword(int employeeId);
    }
}