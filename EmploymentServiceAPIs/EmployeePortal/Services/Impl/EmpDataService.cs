using AutoMapper;
using CommonLib.Dtos;
using CommonLib.Models.Client;
using EmploymentServiceAPIs.Common.Constants;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using EmploymentServiceAPIs.EmployeePortal.Dtos;
using EmploymentServiceAPIs.EmployeePortal.Repositories.Interfaces;
using EmploymentServiceAPIs.EmployeePortal.Services.Interfaces;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.EmployeePortal.Services.Impl
{
    /// <summary>
    /// Implementation class for IEmpDataService
    /// </summary>
    public class EmpDataService : IEmpDataService
    {
        private readonly ILogger<EmpDataService> _logger;

        private readonly ITaxReturnRepository _taxReturnRepo;
        private readonly IPaySlipRepository _paySlipRepo;

        private readonly IEmployeeRepository _employeeRepo;

        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="taxReturnRepo"></param>
        /// <param name="employeeRepo"></param>
        /// <param name="paySlipRepo"></param>
        /// <param name="mapper"></param>
        /// 
        public EmpDataService(
            ILogger<EmpDataService> logger,
            ITaxReturnRepository taxReturnRepo,
            IEmployeeRepository employeeRepo,
            IPaySlipRepository paySlipRepo,
            IMapper mapper)
        {
            this._logger = logger;
            this._taxReturnRepo = taxReturnRepo;
            this._paySlipRepo = paySlipRepo;
            this._employeeRepo = employeeRepo;
            this._mapper = mapper;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public async Task<EmpDataDto> GetInfo(int employeeId)
        {
            _logger.LogInformation($"GetInfo -- {employeeId}");
            EmpDataDto data = new EmpDataDto();

            TbEmpEmployee emp = await _employeeRepo.GetById(employeeId);
            if (emp != null)
            {
                EmpDetailDto empDetailDto = _mapper.Map<EmpDetailDto>(emp);
                data.EmpDetail = empDetailDto;
            }

            return data;

        }

        /// <summary>
        /// Get basic employee profile
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="languageTag"></param>
        /// <param name="ray"></param>
        /// <returns></returns>
        public async Task<ApiResponse<Dictionary<string, object>>> GetEmployeeProfile(string ray, int employeeId, string languageTag)
        {
            _logger.LogInformation($"GetEmployeeProfile -- EmployeeId: {employeeId}");
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = ray != null ? ray : string.Empty;
            response.Data = new Dictionary<string, object>();

            var profile = await _employeeRepo.GetEmployeeProfile(employeeId, languageTag);
            if (profile != null)
            {
                response.Data.Add("profile", profile);
            }

            return response;

        }

        /// <summary>
        /// Get All Employee Payslips
        /// </summary>
        /// <param name="ray"></param>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public async Task<ApiResponse<Dictionary<string, object>>> GetAllPayslips(string ray, int employeeId)
        {
            _logger.LogInformation($"GetAllPayslips -- EmployeeId: {employeeId}");
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = ray != null ? ray : string.Empty;
            response.Data = new Dictionary<string, object>();

            var result = await _paySlipRepo.GetByEmployeeId(employeeId);
            if (result != null)
            {
                response.Data.Add("payslips", result);
            }

            return response;

        }

        /// <summary>
        /// Validate payslip by fileId
        /// </summary>
        /// <param name="ray"></param>
        /// <param name="employeeId"></param>
        /// <param name="requestedfileIds"></param>
        /// <returns></returns>
        public async Task<ApiResponse<List<int>>> ValidatePaySlipByFileIds(List<int> requestedfileIds, int employeeId, string ray)
        {
            _logger.LogInformation($"ValidatePaySlipByFileIds -- EmployeeId: {employeeId}");
            ApiResponse<List<int>> response = new ApiResponse<List<int>>();
            response.Ray = ray != null ? ray : string.Empty;
            response.Data = new List<int>();
            var result = await _paySlipRepo.GetFileIdsByEmployeeId(requestedfileIds, employeeId);
            if (result.Count == 0)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = EmployeePortalLabel.PAY5001,
                });
                return response;
            }
            response.Data = result;
            return response;
        }

        /// <summary>
        /// Get All Employee Tax Return
        /// </summary>
        /// <param name="ray"></param>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public async Task<ApiResponse<Dictionary<string, object>>> GetAllTaxReturn(string ray, int employeeId)
        {
            _logger.LogInformation($"GetAllTaxReturn -- EmployeeId: {employeeId}");
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = ray != null ? ray : string.Empty;
            response.Data = new Dictionary<string, object>();

            var result = await _taxReturnRepo.GetByEmployeeId(employeeId);
            if (result != null)
            {
                response.Data.Add("taxReturn", result);
            }

            return response;

        }

        /// <summary>
        /// Validate tax return by fileId
        /// </summary>
        /// <param name="ray"></param>
        /// <param name="employeeId"></param>
        /// <param name="requestedfileIds"></param>
        /// <returns></returns>
        public async Task<ApiResponse<List<int>>> ValidateTaxReturnByFileIds(List<int> requestedfileIds, int employeeId, string ray)
        {
            _logger.LogInformation($"ValidateTaxReturnByFileIds -- EmployeeId: {employeeId}");
            ApiResponse<List<int>> response = new ApiResponse<List<int>>();
            response.Ray = ray != null ? ray : string.Empty;
            response.Data = new List<int>();
            var result = await _taxReturnRepo.GetFileIdsByEmployeeId(requestedfileIds, employeeId);
            if (result.Count == 0)
            {
                response.Message.Toast.Add(new CommonLib.Dtos.MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = EmployeePortalLabel.PAY5101,
                });
                return response;
            }
            response.Data = result;
            return response;
        }

    }
}