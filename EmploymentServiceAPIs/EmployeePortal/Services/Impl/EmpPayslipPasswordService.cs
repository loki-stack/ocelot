using CommonLib.Dtos;
using CommonLib.Models.Client;
using CommonLib.Utils;
using EmploymentServiceAPIs.Employee.Repositories.Intefaces;
using EmploymentServiceAPIs.EmployeePortal.Dtos;
using EmploymentServiceAPIs.EmployeePortal.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.EmployeePortal.Services.Impl
{
    public class EmpPayslipPasswordService : IEmpPayslipPasswordService
    {
        private readonly IPayslipPasswordRepository _payslipPasswordRepo;
        private readonly ILogger<EmpPayslipPasswordService> _logger;
        private readonly IConfiguration _config;

        public EmpPayslipPasswordService(IPayslipPasswordRepository payslipPasswordRepo,
            ILogger<EmpPayslipPasswordService> logger, IConfiguration config)
        {
            this._payslipPasswordRepo = payslipPasswordRepo;
            this._logger = logger;
            this._config = config;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public async Task<ApiResponse<EmpPayslipPasswordDto>> GetCurrentPassword(int employeeId)
        {
            EmpPayslipPasswordDto result = new EmpPayslipPasswordDto();
            ApiResponse<EmpPayslipPasswordDto> response = new ApiResponse<EmpPayslipPasswordDto>();
            // testing
            string encrypted = AesUtil.EncryptString(_config["AppSettings:AesKey"], "payslipPwd!");
            _logger.LogInformation($"------encrypted-password---{encrypted}");

            TbEmpPayslipPassword p = await _payslipPasswordRepo.GetLatestByEmployeeId(employeeId);
            if (p == null)
            {
                response.Data = null;
                return response;
            }
            // descrypt the password
            result.Password = AesUtil.DecryptString(_config["AppSettings:AesKey"], p.EncryptedPassword);
            response.Data = result;
            return response;
        }
    }
}