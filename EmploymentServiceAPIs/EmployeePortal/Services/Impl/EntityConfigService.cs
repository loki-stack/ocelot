using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using EmploymentServiceAPIs.Common.Constants;
using EmploymentServiceAPIs.Common.Dtos;
using EmploymentServiceAPIs.Common.Repositories.Interfaces;
using EmploymentServiceAPIs.EmployeePortal.Dtos;
using EmploymentServiceAPIs.EmployeePortal.Services.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Transactions;

namespace EmploymentServiceAPIs.EmployeePortal.Services.Impl
{
    /// <summary>
    /// 
    /// </summary>
    public class EntityConfigService : IEntityConfigService
    {
        private readonly ITbCamEntityRepository _camEntityRepository;
        private readonly ITbCamEntitySupportRepository _camEntitySupportRepository;
        private readonly ITbCamEntityFaqRepository _camEntityFaqRepository;
        private readonly ILogger<EntityConfigService> _logger;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="camEntityRepository"></param>
        /// <param name="camEntitySupportRepository"></param>
        /// <param name="camEntityFaqRepository"></param>
        public EntityConfigService(
            ILogger<EntityConfigService> logger,
            ITbCamEntityRepository camEntityRepository,
            ITbCamEntitySupportRepository camEntitySupportRepository,
            ITbCamEntityFaqRepository camEntityFaqRepository
            )
        {
            this._logger = logger;
            this._camEntityRepository = camEntityRepository;
            this._camEntitySupportRepository = camEntitySupportRepository;
            this._camEntityFaqRepository = camEntityFaqRepository;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityCode"></param>
        /// <returns></returns>
        public async Task<EntityConfigDto> GetConfig(string entityCode)
        {
            _logger.LogInformation($"GetConfig -- EntityCode: {entityCode}");
            EntityConfigDto dto = new EntityConfigDto();
            TbCamEntity entity = await _camEntityRepository.GetByCode(entityCode);

            TbCamClient client = entity.ParentClient;
            dto.ClientId = client.ClientId;
            dto.ClientNameTxt = client.ClientNameTxt;
            dto.ClientCode = client.ClientCode;

            dto.EntityCode = entity.EntityCode;
            dto.EntityId = entity.EntityId;
            dto.EntityNameTxt = entity.EntityNameTxt;
            dto.EntityDisplayName = entity.EntityDisplayName;

            bool checkSupport = await _camEntitySupportRepository.CheckSupport(dto.EntityId);
            bool checkFAQ = await _camEntityFaqRepository.CheckFAQ(dto.EntityId);
            dto.HasSupport = checkSupport;
            dto.HasFAQ = checkFAQ;

            return dto;
        }

        /// <summary>
        /// Get Support
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="ray"></param>
        /// <returns></returns>
        public async Task<ApiResponse<EmpPortalSupportDto>> GetSupport(int entityId, string ray)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                ApiResponse<EmpPortalSupportDto> response = new ApiResponse<EmpPortalSupportDto>();
                try
                {
                    _logger.LogInformation($"GetSupport -- EntityId: {entityId}");
                    EmpPortalSupportDto data = new EmpPortalSupportDto();
                    List<TbCamEntitySupportDto> support = await _camEntitySupportRepository.GetByEntityId(entityId);
                    List<TbCamEntityFaqDto> faqs = await _camEntityFaqRepository.GetByEntityId(entityId);
                    response.Ray = ray;

                    data.Support = support;
                    data.Faqs = faqs;
                    response.Data = data;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                    scope.Dispose();
                    response.Message.Toast.Add(new MessageItem
                    {
                        Level = Level.ERROR,
                        LabelCode = Label.ESS50_E_006
                    });
                }
                return response;
            }
        }
    }
}