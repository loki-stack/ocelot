using CommonLib.Models.Client;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.EmployeePortal.Repositories.Interfaces
{
    /// <summary>
    ///  Tax Return Repository
    /// </summary>
    /// <returns></returns>
    public interface ITaxReturnRepository
    {


        /// <summary>
        /// Create TaxReturn
        /// </summary>
        /// <param name="taxReturns"></param>
        /// <returns></returns>
        Task<List<TbEmpTaxReturn>> Create(List<TbEmpTaxReturn> taxReturns);

        /// <summary>
        /// Update TaxReturn
        /// </summary>
        /// <param name="taxReturn"></param>
        /// <returns></returns>
        Task<TbEmpTaxReturn> Update(TbEmpTaxReturn taxReturn);

        /// <summary>
        /// Get TaxReturn By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<TbEmpTaxReturn> GetById(int id);
        /// <summary>
        /// Get TaxReturn by employeeId
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        Task<List<TbEmpTaxReturn>> GetByEmployeeId(int employeeId);

        /// <summary>
        /// Get tax return' ids by employeeIds
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="fileIds"></param>
        /// <returns></returns>
        Task<List<int>> GetFileIdsByEmployeeId(List<int> fileIds, int employeeId);
    }
}