using CommonLib.Models.Client;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.EmployeePortal.Repositories.Interfaces
{
    public interface IPaySlipRepository
    {


        /// <summary>
        /// Create Payslip
        /// </summary>
        /// <param name="payslips"></param>
        /// <returns></returns>
        Task<List<TbEmpPayslip>> Create(List<TbEmpPayslip> payslips);

        /// <summary>
        /// Update Payslip
        /// </summary>
        /// <param name="payslip"></param>
        /// <returns></returns>
        Task<TbEmpPayslip> Update(TbEmpPayslip payslip);

        /// <summary>
        /// Get Payslip By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<TbEmpPayslip> GetById(int id);

        /// <summary>
        /// Get Payslip by employeeId
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        Task<List<TbEmpPayslip>> GetByEmployeeId(int employeeId);

        /// <summary>
        /// Get All fileIds
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="fileIds"></param>
        /// <returns></returns>
        Task<List<int>> GetFileIdsByEmployeeId(List<int> fileIds, int employeeId);
    }
}