using CommonLib.Models;
using CommonLib.Models.Client;
using CommonLib.Models.Core;
using CommonLib.Services;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.EmployeePortal.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.EmployeePortal.Repositories.Impl
{
    public class PaySlipRepository : IPaySlipRepository
    {
        protected readonly IConnectionStringContainer _connectionStringRepository;
        private readonly ILogger<PaySlipRepository> _logger;
        protected string _connectionString;
        protected readonly CoreDbContext _coreDbContext;

        /// <summary>
        /// </summary>
        /// <param name="connectionStringRepository"></param>
        /// <param name="coreDBContext"></param>
        /// <param name="logger"></param>
        public PaySlipRepository(IConnectionStringContainer connectionStringRepository, CoreDbContext coreDBContext, ILogger<PaySlipRepository> logger)
        {
            _connectionStringRepository = connectionStringRepository;
            _coreDbContext = coreDBContext;
            _connectionString = _connectionStringRepository.GetConnectionString();
            _logger = logger;
        }

        protected TenantDBContext CreateContext(string _connectionString)
        {
            return new TenantDBContext(_connectionString);
        }

        /// <summary>
        /// Create Payslip
        /// </summary>
        /// <param name="payslips"></param>
        /// <returns></returns>
        public async Task<List<TbEmpPayslip>> Create(List<TbEmpPayslip> payslips)
        {
            using (var _context = CreateContext(_connectionString))
            {
                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    try
                    {
                        await _context.TbEmpPayslip.AddRangeAsync(payslips);
                        await _context.SaveChangesAsync();
                        await transaction.CommitAsync();
                        return payslips;
                    }
                    catch (Exception ex)
                    {
                        await transaction.RollbackAsync();
                        _logger.LogWarning(ex.ToString());
                        throw ex;
                    }
                }
            }
        }

        /// <summary>
        /// Get Payslip By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<TbEmpPayslip> GetById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    try
                    {
                        var payslip = await _context.TbEmpPayslip.FirstOrDefaultAsync(x => x.FileId == id);
                        return payslip;
                    }
                    catch (Exception ex)
                    {
                        await transaction.RollbackAsync();
                        _logger.LogWarning(ex.ToString());
                        throw ex;
                    }
                }
            }
        }

        /// <summary>
        /// Update Payslip
        /// </summary>
        /// <param name="payslip"></param>
        /// <returns></returns>
        public async Task<TbEmpPayslip> Update(TbEmpPayslip payslip)
        {
            using (var _context = CreateContext(_connectionString))
            {
                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    try
                    {
                        _context.TbEmpPayslip.Update(payslip);
                        await _context.SaveChangesAsync();
                        return payslip;
                    }
                    catch (Exception ex)
                    {
                        await transaction.RollbackAsync();
                        _logger.LogWarning(ex.ToString());
                        throw ex;
                    }
                }
            }
        }

        /// <summary>
        /// Get Payslip by employeeId
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public async Task<List<TbEmpPayslip>> GetByEmployeeId(int employeeId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    try
                    {
                        var result = await _context.TbEmpPayslip.Where(x => x.EmployeeId == employeeId).ToListAsync();
                        return result;
                    }
                    catch (Exception ex)
                    {
                        await transaction.RollbackAsync();
                        _logger.LogWarning(ex.ToString());
                        throw ex;
                    }
                }
            }
        }

        /// <summary>
        /// Get payslips' ids by employeeIds
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="fileIds"></param>
        /// <returns></returns>
        public async Task<List<int>> GetFileIdsByEmployeeId(List<int> fileIds, int employeeId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    try
                    {
                        HashSet<int> fIds = new HashSet<int>(fileIds.Select(s => s));
                        var result = await _context.TbEmpPayslip
                            .Where(x => x.EmployeeId == employeeId)
                            .Where(x => fIds.Contains(x.FileId))
                            .Select(x => x.FileId).ToListAsync();
                        return result;
                    }
                    catch (Exception ex)
                    {
                        await transaction.RollbackAsync();
                        _logger.LogWarning(ex.ToString());
                        throw ex;
                    }
                }
            }
        }
    }
}