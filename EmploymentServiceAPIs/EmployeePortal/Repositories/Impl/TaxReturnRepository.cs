using CommonLib.Models;
using CommonLib.Models.Client;
using CommonLib.Models.Core;
using CommonLib.Services;
using EmploymentServiceAPIs.Common.Models;
using EmploymentServiceAPIs.EmployeePortal.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmploymentServiceAPIs.EmployeePortal.Repositories.Impl
{
    public class TaxReturnRepository : ITaxReturnRepository
    {
        protected readonly IConnectionStringContainer _connectionStringRepository;
        private readonly ILogger<TaxReturnRepository> _logger;

        protected string _connectionString;
        protected readonly CoreDbContext _coreDbContext;

        /// <summary>
        /// </summary>
        /// <param name="connectionStringRepository"></param>
        /// <param name="coreDBContext"></param>
        /// <param name="logger"></param>
        public TaxReturnRepository(ILogger<TaxReturnRepository> logger, IConnectionStringContainer connectionStringRepository, CoreDbContext coreDBContext)
        {
            _connectionStringRepository = connectionStringRepository;
            _coreDbContext = coreDBContext;
            _connectionString = _connectionStringRepository.GetConnectionString();
            _logger = logger;
        }

        /// <summary>
        /// </summary>
        /// <param name="_connectionString"></param>
        protected TenantDBContext CreateContext(string _connectionString)
        {
            return new TenantDBContext(_connectionString);
        }

        /// <summary>
        /// Create Tax Returns
        /// </summary>
        /// <param name="taxReturns"></param>
        /// <returns></returns>
        public async Task<List<TbEmpTaxReturn>> Create(List<TbEmpTaxReturn> taxReturns)
        {
            using (var _context = CreateContext(_connectionString))
            {
                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    try
                    {
                        await _context.TbEmpTaxReturn.AddRangeAsync(taxReturns);
                        await _context.SaveChangesAsync();
                        await transaction.CommitAsync();
                        return taxReturns;
                    }
                    catch (Exception ex)
                    {
                        await transaction.RollbackAsync();
                        _logger.LogWarning(ex.ToString());
                        throw ex;
                    }
                }
            }
        }

        /// <summary>
        /// Get Tax Returns By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<TbEmpTaxReturn> GetById(int id)
        {
            using (var _context = CreateContext(_connectionString))
            {
                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    try
                    {
                        var taxReturn = await _context.TbEmpTaxReturn.FirstOrDefaultAsync(x => x.FileId == id);
                        return taxReturn;
                    }
                    catch (Exception ex)
                    {
                        await transaction.RollbackAsync();
                        _logger.LogWarning(ex.ToString());
                        throw ex;
                    }
                }
            }
        }

        /// <summary>
        /// Update Tax Returns
        /// </summary>
        /// <param name="taxReturn"></param>
        /// <returns></returns>
        public async Task<TbEmpTaxReturn> Update(TbEmpTaxReturn taxReturn)
        {
            using (var _context = CreateContext(_connectionString))
            {
                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    try
                    {
                        _context.TbEmpTaxReturn.Update(taxReturn);
                        await _context.SaveChangesAsync();
                        return taxReturn;
                    }
                    catch (Exception ex)
                    {
                        await transaction.RollbackAsync();
                        _logger.LogWarning(ex.ToString());
                        throw ex;
                    }
                }
            }
        }

        /// <summary>
        /// Get TaxReturnIds by employeeId
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public async Task<List<TbEmpTaxReturn>> GetByEmployeeId(int employeeId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                var result = await _context.TbEmpTaxReturn.Where(x => x.EmployeeId == employeeId).ToListAsync();
                return result;
            }
        }

        /// <summary>
        /// Get tax return' ids by employeeIds
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="fileIds"></param>
        /// <returns></returns>
        public async Task<List<int>> GetFileIdsByEmployeeId(List<int> fileIds, int employeeId)
        {
            using (var _context = CreateContext(_connectionString))
            {
                HashSet<int> fIds = new HashSet<int>(fileIds.Select(s => s));
                var result = await _context.TbEmpTaxReturn
                    .Where(x => x.EmployeeId == employeeId)
                    .Where(x => fIds.Contains(x.FileId))
                    .Select(x => x.FileId).ToListAsync();
                return result;
            }
        }

    }
}