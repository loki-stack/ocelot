using EmploymentServiceAPIs.Common.Dtos;
using System.Collections.Generic;

namespace EmploymentServiceAPIs.EmployeePortal.Dtos
{
    public class EmpPortalSupportDto
    {
        public List<TbCamEntityFaqDto> Faqs { get; set; }
        public List<TbCamEntitySupportDto> Support { get; set; }
    }
}