// Disable all XML Comment warnings in this file //
#pragma warning disable 1591
using System;

namespace EmploymentServiceAPIs.EmployeePortal.Dtos
{
    /// <summary>
    /// Employee details - subset of TbEmpEmployee
    /// </summary>
    public class EmpDetailDto
    {
        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string ChristianName { get; set; }
        public string DisplayName { get; set; }
        public string NamePrefix { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Gender { get; set; }
        // public int? Photo { get; set; }
        // public int? Age { get; set; }
        public string MartialStatus { get; set; }
        public string PreferredLanguage { get; set; }
        public string CitizenIdNo { get; set; }
        public string Nationality { get; set; }
        public string PassportIdNo { get; set; }
        public string CountryOfIssue { get; set; }
        // public bool? WorkVisaHolder { get; set; }
        // public DateTime? WorkArrivingDate { get; set; }
        // public DateTime? WorkVisaExpiryDate { get; set; }
        // public DateTime? WorkVisaIssueDate { get; set; }
        // public DateTime? WorkVisaLandingDate { get; set; }
        public string HomePhone { get; set; }
        public string OfficePhone { get; set; }
        public string MobilePhone { get; set; }
        public string WorkEmail { get; set; }
        public string PersonalEmail { get; set; }
        // public string ResidentialAddress1 { get; set; }
        // public string ResidentialAddress2 { get; set; }
        // public string ResidentialAddress3 { get; set; }
        // public string ResidentialDistrict { get; set; }
        // public string ResidentialCity { get; set; }
        // public string ResidentialState { get; set; }
        // public string ResidentialCountry { get; set; }
        // public string ResidentialPostalCode { get; set; }
        // public string ResidentialHongkongTaxForm { get; set; }
        // public string PostalAddress1 { get; set; }
        // public string PostalAddress2 { get; set; }
        // public string PostalAddress3 { get; set; }
        // public string PostalDistrict { get; set; }
        // public string PostalCity { get; set; }
        // public string PostalState { get; set; }
        // // public string PostalCountry { get; set; }
        // public string PostalPostalCode { get; set; }
        // public string PostalHongkongTaxForm { get; set; }
        // public string Remarks { get; set; }
        // public string CreatedBy { get; set; }
        // public DateTimeOffset? CreatedDt { get; set; }
        // public string ModifiedBy { get; set; }
        // public DateTimeOffset? ModifiedDt { get; set; }
    }
}

#pragma warning restore 1591