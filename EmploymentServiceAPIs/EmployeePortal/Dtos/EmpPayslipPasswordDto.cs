using System;

namespace EmploymentServiceAPIs.EmployeePortal.Dtos
{
    public class EmpPayslipPasswordDto
    {
        public string Password { get; set; }
        public DateTimeOffset CreatedDt { get; set; }
    }
}