namespace EmploymentServiceAPIs.EmployeePortal.Dtos
{
    /// <summary>
    /// Employee Data DTO - for returning employee data to frontend
    /// </summary>
    public class EmpDataDto
    {
        public string RequestType { get; set; }

        public EmpDetailDto EmpDetail { get; set; }
    }
}