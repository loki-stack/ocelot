namespace EmploymentServiceAPIs.EmployeePortal.Dtos
{
    public class EntityConfigDto
    {
        public string ClientCode { get; set; }
        public string ClientNameTxt { get; set; }

        public int ClientId { get; set; }

        public string EntityCode { get; set; }
        public string EntityNameTxt { get; set; }

        public int EntityId { get; set; }

        public string EntityDisplayName { get; set; }
        public bool HasSupport { get; set; }
        public bool HasFAQ { get; set; }


    }
}