using AutoMapper;
using CommonLib.Dtos;
using CommonLib.Exception;
using CommonLib.Models;
using CommonLib.Models.Core;
using CommonLib.Repositories.Impl;
using CommonLib.Repositories.Interfaces;
using CommonLib.Services;
using CommonLib.Validation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using PayrollServiceAPIs.Common.Mapper;
using PayrollServiceAPIs.Common.Repositories.Impl;
using PayrollServiceAPIs.Common.Repositories.Interfaces;
using PayrollServiceAPIs.Common.Services.Impl;
using PayrollServiceAPIs.Common.Services.Interfaces;
using PayrollServiceAPIs.Formulas.Repository.Impl;
using PayrollServiceAPIs.Formulas.Services.Impl;
using PayrollServiceAPIs.Formulas.Services.Interfaces;
using PayrollServiceAPIs.PayRoll.Repositories.Impl;
using PayrollServiceAPIs.PayRoll.Repositories.Interfaces;
using PayrollServiceAPIs.PayRoll.Services.Impl;
using PayrollServiceAPIs.PayRoll.Services.Interfaces;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace PayrollServiceAPIs
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddHttpClient();
            var secret = Configuration.GetSection("AppSettings").GetSection("Secret").Value;
            var key = Encoding.ASCII.GetBytes(secret);
            services.AddAutoMapper(typeof(AutoMapperProfile).Assembly);
            services.Configure<KestrelServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });
            services.Configure<IISServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });

            var tokenValidationParameters = new TokenValidationParameters
            {
                ClockSkew = TimeSpan.Zero,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false,
                RequireExpirationTime = false,
                ValidateLifetime = true
            };

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = tokenValidationParameters;
            });

            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));
            services.AddSingleton(tokenValidationParameters);
            services.AddDbContext<CoreDbContext>(options
                => options.UseSqlServer(Configuration.GetConnectionString("CoreConnection"),
                 conf =>
                 {
                     conf.UseHierarchyId(); // enable hierarchyid support
                 })
            );

            //disable automatic model state validation
            services.AddSingleton<IObjectModelValidator, IgnoreAutoValidator>();
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IGenericCodeRepository, GenericCodeRepository>();
            services.AddScoped<CommonLib.Repositories.Interfaces.IFormulasRepository, CommonLib.Repositories.Impl.FormulasRepository>();

            services.AddScoped<IHttpRequestAnalyzer, HttpRequestAnalyzer>();
            services.AddScoped<IConnectionStringContainer, ConnectionStringContainer>();
            // Payroll Profile
            services.AddScoped<IPayrollProfileRepository, PayrollProfileRepository>();
            services.AddScoped<IPayrollProfileService, PayrollProfileService>();

            // PayrollItem
            services.AddScoped<IMasterPayrollItemRepository, MasterPayrollItemRepository>();
            services.AddScoped<IMasterPayRollItemService, MasterPayrollItemService>();

            //validate
            services.AddScoped<IValidator, Validator>();

            // payroll run
            services.AddScoped<IPayrollRunService, PayrollRunService>();
            services.AddScoped<IPayrollRunRepository, PayrollRunRepository>();

            // Formulas
            services.AddScoped<IFormulasService, FormulasService>();
            services.AddScoped<PayrollServiceAPIs.Formulas.Repository.Interfaces.IFormulasRepository, PayrollServiceAPIs.Formulas.Repository.Impl.FormulasRepository>();

            // MPF Registered scheme
            services.AddScoped<IRegisteredSchemeService, RegisteredSchemeService>();
            services.AddScoped<IRegisteredSchemeRepository, RegisteredSchemeRepository>();

            // enrolled scheme
            services.AddScoped<IEnrolledSchemeService, EnrolledSchemeService>();
            services.AddScoped<IEnrolledSchemeRepository, EnrolledSchemeRepository>();

            // ORSO Scheme
            services.AddScoped<IORSOSchemeService, ORSOSchemeService>();
            services.AddScoped<IORSOSchemeRepository, ORSOSchemeRepository>();

            // MPF Trustee
            services.AddScoped<IMPFTrusteeRepository, MPFTrusteeRepository>();

            // Payroll Item
            services.AddScoped<IPayrollItemRepository, PayrollItemRepository>();

            // exchange rate
            services.AddScoped<IExchangeRateService, ExchangeRateService>();
            services.AddScoped<IExchangeRateRepository, ExchangeRateRepository>();

            // GL Code
            services.AddScoped<IGlCodeService, GlCodeService>();
            services.AddScoped<IGlCodeRepository, GlCodeRepository>();

            // Payroll cycle
            services.AddScoped<IPayrollCycleService, PayrollCycleService>();
            services.AddScoped<IPayrollCycleRepository, PayrollCycleRepository>();

            // Payroll cycle setting
            services.AddScoped<IPayrollCycleSettingService, PayrollCycleSettingService>();
            services.AddScoped<IPayrollCycleSettingRepository, PayrollCycleSettingRepository>();

            services.AddScoped<IEmployeeService, EmployeeService>();

            //disable automatic model state validation
            services.AddSingleton<IObjectModelValidator, IgnoreAutoValidator>();
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            services.AddSwaggerGen(config =>
            {
                config.SwaggerDoc("v1", new OpenApiInfo { Title = "Employment", Version = "v1" });
                config.CustomOperationIds(apiDesc =>
                {
                    string controllerName = apiDesc.ActionDescriptor.RouteValues["controller"];
                    return apiDesc.TryGetMethodInfo(out MethodInfo methodInfo) ? controllerName + "_" + methodInfo.Name : null;
                });
                config.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = @"JWT Authorization header using the Bearer scheme. \r\n\r\n
                      Enter 'Bearer' [space] and then your token in the text input below.
                      \r\n\r\nExample: 'Bearer 12345abcdef'",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                config.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header,
                        },
                        new List<string>()
                    }
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                config.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context => new GlobalExceptionHandler(loggerFactory).Handle(context));
            });

            app.UseSwagger();
            app.UseSwaggerUI(config =>
            {
                config.SwaggerEndpoint("/swagger/v1/swagger.json", "Employment v1");
                config.DisplayOperationId();
                config.RoutePrefix = string.Empty;
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
