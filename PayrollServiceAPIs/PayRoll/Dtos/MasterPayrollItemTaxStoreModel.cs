﻿namespace PayrollServiceAPIs.PayRoll.Dtos
{
    public class MasterPayrollItemTaxStoreModel
    {
        public int TaxItemId { get; set; }
        public int TaxItemValue { get; set; }
    }
}
