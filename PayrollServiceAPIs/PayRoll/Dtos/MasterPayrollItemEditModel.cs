﻿using System.Collections.Generic;

namespace PayrollServiceAPIs.PayRoll.Dtos
{
    public class MasterPayrollItemEditModel
    {
        public MasterPayrollViewModel Detail { get; set; }
        public List<PayrollItemFlag> Types { get; set; }
        public List<MasterPayrollItemTaxModel> TaxSelect { get; set; }
        public List<int> FlagIds { get; set; }
        public List<MasterPayrollTaxEditModel> Taxable { get; set; }
    }
}

