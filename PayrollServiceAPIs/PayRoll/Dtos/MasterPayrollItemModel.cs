﻿namespace PayrollServiceAPIs.PayRoll.Dtos
{
    public class MasterPayrollItemModel
    {
        public int PayrollItemId { get; set; }
        public string ItemName { get; set; }
        public string ItemCode { get; set; }
    }
}
