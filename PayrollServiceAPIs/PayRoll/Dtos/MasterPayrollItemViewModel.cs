﻿using System.Collections.Generic;

namespace PayrollServiceAPIs.PayRoll.Dtos
{
    public class MasterPayrollItemViewModel
    {
        public MasterPayrollViewModel Detail { get; set; }
        public Dictionary<string, bool> Flags { get; set; }
        public bool TaxableFlag { get; set; }
    }
}
