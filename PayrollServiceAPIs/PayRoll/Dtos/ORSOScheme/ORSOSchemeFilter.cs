﻿namespace PayrollServiceAPIs.PayRoll.Dtos.ORSOScheme
{
    public class ORSOSchemeFilter
    {
        public string OrsoSchemeType { get; set; }
        public string StatusCd { get; set; }
    }
}
