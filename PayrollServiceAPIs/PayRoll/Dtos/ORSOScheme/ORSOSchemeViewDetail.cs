﻿using PayrollServiceAPIs.Common.Models;
using System.Collections.Generic;
using CommonLib.Models.Client;

namespace PayrollServiceAPIs.PayRoll.Dtos.ORSOScheme
{
    public class ORSOSchemeViewDetail : TbPayOrsoScheme
    {
        public List<TbPayPayrollItem> VcPayrollItem { get; set; }
    }
}
