﻿using System;

namespace PayrollServiceAPIs.PayRoll.Dtos.ORSOScheme
{
    public class ORSOSchemeViewList
    {
        public int OrsoSchemeId { get; set; }
        public string OrsoSchemeName { get; set; }
        public string OrsoSchemeType { get; set; }
        public string OrsoSchemeCode { get; set; }
        public string StatusCd { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
    }
}
