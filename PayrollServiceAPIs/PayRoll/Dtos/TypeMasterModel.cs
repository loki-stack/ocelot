﻿namespace PayrollServiceAPIs.PayRoll.Dtos
{
    public class TypeMasterModel
    {
        public int TypeId { get; set; }
        public string DisplayName { get; set; }
    }
}
