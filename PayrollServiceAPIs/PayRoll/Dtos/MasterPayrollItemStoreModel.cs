﻿using CommonLib.Validation;
using PayrollServiceAPIs.Common.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace PayrollServiceAPIs.PayRoll.Dtos
{
    public class MasterPayrollItemStoreModel
    {
        [JsonIgnore]
        public int ClientId { get; set; }
        [JsonIgnore]
        public int EntityId { get; set; }
        [MaxLength(100)]
        public String ItemName { get; set; }
        [MaxLength(100)]
        public String ItemCode { get; set; }
        public String Remark { get; set; }
        [Required]
        [RegularExpression("INACTIVE|ACTIVE", ErrorMessage = "The StatusCd must be either 'INACTIVE' or 'ACTIVE' only.")]
        [ExtraResult(ComponentId = "StatusCd", Level = Level.ERROR, Placeholder = new string[] { })]
        public String StatusCd { get; set; }
        public int GlCodeId { get; set; }
        public byte PaymentSign { get; set; }
        [JsonIgnore]
        public string CreatedBy { get; set; }
        [JsonIgnore]
        public string ModifiedBy { get; set; }
        public List<int> FlagIds { get; set; }
        public List<MasterPayrollItemTaxStoreModel> TaxItemValue { get; set; }
    }
}
