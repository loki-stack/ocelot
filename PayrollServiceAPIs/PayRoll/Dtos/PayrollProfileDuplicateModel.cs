﻿using CommonLib.Validation;
using PayrollServiceAPIs.Common.Constants;
using System.ComponentModel.DataAnnotations;

namespace PayrollServiceAPIs.PayRoll.Dtos
{
    public class PayrollProfileDuplicateModel
    {
        [Required]
        [MaxLength(100)]
        [ExtraResult(ComponentId = "ProfileName", Level = Level.ERROR, Placeholder = new string[] { })]
        public string PayrollProfileName { get; set; }
    }
}
