﻿using System.Collections.Generic;

namespace PayrollServiceAPIs.PayRoll.Dtos
{
    public class MasterPayrollItemTaxModel
    {
        public TypeMasterModel Detail { get; set; }
        public List<TypeMasterModel> Child { get; set; }
    }
}
