﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace PayrollServiceAPIs.PayRoll.Dtos
{
    public class MasterPayrollViewModel
    {
        public int PayrollItemId { get; set; }
        public int ClientId { get; set; }
        public int EntityId { get; set; }
        public String ItemName { get; set; }
        public String ItemCode { get; set; }
        public byte PaymentSign { get; set; }
        public String Remark { get; set; }
        public String StatusCd { get; set; }
        [NotMapped]
        public string StatusText { get; set; }
        public int? GlCodeId { get; set; }

    }
}
