﻿using CommonLib.Validation;
using PayrollServiceAPIs.Common.Constants;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace PayrollServiceAPIs.PayRoll.Dtos
{
    public class PayrollProfileStoreModel
    {
        [JsonIgnore]
        public int ClientId { get; set; }
        [JsonIgnore]
        public int EntityId { get; set; }
        [Required]
        [MaxLength(100)]
        [ExtraResult(ComponentId = "ProfileName", Level = Level.ERROR, Placeholder = new string[] { })]
        public string ProfileName { get; set; }
        [Required]
        [RegularExpression("INACTIVE|ACTIVE", ErrorMessage = "The StatusCd must be either 'INACTIVE' or 'ACTIVE' only.")]
        [ExtraResult(ComponentId = "StatusCd", Level = Level.ERROR, Placeholder = new string[] { })]
        public string StatusCd { get; set; }
        public string Remark { get; set; }
        [Required]
        [ExtraResult(ComponentId = "PayrollItems", Level = Level.ERROR, Placeholder = new string[] { })]
        public List<int> PayrollItems { get; set; }
        [JsonIgnore]
        public string CreatedBy { get; set; }
        [JsonIgnore]
        public string ModifiedBy { get; set; }

        public int FormulasId { get; set; }
    }
}
