﻿using CommonLib.Validation;
using PayrollServiceAPIs.Common.Constants;
using System;
using System.ComponentModel.DataAnnotations;

namespace PayrollServiceAPIs.PayRoll.Dtos
{
    public class MasterPayrollItemDuplicateModel
    {
        [Required]
        [MaxLength(100)]
        [ExtraResult(ComponentId = "ItemName", Level = Level.ERROR, Placeholder = new string[] { })]
        public String ItemName { get; set; }
        [Required]
        [MaxLength(100)]
        [ExtraResult(ComponentId = "ItemCode", Level = Level.ERROR, Placeholder = new string[] { })]
        public String ItemCode { get; set; }
    }
}
