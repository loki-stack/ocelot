﻿using System;
using System.Text.Json.Serialization;

namespace PayrollServiceAPIs.PayRoll.Dtos.PayrollCycle
{
    public class PayrollCycleStoreModel
    {
        public string PayrollCycleCode { get; set; }
        public DateTimeOffset DateStart { get; set; }
        public DateTimeOffset DateEnd { get; set; }
        public DateTimeOffset? CutOffDate { get; set; }
        [JsonIgnore]
        public string CreatedBy { get; set; }
        [JsonIgnore]
        public string ModifiedBy { get; set; }
    }
}
