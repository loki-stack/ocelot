﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace PayrollServiceAPIs.PayRoll.Dtos.PayrollCycle
{
    public class PayrollCycleViewModel
    {
        public int PayrollCycleId { get; set; }
        public string PayrollCycleCode { get; set; }
        public DateTimeOffset DateStart { get; set; }
        public DateTimeOffset DateEnd { get; set; }
        public DateTimeOffset? CutOffDate { get; set; }
        public bool LinkPayrollRunFlg { get; set; }
        public string StatusCd { get; set; }
        [NotMapped]
        public string StatusText { get; set; }
    }
}
