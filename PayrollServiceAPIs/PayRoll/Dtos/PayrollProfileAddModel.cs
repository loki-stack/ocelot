﻿using System.Collections.Generic;

namespace PayrollServiceAPIs.PayRoll.Dtos
{
    public class PayrollProfileAddModel
    {
        public List<PayrollProfileViewItemModel> Items { get; set; }
        public List<PayrollItemFlag> Types { get; set; }
    }
}
