﻿using CommonLib.Models;
using CommonLib.Models.Core;

namespace PayrollServiceAPIs.PayRoll.Dtos
{
    public class MPFRegisteredSchemeViewList : TbPayMpfRegisteredScheme
    {
        public string TrusteeName { get; set; }
    }
}
