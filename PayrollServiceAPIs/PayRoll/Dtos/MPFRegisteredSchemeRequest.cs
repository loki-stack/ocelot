﻿using CommonLib.Validation;
using PayrollServiceAPIs.Common.Constants;
using System.ComponentModel.DataAnnotations;

namespace PayrollServiceAPIs.PayRoll.Dtos
{
    [System.ComponentModel.DisplayName("MPFRegisteredScheme")]
    public class MPFRegisteredSchemeRequest
    {
        [MaxLength(100, ErrorMessage = Label.RS0101)]
        [ExtraResult(ComponentId = "schemeNameEn", Level = Level.ERROR, Placeholder = new string[] { })]
        public string SchemeNameEn { get; set; }

        [MaxLength(100, ErrorMessage = Label.RS0102)]
        [ExtraResult(ComponentId = "schemeNameCn", Level = Level.ERROR, Placeholder = new string[] { })]
        public string SchemeNameCn { get; set; }

        [MaxLength(20, ErrorMessage = Label.RS0103)]
        [ExtraResult(ComponentId = "remittanceStatementFormat", Level = Level.ERROR, Placeholder = new string[] { })]
        public string RemittanceStatementFormat { get; set; }

        public bool ProrateRiRemittanceStatement { get; set; }

        [MaxLength(20, ErrorMessage = Label.RS0104)]
        [ExtraResult(ComponentId = "interfaceFileFormat", Level = Level.ERROR, Placeholder = new string[] { })]
        public string InterfaceFileFormat { get; set; }

        [MaxLength(50, ErrorMessage = Label.RS0105)]
        [ExtraResult(ComponentId = "type", Level = Level.ERROR, Placeholder = new string[] { })]
        public string Type { get; set; }

        [MaxLength(20, ErrorMessage = Label.RS0106)]
        [ExtraResult(ComponentId = "financialYearDate", Level = Level.ERROR, Placeholder = new string[] { })]
        public string FinancialYearDate { get; set; }
    }
}
