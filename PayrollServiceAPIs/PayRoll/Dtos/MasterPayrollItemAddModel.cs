﻿using System.Collections.Generic;

namespace PayrollServiceAPIs.PayRoll.Dtos
{
    public class MasterPayrollItemAddModel
    {
        public List<PayrollItemFlag> Types { get; set; }
        public List<MasterPayrollItemTaxModel> TaxSelect { get; set; }
    }
}
