﻿namespace PayrollServiceAPIs.PayRoll.Dtos
{
    public class MasterPayrollTaxEditModel
    {
        public int TaxItemId { get; set; }
        public int TaxItemValue { get; set; }
        public string StatusCd { get; set; }
    }
}
