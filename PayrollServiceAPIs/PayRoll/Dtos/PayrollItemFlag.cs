﻿namespace PayrollServiceAPIs.PayRoll.Dtos
{
    public class PayrollItemFlag
    {
        public int TypeId { get; set; }
        public string DisplayName { get; set; }
        public string GroupName { get; set; }
    }
}
