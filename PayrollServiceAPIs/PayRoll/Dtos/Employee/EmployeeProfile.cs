﻿namespace PayrollServiceAPIs.PayRoll.Dtos.Employee
{
    public class EmployeeProfile
    {
        public string Name { get; set; }
        public string Role { get; set; }
        public string Division { get; set; }
        public string ReportTo { get; set; }
        public string StaffId { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public int? Photo { get; set; }
    }
}