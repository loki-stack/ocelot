﻿namespace PayrollServiceAPIs.PayRoll.Dtos
{
    public class PayMasterPayrollItem
    {

        public int ClientId { get; set; }
        public int EntityId { get; set; }
        public string ItemName { get; set; }
        public string ItemCode { get; set; }
        public byte PaymentSign { get; set; }
        public string Remark { get; set; }
        public string StatusCd { get; set; }
    }
}
