﻿using CommonLib.Dtos;
using CommonLib.Models.Client;
using PayrollServiceAPIs.Common.Models;
using System.Collections.Generic;

namespace PayrollServiceAPIs.PayRoll.Dtos.ExchangeRate
{
    public class ExchangeRateEditModel
    {
        public ExchangeRateViewModel Detail { get; set; }
        public List<PayrollCycleItemModel> PayrollCycle { get; set; }
        public List<GenericCodeResult> Currency { get; set; }
    }
}
