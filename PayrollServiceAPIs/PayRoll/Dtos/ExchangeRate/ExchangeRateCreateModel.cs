﻿using CommonLib.Dtos;
using CommonLib.Models.Client;
using PayrollServiceAPIs.Common.Models;
using System.Collections.Generic;

namespace PayrollServiceAPIs.PayRoll.Dtos.ExchangeRate
{
    public class ExchangeRateCreateModel
    {
        public List<GenericCodeResult> Currency { get; set; }
        public List<PayrollCycleItemModel> PayrollCycle { get; set; }
    }
}
