﻿using System;

namespace PayrollServiceAPIs.PayRoll.Dtos.ExchangeRate
{
    public class ExchangeRateViewModel
    {
        public int ExchangeRateId { get; set; }
        public int? PayrollCycleId { get; set; }
        public DateTimeOffset? StartDate { get; set; }
        public DateTimeOffset? EndDate { get; set; }
        public string CurrencyFrom { get; set; }
        public string CurrencyTo { get; set; }
        public double AverageRate { get; set; }
        public double MonthEndRate { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
    }
}
