﻿using CommonLib.Validation;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace PayrollServiceAPIs.PayRoll.Dtos.ExchangeRate
{
    public class ExchangeRateStoreModel
    {
        [Required]
        [ExtraResult(ComponentId = "PayrollCycleId", Level = CommonLib.Constants.Level.ERROR, Placeholder = new string[] { })]
        public int PayrollCycleId { get; set; }
        [Required]
        [ExtraResult(ComponentId = "CurrencyFrom", Level = CommonLib.Constants.Level.ERROR, Placeholder = new string[] { })]
        public string CurrencyFrom { get; set; }
        [Required]
        [ExtraResult(ComponentId = "CurrencyTo", Level = CommonLib.Constants.Level.ERROR, Placeholder = new string[] { })]
        public string CurrencyTo { get; set; }
        [Required]
        [ExtraResult(ComponentId = "AverageRate", Level = CommonLib.Constants.Level.ERROR, Placeholder = new string[] { })]
        public double AverageRate { get; set; }
        [Required]
        [ExtraResult(ComponentId = "MonthEndRate", Level = CommonLib.Constants.Level.ERROR, Placeholder = new string[] { })]
        public double MonthEndRate { get; set; }
        [JsonIgnore]
        public string CreatedBy { get; set; }
        [JsonIgnore]
        public string ModifiedBy { get; set; }
    }
}
