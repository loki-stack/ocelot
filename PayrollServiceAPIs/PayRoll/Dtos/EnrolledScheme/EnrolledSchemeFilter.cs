﻿namespace PayrollServiceAPIs.PayRoll.Dtos.EnrolledScheme
{
    public class EnrolledSchemeFilter
    {
        public string EnrolledSchemeType { get; set; }
        public string StatusCd { get; set; }
    }
}
