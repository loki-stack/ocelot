﻿using System;
using System.Collections.Generic;

namespace PayrollServiceAPIs.PayRoll.Dtos.EnrolledScheme
{
    public class EnrolledSchemeRequest
    {
        public int MPFRegisteredSchemeId { get; set; }
        public string EnrolledSchemeName { get; set; }
        public string EnrolledSchemeCode { get; set; }
        public string EnrolledSchemeType { get; set; }
        public string EnrolledSchemeNo { get; set; }
        public string ContributionCycle { get; set; }
        public string ContactPerson { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string ParticipationNo { get; set; }
        public string PaymentMethod { get; set; }
        public string Currency { get; set; }
        public string PayCentre { get; set; }
        public List<int> VcPayrollItemIds { get; set; }
        public string RoundType { get; set; }
        public int NearestDec { get; set; }
        public string StatusCd { get; set; }
        public DateTime? ClosedDate { get; set; }
    }
}
