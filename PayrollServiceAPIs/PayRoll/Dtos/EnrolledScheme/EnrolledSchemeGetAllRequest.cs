﻿using PayrollServiceAPIs.Common.Dtos;

namespace PayrollServiceAPIs.PayRoll.Dtos.EnrolledScheme
{
    public class EnrolledSchemeGetAllRequest : PaginationRequest
    {
        public int RegisteredSchemeId { get; set; }
    }
}
