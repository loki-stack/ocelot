﻿namespace PayrollServiceAPIs.PayRoll.Dtos.EnrolledScheme
{
    public class EnrolledSchemeEntityRequest
    {
        public int EnrolledSchemeId { get; set; }
        public int EntityId { get; set; }
    }
}
