﻿using CommonLib.Models.Client;
using PayrollServiceAPIs.Common.Models;
using System.Collections.Generic;

namespace PayrollServiceAPIs.PayRoll.Dtos.EnrolledScheme
{
    public class EnrolledSchemeViewDetail : TbPayEnrolledScheme
    {
        public List<TbPayPayrollItem> VcPayrollItem { get; set; }
    }
}
