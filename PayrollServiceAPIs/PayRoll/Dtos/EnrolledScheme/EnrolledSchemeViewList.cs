﻿using CommonLib.Models.Client;
using PayrollServiceAPIs.Common.Models;
using System;

namespace PayrollServiceAPIs.PayRoll.Dtos.EnrolledScheme
{
    public class EnrolledSchemeViewList
    {
        public int EnrolledSchemeId { get; set; }
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public int EntityId { get; set; }
        public string EntityName { get; set; }
        public string EnrolledSchemeName { get; set; }
        public string EnrolledSchemeCode { get; set; }
        public string EnrolledSchemeType { get; set; }
        public string EnrolledSchemeNo { get; set; }
        public string StatusCd { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
        public EnrolledSchemeViewList(int clientId, string clientName, int entityId, string entityName, TbPayEnrolledScheme model)
        {
            this.ClientId = clientId;
            this.ClientName = clientName;
            this.EntityId = entityId;
            this.EntityName = entityName;
            this.EnrolledSchemeId = model.EnrolledSchemeId;
            this.EnrolledSchemeName = model.EnrolledSchemeName;
            this.EnrolledSchemeCode = model.EnrolledSchemeCode;
            this.EnrolledSchemeNo = model.EnrolledSchemeNo;
            this.EnrolledSchemeType = model.EnrolledSchemeType;
            this.StatusCd = model.StatusCd;
            this.CreatedBy = model.CreatedBy;
            this.CreatedDt = model.CreatedDt;
            this.ModifiedBy = model.ModifiedBy;
            this.ModifiedDt = model.ModifiedDt;
        }

        public EnrolledSchemeViewList()
        {

        }
    }
}
