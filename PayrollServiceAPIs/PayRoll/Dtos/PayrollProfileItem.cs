﻿using System.Text.Json.Serialization;

namespace PayrollServiceAPIs.PayRoll.Dtos
{
    public class PayrollProfileItem
    {
        [JsonIgnore]
        public int PayrollProfileId { get; set; }
        [JsonIgnore]
        public int PayrollItemId { get; set; }
        public string DisplayName { get; set; }
        public byte? DisplayStatus { get; set; }
        public byte? RecurrentItem { get; set; }
        public int? FormulasId { get; set; }
    }
}
