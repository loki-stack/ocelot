﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Dtos.PayrollRun
{
    public class PayrollRunImportError
    {
        public int Row { get; set; }
        public string LabelCode { get; set; }
        public string[] Placeholder { get; set; }
    }
}
