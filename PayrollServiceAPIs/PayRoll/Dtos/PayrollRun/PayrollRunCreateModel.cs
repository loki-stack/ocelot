﻿using CommonLib.Dtos;
using CommonLib.Models.Client;
using PayrollServiceAPIs.Common.Models;
using System.Collections.Generic;

namespace PayrollServiceAPIs.PayRoll.Dtos.PayrollRun
{
    public class PayrollRunCreateModel
    {
        public List<GenericCodeResult> Status { get; set; }
        public List<TbPayPayrollCycle> Cycles { get; set; }
    }
}
