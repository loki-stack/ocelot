﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace PayrollServiceAPIs.PayRoll.Dtos.PayrollRun
{
    public class PayrollRunTransactionStoreModel
    {
        public List<int> EmployeeIds { get; set; }
        public int PayrollItemId { get; set; }
        public int CostCenterId { get; set; }
        public string Currency { get; set; }
        [JsonIgnore]
        public string CreatedBy { get; set; }
    }
}
