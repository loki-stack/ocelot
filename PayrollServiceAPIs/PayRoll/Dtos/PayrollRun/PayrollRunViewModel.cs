﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace PayrollServiceAPIs.PayRoll.Dtos.PayrollRun
{
    public class PayrollRunViewModel
    {
        public int PayrollRunId { get; set; }
        public int PayrollCycleId { get; set; }
        public string Name { get; set; }
        public string ItemCode { get; set; }
        public String StatusCd { get; set; }
        [NotMapped]
        public string StatusText { get; set; }
        public DateTimeOffset DateStart { get; set; }
        public DateTimeOffset DateEnd { get; set; }
        public string Remark { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
    }
}
