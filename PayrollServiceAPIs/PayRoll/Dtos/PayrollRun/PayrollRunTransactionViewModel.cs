﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace PayrollServiceAPIs.PayRoll.Dtos.PayrollRun
{
    public class PayrollRunTransactionViewModel
    {
        public int PayrollRunTransactionId { get; set; }
        public string EmployeeName { get; set; }
        public int EmployeeId { get; set; }
        public string PayrollItemCode { get; set; }
        public byte PaymentSign { get; set; }
        public string Currency { get; set; }
        public int? BaseAmount { get; set; }
        [NotMapped]
        public string Formula { get; set; }
        public int? FormulaId { get; set; }
        public string Unit { get; set; }
        public string AddedBy { get; set; }
        public string DisplayText { get; set; }
        public DateTimeOffset? CalculationCycleStart { get; set; }
        public DateTimeOffset? CalculationCycleEnd { get; set; }
        public DateTimeOffset? TaxCycle { get; set; }
        [NotMapped]
        public int? Row { get; set; }
    }
}
