﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace PayrollServiceAPIs.PayRoll.Dtos.PayrollRun
{
    public class PayrollRunEmployeeStoreModel
    {
        public List<int> EmployeeIds { get; set; }
        public byte RecurrentTransactionFlg { get; set; }
        public byte ScheduleTransactionFlg { get; set; }
        [JsonIgnore]
        public string CreatedBy { get; set; }
    }
}
