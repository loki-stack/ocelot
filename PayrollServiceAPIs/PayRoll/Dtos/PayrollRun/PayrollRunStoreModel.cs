﻿using CommonLib.Validation;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace PayrollServiceAPIs.PayRoll.Dtos.PayrollRun
{
    public class PayrollRunStoreModel
    {
        [Required]
        [MaxLength(100)]
        [ExtraResult(ComponentId = "Name", Level = CommonLib.Constants.Level.ERROR, Placeholder = new string[] { })]
        public string Name { get; set; }
        public string Remark { get; set; }
        [Required]
        [ExtraResult(ComponentId = "StatusCd", Level = CommonLib.Constants.Level.ERROR, Placeholder = new string[] { })]
        public string StatusCd { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        [ExtraResult(ComponentId = "PayrollCycleId", Level = CommonLib.Constants.Level.ERROR, Placeholder = new string[] { })]
        public int PayrollCycleId { get; set; }
        [JsonIgnore]
        public string CreatedBy { get; set; }
        [JsonIgnore]
        public string ModifiedBy { get; set; }
    }
}
