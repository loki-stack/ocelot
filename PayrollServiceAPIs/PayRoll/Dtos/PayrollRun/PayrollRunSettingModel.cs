﻿using System.Collections.Generic;

namespace PayrollServiceAPIs.PayRoll.Dtos.PayrollRun
{
    public class PayrollRunSettingModel
    {
        public PayrollRunViewModel Detail { get; set; }
        public List<PayrollRunEmployeeViewModel> Employee { get; set; }
        public List<PayrollRunTransactionViewModel> Transaction { get; set; }
    }

}
