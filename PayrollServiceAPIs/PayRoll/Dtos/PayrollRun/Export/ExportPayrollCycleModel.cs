﻿using CommonLib.Constants;
using CommonLib.Excel;
using System;

namespace PayrollServiceAPIs.PayRoll.Dtos.PayrollRun.Export
{
    [SheetInfo(Type = (int)SheetType.HORIZONTAL, SheetName = "Payroll Cycles")]
    public class ExportPayrollCycleModel
    {
        [ExcelHeader(HeaderName = "Cycle Name")]
        public string CycleName { get; set; }
        [ExcelHeader(HeaderName = "Start Date")]
        public DateTime? StartDate { get; set; }
        [ExcelHeader(HeaderName = "End Date")]
        public DateTime? EndDate { get; set; }
    }
}
