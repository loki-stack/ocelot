﻿using CommonLib.Constants;
using CommonLib.Excel;
using CommonLib.Excel.Models;
using System;

namespace PayrollServiceAPIs.PayRoll.Dtos.PayrollRun.Export
{
    [SheetInfo(Type = SheetType.VERTICAL, SheetName = "Payroll")]
    public class ExportPayrollModel
    {
        [ExcelHeader(HeaderName = "Client ID")]
        public string ClientId { get; set; }
        [ExcelHeader(HeaderName = "Payroll Run ID")]
        public int PayrollRunId { get; set; }
        [ExcelHeader(HeaderName = "Default Payment Currency")]
        public string DefaultPaymentCurrency { get; set; }
        [ExcelHeader(HeaderName = "Export Time")]
        public DateTime ExportTime { get; set; }
        [ExcelHeader(HeaderName = "Exported By")]
        public string ExportBy { get; set; }
    }
}
