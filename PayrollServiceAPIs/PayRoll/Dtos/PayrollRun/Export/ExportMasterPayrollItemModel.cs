﻿using CommonLib.Constants;
using CommonLib.Excel;
using CommonLib.Excel.Models;

namespace PayrollServiceAPIs.PayRoll.Dtos.PayrollRun.Export
{
    [SheetInfo(Type = (int)SheetType.HORIZONTAL, SheetName = "Master Payroll Items")]
    public class ExportMasterPayrollItemModel
    {
        [ExcelHeader(HeaderName = "Payroll Code")]
        public string PayrollCode { get; set; }
        [ExcelHeader(HeaderName = "Item Name")]
        public string ItemName { get; set; }
        [ExcelHeader(HeaderName = "Sign")]
        public string Sign { get; set; }
        [ExcelHeader(HeaderName = "Recurrence")]
        public string Recurrence { get; set; }
    }
}
