﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Dtos.PayrollRun.Export
{
    public class PayrollRunExportModel
    {
        public string ClientName { get; set; }
        public int BrowserTimeOffset { get; set; }
    }
}
