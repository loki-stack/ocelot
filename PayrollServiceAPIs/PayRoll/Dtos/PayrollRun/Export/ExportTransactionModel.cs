﻿using CommonLib.Constants;
using CommonLib.Excel;
using System;

namespace PayrollServiceAPIs.PayRoll.Dtos.PayrollRun.Export
{
    [SheetInfo(Type = (int)SheetType.HORIZONTAL, SheetName = "Transactions")]
    public class ExportTransactionModel
    {
        [ExcelHeader(HeaderName = "Transaction ID")]
        public int TransactionId { get; set; }
        [ExcelHeader(HeaderName = "Delete transaction")]
        public string DeleteTransaction { get; set; }
        [ExcelHeader(HeaderName = "EmployeeID")]
        public int EmployeeId { get; set; }
        [ExcelHeader(HeaderName = "ItemCode")]
        public string ItemCode { get; set; }
        [ExcelHeader(HeaderName = "DisplayText")]
        public string DisplayText { get; set; }
        [ExcelHeader(HeaderName = "CalculationStartDate (DD/MM/YYYY)")]
        public DateTime? CalculationStartDate { get; set; }
        [ExcelHeader(HeaderName = "CalculationEndDate (DD/MM/YYYY)")]
        public DateTime? CalculationEndDate { get; set; }
        [ExcelHeader(HeaderName = "TaxCycle")]
        public DateTime? TaxCycle { get; set; }
        [ExcelHeader(HeaderName = "EAO713Cycle")]
        public DateTime? EAO713Cycle { get; set; }
        [ExcelHeader(HeaderName = "MPFCycle")]
        public DateTime? MPFCycle { get; set; }
        [ExcelHeader(HeaderName = "BaseAmount")]
        public int? BaseAmount { get; set; }
        [ExcelHeader(HeaderName = "Units")]
        public string Units { get; set; }
        [ExcelHeader(HeaderName = "FinalTransactionAmount")]
        public int? FinalTransactionAmount { get; set; }
        [ExcelHeader(HeaderName = "GLCode")]
        public string GLCode { get; set; }
        [ExcelHeader(HeaderName = "CostCenterCode")]
        public string CostCenterCode { get; set; }
        [ExcelHeader(HeaderName = "InputCurrency")]
        public string InputCurrency { get; set; }
        [ExcelHeader(HeaderName = "PaymentCurrency")]
        public string PaymentCurrency { get; set; }
        [ExcelHeader(HeaderName = "ExchangeRate (Input to Payment)")]
        public double? ExchangeRate { get; set; }
        [ExcelHeader(HeaderName = "Param1")]
        public string Param1 { get; set; }
        [ExcelHeader(HeaderName = "Param2")]
        public string Param2 { get; set; }
        [ExcelHeader(HeaderName = "Param3")]
        public string Param3 { get; set; }
        [ExcelHeader(HeaderName = "Param4")]
        public string Param4 { get; set; }
        [ExcelHeader(HeaderName = "Param5")]
        public string Param5 { get; set; }
        [ExcelHeader(HeaderName = "Param6")]
        public string Param6 { get; set; }
        [ExcelHeader(HeaderName = "Param7")]
        public string Param7 { get; set; }
        [ExcelHeader(HeaderName = "Param8")]
        public string Param8 { get; set; }
        [ExcelHeader(HeaderName = "Param9")]
        public string Param9 { get; set; }
        [ExcelHeader(HeaderName = "Param10")]
        public string Param10 { get; set; }
        [ExcelHeader(HeaderName = "First Name")]
        public string FirstName { get; set; }
        [ExcelHeader(HeaderName = "Payroll Item Name")]
        public string PayrollItemName { get; set; }
        [ExcelHeader(HeaderName = "Sign")]
        public string Sign { get; set; }
        [ExcelHeader(HeaderName = "Recurrence")]
        public string Recurrence { get; set; }
        [ExcelHeader(HeaderName = "Department")]
        public string Department { get; set; }
    }
}
