﻿namespace PayrollServiceAPIs.PayRoll.Dtos.PayrollRun
{
    public class EmployeeNameErrorModel
    {
        public string Name { get; set; }
    }
}
