﻿using CommonLib.Dtos;
using System.Collections.Generic;

namespace PayrollServiceAPIs.PayRoll.Dtos.PayrollRun
{
    public class PayrollRunTranssationCreateModel
    {
        public List<MasterPayrollItemNameModel> MasterPayrollItems { get; set; }
        public List<GenericCodeResult> Currency { get; set; }
    }

    public class MasterPayrollItemNameModel
    {
        public int MasterPayrollItemId { get; set; }
        public string Name { get; set; }
    }
}
