﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Dtos.PayrollRun
{
    public class PayrollRunImportConfirmModel
    {
        public int BrowserTimeOffset { get; set; }
        public List<int> Rows { get; set; }
    }
}
