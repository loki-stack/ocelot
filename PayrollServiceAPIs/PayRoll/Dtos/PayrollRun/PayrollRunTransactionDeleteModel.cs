﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace PayrollServiceAPIs.PayRoll.Dtos.PayrollRun
{
    public class PayrollRunTransactionDeleteModel
    {
        public List<int> PayrollRunTransactionId { get; set; }
        [JsonIgnore]
        public string ModifiedBy { get; set; }
    }
}
