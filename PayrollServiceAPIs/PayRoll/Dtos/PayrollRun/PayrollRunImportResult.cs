﻿using PayrollServiceAPIs.PayRoll.Dtos.PayrollRun.Export;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Dtos.PayrollRun
{
    public class PayrollRunImportResult
    {
        public List<PayrollRunImportError> Errors { get; set; }
        public List<PayrollRunTransactionViewModel> Transactions { get; set; }
        public int? PayrollRunImportId { get; set; }
    }
}
