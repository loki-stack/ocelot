﻿using System.Collections.Generic;

namespace PayrollServiceAPIs.PayRoll.Dtos.PayrollRun
{
    public class PayrollRunEmployeeDeleteModel
    {
        public List<int> EmployeeIds { get; set; }
        public string ModifiedBy { get; set; }
    }
}
