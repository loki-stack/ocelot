﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace PayrollServiceAPIs.PayRoll.Dtos.PayrollRun
{
    public class PayrollRunEmployeeViewModel
    {
        public int? PayrollRunEmployeeId { get; set; }
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public string Division { get; set; }
        public int? Grade { get; set; }
        public string StaffId { get; set; }
        public string StatusCd { get; set; }
        [NotMapped]
        public string StatusText { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset? TerminationDate { get; set; }
        public byte? RecurrentTransactionFlg { get; set; }
        public byte? ScheduleTransactionFlg { get; set; }
        public string Profile { get; set; }
    }
}
