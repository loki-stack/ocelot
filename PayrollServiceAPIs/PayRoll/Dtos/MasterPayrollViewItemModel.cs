﻿using System;
using System.Collections.Generic;

namespace PayrollServiceAPIs.PayRoll.Dtos
{
    public class MasterPayrollViewItemModel
    {
        public int MasterPayrollItemId { get; set; }
        public String ItemName { get; set; }
        public String ItemCode { get; set; }
        public byte? DisplayStatus { get; set; }
        public byte PaymentSign { get; set; }
        public Dictionary<string, bool> PayrollItemFlags { get; set; }
        public bool TaxableFlag { get; set; }




    }
}
