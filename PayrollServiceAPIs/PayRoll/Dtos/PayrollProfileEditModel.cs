﻿using System.Collections.Generic;

namespace PayrollServiceAPIs.PayRoll.Dtos
{
    public class PayrollProfileEditModel
    {
        public PayrollProfileViewModel Detail { get; set; }
        public List<PayrollProfileViewItemModel> Items { get; set; }
        public List<PayrollItemFlag> Types { get; set; }

    }
}
