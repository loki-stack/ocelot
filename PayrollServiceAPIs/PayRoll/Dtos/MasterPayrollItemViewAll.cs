﻿using System.Collections.Generic;

namespace PayrollServiceAPIs.PayRoll.Dtos
{
    public class MasterPayrollItemViewAll
    {
        public List<MasterPayrollItemViewModel> Data { get; set; }
        public List<PayrollItemFlag> Types { get; set; }
    }
}
