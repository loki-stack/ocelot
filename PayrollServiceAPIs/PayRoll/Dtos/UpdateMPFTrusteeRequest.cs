﻿using System.Text.Json.Serialization;

namespace PayrollServiceAPIs.PayRoll.Dtos
{
    public class UpdateMPFTrusteeRequest
    {
        [JsonIgnore]
        public int TrusteeId { get; set; }
        public string TrusteeName { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string EmailAddress { get; set; }
        public string Website { get; set; }
        public string RemittanceStatementFormat { get; set; }
        public bool ProrateRI { get; set; }
        [JsonIgnore]
        public string ModifiedBy { get; set; }
    }
}
