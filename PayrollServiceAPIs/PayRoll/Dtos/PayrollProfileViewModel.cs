﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace PayrollServiceAPIs.PayRoll.Dtos
{
    public class PayrollProfileViewModel
    {
        public int PayrollProfileId { get; set; }
        public string ProfileName { get; set; }
        public int EmployeeCount { get; set; }
        public string StatusCd { get; set; }
        [NotMapped]
        public string StatusText { get; set; }
        public string Remark { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
    }
}
