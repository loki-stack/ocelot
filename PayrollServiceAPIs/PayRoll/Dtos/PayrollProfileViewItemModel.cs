﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace PayrollServiceAPIs.PayRoll.Dtos
{
    public class PayrollProfileViewItemModel
    {
        public int? PayrollProfileItemId { get; set; }
        public int PayrollItemId { get; set; }
        public int? PayrollProfileId { get; set; }
        public string PayrollItemName { get; set; }
        public byte PaymentSign { get; set; }
        public byte? RecurrenceItem { get; set; }
        [NotMapped]
        public bool IntermediateResult { get; set; }
        public byte? DisplayStatus { get; set; }
        [NotMapped]
        public Dictionary<string, bool> PayrollItemFlags { get; set; }
        [NotMapped]
        public bool TaxableFlg { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
    }
}
