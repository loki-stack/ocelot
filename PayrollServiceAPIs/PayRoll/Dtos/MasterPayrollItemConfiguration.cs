﻿using System.Collections.Generic;

namespace PayrollServiceAPIs.PayRoll.Dtos
{
    public class MasterPayrollItemConfiguration
    {
        public PayrollProfileItem Detail { get; set; }
        public List<PayrollItemFlag> Flags { get; set; }
    }
}
