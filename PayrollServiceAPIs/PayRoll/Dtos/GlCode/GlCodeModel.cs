﻿using System.ComponentModel.DataAnnotations.Schema;

namespace PayrollServiceAPIs.Common.Dtos
{
    public class GlCodeModel
    {
        public int GlCodeId { get; set; }
        public string GlCode { get; set; }
        public string Currency { get; set; }
        public string AccountType { get; set; }
        public string StatusCd { get; set; }
        public string Remark { get; set; }
        [NotMapped]
        public string CurrencyText { get; set; }
    }
}
