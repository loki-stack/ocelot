﻿using CommonLib.Validation;
using PayrollServiceAPIs.Common.Constants;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace PayrollServiceAPIs.Common.Dtos
{
    public class GlCodeUpdateModel
    {
        [Required]
        [MaxLength(20)]
        [ExtraResult(ComponentId = "GlCode", Level = Level.ERROR, Placeholder = new string[] { })]
        public string GlCode { get; set; }
        [Required]
        [MaxLength(20)]
        [ExtraResult(ComponentId = "Currency", Level = Level.ERROR, Placeholder = new string[] { })]
        public string Currency { get; set; }
        [Required]
        [MaxLength(20)]
        [ExtraResult(ComponentId = "AccountType", Level = Level.ERROR, Placeholder = new string[] { })]
        public string AccountType { get; set; }
        [Required]
        [RegularExpression("INACTIVE|ACTIVE", ErrorMessage = "The StatusCd must be either 'INACTIVE' or 'ACTIVE' only.")]
        [ExtraResult(ComponentId = "StatusCd", Level = Level.ERROR, Placeholder = new string[] { })]
        public string StatusCd { get; set; }
        public string Remark { get; set; }
        [JsonIgnore]
        public string ModifiedBy { get; set; }
    }
}
