﻿using CommonLib.Dtos;
using System.Collections.Generic;

namespace PayrollServiceAPIs.Common.Dtos
{
    public class GlCodeEditModel
    {
        public GlCodeModel Detail { get; set; }
        public List<GenericCodeResult> Currency { get; set; }
    }
}
