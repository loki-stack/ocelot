﻿using CommonLib.Dtos;
using System.Collections.Generic;

namespace PayrollServiceAPIs.Common.Dtos
{
    public class GlCodeCreateModel
    {
        public List<GenericCodeResult> Currency { get; set; }
    }
}
