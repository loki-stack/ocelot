﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Dtos.PayrollCycleSetting
{
    public class PayrollCycleSettingModel
    {
        public string PayrollCyclePrequency { get; set; }
        public int StartDay { get; set; }
        public int CutOffDay { get; set; }
    }
}
