﻿using CommonLib.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Dtos.PayrollCycleSetting
{
    public class PayrollCycleSettingUpdateModel
    {
        [Required]
        [ExtraResult(ComponentId = "PayrollCyclePrequency", Level = CommonLib.Constants.Level.ERROR, Placeholder = new string[] { })]
        public string PayrollCyclePrequency { get; set; }
        [Required]
        [Range(1, 31)]
        [ExtraResult(ComponentId = "StartDay", Level = CommonLib.Constants.Level.ERROR, Placeholder = new string[] { })]
        public int StartDay { get; set; }
        [Required]
        [Range(1, 31)]
        [ExtraResult(ComponentId = "CutOffDay", Level = CommonLib.Constants.Level.ERROR, Placeholder = new string[] { })]
        public int CutOffDay { get; set; }
    }
}
