﻿using CommonLib.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Dtos.PayrollCycleSetting
{
    public class PayrollCycleSettingEditModel
    {
        public PayrollCycleSettingModel Detail { get; set; }
        public List<GenericCodeResult> Prequency { get; set; }
    }
}
