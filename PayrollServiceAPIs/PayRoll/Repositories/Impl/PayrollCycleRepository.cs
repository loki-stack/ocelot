﻿using CommonLib.Models.Client;
using CommonLib.Services;
using Microsoft.EntityFrameworkCore;
using PayrollServiceAPIs.Common.Constants;
using PayrollServiceAPIs.Common.Models;
using PayrollServiceAPIs.PayRoll.Dtos.PayrollCycle;
using PayrollServiceAPIs.PayRoll.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Repositories.Impl
{
    public class PayrollCycleRepository : IPayrollCycleRepository
    {
        private readonly TenantDBContext _context;

        public PayrollCycleRepository(IConnectionStringContainer connectionStringRepository)
        {
            _context = new TenantDBContext(connectionStringRepository.GetConnectionString());
        }

        /// <summary>
        /// Get ALl Payroll cycle
        /// </summary>
        /// <returns></returns>
        public async Task<List<PayrollCycleViewModel>> GetAll()
        {
            return await _context.Set<PayrollCycleViewModel>().FromSqlRaw("EXECUTE SP_GET_PAYROLL_CYCLE").ToListAsync();
        }

        /// <summary>
        /// Add Payroll cycle
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<int> Store(PayrollCycleStoreModel request)
        {
            TbPayPayrollCycle model = new TbPayPayrollCycle
            {
                CycleCode = request.PayrollCycleCode,
                DateStart = request.DateStart,
                DateEnd = request.DateEnd,
                StatusCd = TableStatusCode.ACTIVE,
                CutOffDate = request.CutOffDate,
                CreatedDt = DateTimeOffset.Now,
                CreatedBy = request.CreatedBy,
                ModifiedDt = DateTimeOffset.Now,
                ModifiedBy = request.CreatedBy
            };
            await _context.AddAsync(model);
            await _context.SaveChangesAsync();
            return model.PayrollCycleId;
        }

        /// <summary>
        /// Update Payroll cycle
        /// </summary>
        /// <param name="payrollCycleId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<int?> Update(int payrollCycleId, PayrollCycleStoreModel request)
        {
            TbPayPayrollCycle model = new TbPayPayrollCycle();
            model = await _context.TbPayPayrollCycle.FindAsync(payrollCycleId);
            if (model != null)
            {
                model.CycleCode = request.PayrollCycleCode;
                model.CutOffDate = request.CutOffDate;
                model.ModifiedDt = DateTimeOffset.Now;
                model.ModifiedBy = request.ModifiedBy;
            }
            else
            {
                return null;
            }
            _context.Entry(model).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return payrollCycleId;
        }

        /// <summary>
        /// Get Payroll cycle by Id
        /// </summary>
        /// <param name="payrollCycleId"></param>
        /// <returns></returns>
        public async Task<PayrollCycleViewModel> GetById(int payrollCycleId)
        {
            return await _context.TbPayPayrollCycle.Where(n => n.PayrollCycleId == payrollCycleId).Select(n => new PayrollCycleViewModel()
            {
                PayrollCycleId = n.PayrollCycleId,
                DateStart = n.DateStart,
                DateEnd = n.DateEnd,
                PayrollCycleCode = n.CycleCode,
                CutOffDate = n.CutOffDate,
                StatusCd = n.StatusCd
            }).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Get Payroll cycle by code
        /// </summary>
        /// <param name="payrollCycleCode"></param>
        /// <returns></returns>
        public async Task<PayrollCycleViewModel> GetByCode(string payrollCycleCode)
        {
            return await _context.TbPayPayrollCycle.Where(n => n.CycleCode == payrollCycleCode).Select(n => new PayrollCycleViewModel()
            {
                PayrollCycleId = n.PayrollCycleId,
                DateStart = n.DateStart,
                DateEnd = n.DateEnd,
                PayrollCycleCode = n.CycleCode,
                CutOffDate = n.CutOffDate,
                StatusCd = n.StatusCd
            }).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Delete Payroll cycle
        /// </summary>
        /// <param name="payrollCycleId"></param>
        /// <param name="modifiedBy"></param>
        /// <returns></returns>
        public async Task<bool> Delete(int payrollCycleId, string modifiedBy)
        {
            TbPayPayrollCycle model = new TbPayPayrollCycle();
            model = await _context.TbPayPayrollCycle.FindAsync(payrollCycleId);
            if (model != null)
            {
                model.StatusCd = TableStatusCode.INACTIVE;
                model.ModifiedDt = DateTimeOffset.Now;
                model.ModifiedBy = modifiedBy;
            }
            else
            {
                return false;
            }
            _context.Entry(model).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// check payroll cycle link payroll run
        /// </summary>
        /// <param name="payrollCycleId"></param>
        /// <returns></returns>
        public async Task<bool> CheckLinkPayrollRun(int payrollCycleId)
        {
            return await _context.TbPayPayrollRun.AnyAsync(n => n.PayrollCycleId == payrollCycleId);
        }

        /// <summary>
        /// Check payrolll cycle by start date
        /// </summary>
        /// <param name="startDate"></param>
        /// <returns></returns>
        public async Task<bool> CheckPayrollCycleByStartDate(DateTimeOffset startDate)
        {
            return await _context.TbPayPayrollCycle.AnyAsync(n => n.DateStart == startDate);
        }
    }
}
