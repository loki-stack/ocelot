﻿using CommonLib.Services;
using CommonLib.Models.Client;
using Microsoft.EntityFrameworkCore;
using PayrollServiceAPIs.Common.Models;
using PayrollServiceAPIs.PayRoll.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Repositories.Impl
{
    public class PayrollItemRepository : IPayrollItemRepository
    {
        private readonly IConnectionStringContainer _connectionStringContainer;
        private readonly string _connectionString;
        public PayrollItemRepository(IConnectionStringContainer connectionStringContainer)
        {
            _connectionStringContainer = connectionStringContainer;
            _connectionString = _connectionStringContainer.GetConnectionString();
        }

        protected TenantDBContext CreateContext(string _connectionString)
        {
            return new TenantDBContext(_connectionString);
        }

        public async Task<List<TbPayPayrollItem>> GetPayRollItemByScheme(int schemeId, string schemeType)
        {
            using (var context = CreateContext(_connectionString))
            {
                List<TbPayPayrollItem> payrollItems = await context.TbPayPayrollItem.Where(x => x.SchemeId == schemeId && x.SchemeType == schemeType).ToListAsync();
                return payrollItems;
            }
        }

        public async Task<List<TbPayPayrollItem>> Create(int schemeId, string schemeType, List<int> masterPayrollItemIds)
        {
            using (var context = CreateContext(_connectionString))
            {
                if (masterPayrollItemIds != null)
                {
                    //remove old payroll item
                    List<TbPayPayrollItem> oldPayrollItems = await context.TbPayPayrollItem.Where(x => x.SchemeId == schemeId && x.SchemeType == schemeType).ToListAsync();
                    if (oldPayrollItems.Count > 0)
                    {
                        context.RemoveRange(oldPayrollItems);
                        await context.SaveChangesAsync();
                    }

                    //add new payroll item
                    List<TbPayPayrollItem> payrollItems = new List<TbPayPayrollItem>();
                    foreach (var id in masterPayrollItemIds)
                    {
                        payrollItems.Add(new TbPayPayrollItem
                        {
                            SchemeId = schemeId,
                            SchemeType = schemeType,
                            MasterPayrollItemId = id
                        });
                    }
                    await context.AddRangeAsync(payrollItems);
                    await context.SaveChangesAsync();
                    return payrollItems;
                }
            }
            return new List<TbPayPayrollItem>();
        }
    }
}
