﻿using CommonLib.Models;
using CommonLib.Models.Core;
using Microsoft.EntityFrameworkCore;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos;
using PayrollServiceAPIs.PayRoll.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Repositories.Impl
{
    public class MPFTrusteeRepository : IMPFTrusteeRepository
    {
        private readonly CoreDbContext _context;
        public MPFTrusteeRepository(CoreDbContext context)
        {
            _context = context;
        }

        public async Task<bool> CheckExist(int trusteeId)
        {
            TbPayMpfTrustee model = await _context.TbPayMpfTrustee.Where(x => x.TrusteeId == trusteeId).FirstOrDefaultAsync();
            if (model != null)
                return true;
            else return false;
        }

        public async Task<List<TbPayMpfTrustee>> GetAll()
        {
            return await _context.TbPayMpfTrustee.ToListAsync();
        }

        public async Task<TbPayMpfTrustee> GetByTrusteeId(int trusteeId)
        {
            return await _context.TbPayMpfTrustee.Where(x => x.TrusteeId == trusteeId).FirstOrDefaultAsync();
        }

        public async Task<UpdateObjectResult> Update(UpdateMPFTrusteeRequest request)
        {
            UpdateObjectResult result = new UpdateObjectResult();
            try
            {
                TbPayMpfTrustee model = _context.TbPayMpfTrustee.Find(request.TrusteeId);
                model.TrusteeName = request.TrusteeName;
                model.Description = request.Description;
                model.Address = request.Address;
                model.PhoneNumber = request.PhoneNumber;
                model.FaxNumber = request.FaxNumber;
                model.EmailAddress = request.EmailAddress;
                model.Website = request.Website;
                model.ModifiedBy = request.ModifiedBy;
                _context.Entry(model).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                result.IsSuceess = true;
            }
            catch (Exception)
            {
                result.IsSuceess = false;
            }
            return result;
        }
    }
}
