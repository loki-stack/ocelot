﻿using AutoMapper;
using CommonLib.Services;
using CommonLib.Models.Client;
using Microsoft.EntityFrameworkCore;
using PayrollServiceAPIs.Common.Models;
using PayrollServiceAPIs.PayRoll.Dtos.ORSOScheme;
using PayrollServiceAPIs.PayRoll.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Repositories.Impl
{
    public class ORSOSchemeRepository : IORSOSchemeRepository
    {
        private readonly string _connectionString;
        private readonly IMapper _mapper;

        public ORSOSchemeRepository(IConnectionStringContainer connectionStringContainer, IMapper mapper)
        {
            _connectionString = connectionStringContainer.GetConnectionString();
            _mapper = mapper;
        }

        protected TenantDBContext CreateContext(string connectionString)
        {
            return new TenantDBContext(connectionString);
        }

        public async Task<List<TbPayOrsoScheme>> GetAll()
        {
            using (var context = CreateContext(_connectionString))
            {
                return await context.TbPayOrsoScheme.ToListAsync();
            }
        }

        public async Task<TbPayOrsoScheme> GetById(int orsoSchemeId)
        {
            using (var context = CreateContext(_connectionString))
            {
                return await context.TbPayOrsoScheme.Where(x => x.OrsoSchemeId == orsoSchemeId).FirstOrDefaultAsync();
            }
        }

        public async Task<TbPayOrsoScheme> Create(ORSOSchemeRequest request, string createdBy)
        {
            using (var context = CreateContext(_connectionString))
            {
                TbPayOrsoScheme model = new TbPayOrsoScheme();
                model = _mapper.Map<ORSOSchemeRequest, TbPayOrsoScheme>(request);
                model.CreatedBy = createdBy;
                model.CreatedDt = DateTime.UtcNow;
                await context.AddAsync(model);
                await context.SaveChangesAsync();
                return model;
            }
        }

        public async Task<TbPayOrsoScheme> Update(int orsoSchemeId, ORSOSchemeRequest request, string modifiedBy)
        {
            using (var context = CreateContext(_connectionString))
            {
                TbPayOrsoScheme model = await context.TbPayOrsoScheme.Where(x => x.OrsoSchemeId == orsoSchemeId).FirstAsync();
                model.OrsoSchemeName = request.OrsoSchemeName;
                model.OrsoSchemeType = request.OrsoSchemeType;
                model.OrsoSchemeCode = request.OrsoSchemeCode;
                model.OrsoExemptionNo = request.OrsoExemptionNo;
                model.ContributionCycle = request.ContributionCycle;
                model.MpfExemptionNo = request.MpfExemptionNo;
                model.MpfExemptionDate = request.MpfExemptionDate;
                model.Domicile = request.Domicile;
                model.SchemeEstablishmentDate = request.SchemeEstablishmentDate;
                model.InvestmentManagerName = request.InvestmentManagerName;
                model.DesignatedPersonName = request.DesignatedPersonName;
                model.SchemeAdministratorName = request.SchemeAdministratorName;
                model.ContactPerson = request.ContactPerson;
                model.PhoneNumber = request.PhoneNumber;
                model.FaxNumber = request.FaxNumber;
                model.ParticipationNo = request.ParticipationNo;
                model.PaymentMethod = request.PaymentMethod;
                model.Currency = request.Currency;
                model.PayCentre = request.PayCentre;
                model.OrsoAdminFee = request.OrsoAdminFee;
                model.OrsoAdminUnit = request.OrsoAdminUnit;
                model.RoundType = request.RoundType;
                model.NearestDec = request.NearestDec;
                model.StatusCd = request.StatusCd;
                model.ClosedDate = request.ClosedDate;
                model.ModifiedBy = modifiedBy;
                model.ModifiedDt = DateTime.UtcNow;
                context.Entry(model).State = EntityState.Modified;
                await context.SaveChangesAsync();
                return model;
            }
        }

        public async Task<bool> Delete(int orsoSchemeId)
        {
            using (var context = CreateContext(_connectionString))
            {
                TbPayOrsoScheme model = await context.TbPayOrsoScheme.Where(x => x.OrsoSchemeId == orsoSchemeId).FirstAsync();
                context.Remove(model);
                await context.SaveChangesAsync();
                return true;
            }
        }

        public bool CheckSchemeCodeExist(string orsoSchemeCode)
        {
            using (var context = CreateContext(_connectionString))
            {
                return context.TbPayOrsoScheme.Any(x => x.OrsoSchemeCode == orsoSchemeCode);
            }
        }
    }
}
