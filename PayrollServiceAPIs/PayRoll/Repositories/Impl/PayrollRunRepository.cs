﻿using CommonLib.Services;
using CommonLib.Models.Client;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using PayrollServiceAPIs.Common.Constants;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.Common.Models;
using PayrollServiceAPIs.PayRoll.Dtos.PayrollRun;
using PayrollServiceAPIs.PayRoll.Dtos.PayrollRun.Export;
using PayrollServiceAPIs.PayRoll.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Repositories.Impl
{
    public class PayrollRunRepository : IPayrollRunRepository
    {
        private readonly TenantDBContext _context;

        public PayrollRunRepository(IConnectionStringContainer connectionStringRepository)
        {
            _context = new TenantDBContext(connectionStringRepository.GetConnectionString());
        }

        /// <summary>
        /// Get ALl Payroll Run
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<List<PayrollRunViewModel>> GetAll(FilterRequest filter)
        {
            var sortParam = new SqlParameter("@sort", string.IsNullOrEmpty(filter.Sort) ? (object)DBNull.Value : filter.Sort);
            var pageParam = new SqlParameter("@page", filter.Page.HasValue ? filter.Page : (object)DBNull.Value);
            var sizeParam = new SqlParameter("@size", filter.Size.HasValue ? filter.Size : (object)DBNull.Value);
            var filterParam = new SqlParameter("@filter", string.IsNullOrEmpty(filter.Filter) ? (object)DBNull.Value : filter.Filter);
            var fullTextSearchParam = new SqlParameter("@fullTextSearch", string.IsNullOrEmpty(filter.FullTextSearch) ? (object)DBNull.Value : filter.FullTextSearch);
            return await _context.Set<PayrollRunViewModel>().FromSqlRaw("EXECUTE SP_GET_PAYROLL_RUN @sort, @page, @size, @filter, @fullTextSearch", sortParam, pageParam, sizeParam, filterParam, fullTextSearchParam).ToListAsync();
        }

        /// <summary>
        /// Add Payroll run
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<int?> Store(PayrollRunStoreModel request)
        {
            TbPayPayrollRun model = new TbPayPayrollRun
            {
                Name = request.Name,
                PayrollCycleId = request.PayrollCycleId,
                Remark = request.Remark,
                StatusCd = request.StatusCd,
                CreatedDt = DateTimeOffset.Now,
                CreatedBy = request.CreatedBy,
                ModifiedDt = DateTimeOffset.Now,
                ModifiedBy = request.CreatedBy
            };
            await _context.AddAsync(model);
            await _context.SaveChangesAsync();
            return model.PayrollRunId;
        }

        /// <summary>
        /// Add Payroll run employee
        /// </summary>
        /// <param name="payrollRunId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task StorePayrollRunEmployee(int payrollRunId, PayrollRunEmployeeStoreModel request)
        {
            foreach (int employeeId in request.EmployeeIds)
            {
                TbPayPayrollRunEmployee model = await _context.TbPayPayrollRunEmployee.Where(n => n.PayrollRunId == payrollRunId && n.EmployeeId == employeeId && n.StatusCd == TableStatusCode.ACTIVE).FirstOrDefaultAsync();

                if (model == null)
                {
                    TbPayPayrollRunEmployee modelAdd = new TbPayPayrollRunEmployee
                    {
                        PayrollRunId = payrollRunId,
                        EmployeeId = employeeId,
                        RecurrentTransactionFlg = request.RecurrentTransactionFlg,
                        ScheduleTransactionFlg = request.ScheduleTransactionFlg,
                        StatusCd = TableStatusCode.ACTIVE,
                        CreatedDt = DateTimeOffset.Now,
                        CreatedBy = request.CreatedBy,
                        ModifiedDt = DateTimeOffset.Now,
                        ModifiedBy = request.CreatedBy
                    };
                    await _context.AddAsync(modelAdd);
                    await _context.SaveChangesAsync();
                }
                else if (model.RecurrentTransactionFlg != request.RecurrentTransactionFlg || model.ScheduleTransactionFlg != request.ScheduleTransactionFlg)
                {
                    model.RecurrentTransactionFlg = request.RecurrentTransactionFlg;
                    model.ScheduleTransactionFlg = request.ScheduleTransactionFlg;
                    model.ModifiedDt = DateTimeOffset.Now;
                    model.ModifiedBy = request.CreatedBy;
                    _context.Entry(model).State = EntityState.Modified;
                    await _context.SaveChangesAsync();
                }
            }
        }

        /// <summary>
        /// Get payroll run employee
        /// </summary>
        /// <param name="payrollRunId"></param>
        /// <returns></returns>
        public async Task<List<int>> GetPayrollRunEmployeeId(int payrollRunId)
        {
            return _context.TbPayPayrollRunEmployee.Where(n => n.PayrollRunId == payrollRunId && n.StatusCd == TableStatusCode.ACTIVE).Select(n => n.EmployeeId).ToList();
        }

        /// <summary>
        /// Add Payroll run transaction
        /// </summary>
        /// <param name="payrollRunId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task StorePayrollRunTransaction(int payrollRunId, PayrollRunTransactionStoreModel request)
        {
            using (var table = new DataTable())
            {
                table.Columns.Add("Id", typeof(int));

                if (request.EmployeeIds != null && request.EmployeeIds.Any())
                {
                    foreach (var item in request.EmployeeIds)
                    {
                        table.Rows.Add(item);
                    }
                }

                var payrollRunIdParam = new SqlParameter("@payrollRunId", payrollRunId);
                var PayrollItemIdParam = new SqlParameter("@PayrollItemId", request.PayrollItemId);
                var CostCenterIdParam = new SqlParameter("@CostCenterId", request.CostCenterId);
                var currencyParam = new SqlParameter("@currency", request.Currency);
                var createByParam = new SqlParameter("@createdBy", request.CreatedBy);
                var employeeIdParams = new SqlParameter("@employeeIds", SqlDbType.Structured)
                {
                    TypeName = "UDT_INT_ARRAY",
                    Value = table
                };

                await _context.Database.ExecuteSqlRawAsync("EXECUTE SP_PAYROLL_RUN_TRANSACTION_STORE @payrollRunId, @PayrollItemId, @CostCenterId, @createdBy, @employeeIds, @currency", payrollRunIdParam, PayrollItemIdParam, CostCenterIdParam, createByParam, employeeIdParams, currencyParam);
            }
        }

        /// <summary>
        /// Update Payroll run
        /// </summary>
        /// <param name="payrollRunId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<int?> Update(int payrollRunId, PayrollRunStoreModel request)
        {
            TbPayPayrollRun model = new TbPayPayrollRun();
            model = await _context.TbPayPayrollRun.FindAsync(payrollRunId);
            if (model != null)
            {
                model.Name = request.Name;
                model.PayrollCycleId = request.PayrollCycleId;
                model.Remark = request.Remark;
                model.StatusCd = request.StatusCd;
                model.ModifiedDt = DateTimeOffset.Now;
                model.ModifiedBy = request.ModifiedBy;
            }
            else
            {
                return null;
            }
            _context.Entry(model).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return payrollRunId;
        }

        /// <summary>
        /// Get Payroll run by Id
        /// </summary>
        /// <param name="payrollRunId"></param>
        /// <returns></returns>
        public async Task<PayrollRunViewModel> GetById(int payrollRunId)
        {
            var payrollRunIdParam = new SqlParameter("@payrollRunId", payrollRunId);
            var listData = await _context.Set<PayrollRunViewModel>().FromSqlRaw("EXECUTE SP_GET_PAYROLL_RUN_BY_ID @payrollRunId", payrollRunIdParam).ToListAsync();
            if (listData.Count > 0)
                return listData.First();
            return null;
        }

        /// <summary>
        /// Duplicate payroll run
        /// </summary>
        /// <param name="payrollRunId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task Duplicate(int payrollRunId, PayrollRunStoreModel request)
        {
            var payrollRunIdParam = new SqlParameter("@payrollRunId", payrollRunId);
            var nameParam = new SqlParameter("@name", request.Name);
            var payrollCycleIdParam = new SqlParameter("@payrollCycleId", request.PayrollCycleId);
            var remarkParam = new SqlParameter("@remark", string.IsNullOrEmpty(request.Remark) ? (object)DBNull.Value : request.Remark);
            var createByParam = new SqlParameter("@createdBy", request.CreatedBy);
            await _context.Database.ExecuteSqlRawAsync("EXECUTE SP_DUPLICATE_PAYROLL_RUN @payrollRunId, @name, @payrollCycleId, @remark, @createdBy", payrollRunIdParam, nameParam, payrollCycleIdParam, remarkParam, createByParam);
        }

        /// <summary>
        /// Get Payroll Run Employee
        /// </summary>
        /// <param name="payrollRunId"></param>
        /// <param name="locate"></param>
        /// <returns></returns>
        public async Task<List<PayrollRunEmployeeViewModel>> GetPayrollRunEmployee(int payrollRunId, string locate)
        {
            var payrollRunIdParam = new SqlParameter("@payrollRunId", payrollRunId);
            var locateParam = new SqlParameter("@locate", locate);
            return await _context.Set<PayrollRunEmployeeViewModel>().FromSqlRaw("EXECUTE SP_GET_PAYROLL_RUN_EMPLOYEE @payrollRunId, @locate", payrollRunIdParam, locateParam).ToListAsync();
        }

        /// <summary>
        /// Get Payroll Run transaction
        /// </summary>
        /// <param name="payrollRunId"></param>
        /// <param name="employeeIds"></param>
        /// <param name="locate"></param>
        /// <returns></returns>
        public async Task<List<PayrollRunTransactionViewModel>> GetPayrollRunTransaction(int payrollRunId, List<int> employeeIds, string locate)
        {
            using (var table = new DataTable())
            {
                table.Columns.Add("Id", typeof(int));

                if (employeeIds != null && employeeIds.Any())
                {
                    foreach (var item in employeeIds)
                    {
                        table.Rows.Add(item);
                    }
                }

                var payrollRunIdParam = new SqlParameter("@payrollRunId", payrollRunId);
                var locateParam = new SqlParameter("@locate", locate);
                var employeeIdParams = new SqlParameter("@employeeIds", SqlDbType.Structured)
                {
                    TypeName = "UDT_INT_ARRAY",
                    Value = table
                };

                return await _context.Set<PayrollRunTransactionViewModel>().FromSqlRaw("EXECUTE SP_GET_PAYROLL_RUN_TRANSACTION @payrollRunId, @employeeIds, @locate", payrollRunIdParam, employeeIdParams, locateParam).ToListAsync();
            }
        }

        /// <summary>
        /// Delete payroll run employee
        /// </summary>
        /// <param name="payrollRunId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="employeeIds"></param>
        /// <returns></returns>
        public async Task DeletePayrollRunEmployee(int payrollRunId, string modifiedBy, List<int> employeeIds)
        {
            using (var table = new DataTable())
            {
                table.Columns.Add("Id", typeof(int));

                if (employeeIds != null && employeeIds.Any())
                {
                    foreach (var item in employeeIds)
                    {
                        table.Rows.Add(item);
                    }
                }

                var payrollRunIdParam = new SqlParameter("@payrollRunId", payrollRunId);
                var modifiedByParam = new SqlParameter("@modifiedBy", modifiedBy);
                var employeeIdParams = new SqlParameter("@employeeIds", SqlDbType.Structured)
                {
                    TypeName = "UDT_INT_ARRAY",
                    Value = table
                };

                await _context.Database.ExecuteSqlRawAsync("EXECUTE SP_DELETE_PAYROLL_RUN_EMPLOYEE @payrollRunId, @modifiedBy, @employeeIds", payrollRunIdParam, modifiedByParam, employeeIdParams);
            }
        }

        /// <summary>
        /// Delete payroll run transaction
        /// </summary>
        /// <param name="modifiedBy"></param>
        /// <param name="PayrollRunTransactionId"></param>
        /// <returns></returns>
        public async Task DeletePayrollRunTransaction(string modifiedBy, List<int> PayrollRunTransactionId)
        {
            using (var table = new DataTable())
            {
                table.Columns.Add("Id", typeof(int));

                if (PayrollRunTransactionId != null && PayrollRunTransactionId.Any())
                {
                    foreach (var item in PayrollRunTransactionId)
                    {
                        table.Rows.Add(item);
                    }
                }

                var modifiedByParam = new SqlParameter("@modifiedBy", modifiedBy);
                var payrollRunTransactionIdParams = new SqlParameter("@payrollRunTrasactionId", SqlDbType.Structured)
                {
                    TypeName = "UDT_INT_ARRAY",
                    Value = table
                };

                await _context.Database.ExecuteSqlRawAsync("EXECUTE SP_DELETE_PAYROLL_RUN_TRANSACTION @payrollRunTrasactionId, @modifiedBy", modifiedByParam, payrollRunTransactionIdParams);
            }
        }

        /// <summary>
        /// Get payroll cyclle
        /// </summary>
        /// <returns></returns>
        public async Task<List<TbPayPayrollCycle>> GetPayrollCycle()
        {
            return await _context.TbPayPayrollCycle.Where(n => n.StatusCd == TableStatusCode.ACTIVE).ToListAsync();
        }

        /// <summary>
        /// Check exist payroll cycle
        /// </summary>
        /// <param name="payrollCycleId"></param>
        /// <returns>
        /// true: exist
        /// false: not exist
        /// </returns>
        public async Task<bool> CheckPayrollCycle(int payrollCycleId)
        {
            return await _context.TbPayPayrollCycle.AnyAsync(n => n.PayrollCycleId == payrollCycleId);
        }

        /// <summary>
        /// Get master payroll item
        /// </summary>
        /// <returns></returns>
        public async Task<List<MasterPayrollItemNameModel>> GetMasterPayrollItems(List<int> employeeIds)
        {
            using (var table = new DataTable())
            {
                table.Columns.Add("Id", typeof(int));

                if (employeeIds != null && employeeIds.Any())
                {
                    foreach (var item in employeeIds)
                    {
                        table.Rows.Add(item);
                    }
                }

                var employeeIdParams = new SqlParameter("@employeeIds", SqlDbType.Structured)
                {
                    TypeName = "UDT_INT_ARRAY",
                    Value = table
                };

                return await _context.Set<MasterPayrollItemNameModel>().FromSqlRaw("EXECUTE SP_GET_PAYROLL_ITEM_AVAIABLE @employeeIds", employeeIdParams).ToListAsync();
            }
        }

        /// <summary>
        /// Get employee not link transaction
        /// </summary>
        /// <param name="request"></param>
        /// <param name="locate"></param>
        /// <returns></returns>
        public async Task<List<EmployeeNameErrorModel>> GetEmployeeNotLinkTransaction(PayrollRunTransactionStoreModel request, string locate)
        {
            using (var table = new DataTable())
            {
                table.Columns.Add("Id", typeof(int));

                if (request.EmployeeIds != null && request.EmployeeIds.Any())
                {
                    foreach (var item in request.EmployeeIds)
                    {
                        table.Rows.Add(item);
                    }
                }

                var payrollItemIdParam = new SqlParameter("@payrollItemId", request.PayrollItemId);
                var locateParam = new SqlParameter("@locate", locate);
                var employeeIdParams = new SqlParameter("@employeeIds", SqlDbType.Structured)
                {
                    TypeName = "UDT_INT_ARRAY",
                    Value = table
                };

                return await _context.Set<EmployeeNameErrorModel>().FromSqlRaw("EXECUTE SP_GET_PAYROLL_RUN_EMPLOYEE_ADD_TRANSACTION_ERROR @payrollItemId, @employeeIds, @locate", payrollItemIdParam, employeeIdParams, locateParam).ToListAsync();
            }
        }

        /// <summary>
        /// Get employee select add to payroll run
        /// </summary>
        /// <param name="locate"></param>
        /// <returns></returns>
        public async Task<List<PayrollRunEmployeeViewModel>> GetEmployeeSelectInPayroll(string locate)
        {
            var locateParam = new SqlParameter("@locate", locate);
            return await _context.Set<PayrollRunEmployeeViewModel>().FromSqlRaw("EXECUTE SP_GET_EMPLOYEE_IN_PAYROLL @locate", locateParam).ToListAsync();
        }

        /// <summary>
        /// get data export transaction
        /// </summary>
        /// <param name="payrollRunId"></param>
        /// <param name="locate"></param>
        /// <returns></returns>
        public async Task<List<ExportTransactionModel>> GetExportTransaction(int payrollRunId, string locate)
        {
                var payrollRunIdParam = new SqlParameter("@payrollRunId", payrollRunId);
                var locateParam = new SqlParameter("@locate", locate);

                return await _context.Set<ExportTransactionModel>().FromSqlRaw("EXECUTE SP_GET_PAYROLL_RUN_EXPORT_TRANSACTION @payrollRunId, @locate", payrollRunIdParam, locateParam).ToListAsync();
        }
          
        /// <summary>
        /// Get data export master payroll item
        /// </summary>
        /// <returns></returns>
        public async Task<List<ExportMasterPayrollItemModel>> GetExportPayrollItem()
        {
            return await _context.TbPayMasterPayrollItem.Where(n => n.StatusCd == TableStatusCode.ACTIVE).Select(n => new ExportMasterPayrollItemModel() { 
                ItemName = n.ItemName,
                PayrollCode = n.ItemCode,
                Sign = n.PaymentSign == 1 ? "+" : "-",
                Recurrence = "Y"
            }).ToListAsync();
        }

        /// <summary>
        /// Get data export payroll cycle
        /// </summary>
        /// <param name="payrollRunId"></param>
        /// <returns></returns>
        public async Task<ExportPayrollCycleModel> GetExportPayrollCycle(int payrollRunId)
        {
            return await (from payRun in _context.TbPayPayrollRun
                          join payCycle in _context.TbPayPayrollCycle on payRun.PayrollCycleId equals payCycle.PayrollCycleId
                          where payRun.PayrollRunId == payrollRunId
                          select new
                          {
                              payCycle.DateStart,
                              payCycle.DateEnd,
                              payCycle.CycleCode
                          }).Select(n => new ExportPayrollCycleModel() { EndDate = n.DateEnd.DateTime, StartDate = n.DateStart.DateTime, CycleName = n.CycleCode }).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Add Payroll run import
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<int?> StorePayrollRunImport(TbPayPayrollRunImport request)
        {
            TbPayPayrollRunImport model = new TbPayPayrollRunImport
            {
                PayrollRunId = request.PayrollRunId,
                FileName = request.FileName,
                Content = request.Content,
                CreatedDt = DateTimeOffset.Now,
                CreatedBy = request.CreatedBy
            };
            await _context.AddAsync(model);
            await _context.SaveChangesAsync();
            return model.PayrollRunImportId;
        }

        /// <summary>
        /// Get Payroll run import
        /// </summary>
        /// <param name="payrollRunImportId"></param>
        /// <param name="payrollRunId"></param>
        /// <returns></returns>
        public async Task<TbPayPayrollRunImport> GetPayrollRunImport(int payrollRunImportId, int payrollRunId)
        {
            return await _context.TbPayPayrollRunImport.Where(n => n.PayrollRunImportId == payrollRunImportId && n.PayrollRunId == payrollRunId).FirstOrDefaultAsync();
        }

        /// <summary>
        /// update data payroll run transaction import
        /// </summary>
        /// <param name="payrollRunId"></param>
        /// <param name="data"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public async Task UpdateDataPayrollRunTransactionImport(int payrollRunId, DataTable data, string userName)
        {
            var payrollRunIdParam = new SqlParameter("@payrollRunId", payrollRunId);
            var userNameParam = new SqlParameter("@userName", userName);
            var dataParams = new SqlParameter("@data", SqlDbType.Structured)
            {
                TypeName = "UDT_PAYROLLRUNTRANSACTION_ARRAY",
                Value = data
            };

            await _context.Database.ExecuteSqlRawAsync("EXECUTE SP_UPDATE_PAYROLL_RUN_TRANSACTION_IMPORT @payrollRunId, @userName, @data", payrollRunIdParam, userNameParam, dataParams);
        }
    }
}
