﻿using CommonLib.Models.Client;
using CommonLib.Services;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using PayrollServiceAPIs.Common.Constants;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.Common.Models;
using PayrollServiceAPIs.PayRoll.Dtos;
using PayrollServiceAPIs.PayRoll.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Repositories.Impl
{
    public class MasterPayrollItemRepository : IMasterPayrollItemRepository
    {

        private readonly TenantDBContext _context;
        public MasterPayrollItemRepository(IConnectionStringContainer connectionStringRepository)
        {
            _context = new TenantDBContext(connectionStringRepository.GetConnectionString());
        }

        /// <summary>
        /// Get All Master Payroll Item
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<List<MasterPayrollViewModel>> GetAll(FilterRequest filter, int clientId, int entityId)
        {
            var clientIdParam = new SqlParameter("@clientId", clientId);
            var entityIdParam = new SqlParameter("@entityId", entityId);
            var sortParam = new SqlParameter("@sort", filter.Sort);
            var pageParam = new SqlParameter("@page", filter.Page.HasValue ? filter.Page : (object)DBNull.Value);
            var sizeParam = new SqlParameter("@size", filter.Size.HasValue ? filter.Size : (object)DBNull.Value);
            var filterParam = new SqlParameter("@filter", string.IsNullOrEmpty(filter.Filter) ? (object)DBNull.Value : filter.Filter);
            var fullTextSearchParam = new SqlParameter("@fullTextSearch", string.IsNullOrEmpty(filter.FullTextSearch) ? (object)DBNull.Value : filter.FullTextSearch);
            return await _context.Set<MasterPayrollViewModel>().FromSqlRaw("EXECUTE GET_MASTER_PAYROLL_ITEM @clientId, @entityId, @sort, @page, @size, @filter, @fullTextSearch", clientIdParam, entityIdParam, sortParam, pageParam, sizeParam, filterParam, fullTextSearchParam).ToListAsync();
        }

        /// <summary>
        /// Add Basic imformation of Master Payroll Item 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<int?> Store(MasterPayrollItemStoreModel request)
        {
            TbPayMasterPayrollItem model = new TbPayMasterPayrollItem
            {
                ClientId = request.ClientId,
                EntityId = request.EntityId,
                ItemName = request.ItemName,
                ItemCode = request.ItemCode,
                PaymentSign = request.PaymentSign,
                Remark = request.Remark,
                StatusCd = request.StatusCd,
                CreatedDt = DateTimeOffset.Now,
                CreatedBy = request.CreatedBy,
                ModifiedDt = DateTimeOffset.Now,
                ModifiedBy = request.ModifiedBy,
                GlCodeId = request.GlCodeId

            };

            await _context.AddAsync(model);
            await _context.SaveChangesAsync();
            return model.PayrollItemId;
        }

        public async Task<bool> StoreMasterPayrollItem(int masterPayrollItemId, string modifiedBy)
        {
            TbPayMasterPayrollItem model = new TbPayMasterPayrollItem
            {
                PayrollItemId = masterPayrollItemId,
                StatusCd = TableStatusCode.ACTIVE,
                CreatedBy = modifiedBy,
                ModifiedBy = modifiedBy,
                CreatedDt = DateTimeOffset.Now,
                ModifiedDt = DateTimeOffset.Now,

            };
            await _context.AddAsync(model);
            await _context.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// Update Master Payroll Item 
        /// </summary>
        /// <param name="masterPayrollItemId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<int?> Update(int masterPayrollItemId, MasterPayrollItemStoreModel request)
        {
            TbPayMasterPayrollItem model = new TbPayMasterPayrollItem();
            model = await _context.TbPayMasterPayrollItem.FindAsync(masterPayrollItemId);
            if (model != null)
            {
                model.ItemName = request.ItemName;
                model.ItemCode = request.ItemCode;
                model.Remark = request.Remark;
                model.StatusCd = request.StatusCd;
                model.PaymentSign = request.PaymentSign;
                model.ModifiedBy = request.ModifiedBy;
                model.GlCodeId = request.GlCodeId;
            }
            else
            {
                return null;
            }
            _context.Entry(model).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return masterPayrollItemId;
        }

        /// <summary>
        /// Add Flag of Master Payroll Item 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<bool> StoreFlag(int payrollItemId, int flagId, string modifiedBy)
        {
            TbPayMasterPayrollItemFlag flag = new TbPayMasterPayrollItemFlag
            {
                PayrollItemId = payrollItemId,
                FlagId = flagId,
                StatusCd = TableStatusCode.ACTIVE,
                CreatedDt = DateTimeOffset.Now,
                ModifiedDt = DateTimeOffset.Now,
                ModifiedBy = modifiedBy
            };
            await _context.AddAsync(flag);
            await _context.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// Add Master Payroll Item 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<bool> StoreTaxValue(int payrolItemId, int taxItemId, int taxValueId, string modifiedBy)
        {
            TbPayMasterPayrollItemTax tax = new TbPayMasterPayrollItemTax
            {

                PayrollItemId = payrolItemId,
                TaxItemId = taxItemId,
                TaxItemValue = taxValueId,
                StatusCd = TableStatusCode.ACTIVE,
                CreatedDt = DateTimeOffset.Now,
                ModifiedDt = DateTimeOffset.Now,
                ModifiedBy = modifiedBy
            };

            await _context.AddAsync(tax);
            await _context.SaveChangesAsync();
            return true;
        }


        /// <summary>
        /// Get payroll item
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public async Task<List<int>> GetMasterPayrollItem(int masterPayrollItemId)
        {
            return await _context.TbPayMasterPayrollItemFlag.Where(n => n.PayrollItemId == masterPayrollItemId && n.StatusCd == TableStatusCode.ACTIVE).Select(n => n.FlagId).ToListAsync();
        }

        public async Task<List<MasterPayrollTaxEditModel>> GetTaxableMasterPayroll(int masterPayrollItemId)
        {
            return await _context.TbPayMasterPayrollItemTax.Where(n => n.PayrollItemId == masterPayrollItemId && n.StatusCd == TableStatusCode.ACTIVE)
                .Select(n => new MasterPayrollTaxEditModel() { TaxItemId = n.TaxItemId, TaxItemValue = n.TaxItemValue }).ToListAsync();
        }

        public async Task<List<MasterPayrollTaxEditModel>> GetTaxableUpdateMasterPayroll(int masterPayrollItemId, string StatusCd)
        {
            return await _context.TbPayMasterPayrollItemTax.Where(n => n.PayrollItemId == masterPayrollItemId && n.StatusCd == TableStatusCode.ACTIVE)
                .Select(n => new MasterPayrollTaxEditModel() { TaxItemId = n.TaxItemId, TaxItemValue = n.TaxItemValue, StatusCd = n.StatusCd }).ToListAsync();
        }

        /// <summary>
        /// Get payroll item tax by payroll item id
        /// </summary>
        /// <param name="payrollItemId"></param>
        /// <returns></returns>
        public async Task<int> CountPayrollItemTaxByPayrollItemId(int payrollItemId)
        {
            return await _context.TbPayMasterPayrollItemTax.Where(n => n.PayrollItemId == payrollItemId && n.StatusCd == TableStatusCode.ACTIVE).CountAsync();
        }


        /// <summary>
        /// Get payroll item flag by payroll item id
        /// </summary>
        /// <param name="payrollItemId"></param>
        /// <returns></returns>
        public async Task<List<TbPayMasterPayrollItemFlag>> GetPayrollItemFlagsByPayrollItemId(int payrollItemId)
        {
            return await _context.TbPayMasterPayrollItemFlag.Where(n => n.PayrollItemId == payrollItemId && n.StatusCd == TableStatusCode.ACTIVE).ToListAsync();
        }


        /// <summary>
        /// Get Master Payroll Item
        /// </summary>
        /// <param name="masterPayrollItem"></param>
        /// <returns></returns>
        public async Task<MasterPayrollViewModel> GetById(int masterPayrollItem)
        {
            return await _context.TbPayMasterPayrollItem.Where(n => n.PayrollItemId == masterPayrollItem).Select(n => new MasterPayrollViewModel() { PayrollItemId = n.PayrollItemId, ItemCode = n.ItemCode, ItemName = n.ItemName, PaymentSign = n.PaymentSign, Remark = n.Remark, StatusCd = n.StatusCd, GlCodeId = n.GlCodeId, ClientId = n.ClientId, EntityId = n.EntityId }).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Delete payroll profile item
        /// </summary>
        /// <param name="masterPayrollFlagId"></param>
        /// <param name="payrollItemId"></param>
        /// <param name="modifiedBy"></param>
        /// <returns></returns>
        public async Task<bool> DeleteMasterPayrollItem(int payrollItemId, int masterPayrollFlagId, string modifiedBy)
        {
            TbPayMasterPayrollItemFlag model = new TbPayMasterPayrollItemFlag();
            model = await _context.TbPayMasterPayrollItemFlag.FirstOrDefaultAsync(n => n.PayrollItemId == payrollItemId && n.FlagId == masterPayrollFlagId && n.StatusCd == TableStatusCode.ACTIVE);
            if (model != null)
            {
                model.ModifiedDt = DateTimeOffset.Now;
                model.ModifiedBy = modifiedBy;
                model.StatusCd = TableStatusCode.INACTIVE;
            }
            else
            {
                return false;
            }
            _context.Entry(model).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// Delete Item Tax Master Payroll Item 
        /// </summary>
        /// <param name="payrollItemId"></param>
        /// <param name="taxitemId"></param>
        /// <param name="modifiedBy"></param>
        /// <returns></returns>
        public async Task<bool> DeleteMasterPayrollItemTax(int payrollItemId, int taxitemId, string modifiedBy)
        {
            TbPayMasterPayrollItemTax model = new TbPayMasterPayrollItemTax();
            model = await _context.TbPayMasterPayrollItemTax.FirstOrDefaultAsync(n => n.PayrollItemId == payrollItemId && n.TaxItemId == taxitemId && n.StatusCd == TableStatusCode.ACTIVE);
            if (model != null)
            {
                model.ModifiedDt = DateTimeOffset.Now;
                model.ModifiedBy = modifiedBy;
                model.StatusCd = TableStatusCode.INACTIVE;
            }

            else
            {
                return false;
            }
            _context.Entry(model).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return true;
        }
        /// <summary>
        /// Get Master Payroll Item By Flag
        /// </summary>
        /// <param name="flagId"></param>
        /// <returns></returns>
        public async Task<List<MasterPayrollItemModel>> GetMasterPayrollItemByFlag(List<int> flagIds)
        {
            string flagIdStr = string.Join(",", flagIds.ToArray());
            var flagIdParam = new SqlParameter("@flagIds", flagIdStr);
            return await _context.Set<MasterPayrollItemModel>().FromSqlRaw("EXECUTE SP_GET_MASTER_PAYROLL_ITEM_BY_FLAG @flagIds", flagIdParam).ToListAsync();
        }

        /// <summary>
        /// Duplicate Master Payroll Item
        /// </summary>
        /// <param name="flagId"></param>
        /// <returns></returns>
        public async Task Duplicate(int masterPayrollItemId, string strCreatedBy, string itemName, string itemCode)
        {
            var masterPayrollItemParam = new SqlParameter("@masterPayrollItemId", masterPayrollItemId);
            var createdByParam = new SqlParameter("@createdBy", strCreatedBy);
            var itemNameParam = new SqlParameter("@ItemName", itemName);
            var itemCodeParam = new SqlParameter("@itemCode", itemCode);
            await _context.Database.ExecuteSqlRawAsync("EXECUTE [SP_DUPLICATE_MASTER_PAYROLL_ITEM] @masterPayrollItemId, @createdBy, @itemName,@itemCode", masterPayrollItemParam, createdByParam, itemNameParam, itemCodeParam);
        }

        /// <summary>
        /// check duplicate code
        /// </summary>
        /// <param name="payrollItemId"></param>
        /// <param name="payrollItemCode"></param>
        /// <returns>
        /// true: not duplicdate
        /// false: duplicate
        /// </returns>
        public async Task<bool> CheckDuplicateCode(int? payrollItemId, string payrollItemCode)
        {
            return !await _context.TbPayMasterPayrollItem.AnyAsync(n => (!payrollItemId.HasValue || n.PayrollItemId != payrollItemId.Value) && n.ItemCode == payrollItemCode);
        }

        /// <summary>
        /// get id payroll item by code
        /// </summary>
        /// <param name="payrollItemCode"></param>
        /// <returns></returns>
        public async Task<MasterPayrollViewModel> GetByCode(string payrollItemCode)
        {
            return await _context.TbPayMasterPayrollItem.Where(n => n.ItemCode == payrollItemCode).Select(n => new MasterPayrollViewModel() { PayrollItemId = n.PayrollItemId, ItemCode = n.ItemCode, ItemName = n.ItemName, PaymentSign = n.PaymentSign, Remark = n.Remark, StatusCd = n.StatusCd, GlCodeId = n.GlCodeId, ClientId = n.ClientId, EntityId = n.EntityId }).FirstOrDefaultAsync();
        }
    }
}
