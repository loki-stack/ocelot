﻿using CommonLib.Services;
using CommonLib.Models.Client;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using PayrollServiceAPIs.Common.Constants;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.Common.Models;
using PayrollServiceAPIs.PayRoll.Dtos;
using PayrollServiceAPIs.PayRoll.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Repositories.Impl
{
    public class PayrollProfileRepository : IPayrollProfileRepository
    {
        private readonly TenantDBContext _context;

        public PayrollProfileRepository(IConnectionStringContainer connectionStringRepository)
        {
            _context = new TenantDBContext(connectionStringRepository.GetConnectionString());
        }

        /// <summary>
        /// Get ALl Payroll Profile
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<List<PayrollProfileViewModel>> GetAll(FilterRequest filter, int clientId, int entityId)
        {
            var clientIdParam = new SqlParameter("@clientId", clientId);
            var entityIdParam = new SqlParameter("@entityId", entityId);
            var sortParam = new SqlParameter("@sort", filter.Sort);
            var pageParam = new SqlParameter("@page", filter.Page.HasValue ? filter.Page : (object)DBNull.Value);
            var sizeParam = new SqlParameter("@size", filter.Size.HasValue ? filter.Size : (object)DBNull.Value);
            var filterParam = new SqlParameter("@filter", string.IsNullOrEmpty(filter.Filter) ? (object)DBNull.Value : filter.Filter);
            var fullTextSearchParam = new SqlParameter("@fullTextSearch", string.IsNullOrEmpty(filter.FullTextSearch) ? (object)DBNull.Value : filter.FullTextSearch);
            return await _context.Set<PayrollProfileViewModel>().FromSqlRaw("EXECUTE SP_GET_PAYROLL_PROFILE @clientId, @entityId, @sort, @page, @size, @filter, @fullTextSearch", clientIdParam, entityIdParam, sortParam, pageParam, sizeParam, filterParam, fullTextSearchParam).ToListAsync();
        }

        /// <summary>
        /// Add Payroll Profile
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<int?> Store(PayrollProfileStoreModel request)
        {
            TbPayPayrollProfile model = new TbPayPayrollProfile
            {
                ClientId = request.ClientId,
                EntityId = request.EntityId,
                ProfileName = request.ProfileName,
                StatusCd = request.StatusCd,
                Remark = request.Remark,
                CreatedDt = DateTimeOffset.Now,
                CreatedBy = request.CreatedBy,
                ModifiedDt = DateTimeOffset.Now,
                ModifiedBy = request.CreatedBy
            };
            await _context.AddAsync(model);
            await _context.SaveChangesAsync();
            return model.PayrollProfileId;
        }

        /// <summary>
        /// Update Payroll Profile
        /// </summary>
        /// <param name="payrollProfileId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<int?> Update(int payrollProfileId, PayrollProfileStoreModel request)
        {
            TbPayPayrollProfile model = new TbPayPayrollProfile();
            model = await _context.TbPayPayrollProfile.FindAsync(payrollProfileId);
            if (model != null)
            {
                model.ProfileName = request.ProfileName;
                model.StatusCd = request.StatusCd;
                model.Remark = request.Remark;
                model.ModifiedDt = DateTimeOffset.Now;
                model.ModifiedBy = request.ModifiedBy;
            }
            else
            {
                return null;
            }
            _context.Entry(model).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return payrollProfileId;
        }

        /// <summary>
        /// Get Payroll Profile by Id
        /// </summary>
        /// <param name="payrollProfileId"></param>
        /// <returns></returns>
        public async Task<PayrollProfileViewModel> GetById(int payrollProfileId)
        {
            var payrollProfileIdParam = new SqlParameter("@payrollProfileId", payrollProfileId);
            var listData = await _context.Set<PayrollProfileViewModel>().FromSqlRaw("EXECUTE SP_GET_PAYROLL_PROFILE_BY_ID @payrollProfileId", payrollProfileIdParam).ToListAsync();
            if (listData.Count > 0)
                return listData.First();
            return null;
        }

        /// <summary>
        /// Get payroll profile item by payroll profile id
        /// </summary>
        /// <param name="payrollProfileId"></param>
        /// <returns></returns>
        public async Task<List<PayrollProfileItem>> GetPayrollProfileItem(int payrollProfileId)
        {
            return await _context.TbPayPayrollProfileItem.Where(n => n.PayrollProfileId == payrollProfileId && n.StatusCd == TableStatusCode.ACTIVE)
                                .Select(n => new PayrollProfileItem() { PayrollItemId = n.PayrollItemId, PayrollProfileId = n.PayrollProfileId }).ToListAsync();
        }

        /// <summary>
        /// Store payroll profile item
        /// </summary>
        /// <param name="payrollProfileId"></param>
        /// <param name="payrollItemId"></param>
        /// <param name="modifiedBy"></param>
        /// <returns></returns>
        public async Task<bool> StorePayrollProfileItem(int payrollProfileId, int payrollItemId, string modifiedBy)
        {
            TbPayPayrollProfileItem model = new TbPayPayrollProfileItem
            {
                PayrollItemId = payrollItemId,
                PayrollProfileId = payrollProfileId,
                StatusCd = TableStatusCode.ACTIVE,
                DisplayStatus = DisplayModel.HIDDEN_IF_ZERO,
                CreatedBy = modifiedBy,
                ModifiedBy = modifiedBy,
                CreatedDt = DateTimeOffset.Now,
                ModifiedDt = DateTimeOffset.Now,
            };
            await _context.AddAsync(model);
            await _context.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// Update payroll profile item
        /// </summary>
        /// <param name="request"></param>
        /// <param name="modifiedBy"></param>
        /// <returns></returns>
        public async Task<bool> UpdatePayrollProfileItem(PayrollProfileItem request, string modifiedBy)
        {
            TbPayPayrollProfileItem model = new TbPayPayrollProfileItem();
            model = await _context.TbPayPayrollProfileItem.FirstOrDefaultAsync(n => n.PayrollProfileId == request.PayrollProfileId && n.PayrollItemId == request.PayrollItemId && n.StatusCd == TableStatusCode.ACTIVE);
            if (model != null)
            {
                model.DisplayName = request.DisplayName;
                model.DisplayStatus = request.DisplayStatus;
                model.RecurrentItem = request.RecurrentItem;
                model.ModifiedDt = DateTimeOffset.Now;
                model.ModifiedBy = modifiedBy;
                model.FormulasId = request.FormulasId;
                _context.Entry(model).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Delete payroll profile item
        /// </summary>
        /// <param name="payrollProfileId"></param>
        /// <param name="payrollItemId"></param>
        /// <param name="modifiedBy"></param>
        /// <returns></returns>
        public async Task<bool> DeletePayrollProfileItem(int payrollProfileId, int payrollItemId, string modifiedBy)
        {
            TbPayPayrollProfileItem model = new TbPayPayrollProfileItem();
            model = await _context.TbPayPayrollProfileItem.FirstOrDefaultAsync(n => n.PayrollProfileId == payrollProfileId && n.PayrollItemId == payrollItemId && n.StatusCd == TableStatusCode.ACTIVE);
            if (model != null)
            {
                model.ModifiedDt = DateTimeOffset.Now;
                model.ModifiedBy = modifiedBy;
                model.StatusCd = TableStatusCode.INACTIVE;
            }
            else
            {
                return false;
            }
            _context.Entry(model).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// Get payroll item
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public async Task<List<PayrollProfileViewItemModel>> GetPayrollItem(int payrollProfileId, int clientId, int entityId)
        {
            var payrollProfileIdParam = new SqlParameter("@payrollProfileId", payrollProfileId);
            var clientIdParam = new SqlParameter("@clientId", clientId);
            var entityIdParam = new SqlParameter("@entityId", entityId);
            return await _context.Set<PayrollProfileViewItemModel>().FromSqlRaw("EXECUTE SP_GET_PAYROLL_PROFILE_ITEM @payrollProfileId, @clientId, @entityId", payrollProfileIdParam, @clientIdParam, @entityIdParam).ToListAsync();
        }

        /// <summary>
        /// Get payroll item flag by payroll item id
        /// </summary>
        /// <param name="payrollItemId"></param>
        /// <returns></returns>
        public async Task<List<TbPayMasterPayrollItemFlag>> GetPayrollItemFlagsByPayrollItemId(int payrollItemId)
        {
            return await _context.TbPayMasterPayrollItemFlag.Where(n => n.PayrollItemId == payrollItemId && n.StatusCd == TableStatusCode.ACTIVE).ToListAsync();
        }

        /// <summary>
        /// Get payroll item tax by payroll item id
        /// </summary>
        /// <param name="payrollItemId"></param>
        /// <returns></returns>
        public async Task<int> CountPayrollItemTaxByPayrollItemId(int payrollItemId)
        {
            return await _context.TbPayMasterPayrollItemTax.Where(n => n.PayrollItemId == payrollItemId && n.StatusCd == TableStatusCode.ACTIVE).CountAsync();
        }

        /// <summary>
        /// Get payroll profile item 
        /// </summary>
        /// <param name="payrollProfileId"></param>
        /// <param name="payrollItemId"></param>
        /// <returns></returns>
        public async Task<PayrollProfileItem> GetPayrollProfileItemConfiguration(int payrollProfileId, int payrollItemId)
        {
            return await _context.TbPayPayrollProfileItem.Where(n => n.PayrollProfileId == payrollProfileId && n.PayrollItemId == payrollItemId && n.StatusCd == TableStatusCode.ACTIVE)
                                .Select(n => new PayrollProfileItem() { PayrollItemId = n.PayrollItemId, PayrollProfileId = n.PayrollProfileId, DisplayName = n.DisplayName, DisplayStatus = n.DisplayStatus, RecurrentItem = n.RecurrentItem, FormulasId = n.FormulasId }).FirstOrDefaultAsync();
        }

        /// <summary>
        /// check master payrol item
        /// </summary>
        /// <param name="payrollItemId"></param>
        /// <returns>
        /// true: exist
        /// false: not exist
        /// </returns>
        public async Task<bool> CheckExistMasterPayrollItem(int payrollItemId)
        {
            return await _context.TbPayMasterPayrollItem.AnyAsync(n => n.PayrollItemId == payrollItemId && n.StatusCd == TableStatusCode.ACTIVE);
        }
        /// <summary>
        /// Duplicate payroll profile (copy: payroll profile, payroll profile item)
        /// </summary>
        /// <param name="payrollProfileId"></param>
        /// <param name="strCreatedBy"></param>
        /// <param name="payrollProfileName"></param>
        /// <returns></returns>
        public async Task Duplicate(int payrollProfileId, string strCreatedBy, string payrollProfileName)
        {
            var payrollProfileIdParam = new SqlParameter("@payrollProfileId", payrollProfileId);
            var createdByParam = new SqlParameter("@createdBy", strCreatedBy);
            var payrollProfileNameParam = new SqlParameter("@payrollProfileName", payrollProfileName);
            await _context.Database.ExecuteSqlRawAsync("EXECUTE SP_DUPLICATE_PAYROLL_PROFILE @payrollProfileId, @createdBy, @payrollProfileName", payrollProfileIdParam, createdByParam, payrollProfileNameParam);
        }

        /// <summary>
        /// Get formula id by payroll item and employeeid
        /// </summary>
        /// <param name="masterPayrollItemId"></param>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public async Task<int?> GetFormulaIdByPayrollItemEmployee(int masterPayrollItemId, int employeeId)
        {
            return await (from profile in _context.TbPayPayrollProfileEmployee
                          join payrollItem in _context.TbPayPayrollProfileItem
                          on profile.PayrollProfileId equals payrollItem.PayrollProfileId
                          where profile.EmployeeId == employeeId && profile.StatusCd == TableStatusCode.ACTIVE && payrollItem.StatusCd == TableStatusCode.ACTIVE && payrollItem.PayrollItemId == masterPayrollItemId

                          select payrollItem.FormulasId).FirstOrDefaultAsync();
        }
    }
}
