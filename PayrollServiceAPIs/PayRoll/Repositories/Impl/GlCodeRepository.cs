﻿using CommonLib.Models.Client;
using CommonLib.Services;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.Common.Models;
using PayrollServiceAPIs.Common.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.Common.Repositories.Impl
{
    public class GlCodeRepository : IGlCodeRepository
    {
        private readonly TenantDBContext _context;

        public GlCodeRepository(IConnectionStringContainer connectionStringRepository)
        {
            _context = new TenantDBContext(connectionStringRepository.GetConnectionString());
        }

        /// <summary>
        /// Get ALl GL code
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<List<GlCodeModel>> GetAll(FilterRequest filter)
        {
            var sortParam = new SqlParameter("@sort", string.IsNullOrEmpty(filter.Sort) ? (object)DBNull.Value : filter.Sort);
            var pageParam = new SqlParameter("@page", filter.Page.HasValue ? filter.Page : (object)DBNull.Value);
            var sizeParam = new SqlParameter("@size", filter.Size.HasValue ? filter.Size : (object)DBNull.Value);
            var filterParam = new SqlParameter("@filter", string.IsNullOrEmpty(filter.Filter) ? (object)DBNull.Value : filter.Filter);
            var fullTextSearchParam = new SqlParameter("@fullTextSearch", string.IsNullOrEmpty(filter.FullTextSearch) ? (object)DBNull.Value : filter.FullTextSearch);
            return await _context.Set<GlCodeModel>().FromSqlRaw("EXECUTE SP_GET_GL_CODE @sort, @page, @size, @filter, @fullTextSearch", sortParam, pageParam, sizeParam, filterParam, fullTextSearchParam).ToListAsync();
        }

        /// <summary>
        /// Add GL code
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<int?> Store(GlCodeStoreModel request)
        {
            TbPayGlCode model = new TbPayGlCode
            {
                GlCode = request.GlCode,
                Currency = request.Currency,
                AccountType = request.AccountType,
                StatusCd = request.StatusCd,
                Remark = request.Remark,
                CreatedDt = DateTimeOffset.Now,
                CreatedBy = request.CreatedBy,
                ModifiedDt = DateTimeOffset.Now,
                ModifiedBy = request.CreatedBy
            };
            await _context.AddAsync(model);
            await _context.SaveChangesAsync();

            return model.GlCodeId;
        }

        /// <summary>
        /// Update GL code
        /// </summary>
        /// <param name="glCodeId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<int?> Update(int glCodeId, GlCodeUpdateModel request)
        {
            TbPayGlCode model = new TbPayGlCode();
            model = await _context.TbPayGlCode.FindAsync(glCodeId);
            if (model != null)
            {
                model.GlCode = request.GlCode;
                model.Currency = request.Currency;
                model.AccountType = request.AccountType;
                model.StatusCd = request.StatusCd;
                model.Remark = request.Remark;
                model.ModifiedDt = DateTimeOffset.Now;
                model.ModifiedBy = request.ModifiedBy;
            }
            else
            {
                return null;
            }
            _context.Entry(model).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return glCodeId;
        }

        /// <summary>
        /// Get GL Code By Id
        /// </summary>
        /// <param name="glCodeId"></param>
        /// <returns></returns>
        public async Task<GlCodeModel> GetById(int glCodeId)
        {
            return await _context.TbPayGlCode.Where(n => n.GlCodeId == glCodeId).Select(n => new GlCodeModel()
            {
                GlCodeId = n.GlCodeId,
                GlCode = n.GlCode,
                Currency = n.Currency,
                AccountType = n.AccountType,
                Remark = n.Remark,
                StatusCd = n.StatusCd
            }).FirstOrDefaultAsync();
        }
    }
}
