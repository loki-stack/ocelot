﻿using AutoMapper;
using CommonLib.Models;
using CommonLib.Models.Core;
using Microsoft.EntityFrameworkCore;
using PayrollServiceAPIs.PayRoll.Dtos;
using PayrollServiceAPIs.PayRoll.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Repositories.Impl
{
    public class RegisteredSchemeRepository : IRegisteredSchemeRepository
    {
        private readonly CoreDbContext _context;
        private readonly IMapper _mapper;
        public RegisteredSchemeRepository(CoreDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<List<MPFRegisteredSchemeViewList>> GetAll()
        {
            List<MPFRegisteredSchemeViewList> viewLists = new List<MPFRegisteredSchemeViewList>();
            var result = await (from mpfScheme in _context.TbPayMpfRegisteredScheme
                                join mpfTrustee in _context.TbPayMpfTrustee
                                on mpfScheme.TrusteeId equals mpfTrustee.TrusteeId into mpfSchemeTrustee
                                from item in mpfSchemeTrustee.DefaultIfEmpty()
                                select new
                                {
                                    mpfScheme,
                                    TrusteeName = item.TrusteeName
                                }).ToListAsync();
            foreach (var item in result)
            {
                MPFRegisteredSchemeViewList viewList = new MPFRegisteredSchemeViewList();
                viewList = _mapper.Map<TbPayMpfRegisteredScheme, MPFRegisteredSchemeViewList>(item.mpfScheme);
                viewList.TrusteeName = item.TrusteeName;
                viewLists.Add(viewList);
            }
            return viewLists;
        }

        public async Task<TbPayMpfRegisteredScheme> Update(int registeredSchemeId, MPFRegisteredSchemeRequest request, string modifiedBy)
        {
            try
            {
                TbPayMpfRegisteredScheme model = _context.TbPayMpfRegisteredScheme.Find(registeredSchemeId);
                model.SchemeNameEn = request.SchemeNameEn;
                model.SchemeNameCn = request.SchemeNameCn;
                model.RemittanceStatementFormat = request.RemittanceStatementFormat;
                model.ProrateRiRemittanceStatement = request.ProrateRiRemittanceStatement;
                model.InterfaceFileFormat = request.InterfaceFileFormat;
                model.Type = request.Type;
                model.FinancialYearDate = request.FinancialYearDate;
                model.ModifiedBy = model.ModifiedBy;
                model.ModifiedDt = DateTime.UtcNow;
                _context.Entry(model).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                return await _context.TbPayMpfRegisteredScheme.Where(x => x.MpfRegisteredSchemeId == registeredSchemeId).FirstAsync();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<bool> Delete(int registeredSchemeId)
        {
            try
            {
                TbPayMpfRegisteredScheme model = _context.TbPayMpfRegisteredScheme.Find(registeredSchemeId);
                _context.TbPayMpfRegisteredScheme.Remove(model);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<TbPayMpfRegisteredScheme> GetById(int registeredSchemeId)
        {
            return await _context.TbPayMpfRegisteredScheme.Where(x => x.MpfRegisteredSchemeId == registeredSchemeId).FirstOrDefaultAsync();
        }
    }
}
