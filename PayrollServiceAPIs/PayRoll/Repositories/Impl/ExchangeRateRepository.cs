﻿using CommonLib.Services;
using CommonLib.Models.Client;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using PayrollServiceAPIs.Common.Constants;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.Common.Models;
using PayrollServiceAPIs.PayRoll.Dtos.ExchangeRate;
using PayrollServiceAPIs.PayRoll.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Repositories.Impl
{
    public class ExchangeRateRepository : IExchangeRateRepository
    {
        private readonly TenantDBContext _context;

        public ExchangeRateRepository(IConnectionStringContainer connectionStringRepository)
        {
            _context = new TenantDBContext(connectionStringRepository.GetConnectionString());
        }

        /// <summary>
        /// Get all exchange rate
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<List<ExchangeRateViewModel>> GetAll(FilterRequest filter)
        {
            var sortParam = new SqlParameter("@sort", string.IsNullOrEmpty(filter.Sort) ? (object)DBNull.Value : filter.Sort);
            var pageParam = new SqlParameter("@page", filter.Page.HasValue ? filter.Page : (object)DBNull.Value);
            var sizeParam = new SqlParameter("@size", filter.Size.HasValue ? filter.Size : (object)DBNull.Value);
            var filterParam = new SqlParameter("@filter", string.IsNullOrEmpty(filter.Filter) ? (object)DBNull.Value : filter.Filter);
            var fullTextSearchParam = new SqlParameter("@fullTextSearch", string.IsNullOrEmpty(filter.FullTextSearch) ? (object)DBNull.Value : filter.FullTextSearch);
            return await _context.Set<ExchangeRateViewModel>().FromSqlRaw("EXECUTE SP_GET_EXCHANGE_RATE @sort, @page, @size, @filter, @fullTextSearch", sortParam, pageParam, sizeParam, filterParam, fullTextSearchParam).ToListAsync();
        }

        /// <summary>
        /// Add exchange rate
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<int?> Store(ExchangeRateStoreModel request)
        {
            TbPayExchangeRate model = new TbPayExchangeRate
            {
                PayrollCycleId = request.PayrollCycleId,
                CurrencyFrom = request.CurrencyFrom,
                CurrencyTo = request.CurrencyTo,
                AverageRate = request.AverageRate,
                MonthEndRate = request.MonthEndRate,
                CreatedDt = DateTimeOffset.Now,
                CreatedBy = request.CreatedBy,
                ModifiedDt = DateTimeOffset.Now,
                ModifiedBy = request.CreatedBy
            };
            await _context.AddAsync(model);
            await _context.SaveChangesAsync();
            return model.ExchangeRateId;
        }

        /// <summary>
        /// Update exchange rate
        /// </summary>
        /// <param name="exchangeRateId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<int?> Update(int exchangeRateId, ExchangeRateStoreModel request)
        {
            TbPayExchangeRate model = new TbPayExchangeRate();
            model = await _context.TbPayExchangeRate.FindAsync(exchangeRateId);
            if (model != null)
            {
                model.PayrollCycleId = request.PayrollCycleId;
                model.CurrencyTo = request.CurrencyTo;
                model.CurrencyFrom = request.CurrencyFrom;
                model.AverageRate = request.AverageRate;
                model.MonthEndRate = request.MonthEndRate;
                model.ModifiedDt = DateTimeOffset.Now;
                model.ModifiedBy = request.ModifiedBy;
            }
            else
            {
                return null;
            }
            _context.Entry(model).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return exchangeRateId;
        }

        /// <summary>
        /// Get exchange rate by id
        /// </summary>
        /// <param name="exchangeRateId"></param>
        /// <returns></returns>
        public async Task<ExchangeRateViewModel> GetById(int exchangeRateId)
        {
            var exchangeRateIdParam = new SqlParameter("@exchangeRateId", exchangeRateId);
            var result = await _context.Set<ExchangeRateViewModel>().FromSqlRaw("EXECUTE SP_GET_EXCHANGE_RATE_DEATAIL @exchangeRateId", exchangeRateIdParam).ToListAsync();

            if (result.Any())
                return result.First();
            return null;
        }

        /// <summary>
        /// get handle periold
        /// </summary>
        /// <returns></returns>
        public async Task<List<PayrollCycleItemModel>> GetPayrollCycle()
        {
            return await _context.TbPayPayrollCycle.Where(n => n.StatusCd == TableStatusCode.ACTIVE).Select(n => new PayrollCycleItemModel() { 
                PayrollCycleId = n.PayrollCycleId,
                StartDate = n.DateStart,
                EndDate = n.DateEnd
            }).ToListAsync();
        }

        /// <summary>
        /// check handle period exist
        /// </summary>
        /// <param name="PayrollCycleId"></param>
        /// <returns></returns>
        public async Task<bool> CheckPayrollCycleExist(int PayrollCycleId)
        {
            return await _context.TbPayPayrollCycle.AnyAsync(n => n.PayrollCycleId == PayrollCycleId);
        }
    }
}
