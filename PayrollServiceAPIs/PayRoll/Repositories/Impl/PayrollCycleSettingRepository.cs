﻿using CommonLib.Models.Client;
using CommonLib.Services;
using Microsoft.EntityFrameworkCore;
using PayrollServiceAPIs.Common.Models;
using PayrollServiceAPIs.PayRoll.Dtos.PayrollCycleSetting;
using PayrollServiceAPIs.PayRoll.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Repositories.Impl
{
    public class PayrollCycleSettingRepository : IPayrollCycleSettingRepository
    {
        private readonly TenantDBContext _context;
        public PayrollCycleSettingRepository(IConnectionStringContainer connectionStringRepository)
        {
            _context = new TenantDBContext(connectionStringRepository.GetConnectionString());
        }

        /// <summary>
        /// Get Setting Payroll Cycle
        /// </summary>
        /// <returns></returns>
        public async Task<PayrollCycleSettingModel> GetSeting()
        {
            return await _context.TbPayPayrollCycleSetting.Select(n => new PayrollCycleSettingModel() { 
                PayrollCyclePrequency = n.PayrollCyclePrequency,
                StartDay = n.StartDay,
                CutOffDay = n.CutOffDay
            }).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Update Payroll cycle Setting
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task Update(PayrollCycleSettingUpdateModel request)
        {
            TbPayPayrollCycleSetting model = await _context.TbPayPayrollCycleSetting.FirstOrDefaultAsync();

            if(model == null)
            {
                model = new TbPayPayrollCycleSetting
                {
                    PayrollCyclePrequency = request.PayrollCyclePrequency,
                    StartDay = request.StartDay,
                    CutOffDay = request.CutOffDay
                };
                await _context.AddAsync(model);
                await _context.SaveChangesAsync();
                
            }
            else
            {
                model.PayrollCyclePrequency = request.PayrollCyclePrequency;
                model.StartDay = request.StartDay;
                model.CutOffDay = request.CutOffDay;
                _context.Entry(model).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
        }
    }
}
