﻿using CommonLib.Models;
using CommonLib.Models.Client;
using CommonLib.Models.Core;
using CommonLib.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using PayrollServiceAPIs.Common.Models;
using PayrollServiceAPIs.PayRoll.Dtos.EnrolledScheme;
using PayrollServiceAPIs.PayRoll.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Repositories.Impl
{
    public class EnrolledSchemeRepository : IEnrolledSchemeRepository
    {
        protected readonly IConnectionStringContainer _connectionStringContainer;
        protected Dictionary<int, string> _connectionStrings;
        protected readonly CoreDbContext _coreDBContext;
        protected readonly ILogger<EnrolledSchemeRepository> _logger;

        public EnrolledSchemeRepository(IConnectionStringContainer connectionStringContainer, CoreDbContext coreDBContext, ILogger<EnrolledSchemeRepository> logger)
        {
            _connectionStringContainer = connectionStringContainer;
            _coreDBContext = coreDBContext;
            _logger = logger;
            _connectionStrings = _connectionStringContainer.GetListConnectionString();
        }

        protected TenantDBContext CreateContext(string _connectionString)
        {
            return new TenantDBContext(_connectionString);
        }

        public async Task<List<EnrolledSchemeViewList>> GetByRegisteredSchemeId(int registeredSchemeId)
        {
            List<EnrolledSchemeViewList> viewLists = new List<EnrolledSchemeViewList>();
            foreach (var connectionString in _connectionStrings)
            {
                using (var _context = CreateContext(connectionString.Value))
                {
                    int entityId = connectionString.Key;
                    var entity = _coreDBContext.TbCamEntity.Where(x => x.EntityId == entityId).First();
                    var client = _coreDBContext.TbCamClient.Where(x => x.ClientId == entity.ParentClientId).First();
                    var enrolledSchemes = await _context.TbPayEnrolledScheme.Where(x => x.MpfRegisteredSchemeId == registeredSchemeId).ToListAsync();
                    foreach (var enrolledScheme in enrolledSchemes)
                    {
                        viewLists.Add(new EnrolledSchemeViewList(client.ClientId, client.ClientNameTxt, entity.EntityId, entity.EntityNameTxt, enrolledScheme));
                    }
                }
            }
            return viewLists;
        }

        public async Task<EnrolledSchemeViewList> Create(EnrolledSchemeRequest request, int entityId, string createdBy)
        {
            if (_connectionStrings.ContainsKey(entityId))
            {
                string connectionStr = _connectionStrings[entityId];
                using (var _context = CreateContext(connectionStr))
                {
                    try
                    {
                        TbPayEnrolledScheme model = new TbPayEnrolledScheme
                        {
                            MpfRegisteredSchemeId = request.MPFRegisteredSchemeId,
                            EnrolledSchemeName = request.EnrolledSchemeName,
                            EnrolledSchemeCode = request.EnrolledSchemeCode,
                            EnrolledSchemeType = request.EnrolledSchemeType,
                            EnrolledSchemeNo = request.EnrolledSchemeNo,
                            ContributionCycle = request.ContributionCycle,
                            ContactPerson = request.ContactPerson,
                            PhoneNumber = request.PhoneNumber,
                            FaxNumber = request.FaxNumber,
                            ParticipationNo = request.ParticipationNo,
                            PaymentMethod = request.PaymentMethod,
                            Currency = request.Currency,
                            PayCentre = request.PayCentre,
                            RoundType = request.RoundType,
                            NearestDec = request.NearestDec,
                            StatusCd = request.StatusCd,
                            CreatedBy = createdBy,
                            CreatedDt = DateTime.UtcNow
                        };
                        model.ClosedDate = request.ClosedDate;
                        await _context.TbPayEnrolledScheme.AddAsync(model);
                        await _context.SaveChangesAsync();

                        var entity = _coreDBContext.TbCamEntity.Where(x => x.EntityId == entityId).First();
                        var client = _coreDBContext.TbCamClient.Where(x => x.ClientId == entity.ParentClientId).First();
                        EnrolledSchemeViewList viewList = new EnrolledSchemeViewList(client.ClientId, client.ClientNameTxt, entity.EntityId, entity.EntityNameTxt, model);
                        return viewList;
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex.Message);
                        return null;
                    }
                }
            }
            return null;
        }

        public async Task<bool> CheckExist(int enrolledSchemeId, int entityId)
        {
            if (_connectionStrings.ContainsKey(entityId))
            {
                string connectionStr = _connectionStrings[entityId];
                using (var _context = CreateContext(connectionStr))
                {
                    TbPayEnrolledScheme model = await _context.TbPayEnrolledScheme.Where(x => x.EnrolledSchemeId == enrolledSchemeId).FirstOrDefaultAsync();
                    if (model != null)
                        return true;
                    return false;
                }
            }
            return false;
        }

        public async Task<EnrolledSchemeViewList> Update(int enrolledSchemeId, EnrolledSchemeRequest request, int entityId, string modifiedBy)
        {
            if (_connectionStrings.ContainsKey(entityId))
            {
                string connectionStr = _connectionStrings[entityId];
                using (var _context = CreateContext(connectionStr))
                {
                    try
                    {
                        TbPayEnrolledScheme model = await _context.TbPayEnrolledScheme.FindAsync(enrolledSchemeId);
                        model.MpfRegisteredSchemeId = request.MPFRegisteredSchemeId;
                        model.EnrolledSchemeName = request.EnrolledSchemeName;
                        model.EnrolledSchemeCode = request.EnrolledSchemeCode;
                        model.EnrolledSchemeType = request.EnrolledSchemeType;
                        model.EnrolledSchemeNo = request.EnrolledSchemeNo;
                        model.ContributionCycle = request.ContributionCycle;
                        model.ContactPerson = request.ContactPerson;
                        model.PhoneNumber = request.PhoneNumber;
                        model.FaxNumber = request.FaxNumber;
                        model.ParticipationNo = request.ParticipationNo;
                        model.PaymentMethod = request.PaymentMethod;
                        model.Currency = request.Currency;
                        model.PayCentre = request.PayCentre;
                        model.RoundType = request.RoundType;
                        model.NearestDec = request.NearestDec;
                        model.StatusCd = request.StatusCd;
                        model.ClosedDate = request.ClosedDate;
                        model.ModifiedBy = modifiedBy;
                        model.ModifiedDt = DateTime.UtcNow;
                        _context.Entry(model).State = EntityState.Modified;
                        await _context.SaveChangesAsync();
                        var entity = _coreDBContext.TbCamEntity.Where(x => x.EntityId == entityId).First();
                        var client = _coreDBContext.TbCamClient.Where(x => x.ClientId == entity.ParentClientId).First();
                        EnrolledSchemeViewList viewList = new EnrolledSchemeViewList(client.ClientId, client.ClientNameTxt, entity.EntityId, entity.EntityNameTxt, model);
                        return viewList;
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex.Message);
                        return null;
                    }
                }
            }
            return null;
        }

        public async Task<bool> Delete(int schemeId, int entityId)
        {
            if (_connectionStrings.ContainsKey(entityId))
            {
                string connectionStr = _connectionStrings[entityId];
                using (var _context = CreateContext(connectionStr))
                {
                    try
                    {
                        TbPayEnrolledScheme model = await _context.TbPayEnrolledScheme.FindAsync(schemeId);
                        _context.TbPayEnrolledScheme.Remove(model);
                        _context.Entry(model).State = EntityState.Deleted;
                        await _context.SaveChangesAsync();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex.Message);
                        return false;
                    }
                }
            }
            return false;
        }

        public async Task<bool> CheckEnrolledSchemeCode(string enrolledSchemeCode, int entityId)
        {
            if (_connectionStrings.ContainsKey(entityId))
            {
                string connectionStr = _connectionStrings[entityId];
                using (var _context = CreateContext(connectionStr))
                {
                    TbPayEnrolledScheme model = await _context.TbPayEnrolledScheme.Where(x => x.EnrolledSchemeCode == enrolledSchemeCode).FirstOrDefaultAsync();
                    if (model != null)
                        return true;
                    else return false;
                }
            }
            return false;
        }

        public async Task<TbPayEnrolledScheme> GetByEnrolledSchemeId(int enrolledSchemeId, int entityId)
        {
            if (_connectionStrings.ContainsKey(entityId))
            {
                string connectionStr = _connectionStrings[entityId];
                using (var _context = CreateContext(connectionStr))
                {
                    TbPayEnrolledScheme enrolledScheme = await _context.TbPayEnrolledScheme.Where(x => x.EnrolledSchemeId == enrolledSchemeId).FirstOrDefaultAsync();
                    return enrolledScheme;
                }
            }
            return null;
        }

        public async Task<bool> DeleteMany(List<EnrolledSchemeEntityRequest> request)
        {
            try
            {
                Dictionary<int, List<int>> entityEnrollScheme = new Dictionary<int, List<int>>();
                foreach (var item in request)
                {
                    if (entityEnrollScheme.ContainsKey(item.EntityId))
                    {
                        entityEnrollScheme[item.EntityId].Add(item.EnrolledSchemeId);
                    }
                    else
                    {
                        entityEnrollScheme.Add(item.EntityId, new List<int> { item.EnrolledSchemeId });
                    }
                }
                foreach (var item in entityEnrollScheme)
                {
                    if (_connectionStrings.ContainsKey(item.Key))
                    {
                        string connectionStr = _connectionStrings[item.Key];
                        using (var _context = CreateContext(connectionStr))
                        {
                            _context.TbPayEnrolledScheme.RemoveRange(_context.TbPayEnrolledScheme.Where(x => item.Value.Contains(x.EnrolledSchemeId)));
                            await _context.SaveChangesAsync();
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return false;
            }
        }

        public bool CheckSchemeCodeExist(string enrolledSchemeCode, int entityId)
        {
            if (_connectionStrings.ContainsKey(entityId))
            {
                string connectionStr = _connectionStrings[entityId];
                using (var _context = CreateContext(connectionStr))
                {
                    return _context.TbPayEnrolledScheme.Any(x => x.EnrolledSchemeCode == enrolledSchemeCode);
                }
            }
            return false;
        }
    }
}
