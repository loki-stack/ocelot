﻿using PayrollServiceAPIs.Common.Models;
using PayrollServiceAPIs.PayRoll.Dtos.ORSOScheme;
using System.Collections.Generic;
using System.Threading.Tasks;
using CommonLib.Models.Client;

namespace PayrollServiceAPIs.PayRoll.Repositories.Interfaces
{
    public interface IORSOSchemeRepository
    {
        Task<List<TbPayOrsoScheme>> GetAll();
        Task<TbPayOrsoScheme> GetById(int orsoSchemeId);
        Task<TbPayOrsoScheme> Create(ORSOSchemeRequest request, string createdBy);
        Task<TbPayOrsoScheme> Update(int orsoSchemeId, ORSOSchemeRequest request, string modifiedBy);
        Task<bool> Delete(int orsoSchemeId);
        bool CheckSchemeCodeExist(string orsoSchemeCode);
    }
}
