﻿using PayrollServiceAPIs.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using CommonLib.Models.Client;

namespace PayrollServiceAPIs.PayRoll.Repositories.Interfaces
{
    public interface IPayrollItemRepository
    {
        Task<List<TbPayPayrollItem>> GetPayRollItemByScheme(int schemeId, string schemeType);
        Task<List<TbPayPayrollItem>> Create(int schemeId, string schemeType, List<int> masterPayrollItemIds);
    }
}
