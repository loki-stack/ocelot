﻿using PayrollServiceAPIs.Common.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.Common.Repositories.Interfaces
{
    public interface IGlCodeRepository
    {
        Task<List<GlCodeModel>> GetAll(FilterRequest filter);
        Task<int?> Store(GlCodeStoreModel request);
        Task<int?> Update(int glCodeId, GlCodeUpdateModel request);
        Task<GlCodeModel> GetById(int glCodeId);
    }
}
