﻿using PayrollServiceAPIs.Common.Models;
using PayrollServiceAPIs.PayRoll.Dtos.EnrolledScheme;
using System.Collections.Generic;
using System.Threading.Tasks;
using CommonLib.Models.Client;

namespace PayrollServiceAPIs.PayRoll.Repositories.Interfaces
{
    public interface IEnrolledSchemeRepository
    {
        Task<List<EnrolledSchemeViewList>> GetByRegisteredSchemeId(int registeredSchemeId);
        bool CheckSchemeCodeExist(string enrolledSchemeCode, int entityId);
        Task<bool> CheckExist(int enrolledSchemeId, int entityId);
        Task<TbPayEnrolledScheme> GetByEnrolledSchemeId(int enrolledSchemeId, int entityId);
        Task<EnrolledSchemeViewList> Create(EnrolledSchemeRequest request, int entityId, string createdBy);
        Task<EnrolledSchemeViewList> Update(int enrolledSchemeId, EnrolledSchemeRequest request, int entityId, string modifiedBy);
        Task<bool> Delete(int enrolledSchemeId, int entityId);
        Task<bool> DeleteMany(List<EnrolledSchemeEntityRequest> request);
    }
}
