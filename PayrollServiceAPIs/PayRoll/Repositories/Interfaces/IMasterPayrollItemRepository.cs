﻿using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Repositories.Interfaces
{
    public interface IMasterPayrollItemRepository
    {

        Task<int?> Store(MasterPayrollItemStoreModel request);
        Task<bool> StoreMasterPayrollItem(int masterPayrollItemId, string modifiedBy);
        Task<int?> Update(int masterPayrollItemId, MasterPayrollItemStoreModel request);
        Task<bool> StoreFlag(int payrollItemId, int FlagId, string modifiedBy);
        Task<bool> StoreTaxValue(int payrolItemId, int taxItemId, int taxValueId, string modifiedBy);
        Task<List<MasterPayrollViewModel>> GetAll(FilterRequest filter, int clientId, int entityId);
        Task<List<int>> GetMasterPayrollItem(int masterPayrollItemId);
        Task<int> CountPayrollItemTaxByPayrollItemId(int payrollItemId);
        Task<MasterPayrollViewModel> GetById(int masterPayrollItem);
        Task<bool> DeleteMasterPayrollItem(int payrollItemId, int masterPayrollFlagId, string modifiedBy);
        Task<List<MasterPayrollTaxEditModel>> GetTaxableMasterPayroll(int masterPayrollItemId);
        Task<bool> DeleteMasterPayrollItemTax(int payrollItemId, int taxitemId, string modifiedBy);
        Task<List<MasterPayrollTaxEditModel>> GetTaxableUpdateMasterPayroll(int masterPayrollItemId, string StatusCd);
        Task Duplicate(int masterPayrollItemId, string strCreatedBy, string ItemName, string ItemCode);
        Task<List<MasterPayrollItemModel>> GetMasterPayrollItemByFlag(List<int> flagIds);
        Task<bool> CheckDuplicateCode(int? payrollItemId, string payrollItemCode);
        Task<MasterPayrollViewModel> GetByCode(string payrollItemCode);
    }
}
