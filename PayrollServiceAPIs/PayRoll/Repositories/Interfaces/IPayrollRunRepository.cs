﻿using CommonLib.Models.Client;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.Common.Models;
using PayrollServiceAPIs.PayRoll.Dtos.PayrollRun;
using PayrollServiceAPIs.PayRoll.Dtos.PayrollRun.Export;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Repositories.Interfaces
{
    public interface IPayrollRunRepository
    {
        Task<List<PayrollRunViewModel>> GetAll(FilterRequest filter);
        Task<int?> Store(PayrollRunStoreModel request);
        Task<int?> Update(int payrollRunId, PayrollRunStoreModel request);
        Task<PayrollRunViewModel> GetById(int payrollRunId);
        Task<List<PayrollRunEmployeeViewModel>> GetPayrollRunEmployee(int payrollRunId, string locate);
        Task<List<PayrollRunTransactionViewModel>> GetPayrollRunTransaction(int payrollRunId, List<int> employeeIds, string locate);
        Task Duplicate(int payrollRunId, PayrollRunStoreModel request);
        Task StorePayrollRunTransaction(int payrollRunId, PayrollRunTransactionStoreModel request);
        Task StorePayrollRunEmployee(int payrollRunId, PayrollRunEmployeeStoreModel request);
        Task DeletePayrollRunEmployee(int payrollRunId, string modifiedBy, List<int> EmployeeIds);
        Task DeletePayrollRunTransaction(string modifiedBy, List<int> PayrollRunTransactionId);
        Task<List<int>> GetPayrollRunEmployeeId(int payrollRunId);
        Task<List<TbPayPayrollCycle>> GetPayrollCycle();
        Task<bool> CheckPayrollCycle(int payrollCycleId);
        Task<List<MasterPayrollItemNameModel>> GetMasterPayrollItems(List<int> employeeIds);
        Task<List<ExportTransactionModel>> GetExportTransaction(int payrollRunId, string locate);
        Task<List<ExportMasterPayrollItemModel>> GetExportPayrollItem();
        Task<ExportPayrollCycleModel> GetExportPayrollCycle(int payrollRunId);
        Task<List<EmployeeNameErrorModel>> GetEmployeeNotLinkTransaction(PayrollRunTransactionStoreModel request, string locate);
        Task<List<PayrollRunEmployeeViewModel>> GetEmployeeSelectInPayroll(string locate);
        Task<int?> StorePayrollRunImport(TbPayPayrollRunImport request);
        Task<TbPayPayrollRunImport> GetPayrollRunImport(int payrollRunImportId, int payrollRunId);
        Task UpdateDataPayrollRunTransactionImport(int payrollRunId, DataTable data, string userName);
    }
}
