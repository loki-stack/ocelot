﻿using CommonLib.Models;
using CommonLib.Models.Core;
using PayrollServiceAPIs.PayRoll.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Repositories.Interfaces
{
    public interface IRegisteredSchemeRepository
    {
        Task<List<MPFRegisteredSchemeViewList>> GetAll();
        Task<TbPayMpfRegisteredScheme> GetById(int registeredSchemeId);
        Task<TbPayMpfRegisteredScheme> Update(int registeredSchemeId, MPFRegisteredSchemeRequest request, string modifiedBy);
        Task<bool> Delete(int registeredSchemeId);
    }
}
