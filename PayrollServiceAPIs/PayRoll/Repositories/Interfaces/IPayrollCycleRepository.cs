﻿using PayrollServiceAPIs.PayRoll.Dtos.PayrollCycle;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Repositories.Interfaces
{
    public interface IPayrollCycleRepository
    {
        Task<List<PayrollCycleViewModel>> GetAll();
        Task<int> Store(PayrollCycleStoreModel request);
        Task<int?> Update(int payrollRunId, PayrollCycleStoreModel request);
        Task<PayrollCycleViewModel> GetById(int payrollCycleId);
        Task<PayrollCycleViewModel> GetByCode(string payrollCycleCode);
        Task<bool> Delete(int payrollCycleId, string modifiedBy);
        Task<bool> CheckLinkPayrollRun(int payrollCycleId);
        Task<bool> CheckPayrollCycleByStartDate(DateTimeOffset startDate);
    }
}
