﻿using PayrollServiceAPIs.PayRoll.Dtos.PayrollCycleSetting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Repositories.Interfaces
{
    public interface IPayrollCycleSettingRepository
    {
        Task<PayrollCycleSettingModel> GetSeting();
        Task Update(PayrollCycleSettingUpdateModel request);
    }
}
