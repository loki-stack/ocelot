﻿using CommonLib.Models;
using CommonLib.Models.Core;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Repositories.Interfaces
{
    public interface IMPFTrusteeRepository
    {
        Task<bool> CheckExist(int trusteeId);
        Task<List<TbPayMpfTrustee>> GetAll();
        Task<TbPayMpfTrustee> GetByTrusteeId(int trusteeId);
        Task<UpdateObjectResult> Update(UpdateMPFTrusteeRequest request);
    }
}
