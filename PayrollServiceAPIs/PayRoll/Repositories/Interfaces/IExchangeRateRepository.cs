﻿using CommonLib.Models.Client;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.Common.Models;
using PayrollServiceAPIs.PayRoll.Dtos.ExchangeRate;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Repositories.Interfaces
{
    public interface IExchangeRateRepository
    {
        Task<List<ExchangeRateViewModel>> GetAll(FilterRequest filter);
        Task<int?> Store(ExchangeRateStoreModel request);
        Task<int?> Update(int exchangeRateId, ExchangeRateStoreModel request);
        Task<ExchangeRateViewModel> GetById(int exchangeRateId);
        Task<List<PayrollCycleItemModel>> GetPayrollCycle();
        Task<bool> CheckPayrollCycleExist(int handlePeriodId);
    }
}
