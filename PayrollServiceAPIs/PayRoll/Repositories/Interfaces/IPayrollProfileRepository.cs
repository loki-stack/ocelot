﻿using CommonLib.Models.Client;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.Common.Models;
using PayrollServiceAPIs.PayRoll.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Repositories.Interfaces
{
    public interface IPayrollProfileRepository
    {
        Task<List<PayrollProfileViewModel>> GetAll(FilterRequest filter, int clientId, int entityId);
        Task<int?> Store(PayrollProfileStoreModel request);
        Task<int?> Update(int payrollProfileId, PayrollProfileStoreModel request);
        Task<PayrollProfileViewModel> GetById(int payrollProfileId);
        Task<List<PayrollProfileItem>> GetPayrollProfileItem(int payrollProfileId);
        Task<bool> StorePayrollProfileItem(int payrollProfileId, int payrollItemId, string modifiedBy);
        Task<bool> UpdatePayrollProfileItem(PayrollProfileItem request, string modifiedBy);
        Task<bool> DeletePayrollProfileItem(int payrollProfileId, int payrollItemId, string modifiedBy);
        Task<List<PayrollProfileViewItemModel>> GetPayrollItem(int payrollProfileId, int clientId, int entityId);
        Task<List<TbPayMasterPayrollItemFlag>> GetPayrollItemFlagsByPayrollItemId(int payrollItemId);
        Task<int> CountPayrollItemTaxByPayrollItemId(int payrollItemId);
        Task<PayrollProfileItem> GetPayrollProfileItemConfiguration(int payrollProfileId, int payrollItemId);
        Task<bool> CheckExistMasterPayrollItem(int payrollItemId);
        Task Duplicate(int payrollProfileId, string strCreatedBy, string payrollProfileName);
        Task<int?> GetFormulaIdByPayrollItemEmployee(int masterPayrollItemId, int employeeId);
    }
}
