﻿using CommonLib.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos.EnrolledScheme;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Services.Interfaces
{
    public interface IEnrolledSchemeService
    {
        Task<ApiResponse<Dictionary<string, object>>> GetByRegisteredSchemeId(int registeredSchemeId, ApiRequest<EnrolledSchemeGetAllRequest> request);
        Task<ApiResponse<Dictionary<string, object>>> GetById(int enrolledSchemeId, ApiRequest request);
        Task<ApiResponse<Dictionary<string, object>>> Create(ApiRequest<EnrolledSchemeRequest> request, string createdBy);
        Task<ApiResponse<Dictionary<string, object>>> Update(int enrolledSchemeId, ApiRequest<EnrolledSchemeRequest> request, string modifiedBy);
        Task<ApiResponse<int?>> Delete(int enrolledSchemeId, ApiRequest request);
        Task<ApiResponse<bool?>> DeleteMany(ApiRequest<List<EnrolledSchemeEntityRequest>> request);
    }
}
