﻿using CommonLib.Dtos;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace PayrollServiceAPIs.PayRoll.Services.Interfaces
{
    public interface IMasterPayRollItemService
    {
        Task<ApiResponse<int>> Store(ApiRequest<MasterPayrollItemStoreModel> request);
        Task<ApiResponse<int>> Update(int masterPayrollItemId, ApiRequest<MasterPayrollItemStoreModel> request, string token);
        Task<ApiResponse<MasterPayrollItemViewAll>> GetAll(ApiRequest<FilterRequest> request, string token);
        Task<ApiResponse<MasterPayrollItemAddModel>> GetPayrollItem(ApiRequest request, string token);
        Task<ApiResponse<MasterPayrollItemEditModel>> GetMasterPayrollItemById(int masterPayrollItemId, ApiRequest request, string token);

        Task<ApiResponse<bool>> Duplicate(int masterPayrollItem, string strCreatedBy, ApiRequest<MasterPayrollItemDuplicateModel> request);

        Task<ApiResponse<List<MasterPayrollItemModel>>> GetMasterPayrollItemByFlag(ApiRequest<List<int>> request);


    }
}
