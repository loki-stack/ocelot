﻿using CommonLib.Dtos;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos.PayrollCycle;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Services.Interfaces
{
    public interface IPayrollCycleService
    {
        Task<ApiResponse<List<PayrollCycleViewModel>>> GetAll(ApiRequest<FilterRequest> request, string token);
        Task<ApiResponse<bool>> Store(ApiRequest<int> request, string createBy);
        Task<ApiResponse<PayrollCycleViewModel>> Edit(int payrollCycleId, ApiRequest request);
        Task<ApiResponse<int>> Update(int payrollCycleId, ApiRequest<PayrollCycleStoreModel> request);
        Task<ApiResponse<bool>> Delete(ApiRequest<List<int>> request, string modifiedBy);
    }
}
