﻿using CommonLib.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Services.Interfaces
{
    public interface IRegisteredSchemeService
    {
        Task<ApiResponse<Dictionary<string, object>>> GetAll(ApiRequest request);
        Task<ApiResponse<Dictionary<string, object>>> GetById(int mpfRegisteredSchemeId, ApiRequest request);
        Task<ApiResponse<Dictionary<string, object>>> Update(int mpfRegisteredSchemeId, ApiRequest<MPFRegisteredSchemeRequest> request, string modifiedBy);
        Task<ApiResponse<bool?>> Delete(int registeredSchemeId, ApiRequest request);
    }
}
