﻿using CommonLib.Dtos;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos.ExchangeRate;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Services.Interfaces
{
    public interface IExchangeRateService
    {
        Task<ApiResponse<List<ExchangeRateViewModel>>> GetAll(ApiRequest<FilterRequest> request, string token);
        Task<ApiResponse<ExchangeRateCreateModel>> Create(ApiRequest request, string token);
        Task<ApiResponse<int>> Store(ApiRequest<ExchangeRateStoreModel> request, string token);
        Task<ApiResponse<ExchangeRateEditModel>> Edit(int exchangeRateId, ApiRequest request, string token);
        Task<ApiResponse<int>> Update(int exchangeRateId, ApiRequest<ExchangeRateStoreModel> request, string token);
    }
}
