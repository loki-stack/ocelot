﻿using CommonLib.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Services.Interfaces
{
    public interface IEmployeeService
    {
        Task<EmployeeProfile> GetEmployeeProfile(int employeeId, string token, StdRequestParam param);
    }
}
