﻿using CommonLib.Dtos;
using Microsoft.AspNetCore.Http;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos.PayrollRun;
using PayrollServiceAPIs.PayRoll.Dtos.PayrollRun.Export;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Services.Interfaces
{
    public interface IPayrollRunService
    {
        Task<ApiResponse<List<PayrollRunViewModel>>> GetAll(ApiRequest<FilterRequest> request, string token);
        Task<ApiResponse<PayrollRunCreateModel>> Create(ApiRequest request, string token);
        Task<ApiResponse<int>> Store(ApiRequest<PayrollRunStoreModel> request, string token);
        Task<ApiResponse<PayrollRunEditModel>> Edit(int payrollRunId, ApiRequest request, string token);
        Task<ApiResponse<PayrollRunSettingModel>> Setting(int payrollRunId, ApiRequest request, string token);
        Task<ApiResponse<int>> Update(int payrollRunId, ApiRequest<PayrollRunStoreModel> request, string token);
        Task<ApiResponse<bool>> Duplicate(int payrollRunId, ApiRequest<PayrollRunStoreModel> request);
        Task<ApiResponse<bool>> EmployeeStore(int payrollRunId, ApiRequest<PayrollRunEmployeeStoreModel> request);
        Task<ApiResponse<PayrollRunTranssationCreateModel>> TransactionCreate(ApiRequest<List<int>> request, string token);
        Task<ApiResponse<bool>> TransactionStore(int payrollRunId, ApiRequest<PayrollRunTransactionStoreModel> request);
        Task<ApiResponse<bool>> TransactionDelete(ApiRequest<PayrollRunTransactionDeleteModel> request);
        Task<ApiResponse<List<PayrollRunEmployeeViewModel>>> GetEmployeeSelectInPayroll(ApiRequest request, string token);
        Task<ApiResponse<List<PayrollRunTransactionViewModel>>> GetTransactionByEmployee(int payrollRunId, ApiRequest<List<int>> request);
        Task<ApiResponse<string>> Export(int payrollRunId, string exportBy, ApiRequest<PayrollRunExportModel> request);
        Task<ApiResponse<PayrollRunImportResult>> Import(int payrollRunId, string userName, string token, ApiRequest<int> request, IFormFile file);
        Task<PayrollRunImportResult> ValidatePayrollRunImport(int payrollRunId, string userName, string token, StdRequestParam parameter, List<ExportTransactionModel> payrollRunTransactions, ExportPayrollModel payrollRun, List<int> rowChecks);
        Task<ApiResponse<PayrollRunImportResult>> ImportConfirm(int payrollRunId, int payrollRumImportId, string userName, string token, ApiRequest<PayrollRunImportConfirmModel> request);
        DataTable CreateDataTable(List<ExportTransactionModel> data, List<int> rowProcess);
    }
}
