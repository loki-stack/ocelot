﻿using CommonLib.Dtos;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos.ORSOScheme;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Services.Interfaces
{
    public interface IORSOSchemeService
    {
        Task<ApiResponse<Dictionary<string, object>>> GetAll(ApiRequest<PaginationRequest> request);
        Task<ApiResponse<Dictionary<string, object>>> GetById(int orsoSchemeId, ApiRequest request);
        Task<ApiResponse<Dictionary<string, object>>> Create(ApiRequest<ORSOSchemeRequest> request, string createdBy);
        Task<ApiResponse<Dictionary<string, object>>> Update(int orsoSchemeId, ApiRequest<ORSOSchemeRequest> request, string modifiedBy);
        Task<ApiResponse<bool?>> Delete(int orsoSchemeId, ApiRequest<ORSOSchemeRequest> request);
    }
}
