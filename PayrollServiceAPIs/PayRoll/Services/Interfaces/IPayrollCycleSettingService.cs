﻿using CommonLib.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos.PayrollCycleSetting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Services.Interfaces
{
    public interface IPayrollCycleSettingService
    {
        Task<ApiResponse<PayrollCycleSettingEditModel>> Edit(ApiRequest request);
        Task<ApiResponse<bool>> Update(ApiRequest<PayrollCycleSettingUpdateModel> request);
    }
}
