﻿using CommonLib.Dtos;
using PayrollServiceAPIs.Common.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.Common.Services.Interfaces
{
    public interface IGlCodeService
    {
        Task<ApiResponse<List<GlCodeModel>>> GetAll(ApiRequest<FilterRequest> request, string token);
        Task<ApiResponse<GlCodeCreateModel>> Create(ApiRequest request, string token);
        Task<ApiResponse<int>> Store(ApiRequest<GlCodeStoreModel> request, string token);
        Task<ApiResponse<GlCodeEditModel>> Edit(int glCodeId, ApiRequest request, string token);
        Task<ApiResponse<int>> Update(int glCodeId, ApiRequest<GlCodeUpdateModel> request, string token);

    }
}
