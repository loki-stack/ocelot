﻿using CommonLib.Dtos;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Services.Interfaces
{
    public interface IPayrollProfileService
    {
        Task<ApiResponse<List<PayrollProfileViewModel>>> GetAll(ApiRequest<FilterRequest> request, string token);
        Task<ApiResponse<int>> Store(ApiRequest<PayrollProfileStoreModel> request);
        Task<ApiResponse<int>> Update(int payrollProfileId, ApiRequest<PayrollProfileStoreModel> request);
        Task<ApiResponse<PayrollProfileAddModel>> GetPayrollItem(ApiRequest request, string token);
        Task<ApiResponse<PayrollProfileEditModel>> GetPayrollProfileById(int payrollProfileId, ApiRequest request, string token);
        Task<ApiResponse<MasterPayrollItemConfiguration>> GetPayrollProfileItemConfiguration(int payrollProfileId, int payrollItemId, ApiRequest request, string token);
        Task<ApiResponse<bool>> UpdatePayrollProfileItemConfiguration(int payrollProfileId, int payrollItemId, string strModifiedBy, ApiRequest<PayrollProfileItem> request);
        Task<ApiResponse<bool>> Duplicate(int payrollProfileId, string strCreatedBy, ApiRequest<PayrollProfileDuplicateModel> request);
    }
}
