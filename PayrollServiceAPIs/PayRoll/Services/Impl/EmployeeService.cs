﻿using CommonLib.Dtos;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using PayrollServiceAPIs.PayRoll.Dtos.Employee;
using PayrollServiceAPIs.PayRoll.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Services.Impl
{
    public class EmployeeService : IEmployeeService
    {
        private readonly AppSettings _appSettings;

        public EmployeeService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public async Task<EmployeeProfile> GetEmployeeProfile(int employeeId, string token, StdRequestParam requestParam)
        {
            EmployeeProfile result = new EmployeeProfile();
            try
            {
                var myUri = new Uri(string.Format(_appSettings.EmployeeServiceProfile, employeeId));
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                string locale = requestParam == null ? "" : requestParam.Locale;
                int clientId = requestParam == null ? 0 : requestParam.ClientId;
                int entityId = requestParam == null ? 0 : requestParam.EntityId;

                string param = string.Empty;
                param = param + "Parameter.Locale=" + locale + "&";
                param = param + "Parameter.ClientId=" + clientId + "&";
                param = param + "Parameter.EntityId=" + entityId + "&";

                
                HttpResponseMessage response = await client.GetAsync(myUri + "?" + param);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                ApiResponse<Dictionary<string, object>> responseObj = JsonConvert.DeserializeObject<ApiResponse<Dictionary<string, object>>>(responseBody);
                result = JsonConvert.DeserializeObject<EmployeeProfile>(responseObj.Data["employeeProfile"].ToString());
            }
            catch (Exception ex)
            {
            }
            return result;
        }
    }
}
