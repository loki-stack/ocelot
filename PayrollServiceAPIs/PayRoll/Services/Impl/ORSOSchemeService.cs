﻿using AutoMapper;
using CommonLib.Dtos;
using CommonLib.Models.Client;
using Newtonsoft.Json;
using PayrollServiceAPIs.Common.Constants;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.Common.Models;
using PayrollServiceAPIs.PayRoll.Dtos.ORSOScheme;
using PayrollServiceAPIs.PayRoll.Repositories.Interfaces;
using PayrollServiceAPIs.PayRoll.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Services.Impl
{
    public class ORSOSchemeService : IORSOSchemeService
    {
        private readonly IORSOSchemeRepository _orsoSchemeRepository;
        private readonly IPayrollItemRepository _payrollItemRepository;
        private readonly IMapper _mapper;

        public ORSOSchemeService(IORSOSchemeRepository orsoSchemeRepository, IPayrollItemRepository payrollItemRepository, IMapper mapper)
        {
            _orsoSchemeRepository = orsoSchemeRepository;
            _payrollItemRepository = payrollItemRepository;
            _mapper = mapper;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> Create(ApiRequest<ORSOSchemeRequest> request, string createdBy)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();

            //check orso scheme code exist
            bool isSchemeCodeExist = _orsoSchemeRepository.CheckSchemeCodeExist(request.Data.OrsoSchemeCode);
            if (isSchemeCodeExist)
            {
                response.Message.ValidateResult.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.ORSO004,
                    ComponentId = "orsoSchemeCode"
                });
                response.Data = null;
                return response;
            }

            TbPayOrsoScheme orsoScheme = await _orsoSchemeRepository.Create(request.Data, createdBy);
            ORSOSchemeViewList viewList = _mapper.Map<TbPayOrsoScheme, ORSOSchemeViewList>(orsoScheme);
            List<TbPayPayrollItem> payrollItems = await _payrollItemRepository.Create(orsoScheme.OrsoSchemeId, orsoScheme.OrsoSchemeType, request.Data.OrsoPayrollItemIds);
            response.Data.Add("orsoScheme", viewList);
            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.ORSO002
            });
            return response;
        }

        public Task<ApiResponse<bool?>> Delete(int orsoSchemeId, ApiRequest<ORSOSchemeRequest> request)
        {
            throw new NotImplementedException();
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetAll(ApiRequest<PaginationRequest> request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();
            PaginationRequest paginationRequest = request.Data;
            List<TbPayOrsoScheme> orsoSchemes = await _orsoSchemeRepository.GetAll();

            ORSOSchemeFilter filter = new ORSOSchemeFilter();

            //get filter from request
            if (!string.IsNullOrEmpty(paginationRequest.Filter))
            {
                filter = JsonConvert.DeserializeObject<ORSOSchemeFilter>(paginationRequest.Filter);
            }

            // sort by item from request
            if (!string.IsNullOrEmpty(paginationRequest.Sort))
            {
                char sortCharacter = paginationRequest.Sort.ElementAt(0);
                string sortField = paginationRequest.Sort.Substring(1, paginationRequest.Sort.Length - 1);
                if (sortField.ToLower().Contains("orsoschemename"))
                {
                    if (sortCharacter == '-')
                        orsoSchemes = orsoSchemes.OrderByDescending(x => x.OrsoSchemeName).ToList();
                    else orsoSchemes = orsoSchemes.OrderBy(x => x.OrsoSchemeName).ToList();
                }
                if (sortField.ToLower().Contains("orsoschemetype"))
                {
                    if (sortCharacter == '-')
                        orsoSchemes = orsoSchemes.OrderByDescending(x => x.OrsoSchemeType).ToList();
                    else orsoSchemes = orsoSchemes.OrderBy(x => x.OrsoSchemeType).ToList();
                }
                if (sortField.ToLower().Contains("orsoschemecode"))
                {
                    if (sortCharacter == '-')
                        orsoSchemes = orsoSchemes.OrderByDescending(x => x.OrsoSchemeCode).ToList();
                    else orsoSchemes = orsoSchemes.OrderBy(x => x.OrsoSchemeCode).ToList();
                }
                if (sortField.ToLower().Contains("statuscd"))
                {
                    if (sortCharacter == '-')
                        orsoSchemes = orsoSchemes.OrderByDescending(x => x.StatusCd).ToList();
                    else orsoSchemes = orsoSchemes.OrderBy(x => x.StatusCd).ToList();
                }
            }

            // search by filter
            if (!string.IsNullOrEmpty(filter.OrsoSchemeType))
            {
                orsoSchemes = orsoSchemes.Where(x => x.OrsoSchemeType == filter.OrsoSchemeType).ToList();
            }
            if (!string.IsNullOrEmpty(filter.StatusCd))
            {
                orsoSchemes = orsoSchemes.Where(x => x.StatusCd == filter.StatusCd).ToList();
            }

            // search by text
            if (!string.IsNullOrEmpty(paginationRequest.FullTextSearch))
            {
                orsoSchemes = orsoSchemes.Where(x => x.OrsoSchemeName.ToLower().Contains(paginationRequest.FullTextSearch) ||
                                                    x.OrsoSchemeCode.ToLower().Contains(paginationRequest.FullTextSearch)).ToList();
            }

            // return data
            Pagination<TbPayOrsoScheme> pagination = new Pagination<TbPayOrsoScheme>();
            if (request.Data.Size.HasValue)
            {
                pagination.Page = paginationRequest.Page.Value;
                pagination.Size = paginationRequest.Size.Value;
                pagination.TotalPages = (orsoSchemes.Count + paginationRequest.Size.Value - 1) / paginationRequest.Size.Value;
                pagination.TotalElements = orsoSchemes.Count;

                orsoSchemes = orsoSchemes.Skip((paginationRequest.Page.Value - 1) * paginationRequest.Size.Value).Take(paginationRequest.Size.Value).ToList();
                pagination.NumberOfElements = orsoSchemes.Count;
                pagination.Content = orsoSchemes;
            }
            else
            {
                pagination.TotalElements = orsoSchemes.Count;
                pagination.Content = orsoSchemes;
            }
           
            response.Data.Add("pagination", pagination);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetById(int orsoSchemeId, ApiRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();
            TbPayOrsoScheme orsoScheme = await _orsoSchemeRepository.GetById(orsoSchemeId);
            if (orsoScheme == null)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.ORSO001
                });
                response.Data = null;
                return response;
            }
            ORSOSchemeViewDetail viewDetail = _mapper.Map<TbPayOrsoScheme, ORSOSchemeViewDetail>(orsoScheme);
            viewDetail.VcPayrollItem = await _payrollItemRepository.GetPayRollItemByScheme(orsoScheme.OrsoSchemeId, orsoScheme.OrsoSchemeType);
            response.Data.Add("orsoScheme", viewDetail);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> Update(int orsoSchemeId, ApiRequest<ORSOSchemeRequest> request, string modifiedBy)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();
            //check exist
            TbPayOrsoScheme orsoScheme = await _orsoSchemeRepository.GetById(orsoSchemeId);
            if (orsoScheme == null)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.ORSO001
                });
                response.Data = null;
                return response;
            }
            //update
            orsoScheme = await _orsoSchemeRepository.Update(orsoSchemeId, request.Data, modifiedBy);
            List<TbPayPayrollItem> payrollItems = await _payrollItemRepository.Create(orsoScheme.OrsoSchemeId, orsoScheme.OrsoSchemeType, request.Data.OrsoPayrollItemIds);
            response.Data.Add("orsoScheme", orsoScheme);
            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.ORSO003
            });
            return response;
        }
    }
}
