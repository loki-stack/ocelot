﻿using CommonLib.Dtos;
using CommonLib.Excel;
using CommonLib.Models.Client;
using CommonLib.Repositories.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using PayrollServiceAPIs.Common.Constants;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.Common.Models;
using PayrollServiceAPIs.PayRoll.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos.Employee;
using PayrollServiceAPIs.PayRoll.Dtos.PayrollRun;
using PayrollServiceAPIs.PayRoll.Dtos.PayrollRun.Export;
using PayrollServiceAPIs.PayRoll.Repositories.Interfaces;
using PayrollServiceAPIs.PayRoll.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace PayrollServiceAPIs.PayRoll.Services.Impl
{
    public class PayrollRunService : IPayrollRunService
    {
        private readonly IPayrollRunRepository _payrollRunRepository;
        private readonly IMasterPayrollItemRepository _payrollItemRepository;
        private readonly IPayrollProfileRepository _payrollProfileRepository;
        private readonly IGenericCodeRepository _genericCodeRepository;
        private readonly IFormulasRepository _formulasRepository;
        private readonly IEmployeeService _employeeService;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public PayrollRunService(IPayrollRunRepository payrollRunRepository, IMasterPayrollItemRepository payrollItemRepository, IPayrollProfileRepository payrollProfileRepository,
            IFormulasRepository formulasRepository, IGenericCodeRepository genericCodeRepository, IEmployeeService employeeService, IWebHostEnvironment webHostEnvironment)
        {
            _payrollRunRepository = payrollRunRepository;
            _payrollItemRepository = payrollItemRepository;
            _payrollProfileRepository = payrollProfileRepository;
            _formulasRepository = formulasRepository;
            _genericCodeRepository = genericCodeRepository;
            _employeeService = employeeService;
            _webHostEnvironment = webHostEnvironment;
        }

        /// <summary>
        /// Get All Payroll Run
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ApiResponse<List<PayrollRunViewModel>>> GetAll(ApiRequest<FilterRequest> request, string token)
        {
            ApiResponse<List<PayrollRunViewModel>> response = new ApiResponse<List<PayrollRunViewModel>>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new List<PayrollRunViewModel>()
            };

            if (request.Data == null)
            {
                request.Data = new FilterRequest();
            }

            response.Data = await _payrollRunRepository.GetAll(request.Data);
            List<string> genericCodeList = new List<string>();
            genericCodeList.Add(Common.Constants.GenericCode.PAYROLL_RUN_STATUS);
            var genericCodes = await _genericCodeRepository.GetGenericCodes(genericCodeList, token, request.Parameter);
            List<GenericCodeResult> statuses = new List<GenericCodeResult>();
            if (genericCodes.ContainsKey(Common.Constants.GenericCode.PAYROLL_RUN_STATUS.ToLower()))
            {
                statuses = genericCodes[Common.Constants.GenericCode.PAYROLL_RUN_STATUS.ToLower()];
            }

            foreach (var item in response.Data)
            {
                if (statuses.Any(n => n.CodeValue == item.StatusCd))
                {
                    item.StatusText = statuses.First(n => n.CodeValue == item.StatusCd).TextValue;
                }
            }

            return response;
        }

        /// <summary>
        /// Get data screen create payroll run
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ApiResponse<PayrollRunCreateModel>> Create(ApiRequest request, string token)
        {
            ApiResponse<PayrollRunCreateModel> response = new ApiResponse<PayrollRunCreateModel>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new PayrollRunCreateModel()
            };

            var genericCodes = await _genericCodeRepository.GetGenericCodes(new List<string>() { Common.Constants.GenericCode.PAYROLL_RUN_STATUS }, token, request.Parameter);
            if (genericCodes.ContainsKey(Common.Constants.GenericCode.PAYROLL_RUN_STATUS.ToLower()))
            {
                response.Data.Status = genericCodes[Common.Constants.GenericCode.PAYROLL_RUN_STATUS.ToLower()];
            }

            response.Data.Cycles = await _payrollRunRepository.GetPayrollCycle();

            return response;
        }

        /// <summary>
        /// Get data payroll run for screen edit
        /// </summary>
        /// <param name="payrollRunId"></param>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ApiResponse<PayrollRunEditModel>> Edit(int payrollRunId, ApiRequest request, string token)
        {
            ApiResponse<PayrollRunEditModel> response = new ApiResponse<PayrollRunEditModel>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new PayrollRunEditModel()
            };

            var genericCodes = await _genericCodeRepository.GetGenericCodes(new List<string>() { Common.Constants.GenericCode.PAYROLL_RUN_STATUS }, token, request.Parameter);
            if (genericCodes.ContainsKey(Common.Constants.GenericCode.PAYROLL_RUN_STATUS.ToLower()))
            {
                response.Data.Status = genericCodes[Common.Constants.GenericCode.PAYROLL_RUN_STATUS.ToLower()];
            }

            response.Data.Detail = await _payrollRunRepository.GetById(payrollRunId);
            response.Data.Cycles = await _payrollRunRepository.GetPayrollCycle();

            return response;
        }

        /// <summary>
        /// Add Payroll Run
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ApiResponse<int>> Store(ApiRequest<PayrollRunStoreModel> request, string token)
        {
            ApiResponse<int> response = new ApiResponse<int>
            {
                Ray = request.Ray ?? string.Empty,
                Data = 0
            };

            // check status 
            List<GenericCodeResult> payrollRunStatus = new List<GenericCodeResult>();
            var genericCodes = await _genericCodeRepository.GetGenericCodes(new List<string>() { Common.Constants.GenericCode.PAYROLL_RUN_STATUS }, token, request.Parameter);
            if (genericCodes.ContainsKey(Common.Constants.GenericCode.PAYROLL_RUN_STATUS.ToLower()))
            {
                payrollRunStatus = genericCodes[Common.Constants.GenericCode.PAYROLL_RUN_STATUS.ToLower()];
            }

            if (!payrollRunStatus.Any(n => n.CodeValue == request.Data.StatusCd))
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0407
                });

                return response;
            }

            // check payroll cycle id
            if (!await _payrollRunRepository.CheckPayrollCycle(request.Data.PayrollCycleId))
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0410
                });

                return response;
            }

            int? newId = await _payrollRunRepository.Store(request.Data);
            if (!newId.HasValue)
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0401
                });
            }
            else
            {
                response.Data = newId.Value;
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.SUCCESS,
                    LabelCode = Label.PAY0402
                });
            }

            return response;
        }

        /// <summary>
        /// Update Payroll Run
        /// </summary>
        /// <param name="payrollRunId"></param>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ApiResponse<int>> Update(int payrollRunId, ApiRequest<PayrollRunStoreModel> request, string token)
        {
            ApiResponse<int> response = new ApiResponse<int>
            {
                Ray = request.Ray ?? string.Empty,
                Data = 0
            };

            var objPayrollRun = await _payrollRunRepository.GetById(payrollRunId);
            if (objPayrollRun == null)
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0403
                });
                return response;
            }

            // check status 
            List<GenericCodeResult> payrollRunStatus = new List<GenericCodeResult>();
            var genericCodes = await _genericCodeRepository.GetGenericCodes(new List<string>() { Common.Constants.GenericCode.PAYROLL_RUN_STATUS }, token, request.Parameter);
            if (genericCodes.ContainsKey(Common.Constants.GenericCode.PAYROLL_RUN_STATUS.ToLower()))
            {
                payrollRunStatus = genericCodes[Common.Constants.GenericCode.PAYROLL_RUN_STATUS.ToLower()];
            }

            if (!payrollRunStatus.Any(n => n.CodeValue == request.Data.StatusCd))
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0408
                });
                return response;
            }

            // check payroll cycle id
            if (!await _payrollRunRepository.CheckPayrollCycle(request.Data.PayrollCycleId))
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0410
                });

                return response;
            }

            // update payroll run
            if (await _payrollRunRepository.Update(payrollRunId, request.Data) == null)
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0404
                });
            }
            else
            {
                response.Data = payrollRunId;
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.SUCCESS,
                    LabelCode = Label.PAY0405
                });
            }

            return response;
        }

        /// <summary>
        /// duplicate payroll profile
        /// </summary>
        /// <param name="payrollRunId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<bool>> Duplicate(int payrollRunId, ApiRequest<PayrollRunStoreModel> request)
        {
            ApiResponse<bool> response = new ApiResponse<bool>
            {
                Ray = request.Ray ?? string.Empty,
                Data = false
            };

            var objPayrollProfile = await _payrollRunRepository.GetById(payrollRunId);
            if (objPayrollProfile == null)
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0403
                });
                return response;
            }

            await _payrollRunRepository.Duplicate(payrollRunId, request.Data);

            response.Data = true;
            response.Message.Toast.Add(new MessageItem()
            {
                Level = CommonLib.Constants.Level.SUCCESS,
                LabelCode = Label.PAY0409
            });
            return response;
        }

        /// <summary>
        /// Get data payroll run for screen setting
        /// </summary>
        /// <param name="payrollRunId"></param>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ApiResponse<PayrollRunSettingModel>> Setting(int payrollRunId, ApiRequest request, string token)
        {
            ApiResponse<PayrollRunSettingModel> response = new ApiResponse<PayrollRunSettingModel>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new PayrollRunSettingModel()
            };

            response.Data.Detail = await _payrollRunRepository.GetById(payrollRunId);
            response.Data.Employee = await _payrollRunRepository.GetPayrollRunEmployee(payrollRunId, request.Parameter.Locale);
            response.Data.Transaction = await _payrollRunRepository.GetPayrollRunTransaction(payrollRunId, response.Data.Employee.Select(n => n.EmployeeId).ToList(), request.Parameter.Locale);

            List<GenericCodeResult> payrollRunStatus = new List<GenericCodeResult>();
            var genericCodes = await _genericCodeRepository.GetGenericCodes(new List<string>() { Common.Constants.GenericCode.PAYROLL_RUN_STATUS }, token, request.Parameter);
            if (genericCodes.ContainsKey(Common.Constants.GenericCode.PAYROLL_RUN_STATUS.ToLower()))
            {
                payrollRunStatus = genericCodes[Common.Constants.GenericCode.PAYROLL_RUN_STATUS.ToLower()];
            }

            foreach (var item in response.Data.Employee)
            {
                if (payrollRunStatus.Any(n => n.CodeValue == item.StatusCd))
                {
                    item.StatusText = payrollRunStatus.First(n => n.CodeValue == item.StatusCd).TextValue;
                }
            }

            List<FormulasResult> formulas = await _formulasRepository.GetFormulas(request.Parameter.ClientId, request.Parameter.EntityId);
            if(formulas != null && formulas.Any())
            {
                foreach (var item in response.Data.Transaction)
                {
                    if (formulas.Any(n => n.FormulasId == item.FormulaId))
                    {
                        item.Formula = formulas.First(n => n.FormulasId == item.FormulaId).Name;
                    }
                }
            }

            return response;
        }

        /// <summary>
        /// Get transaction by employee id
        /// </summary>
        /// <param name="payrollRunId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<List<PayrollRunTransactionViewModel>>> GetTransactionByEmployee(int payrollRunId, ApiRequest<List<int>> request)
        {
            ApiResponse<List<PayrollRunTransactionViewModel>> response = new ApiResponse<List<PayrollRunTransactionViewModel>>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new List<PayrollRunTransactionViewModel>()
            };

            response.Data = await _payrollRunRepository.GetPayrollRunTransaction(payrollRunId, request.Data, request.Parameter.Locale);

            return response;
        }

        /// <summary>
        /// add employee to payroll run
        /// </summary>
        /// <param name="payrollRunId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<bool>> EmployeeStore(int payrollRunId, ApiRequest<PayrollRunEmployeeStoreModel> request)
        {
            ApiResponse<bool> response = new ApiResponse<bool>
            {
                Ray = request.Ray ?? string.Empty,
                Data = true
            };

            List<int> employeeId = await _payrollRunRepository.GetPayrollRunEmployeeId(payrollRunId);

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    // remove employee from payroll run
                    List<int> employeeIdRemove = employeeId.Where(n => !request.Data.EmployeeIds.Contains(n)).ToList();
                    if (employeeIdRemove.Any())
                    {
                        await _payrollRunRepository.DeletePayrollRunEmployee(payrollRunId, request.Data.CreatedBy, employeeIdRemove);
                    }

                    // add / update employee to payroll run
                    await _payrollRunRepository.StorePayrollRunEmployee(payrollRunId, request.Data);

                    response.Message.Toast.Add(new MessageItem()
                    {
                        Level = CommonLib.Constants.Level.SUCCESS,
                        LabelCode = Label.PAY0414
                    });

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }

            return response;
        }

        /// <summary>
        /// Get data page select transaction payroll run
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ApiResponse<PayrollRunTranssationCreateModel>> TransactionCreate(ApiRequest<List<int>> request, string token)
        {
            ApiResponse<PayrollRunTranssationCreateModel> response = new ApiResponse<PayrollRunTranssationCreateModel>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new PayrollRunTranssationCreateModel()
            };

            response.Data.MasterPayrollItems = await _payrollRunRepository.GetMasterPayrollItems(request.Data);

            List<GenericCodeResult> payrollRunStatus = new List<GenericCodeResult>();
            var genericCodes = await _genericCodeRepository.GetGenericCodes(new List<string>() { Common.Constants.GenericCode.CURRENCY }, token, request.Parameter);
            if (genericCodes.ContainsKey(Common.Constants.GenericCode.CURRENCY.ToLower()))
            {
                response.Data.Currency = genericCodes[Common.Constants.GenericCode.CURRENCY.ToLower()];
            }

            return response;
        }

        /// <summary>
        /// add transaction to payroll run
        /// </summary>
        /// <param name="payrollRunId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<bool>> TransactionStore(int payrollRunId, ApiRequest<PayrollRunTransactionStoreModel> request)
        {
            ApiResponse<bool> response = new ApiResponse<bool>
            {
                Ray = request.Ray ?? string.Empty,
                Data = true
            };

            // check add transaction
            List<EmployeeNameErrorModel> employeeNameError = await _payrollRunRepository.GetEmployeeNotLinkTransaction(request.Data, request.Parameter.Locale);
            if (employeeNameError.Any())
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0411,
                    Placeholder = (new List<string>() { string.Join(",", employeeNameError.Select(n => n.Name)) }).ToArray()
                });

                return response;
            }

            await _payrollRunRepository.StorePayrollRunTransaction(payrollRunId, request.Data);

            response.Message.Toast.Add(new MessageItem()
            {
                Level = CommonLib.Constants.Level.SUCCESS,
                LabelCode = Label.PAY0412
            });

            return response;
        }

        /// <summary>
        /// remove transaction from payroll run
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<bool>> TransactionDelete(ApiRequest<PayrollRunTransactionDeleteModel> request)
        {
            ApiResponse<bool> response = new ApiResponse<bool>
            {
                Ray = request.Ray ?? string.Empty,
                Data = true
            };

            await _payrollRunRepository.DeletePayrollRunTransaction(request.Data.ModifiedBy, request.Data.PayrollRunTransactionId);

            response.Message.Toast.Add(new MessageItem()
            {
                Level = CommonLib.Constants.Level.SUCCESS,
                LabelCode = Label.PAY0413
            });

            return response;
        }

        /// <summary>
        /// Get employee select add to payroll run
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ApiResponse<List<PayrollRunEmployeeViewModel>>> GetEmployeeSelectInPayroll(ApiRequest request, string token)
        {
            ApiResponse<List<PayrollRunEmployeeViewModel>> response = new ApiResponse<List<PayrollRunEmployeeViewModel>>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new List<PayrollRunEmployeeViewModel>()
            };

            response.Data = await _payrollRunRepository.GetEmployeeSelectInPayroll(request.Parameter.Locale);

            List<GenericCodeResult> payrollRunStatus = new List<GenericCodeResult>();
            var genericCodes = await _genericCodeRepository.GetGenericCodes(new List<string>() { Common.Constants.GenericCode.PAYROLL_RUN_STATUS }, token, request.Parameter);
            if (genericCodes.ContainsKey(Common.Constants.GenericCode.PAYROLL_RUN_STATUS.ToLower()))
            {
                payrollRunStatus = genericCodes[Common.Constants.GenericCode.PAYROLL_RUN_STATUS.ToLower()];
            }

            foreach (var item in response.Data)
            {
                if (payrollRunStatus.Any(n => n.CodeValue == item.StatusCd))
                {
                    item.StatusText = payrollRunStatus.First(n => n.CodeValue == item.StatusCd).TextValue;
                }
            }

            return response;
        }
        
        /// <summary>
        /// Export payroll run transaction
        /// </summary>
        /// <param name="payrollRunId"></param>
        /// <param name="exportBy"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<string>> Export(int payrollRunId, string exportBy, ApiRequest<PayrollRunExportModel> request)
        {
            ApiResponse<string> response = new ApiResponse<string>()
            {
                Ray = request.Ray ?? string.Empty,
                Data = string.Empty
            };
            request.Data.BrowserTimeOffset = -request.Data.BrowserTimeOffset;
            List<ExportTransactionModel> transactions = await _payrollRunRepository.GetExportTransaction(payrollRunId, request.Parameter.Locale);
            List<ExportPayrollModel> payrolls = new List<ExportPayrollModel>()
            {
                new ExportPayrollModel() { ClientId = request.Data.ClientName, DefaultPaymentCurrency = string.Empty, ExportBy = exportBy, PayrollRunId = payrollRunId, ExportTime = DateTime.Now }
            };
            List<ExportMasterPayrollItemModel> payrollItems = await _payrollRunRepository.GetExportPayrollItem();
            List<ExportPayrollCycleModel> payrollCycles = new List<ExportPayrollCycleModel>() { await _payrollRunRepository.GetExportPayrollCycle(payrollRunId) };

            ExcelUtil excelExport = new ExcelUtil();

            if (!Directory.Exists(Path.Combine(_webHostEnvironment.ContentRootPath, PayrollRunPath.EXPORT))){
                Directory.CreateDirectory(Path.Combine(_webHostEnvironment.ContentRootPath, PayrollRunPath.EXPORT));
            }

            string fileName = Path.Combine(_webHostEnvironment.ContentRootPath, PayrollRunPath.EXPORT + "Export_" + DateTime.Now.ToString("yyyyMMddHHmmssffffff") + ".xlsx");
            FileStream fs = new FileStream(fileName, FileMode.CreateNew);
            excelExport.Export(new Configuration() { BrowserTimeOffset = new TimeSpan(request.Data.BrowserTimeOffset / 60, request.Data.BrowserTimeOffset % 60, 0) }, fs, transactions, payrolls, payrollItems, payrollCycles);

            response.Data = fileName;

            return response;
        }

        /// <summary>
        /// Import payroll run transaction
        /// </summary>
        /// <param name="payrollRunId"></param>
        /// <param name="request"></param>
        /// <param name="userName"></param>
        /// <param name="token"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        public async Task<ApiResponse<PayrollRunImportResult>> Import(int payrollRunId, string userName, string token, ApiRequest<int> request, IFormFile file)
        {
            ApiResponse<PayrollRunImportResult> response = new ApiResponse<PayrollRunImportResult>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new PayrollRunImportResult()
            };

            // check extension
            if(Path.GetExtension(file.FileName) != ".xlsx")
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0415
                });

                return response;
            }

            ExcelUtil excelParser = new ExcelUtil();
            List<Error> errors = new List<Error>();
            List<Type> sheetModels = new List<Type>
            {
                typeof(ExportTransactionModel),
                typeof(ExportPayrollModel)
            };

            Configuration configuration = new Configuration() { SheetTypes = sheetModels };
            configuration.BrowserTimeOffset = new TimeSpan(-request.Data / 60, -request.Data % 60, 0);

            using (var memoryStream = new MemoryStream())
            {
                await file.CopyToAsync(memoryStream);
                memoryStream.Position = 0;
                var results = excelParser.Parse(memoryStream, configuration, ref errors);

                List<ExportTransactionModel> payrollRunTransactions = new List<ExportTransactionModel>();
                ExportPayrollModel payrollRun = new ExportPayrollModel();
                if (results.ContainsKey(typeof(ExportTransactionModel).Name))
                {
                    payrollRunTransactions = results[typeof(ExportTransactionModel).Name].Cast<ExportTransactionModel>().ToList();
                }

                if (results.ContainsKey(typeof(ExportPayrollModel).Name))
                {
                    payrollRun = results[typeof(ExportPayrollModel).Name].Cast<ExportPayrollModel>().FirstOrDefault();
                }

                if (errors.Any())
                {
                    response.Data.Errors = errors.Select(n => new PayrollRunImportError() { Row = n.Row, LabelCode = n.Message }).ToList();
                    return response;
                }
                // add payroll run import
                int? payrollRunImportId = await _payrollRunRepository.StorePayrollRunImport(new CommonLib.Models.Client.TbPayPayrollRunImport()
                {
                    PayrollRunId = payrollRunId,
                    FileName = file.FileName,
                    Content = memoryStream.ToArray(),
                    CreatedBy = userName
                });

                // validate payroll run transaction
                response.Data = await ValidatePayrollRunImport(payrollRunId, userName, token, request.Parameter, payrollRunTransactions, payrollRun, null);
                response.Data.PayrollRunImportId = payrollRunImportId;
            }

            return response;
        }

        /// <summary>
        /// validate file import
        /// </summary>
        /// <param name="payrollRunId"></param>
        /// <param name="userName"></param>
        /// <param name="token"></param>
        /// <param name="parameter"></param>
        /// <param name="payrollRunTransactions"></param>
        /// <param name="payrollRun"></param>
        /// <param name="rowChecks"></param>
        /// <returns></returns>
        public async Task<PayrollRunImportResult> ValidatePayrollRunImport(int payrollRunId, string userName, string token, StdRequestParam parameter, List<ExportTransactionModel> payrollRunTransactions, ExportPayrollModel payrollRun, List<int> rowChecks)
        {
            PayrollRunImportResult result = new PayrollRunImportResult() {
                Errors = new List<PayrollRunImportError>(),
                Transactions = new List<PayrollRunTransactionViewModel>()
            };

            if (payrollRun.ClientId != parameter.ClientId.ToString())
            {
                result.Errors.Add(new PayrollRunImportError() { LabelCode = Label.PAY0416 });
            }

            if (payrollRun.PayrollRunId != payrollRunId)
            {
                result.Errors.Add(new PayrollRunImportError() { LabelCode = Label.PAY0417 });
            }

            if (rowChecks != null && !rowChecks.Any())
            {
                result.Errors.Add(new PayrollRunImportError() { LabelCode = Label.PAY0420 });
            }

            if (result.Errors.Any())
            {
                return result;
            }

            ExportPayrollCycleModel payrollCycle = await _payrollRunRepository.GetExportPayrollCycle(payrollRunId);
            if (payrollCycle == null)
            {
                payrollCycle = new ExportPayrollCycleModel();
            }
            var employee = await _payrollRunRepository.GetPayrollRunEmployee(payrollRunId, parameter.Locale);
            var transaction = await _payrollRunRepository.GetPayrollRunTransaction(payrollRunId, employee.Select(n => n.EmployeeId).ToList(), parameter.Locale);

            List<FormulasResult> formulas = await _formulasRepository.GetFormulas(parameter.ClientId, parameter.EntityId);

            int intRow = 1;
            foreach (var item in payrollRunTransactions)
            {
                if (rowChecks != null && !rowChecks.Contains(intRow))
                {
                    intRow++;
                    continue;
                }
                // insert transaction
                if(item.TransactionId == 0)
                {
                    MasterPayrollViewModel objPpayrollItem = await _payrollItemRepository.GetByCode(item.ItemCode);
                    if (objPpayrollItem == null)
                    {
                        result.Errors.Add(new PayrollRunImportError() { Row = intRow, LabelCode = MasterPayrollItemLabel.MPRI0101 });
                        intRow++;
                        continue;
                    }

                    List<EmployeeNameErrorModel> employeeNameError = await _payrollRunRepository.GetEmployeeNotLinkTransaction(new PayrollRunTransactionStoreModel() { 
                        EmployeeIds = new List<int>() { item.EmployeeId},
                        PayrollItemId = objPpayrollItem.PayrollItemId
                    }, parameter.Locale);

                    if (employeeNameError.Any())
                    {
                        result.Errors.Add(new PayrollRunImportError() { Row = intRow, LabelCode = Label.PAY0411, Placeholder = (new List<string>() { string.Join(",", employeeNameError.Select(n => n.Name)) }).ToArray() });
                        intRow++;
                        continue;
                    }

                    int? formulaId = await _payrollProfileRepository.GetFormulaIdByPayrollItemEmployee(objPpayrollItem.PayrollItemId, item.EmployeeId);

                    EmployeeProfile objEmployee = await _employeeService.GetEmployeeProfile(item.EmployeeId, token, parameter);

                    result.Transactions.Add(new PayrollRunTransactionViewModel()
                    {
                        Row = intRow,
                        PayrollRunTransactionId = 0,
                        EmployeeName = objEmployee == null ? string.Empty : objEmployee.Name,
                        EmployeeId = item.EmployeeId,
                        PayrollItemCode = item.ItemCode,
                        PaymentSign = objPpayrollItem.PaymentSign,
                        Currency = item.PaymentCurrency,
                        BaseAmount = item.BaseAmount,
                        Formula = formulas.Any(n => n.FormulasId == formulaId) ? formulas.First(n => n.FormulasId == formulaId).Name : string.Empty,
                        Unit = item.Units,
                        AddedBy = userName,
                        DisplayText = item.DisplayText,
                        CalculationCycleStart = payrollCycle.StartDate,
                        CalculationCycleEnd = payrollCycle.EndDate,
                        TaxCycle = item.TaxCycle
                    });
                }
                // update transation
                else
                {
                    PayrollRunTransactionViewModel transactionMatch = transaction.FirstOrDefault(n => n.PayrollRunTransactionId == item.TransactionId && n.EmployeeId == item.EmployeeId && n.PayrollItemCode == item.ItemCode);

                    if (transactionMatch == null)
                    {
                        result.Errors.Add(new PayrollRunImportError() { Row = intRow, LabelCode = Label.PAY0418 });
                    }
                    else
                    {
                        result.Transactions.Add(new PayrollRunTransactionViewModel()
                        {
                            Row = intRow,
                            PayrollRunTransactionId = item.TransactionId,
                            EmployeeName = transactionMatch.EmployeeName,
                            EmployeeId = transactionMatch.EmployeeId,
                            PayrollItemCode = transactionMatch.PayrollItemCode,
                            PaymentSign = transactionMatch.PaymentSign,
                            Currency = item.PaymentCurrency,
                            BaseAmount = item.BaseAmount,
                            Formula = formulas.Any(n => n.FormulasId == transactionMatch.FormulaId) ? formulas.First(n => n.FormulasId == transactionMatch.FormulaId).Name : string.Empty,
                            Unit = item.Units,
                            AddedBy = transactionMatch.AddedBy,
                            DisplayText = transactionMatch.DisplayText,
                            CalculationCycleStart = payrollCycle.StartDate,
                            CalculationCycleEnd = payrollCycle.EndDate,
                            TaxCycle = item.TaxCycle
                        });
                    }
                }

                intRow++;
            }

            return result;
        }

        /// <summary>
        /// Import payroll run transaction confirm
        /// </summary>
        /// <param name="payrollRunId"></param>
        /// <param name="payrollRumImportId"></param>
        /// <param name="userName"></param>
        /// <param name="token"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<PayrollRunImportResult>> ImportConfirm(int payrollRunId, int payrollRumImportId, string userName, string token, ApiRequest<PayrollRunImportConfirmModel> request)
        {
            ApiResponse<PayrollRunImportResult> response = new ApiResponse<PayrollRunImportResult>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new PayrollRunImportResult()
            };

            TbPayPayrollRunImport objImport = await _payrollRunRepository.GetPayrollRunImport(payrollRumImportId, payrollRunId);

            if(objImport == null)
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0419
                });

                return response;
            }

            ExcelUtil excelParser = new ExcelUtil();
            List<Error> errors = new List<Error>();
            List<Type> sheetModels = new List<Type>
            {
                typeof(ExportTransactionModel),
                typeof(ExportPayrollModel)
            };

            Configuration configuration = new Configuration() { SheetTypes = sheetModels };
            configuration.BrowserTimeOffset = new TimeSpan(-request.Data.BrowserTimeOffset / 60, -request.Data.BrowserTimeOffset % 60, 0);

            using (var memoryStream = new MemoryStream(objImport.Content))
            {
                memoryStream.Position = 0;
                var results = excelParser.Parse(memoryStream, configuration, ref errors);

                List<ExportTransactionModel> payrollRunTransactions = new List<ExportTransactionModel>();
                ExportPayrollModel payrollRun = new ExportPayrollModel();
                if (results.ContainsKey(typeof(ExportTransactionModel).Name))
                {
                    payrollRunTransactions = results[typeof(ExportTransactionModel).Name].Cast<ExportTransactionModel>().ToList();
                }

                if (results.ContainsKey(typeof(ExportPayrollModel).Name))
                {
                    payrollRun = results[typeof(ExportPayrollModel).Name].Cast<ExportPayrollModel>().FirstOrDefault();
                }

                if (errors.Any())
                {
                    response.Data.Errors = errors.Select(n => new PayrollRunImportError() { Row = n.Row, LabelCode = n.Message }).ToList();
                    return response;
                }

                // validate payroll run transaction
                response.Data = await ValidatePayrollRunImport(payrollRunId, userName, token, request.Parameter, payrollRunTransactions, payrollRun, request.Data.Rows);

                if (response.Data.Errors.Any())
                {
                    return response;
                }

                // update data
                await _payrollRunRepository.UpdateDataPayrollRunTransactionImport(payrollRunId, CreateDataTable(payrollRunTransactions, request.Data.Rows), userName);
            }

            return response;
        }

        /// <summary>
        /// Create datatable payroll run transaction
        /// </summary>
        /// <param name="data"></param>
        /// <param name="rowProcess"></param>
        /// <returns></returns>
        public DataTable CreateDataTable(List<ExportTransactionModel> data, List<int> rowProcess)
        {
            DataTable result = new DataTable();
            result.Columns.Add("TransactionId", typeof(int));
            result.Columns.Add("DeleteTransaction", typeof(string));
            result.Columns.Add("EmployeeId", typeof(int));
            result.Columns.Add("ItemCode", typeof(string));
            result.Columns.Add("DisplayText", typeof(string));
            result.Columns.Add("CalculationStartDate", typeof(DateTimeOffset));
            result.Columns.Add("CalculationEndDate", typeof(DateTimeOffset));
            result.Columns.Add("TaxCycle", typeof(DateTime));
            result.Columns.Add("EAO713Cycle", typeof(DateTime));
            result.Columns.Add("MPFCycle", typeof(DateTime));
            result.Columns.Add("BaseAmount", typeof(int));
            result.Columns.Add("Units", typeof(string));
            result.Columns.Add("Param1", typeof(string));
            result.Columns.Add("Param2", typeof(string));
            result.Columns.Add("Param3", typeof(string));
            result.Columns.Add("FinalTransactionAmount", typeof(int));
            result.Columns.Add("InputCurrency", typeof(string));
            result.Columns.Add("PaymentCurrency", typeof(string));
            result.Columns.Add("ExchangeRate", typeof(double));
            result.Columns.Add("CostCenterCode", typeof(string));
            
            if (data != null && data.Any())
            {
                int intRow = 1;
                foreach (var item in data)
                {
                    if (!rowProcess.Contains(intRow))
                    {
                        intRow++;
                        continue;
                    }
                    result.Rows.Add(item.TransactionId, item.DeleteTransaction, item.EmployeeId, item.ItemCode, item.DisplayText, item.CalculationStartDate, item.CalculationEndDate, item.TaxCycle,
                        item.EAO713Cycle, item.MPFCycle, item.BaseAmount, item.Units, item.Param1, item.Param2, item.Param3, item.FinalTransactionAmount, item.InputCurrency, item.PaymentCurrency, item.ExchangeRate, item.CostCenterCode);
                }
            }

            return result;
        }
    }
}
