﻿using CommonLib.Dtos;
using CommonLib.Repositories.Interfaces;
using PayrollServiceAPIs.Common.Constants;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos;
using PayrollServiceAPIs.PayRoll.Repositories.Interfaces;
using PayrollServiceAPIs.PayRoll.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace PayrollServiceAPIs.PayRoll.Services.Impl
{
    public class MasterPayrollItemService : IMasterPayRollItemService
    {
        private readonly IMasterPayrollItemRepository _payrollitemRepository;
        private readonly IGenericCodeRepository _genericCodeRepository;
        public MasterPayrollItemService(IMasterPayrollItemRepository payrollitemRepository, IGenericCodeRepository genericCodeRepository)
        {
            _payrollitemRepository = payrollitemRepository;
            _genericCodeRepository = genericCodeRepository;
        }

        /// <summary>
        /// Get All Master Payroll Item
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<MasterPayrollItemViewAll>> GetAll(ApiRequest<FilterRequest> request, string token)
        {
            ApiResponse<MasterPayrollItemViewAll> respone = new ApiResponse<MasterPayrollItemViewAll>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new MasterPayrollItemViewAll()
                {
                    Data = new List<MasterPayrollItemViewModel>()
                }
            };
            if (request.Data == null)
            {
                request.Data = new FilterRequest();
            }

            if (request.Data.Page <= 0) request.Data.Page = 1;
            if (request.Data.Size <= 0) request.Data.Page = 10;
            if (string.IsNullOrEmpty(request.Data.Sort)) request.Data.Sort = "PayrollItemId";

            var payrollItems = await _payrollitemRepository.GetAll(request.Data, request.Parameter.ClientId, request.Parameter.EntityId);
            List<string> genericCodeList = new List<string>();
            genericCodeList.Add(Common.Constants.GenericCode.STATUS);
            genericCodeList.Add(Common.Constants.GenericCode.PAYROLL_ITEM_FLAG);
            var genericCodes = await _genericCodeRepository.GetGenericCodes(genericCodeList, token, request.Parameter);
            List<GenericCodeResult> statuses = new List<GenericCodeResult>();
            if (genericCodes.ContainsKey(Common.Constants.GenericCode.STATUS.ToLower()))
            {
                statuses = genericCodes[Common.Constants.GenericCode.STATUS.ToLower()];
            }

            // Get master payroll flag include TypeId and DisplayName
            List<PayrollItemFlag> types = new List<PayrollItemFlag>();
            int intCodeValue = 0;
            if (genericCodes.ContainsKey(Common.Constants.GenericCode.PAYROLL_ITEM_FLAG))
            {
                types = genericCodes[Common.Constants.GenericCode.PAYROLL_ITEM_FLAG].Where(n => int.TryParse(n.CodeValue, out intCodeValue)).Select(n => CreatePayrollItemFlag(n)).ToList();
            }
            foreach (var item in payrollItems)
            {
                // Check status is ACTICE or UNACTICE
                if (statuses.Any(n => n.CodeValue == item.StatusCd))
                {
                    item.StatusText = statuses.First(n => n.CodeValue == item.StatusCd).TextValue;
                }
                // Get FlagId
                var flags = await _payrollitemRepository.GetMasterPayrollItem(item.PayrollItemId);
                Dictionary<string, bool> typeFlags = new Dictionary<string, bool>();
                foreach (var itemTypes in types)
                {
                    if (!typeFlags.ContainsKey(itemTypes.TypeId.ToString()))
                    {
                        typeFlags.Add(itemTypes.TypeId.ToString(), false);
                    }

                    if (flags.Contains(itemTypes.TypeId))
                        typeFlags[itemTypes.TypeId.ToString()] = true;
                }

                var masterPayrollItemView = new MasterPayrollItemViewModel()
                {
                    Detail = item,
                    Flags = typeFlags,
                    // Get Tax item flag
                    TaxableFlag = await _payrollitemRepository.CountPayrollItemTaxByPayrollItemId(item.PayrollItemId) > 0
                };

                respone.Data.Data.Add(masterPayrollItemView);
            }
            respone.Data.Types = types;
            return respone;
        }

        /// <summary>
        /// Get Payroll Item
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<MasterPayrollItemAddModel>> GetPayrollItem(ApiRequest request, string token)
        {
            ApiResponse<MasterPayrollItemAddModel> response = new ApiResponse<MasterPayrollItemAddModel>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new MasterPayrollItemAddModel()
            };

            List<MasterPayrollItemTaxModel> masterPayrollItemTaxs = new List<MasterPayrollItemTaxModel>();

            var genericCodes = await _genericCodeRepository.GetGenericCodes(new List<string>() { Common.Constants.GenericCode.PAYROLL_ITEM_TAX, Common.Constants.GenericCode.PAYROLL_ITEM_FLAG }, token, request.Parameter);
            List<GenericCodeResult> genericCodeTaxs = new List<GenericCodeResult>();
            if (genericCodes.ContainsKey(Common.Constants.GenericCode.PAYROLL_ITEM_TAX.ToLower()))
            {
                genericCodeTaxs = genericCodes[Common.Constants.GenericCode.PAYROLL_ITEM_TAX.ToLower()];
            }

            int intCodeValue = 0;
            List<TypeMasterModel> masterPayrollTaxSelect = genericCodeTaxs.Where(n => int.TryParse(n.CodeValue, out intCodeValue)).Select(n => new TypeMasterModel() { TypeId = int.Parse(n.CodeValue), DisplayName = n.TextValue }).ToList();

            // get genericCode Tax option
            var genericCodeTaxOption = await _genericCodeRepository.GetGenericCodes(masterPayrollTaxSelect.Select(n => Common.Constants.GenericCode.PAYROLL_ITEM_TAX + n.TypeId.ToString()).ToList(), token, request.Parameter);

            foreach (var item in masterPayrollTaxSelect)
            {
                var child = new List<TypeMasterModel>();
                if (genericCodeTaxOption.ContainsKey(Common.Constants.GenericCode.PAYROLL_ITEM_TAX + item.TypeId.ToString()))
                {
                    child = genericCodeTaxOption[Common.Constants.GenericCode.PAYROLL_ITEM_TAX + item.TypeId.ToString()].Where(n => int.TryParse(n.CodeValue, out intCodeValue)).Select(n => new TypeMasterModel() { TypeId = int.Parse(n.CodeValue), DisplayName = n.TextValue }).ToList();
                }

                masterPayrollItemTaxs.Add(new MasterPayrollItemTaxModel()
                {
                    Detail = item,
                    Child = child
                });

            }
            response.Data.Types = new List<PayrollItemFlag>();
            if (genericCodes.ContainsKey(Common.Constants.GenericCode.PAYROLL_ITEM_FLAG))
            {
                response.Data.Types = genericCodes[Common.Constants.GenericCode.PAYROLL_ITEM_FLAG].Where(n => int.TryParse(n.CodeValue, out intCodeValue)).Select(n => CreatePayrollItemFlag(n)).ToList();
            }
            response.Data.TaxSelect = masterPayrollItemTaxs;
            return response;
        }

        /// <summary>
        /// Create payroll item flag
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public PayrollItemFlag CreatePayrollItemFlag(GenericCodeResult item)
        {
            PayrollItemFlag result = new PayrollItemFlag();
            int intCodeValue = 0;
            if (int.TryParse(item.CodeValue, out intCodeValue))
            {
                result.TypeId = int.Parse(item.CodeValue);
            }

            if (!string.IsNullOrEmpty(item.TextValue))
            {
                var arrFlagName = item.TextValue.Split(":").ToList();
                if (arrFlagName.Count > 1)
                {
                    result.GroupName = arrFlagName.ElementAt(0);
                    result.DisplayName = item.TextValue.Substring(arrFlagName.ElementAt(0).Length + 1, item.TextValue.Length - arrFlagName.ElementAt(0).Length - 1);
                }
                else
                {
                    result.DisplayName = item.TextValue;
                }
            }

            return result;
        }

        /// <summary>
        /// Add MasterPayroll Item
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<int>> Store(ApiRequest<MasterPayrollItemStoreModel> request)
        {
            ApiResponse<int> response = new ApiResponse<int>
            {
                Ray = request.Ray ?? string.Empty,
                Data = 0
            };

            // check duplicate code
            if (!await _payrollitemRepository.CheckDuplicateCode(null, request.Data.ItemCode))
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = Level.ERROR,
                    LabelCode = MasterPayrollItemLabel.MPRI0107
                });
                return response;
            }

            MasterPayrollItemTaxStoreModel taxModel = new MasterPayrollItemTaxStoreModel();
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                request.Data.ClientId = request.Parameter.ClientId;
                request.Data.EntityId = request.Parameter.EntityId;

                int? newMasterPayrolllId = await _payrollitemRepository.Store(request.Data);
                if (!newMasterPayrolllId.HasValue)
                {
                    response.Data = newMasterPayrolllId.Value;
                    response.Message.Toast.Add(new MessageItem()
                    {
                        Level = Level.ERROR,
                        LabelCode = MasterPayrollItemLabel.MPRI0104
                    });
                    return response;
                }

                foreach (var item in request.Data.FlagIds)
                {
                    if (!await _payrollitemRepository.StoreFlag(newMasterPayrolllId.Value, item, request.Data.ModifiedBy))
                    {
                        response.Message.Toast.Add(new MessageItem()
                        {
                            Level = Level.ERROR,
                            LabelCode = MasterPayrollItemLabel.MPRI0104
                        });
                        return response;
                    }
                }

                foreach (var item in request.Data.TaxItemValue)
                {
                    if (!await _payrollitemRepository.StoreTaxValue(newMasterPayrolllId.Value, item.TaxItemId, item.TaxItemValue, request.Data.ModifiedBy))
                    {
                        response.Message.Toast.Add(new MessageItem()
                        {
                            Level = Level.ERROR,
                            LabelCode = MasterPayrollItemLabel.MPRI0104
                        });
                        return response;
                    }
                }

                scope.Complete();
            }

            response.Data = (int)StatusCode.SUCCESS;
            response.Message.Toast.Add(new MessageItem()
            {
                Level = Level.SUCCESS,
                LabelCode = MasterPayrollItemLabel.MPRI0103
            });
            return response;
        }


        /// <summary>
        /// Update MasterPayroll Item
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>

        public async Task<ApiResponse<int>> Update(int masterPayrollItemId, ApiRequest<MasterPayrollItemStoreModel> request, string token)

        {
            ApiResponse<int> response = new ApiResponse<int>
            {
                Ray = request.Ray ?? string.Empty,
                Data = 0
            };

            // check duplicate code
            if (!await _payrollitemRepository.CheckDuplicateCode(masterPayrollItemId, request.Data.ItemCode))
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = Level.ERROR,
                    LabelCode = MasterPayrollItemLabel.MPRI0107
                });
                return response;
            }

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                var objectMasterPayrollItem = await _payrollitemRepository.GetById(masterPayrollItemId);
                if (objectMasterPayrollItem == null)
                {
                    response.Message.Toast.Add(new MessageItem()
                    {
                        Level = Level.ERROR,
                        LabelCode = MasterPayrollItemLabel.MPRI0101
                    });
                    scope.Dispose();
                    return response;
                }

                if (await _payrollitemRepository.Update(masterPayrollItemId, request.Data) == null)
                {
                    response.Message.Toast.Add(new MessageItem()
                    {
                        Level = Level.ERROR,
                        LabelCode = MasterPayrollItemLabel.MPRI0106
                    });

                    return response;
                }

                List<int> flags = await _payrollitemRepository.GetMasterPayrollItem(masterPayrollItemId);
                if (request.Data.FlagIds == null)
                {
                    request.Data.FlagIds = new List<int>();
                }

                // Add Master Payroll Item
                foreach (var item in request.Data.FlagIds.Where(n => !flags.Contains(n)))
                {
                    if (!await _payrollitemRepository.StoreFlag(masterPayrollItemId, item, request.Data.ModifiedBy))
                    {
                        response.Message.Toast.Add(new MessageItem()
                        {
                            Level = Level.ERROR,
                            LabelCode = MasterPayrollItemLabel.MPRI0104
                        });
                        scope.Dispose();
                        return response;
                    }
                }

                List<MasterPayrollTaxEditModel> taxMasterPayroll = await _payrollitemRepository.GetTaxableUpdateMasterPayroll(masterPayrollItemId, request.Data.StatusCd);

                //Delete tax item
                foreach (var item in taxMasterPayroll)
                {
                    if (!await _payrollitemRepository.DeleteMasterPayrollItemTax(masterPayrollItemId, item.TaxItemId, request.Data.ModifiedBy))
                    {
                        response.Message.Toast.Add(new MessageItem()
                        {
                            Level = Level.ERROR,
                            LabelCode = MasterPayrollItemLabel.MPRI0108
                        });
                        scope.Dispose();
                        return response;
                    }
                }
                if (request.Data.TaxItemValue == null)
                {
                    request.Data.TaxItemValue = new List<MasterPayrollItemTaxStoreModel>();
                }

                // update
                foreach (var item in request.Data.TaxItemValue)
                {
                    if (!await _payrollitemRepository.StoreTaxValue(masterPayrollItemId, item.TaxItemId, item.TaxItemValue, request.Data.ModifiedBy))
                    {
                        response.Message.Toast.Add(new MessageItem()
                        {
                            Level = Level.ERROR,
                            LabelCode = MasterPayrollItemLabel.MPRI0104
                        });
                        scope.Dispose();
                        return response;
                    }
                }

                // delete flag in masy=ter payroll item
                foreach (var item in flags.Where(n => !request.Data.FlagIds.Contains(n)))
                {
                    if (!await _payrollitemRepository.DeleteMasterPayrollItem(masterPayrollItemId, item, request.Data.ModifiedBy))
                    {
                        response.Message.Toast.Add(new MessageItem()
                        {
                            Level = Level.ERROR,
                            LabelCode = MasterPayrollItemLabel.MPRI0108
                        });
                        scope.Dispose();
                        return response;
                    }
                }

                response.Data = masterPayrollItemId;
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = Level.SUCCESS,
                    LabelCode = MasterPayrollItemLabel.MPRI0105
                });

                scope.Complete();
            }
            return response;
        }

        /// <summary>
        /// Get MasterPayroll Item by Id
        /// </summary>
        /// <param name="request"></param>
        /// <param name="masterPayrollItemId"></param>  
        /// <returns></returns>

        public async Task<ApiResponse<MasterPayrollItemEditModel>> GetMasterPayrollItemById(int masterPayrollItemId, ApiRequest request, string token)

        {
            ApiResponse<MasterPayrollItemEditModel> response = new ApiResponse<MasterPayrollItemEditModel>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new MasterPayrollItemEditModel()
            };

            List<MasterPayrollItemTaxModel> masterPayrollItemTaxs = new List<MasterPayrollItemTaxModel>();
            var genericCodes = await _genericCodeRepository.GetGenericCodes(new List<string>() { Common.Constants.GenericCode.PAYROLL_ITEM_TAX, Common.Constants.GenericCode.PAYROLL_ITEM_FLAG }, token, request.Parameter);
            List<GenericCodeResult> genericCodeTaxs = new List<GenericCodeResult>();
            if (genericCodes.ContainsKey(Common.Constants.GenericCode.PAYROLL_ITEM_TAX.ToLower()))
            {
                genericCodeTaxs = genericCodes[Common.Constants.GenericCode.PAYROLL_ITEM_TAX.ToLower()];
            }

            int intCodeValue = 0;
            List<TypeMasterModel> masterPayrollTaxSelect = genericCodeTaxs.Where(n => int.TryParse(n.CodeValue, out intCodeValue)).Select(n => new TypeMasterModel() { TypeId = int.Parse(n.CodeValue), DisplayName = n.TextValue }).ToList();

            // get genericCode Tax option
            var genericCodeTaxOption = await _genericCodeRepository.GetGenericCodes(masterPayrollTaxSelect.Select(n => Common.Constants.GenericCode.PAYROLL_ITEM_TAX + n.TypeId.ToString()).ToList(), token, request.Parameter);

            foreach (var item in masterPayrollTaxSelect)
            {
                var child = new List<TypeMasterModel>();
                if (genericCodeTaxOption.ContainsKey(Common.Constants.GenericCode.PAYROLL_ITEM_TAX + item.TypeId.ToString()))
                {
                    child = genericCodeTaxOption[Common.Constants.GenericCode.PAYROLL_ITEM_TAX + item.TypeId.ToString()].Where(n => int.TryParse(n.CodeValue, out intCodeValue)).Select(n => new TypeMasterModel() { TypeId = int.Parse(n.CodeValue), DisplayName = n.TextValue }).ToList();
                }

                masterPayrollItemTaxs.Add(new MasterPayrollItemTaxModel()
                {
                    Detail = item,
                    Child = child
                });

            }
            response.Data.Types = new List<PayrollItemFlag>();
            if (genericCodes.ContainsKey(Common.Constants.GenericCode.PAYROLL_ITEM_FLAG))
            {
                response.Data.Types = genericCodes[Common.Constants.GenericCode.PAYROLL_ITEM_FLAG].Where(n => int.TryParse(n.CodeValue, out intCodeValue)).Select(n => CreatePayrollItemFlag(n)).ToList();
            }

            List<int> flags = await _payrollitemRepository.GetMasterPayrollItem(masterPayrollItemId);
            List<MasterPayrollTaxEditModel> masterPayrollItemTaxEdit = new List<MasterPayrollTaxEditModel>();
            List<MasterPayrollTaxEditModel> masterPayrollTax = await _payrollitemRepository.GetTaxableMasterPayroll(masterPayrollItemId);

            MasterPayrollViewModel payrollItem = await _payrollitemRepository.GetById(masterPayrollItemId);
            response.Data.TaxSelect = masterPayrollItemTaxs;
            response.Data.Detail = payrollItem;
            response.Data.FlagIds = flags;
            response.Data.Taxable = masterPayrollTax;
            return response;
        }

        /// <summary>
        /// Get Master Payroll Item by Flag
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<List<MasterPayrollItemModel>>> GetMasterPayrollItemByFlag(ApiRequest<List<int>> request)
        {
            return new ApiResponse<List<MasterPayrollItemModel>>
            {
                Ray = request.Ray ?? string.Empty,
                Data = await _payrollitemRepository.GetMasterPayrollItemByFlag(request.Data)
            };
        }
        public async Task<ApiResponse<bool>> Duplicate(int masterPayrollItemId, string strCreatedBy, ApiRequest<MasterPayrollItemDuplicateModel> request)
        {
            ApiResponse<bool> response = new ApiResponse<bool>
            {
                Ray = request.Ray ?? string.Empty,
                Data = false
            };

            var objmasterPayrollItem = await _payrollitemRepository.GetById(masterPayrollItemId);
            if (objmasterPayrollItem == null)
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = Level.ERROR,
                    LabelCode = MasterPayrollItemLabel.MPRI0101
                });
                return response;
            }

            // check duplicate code
            if (!await _payrollitemRepository.CheckDuplicateCode(null, request.Data.ItemCode))
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = Level.ERROR,
                    LabelCode = MasterPayrollItemLabel.MPRI0107
                });
                return response;
            }

            await _payrollitemRepository.Duplicate(masterPayrollItemId, strCreatedBy, request.Data.ItemName, request.Data.ItemCode);

            response.Data = true;
            response.Message.Toast.Add(new MessageItem()
            {
                Level = Level.SUCCESS,
                LabelCode = MasterPayrollItemLabel.MPRI0109
            });
            return response;
        }
    }
}
