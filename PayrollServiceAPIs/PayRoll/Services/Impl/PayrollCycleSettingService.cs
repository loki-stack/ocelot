﻿using CommonLib.Dtos;
using CommonLib.Repositories.Interfaces;
using PayrollServiceAPIs.Common.Constants;
using PayrollServiceAPIs.PayRoll.Dtos.PayrollCycleSetting;
using PayrollServiceAPIs.PayRoll.Repositories.Interfaces;
using PayrollServiceAPIs.PayRoll.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Services.Impl
{
    public class PayrollCycleSettingService : IPayrollCycleSettingService
    {
        private readonly IPayrollCycleSettingRepository _payrollCycleSettingRepository;
        private readonly IGenericCodeRepository _genericCodeRepository;
        public PayrollCycleSettingService(IPayrollCycleSettingRepository payrollCycleSettingRepository, IGenericCodeRepository genericCodeRepository)
        {
            _payrollCycleSettingRepository = payrollCycleSettingRepository;
            _genericCodeRepository = genericCodeRepository;
        }

        /// <summary>
        /// Get data payroll cycle setting for screen edit
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<PayrollCycleSettingEditModel>> Edit(ApiRequest request)
        {
            ApiResponse<PayrollCycleSettingEditModel> response = new ApiResponse<PayrollCycleSettingEditModel>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new PayrollCycleSettingEditModel()
            };

            response.Data.Detail = await _payrollCycleSettingRepository.GetSeting();
            response.Data.Prequency = new List<GenericCodeResult>()
            {
                new GenericCodeResult()
                {
                    CodeValue = "Monthly",
                    TextValue = "Monthly"
                }
            };

            return response;
        }

        /// <summary>
        /// Update Payroll Cycle setting
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<bool>> Update(ApiRequest<PayrollCycleSettingUpdateModel> request)
        {
            ApiResponse<bool> response = new ApiResponse<bool>
            {
                Ray = request.Ray ?? string.Empty,
                Data = false
            };

            await _payrollCycleSettingRepository.Update(request.Data);

            response.Message.Toast.Add(new MessageItem()
            {
                Level = CommonLib.Constants.Level.SUCCESS,
                LabelCode = Label.PAY0901
            });

            response.Data = true;
            return response;
        }

    }
}
