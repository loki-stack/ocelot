﻿using CommonLib.Dtos;
using CommonLib.Repositories.Interfaces;
using PayrollServiceAPIs.Common.Constants;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos.PayrollCycle;
using PayrollServiceAPIs.PayRoll.Repositories.Interfaces;
using PayrollServiceAPIs.PayRoll.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace PayrollServiceAPIs.PayRoll.Services.Impl
{
    public class PayrollCycleService : IPayrollCycleService
    {
        private readonly IPayrollCycleRepository _payrollCycleRepository;
        private readonly IPayrollCycleSettingRepository _payrollCycleSettingRepository;
        private readonly IGenericCodeRepository _genericCodeRepository;
        public PayrollCycleService(IPayrollCycleRepository payrollCycleRepository, IGenericCodeRepository genericCodeRepository, IPayrollCycleSettingRepository payrollCycleSettingRepository)
        {
            _payrollCycleRepository = payrollCycleRepository;
            _genericCodeRepository = genericCodeRepository;
            _payrollCycleSettingRepository = payrollCycleSettingRepository;
        }

        /// <summary>
        /// Get All Payroll Run
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ApiResponse<List<PayrollCycleViewModel>>> GetAll(ApiRequest<FilterRequest> request, string token)
        {
            ApiResponse<List<PayrollCycleViewModel>> response = new ApiResponse<List<PayrollCycleViewModel>>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new List<PayrollCycleViewModel>()
            };

            if (request.Data == null)
            {
                request.Data = new FilterRequest();
            }

            response.Data = await _payrollCycleRepository.GetAll();
            List<string> genericCodeList = new List<string>();
            genericCodeList.Add(Common.Constants.GenericCode.STATUS);
            var genericCodes = await _genericCodeRepository.GetGenericCodes(genericCodeList, token, request.Parameter);
            List<GenericCodeResult> statuses = new List<GenericCodeResult>();
            if (genericCodes.ContainsKey(Common.Constants.GenericCode.STATUS.ToLower()))
            {
                statuses = genericCodes[Common.Constants.GenericCode.STATUS.ToLower()];
            }

            foreach (var item in response.Data)
            {
                if (statuses.Any(n => n.CodeValue == item.StatusCd))
                {
                    item.StatusText = statuses.First(n => n.CodeValue == item.StatusCd).TextValue;
                }
            }

            // search by text
            if (!string.IsNullOrEmpty(request.Data.FullTextSearch))
            {
                response.Data = response.Data.Where(x => x.PayrollCycleCode.ToLower().Contains(request.Data.FullTextSearch.ToLower()))
                                         .ToList();
            }

            if (request.Data.Page.HasValue && request.Data.Size.HasValue)
            {
                response.Data = response.Data.Skip((request.Data.Page.Value - 1) * request.Data.Size.Value).Take(request.Data.Size.Value).ToList();
            }

            return response;
        }

        /// <summary>
        /// Add Payroll cycle
        /// </summary>
        /// <param name="request"></param>
        /// <param name="createBy"></param>
        /// <returns></returns>
        public async Task<ApiResponse<bool>> Store(ApiRequest<int> request, string createBy)
        {
            ApiResponse<bool> response = new ApiResponse<bool>
            {
                Ray = request.Ray ?? string.Empty,
                Data = false
            };

            // check input Year
            if (request.Data < DateTime.Now.Year - 15 || request.Data > DateTime.Now.Year + 3)
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0809
                });
                return response;
            }

            // Get Payroll Cycle Setting
            var payrollCycleSetting = await _payrollCycleSettingRepository.GetSeting();
            if (payrollCycleSetting == null)
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0810
                });
                return response;
            }

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    for (var i = 1; i <= 12; i++)
                    {
                        int days = DateTime.DaysInMonth(request.Data, i);

                        if (!await _payrollCycleRepository.CheckPayrollCycleByStartDate(new DateTimeOffset(new DateTime(request.Data, i, payrollCycleSetting.StartDay <= days ? payrollCycleSetting.StartDay : days))))
                        {
                            DateTimeOffset dateStart = new DateTimeOffset(new DateTime(request.Data, i, payrollCycleSetting.StartDay <= days ? payrollCycleSetting.StartDay : days));
                            DateTimeOffset dateEnd = dateStart.AddMonths(1).AddDays(-1);
                            await _payrollCycleRepository.Store(new PayrollCycleStoreModel()
                            {
                                PayrollCycleCode = string.Format("{0} - {1}", dateStart.ToString("ddMM"), dateEnd.ToString("ddMM")),
                                DateStart = dateStart,
                                CutOffDate = new DateTimeOffset(new DateTime(request.Data, i, payrollCycleSetting.CutOffDay <= days ? payrollCycleSetting.CutOffDay : days)),
                                DateEnd = dateEnd,
                                CreatedBy = createBy,
                                ModifiedBy = createBy
                            });
                        }
                    }
                    scope.Complete();
                }
                catch (Exception ex)
                {
                    response.Message.Toast.Add(new MessageItem()
                    {
                        Level = Level.ERROR,
                        LabelCode = Label.PAY0802
                    });
                    scope.Dispose();
                    throw ex;
                }
            }

            response.Data = true;
            response.Message.Toast.Add(new MessageItem()
            {
                Level = CommonLib.Constants.Level.SUCCESS,
                LabelCode = Label.PAY0803
            });

            return response;
        }

        /// <summary>
        /// Get data payroll cycle for screen edit
        /// </summary>
        /// <param name="payrollCycleId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<PayrollCycleViewModel>> Edit(int payrollCycleId, ApiRequest request)
        {
            ApiResponse<PayrollCycleViewModel> response = new ApiResponse<PayrollCycleViewModel>
            {
                Ray = request.Ray ?? string.Empty,
                Data = await _payrollCycleRepository.GetById(payrollCycleId)
            };

            return response;
        }

        /// <summary>
        /// Update Payroll Cycle
        /// </summary>
        /// <param name="payrollCycleId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<int>> Update(int payrollCycleId, ApiRequest<PayrollCycleStoreModel> request)
        {
            ApiResponse<int> response = new ApiResponse<int>
            {
                Ray = request.Ray ?? string.Empty,
                Data = 0
            };

            var objPayrollCycle = await _payrollCycleRepository.GetById(payrollCycleId);
            if (objPayrollCycle == null)
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0804
                });
                return response;
            }
            // check payroll cycle link payroll run
            if (await _payrollCycleRepository.CheckLinkPayrollRun(payrollCycleId))
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0808
                });

                return response;
            }

            // check payroll cycle code
            objPayrollCycle = await _payrollCycleRepository.GetByCode(request.Data.PayrollCycleCode);
            if (objPayrollCycle != null && objPayrollCycle.PayrollCycleId != payrollCycleId)
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0801
                });

                return response;
            }

            // update payroll run
            if (await _payrollCycleRepository.Update(payrollCycleId, request.Data) == null)
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0805
                });
            }
            else
            {
                response.Data = payrollCycleId;
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.SUCCESS,
                    LabelCode = Label.PAY0806
                });
            }

            return response;
        }

        /// <summary>
        /// Delete Payroll Cycle
        /// </summary>
        /// <param name="request"></param>
        /// <param name="modifiedBy"></param>
        /// <returns></returns>
        public async Task<ApiResponse<bool>> Delete(ApiRequest<List<int>> request, string modifiedBy)
        {
            ApiResponse<bool> response = new ApiResponse<bool>
            {
                Ray = request.Ray ?? string.Empty,
                Data = false
            };

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                foreach (var item in request.Data)
                {
                    // check payroll cycle link payroll run
                    if (await _payrollCycleRepository.CheckLinkPayrollRun(item))
                    {
                        response.Message.Toast.Add(new MessageItem()
                        {
                            Level = CommonLib.Constants.Level.ERROR,
                            LabelCode = Label.PAY0808
                        });
                        scope.Dispose();
                        return response;
                    }

                    if (!await _payrollCycleRepository.Delete(item, modifiedBy))
                    {
                        response.Message.Toast.Add(new MessageItem()
                        {
                            Level = Level.ERROR,
                            LabelCode = Label.PAY0804
                        });
                        scope.Dispose();
                        return response;
                    }
                }

                scope.Complete();
            }

            response.Data = true;
            response.Message.Toast.Add(new MessageItem()
            {
                Level = Level.SUCCESS,
                LabelCode = Label.PAY0807
            });

            return response;
        }
    }
}
