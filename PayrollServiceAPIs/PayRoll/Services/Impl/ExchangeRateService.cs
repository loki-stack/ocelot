﻿using CommonLib.Dtos;
using CommonLib.Repositories.Interfaces;
using PayrollServiceAPIs.Common.Constants;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos.ExchangeRate;
using PayrollServiceAPIs.PayRoll.Repositories.Interfaces;
using PayrollServiceAPIs.PayRoll.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Services.Impl
{
    public class ExchangeRateService : IExchangeRateService
    {
        private readonly IExchangeRateRepository _exchangeRateRepository;
        private readonly IGenericCodeRepository _genericCodeRepository;
        public ExchangeRateService(IExchangeRateRepository exchangeRateRepository, IGenericCodeRepository genericCodeRepository)
        {
            _exchangeRateRepository = exchangeRateRepository;
            _genericCodeRepository = genericCodeRepository;
        }

        /// <summary>
        /// Get All Exchange Rate
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ApiResponse<List<ExchangeRateViewModel>>> GetAll(ApiRequest<FilterRequest> request, string token)
        {
            ApiResponse<List<ExchangeRateViewModel>> response = new ApiResponse<List<ExchangeRateViewModel>>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new List<ExchangeRateViewModel>()
            };

            if (request.Data == null)
            {
                request.Data = new FilterRequest();
            }

            response.Data = await _exchangeRateRepository.GetAll(request.Data);

            return response;
        }

        /// <summary>
        /// Get data screen create exchange rate
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ApiResponse<ExchangeRateCreateModel>> Create(ApiRequest request, string token)
        {
            ApiResponse<ExchangeRateCreateModel> response = new ApiResponse<ExchangeRateCreateModel>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new ExchangeRateCreateModel()
            };

            var genericCodes = await _genericCodeRepository.GetGenericCodes(new List<string>() { Common.Constants.GenericCode.CURRENCY }, token, request.Parameter);
            if (genericCodes.ContainsKey(Common.Constants.GenericCode.CURRENCY.ToLower()))
            {
                response.Data.Currency = genericCodes[Common.Constants.GenericCode.CURRENCY.ToLower()];
            }

            response.Data.PayrollCycle = await _exchangeRateRepository.GetPayrollCycle();

            return response;
        }

        /// <summary>
        /// Get data exchange rate for screen edit
        /// </summary>
        /// <param name="exchangeRateId"></param>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ApiResponse<ExchangeRateEditModel>> Edit(int exchangeRateId, ApiRequest request, string token)
        {
            ApiResponse<ExchangeRateEditModel> response = new ApiResponse<ExchangeRateEditModel>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new ExchangeRateEditModel()
            };

            var genericCodes = await _genericCodeRepository.GetGenericCodes(new List<string>() { Common.Constants.GenericCode.CURRENCY }, token, request.Parameter);
            if (genericCodes.ContainsKey(Common.Constants.GenericCode.CURRENCY.ToLower()))
            {
                response.Data.Currency = genericCodes[Common.Constants.GenericCode.CURRENCY.ToLower()];
            }

            response.Data.Detail = await _exchangeRateRepository.GetById(exchangeRateId);
            response.Data.PayrollCycle = await _exchangeRateRepository.GetPayrollCycle();

            return response;
        }

        /// <summary>
        /// Add Exchange rate
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ApiResponse<int>> Store(ApiRequest<ExchangeRateStoreModel> request, string token)
        {
            ApiResponse<int> response = new ApiResponse<int>
            {
                Ray = request.Ray ?? string.Empty,
                Data = 0
            };

            List<GenericCodeResult> currencies = new List<GenericCodeResult>();
            var genericCodes = await _genericCodeRepository.GetGenericCodes(new List<string>() { Common.Constants.GenericCode.CURRENCY }, token, request.Parameter);
            if (genericCodes.ContainsKey(Common.Constants.GenericCode.CURRENCY.ToLower()))
            {
                currencies = genericCodes[Common.Constants.GenericCode.CURRENCY.ToLower()];
            }

            // check currency from 
            if (!currencies.Any(n => n.CodeValue == request.Data.CurrencyFrom))
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0502
                });

                return response;
            }

            // check currency to 
            if (!currencies.Any(n => n.CodeValue == request.Data.CurrencyTo))
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0503
                });

                return response;
            }

            // check average rate
            if (request.Data.AverageRate <= 0)
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0509
                });

                return response;
            }

            // check month end rate
            if (request.Data.MonthEndRate <= 0)
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0510
                });

                return response;
            }

            // check handle period
            if (!await _exchangeRateRepository.CheckPayrollCycleExist(request.Data.PayrollCycleId))
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0501
                });

                return response;
            }

            int? newId = await _exchangeRateRepository.Store(request.Data);
            if (!newId.HasValue)
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0504
                });
            }
            else
            {
                response.Data = newId.Value;
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.SUCCESS,
                    LabelCode = Label.PAY0505
                });
            }

            return response;
        }

        /// <summary>
        /// Update exchange rate
        /// </summary>
        /// <param name="exchangeRateId"></param>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ApiResponse<int>> Update(int exchangeRateId, ApiRequest<ExchangeRateStoreModel> request, string token)
        {
            ApiResponse<int> response = new ApiResponse<int>
            {
                Ray = request.Ray ?? string.Empty,
                Data = 0
            };

            var objPayrollRun = await _exchangeRateRepository.GetById(exchangeRateId);
            if (objPayrollRun == null)
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0506
                });
                return response;
            }

            List<GenericCodeResult> currencies = new List<GenericCodeResult>();
            var genericCodes = await _genericCodeRepository.GetGenericCodes(new List<string>() { Common.Constants.GenericCode.CURRENCY }, token, request.Parameter);
            if (genericCodes.ContainsKey(Common.Constants.GenericCode.CURRENCY.ToLower()))
            {
                currencies = genericCodes[Common.Constants.GenericCode.CURRENCY.ToLower()];
            }

            // check currency from 
            if (!currencies.Any(n => n.CodeValue == request.Data.CurrencyFrom))
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0502
                });

                return response;
            }

            // check currency to 
            if (!currencies.Any(n => n.CodeValue == request.Data.CurrencyTo))
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0503
                });

                return response;
            }

            // check average rate
            if (request.Data.AverageRate <= 0)
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0509
                });

                return response;
            }

            // check month end rate
            if (request.Data.MonthEndRate <= 0)
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0510
                });

                return response;
            }

            // check handle period
            if (!await _exchangeRateRepository.CheckPayrollCycleExist(request.Data.PayrollCycleId))
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0501
                });

                return response;
            }

            // update exchange rate
            if (await _exchangeRateRepository.Update(exchangeRateId, request.Data) == null)
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.ERROR,
                    LabelCode = Label.PAY0507
                });
            }
            else
            {
                response.Data = exchangeRateId;
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = CommonLib.Constants.Level.SUCCESS,
                    LabelCode = Label.PAY0508
                });
            }

            return response;
        }

    }
}
