﻿using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using PayrollServiceAPIs.Common.Constants;
using PayrollServiceAPIs.PayRoll.Dtos;
using PayrollServiceAPIs.PayRoll.Repositories.Interfaces;
using PayrollServiceAPIs.PayRoll.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Services.Impl
{
    public class RegisteredSchemeService : IRegisteredSchemeService
    {
        private readonly IRegisteredSchemeRepository _registeredSchemeRepository;
        private readonly IMPFTrusteeRepository _mpfTrusteeRepository;
        public RegisteredSchemeService(IRegisteredSchemeRepository registeredSchemeRepository, IMPFTrusteeRepository mpfTrusteeRepository)
        {
            _registeredSchemeRepository = registeredSchemeRepository;
            _mpfTrusteeRepository = mpfTrusteeRepository;
        }

        public async Task<ApiResponse<bool?>> Delete(int registeredSchemeId, ApiRequest request)
        {
            ApiResponse<bool?> response = new ApiResponse<bool?>();
            response.Ray = request.Ray;
            TbPayMpfRegisteredScheme model = await _registeredSchemeRepository.GetById(registeredSchemeId);
            if (model == null)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.RS001
                });
                response.Data = null;
                return response;
            }
            bool isDelete = await _registeredSchemeRepository.Delete(registeredSchemeId);
            if (!isDelete)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.RS002
                });
                response.Data = null;
                return response;
            }
            response.Data = true;
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetAll(ApiRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();
            List<MPFRegisteredSchemeViewList> result = await _registeredSchemeRepository.GetAll();
            response.Data.Add("totalElements", result.Count);
            response.Data.Add("mpfRegisteredSchemes", result);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetById(int mpfRegisteredSchemeId, ApiRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();

            //get MPF Registered Scheme information
            TbPayMpfRegisteredScheme mpfRegisteredScheme = await _registeredSchemeRepository.GetById(mpfRegisteredSchemeId);
            if (mpfRegisteredScheme == null)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.RS001
                });
                response.Data = null;
                return response;
            }
            response.Data.Add("mpfRegisteredScheme", mpfRegisteredScheme);

            //get MPF Trustee information
            TbPayMpfTrustee mpfTrustee = await _mpfTrusteeRepository.GetByTrusteeId(mpfRegisteredScheme.TrusteeId);
            response.Data.Add("mpfTrustee", mpfTrustee);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> Update(int registeredSchemeId, ApiRequest<MPFRegisteredSchemeRequest> request, string modifiedBy)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();

            TbPayMpfRegisteredScheme model = await _registeredSchemeRepository.GetById(registeredSchemeId);
            if (model == null)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.RS001
                });
                response.Data = null;
                return response;
            }

            //validate registerd scheme property
            var resultCheck = new CommonLib.Validation.Validator(null).Validate(request.Data);
            if (resultCheck.Count > 0)
            {
                response.Message.ValidateResult.AddRange(resultCheck);
                response.Data = null;
                return response;
            }

            TbPayMpfRegisteredScheme updateModel = await _registeredSchemeRepository.Update(registeredSchemeId, request.Data, modifiedBy);
            if (updateModel == null)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.RS003
                });
                response.Data = null;
                return response;
            }
            response.Data.Add("mpfRegisteredScheme", updateModel);
            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.RS004
            });
            return response;
        }
    }
}
