﻿using AutoMapper;
using CommonLib.Dtos;
using CommonLib.Models.Client;
using Newtonsoft.Json;
using PayrollServiceAPIs.Common.Constants;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.Common.Models;
using PayrollServiceAPIs.PayRoll.Dtos.EnrolledScheme;
using PayrollServiceAPIs.PayRoll.Repositories.Interfaces;
using PayrollServiceAPIs.PayRoll.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Services.Impl
{
    public class EnrolledSchemeService : IEnrolledSchemeService
    {
        private readonly IEnrolledSchemeRepository _enrolledchemeRepository;
        private readonly IPayrollItemRepository _payrollItemRepository;
        private readonly IMapper _mapper;
        public EnrolledSchemeService(IEnrolledSchemeRepository enrolledSchemeRepository,
                                     IPayrollItemRepository payrollItemRepository,
                                     IMapper mapper)
        {
            _enrolledchemeRepository = enrolledSchemeRepository;
            _payrollItemRepository = payrollItemRepository;
            _mapper = mapper;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> Create(ApiRequest<EnrolledSchemeRequest> request, string createdBy)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();
            EnrolledSchemeRequest modelRequest = request.Data;

            //check enroll scheme code exist
            bool isSchemeCodeExist = _enrolledchemeRepository.CheckSchemeCodeExist(request.Data.EnrolledSchemeCode, request.Parameter.EntityId);
            if (isSchemeCodeExist)
            {
                response.Message.ValidateResult.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.PS0108,
                    ComponentId = "enrolledSchemeCode"
                });
                response.Data = null;
                return response;
            }

            EnrolledSchemeViewList viewList = await _enrolledchemeRepository.Create(modelRequest, request.Parameter.EntityId, createdBy);
            if (viewList == null)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.PS0101
                });
                response.Data = null;
                return response;
            }
            List<TbPayPayrollItem> payrollItems = await _payrollItemRepository.Create(viewList.EnrolledSchemeId, viewList.EnrolledSchemeType, modelRequest.VcPayrollItemIds);

            response.Data.Add("enrolledScheme", viewList);
            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.PS0102
            });
            return response;
        }

        public async Task<ApiResponse<int?>> Delete(int enrolledSchemeId, ApiRequest request)
        {
            ApiResponse<int?> response = new ApiResponse<int?>();
            response.Ray = request.Ray;
            bool isExist = await _enrolledchemeRepository.CheckExist(enrolledSchemeId, request.Parameter.EntityId);
            if (!isExist)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.PS0103
                });
                response.Data = null;
                return response;
            }
            bool isDeleted = await _enrolledchemeRepository.Delete(enrolledSchemeId, request.Parameter.EntityId);
            if (!isDeleted)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.PS0106
                });
                response.Data = null;
                return response;
            }
            response.Data = enrolledSchemeId;
            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.PS0107
            });
            return response;
        }

        public async Task<ApiResponse<bool?>> DeleteMany(ApiRequest<List<EnrolledSchemeEntityRequest>> request)
        {
            ApiResponse<bool?> response = new ApiResponse<bool?>();
            response.Ray = request.Ray;
            bool isDeleted = await _enrolledchemeRepository.DeleteMany(request.Data);
            if (!isDeleted)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.PS0106
                });
                response.Data = null;
                return response;
            }
            response.Data = true;
            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.PS0107
            });
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetById(int enrolledSchemeId, ApiRequest request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();
            TbPayEnrolledScheme enrolledScheme = await _enrolledchemeRepository.GetByEnrolledSchemeId(enrolledSchemeId, request.Parameter.EntityId);
            if (enrolledScheme == null)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.PS0103
                });
                response.Data = null;
                return response;
            }
            EnrolledSchemeViewDetail viewDetail = _mapper.Map<TbPayEnrolledScheme, EnrolledSchemeViewDetail>(enrolledScheme);
            viewDetail.VcPayrollItem = await _payrollItemRepository.GetPayRollItemByScheme(enrolledScheme.EnrolledSchemeId, enrolledScheme.EnrolledSchemeType);
            response.Data.Add("enrolledScheme", viewDetail);
            response.Data.Add("entityId", request.Parameter.EntityId);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> GetByRegisteredSchemeId(int registeredSchemeId, ApiRequest<EnrolledSchemeGetAllRequest> request)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();
            EnrolledSchemeGetAllRequest paginationRequest = request.Data;
            List<EnrolledSchemeViewList> enrolledSchemes = await _enrolledchemeRepository.GetByRegisteredSchemeId(registeredSchemeId);

            EnrolledSchemeFilter filter = new EnrolledSchemeFilter();
            // get filter from request
            if (!string.IsNullOrEmpty(paginationRequest.Filter))
            {
                filter = JsonConvert.DeserializeObject<EnrolledSchemeFilter>(paginationRequest.Filter);
            }

            // sort by item from request
            if (!string.IsNullOrEmpty(paginationRequest.Sort))
            {
                char sortCharacter = paginationRequest.Sort.ElementAt(0);
                string sortField = paginationRequest.Sort.Substring(1, paginationRequest.Sort.Length - 1);
                if (sortField.ToLower().Contains("enrolledschemename"))
                {
                    if (sortCharacter == '-')
                        enrolledSchemes = enrolledSchemes.OrderByDescending(x => x.EnrolledSchemeName).ToList();
                    else enrolledSchemes = enrolledSchemes.OrderBy(x => x.EnrolledSchemeName).ToList();
                }
                if (sortField.ToLower().Contains("enrolledschemecode"))
                {
                    if (sortCharacter == '-')
                        enrolledSchemes = enrolledSchemes.OrderByDescending(x => x.EnrolledSchemeCode).ToList();
                    else enrolledSchemes = enrolledSchemes.OrderBy(x => x.EnrolledSchemeCode).ToList();
                }
                if (sortField.ToLower().Contains("enrolledschemetype"))
                {
                    if (sortCharacter == '-')
                        enrolledSchemes = enrolledSchemes.OrderByDescending(x => x.EnrolledSchemeType).ToList();
                    else enrolledSchemes = enrolledSchemes.OrderBy(x => x.EnrolledSchemeType).ToList();
                }
                if (sortField.ToLower().Contains("enrolledschemeno"))
                {
                    if (sortCharacter == '-')
                        enrolledSchemes = enrolledSchemes.OrderByDescending(x => x.EnrolledSchemeNo).ToList();
                    else enrolledSchemes = enrolledSchemes.OrderBy(x => x.EnrolledSchemeNo).ToList();
                }
                if (sortField.ToLower().Contains("statuscd"))
                {
                    if (sortCharacter == '-')
                        enrolledSchemes = enrolledSchemes.OrderByDescending(x => x.StatusCd).ToList();
                    else enrolledSchemes = enrolledSchemes.OrderBy(x => x.StatusCd).ToList();
                }
            }

            // search by filter
            if (!string.IsNullOrEmpty(filter.EnrolledSchemeType))
            {
                enrolledSchemes = enrolledSchemes.Where(x => x.EnrolledSchemeType == filter.EnrolledSchemeType).ToList();
            }
            if (!string.IsNullOrEmpty(filter.StatusCd))
            {
                enrolledSchemes = enrolledSchemes.Where(x => x.StatusCd == filter.StatusCd).ToList();
            }
            // search by text
            if (!string.IsNullOrEmpty(paginationRequest.FullTextSearch))
            {
                enrolledSchemes = enrolledSchemes.Where(x => x.EnrolledSchemeName.ToLower().Contains(paginationRequest.FullTextSearch) ||
                                                           x.EnrolledSchemeCode.ToLower().Contains(paginationRequest.FullTextSearch) ||
                                                           x.EnrolledSchemeNo.ToLower().Contains(paginationRequest.FullTextSearch)).ToList();
            }

            // return data
            Pagination<EnrolledSchemeViewList> pagination = new Pagination<EnrolledSchemeViewList>();
            if (request.Data.Size.HasValue)
            {
                
                pagination.Page = paginationRequest.Page.Value;
                pagination.Size = paginationRequest.Size.Value;
                pagination.TotalPages = (enrolledSchemes.Count + pagination.Size - 1) / pagination.Size;
                pagination.TotalElements = enrolledSchemes.Count;

                enrolledSchemes = enrolledSchemes.Skip((pagination.Page - 1) * pagination.Size).Take(pagination.Size).ToList();
                pagination.NumberOfElements = enrolledSchemes.Count;
                pagination.Content = enrolledSchemes;
            }
            else
            {
                pagination.TotalElements = enrolledSchemes.Count;
                pagination.Content = enrolledSchemes;
            }
            response.Data.Add("pagination", pagination);
            return response;
        }

        public async Task<ApiResponse<Dictionary<string, object>>> Update(int schemeId, ApiRequest<EnrolledSchemeRequest> request, string modifiedBy)
        {
            ApiResponse<Dictionary<string, object>> response = new ApiResponse<Dictionary<string, object>>();
            response.Ray = request.Ray;
            response.Data = new Dictionary<string, object>();
            bool isExist = await _enrolledchemeRepository.CheckExist(schemeId, request.Parameter.EntityId);
            if (!isExist)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.PS0103
                });
                response.Data = null;
                return response;
            }

            EnrolledSchemeViewList viewList = await _enrolledchemeRepository.Update(schemeId, request.Data, request.Parameter.EntityId, modifiedBy);
            if (viewList == null)
            {
                response.Message.Toast.Add(new MessageItem
                {
                    Level = Level.ERROR,
                    LabelCode = Label.PS0104
                });
                response.Data = null;
                return response;
            }
            List<TbPayPayrollItem> payrollItems = await _payrollItemRepository.Create(viewList.EnrolledSchemeId, viewList.EnrolledSchemeType, request.Data.VcPayrollItemIds);
            response.Data.Add("enrolledScheme", viewList);
            response.Message.Toast.Add(new MessageItem
            {
                Level = Level.SUCCESS,
                LabelCode = Label.PS0105
            });
            return response;
        }
    }
}
