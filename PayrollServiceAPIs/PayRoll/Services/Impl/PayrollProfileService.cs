﻿using CommonLib.Dtos;
using CommonLib.Models.Client;
using CommonLib.Repositories.Impl;
using CommonLib.Repositories.Interfaces;
using PayrollServiceAPIs.Common.Constants;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.Common.Models;
using PayrollServiceAPIs.PayRoll.Dtos;
using PayrollServiceAPIs.PayRoll.Repositories.Interfaces;
using PayrollServiceAPIs.PayRoll.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace PayrollServiceAPIs.PayRoll.Services.Impl
{
    public class PayrollProfileService : IPayrollProfileService
    {
        private readonly IPayrollProfileRepository _payrollProfileRepository;
        private readonly IGenericCodeRepository _genericCodeRepository;
        public PayrollProfileService(IPayrollProfileRepository payrollProfileRepository, IGenericCodeRepository genericCodeRepository)
        {
            _payrollProfileRepository = payrollProfileRepository;
            _genericCodeRepository = genericCodeRepository;
        }

        /// <summary>
        /// Get All Payroll Profile
        /// </summary>
        /// <param name="request"></param>
        /// <param name="clientId"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public async Task<ApiResponse<List<PayrollProfileViewModel>>> GetAll(ApiRequest<FilterRequest> request, string token)
        {
            ApiResponse<List<PayrollProfileViewModel>> response = new ApiResponse<List<PayrollProfileViewModel>>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new List<PayrollProfileViewModel>()
            };

            if (request.Data == null)
            {
                request.Data = new FilterRequest();
            }

            if (request.Data.Page <= 0)
                request.Data.Page = 1;
            if (request.Data.Size <= 0)
                request.Data.Page = 10;
            if (string.IsNullOrEmpty(request.Data.Sort))
                request.Data.Sort = "PayrollProfileId";

            response.Data = await _payrollProfileRepository.GetAll(request.Data, request.Parameter.ClientId, request.Parameter.EntityId);
            List<string> genericCodeList = new List<string>();
            genericCodeList.Add(Common.Constants.GenericCode.STATUS);
            var genericCodes = await _genericCodeRepository.GetGenericCodes(genericCodeList, token, request.Parameter);
            List<GenericCodeResult> statuses = new List<GenericCodeResult>();
            if (genericCodes.ContainsKey(Common.Constants.GenericCode.STATUS.ToLower()))
            {
                statuses = genericCodes[Common.Constants.GenericCode.STATUS.ToLower()];
            }

            foreach (var item in response.Data)
            {
                if (statuses.Any(n => n.CodeValue == item.StatusCd))
                {
                    item.StatusText = statuses.First(n => n.CodeValue == item.StatusCd).TextValue;
                }
            }

            return response;
        }

        /// <summary>
        /// Add Payroll Profile
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<int>> Store(ApiRequest<PayrollProfileStoreModel> request)
        {
            ApiResponse<int> response = new ApiResponse<int>
            {
                Ray = request.Ray ?? string.Empty,
                Data = 0
            };
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    int? newId = await _payrollProfileRepository.Store(request.Data);
                    if (!newId.HasValue)
                    {
                        response.Message.Toast.Add(new MessageItem()
                        {
                            Level = Level.ERROR,
                            LabelCode = Label.PAY0201
                        });
                        scope.Dispose();
                    }
                    else
                    {
                        if (request.Data.PayrollItems.Any())
                        {
                            foreach (var item in request.Data.PayrollItems)
                            {
                                if (!await _payrollProfileRepository.CheckExistMasterPayrollItem(item))
                                {
                                    scope.Dispose();
                                    response.Message.Toast.Add(new MessageItem()
                                    {
                                        Level = Level.ERROR,
                                        LabelCode = Label.PAY0210
                                    });
                                    return response;
                                }

                                if (!await _payrollProfileRepository.StorePayrollProfileItem(newId.Value, item, request.Data.ModifiedBy))
                                {
                                    scope.Dispose();
                                    response.Message.Toast.Add(new MessageItem()
                                    {
                                        Level = Level.ERROR,
                                        LabelCode = Label.PAY0202
                                    });
                                    return response;
                                }
                            }
                        }
                        response.Data = newId.Value;
                        response.Message.Toast.Add(new MessageItem()
                        {
                            Level = Level.SUCCESS,
                            LabelCode = Label.PAY0203
                        });
                        scope.Complete();
                    }
                    return response;
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                };

            }
            return response;
        }

        /// <summary>
        /// Update Payroll Profile
        /// </summary>
        /// <param name="payrollProfileId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<int>> Update(int payrollProfileId, ApiRequest<PayrollProfileStoreModel> request)
        {
            ApiResponse<int> response = new ApiResponse<int>
            {
                Ray = request.Ray ?? string.Empty,
                Data = 0
            };
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var objPayrollProfile = await _payrollProfileRepository.GetById(payrollProfileId);
                    if (objPayrollProfile == null)
                    {
                        response.Message.Toast.Add(new MessageItem()
                        {
                            Level = Level.ERROR,
                            LabelCode = Label.PAY0204
                        });
                        scope.Dispose();
                        return response;
                    }

                    // update payroll profile
                    if (await _payrollProfileRepository.Update(payrollProfileId, request.Data) == null)
                    {
                        response.Message.Toast.Add(new MessageItem()
                        {
                            Level = Level.ERROR,
                            LabelCode = Label.PAY0205
                        });
                        scope.Dispose();
                        return response;
                    }

                    List<PayrollProfileItem> payrollProfileItems = await _payrollProfileRepository.GetPayrollProfileItem(payrollProfileId);
                    if (request.Data.PayrollItems == null)
                    {
                        request.Data.PayrollItems = new List<int>();
                    }
                    // add payroll profile item
                    foreach (var item in request.Data.PayrollItems.Where(n => !payrollProfileItems.Any(m => m.PayrollItemId == n)))
                    {
                        if (!await _payrollProfileRepository.CheckExistMasterPayrollItem(item))
                        {
                            scope.Dispose();
                            response.Message.Toast.Add(new MessageItem()
                            {
                                Level = Level.ERROR,
                                LabelCode = Label.PAY0210
                            });
                            return response;
                        }

                        if (!await _payrollProfileRepository.StorePayrollProfileItem(payrollProfileId, item, request.Data.ModifiedBy))
                        {
                            response.Message.Toast.Add(new MessageItem()
                            {
                                Level = Level.ERROR,
                                LabelCode = Label.PAY0202
                            });
                            scope.Dispose();
                            return response;
                        }
                    }

                    // delete payroll profile item
                    foreach (var item in payrollProfileItems.Where(n => !request.Data.PayrollItems.Contains(n.PayrollItemId)))
                    {
                        if (!await _payrollProfileRepository.DeletePayrollProfileItem(payrollProfileId, item.PayrollItemId, request.Data.ModifiedBy))
                        {
                            response.Message.Toast.Add(new MessageItem()
                            {
                                Level = Level.ERROR,
                                LabelCode = Label.PAY0206
                            });
                            scope.Dispose();
                            return response;
                        }
                    }

                    response.Data = payrollProfileId;
                    response.Message.Toast.Add(new MessageItem()
                    {
                        Level = Level.SUCCESS,
                        LabelCode = Label.PAY0207
                    });
                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                };
            }
            return response;
        }

        public async Task<ApiResponse<PayrollProfileAddModel>> GetPayrollItem(ApiRequest request, string token)
        {
            ApiResponse<PayrollProfileAddModel> response = new ApiResponse<PayrollProfileAddModel>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new PayrollProfileAddModel()
            };

            var genericCodes = await _genericCodeRepository.GetGenericCodes(new List<string>() { Common.Constants.GenericCode.PAYROLL_ITEM_FLAG }, token, request.Parameter);
            List<GenericCodeResult> flags = new List<GenericCodeResult>();
            if (genericCodes.ContainsKey(Common.Constants.GenericCode.PAYROLL_ITEM_FLAG.ToLower()))
            {
                flags = genericCodes[Common.Constants.GenericCode.PAYROLL_ITEM_FLAG.ToLower()];
            }
            int intCodeValue = 0;
            response.Data.Types = flags.Where(n => int.TryParse(n.CodeValue, out intCodeValue)).Select(n => new PayrollItemFlag() { TypeId = int.Parse(n.CodeValue), DisplayName = GetPayrollItemFlagName(n.TextValue) }).ToList();
            response.Data.Items = await CreatePayrollProfileItems(0, request.Parameter.ClientId, request.Parameter.EntityId, response.Data.Types);

            return response;
        }

        /// <summary>
        /// Get payroll profile by id
        /// </summary>
        /// <param name="payrollProfileId"></param>
        /// <param name="clientId"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<PayrollProfileEditModel>> GetPayrollProfileById(int payrollProfileId, ApiRequest request, string token)
        {
            ApiResponse<PayrollProfileEditModel> response = new ApiResponse<PayrollProfileEditModel>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new PayrollProfileEditModel()
            };

            var genericCodes = await _genericCodeRepository.GetGenericCodes(new List<string>() { Common.Constants.GenericCode.PAYROLL_ITEM_FLAG }, token, request.Parameter);
            List<GenericCodeResult> flags = new List<GenericCodeResult>();
            if (genericCodes.ContainsKey(Common.Constants.GenericCode.PAYROLL_ITEM_FLAG.ToLower()))
            {
                flags = genericCodes[Common.Constants.GenericCode.PAYROLL_ITEM_FLAG.ToLower()];
            }

            int intCodeValue = 0;
            response.Data.Types = flags.Where(n => int.TryParse(n.CodeValue, out intCodeValue)).Select(n => new PayrollItemFlag() { TypeId = int.Parse(n.CodeValue), DisplayName = GetPayrollItemFlagName(n.TextValue) }).ToList();
            response.Data.Items = await CreatePayrollProfileItems(payrollProfileId, request.Parameter.ClientId, request.Parameter.EntityId, response.Data.Types);
            response.Data.Detail = await _payrollProfileRepository.GetById(payrollProfileId);


            return response;
        }

        /// <summary>
        /// Create payroll item display
        /// </summary>
        /// <param name="payrollProfileId"></param>
        /// <param name="clientId"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public async Task<List<PayrollProfileViewItemModel>> CreatePayrollProfileItems(int payrollProfileId, int clientId, int entityId, List<PayrollItemFlag> payrollItemFlagsTypes)
        {
            List<PayrollProfileViewItemModel> payrollItems = await _payrollProfileRepository.GetPayrollItem(payrollProfileId, clientId, entityId);
            foreach (var item in payrollItems)
            {
                item.PayrollItemFlags = new Dictionary<string, bool>();
                if (await _payrollProfileRepository.CountPayrollItemTaxByPayrollItemId(item.PayrollItemId) > 0)
                {
                    item.TaxableFlg = true;
                }

                List<TbPayMasterPayrollItemFlag> payrollItemFlags = await _payrollProfileRepository.GetPayrollItemFlagsByPayrollItemId(item.PayrollItemId);
                foreach (var itemFlg in payrollItemFlagsTypes)
                {
                    if (!item.PayrollItemFlags.ContainsKey(itemFlg.TypeId.ToString()))
                    {
                        item.PayrollItemFlags.Add(itemFlg.TypeId.ToString(), false);
                    }
                    if (payrollItemFlags.Any(n => n.FlagId == itemFlg.TypeId))
                    {
                        item.PayrollItemFlags[itemFlg.TypeId.ToString()] = true;
                    }
                }
            }

            return payrollItems;
        }

        /// <summary>
        /// Get payroll profile item configuration
        /// </summary>
        /// <param name="payrollProfileId"></param>
        /// <param name="payrollItemId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<MasterPayrollItemConfiguration>> GetPayrollProfileItemConfiguration(int payrollProfileId, int payrollItemId, ApiRequest request, string token)
        {
            ApiResponse<MasterPayrollItemConfiguration> response = new ApiResponse<MasterPayrollItemConfiguration>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new MasterPayrollItemConfiguration()
            };

            response.Data.Detail = await _payrollProfileRepository.GetPayrollProfileItemConfiguration(payrollProfileId, payrollItemId);
            var genericCodes = await _genericCodeRepository.GetGenericCodes(new List<string>() { Common.Constants.GenericCode.DISPLAY_MODEL }, token, request.Parameter);
            List<GenericCodeResult> flags = new List<GenericCodeResult>();
            if (genericCodes.ContainsKey(Common.Constants.GenericCode.DISPLAY_MODEL.ToLower()))
            {
                flags = genericCodes[Common.Constants.GenericCode.DISPLAY_MODEL.ToLower()];
            }

            int intCodeValue = 0;
            response.Data.Flags = flags.Where(n => int.TryParse(n.CodeValue, out intCodeValue)).Select(n => new PayrollItemFlag() { TypeId = int.Parse(n.CodeValue), DisplayName = GetPayrollItemFlagName(n.TextValue) }).ToList();

            return response;
        }

        /// <summary>
        /// Update payroll profile item configuration
        /// </summary>
        /// <param name="payrollProfileItemId"></param>
        /// <param name="strModifiedBy"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<bool>> UpdatePayrollProfileItemConfiguration(int payrollProfileId, int payrollItemId, string strModifiedBy, ApiRequest<PayrollProfileItem> request)
        {
            ApiResponse<bool> response = new ApiResponse<bool>
            {
                Ray = request.Ray ?? string.Empty,
                Data = false
            };
            // update payroll profile
            request.Data.PayrollProfileId = payrollProfileId;
            request.Data.PayrollItemId = payrollItemId;
            if (!await _payrollProfileRepository.UpdatePayrollProfileItem(request.Data, strModifiedBy))
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = Level.ERROR,
                    LabelCode = Label.PAY0208
                });
                return response;
            }

            response.Data = true;
            response.Message.Toast.Add(new MessageItem()
            {
                Level = Level.SUCCESS,
                LabelCode = Label.PAY0209
            });
            return response;
        }

        /// <summary>
        /// duplicate payroll profile
        /// </summary>
        /// <param name="payrollProfileId"></param>
        /// <param name="strCreatedBy"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<bool>> Duplicate(int payrollProfileId, string strCreatedBy, ApiRequest<PayrollProfileDuplicateModel> request)
        {
            ApiResponse<bool> response = new ApiResponse<bool>
            {
                Ray = request.Ray ?? string.Empty,
                Data = false
            };

            var objPayrollProfile = await _payrollProfileRepository.GetById(payrollProfileId);
            if (objPayrollProfile == null)
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = Level.ERROR,
                    LabelCode = Label.PAY0204
                });
                return response;
            }

            await _payrollProfileRepository.Duplicate(payrollProfileId, strCreatedBy, request.Data.PayrollProfileName);

            response.Data = true;
            response.Message.Toast.Add(new MessageItem()
            {
                Level = Level.SUCCESS,
                LabelCode = Label.PAY0212
            });
            return response;
        }

        public string GetPayrollItemFlagName(string flagName)
        {
            if (string.IsNullOrEmpty(flagName))
                return string.Empty;

            var arrFlagName = flagName.Split(":").ToList();
            if (arrFlagName.Count < 2)
                return flagName;

            return flagName.Substring(arrFlagName.ElementAt(0).Length + 1, flagName.Length - arrFlagName.ElementAt(0).Length - 1);
        }
    }
}
