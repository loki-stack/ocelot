﻿using CommonLib.Dtos;
using CommonLib.Repositories.Interfaces;
using PayrollServiceAPIs.Common.Constants;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.Common.Repositories.Interfaces;
using PayrollServiceAPIs.Common.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.Common.Services.Impl
{
    public class GlCodeService : IGlCodeService
    {
        private readonly IGlCodeRepository _glCodeRepository;
        private readonly IGenericCodeRepository _genericCodeRepository;
        public GlCodeService(IGlCodeRepository glCodeRepository, IGenericCodeRepository genericCodeRepository)
        {
            _glCodeRepository = glCodeRepository;
            _genericCodeRepository = genericCodeRepository;
        }

        /// <summary>
        /// Get All GL code
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ApiResponse<List<GlCodeModel>>> GetAll(ApiRequest<FilterRequest> request, string token)
        {
            ApiResponse<List<GlCodeModel>> response = new ApiResponse<List<GlCodeModel>>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new List<GlCodeModel>()
            };

            if (request.Data == null)
            {
                request.Data = new FilterRequest();
            }

            response.Data = await _glCodeRepository.GetAll(request.Data);
            List<GenericCodeResult> currencies = new List<GenericCodeResult>();
            var genericCodes = await _genericCodeRepository.GetGenericCodes(new List<string>() { GenericCode.CURRENCY }, token, request.Parameter);
            if (genericCodes.ContainsKey(GenericCode.CURRENCY.ToLower()))
            {
                currencies = genericCodes[GenericCode.CURRENCY.ToLower()];
            }

            foreach (var item in response.Data)
            {
                if (currencies.Any(n => n.CodeValue == item.Currency))
                {
                    item.CurrencyText = currencies.First(n => n.CodeValue == item.Currency).TextValue;
                }
            }

            return response;
        }

        /// <summary>
        /// Get GL Edit By Id
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ApiResponse<GlCodeCreateModel>> Create(ApiRequest request, string token)
        {
            ApiResponse<GlCodeCreateModel> response = new ApiResponse<GlCodeCreateModel>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new GlCodeCreateModel()
            };

            var genericCodes = await _genericCodeRepository.GetGenericCodes(new List<string>() { GenericCode.CURRENCY }, token, request.Parameter);
            if (genericCodes.ContainsKey(GenericCode.CURRENCY.ToLower()))
            {
                response.Data.Currency = genericCodes[GenericCode.CURRENCY.ToLower()];
            }

            return response;
        }

        /// <summary>
        /// Store GL Code
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ApiResponse<int>> Store(ApiRequest<GlCodeStoreModel> request, string token)
        {
            ApiResponse<int> response = new ApiResponse<int>
            {
                Ray = request.Ray ?? string.Empty,
                Data = 0
            };

            // validate currency
            List<GenericCodeResult> currencies = new List<GenericCodeResult>();
            var genericCodes = await _genericCodeRepository.GetGenericCodes(new List<string>() { GenericCode.CURRENCY }, token, request.Parameter);
            if (genericCodes.ContainsKey(GenericCode.CURRENCY.ToLower()))
            {
                currencies = genericCodes[GenericCode.CURRENCY.ToLower()];
            }

            if (!currencies.Any(n => n.CodeValue == request.Data.Currency))
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = Constants.Level.ERROR,
                    LabelCode = Constants.Label.GLC0102
                });
            }

            int? newId = await _glCodeRepository.Store(request.Data);
            if (!newId.HasValue)
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = Constants.Level.ERROR,
                    LabelCode = Constants.Label.GLC0102
                });
            }
            else
            {
                response.Data = newId.Value;
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = Constants.Level.SUCCESS,
                    LabelCode = Constants.Label.GLC0103
                });
            }
            return response;
        }

        /// <summary>
        /// Get GL Edit By Id
        /// </summary>
        /// <param name="glCodeId"></param>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ApiResponse<GlCodeEditModel>> Edit(int glCodeId, ApiRequest request, string token)
        {
            ApiResponse<GlCodeEditModel> response = new ApiResponse<GlCodeEditModel>
            {
                Ray = request.Ray ?? string.Empty,
                Data = new GlCodeEditModel()
            };

            response.Data.Detail = await _glCodeRepository.GetById(glCodeId);

            var genericCodes = await _genericCodeRepository.GetGenericCodes(new List<string>() { GenericCode.CURRENCY }, token, request.Parameter);
            if (genericCodes.ContainsKey(GenericCode.CURRENCY.ToLower()))
            {
                response.Data.Currency = genericCodes[GenericCode.CURRENCY.ToLower()];
            }

            return response;
        }

        /// <summary>
        /// Update GL Code
        /// </summary>
        /// <param name="glCodeId"></param>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ApiResponse<int>> Update(int glCodeId, ApiRequest<GlCodeUpdateModel> request, string token)
        {
            ApiResponse<int> response = new ApiResponse<int>
            {
                Ray = request.Ray ?? string.Empty,
                Data = 0
            };
            var objGLCode = await _glCodeRepository.GetById(glCodeId);
            if (objGLCode == null)
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = Constants.Level.ERROR,
                    LabelCode = Constants.Label.GLC0104
                });
                return response;
            }

            if (request.Data.Currency != objGLCode.Currency)
            {
                // validate currency
                List<GenericCodeResult> currencies = new List<GenericCodeResult>();
                var genericCodes = await _genericCodeRepository.GetGenericCodes(new List<string>() { GenericCode.CURRENCY }, token, request.Parameter);
                if (genericCodes.ContainsKey(GenericCode.CURRENCY.ToLower()))
                {
                    currencies = genericCodes[GenericCode.CURRENCY.ToLower()];
                }

                if (!currencies.Any(n => n.CodeValue == request.Data.Currency))
                {
                    response.Message.Toast.Add(new MessageItem()
                    {
                        Level = Constants.Level.ERROR,
                        LabelCode = Constants.Label.GLC0102
                    });
                }
            }

            // update GL Code
            if (await _glCodeRepository.Update(glCodeId, request.Data) == null)
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = Constants.Level.ERROR,
                    LabelCode = Constants.Label.GLC0105
                });
                return response;
            }

            response.Data = glCodeId;
            response.Message.Toast.Add(new MessageItem()
            {
                Level = Constants.Level.SUCCESS,
                LabelCode = Constants.Label.GLC0106
            });
            return response;
        }
    }
}
