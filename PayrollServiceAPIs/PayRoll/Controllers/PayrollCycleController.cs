﻿using CommonLib.Dtos;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PayrollServiceAPIs.Common.Constants;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos.PayrollCycle;
using PayrollServiceAPIs.PayRoll.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Controllers
{
    [Authorize]
    [Route("api/PAY/[action]/[controller]")]
    [ApiController]
    public class PayrollCycleController : ControllerBase
    {
        private readonly IPayrollCycleService _payrollCycleService;
        public PayrollCycleController(IPayrollCycleService payrollCycleService)
        {
            _payrollCycleService = payrollCycleService;
        }

        [HttpGet()]
        [ActionName("PAY0801")]
        [Authorize(Roles = "PAY0801")]
        public async Task<ActionResult<ApiResponse<List<PayrollCycleViewModel>>>> GetAll([FromQuery] ApiRequest<FilterRequest> request)
        {
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);
            var response = await _payrollCycleService.GetAll(request, token);
            return Ok(response);
        }

        [HttpGet("Edit/{payrollCycleId}")]
        [ActionName("PAY0801")]
        [Authorize(Roles = "PAY0801")]
        public async Task<ActionResult<ApiResponse<PayrollCycleViewModel>>> Edit(int payrollCycleId, [FromQuery] ApiRequest request)
        {
            var response = await _payrollCycleService.Edit(payrollCycleId, request);
            return Ok(response);
        }

        [HttpPost()]
        [ActionName("PAY0802")]
        [Authorize(Roles = "PAY0802")]
        public async Task<ActionResult<ApiResponse<int>>> Store([FromBody] ApiRequest<int> request)
        {
            string createdBy = User.FindFirst(ClaimTypes.Name).Value;
            var response = await _payrollCycleService.Store(request, createdBy);
            return Ok(response);
        }

        [HttpPut("{payrollCycleId}")]
        [ActionName("PAY0803")]
        [Authorize(Roles = "PAY0803")]
        public async Task<ActionResult<ApiResponse<int>>> Update(int payrollCycleId, [FromBody] ApiRequest<PayrollCycleStoreModel> request)
        {
            var resultCheck = new CommonLib.Validation.Validator(null).Validate(request.Data);
            if (resultCheck.Any())
            {
                return StatusCode(StatusCodes.Status400BadRequest, resultCheck);
            }

            request.Data.ModifiedBy = User.FindFirst(ClaimTypes.Name).Value;
            var response = await _payrollCycleService.Update(payrollCycleId, request);
            return Ok(response);
        }

        [HttpDelete("Delete")]
        [ActionName("PAY0804")]
        [Authorize(Roles = "PAY0804")]
        public async Task<ActionResult<ApiResponse<int>>> Delete([FromBody] ApiRequest<List<int>> request)
        {

            string modifiedBy = User.FindFirst(ClaimTypes.Name).Value;
            var response = await _payrollCycleService.Delete(request, modifiedBy);
            return Ok(response);
        }
    }
}
