﻿using CommonLib.Dtos;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PayrollServiceAPIs.Common.Constants;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos.PayrollRun;
using PayrollServiceAPIs.PayRoll.Dtos.PayrollRun.Export;
using PayrollServiceAPIs.PayRoll.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Controllers
{
    [Authorize]
    [Route("api/PAY/[action]/[controller]")]
    [ApiController]
    public class PayrollRunController : ControllerBase
    {
        private readonly IPayrollRunService _payrollRunService;
        public PayrollRunController(IPayrollRunService payrollRunService)
        {
            _payrollRunService = payrollRunService;
        }

        [HttpGet()]
        [ActionName("PAY0401")]
        [Authorize(Roles = "PAY0401")]
        public async Task<ActionResult<ApiResponse<List<PayrollRunViewModel>>>> GetAll([FromQuery] ApiRequest<FilterRequest> request)
        {
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);

            var response = await _payrollRunService.GetAll(request, token);
            return Ok(response);
        }

        [HttpGet("Create")]
        [ActionName("PAY0401")]
        [Authorize(Roles = "PAY0401")]
        public async Task<ActionResult<ApiResponse<PayrollRunCreateModel>>> Create([FromQuery] ApiRequest request)
        {
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);
            var response = await _payrollRunService.Create(request, token);
            return Ok(response);
        }

        [HttpGet("Edit/{payrollRunId}")]
        [ActionName("PAY0401")]
        [Authorize(Roles = "PAY0401")]
        public async Task<ActionResult<ApiResponse<PayrollRunEditModel>>> Edit(int payrollRunId, [FromQuery] ApiRequest request)
        {
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);
            var response = await _payrollRunService.Edit(payrollRunId, request, token);
            return Ok(response);
        }

        [HttpPost()]
        [ActionName("PAY0402")]
        [Authorize(Roles = "PAY0402")]
        public async Task<ActionResult<ApiResponse<int>>> Store([FromBody] ApiRequest<PayrollRunStoreModel> request)
        {
            var resultCheck = new CommonLib.Validation.Validator(null).Validate(request.Data);
            if (resultCheck.Any())
            {
                return StatusCode(StatusCodes.Status400BadRequest, resultCheck);
            }

            request.Data.CreatedBy = User.FindFirst(ClaimTypes.Name).Value;
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);
            var response = await _payrollRunService.Store(request, token);
            return Ok(response);
        }

        [HttpPut("{payrollRunId}")]
        [ActionName("PAY0403")]
        [Authorize(Roles = "PAY0403")]
        public async Task<ActionResult<ApiResponse<int>>> Update(int payrollRunId, [FromBody] ApiRequest<PayrollRunStoreModel> request)
        {
            var resultCheck = new CommonLib.Validation.Validator(null).Validate(request.Data);
            if (resultCheck.Any())
            {
                return StatusCode(StatusCodes.Status400BadRequest, resultCheck);
            }

            request.Data.ModifiedBy = User.FindFirst(ClaimTypes.Name).Value;
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);
            var response = await _payrollRunService.Update(payrollRunId, request, token);
            return Ok(response);
        }

        [HttpGet("Setting/{payrollRunId}")]
        [ActionName("PAY0401")]
        [Authorize(Roles = "PAY0401")]
        public async Task<ActionResult<ApiResponse<PayrollRunSettingModel>>> Setting(int payrollRunId, [FromQuery] ApiRequest request)
        {
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);
            var response = await _payrollRunService.Setting(payrollRunId, request, token);
            return Ok(response);
        }

        [HttpGet("CreateEmployee")]
        [ActionName("PAY0401")]
        [Authorize(Roles = "PAY0401")]
        public async Task<ActionResult<ApiResponse<List<PayrollRunEmployeeViewModel>>>> CreateEmployee([FromQuery] ApiRequest request)
        {
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);
            var response = await _payrollRunService.GetEmployeeSelectInPayroll(request, token);
            return Ok(response);
        }

        [HttpPost("{payrollRunId}/StoreEmployee")]
        [ActionName("PAY0403")]
        [Authorize(Roles = "PAY0403")]
        public async Task<ActionResult<ApiResponse<bool>>> StoreEmployee(int payrollRunId, ApiRequest<PayrollRunEmployeeStoreModel> request)
        {
            request.Data.CreatedBy = User.FindFirst(ClaimTypes.Name).Value;
            var response = await _payrollRunService.EmployeeStore(payrollRunId, request);
            return Ok(response);
        }

        [HttpGet("CreateTransaction")]
        [ActionName("PAY0401")]
        [Authorize(Roles = "PAY0401")]
        public async Task<ActionResult<ApiResponse<PayrollRunTranssationCreateModel>>> CreateTransaction([FromQuery] ApiRequest<List<int>> request)
        {
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);
            var response = await _payrollRunService.TransactionCreate(request, token);
            return Ok(response);
        }

        [HttpPost("{payrollRunId}/StoreTransaction")]
        [ActionName("PAY0403")]
        [Authorize(Roles = "PAY0403")]
        public async Task<ActionResult<ApiResponse<bool>>> StoreTransaction(int payrollRunId, ApiRequest<PayrollRunTransactionStoreModel> request)
        {
            request.Data.CreatedBy = User.FindFirst(ClaimTypes.Name).Value;
            var response = await _payrollRunService.TransactionStore(payrollRunId, request);
            return Ok(response);
        }

        [HttpDelete("{payrollRunId}/DeleteTransaction")]
        [ActionName("PAY0404")]
        [Authorize(Roles = "PAY0404")]
        public async Task<ActionResult<ApiResponse<bool>>> DeleteTransaction(int payrollRunId, ApiRequest<PayrollRunTransactionDeleteModel> request)
        {
            request.Data.ModifiedBy = User.FindFirst(ClaimTypes.Name).Value;
            var response = await _payrollRunService.TransactionDelete(request);
            return Ok(response);
        }

        [HttpPost("{payrollRunId}/Duplicate")]
        [ActionName("PAY0405")]
        [Authorize(Roles = "PAY0405")]
        public async Task<ActionResult<ApiResponse<bool>>> Duplicate(int payrollRunId, [FromBody] ApiRequest<PayrollRunStoreModel> request)
        {
            var resultCheck = new CommonLib.Validation.Validator(null).Validate(request.Data);
            if (resultCheck.Any())
            {
                return StatusCode(StatusCodes.Status400BadRequest, resultCheck);
            }

            request.Data.CreatedBy = User.FindFirst(ClaimTypes.Name).Value;
            var response = await _payrollRunService.Duplicate(payrollRunId, request);
            return Ok(response);
        }

        [HttpGet("{payrollRunId}/Transaction")]
        [ActionName("PAY0401")]
        [Authorize(Roles = "PAY0401")]
        public async Task<ActionResult<ApiResponse<bool>>> GetTransactionByEmployee(int payrollRunId, [FromQuery] ApiRequest<List<int>> request)
        {
            var response = await _payrollRunService.GetTransactionByEmployee(payrollRunId, request);
            return Ok(response);
        }

        [HttpGet("{payrollRunId}/Export")]
        [ActionName("PAY0401")]
        [Authorize(Roles = "PAY0401")]
        public async Task<ActionResult<ApiResponse<bool>>> Export(int payrollRunId, [FromQuery] ApiRequest<PayrollRunExportModel> request)
        {
            string exportBy = User.FindFirst(ClaimTypes.Name).Value;
            var response = await _payrollRunService.Export(payrollRunId, exportBy, request);

            return File(new System.IO.FileStream(response.Data, System.IO.FileMode.Open), "application/octet-stream", "PayrollRunTransaction.xlsx");
        }

        [HttpPost("{payrollRunId}/Import")]
        [ActionName("PAY0402")]
        [Authorize(Roles = "PAY0402")]
        public async Task<ActionResult<ApiResponse<PayrollRunImportResult>>> Import(int payrollRunId, [FromForm] ApiRequest<int> request, IFormFile file)
        {
            string userName = User.FindFirst(ClaimTypes.Name).Value;
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);
            var response = await _payrollRunService.Import(payrollRunId, userName, token, request, file);
            return Ok(response);
        }

        [HttpPost("{payrollRunId}/Import/{payrollRumImportId}")]
        [ActionName("PAY0402")]
        [Authorize(Roles = "PAY0402")]
        public async Task<ActionResult<ApiResponse<PayrollRunImportResult>>> ImportConfirm(int payrollRunId, int payrollRumImportId, ApiRequest<PayrollRunImportConfirmModel> request)
        {
            string userName = User.FindFirst(ClaimTypes.Name).Value;
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);
            var response = await _payrollRunService.ImportConfirm(payrollRunId, payrollRumImportId, userName, token, request);
            return Ok(response);
        }
    }
}
