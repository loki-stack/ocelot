﻿using CommonLib.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PayrollServiceAPIs.PayRoll.Dtos;
using PayrollServiceAPIs.PayRoll.Services.Interfaces;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Controllers
{
    [Authorize]
    [Route("api/PAY/[action]/[controller]")]
    [ApiController]
    public class MPFRegisteredSchemeController : ControllerBase
    {
        private readonly IRegisteredSchemeService _registeredSchemeService;
        private readonly ILogger<MPFRegisteredSchemeController> _logger;
        public MPFRegisteredSchemeController(IRegisteredSchemeService registeredSchemeService, ILogger<MPFRegisteredSchemeController> logger)
        {
            _registeredSchemeService = registeredSchemeService;
            _logger = logger;
        }

        [ActionName("PAY0501")]
        [HttpGet()]
        [Authorize(Roles = "PAY0501")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAll([FromQuery] ApiRequest request)
        {
            try
            {
                var response = await _registeredSchemeService.GetAll(request);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [ActionName("PAY0501")]
        [HttpGet("{mpfRegisteredSchemeId}")]
        [Authorize(Roles = "PAY0501")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetById(int mpfRegisteredSchemeId, [FromQuery] ApiRequest request)
        {
            try
            {
                var response = await _registeredSchemeService.GetById(mpfRegisteredSchemeId, request);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [ActionName("PAY0503")]
        [HttpPut("{mpfRegisteredSchemeId}")]
        [Authorize(Roles = "PAY0503")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Update(int mpfRegisteredSchemeId, [FromBody] ApiRequest<MPFRegisteredSchemeRequest> request)
        {
            try
            {
                string modifiedBy = User.FindFirst(ClaimTypes.Name).Value;
                var response = await _registeredSchemeService.Update(mpfRegisteredSchemeId, request, modifiedBy);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [ActionName("PAY0504")]
        [HttpDelete("{registeredSchemeId}")]
        [Authorize(Roles = "PAY0504")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Delete(int registeredSchemeId, [FromBody] ApiRequest request)
        {
            try
            {
                var response = await _registeredSchemeService.Delete(registeredSchemeId, request);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
