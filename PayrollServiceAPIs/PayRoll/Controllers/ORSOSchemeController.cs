﻿using CommonLib.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos.ORSOScheme;
using PayrollServiceAPIs.PayRoll.Services.Interfaces;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Controllers
{
    [Authorize]
    [Route("api/PAY/[action]/[controller]")]
    [ApiController]
    public class ORSOSchemeController : ControllerBase
    {
        private readonly IORSOSchemeService _orsoSchemeService;
        private readonly ILogger<ORSOSchemeController> _logger;

        public ORSOSchemeController(IORSOSchemeService orsoSchemeService, ILogger<ORSOSchemeController> logger)
        {
            _orsoSchemeService = orsoSchemeService;
            _logger = logger;
        }

        [ActionName("PAY0701")]
        [HttpGet()]
        [Authorize(Roles = "PAY0701")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAll([FromQuery] ApiRequest<PaginationRequest> request)
        {
            try
            {
                if (request.Data == null) request.Data = new PaginationRequest();
                var response = await _orsoSchemeService.GetAll(request);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [ActionName("PAY0701")]
        [HttpGet("{orsoSchemeId}")]
        [Authorize(Roles = "PAY0701")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetById(int orsoSchemeId, [FromQuery] ApiRequest request)
        {
            try
            {
                var response = await _orsoSchemeService.GetById(orsoSchemeId, request);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [ActionName("PAY0702")]
        [HttpPost]
        [Authorize(Roles = "PAY0702")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Create([FromBody] ApiRequest<ORSOSchemeRequest> request)
        {
            try
            {
                string createdBy = User.FindFirst(ClaimTypes.Name).Value;
                var response = await _orsoSchemeService.Create(request, createdBy);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [ActionName("PAY0703")]
        [HttpPut("{orsoSchemeId}")]
        [Authorize(Roles = "PAY0703")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Update(int orsoSchemeId, [FromBody] ApiRequest<ORSOSchemeRequest> request)
        {
            try
            {
                string modifiedBy = User.FindFirst(ClaimTypes.Name).Value;
                var response = await _orsoSchemeService.Update(orsoSchemeId, request, modifiedBy);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
