﻿using CommonLib.Dtos;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PayrollServiceAPIs.Common.Constants;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos.ExchangeRate;
using PayrollServiceAPIs.PayRoll.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Controllers
{
    [Route("api/PAY/[action]/[controller]")]
    [ApiController]
    [Authorize]
    public class ExchangeRateController : ControllerBase
    {
        private readonly IExchangeRateService _exchangeRateService;
        public ExchangeRateController(IExchangeRateService exchangeRateService)
        {
            _exchangeRateService = exchangeRateService;
        }

        [HttpGet()]
        [ActionName("PAY0501")]
        [Authorize(Roles = "PAY0501")]
        public async Task<ActionResult<ApiResponse<List<ExchangeRateViewModel>>>> GetAll([FromQuery] ApiRequest<FilterRequest> request)
        {
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);

            var response = await _exchangeRateService.GetAll(request, token);
            return Ok(response);
        }

        [HttpGet("Create")]
        [ActionName("PAY0501")]
        [Authorize(Roles = "PAY0501")]
        public async Task<ActionResult<ApiResponse<ExchangeRateStoreModel>>> Create([FromQuery] ApiRequest request)
        {
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);
            var response = await _exchangeRateService.Create(request, token);
            return Ok(response);
        }

        [HttpGet("Edit/{exchangeRateId}")]
        [ActionName("PAY0501")]
        [Authorize(Roles = "PAY0501")]
        public async Task<ActionResult<ApiResponse<ExchangeRateEditModel>>> Edit(int exchangeRateId, [FromQuery] ApiRequest request)
        {
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);
            var response = await _exchangeRateService.Edit(exchangeRateId, request, token);
            return Ok(response);
        }

        [HttpPost()]
        [ActionName("PAY0502")]
        [Authorize(Roles = "PAY0502")]
        public async Task<ActionResult<ApiResponse<int>>> Store([FromBody] ApiRequest<ExchangeRateStoreModel> request)
        {
            var resultCheck = new CommonLib.Validation.Validator(null).Validate(request.Data);
            if (resultCheck.Any())
            {
                return StatusCode(StatusCodes.Status400BadRequest, resultCheck);
            }

            request.Data.CreatedBy = User.FindFirst(ClaimTypes.Name).Value;
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);
            var response = await _exchangeRateService.Store(request, token);
            return Ok(response);
        }

        [HttpPut("{exchangeRateId}")]
        [ActionName("PAY0503")]
        [Authorize(Roles = "PAY0503")]
        public async Task<ActionResult<ApiResponse<int>>> Update(int exchangeRateId, [FromBody] ApiRequest<ExchangeRateStoreModel> request)
        {
            var resultCheck = new CommonLib.Validation.Validator(null).Validate(request.Data);
            if (resultCheck.Any())
            {
                return StatusCode(StatusCodes.Status400BadRequest, resultCheck);
            }

            request.Data.ModifiedBy = User.FindFirst(ClaimTypes.Name).Value;
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);
            var response = await _exchangeRateService.Update(exchangeRateId, request, token);
            return Ok(response);
        }
    }
}
