﻿using CommonLib.Dtos;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PayrollServiceAPIs.Common.Constants;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.Common.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.Common.Controllers
{
    [Authorize]
    [Route("api/PAY/[action]/[controller]")]
    [ApiController]
    public class GLCodeController : ControllerBase
    {
        private readonly IGlCodeService _glCodeService;

        public GLCodeController(IGlCodeService glCodeService)
        {
            _glCodeService = glCodeService;
        }

        [HttpGet()]
        [ActionName("PAY0801")]
        public async Task<ActionResult<ApiResponse<List<GlCodeModel>>>> GetAll([FromQuery] ApiRequest<FilterRequest> request)
        {
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);

            var response = await _glCodeService.GetAll(request, token);
            return Ok(response);
        }

        [HttpGet()]
        [ActionName("PAY0802")]
        public async Task<ActionResult<ApiResponse<GlCodeEditModel>>> Create([FromQuery] ApiRequest request)
        {
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);
            var response = await _glCodeService.Create(request, token);
            return Ok(response);
        }

        [HttpPost()]
        [ActionName("PAY0802")]
        public async Task<ActionResult<ApiResponse<int>>> Store([FromBody] ApiRequest<GlCodeStoreModel> request)
        {
            var resultCheck = new CommonLib.Validation.Validator(null).Validate(request.Data);
            if (resultCheck.Any())
            {
                return StatusCode(StatusCodes.Status400BadRequest, resultCheck);
            }
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);
            request.Data.CreatedBy = User.FindFirst(ClaimTypes.Name).Value;
            var response = await _glCodeService.Store(request, token);
            return Ok(response);
        }

        [HttpGet("{glCodeId}")]
        [ActionName("PAY0803")]
        public async Task<ActionResult<ApiResponse<GlCodeEditModel>>> Edit(int glCodeId, [FromQuery] ApiRequest request)
        {
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);
            var response = await _glCodeService.Edit(glCodeId, request, token);
            return Ok(response);
        }

        [HttpPut("{glCodeId}")]
        [ActionName("PAY0803")]
        public async Task<ActionResult<ApiResponse<int>>> Update(int glCodeId, [FromBody] ApiRequest<GlCodeUpdateModel> request)
        {
            var resultCheck = new CommonLib.Validation.Validator(null).Validate(request.Data);
            if (resultCheck.Any())
            {
                return StatusCode(StatusCodes.Status400BadRequest, resultCheck);
            }
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);
            request.Data.ModifiedBy = User.FindFirst(ClaimTypes.Name).Value;
            var response = await _glCodeService.Update(glCodeId, request, token);
            return Ok(response);
        }
    }
}
