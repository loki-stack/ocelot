﻿using CommonLib.Dtos;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PayrollServiceAPIs.Common.Constants;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos;
using PayrollServiceAPIs.PayRoll.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Controllers
{
    [Authorize]
    [Route("api/PAY/[action]/[controller]")]
    [ApiController]
    public class MasterPayrollItemController : ControllerBase
    {
        private readonly IMasterPayRollItemService _masterPayrollItemService;
        public MasterPayrollItemController(IMasterPayRollItemService masterPayrollItemService)
        {
            _masterPayrollItemService = masterPayrollItemService;
        }

        [HttpGet()]
        [ActionName("PAY0101")]
        [Authorize(Roles = "PAY0101")]
        public async Task<ActionResult<ApiResponse<MasterPayrollItemViewAll>>> GetAll([FromQuery] ApiRequest<FilterRequest> request)
        {
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);
            var response = await _masterPayrollItemService.GetAll(request, token);
            return Ok(response);
        }

        [HttpGet("Create")]
        [ActionName("PAY0101")]
        [Authorize(Roles = "PAY0101")]
        public async Task<ActionResult<ApiResponse<MasterPayrollItemAddModel>>> Create([FromQuery] ApiRequest request)
        {
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);
            var response = await _masterPayrollItemService.GetPayrollItem(request, token);
            return Ok(response);
        }

        [HttpPost()]
        [ActionName("PAY0102")]
        [Authorize(Roles = "PAY0102")]
        public async Task<ActionResult<ApiResponse<int>>> Store([FromBody] ApiRequest<MasterPayrollItemStoreModel> request)
        {
            request.Data.CreatedBy = User.FindFirst(ClaimTypes.Name).Value;
            var response = await _masterPayrollItemService.Store(request);
            return Ok(response);
        }

        [HttpGet("Edit/{masterPayrollItemId}")]
        [ActionName("PAY0101")]
        [Authorize(Roles = "PAY0101")]
        public async Task<ActionResult<ApiResponse<MasterPayrollItemEditModel>>> Edit(int masterPayrollItemId, [FromQuery] ApiRequest request)
        {
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);
            var response = await _masterPayrollItemService.GetMasterPayrollItemById(masterPayrollItemId, request, token);
            return Ok(response);
        }

        [HttpPut("{masterPayrollItemId}")]
        [ActionName("PAY0103")]
        [Authorize(Roles = "PAY0103")]
        public async Task<ActionResult<ApiResponse<int>>> Update(int masterPayrollItemId, [FromBody] ApiRequest<MasterPayrollItemStoreModel> request)
        {
            var resultCheck = new CommonLib.Validation.Validator(null).Validate(request.Data);
            if (resultCheck.Any())
            {
                return StatusCode(StatusCodes.Status400BadRequest, resultCheck);
            }
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);
            request.Data.ModifiedBy = User.FindFirst(ClaimTypes.Name).Value;


            var response = await _masterPayrollItemService.Update(masterPayrollItemId, request, token);
            return Ok(response);
        }

        [HttpGet("MasterPayrollItemFlag")]
        [ActionName("PAY0101")]
        [Authorize(Roles = "PAY0101")]
        public async Task<ActionResult<ApiResponse<MasterPayrollItemViewAll>>> GetMasterPayrollItemByFlag([FromQuery] ApiRequest<List<int>> request)
        {
            var response = await _masterPayrollItemService.GetMasterPayrollItemByFlag(request);
            return Ok(response);
        }

        [HttpPost("{masterPayrollItemId}")]
        [ActionName("PAY0105")]
        [Authorize(Roles = "PAY0105")]
        public async Task<ActionResult<ApiResponse<bool>>> Duplicate(int masterPayrollItemId, [FromBody] ApiRequest<MasterPayrollItemDuplicateModel> request)
        {
            var resultCheck = new CommonLib.Validation.Validator(null).Validate(request.Data);
            if (resultCheck.Any())
            {
                return StatusCode(StatusCodes.Status400BadRequest, resultCheck);
            }

            var strCreateddBy = User.FindFirst(ClaimTypes.Name).Value;
            var response = await _masterPayrollItemService.Duplicate(masterPayrollItemId, strCreateddBy, request);
            return Ok(response);
        }
    }
}
