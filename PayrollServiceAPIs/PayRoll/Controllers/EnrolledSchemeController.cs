﻿using CommonLib.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PayrollServiceAPIs.PayRoll.Dtos.EnrolledScheme;
using PayrollServiceAPIs.PayRoll.Services.Interfaces;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Controllers
{
    /// <summary>
    /// The controller which handles all operations related to pension scheme
    /// </summary>
    [Route("api/PAY/[action]/[controller]")]
    [ApiController]
    [Authorize]
    public class EnrolledSchemeController : ControllerBase
    {
        private readonly IEnrolledSchemeService _enrolledSchemeService;
        private readonly ILogger<EnrolledSchemeController> _logger;
        public EnrolledSchemeController(IEnrolledSchemeService enrolledSchemeService, ILogger<EnrolledSchemeController> logger)
        {
            _enrolledSchemeService = enrolledSchemeService;
            _logger = logger;
        }

        /// <summary>
        /// Retrieve all of pension schemes which are under a specific entity
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [ActionName("PAY0601")]
        [HttpGet()]
        [Authorize(Roles = "PAY0601")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAll([FromQuery] ApiRequest<EnrolledSchemeGetAllRequest> request)
        {
            try
            {
                if (request.Data == null) request.Data = new EnrolledSchemeGetAllRequest();
                var response = await _enrolledSchemeService.GetByRegisteredSchemeId(request.Data.RegisteredSchemeId, request);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Try get all of pension schemes with parameter: " + JsonConvert.SerializeObject(request));
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Retrieve a pension scheme
        /// </summary>
        /// <param name="enrolledSchemeId">Id of the pension scheme which is retrieved</param>
        /// <param name="request"></param>
        /// <returns></returns>
        [ActionName("PAY0601")]
        [HttpGet("{enrolledSchemeId}")]
        [Authorize(Roles = "PAY0601")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetById(int enrolledSchemeId, [FromQuery] ApiRequest request)
        {
            try
            {
                var response = await _enrolledSchemeService.GetById(enrolledSchemeId, request);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [ActionName("PAY0602")]
        [HttpPost]
        [Authorize(Roles = "PAY0602")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Create([FromBody] ApiRequest<EnrolledSchemeRequest> request)
        {
            try
            {
                string createdBy = User.FindFirst(ClaimTypes.Name).Value;
                var response = await _enrolledSchemeService.Create(request, createdBy);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [ActionName("PAY0603")]
        [HttpPut("{enrolledSchemeId}")]
        [Authorize(Roles = "PAY0603")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Update(int enrolledSchemeId, [FromBody] ApiRequest<EnrolledSchemeRequest> request)
        {
            try
            {
                string modifiedBy = User.FindFirst(ClaimTypes.Name).Value;
                var response = await _enrolledSchemeService.Update(enrolledSchemeId, request, modifiedBy);
                if (response.Data == null)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
