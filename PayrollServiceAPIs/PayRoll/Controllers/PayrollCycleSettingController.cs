﻿using CommonLib.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PayrollServiceAPIs.PayRoll.Dtos.PayrollCycleSetting;
using PayrollServiceAPIs.PayRoll.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Controllers
{
    [Authorize]
    [Route("api/PAY/[action]/[controller]")]
    [ApiController]
    public class PayrollCycleSettingController : ControllerBase
    {
        private readonly IPayrollCycleSettingService _payrollCycleSettingService;
        public PayrollCycleSettingController(IPayrollCycleSettingService payrollCycleSettingService)
        {
            _payrollCycleSettingService = payrollCycleSettingService;
        }

        [HttpGet("Edit")]
        [ActionName("PAY0901")]
        [Authorize(Roles = "PAY0901")]
        public async Task<ActionResult<ApiResponse<PayrollCycleSettingEditModel>>> Edit([FromQuery] ApiRequest request)
        {
            var response = await _payrollCycleSettingService.Edit(request);
            return Ok(response);
        }

        [HttpPut("Update")]
        [ActionName("PAY0903")]
        [Authorize(Roles = "PAY0903")]
        public async Task<ActionResult<ApiResponse<bool>>> Update([FromBody] ApiRequest<PayrollCycleSettingUpdateModel> request)
        {
            var resultCheck = new CommonLib.Validation.Validator(null).Validate(request.Data);
            if (resultCheck.Any())
            {
                return StatusCode(StatusCodes.Status400BadRequest, resultCheck);
            }

            var response = await _payrollCycleSettingService.Update(request);
            return Ok(response);
        }
    }
}
