﻿using CommonLib.Dtos;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PayrollServiceAPIs.Common.Constants;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos;
using PayrollServiceAPIs.PayRoll.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.PayRoll.Controllers
{
    [Authorize]
    [Route("api/PAY/[action]/[controller]")]
    [ApiController]
    public class PayrollProfileController : ControllerBase
    {
        private readonly IPayrollProfileService _payrollProfileService;
        public PayrollProfileController(IPayrollProfileService payrollProfileService)
        {
            _payrollProfileService = payrollProfileService;
        }

        [HttpGet()]
        [ActionName("PAY0201")]
        [Authorize(Roles = "PAY0201")]
        public async Task<ActionResult<ApiResponse<List<PayrollProfileViewModel>>>> GetAll([FromQuery] ApiRequest<FilterRequest> request)
        {
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);

            var response = await _payrollProfileService.GetAll(request, token);
            return Ok(response);
        }

        [HttpGet("Create")]
        [ActionName("PAY0201")]
        [Authorize(Roles = "PAY0201")]
        public async Task<ActionResult<ApiResponse<PayrollProfileAddModel>>> Create([FromQuery] ApiRequest request)
        {
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);
            var response = await _payrollProfileService.GetPayrollItem(request, token);
            return Ok(response);
        }

        [HttpGet("Edit/{payrollProfileId}")]
        [ActionName("PAY0201")]
        [Authorize(Roles = "PAY0201")]
        public async Task<ActionResult<ApiResponse<PayrollProfileEditModel>>> Edit(int payrollProfileId, [FromQuery] ApiRequest request)
        {
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);
            var response = await _payrollProfileService.GetPayrollProfileById(payrollProfileId, request, token);
            return Ok(response);
        }

        [HttpPost()]
        [ActionName("PAY0202")]
        [Authorize(Roles = "PAY0202")]
        public async Task<ActionResult<ApiResponse<int>>> Store([FromBody] ApiRequest<PayrollProfileStoreModel> request)
        {
            var resultCheck = new CommonLib.Validation.Validator(null).Validate(request.Data);
            if (resultCheck.Any())
            {
                return StatusCode(StatusCodes.Status400BadRequest, resultCheck);
            }

            request.Data.ClientId = request.Parameter.ClientId;
            request.Data.EntityId = request.Parameter.EntityId;
            request.Data.CreatedBy = User.FindFirst(ClaimTypes.Name).Value;
            var response = await _payrollProfileService.Store(request);
            return Ok(response);
        }

        [HttpPut("{payrollProfileId}")]
        [ActionName("PAY0203")]
        [Authorize(Roles = "PAY0203")]
        public async Task<ActionResult<ApiResponse<int>>> Update(int payrollProfileId, [FromBody] ApiRequest<PayrollProfileStoreModel> request)
        {
            var resultCheck = new CommonLib.Validation.Validator(null).Validate(request.Data);
            if (resultCheck.Any())
            {
                return StatusCode(StatusCodes.Status400BadRequest, resultCheck);
            }

            request.Data.ModifiedBy = User.FindFirst(ClaimTypes.Name).Value;
            var response = await _payrollProfileService.Update(payrollProfileId, request);
            return Ok(response);
        }

        [HttpGet("{payrollProfileId}/masterPayrollItem/{payrollItemId}")]
        [ActionName("PAY0201")]
        [Authorize(Roles = "PAY0201")]
        public async Task<ActionResult<ApiResponse<MasterPayrollItemConfiguration>>> GetPayrollProfileItemConfiguration(int payrollProfileId, int payrollItemId, [FromQuery] ApiRequest request)
        {
            string token = await HttpContext.GetTokenAsync(JWTKey.TOKEN_KEY);
            var response = await _payrollProfileService.GetPayrollProfileItemConfiguration(payrollProfileId, payrollItemId, request, token);
            return Ok(response);
        }

        [HttpPut("{payrollProfileId}/masterPayrollItem/{payrollItemId}")]
        [ActionName("PAY0203")]
        [Authorize(Roles = "PAY0203")]
        public async Task<ActionResult<ApiResponse<int>>> UpdatePayrollProfileItemConfiguration(int payrollProfileId, int payrollItemId, [FromBody] ApiRequest<PayrollProfileItem> request)
        {
            var strModifiedBy = User.FindFirst(ClaimTypes.Name).Value;
            var response = await _payrollProfileService.UpdatePayrollProfileItemConfiguration(payrollProfileId, payrollItemId, strModifiedBy, request);
            return Ok(response);
        }

        [HttpPost("{payrollProfileId}")]
        [ActionName("PAY0205")]
        [Authorize(Roles = "PAY0205")]
        public async Task<ActionResult<ApiResponse<bool>>> Duplicate(int payrollProfileId, [FromBody] ApiRequest<PayrollProfileDuplicateModel> request)
        {
            var resultCheck = new CommonLib.Validation.Validator(null).Validate(request.Data);
            if (resultCheck.Any())
            {
                return StatusCode(StatusCodes.Status400BadRequest, resultCheck);
            }

            var strCreateddBy = User.FindFirst(ClaimTypes.Name).Value;
            var response = await _payrollProfileService.Duplicate(payrollProfileId, strCreateddBy, request);
            return Ok(response);
        }
    }
}
