﻿namespace PayrollServiceAPIs.Common.Constants
{
    public struct Level
    {
        public const string ERROR = "ERROR";
        public const string SUCCESS = "SUCCESS";
        public const string INFO = "INFO";
    }

    public struct Label
    {
        #region Organization Structure
        // Organization Structure Not Exists
        public const string OS0101 = "OS0101";
        // Organization Structure Cannot InActive. Organization Structure have employee
        public const string OS0102 = "OS0102";
        // Organization Structure Add Success
        public const string OS0103 = "OS0103";
        // Organization Structure Add Fail
        public const string OS0104 = "OS0104";
        // Organization Structure Edit Success
        public const string OS0105 = "OS0105";
        // Organization Structure Edit Fail
        public const string OS0106 = "OS0106";
        /// Update Formulas
        public const string PAY0301 = "PAY0301";
        public const string PAY0303 = "PAY0303";
        public const string PAY0302 = "PAY0302";
        public const string PAY0304 = "PAY0304";
        public const string PAY0305 = "PAY0305";
        public const string PAY0306 = "PAY0306";
        public const string PAY0307 = "PAY0307";
        #endregion

        #region Payroll Profile
        /// <summary>
        /// Add Payroll Profile Fail
        /// </summary>
        public const string PAY0201 = "PAY0201";
        /// <summary>
        /// Add Payroll Profile Item Fail
        /// </summary>
        public const string PAY0202 = "PAY0202";
        /// <summary>
        /// Add Payroll Profile Success
        /// </summary>
        public const string PAY0203 = "PAY0203";
        /// <summary>
        /// Payroll Profile Not Exist
        /// </summary>
        public const string PAY0204 = "PAY0204";
        /// <summary>
        /// Update Payroll Profile Fail
        /// </summary>
        public const string PAY0205 = "PAY0205";
        /// <summary>
        /// Delete Payroll Profile Item Fail
        /// </summary>
        public const string PAY0206 = "PAY0206";
        /// <summary>
        /// Update Payroll Profile Success
        /// </summary>
        public const string PAY0207 = "PAY0207";
        /// <summary>
        /// Update Payroll Profile Item Configuration Fail
        /// </summary>
        public const string PAY0208 = "PAY0208";
        /// <summary>
        /// Update Payroll Profile Item Configuration Success
        /// </summary>
        public const string PAY0209 = "PAY0209";
        /// <summary>
        /// Master Payroll Item Not Exist
        /// </summary>
        public const string PAY0210 = "PAY0210";
        /// <summary>
        /// Duplicate Payroll Profile Fail
        /// </summary>
        public const string PAY0211 = "PAY0211";
        /// <summary>
        /// Duplicate Payroll Profile Success
        /// </summary>
        public const string PAY0212 = "PAY0212";

        #endregion

        #region "GL Code"
        /// <summary>
        /// Currency Not Found
        /// </summary>
        public const string GLC0101 = "GLC0101";
        /// <summary>
        /// Add GL Code Fail
        /// </summary>
        public const string GLC0102 = "GLC0102";
        /// <summary>
        /// Add GL Code Success
        /// </summary>
        public const string GLC0103 = "GLC0103";
        /// <summary>
        /// GL Code Not Exist
        /// </summary>
        public const string GLC0104 = "GLC0104";
        /// <summary>
        /// GL Code Update Fail
        /// </summary>
        public const string GLC0105 = "GLC0105";
        /// <summary>
        /// GL Code Update Success
        /// </summary>
        public const string GLC0106 = "GLC0106";
        #endregion

        #region Payroll Run
        /// <summary>
        /// Add Payroll Run Fail
        /// </summary>
        public const string PAY0401 = "PAY0401";
        /// <summary>
        /// Add Payroll Run Success
        /// </summary>
        public const string PAY0402 = "PAY0402";
        /// <summary>
        /// Add Payroll Run Not Exist
        /// </summary>
        public const string PAY0403 = "PAY0403";
        /// <summary>
        /// Update Payroll Run Fail
        /// </summary>
        public const string PAY0404 = "PAY0404";
        /// <summary>
        /// Update Payroll Run Success
        /// </summary>
        public const string PAY0405 = "PAY0405";
        /// <summary>
        /// Duplicate Payroll Run Success
        /// </summary>
        public const string PAY0406 = "PAY0406";
        /// <summary>
        /// Add Payroll Run Fail. Status incorrect
        /// </summary>
        public const string PAY0407 = "PAY0407";
        /// <summary>
        /// Update Payroll Run Fail. Status incorrect
        /// </summary>
        public const string PAY0408 = "PAY0408";
        /// <summary>
        /// Duplicate Payroll Run Success
        /// </summary>
        public const string PAY0409 = "PAY0409";
        /// <summary>
        /// Payroll Cycle Not Exist
        /// </summary>
        public const string PAY0410 = "PAY0410";
        /// <summary>
        /// new transaction is not exists in the employee's profile
        /// </summary>
        public const string PAY0411 = "PAY0411";
        /// <summary>
        /// Add Transaction success
        /// </summary>
        public const string PAY0412 = "PAY0412";
        /// <summary>
        /// Delete Transaction success
        /// </summary>
        public const string PAY0413 = "PAY0413";
        /// <summary>
        /// Add Employee success
        /// </summary>
        public const string PAY0414 = "PAY0414";
        /// <summary>
        /// File Extension Is InValid - Only Upload xlsx File
        /// </summary>
        public const string PAY0415 = "PAY0415";
        /// <summary>
        /// ClientId Is Invalid
        /// </summary>
        public const string PAY0416 = "PAY0416";
        /// <summary>
        /// Payroll run ID Is Invalid
        /// </summary>
        public const string PAY0417 = "PAY0417";
        /// <summary>
        /// Payroll Transaction ID + EmployeeId + Payroll Item Code Is Invalid
        /// </summary>
        public const string PAY0418 = "PAY0418";
        /// <summary>
        /// Import Payroll Run Transaction Fail
        /// </summary>
        public const string PAY0419 = "PAY0419";
        /// <summary>
        /// Not select row import confirm
        /// </summary>
        public const string PAY0420 = "PAY0420";
        #endregion

        #region MPF Registered scheme
        public const string RS001 = "RS001";
        public const string RS002 = "RS002";
        public const string RS003 = "RS003";
        public const string RS004 = "RS004";
        public const string RS0101 = "RS0101";
        public const string RS0102 = "RS0102";
        public const string RS0103 = "RS0103";
        public const string RS0104 = "RS0104";
        public const string RS0105 = "RS0105";
        public const string RS0106 = "RS0106";
        #endregion

        #region enroll scheme
        public const string PS0101 = "PS0101";
        public const string PS0102 = "PS0102";
        public const string PS0103 = "PS0103";
        public const string PS0104 = "PS0104";
        public const string PS0105 = "PS0105";
        public const string PS0106 = "PS0106";
        public const string PS0107 = "PS0107";
        public const string PS0108 = "PS0108";
        public const string PS0109 = "PS0109";
        public const string PS0110 = "PS0110";
        public const string PS0111 = "PS0111";
        public const string PS0112 = "PS0112";
        public const string PS0113 = "PS0113";
        public const string PS0114 = "PS0114";
        #endregion

        #region ORSO Scheme
        public const string ORSO001 = "ORSO001";
        public const string ORSO002 = "ORSO002";
        public const string ORSO003 = "ORSO003";
        public const string ORSO004 = "ORSO004";
        #endregion ORSO Scheme
        #region Exchange Rate
        /// <summary>
        /// Handle Period Not Exist
        /// </summary>
        public const string PAY0501 = "PAY0501";
        /// <summary>
        /// Currency From incorrect
        /// </summary>
        public const string PAY0502 = "PAY0502";
        /// <summary>
        /// Currency To incorrect
        /// </summary>
        public const string PAY0503 = "PAY0503";
        /// <summary>
        /// Add Exchange Rate Fail
        /// </summary>
        public const string PAY0504 = "PAY0504";
        /// <summary>
        /// Add Exchange Rate Success
        /// </summary>
        public const string PAY0505 = "PAY0505";
        /// <summary>
        /// Exchange Rate Not Exist
        /// </summary>
        public const string PAY0506 = "PAY0506";
        /// <summary>
        /// Update Exchange Rate Fail
        /// </summary>
        public const string PAY0507 = "PAY0507";
        /// <summary>
        /// Update Exchange Rate Success
        /// </summary>
        public const string PAY0508 = "PAY0508";
        /// <summary>
        /// Average Rate Must Be Greater Than 0
        /// </summary>
        public const string PAY0509 = "PAY0509";
        /// <summary>
        /// Month End Rate Must Be Greater Than 0
        /// </summary>
        public const string PAY0510 = "PAY0510";
        #endregion

        #region payroll cycle
        /// <summary>
        /// Duplicate Payroll Cycle Code
        /// </summary>
        public const string PAY0801 = "PAY0801";
        /// <summary>
        /// Add Payroll Cycle Fail
        /// </summary>
        public const string PAY0802 = "PAY0802";
        /// <summary>
        /// Add Payroll Cycle Success
        /// </summary>
        public const string PAY0803 = "PAY0803";
        /// <summary>
        /// Payroll Cycle Not Exist
        /// </summary>
        public const string PAY0804 = "PAY0804";
        /// <summary>
        /// Update Payroll Cycle Fail
        /// </summary>
        public const string PAY0805 = "PAY0805";
        /// <summary>
        /// Update Payroll Cycle Success
        /// </summary>
        public const string PAY0806 = "PAY0806";
        /// <summary>
        /// Delete Payroll Cycle Success
        /// </summary>
        public const string PAY0807 = "PAY0807";
        /// <summary>
        /// Payroll run associated to that payroll cycle. Cannot Edit/Delete
        /// </summary>
        public const string PAY0808 = "PAY0808";
        /// <summary>
        /// Year out of range (Min: Current year -15, Max: current year + 3)
        /// </summary>
        public const string PAY0809 = "PAY0809";
        /// <summary>
        /// Payroll Cycle Setting Not Exists
        /// </summary>
        public const string PAY0810 = "PAY0810";
        #endregion

        #region payroll cycle setting
        /// <summary>
        /// Payroll Cycle Setting Success
        /// </summary>
        public const string PAY0901 = "PAY0901";
        #endregion
    }

    public struct MasterPayrollItemLabel
    {
        #region MasterPayrollItem
        // PayrollItem Not Exists
        public const string MPRI0101 = "PRI0101";
        // PayrollItem Cannot InActive. PayrollItem not display
        public const string MPRI0102 = "PRI0102";
        // PayrollItem Add Success
        public const string MPRI0103 = "PRI0103";
        // PayrollItem Add Fail
        public const string MPRI0104 = "PRI0104";
        // PayrollItem Update Success
        public const string MPRI0105 = "PRI0105";
        // PayrollItem Update Fail
        public const string MPRI0106 = "PRI0106";
        /// <summary>
        /// Duplicate Payroll Item Code
        /// </summary>
        public const string MPRI0107 = "PRI0107";
        //Delete Payroll Item Code
        public const string MPRI0108 = "PRI0108";
        //Duplicate Payroll Item Code Success
        public const string MPRI0109 = "PRI0109";
        #endregion
    }

    public struct ValMsg
    {
        public const string VAL_REQUIRED = "VAL_REQUIRED";
        public const string VAL_MAXLENGTH = "VAL_MAXLENGTH";
        public const string VAL_DUPLICATE = "VAL_DUPLICATE";
    }
}