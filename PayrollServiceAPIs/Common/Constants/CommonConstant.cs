﻿namespace PayrollServiceAPIs.Common.Constants
{
    public struct UserStatus
    {
        public const string ACTIVE_CD = "AT";
        public const string INACTIVE_CD = "IT";
        public const string ACTIVE_VALUE = "Active";
        public const string INACTIVE_VALUE = "Inactive";
    }

    public enum StatusCode
    {
        SUCCESS = 0,
        INVALID = -1,
        ACCOUNT_LOCKED = -2
    }

    public enum FormulasFlag
    {
        PREDEFINE = 0,
        FORMULAS = 1,
        FIELD = 2,
    }

    public struct UnitStatusCode
    {
        public const string INACTIVE = "INACTIVE";
        public const string ACTIVE = "ACTIVE";
        public const string ACTIVE_CD = "AT";
        public const string INACTIVE_CD = "IT";
    }

    public struct TableStatusCode
    {
        public const string INACTIVE = "INACTIVE";
        public const string ACTIVE = "ACTIVE";
    }

    public struct JWTKey
    {
        public const string CLIENT_ID = "ClientId";
        public const string ENTITY_ID = "EntityId";
        public const string TOKEN_KEY = "access_token";
    }

    /// <summary>
    /// Display Model
    /// </summary>
    public struct DisplayModel
    {
        public const byte ALWAYS_HIDDEN = 0;
        public const byte ALWAYS_SHOWN = 1;
        public const byte HIDDEN_IF_ZERO = 2;
    }

    public struct GenericCode
    {
        public const string SCHEME_TYPE = "SCHEMETYPE";
        public const string CONTRIBUTION_CYCLE = "CONTRIBUTION";
        public const string PAYMENT_METHOD = "PAYMENTMETHOD";
        public const string CURRENCY = "CURRENCY";
        public const string STATUS = "STATUS";
        public const string FEE_UNIT = "FEEUNIT";
        public const string ROUNDING_TYPE = "ROUNDINGTYPE";
        public const string SCHEME_TYPE_MPF = "MPF";
        public const string SCHEME_TYPE_ORSO = "ORSO";
        public const string DISPLAY_MODEL = "00006";
        public const string PAYROLL_ITEM_FLAG = "00001";
        public const string PAYROLL_ITEM_TAX = "00005";
        public const string PAYROLL_RUN_STATUS = "PAYROLLRUNSTATUS";
    }


    public struct PayrollRunPath
    {
        public const string EXPORT = "Content/Export/PayrollRun/";
    }
    
}