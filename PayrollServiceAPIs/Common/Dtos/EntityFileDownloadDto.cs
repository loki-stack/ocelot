using Microsoft.AspNetCore.Http;
namespace PayrollServiceAPIs.Common.Dtos
{
    public class EntityFileDownloadDto
    {
        public string FileName { get; set; }
        public IFormFile File { get; set; }
        public string EntityCode { get; set; }
        public byte[] FileContent { get; set; }

    }
}