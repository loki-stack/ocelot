using CommonLib.Validation;
using PayrollServiceAPIs.Common.Constants;
using System.ComponentModel.DataAnnotations;

namespace PayrollServiceAPIs.Common.Dtos
{
    public class TbClientEntityUpdateDto
    {
        public int EntityId { get; set; }

        [Required(ErrorMessage = ValMsg.VAL_REQUIRED)]
        [MaxLength(100, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "entityNameTxt", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.entityName", "100" })]
        public string EntityNameTxt { get; set; }

        [Required(ErrorMessage = ValMsg.VAL_REQUIRED)]
        [MaxLength(100, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "entityDisplayName", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.entityDisplayName", "100" })]
        public string EntityDisplayName { get; set; }

        [Required(ErrorMessage = ValMsg.VAL_REQUIRED)]
        [MaxLength(10, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "entityCode", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.code", "10" })]
        public string EntityCode { get; set; }
        public string CountryCd { get; set; }

        [Required(ErrorMessage = ValMsg.VAL_REQUIRED)]
        [MaxLength(1000, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "remarks", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.entityRemarks", "1000" })]
        public string Remarks { get; set; }
    }
}