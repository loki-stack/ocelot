﻿namespace PayrollServiceAPIs.Common.Dtos
{
    public class TenantViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public TenantViewModel(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}