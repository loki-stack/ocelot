using Microsoft.AspNetCore.Http;

namespace PayrollServiceAPIs.Common.Dtos
{
    public class EntityFileUploadDto
    {
        public IFormFile File { get; set; }
        public string EntityCode { get; set; }
        public string FileCategory { get; set; }
    }
}