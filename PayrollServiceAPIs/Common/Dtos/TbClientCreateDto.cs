using CommonLib.Validation;
using PayrollServiceAPIs.Common.Constants;
using System.ComponentModel.DataAnnotations;

namespace PayrollServiceAPIs.Common.Dtos
{
    public class TbClientCreateDto
    {

        [Required(ErrorMessage = ValMsg.VAL_REQUIRED)]
        [MaxLength(100, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "clientNameTxt", Level = Level.ERROR, Placeholder = new string[] { "common.name", "100" })]
        public string ClientNameTxt { get; set; }

        [Required(ErrorMessage = ValMsg.VAL_REQUIRED)]
        [MaxLength(10, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "clientCode", Level = Level.ERROR, Placeholder = new string[] { "clientModule.clientCode", "10" })]
        public string ClientCode { get; set; }

        [Required(ErrorMessage = ValMsg.VAL_REQUIRED)]
        [MaxLength(200, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "contactPerson", Level = Level.ERROR, Placeholder = new string[] { "common.director", "200" })]
        public string ContactPerson { get; set; }

        [Required(ErrorMessage = ValMsg.VAL_REQUIRED)]
        [MaxLength(2, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "statusCd", Level = Level.ERROR, Placeholder = new string[] { "common.status", "2" })]
        public string StatusCd { get; set; }
    }
}