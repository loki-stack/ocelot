﻿using System.Collections.Generic;

namespace PayrollServiceAPIs.Common.Dtos
{
    public class ResponseModel
    {
        public string Ray { get; set; }
        public Dictionary<string, object> Data { get; set; }
        public WebMessage WebMessage { get; set; }
        public string RedirectUrl { get; set; }
        public string Code { get; set; }
    }

    public class WebMessage
    {
        public List<WebMessageItem> Toast { get; set; }
        public List<WebMessageItem> Component { get; set; }
    }

    public class WebMessageItem
    {
        public string Ref { get; set; }
        public string Level { get; set; }
        public string Label { get; set; }
        public List<string> PlaceHolder { get; set; }
    }
}