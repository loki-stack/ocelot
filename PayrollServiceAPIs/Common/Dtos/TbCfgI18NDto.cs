﻿using System;

namespace PayrollServiceAPIs.Common.Dtos
{
    public class TbCfgI18NDto
    {
        public string LanguageTag { get; set; }
        public string TextKey { get; set; }
        public string TextValue { get; set; }
        public bool? DefaultFlag { get; set; }
        public string CreateBy { get; set; }
        public DateTimeOffset CreateDt { get; set; }
        public string ModifyBy { get; set; }
        public DateTimeOffset ModifyDt { get; set; }

        public TbCfgI18NDto(string languageTag, string textKey, string textValue, bool defaultFlag, string userName, bool isCreate)
        {
            LanguageTag = languageTag;
            TextKey = textKey;
            TextValue = textValue;
            DefaultFlag = defaultFlag;
            if (isCreate)
            {
                CreateBy = userName;
                CreateDt = DateTime.UtcNow;
            }
            else
            {
                ModifyBy = userName;
                ModifyDt = DateTime.UtcNow;
            }
        }
    }
}
