﻿using System.Collections.Generic;

namespace PayrollServiceAPIs.Common.Dtos
{
    public class Pagination<T>
    {
        /// <summary>
        /// current page
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// total page
        /// </summary>
        public int TotalPages { get; set; }

        /// <summary>
        /// total record in page
        /// </summary>
        public int Size { get; set; }

        /// <summary>
        /// record return
        /// </summary>
        public int NumberOfElements { get; set; }

        /// <summary>
        /// total record found
        /// </summary>
        public int TotalElements { get; set; }

        /// <summary>
        /// list data return
        /// </summary>
        public List<T> Content { get; set; }

    }
}
