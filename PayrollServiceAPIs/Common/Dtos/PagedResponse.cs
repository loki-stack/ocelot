﻿using CommonLib.Dtos;

namespace PayrollServiceAPIs.Common.Dtos
{
    //TODO: Need discuss more
    public class PagedResponse<T> : ApiResponse<T>
    {
        public int TotalRecords { get; set; }

        public PagedResponse(T data, int totalRecords)
        {
            this.Data = data;
            this.TotalRecords = totalRecords;
        }
    }
}