using CommonLib.Validation;
using PayrollServiceAPIs.Common.Constants;
using System;
using System.ComponentModel.DataAnnotations;

namespace PayrollServiceAPIs.Common.Dtos
{
    public class TbClientEntityDto
    {
        public int EntityId { get; set; }
        public int ParentClientId { get; set; }
        // [NotMapped]
        //public HierarchyId EntityLevel { get; set; }

        [Required(ErrorMessage = ValMsg.VAL_REQUIRED)]
        [MaxLength(100, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "entityNameTxt", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.entityName", "100" })]
        public string EntityNameTxt { get; set; }

        [Required(ErrorMessage = ValMsg.VAL_REQUIRED)]
        [MaxLength(100, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "entityDisplayName", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.entityDisplayName", "100" })]
        public string EntityDisplayName { get; set; }
        public string EntityName { get; set; }

        [Required(ErrorMessage = ValMsg.VAL_REQUIRED)]
        [MaxLength(10, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "entityCode", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.code", "10" })]
        public string EntityCode { get; set; }
        public string CountryCd { get; set; }

        [Required(ErrorMessage = ValMsg.VAL_REQUIRED)]
        [MaxLength(1000, ErrorMessage = ValMsg.VAL_MAXLENGTH)]
        [ExtraResult(ComponentId = "remarks", Level = Level.ERROR, Placeholder = new string[] { "clientEntityModule.info.entityRemarks", "1000" })]
        public string Remarks { get; set; }
        public string DbHost { get; set; }
        public string DbName { get; set; }
        public int DbPort { get; set; }
        public string ConnStringParam { get; set; }
        public string StatusCd { get; set; }
        public DateTimeOffset CreatedDt { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset ModifiedDt { get; set; }
        public string ModifiedBy { get; set; }
        public string BusinessAddressLine1 { get; set; }
        public string BusinessAddressLine2 { get; set; }
        public string BusinessAddressLine3 { get; set; }
        public string BusinessPhone { get; set; }
        public string BusinessFax { get; set; }
        public string ContactAddressLine1 { get; set; }
        public string ContactAddressLine2 { get; set; }
        public string ContactAddressLine3 { get; set; }
        public string ContactPhone { get; set; }
        public string ContactFax { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public string ClientSupportEmail { get; set; }
        public string TaxId { get; set; }
        public string BusinessPhoneAreaCd { get; set; }
        public string BusinessPhoneFaxAreaCd { get; set; }
        public string ContactPhoneAreaCd { get; set; }
        public string ContactFaxAreaCd { get; set; }

        public virtual TbClientDto ParentClient { get; set; }

    }
}