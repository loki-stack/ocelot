﻿using CommonLib.Models.Client;
using CommonLib.Utils;
using Microsoft.EntityFrameworkCore;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.Formulas.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos.ExchangeRate;
using PayrollServiceAPIs.PayRoll.Dtos.PayrollCycle;
using PayrollServiceAPIs.PayRoll.Dtos.PayrollRun;
using PayrollServiceAPIs.PayRoll.Dtos.PayrollRun.Export;

namespace PayrollServiceAPIs.Common.Models
{
    public class TenantDBContext : ClientBasicDBContext
    {

        public virtual DbSet<PayrollProfileItem> PayrollProfileItem { get; set; }
        public virtual DbSet<PayrollProfileViewModel> PayrollProfileViewModel { get; set; }
        public virtual DbSet<PayrollProfileViewItemModel> PayrollProfileViewItemModel { get; set; }
        public virtual DbSet<MasterPayrollViewModel> BasicItemImfomationPayRollItemModel { get; set; }
        public virtual DbSet<MasterPayrollItemModel> MasterPayrollItems { get; set; }

        public virtual DbSet<PayrollRunViewModel> PayrollRunViewModel { get; set; }
        public virtual DbSet<PayrollRunTransactionViewModel> PayrollRunTransactionViewModel { get; set; }
        public virtual DbSet<PayrollRunEmployeeViewModel> PayrollRunEmployeeViewModel { get; set; }
        public virtual DbSet<EmployeeNameErrorModel> EmployeeNameErrorModel { get; set; }
        public virtual DbSet<MasterPayrollItemNameModel> MasterPayrollItemNameModel { get; set; }
        public virtual DbSet<ExportTransactionModel> ExportTransactionModel { get; set; }
        public virtual DbSet<ExchangeRateViewModel> ExchangeRateViewModel { get; set; }
        public virtual DbSet<GlCodeModel> GlCodes { get; set; }
        public virtual DbSet<PayrollCycleViewModel> PayrollCycleViewModel { get; set; }

        public TenantDBContext(string connectionStr) : base(DbContextUtil.CreateDDRConnection<TenantDBContext>(connectionStr))
        {

        }

        public override void OnModelCreatingExtention(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PayrollProfileItem>().HasNoKey();
            modelBuilder.Entity<PayrollProfileViewModel>().HasNoKey();
            modelBuilder.Entity<PayrollProfileViewItemModel>().HasNoKey();
            modelBuilder.Entity<MasterPayrollViewModel>().HasNoKey();
            modelBuilder.Entity<MasterPayrollItemModel>().HasNoKey();

            modelBuilder.Entity<PayrollRunViewModel>().HasNoKey();
            modelBuilder.Entity<PayrollRunTransactionViewModel>().HasNoKey();
            modelBuilder.Entity<PayrollRunEmployeeViewModel>().HasNoKey();
            modelBuilder.Entity<EmployeeNameErrorModel>().HasNoKey();
            modelBuilder.Entity<MasterPayrollItemNameModel>().HasNoKey();
            modelBuilder.Entity<ExportTransactionModel>().HasNoKey();
            modelBuilder.Entity<ExchangeRateViewModel>().HasNoKey();

            modelBuilder.Entity<GlCodeModel>().HasNoKey();
            modelBuilder.Entity<PayrollCycleViewModel>().HasNoKey();
            modelBuilder.Entity<FormulasModel>().HasNoKey();
        }
    }
}