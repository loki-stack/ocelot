﻿namespace PayrollServiceAPIs.Common.Models
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public string CoreServiceGenericCode { get; set; }
    }
}
