﻿using AutoMapper;
using CommonLib.Models;
using CommonLib.Models.Client;
using CommonLib.Models.Core;
using PayrollServiceAPIs.Common.Models;
using PayrollServiceAPIs.PayRoll.Dtos;
using PayrollServiceAPIs.PayRoll.Dtos.EnrolledScheme;
using PayrollServiceAPIs.PayRoll.Dtos.ORSOScheme;

namespace PayrollServiceAPIs.Common.Mapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<TbPayMpfRegisteredScheme, MPFRegisteredSchemeViewList>();
            CreateMap<TbPayEnrolledScheme, EnrolledSchemeViewDetail>();
            CreateMap<ORSOSchemeRequest, TbPayOrsoScheme>();
            CreateMap<TbPayOrsoScheme, ORSOSchemeViewList>();
            CreateMap<TbPayOrsoScheme, ORSOSchemeViewDetail>();
        }
    }
}
