﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommonLib.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PayrollServiceAPIs.Formulas.Services.Interfaces;

namespace PayrollServiceAPIs.Formulas.Controller
{
    [Route("api/PAY/[action]/[controller]")]
    [ApiController]
    public class FormulasController : ControllerBase
    {
        private readonly IFormulasService _formulasService;
        public FormulasController(IFormulasService formulasService)
        {
            _formulasService = formulasService;
        }

        [HttpGet("{payrollRunId}")]
        [ActionName("PAY0301")]
        [Authorize(Roles = "PAY0301")]
        public async Task<ActionResult<ApiResponse<double>>> GetCanculateFormulas(int payrollRunId, [FromQuery] ApiRequest request)
        {
            var response = await _formulasService.HandleFormulas(payrollRunId, request);
            return Ok(response);
        }
    }
}
