﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.Formulas.Dtos
{
    public class FormulasModel
    {
        public string Tbpaypayrollcycle { get; set; }
        public int? Payrollcycleid { get; set; }
        public DateTimeOffset? Datestart { get; set; }
        public DateTimeOffset? Dateend { get; set; }
        public string Cyclecode { get; set; }
        public DateTimeOffset? Cutoffdate { get; set; }
        public string Statuscd { get; set; }
        public int? Formulasid { get; set; }
        public int? Employeeid { get; set; }
    }
}
