﻿using CommonLib.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.Formulas.Services.Interfaces
{
    public interface IFormulasService
    {
        Task<ApiResponse<string>> HandleFormulas(int payrollProfileId, ApiRequest request);
    }
}
