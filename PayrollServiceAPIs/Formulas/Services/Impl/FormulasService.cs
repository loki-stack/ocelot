﻿using CommonLib.Dtos;
using CommonLib.Repositories.Interfaces;
using PayrollServiceAPIs.Formulas.Dtos;
using PayrollServiceAPIs.Formulas.Services.Interfaces;
using PayrollServiceAPIs.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.Formulas.Services.Impl
{
    public class FormulasService : IFormulasService
    {
        private readonly CommonLib.Repositories.Interfaces.IFormulasRepository _formulasRepository;
        private readonly PayrollServiceAPIs.Formulas.Repository.Interfaces.IFormulasRepository _formulasRepositoryPay;
        public FormulasService(CommonLib.Repositories.Interfaces.IFormulasRepository formulasRepository, PayrollServiceAPIs.Formulas.Repository.Interfaces.IFormulasRepository formulasRepositoryPay)
        {
            _formulasRepository = formulasRepository;
            _formulasRepositoryPay = formulasRepositoryPay;
        }

        public async Task<ApiResponse<string>> HandleFormulas(int payrollProfileId, ApiRequest request)
        {
            ApiResponse<string> response = new ApiResponse<string>()
            {
                Ray = request.Ray ?? string.Empty,
                Data = string.Empty
            };
            string formulas = string.Empty;

            List<Dictionary<string, string>> mapFormulasTag = new List<Dictionary<string, string>>();

            List<FormulasResult> lstFormulas = await _formulasRepository.GetFormulas(request.Parameter.ClientId, request.Parameter.EntityId);

            List<FormulasTagViewResult> listFormulasTag = new List<FormulasTagViewResult>();

            listFormulasTag = await _formulasRepository.GetFormulasTag();

            FormulasModel formulasMapping = await _formulasRepositoryPay.GetValueFromTableField(payrollProfileId);
            
            if (formulasMapping == null)
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = Common.Constants.Level.ERROR,
                    LabelCode = Common.Constants.Label.PAY0303
                });
                return response;
            }

            FormulasResult objFormulas = lstFormulas.Single(s => s.FormulasId == formulasMapping.Formulasid);

            if (objFormulas == null)
            {
                response.Message.Toast.Add(new MessageItem()
                {
                    Level = Common.Constants.Level.ERROR,
                    LabelCode = Common.Constants.Label.PAY0303
                });
                return response;
            }    

            formulas = objFormulas.Formulas.ToString();
            List<string> tagParameter = MathParser.HandleParameter(formulas);

            List<Dictionary<string, string>> formulasTagAvailable = new List<Dictionary<string, string>>();

            foreach (FormulasTagViewResult formulas_tag in listFormulasTag)
            {
                if ((formulas_tag.Flag == (int)Common.Constants.FormulasFlag.PREDEFINE))
                {
                    Dictionary<string, string> subFormulasAvailable = new Dictionary<string, string>();
                    subFormulasAvailable.Add(formulas_tag.TagName.ToString(), formulas_tag.Predefined.ToString());
                    formulasTagAvailable.Add(subFormulasAvailable);

                    if (tagParameter.Contains(formulas_tag.TagName.ToString()))
                    {
                        Dictionary<string, string> subFormulasTag = new Dictionary<string, string>();
                        subFormulasTag.Add(formulas_tag.TagName.ToString(), formulas_tag.Predefined.ToString());
                        mapFormulasTag.Add(subFormulasTag);
                    }
                }
                else if (formulas_tag.Flag == (int)Common.Constants.FormulasFlag.FIELD)
                {
                    var fieldName = MathParser.HandleFieldName(formulas_tag.FieldName.ToString());
                    var lstProperties = MathParser.GetPropertiesNameOfClass(formulasMapping);
                    var propName = string.Empty;

                    int int_item = 0;

                    if(fieldName[0].ToString().ToLower() == lstProperties[0].ToString().ToLower())
                    {
                        int_item += 1;
                    }

                    for(int item = 1; item < lstProperties.Count - 1; item++)
                    {
                        if(lstProperties[item].ToString().ToLower() == fieldName[1].ToString().ToLower())
                        {
                            propName = lstProperties[item];
                            int_item += 1;
                            break;
                        }
                    }

                    if(int_item == 2)
                    {
                        if (tagParameter.Contains(formulas_tag.TagName.ToString()))
                        {
                            Dictionary<string, string> subFormulasTag = new Dictionary<string, string>();
                            var value = formulasMapping.GetType().GetProperty(propName).GetValue(formulasMapping, null);
                            subFormulasTag.Add(formulas_tag.TagName.ToString(), value.ToString());
                            mapFormulasTag.Add(subFormulasTag);
                        }
                    }
                   
                }
                else if (formulas_tag.Flag == (int)Common.Constants.FormulasFlag.FORMULAS)
                {
                    if (tagParameter.Contains(formulas_tag.TagName.ToString()))
                    {
                        FormulasResult formulasInTag = lstFormulas.Single(s => s.FormulasId == formulas_tag.FormulasId);
                        formulas = formulas.Replace(formulas_tag.TagName.ToString(), formulasInTag.Formulas.ToString());

                        tagParameter.Remove(formulas_tag.TagName.ToString());
                        tagParameter.AddRange(MathParser.HandleParameter(formulasInTag.Formulas.ToString()));
                        tagParameter = tagParameter.Distinct().ToList();

                        foreach (string tag in tagParameter)
                        {
                            var tagDictionary = formulasTagAvailable.SelectMany(x => x).Where(x => x.Key == tag).ToList();
                            Dictionary<string, string> newFormulasTag = new Dictionary<string, string>();
                            if (tagDictionary.Count > 0)
                            {
                                newFormulasTag.Add(tagDictionary[0].Key, tagDictionary[0].Value);
                                mapFormulasTag.Add(newFormulasTag);
                            }
                        }
                    }
                }
            }
            mapFormulasTag = mapFormulasTag.Distinct().ToList();

            response.Data =  MathParser.ExcuteFormulas(mapFormulasTag, formulas);
            return response;
        }
    }
}
