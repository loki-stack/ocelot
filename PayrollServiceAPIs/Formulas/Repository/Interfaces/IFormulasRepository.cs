﻿using PayrollServiceAPIs.Formulas.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.Formulas.Repository.Interfaces
{
    public interface IFormulasRepository
    {
        Task<FormulasModel> GetValueFromTableField(int payrollRunId);
    }
}
