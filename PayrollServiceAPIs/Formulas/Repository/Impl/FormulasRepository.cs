﻿using CommonLib.Models.Client;
using CommonLib.Services;
using PayrollServiceAPIs.Common.Models;
using PayrollServiceAPIs.Formulas.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using PayrollServiceAPIs.Common.Dtos;
using PayrollServiceAPIs.Common.Repositories.Interfaces;
using PayrollServiceAPIs.Formulas.Dtos;

namespace PayrollServiceAPIs.Formulas.Repository.Impl
{
    public class FormulasRepository : IFormulasRepository
    {

        private readonly TenantDBContext _context;

        public FormulasRepository(IConnectionStringContainer connectionStringRepository)
        {
            _context = new TenantDBContext(connectionStringRepository.GetConnectionString());
        }

        public async Task<FormulasModel> GetValueFromTableField(int payrollRunId)
        {
            var payrollRunIdPar = new SqlParameter("@payrollRunId", payrollRunId);

            var listFormulasMap = await _context.Set<FormulasModel>().FromSqlRaw("EXECUTE SP_MAP_FORMULAS_AND_CYCLE @payrollRunId", payrollRunIdPar).ToListAsync();
            if (listFormulasMap.Count > 0)
                return listFormulasMap.FirstOrDefault();

            return null;
        }
    }
}
