﻿using org.mariuszgromada.math.mxparser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PayrollServiceAPIs.Utils
{
    public class MathParser
    {
        static public List<string> HandleParameter(string formulas)
        {
            List<string> tagParameter = null;
            if (!string.IsNullOrEmpty(formulas))
            {
                if (formulas.ToString().Contains('#'))
                {
                    var pattern = @"\[\#(.*?)\]";
                    tagParameter = Regex.Matches(formulas, pattern).Cast<Match>().Select(m => m.Value).ToList();
                }
            }
            if (tagParameter != null)
            {
                return tagParameter.Distinct().ToList();
            }
            return null;
        }

        static public List<string> HandleFieldName(string fieldName)
        {
            List<string> fieldNamePar = new List<string>();

            if(!string.IsNullOrEmpty(fieldName))
            {

                if (fieldName.ToString().Contains('#'))
                {
                    var pattern = @"\#(.*?)\]";
                    fieldNamePar = Regex.Matches(fieldName.ToLower(), pattern).Cast<Match>().Select(m => m.Value).ToList();
                }
            }

            foreach (string item in fieldNamePar)
            {
                fieldNamePar = Regex.Replace(item, @"(\#|\]|_)", "").Split(".").ToList();
            }

            return fieldNamePar;
        }

        static public List<string> GetPropertiesNameOfClass(object pObject)
        {
            List<string> lstProperty = new List<string>();
            if(pObject != null)
            {
                foreach(var prop in pObject.GetType().GetProperties())
                {
                    lstProperty.Add(prop.Name);
                }
            }
            return lstProperty;
        }

        static public string ExcuteFormulas(List<Dictionary<string, string>> mapFormulasTag, string formulas)
        {
            string str_subFormulas = string.Empty;
            string str_subArgument = string.Empty;
            string str_formulas = string.Empty;
            string frCanculator = string.Empty;
            string frArgument = string.Empty;

            int int_item = 0;

            foreach (var lstItem in mapFormulasTag)
            {
                foreach (var item in lstItem)
                {
                    int_item += 1;
                    string str_key = item.Key.ToString().Remove(0, 2);
                    str_key = str_key.Remove(str_key.Length - 1, 1);
                    if (str_key.Contains(" "))
                    {
                        str_key = str_key.Replace(" ", "_");
                    }
                    if (str_key.Contains("  "))
                    {
                        str_key = str_key.Replace("  ", "_");
                    }
                    if (str_key.Contains("#"))
                    {
                        str_key = str_key.Replace("#", "");
                    }
                    string arg = str_key.ToString();

                    if (formulas.Contains(item.Key.ToString()))
                    {
                        formulas = formulas.Replace(item.Key.ToString(), arg);
                        str_subFormulas += arg + ",";
                        str_subArgument += item.Value.ToString() + ",";
                    }
                }
            }
            if (str_subFormulas.EndsWith(","))
            {
                str_subFormulas = str_subFormulas.Remove(str_subFormulas.Length - 1, 1);
            }
            if (str_subArgument.EndsWith(","))
            {
                str_subArgument = str_subArgument.Remove(str_subArgument.Length - 1, 1);
            }
            str_formulas = "At(" + str_subFormulas + ")";
            frArgument = "At(" + str_subArgument + ")";

            if (string.IsNullOrEmpty(formulas))
            {
                frCanculator = str_formulas + " = " + "0";
            }
            else
            {
                frCanculator = str_formulas + " = " + formulas;
            }

            Function salary = new Function(frCanculator);
            Expression exec = new Expression(frArgument, salary);

            if (!salary.checkSyntax())
            {
                string functionExpression = salary.getFunctionExpressionString();
                string str_error = "Function Expression: " + functionExpression + " error ";
                var detailError = Regex.Matches(salary.getErrorMessage(), @"(line\s\d+)|(column\s\d+)").Cast<Match>().Select(m => m.Value).ToList();
                foreach (string error in detailError)
                {
                    str_error += " " + error + ", ";
                }

                return str_error;
            }

            return exec.calculate().ToString();
        }
    }
}
