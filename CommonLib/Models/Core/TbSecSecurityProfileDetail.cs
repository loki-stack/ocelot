﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Core
{
    public partial class TbSecSecurityProfileDetail
    {
        public int SecurityProfileId { get; set; }
        public int RoleGroupId { get; set; }
        public int DataAccessGroupId { get; set; }

        public virtual TbSecDataAccessGroup DataAccessGroup { get; set; }
        public virtual TbSecRoleGroup RoleGroup { get; set; }
    }
}
