﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Core
{
    public partial class TbPayMpfRegisteredScheme
    {
        public int MpfRegisteredSchemeId { get; set; }
        public int TrusteeId { get; set; }
        public string SchemeNameEn { get; set; }
        public string SchemeNameCn { get; set; }
        public string RemittanceStatementFormat { get; set; }
        public bool? ProrateRiRemittanceStatement { get; set; }
        public string InterfaceFileFormat { get; set; }
        public string Type { get; set; }
        public string FinancialYearDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
    }
}
