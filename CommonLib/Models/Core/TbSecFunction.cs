﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Core
{
    public partial class TbSecFunction
    {
        public TbSecFunction()
        {
            TbSecRoleFunction = new HashSet<TbSecRoleFunction>();
        }

        public int FunctionId { get; set; }
        public string MenuCd { get; set; }
        public string ModuleCd { get; set; }
        public string FeatureCd { get; set; }
        public string FunctionCd { get; set; }
        public string RoleCd { get; set; }
        public int MenuDisplayOrder { get; set; }
        public string CreateBy { get; set; }
        public DateTimeOffset CreateDt { get; set; }
        public string ModifyBy { get; set; }
        public DateTimeOffset ModifyDt { get; set; }

        public virtual ICollection<TbSecRoleFunction> TbSecRoleFunction { get; set; }
    }
}
