﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Core
{
    public partial class TbSecDataAccessGroup
    {
        public TbSecDataAccessGroup()
        {
            TbSecDataAccessClientEntity = new HashSet<TbSecDataAccessClientEntity>();
            TbSecDataAccessGrade = new HashSet<TbSecDataAccessGrade>();
            TbSecDataAccessRemunerationPackage = new HashSet<TbSecDataAccessRemunerationPackage>();
            TbSecSecurityProfileDetail = new HashSet<TbSecSecurityProfileDetail>();
        }

        public int DataAccessGroupId { get; set; }
        public int SpecificClientId { get; set; }
        public int SpecificCompanyId { get; set; }
        public string DataAccessGroupNameTxt { get; set; }
        public string DataAccessGroupDescriptionTxt { get; set; }
        public string CreateBy { get; set; }
        public DateTimeOffset CreateDt { get; set; }
        public string ModifyBy { get; set; }
        public DateTimeOffset ModifyDt { get; set; }

        public virtual ICollection<TbSecDataAccessClientEntity> TbSecDataAccessClientEntity { get; set; }
        public virtual ICollection<TbSecDataAccessGrade> TbSecDataAccessGrade { get; set; }
        public virtual ICollection<TbSecDataAccessRemunerationPackage> TbSecDataAccessRemunerationPackage { get; set; }
        public virtual ICollection<TbSecSecurityProfileDetail> TbSecSecurityProfileDetail { get; set; }
    }
}
