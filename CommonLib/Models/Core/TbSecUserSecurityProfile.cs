﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Core
{
    public partial class TbSecUserSecurityProfile
    {
        public int UserId { get; set; }
        public int SecurityProfileId { get; set; }
        public DateTimeOffset EffectiveStartDt { get; set; }
        public DateTimeOffset? EffectiveEndDt { get; set; }

        public virtual TbSecSecurityProfile SecurityProfile { get; set; }
        public virtual TbSecUser User { get; set; }
    }
}
