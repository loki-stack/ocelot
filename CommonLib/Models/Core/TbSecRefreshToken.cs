﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Core
{
    public partial class TbSecRefreshToken
    {
        public string Token { get; set; }
        public string JwtId { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public bool? Used { get; set; }
        public bool? Invalidated { get; set; }
        public int? UserId { get; set; }
    }
}
