﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Core
{
    public partial class TbCfgHoliday
    {
        public int HolidayDateId { get; set; }
        public DateTime? Date { get; set; }
        public string DescriptionPrimaryLanguage { get; set; }
        public string DescriptionSecondaryLanguage { get; set; }
        public string HolidayType { get; set; }
        public int? CalendarId { get; set; }
        public bool? IsWorkingDay { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }

        public virtual TbCfgGlobalCalendar Calendar { get; set; }
    }
}
