﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Core
{
    public partial class TbSecDataAccessGrade
    {
        public int DataAccessGradeId { get; set; }
        public int DataAccessGroupId { get; set; }
        public int MinGradeLevelNum { get; set; }
        public int MaxGradeLevelNum { get; set; }

        public virtual TbSecDataAccessGroup DataAccessGroup { get; set; }
    }
}
