﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Core
{
    public partial class TbMsgTemplate
    {
        public TbMsgTemplate()
        {
            TbMsgTemplateLang = new HashSet<TbMsgTemplateLang>();
        }

        public int TemplateId { get; set; }
        public int ClientId { get; set; }
        public int EntityId { get; set; }
        public string TemplateCode { get; set; }
        public string TemplateType { get; set; }
        public bool FlagHtml { get; set; }
        public string Portal { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }

        public virtual ICollection<TbMsgTemplateLang> TbMsgTemplateLang { get; set; }
    }
}
