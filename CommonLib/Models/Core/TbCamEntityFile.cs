﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Core
{
    public partial class TbCamEntityFile
    {
        public int CamEntityFileId { get; set; }
        public int EntityId { get; set; }
        public string FileCategory { get; set; }
        public string FileName { get; set; }
        public byte[] FileContent { get; set; }
        public bool IsDeleted { get; set; }
        public string FileRemark { get; set; }
        public DateTimeOffset CreatedDt { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset ModifiedDt { get; set; }
        public string ModifiedBy { get; set; }

        public virtual TbCamEntity Entity { get; set; }
    }
}
