﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Core
{
    public partial class TbMsgMessage
    {
        public long MessageId { get; set; }
        public int TemplateLangId { get; set; }
        public string MessageType { get; set; }
        public string Recipient { get; set; }
        public string MessageTitle { get; set; }
        public string MessageContent { get; set; }
        public DateTimeOffset? ProcessedDt { get; set; }
        public string Status { get; set; }
        public int? AttemptCount { get; set; }
        public string Portal { get; set; }
        public bool FlagHtml { get; set; }
        public int? UserId { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }

        public virtual TbMsgTemplateLang TemplateLang { get; set; }
        public virtual TbSecUser User { get; set; }
    }
}
