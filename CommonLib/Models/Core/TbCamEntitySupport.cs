﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Core
{
    public partial class TbCamEntitySupport
    {
        public int SupportId { get; set; }
        public int EntityId { get; set; }
        public string SupportContactName { get; set; }
        public string SupportPhoneNumber { get; set; }
        public string SupportEmail { get; set; }
        public string Remark { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }

        public virtual TbCamEntity Entity { get; set; }
    }
}
