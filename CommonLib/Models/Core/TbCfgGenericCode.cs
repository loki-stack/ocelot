﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace CommonLib.Models.Core
{
    public partial class TbCfgGenericCode
    {
        public TbCfgGenericCode()
        {
            InverseParentCode = new HashSet<TbCfgGenericCode>();
        }

        public int CodeId { get; set; }
        public int? ParentCodeId { get; set; }
        public HierarchyId CodeLevel { get; set; }
        public int SpecificClientId { get; set; }
        public int SpecificCompanyId { get; set; }
        public string CodeType { get; set; }
        public string CodeValue { get; set; }
        public string I18nTextKey { get; set; }
        public int DisplayOrder { get; set; }
        public string StatusCd { get; set; }
        public string CreateBy { get; set; }
        public DateTimeOffset CreateDt { get; set; }
        public string ModifyBy { get; set; }
        public DateTimeOffset ModifyDt { get; set; }
        public string Remark { get; set; }

        public virtual TbCfgGenericCode ParentCode { get; set; }
        public virtual ICollection<TbCfgGenericCode> InverseParentCode { get; set; }
    }
}
