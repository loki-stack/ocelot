﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Core
{
    public partial class TbPayMpfTrustee
    {
        public int TrusteeId { get; set; }
        public string TrusteeName { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string EmailAddress { get; set; }
        public string Website { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
    }
}
