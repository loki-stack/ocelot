﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Core
{
    public partial class TbSecDataAccessRemunerationPackage
    {
        public int DataAccessGroupId { get; set; }
        public int RemunerationPackageId { get; set; }

        public virtual TbSecDataAccessGroup DataAccessGroup { get; set; }
    }
}
