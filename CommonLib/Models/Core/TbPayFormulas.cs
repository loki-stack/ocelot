﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Core
{
    public partial class TbPayFormulas
    {
        public int FormulasId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public byte Level { get; set; }
        public string StatusCd { get; set; }
        public string Formulas { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
        public bool? EditAble { get; set; }
        public int? ClientId { get; set; }
        public int? EntityId { get; set; }
    }
}
