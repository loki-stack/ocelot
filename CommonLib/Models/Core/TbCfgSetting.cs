﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Core
{
    public partial class TbCfgSetting
    {
        public int SettingId { get; set; }
        public int SpecificClientId { get; set; }
        public int SpecificEntityId { get; set; }
        public bool PdfStatus { get; set; }
        public string PrimaryLanguage { get; set; }
        public string SecondaryLanguage { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset ModifiedDt { get; set; }
        public int ProbationPeriod { get; set; }
        public string SalaryCurrency { get; set; }
        public string PaymentCurrency { get; set; }
        public int? WorkCalendar { get; set; }
        public int? SignInMethod { get; set; }
        public string NotificationEmail { get; set; }
    }
}
