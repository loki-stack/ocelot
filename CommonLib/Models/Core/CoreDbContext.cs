﻿
using CommonLib.Dtos;
using Microsoft.EntityFrameworkCore;

namespace CommonLib.Models.Core
{
    public class CoreDbContext : CoreBasicDbContext
    {
        public virtual DbSet<GenericCodeResult> GenericCodeResult { get; set; }
        public virtual DbSet<LeftMenuResult> LeftMenus { get; set; }
        public virtual DbSet<RightMenuResult> RightMenus { get; set; }
        public virtual DbSet<ClientResult> Clients { get; set; }
        public virtual DbSet<FormulasResult> FormulasResult { get; set; }

        public CoreDbContext(DbContextOptions<CoreDbContext> options)
            : base(options)
        {
        }

        public override void OnModelCreatingExtention(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GenericCodeResult>().HasNoKey();
            modelBuilder.Entity<LeftMenuResult>().HasNoKey();
            modelBuilder.Entity<RightMenuResult>().HasNoKey();
            modelBuilder.Entity<ClientResult>().HasNoKey();
            modelBuilder.Entity<FormulasResult>().HasNoKey();
        }
    }
}