﻿
using CommonLib.Dtos;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;

namespace CommonLib.Models.Core
{
    public partial class CoreBasicDbContext : DbContext
    {
        public CoreBasicDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {

        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder)
        {
            OnModelCreatingExtention(modelBuilder);
        }

        public virtual void OnModelCreatingExtention(ModelBuilder modelBuilder)
        {

        }
    }
}