﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace CommonLib.Models.Core
{
    public partial class TbSecDataAccessOrganizationStructure
    {
        public int DataAccessGroupId { get; set; }
        public HierarchyId NodeLevel { get; set; }
        public bool IsSelectAllDescendants { get; set; }
    }
}
