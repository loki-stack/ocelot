﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Core
{
    public partial class TbCfgGlobalCalendar
    {
        public TbCfgGlobalCalendar()
        {
            TbCfgHoliday = new HashSet<TbCfgHoliday>();
        }

        public int GlobalCalendarId { get; set; }
        public string GlobalLevelCalendarName { get; set; }
        public string Country { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }

        public virtual ICollection<TbCfgHoliday> TbCfgHoliday { get; set; }
    }
}
