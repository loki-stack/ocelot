﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Core
{
    public partial class TbSecRoleFunction
    {
        public int RoleGroupId { get; set; }
        public int FunctionId { get; set; }

        public virtual TbSecFunction Function { get; set; }
        public virtual TbSecRoleGroup RoleGroup { get; set; }
    }
}
