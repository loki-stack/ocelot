﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Core
{
    public partial class VwSecFarDarFlatten
    {
        public int UserId { get; set; }
        public int SecurityProfileId { get; set; }
        public DateTimeOffset EffectiveStartDt { get; set; }
        public DateTimeOffset? EffectiveEndDt { get; set; }
        public string SecurityProfileNameTxt { get; set; }
        public int? SpecificClientId { get; set; }
        public int? SpecificEntityId { get; set; }
        public int? DataAccessGroupId { get; set; }
        public int? RoleGroupId { get; set; }
        public string RoleGroupNameTxt { get; set; }
        public int? FunctionId { get; set; }
        public string DataAccessGroupNameTxt { get; set; }
        public int? SpecificCompanyId { get; set; }
        public int? MinGradeLevelNum { get; set; }
        public int? MaxGradeLevelNum { get; set; }
        public string ClientLevel { get; set; }
        public string EntityLevel { get; set; }
        public int? RemunerationPackageId { get; set; }
        public string NodeLevel { get; set; }
        public bool? IsSelectAllDescendants { get; set; }
    }
}
