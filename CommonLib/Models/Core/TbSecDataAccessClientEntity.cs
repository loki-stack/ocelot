﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace CommonLib.Models.Core
{
    public partial class TbSecDataAccessClientEntity
    {
        public int DataAccessClientEntityId { get; set; }
        public int DataAccessGroupId { get; set; }
        public HierarchyId ClientLevel { get; set; }
        public HierarchyId EntityLevel { get; set; }

        public virtual TbSecDataAccessGroup DataAccessGroup { get; set; }
    }
}
