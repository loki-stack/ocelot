﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Core
{
    public partial class TbSecUser
    {
        public TbSecUser()
        {
            TbMsgMessage = new HashSet<TbMsgMessage>();
            TbSecUserSecurityProfile = new HashSet<TbSecUserSecurityProfile>();
        }

        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public byte[] Avatar { get; set; }
        public DateTime? LastActive { get; set; }
        public string ResetPwdToken { get; set; }
        public DateTimeOffset? ResetPwdExpiryDt { get; set; }
        public string StatusCd { get; set; }
        public int? ClientId { get; set; }
        public int? EntityId { get; set; }
        public int? EmployeeId { get; set; }
        public DateTime? CreatedDt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDt { get; set; }
        public string ModifiedBy { get; set; }
        public bool? EssAccessFlag { get; set; }
        public DateTime? EssAccessStartDt { get; set; }
        public DateTime? EssAccessEndDt { get; set; }
        public string SingleSignOnId { get; set; }

        public virtual ICollection<TbMsgMessage> TbMsgMessage { get; set; }
        public virtual ICollection<TbSecUserSecurityProfile> TbSecUserSecurityProfile { get; set; }
    }
}
