﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Core
{
    public partial class TbSecRoleGroup
    {
        public TbSecRoleGroup()
        {
            TbSecRoleFunction = new HashSet<TbSecRoleFunction>();
            TbSecSecurityProfileDetail = new HashSet<TbSecSecurityProfileDetail>();
        }

        public int RoleGroupId { get; set; }
        public int SpecificClientId { get; set; }
        public int SpecificEntityId { get; set; }
        public string RoleGroupNameTxt { get; set; }
        public string RoleGroupDescriptionTxt { get; set; }
        public bool BundleGroupFlag { get; set; }
        public string CreateBy { get; set; }
        public DateTimeOffset CreateDt { get; set; }
        public string ModifyBy { get; set; }
        public DateTimeOffset ModifyDt { get; set; }

        public virtual ICollection<TbSecRoleFunction> TbSecRoleFunction { get; set; }
        public virtual ICollection<TbSecSecurityProfileDetail> TbSecSecurityProfileDetail { get; set; }
    }
}
