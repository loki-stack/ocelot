﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace CommonLib.Models.Core
{
    public partial class TbCamClient
    {
        public TbCamClient()
        {
            TbCamEntity = new HashSet<TbCamEntity>();
        }

        public int ClientId { get; set; }
        public HierarchyId ClientLevel { get; set; }
        public string ClientCode { get; set; }
        public string InternalCode { get; set; }
        public string ClientNameTxt { get; set; }
        public string ContactPerson { get; set; }
        public string Remarks { get; set; }
        public string StatusCd { get; set; }
        public DateTimeOffset CreatedDt { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset ModifiedDt { get; set; }
        public string ModifiedBy { get; set; }

        public virtual ICollection<TbCamEntity> TbCamEntity { get; set; }
    }
}
