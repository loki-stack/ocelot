﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CommonLib.Models.Core
{
    public partial class CoreBasicDbContext : DbContext
    {
        public CoreBasicDbContext()
        {
        }

        public CoreBasicDbContext(DbContextOptions<CoreBasicDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TbCamClient> TbCamClient { get; set; }
        public virtual DbSet<TbCamEntity> TbCamEntity { get; set; }
        public virtual DbSet<TbCamEntityFaq> TbCamEntityFaq { get; set; }
        public virtual DbSet<TbCamEntityFile> TbCamEntityFile { get; set; }
        public virtual DbSet<TbCamEntitySupport> TbCamEntitySupport { get; set; }
        public virtual DbSet<TbCfgGenericCode> TbCfgGenericCode { get; set; }
        public virtual DbSet<TbCfgGlobalCalendar> TbCfgGlobalCalendar { get; set; }
        public virtual DbSet<TbCfgHoliday> TbCfgHoliday { get; set; }
        public virtual DbSet<TbCfgI18n> TbCfgI18n { get; set; }
        public virtual DbSet<TbCfgSetting> TbCfgSetting { get; set; }
        public virtual DbSet<TbMsgMessage> TbMsgMessage { get; set; }
        public virtual DbSet<TbMsgTemplate> TbMsgTemplate { get; set; }
        public virtual DbSet<TbMsgTemplateLang> TbMsgTemplateLang { get; set; }
        public virtual DbSet<TbPayFormulas> TbPayFormulas { get; set; }
        public virtual DbSet<TbPayFormulasTag> TbPayFormulasTag { get; set; }
        public virtual DbSet<TbPayMpfRegisteredScheme> TbPayMpfRegisteredScheme { get; set; }
        public virtual DbSet<TbPayMpfTrustee> TbPayMpfTrustee { get; set; }
        public virtual DbSet<TbSecDataAccessClientEntity> TbSecDataAccessClientEntity { get; set; }
        public virtual DbSet<TbSecDataAccessGrade> TbSecDataAccessGrade { get; set; }
        public virtual DbSet<TbSecDataAccessGroup> TbSecDataAccessGroup { get; set; }
        public virtual DbSet<TbSecDataAccessOrganizationStructure> TbSecDataAccessOrganizationStructure { get; set; }
        public virtual DbSet<TbSecDataAccessRemunerationPackage> TbSecDataAccessRemunerationPackage { get; set; }
        public virtual DbSet<TbSecFunction> TbSecFunction { get; set; }
        public virtual DbSet<TbSecRefreshToken> TbSecRefreshToken { get; set; }
        public virtual DbSet<TbSecRoleFunction> TbSecRoleFunction { get; set; }
        public virtual DbSet<TbSecRoleGroup> TbSecRoleGroup { get; set; }
        public virtual DbSet<TbSecSecurityProfile> TbSecSecurityProfile { get; set; }
        public virtual DbSet<TbSecSecurityProfileDetail> TbSecSecurityProfileDetail { get; set; }
        public virtual DbSet<TbSecUser> TbSecUser { get; set; }
        public virtual DbSet<TbSecUserSecurityProfile> TbSecUserSecurityProfile { get; set; }
        public virtual DbSet<VwSecFarDarFlatten> VwSecFarDarFlatten { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=hris-dev-sqlserver.database.windows.net;Database=DEV_HRIS_CORE;User ID=hrisdev;Password=N!4LhRc$gK?=cS5-;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;", x => x.UseHierarchyId());
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TbCamClient>(entity =>
            {
                entity.HasKey(e => e.ClientId);

                entity.ToTable("TB_CAM_CLIENT");

                entity.Property(e => e.ClientId).HasColumnName("CLIENT_ID");

                entity.Property(e => e.ClientCode)
                    .IsRequired()
                    .HasColumnName("CLIENT_CODE")
                    .HasMaxLength(50);

                entity.Property(e => e.ClientLevel).HasColumnName("CLIENT_LEVEL");

                entity.Property(e => e.ClientNameTxt)
                    .IsRequired()
                    .HasColumnName("CLIENT_NAME_TXT")
                    .HasMaxLength(200);

                entity.Property(e => e.ContactPerson)
                    .IsRequired()
                    .HasColumnName("CONTACT_PERSON")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(150);

                entity.Property(e => e.CreatedDt)
                    .HasColumnName("CREATED_DT")
                    .HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.InternalCode)
                    .HasColumnName("INTERNAL_CODE")
                    .HasMaxLength(50);

                entity.Property(e => e.ModifiedBy)
                    .IsRequired()
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(150);

                entity.Property(e => e.ModifiedDt)
                    .HasColumnName("MODIFIED_DT")
                    .HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.Remarks).HasColumnName("REMARKS");

                entity.Property(e => e.StatusCd)
                    .IsRequired()
                    .HasColumnName("STATUS_CD")
                    .HasMaxLength(41);
            });

            modelBuilder.Entity<TbCamEntity>(entity =>
            {
                entity.HasKey(e => e.EntityId);

                entity.ToTable("TB_CAM_ENTITY");

                entity.HasIndex(e => e.ParentClientId);

                entity.Property(e => e.EntityId).HasColumnName("ENTITY_ID");

                entity.Property(e => e.BusinessAddressLine1)
                    .IsRequired()
                    .HasColumnName("BUSINESS_ADDRESS_LINE_1")
                    .HasMaxLength(30)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.BusinessAddressLine1Secondary)
                    .HasColumnName("BUSINESS_ADDRESS_LINE_1_SECONDARY")
                    .HasMaxLength(200);

                entity.Property(e => e.BusinessAddressLine2)
                    .HasColumnName("BUSINESS_ADDRESS_LINE_2")
                    .HasMaxLength(30);

                entity.Property(e => e.BusinessAddressLine2Secondary)
                    .HasColumnName("BUSINESS_ADDRESS_LINE_2_SECONDARY")
                    .HasMaxLength(200);

                entity.Property(e => e.BusinessAddressLine3)
                    .HasColumnName("BUSINESS_ADDRESS_LINE_3")
                    .HasMaxLength(30);

                entity.Property(e => e.BusinessAddressLine3Secondary)
                    .HasColumnName("BUSINESS_ADDRESS_LINE_3_SECONDARY")
                    .HasMaxLength(200);

                entity.Property(e => e.BusinessFax)
                    .IsRequired()
                    .HasColumnName("BUSINESS_FAX")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.BusinessPhone)
                    .IsRequired()
                    .HasColumnName("BUSINESS_PHONE")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.BusinessPhoneAreaCd)
                    .IsRequired()
                    .HasColumnName("BUSINESS_PHONE_AREA_CD")
                    .HasMaxLength(41)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.BusinessPhoneFaxAreaCd)
                    .IsRequired()
                    .HasColumnName("BUSINESS_PHONE_FAX_AREA_CD")
                    .HasMaxLength(41)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ConnStringParam)
                    .IsRequired()
                    .HasColumnName("CONN_STRING_PARAM")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ContactAddressLine1)
                    .IsRequired()
                    .HasColumnName("CONTACT_ADDRESS_LINE_1")
                    .HasMaxLength(30)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ContactAddressLine1Secondary)
                    .HasColumnName("CONTACT_ADDRESS_LINE_1_SECONDARY")
                    .HasMaxLength(200);

                entity.Property(e => e.ContactAddressLine2)
                    .HasColumnName("CONTACT_ADDRESS_LINE_2")
                    .HasMaxLength(30)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ContactAddressLine2Secondary)
                    .HasColumnName("CONTACT_ADDRESS_LINE_2_SECONDARY")
                    .HasMaxLength(200);

                entity.Property(e => e.ContactAddressLine3)
                    .HasColumnName("CONTACT_ADDRESS_LINE_3")
                    .HasMaxLength(30)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ContactAddressLine3Secondary)
                    .HasColumnName("CONTACT_ADDRESS_LINE_3_SECONDARY")
                    .HasMaxLength(200);

                entity.Property(e => e.ContactEmail)
                    .IsRequired()
                    .HasColumnName("CONTACT_EMAIL")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ContactFax)
                    .IsRequired()
                    .HasColumnName("CONTACT_FAX")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ContactFaxAreaCd)
                    .IsRequired()
                    .HasColumnName("CONTACT_FAX_AREA_CD")
                    .HasMaxLength(41)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ContactName)
                    .HasColumnName("CONTACT_NAME")
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ContactPhone)
                    .IsRequired()
                    .HasColumnName("CONTACT_PHONE")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ContactPhoneAreaCd)
                    .IsRequired()
                    .HasColumnName("CONTACT_PHONE_AREA_CD")
                    .HasMaxLength(41)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.CountryCd)
                    .IsRequired()
                    .HasColumnName("COUNTRY_CD")
                    .HasMaxLength(41);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(150);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.DbHost)
                    .IsRequired()
                    .HasColumnName("DB_HOST")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.DbName)
                    .IsRequired()
                    .HasColumnName("DB_NAME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.DbPort).HasColumnName("DB_PORT");

                entity.Property(e => e.EntityCode)
                    .IsRequired()
                    .HasColumnName("ENTITY_CODE")
                    .HasMaxLength(50);

                entity.Property(e => e.EntityDisplayName)
                    .IsRequired()
                    .HasColumnName("ENTITY_DISPLAY_NAME")
                    .HasMaxLength(200);

                entity.Property(e => e.EntityDisplayNameSecondary)
                    .HasColumnName("ENTITY_DISPLAY_NAME_SECONDARY")
                    .HasMaxLength(200);

                entity.Property(e => e.EntityLevel).HasColumnName("ENTITY_LEVEL");

                entity.Property(e => e.EntityNameTxt)
                    .IsRequired()
                    .HasColumnName("ENTITY_NAME_TXT")
                    .HasMaxLength(200);

                entity.Property(e => e.EntityNameTxtSecondary)
                    .HasColumnName("ENTITY_NAME_TXT_SECONDARY")
                    .HasMaxLength(200);

                entity.Property(e => e.InternalCode)
                    .HasColumnName("INTERNAL_CODE")
                    .HasMaxLength(50);

                entity.Property(e => e.ModifiedBy)
                    .IsRequired()
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(150);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.ParentClientId).HasColumnName("PARENT_CLIENT_ID");

                entity.Property(e => e.Remarks).HasColumnName("REMARKS");

                entity.Property(e => e.StatusCd)
                    .IsRequired()
                    .HasColumnName("STATUS_CD")
                    .HasMaxLength(41);

                entity.Property(e => e.TaxId)
                    .IsRequired()
                    .HasColumnName("TAX_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.HasOne(d => d.ParentClient)
                    .WithMany(p => p.TbCamEntity)
                    .HasForeignKey(d => d.ParentClientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_CAM_ENTITY_TB_CAM_CLIENT");
            });

            modelBuilder.Entity<TbCamEntityFaq>(entity =>
            {
                entity.HasKey(e => e.FaqId);

                entity.ToTable("TB_CAM_ENTITY_FAQ");

                entity.Property(e => e.FaqId).HasColumnName("FAQ_ID");

                entity.Property(e => e.Answer)
                    .HasColumnName("ANSWER")
                    .HasMaxLength(500);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.EntityId).HasColumnName("ENTITY_ID");

                entity.Property(e => e.FaqOrder).HasColumnName("FAQ_ORDER");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.Question)
                    .HasColumnName("QUESTION")
                    .HasMaxLength(500);

                entity.HasOne(d => d.Entity)
                    .WithMany(p => p.TbCamEntityFaq)
                    .HasForeignKey(d => d.EntityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_CAM_ENTITY_FAQ_TB_CAM_ENTITY");
            });

            modelBuilder.Entity<TbCamEntityFile>(entity =>
            {
                entity.HasKey(e => e.CamEntityFileId);

                entity.ToTable("TB_CAM_ENTITY_FILE");

                entity.HasIndex(e => e.EntityId);

                entity.Property(e => e.CamEntityFileId).HasColumnName("CAM_ENTITY_FILE_ID");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.EntityId).HasColumnName("ENTITY_ID");

                entity.Property(e => e.FileCategory)
                    .IsRequired()
                    .HasColumnName("FILE_CATEGORY")
                    .HasMaxLength(20)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.FileContent)
                    .IsRequired()
                    .HasColumnName("FILE_CONTENT");

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasColumnName("FILE_NAME")
                    .HasMaxLength(200)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.FileRemark)
                    .IsRequired()
                    .HasColumnName("FILE_REMARK")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.IsDeleted).HasColumnName("IS_DELETED");

                entity.Property(e => e.ModifiedBy)
                    .IsRequired()
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.HasOne(d => d.Entity)
                    .WithMany(p => p.TbCamEntityFile)
                    .HasForeignKey(d => d.EntityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_CAM_ENTITY_FILE_TB_CAM_ENTITY");
            });

            modelBuilder.Entity<TbCamEntitySupport>(entity =>
            {
                entity.HasKey(e => e.SupportId);

                entity.ToTable("TB_CAM_ENTITY_SUPPORT");

                entity.Property(e => e.SupportId).HasColumnName("SUPPORT_ID");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.EntityId).HasColumnName("ENTITY_ID");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.Remark)
                    .HasColumnName("REMARK")
                    .HasMaxLength(500);

                entity.Property(e => e.SupportContactName)
                    .IsRequired()
                    .HasColumnName("SUPPORT_CONTACT_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.SupportEmail)
                    .HasColumnName("SUPPORT_EMAIL")
                    .HasMaxLength(100);

                entity.Property(e => e.SupportPhoneNumber)
                    .HasColumnName("SUPPORT_PHONE_NUMBER")
                    .HasMaxLength(50);

                entity.HasOne(d => d.Entity)
                    .WithMany(p => p.TbCamEntitySupport)
                    .HasForeignKey(d => d.EntityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_CAM_ENTITY_SUPPORT_TB_CAM_ENTITY");
            });

            modelBuilder.Entity<TbCfgGenericCode>(entity =>
            {
                entity.HasKey(e => e.CodeId);

                entity.ToTable("TB_CFG_GENERIC_CODE");

                entity.HasIndex(e => e.ParentCodeId);

                entity.HasIndex(e => new { e.SpecificClientId, e.ParentCodeId, e.CodeType, e.CodeValue })
                    .HasName("UC_TB_CFG_GENERIC_CODE")
                    .IsUnique()
                    .HasFilter("([PARENT_CODE_ID] IS NOT NULL)");

                entity.Property(e => e.CodeId).HasColumnName("CODE_ID");

                entity.Property(e => e.CodeLevel).HasColumnName("CODE_LEVEL");

                entity.Property(e => e.CodeType)
                    .IsRequired()
                    .HasColumnName("CODE_TYPE")
                    .HasMaxLength(20)
                    .HasComment("Compontent type of business code.");

                entity.Property(e => e.CodeValue)
                    .IsRequired()
                    .HasColumnName("CODE_VALUE")
                    .HasMaxLength(20)
                    .HasComment("Business code of compontent's option behind using in logic.");

                entity.Property(e => e.CreateBy)
                    .IsRequired()
                    .HasColumnName("CREATE_BY")
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('SYSTEM')");

                entity.Property(e => e.CreateDt)
                    .HasColumnName("CREATE_DT")
                    .HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.DisplayOrder).HasColumnName("DISPLAY_ORDER");

                entity.Property(e => e.I18nTextKey)
                    .IsRequired()
                    .HasColumnName("I18N_TEXT_KEY")
                    .HasMaxLength(41)
                    .HasComment("Language tag for UI display.")
                    .HasComputedColumnSql("(concat([CODE_TYPE],'.',[CODE_VALUE]))");

                entity.Property(e => e.ModifyBy)
                    .IsRequired()
                    .HasColumnName("MODIFY_BY")
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('SYSTEM')");

                entity.Property(e => e.ModifyDt)
                    .HasColumnName("MODIFY_DT")
                    .HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.ParentCodeId)
                    .HasColumnName("PARENT_CODE_ID")
                    .HasComment("Code id of parent.");

                entity.Property(e => e.Remark)
                    .IsRequired()
                    .HasColumnName("REMARK")
                    .HasMaxLength(255)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.SpecificClientId)
                    .HasColumnName("SPECIFIC_CLIENT_ID")
                    .HasComment("Client specific code.");

                entity.Property(e => e.SpecificCompanyId).HasColumnName("SPECIFIC_COMPANY_ID");

                entity.Property(e => e.StatusCd)
                    .IsRequired()
                    .HasColumnName("STATUS_CD")
                    .HasMaxLength(10)
                    .HasDefaultValueSql("(N'ACTIVE')")
                    .HasComment("Status of business code. [ACTIVE, READONLY, INACTIVE]");

                entity.HasOne(d => d.ParentCode)
                    .WithMany(p => p.InverseParentCode)
                    .HasForeignKey(d => d.ParentCodeId)
                    .HasConstraintName("FK_TB_CFG_GENERIC_CODE_TB_CFG_GENERIC_CODE");
            });

            modelBuilder.Entity<TbCfgGlobalCalendar>(entity =>
            {
                entity.HasKey(e => e.GlobalCalendarId)
                    .HasName("PK__TB_CAM_G__4B4C2DD1805FEBDF");

                entity.ToTable("TB_CFG_GLOBAL_CALENDAR");

                entity.Property(e => e.GlobalCalendarId).HasColumnName("GLOBAL_CALENDAR_ID");

                entity.Property(e => e.Country)
                    .HasColumnName("COUNTRY")
                    .HasMaxLength(20);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.GlobalLevelCalendarName)
                    .HasColumnName("GLOBAL_LEVEL_CALENDAR_NAME")
                    .HasMaxLength(200);

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.Status)
                    .HasColumnName("STATUS")
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<TbCfgHoliday>(entity =>
            {
                entity.HasKey(e => e.HolidayDateId)
                    .HasName("PK__TB_CFG_H__8186AB53B124B51E");

                entity.ToTable("TB_CFG_HOLIDAY");

                entity.Property(e => e.HolidayDateId).HasColumnName("HOLIDAY_DATE_ID");

                entity.Property(e => e.CalendarId).HasColumnName("CALENDAR_ID");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.Date)
                    .HasColumnName("DATE")
                    .HasColumnType("date");

                entity.Property(e => e.DescriptionPrimaryLanguage)
                    .HasColumnName("DESCRIPTION_PRIMARY_LANGUAGE")
                    .HasMaxLength(400);

                entity.Property(e => e.DescriptionSecondaryLanguage)
                    .HasColumnName("DESCRIPTION_SECONDARY_LANGUAGE")
                    .HasMaxLength(400);

                entity.Property(e => e.HolidayType)
                    .HasColumnName("HOLIDAY_TYPE")
                    .HasMaxLength(20);

                entity.Property(e => e.IsWorkingDay).HasColumnName("IS_WORKING_DAY");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.HasOne(d => d.Calendar)
                    .WithMany(p => p.TbCfgHoliday)
                    .HasForeignKey(d => d.CalendarId)
                    .HasConstraintName("FK_TB_CFG_HOLIDAY_TB_CAM_GLOBAL_CALENDAR");
            });

            modelBuilder.Entity<TbCfgI18n>(entity =>
            {
                entity.HasKey(e => new { e.SpecificClientId, e.SpecificEntityId, e.LanguageTag, e.TextKey });

                entity.ToTable("TB_CFG_I18N");

                entity.Property(e => e.SpecificClientId)
                    .HasColumnName("SPECIFIC_CLIENT_ID")
                    .HasComment("Client specific code.");

                entity.Property(e => e.SpecificEntityId).HasColumnName("SPECIFIC_ENTITY_ID");

                entity.Property(e => e.LanguageTag)
                    .HasColumnName("LANGUAGE_TAG")
                    .HasMaxLength(10)
                    .HasDefaultValueSql("('en')")
                    .HasComment("Language tag");

                entity.Property(e => e.TextKey)
                    .HasColumnName("TEXT_KEY")
                    .HasMaxLength(41)
                    .HasComment("Language tag for UI display.");

                entity.Property(e => e.CreateBy)
                    .IsRequired()
                    .HasColumnName("CREATE_BY")
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('SYSTEM')");

                entity.Property(e => e.CreateDt)
                    .HasColumnName("CREATE_DT")
                    .HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.DefaultFlag)
                    .IsRequired()
                    .HasColumnName("DEFAULT_FLAG")
                    .HasDefaultValueSql("((1))")
                    .HasComment("Is default displaying language if selected language tag not available for the options.");

                entity.Property(e => e.ModifyBy)
                    .IsRequired()
                    .HasColumnName("MODIFY_BY")
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('SYSTEM')");

                entity.Property(e => e.ModifyDt)
                    .HasColumnName("MODIFY_DT")
                    .HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.TextValue)
                    .IsRequired()
                    .HasColumnName("TEXT_VALUE")
                    .HasMaxLength(200)
                    .HasComment("Text to be display.");
            });

            modelBuilder.Entity<TbCfgSetting>(entity =>
            {
                entity.HasKey(e => e.SettingId);

                entity.ToTable("TB_CFG_SETTING");

                entity.Property(e => e.SettingId).HasColumnName("SETTING_ID");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.ModifiedBy)
                    .IsRequired()
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.NotificationEmail)
                    .HasColumnName("NOTIFICATION_EMAIL")
                    .HasMaxLength(255);

                entity.Property(e => e.PaymentCurrency)
                    .HasColumnName("PAYMENT_CURRENCY")
                    .HasMaxLength(20);

                entity.Property(e => e.PdfStatus).HasColumnName("PDF_STATUS");

                entity.Property(e => e.PrimaryLanguage)
                    .HasColumnName("PRIMARY_LANGUAGE")
                    .HasMaxLength(50);

                entity.Property(e => e.ProbationPeriod).HasColumnName("PROBATION_PERIOD");

                entity.Property(e => e.SalaryCurrency)
                    .HasColumnName("SALARY_CURRENCY")
                    .HasMaxLength(20);

                entity.Property(e => e.SecondaryLanguage)
                    .HasColumnName("SECONDARY_LANGUAGE")
                    .HasMaxLength(50);

                entity.Property(e => e.SignInMethod).HasColumnName("SIGN_IN_METHOD");

                entity.Property(e => e.SpecificClientId).HasColumnName("SPECIFIC_CLIENT_ID");

                entity.Property(e => e.SpecificEntityId).HasColumnName("SPECIFIC_ENTITY_ID");

                entity.Property(e => e.WorkCalendar).HasColumnName("WORK_CALENDAR");
            });

            modelBuilder.Entity<TbMsgMessage>(entity =>
            {
                entity.HasKey(e => e.MessageId);

                entity.ToTable("TB_MSG_MESSAGE");

                entity.Property(e => e.MessageId).HasColumnName("MESSAGE_ID");

                entity.Property(e => e.AttemptCount).HasColumnName("ATTEMPT_COUNT");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.FlagHtml).HasColumnName("FLAG_HTML");

                entity.Property(e => e.MessageContent)
                    .IsRequired()
                    .HasColumnName("MESSAGE_CONTENT");

                entity.Property(e => e.MessageTitle)
                    .IsRequired()
                    .HasColumnName("MESSAGE_TITLE")
                    .HasMaxLength(500);

                entity.Property(e => e.MessageType)
                    .IsRequired()
                    .HasColumnName("MESSAGE_TYPE")
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.Portal)
                    .IsRequired()
                    .HasColumnName("PORTAL")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ProcessedDt).HasColumnName("PROCESSED_DT");

                entity.Property(e => e.Recipient)
                    .IsRequired()
                    .HasColumnName("RECIPIENT")
                    .HasMaxLength(200);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnName("STATUS")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.TemplateLangId).HasColumnName("TEMPLATE_LANG_ID");

                entity.Property(e => e.UserId).HasColumnName("USER_ID");

                entity.HasOne(d => d.TemplateLang)
                    .WithMany(p => p.TbMsgMessage)
                    .HasForeignKey(d => d.TemplateLangId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_MSG_MESSAGE_TB_MSG_TEMPLATE_LANG");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TbMsgMessage)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_TB_MSG_MESSAGE_TB_SEC_USER");
            });

            modelBuilder.Entity<TbMsgTemplate>(entity =>
            {
                entity.HasKey(e => e.TemplateId);

                entity.ToTable("TB_MSG_TEMPLATE");

                entity.Property(e => e.TemplateId).HasColumnName("TEMPLATE_ID");

                entity.Property(e => e.ClientId).HasColumnName("CLIENT_ID");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.EntityId).HasColumnName("ENTITY_ID");

                entity.Property(e => e.FlagHtml).HasColumnName("FLAG_HTML");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.Portal)
                    .IsRequired()
                    .HasColumnName("PORTAL")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnName("STATUS")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.TemplateCode)
                    .IsRequired()
                    .HasColumnName("TEMPLATE_CODE")
                    .HasMaxLength(20);

                entity.Property(e => e.TemplateType)
                    .IsRequired()
                    .HasColumnName("TEMPLATE_TYPE")
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            modelBuilder.Entity<TbMsgTemplateLang>(entity =>
            {
                entity.HasKey(e => e.TemplateLangId);

                entity.ToTable("TB_MSG_TEMPLATE_LANG");

                entity.Property(e => e.TemplateLangId).HasColumnName("TEMPLATE_LANG_ID");

                entity.Property(e => e.Content)
                    .IsRequired()
                    .HasColumnName("CONTENT");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.LangCode)
                    .IsRequired()
                    .HasColumnName("LANG_CODE")
                    .HasMaxLength(10);

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnName("STATUS")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.TemplateId).HasColumnName("TEMPLATE_ID");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnName("TITLE")
                    .HasMaxLength(500);

                entity.HasOne(d => d.Template)
                    .WithMany(p => p.TbMsgTemplateLang)
                    .HasForeignKey(d => d.TemplateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_MSG_TEMPLATE_LANG_TB_MSG_TEMPLATE");
            });

            modelBuilder.Entity<TbPayFormulas>(entity =>
            {
                entity.HasKey(e => e.FormulasId)
                    .HasName("PK__TB_PAY_F__10768F6CAEEFE6EC");

                entity.ToTable("TB_PAY_FORMULAS");

                entity.Property(e => e.FormulasId).HasColumnName("FORMULAS_ID");

                entity.Property(e => e.ClientId).HasColumnName("CLIENT_ID");

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.Description).HasColumnName("DESCRIPTION");

                entity.Property(e => e.EditAble)
                    .HasColumnName("EDIT_ABLE")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.EntityId).HasColumnName("ENTITY_ID");

                entity.Property(e => e.Formulas).HasColumnName("FORMULAS");

                entity.Property(e => e.Level)
                    .HasColumnName("LEVEL")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.StatusCd)
                    .IsRequired()
                    .HasColumnName("STATUS_CD")
                    .HasMaxLength(10);
            });

            modelBuilder.Entity<TbPayFormulasTag>(entity =>
            {
                entity.HasKey(e => e.FormulasTagId)
                    .HasName("PK__TB_PAY_F__9E0991D1F4EA9826");

                entity.ToTable("TB_PAY_FORMULAS_TAG");

                entity.Property(e => e.FormulasTagId).HasColumnName("FORMULAS_TAG_ID");

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.FieldName)
                    .HasColumnName("FIELD_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.Flag).HasColumnName("FLAG");

                entity.Property(e => e.FormulasId).HasColumnName("FORMULAS_ID");

                entity.Property(e => e.FormulasRepeat).HasColumnName("FORMULAS_REPEAT");

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.Predefined)
                    .HasColumnName("PREDEFINED")
                    .HasMaxLength(100);

                entity.Property(e => e.Remark).HasColumnName("REMARK");

                entity.Property(e => e.TagName)
                    .HasColumnName("TAG_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.ValueType)
                    .HasColumnName("VALUE_TYPE")
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<TbPayMpfRegisteredScheme>(entity =>
            {
                entity.HasKey(e => e.MpfRegisteredSchemeId)
                    .HasName("PK_TB_EMP_MPF_REGISTED_SCHEME");

                entity.ToTable("TB_PAY_MPF_REGISTERED_SCHEME");

                entity.Property(e => e.MpfRegisteredSchemeId).HasColumnName("MPF_REGISTERED_SCHEME_ID");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.FinancialYearDate)
                    .HasColumnName("FINANCIAL_YEAR_DATE")
                    .HasMaxLength(20);

                entity.Property(e => e.InterfaceFileFormat)
                    .HasColumnName("INTERFACE_FILE_FORMAT")
                    .HasMaxLength(20);

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.ProrateRiRemittanceStatement).HasColumnName("PRORATE_RI_REMITTANCE_STATEMENT");

                entity.Property(e => e.RemittanceStatementFormat)
                    .HasColumnName("REMITTANCE_STATEMENT_FORMAT")
                    .HasMaxLength(20);

                entity.Property(e => e.SchemeNameCn)
                    .HasColumnName("SCHEME_NAME_CN")
                    .HasMaxLength(100);

                entity.Property(e => e.SchemeNameEn)
                    .HasColumnName("SCHEME_NAME_EN")
                    .HasMaxLength(100);

                entity.Property(e => e.TrusteeId).HasColumnName("TRUSTEE_ID");

                entity.Property(e => e.Type)
                    .HasColumnName("TYPE")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbPayMpfTrustee>(entity =>
            {
                entity.HasKey(e => e.TrusteeId)
                    .HasName("PK__TB_EMP_M__F15EB39CFB4571AD");

                entity.ToTable("TB_PAY_MPF_TRUSTEE");

                entity.Property(e => e.TrusteeId).HasColumnName("TRUSTEE_ID");

                entity.Property(e => e.Address)
                    .HasColumnName("ADDRESS")
                    .HasMaxLength(200);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.Description)
                    .HasColumnName("DESCRIPTION")
                    .HasMaxLength(100);

                entity.Property(e => e.EmailAddress)
                    .HasColumnName("EMAIL_ADDRESS")
                    .HasMaxLength(50);

                entity.Property(e => e.FaxNumber)
                    .HasColumnName("FAX_NUMBER")
                    .HasMaxLength(50);

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.PhoneNumber)
                    .HasColumnName("PHONE_NUMBER")
                    .HasMaxLength(50);

                entity.Property(e => e.TrusteeName)
                    .HasColumnName("TRUSTEE_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.Website)
                    .HasColumnName("WEBSITE")
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<TbSecDataAccessClientEntity>(entity =>
            {
                entity.HasKey(e => e.DataAccessClientEntityId);

                entity.ToTable("TB_SEC_DATA_ACCESS_CLIENT_ENTITY");

                entity.HasIndex(e => e.DataAccessGroupId);

                entity.Property(e => e.DataAccessClientEntityId).HasColumnName("DATA_ACCESS_CLIENT_ENTITY_ID");

                entity.Property(e => e.ClientLevel)
                    .IsRequired()
                    .HasColumnName("CLIENT_LEVEL");

                entity.Property(e => e.DataAccessGroupId).HasColumnName("DATA_ACCESS_GROUP_ID");

                entity.Property(e => e.EntityLevel)
                    .IsRequired()
                    .HasColumnName("ENTITY_LEVEL");

                entity.HasOne(d => d.DataAccessGroup)
                    .WithMany(p => p.TbSecDataAccessClientEntity)
                    .HasForeignKey(d => d.DataAccessGroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_SEC_DATA_ACCESS_CLIENT_ENTITY_TB_SEC_DATA_ACCESS_GROUP");
            });

            modelBuilder.Entity<TbSecDataAccessGrade>(entity =>
            {
                entity.HasKey(e => e.DataAccessGradeId);

                entity.ToTable("TB_SEC_DATA_ACCESS_GRADE");

                entity.HasIndex(e => e.DataAccessGroupId);

                entity.Property(e => e.DataAccessGradeId).HasColumnName("DATA_ACCESS_GRADE_ID");

                entity.Property(e => e.DataAccessGroupId).HasColumnName("DATA_ACCESS_GROUP_ID");

                entity.Property(e => e.MaxGradeLevelNum).HasColumnName("MAX_GRADE_LEVEL_NUM");

                entity.Property(e => e.MinGradeLevelNum).HasColumnName("MIN_GRADE_LEVEL_NUM");

                entity.HasOne(d => d.DataAccessGroup)
                    .WithMany(p => p.TbSecDataAccessGrade)
                    .HasForeignKey(d => d.DataAccessGroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_SEC_DATA_ACCESS_GRADE_TB_SEC_DATA_ACCESS_GROUP");
            });

            modelBuilder.Entity<TbSecDataAccessGroup>(entity =>
            {
                entity.HasKey(e => e.DataAccessGroupId);

                entity.ToTable("TB_SEC_DATA_ACCESS_GROUP");

                entity.Property(e => e.DataAccessGroupId).HasColumnName("DATA_ACCESS_GROUP_ID");

                entity.Property(e => e.CreateBy)
                    .IsRequired()
                    .HasColumnName("CREATE_BY")
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('SYSTEM')");

                entity.Property(e => e.CreateDt)
                    .HasColumnName("CREATE_DT")
                    .HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.DataAccessGroupDescriptionTxt)
                    .IsRequired()
                    .HasColumnName("DATA_ACCESS_GROUP_DESCRIPTION_TXT")
                    .HasMaxLength(200)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DataAccessGroupNameTxt)
                    .IsRequired()
                    .HasColumnName("DATA_ACCESS_GROUP_NAME_TXT")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifyBy)
                    .IsRequired()
                    .HasColumnName("MODIFY_BY")
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('SYSTEM')");

                entity.Property(e => e.ModifyDt)
                    .HasColumnName("MODIFY_DT")
                    .HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.SpecificClientId).HasColumnName("SPECIFIC_CLIENT_ID");

                entity.Property(e => e.SpecificCompanyId).HasColumnName("SPECIFIC_COMPANY_ID");
            });

            modelBuilder.Entity<TbSecDataAccessOrganizationStructure>(entity =>
            {
                entity.HasKey(e => new { e.DataAccessGroupId, e.NodeLevel });

                entity.ToTable("TB_SEC_DATA_ACCESS_ORGANIZATION_STRUCTURE");

                entity.Property(e => e.DataAccessGroupId).HasColumnName("DATA_ACCESS_GROUP_ID");

                entity.Property(e => e.NodeLevel).HasColumnName("NODE_LEVEL");

                entity.Property(e => e.IsSelectAllDescendants).HasColumnName("IS_SELECT_ALL_DESCENDANTS");
            });

            modelBuilder.Entity<TbSecDataAccessRemunerationPackage>(entity =>
            {
                entity.HasKey(e => new { e.DataAccessGroupId, e.RemunerationPackageId })
                    .HasName("PK_TB_SEC_DATA_ACCESS_REMUNERATION_PACKAGE_1");

                entity.ToTable("TB_SEC_DATA_ACCESS_REMUNERATION_PACKAGE");

                entity.Property(e => e.DataAccessGroupId).HasColumnName("DATA_ACCESS_GROUP_ID");

                entity.Property(e => e.RemunerationPackageId).HasColumnName("REMUNERATION_PACKAGE_ID");

                entity.HasOne(d => d.DataAccessGroup)
                    .WithMany(p => p.TbSecDataAccessRemunerationPackage)
                    .HasForeignKey(d => d.DataAccessGroupId)
                    .HasConstraintName("FK_TB_SEC_DATA_ACCESS_REMUNERATION_PACKAGE_TB_SEC_DATA_ACCESS_GROUP");
            });

            modelBuilder.Entity<TbSecFunction>(entity =>
            {
                entity.HasKey(e => e.FunctionId);

                entity.ToTable("TB_SEC_FUNCTION");

                entity.HasIndex(e => e.FunctionCd)
                    .HasName("UC_TB_SEC_FUNCTION")
                    .IsUnique();

                entity.Property(e => e.FunctionId).HasColumnName("FUNCTION_ID");

                entity.Property(e => e.CreateBy)
                    .IsRequired()
                    .HasColumnName("CREATE_BY")
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('SYSTEM')");

                entity.Property(e => e.CreateDt)
                    .HasColumnName("CREATE_DT")
                    .HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.FeatureCd)
                    .IsRequired()
                    .HasColumnName("FEATURE_CD")
                    .HasMaxLength(20);

                entity.Property(e => e.FunctionCd)
                    .IsRequired()
                    .HasColumnName("FUNCTION_CD")
                    .HasMaxLength(20);

                entity.Property(e => e.MenuCd)
                    .IsRequired()
                    .HasColumnName("MENU_CD")
                    .HasMaxLength(20)
                    .HasComment("Menu: \"TOP\", \"LEFT\", \"RIGHT\", \"NIL\"");

                entity.Property(e => e.MenuDisplayOrder).HasColumnName("MENU_DISPLAY_ORDER");

                entity.Property(e => e.ModifyBy)
                    .IsRequired()
                    .HasColumnName("MODIFY_BY")
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('SYSTEM')");

                entity.Property(e => e.ModifyDt)
                    .HasColumnName("MODIFY_DT")
                    .HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.ModuleCd)
                    .IsRequired()
                    .HasColumnName("MODULE_CD")
                    .HasMaxLength(20);

                entity.Property(e => e.RoleCd)
                    .IsRequired()
                    .HasColumnName("ROLE_CD")
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<TbSecRefreshToken>(entity =>
            {
                entity.HasKey(e => e.Token);

                entity.ToTable("TB_SEC_REFRESH_TOKEN");

                entity.Property(e => e.Token)
                    .HasColumnName("TOKEN")
                    .HasMaxLength(100);

                entity.Property(e => e.CreationDate)
                    .HasColumnName("CREATION_DATE")
                    .HasColumnType("datetime");

                entity.Property(e => e.ExpiryDate)
                    .HasColumnName("EXPIRY_DATE")
                    .HasColumnType("datetime");

                entity.Property(e => e.Invalidated).HasColumnName("INVALIDATED");

                entity.Property(e => e.JwtId)
                    .HasColumnName("JWT_ID")
                    .HasMaxLength(50);

                entity.Property(e => e.Used).HasColumnName("USED");

                entity.Property(e => e.UserId).HasColumnName("USER_ID");
            });

            modelBuilder.Entity<TbSecRoleFunction>(entity =>
            {
                entity.HasKey(e => new { e.RoleGroupId, e.FunctionId });

                entity.ToTable("TB_SEC_ROLE_FUNCTION");

                entity.HasIndex(e => e.FunctionId);

                entity.Property(e => e.RoleGroupId).HasColumnName("ROLE_GROUP_ID");

                entity.Property(e => e.FunctionId).HasColumnName("FUNCTION_ID");

                entity.HasOne(d => d.Function)
                    .WithMany(p => p.TbSecRoleFunction)
                    .HasForeignKey(d => d.FunctionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_SEC_ROLE_FUNCTION_TB_SEC_FUNCTION");

                entity.HasOne(d => d.RoleGroup)
                    .WithMany(p => p.TbSecRoleFunction)
                    .HasForeignKey(d => d.RoleGroupId)
                    .HasConstraintName("FK_TB_SEC_ROLE_FUNCTION_TB_SEC_ROLE_GROUP");
            });

            modelBuilder.Entity<TbSecRoleGroup>(entity =>
            {
                entity.HasKey(e => e.RoleGroupId);

                entity.ToTable("TB_SEC_ROLE_GROUP");

                entity.Property(e => e.RoleGroupId).HasColumnName("ROLE_GROUP_ID");

                entity.Property(e => e.BundleGroupFlag).HasColumnName("BUNDLE_GROUP_FLAG");

                entity.Property(e => e.CreateBy)
                    .IsRequired()
                    .HasColumnName("CREATE_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreateDt)
                    .HasColumnName("CREATE_DT")
                    .HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.ModifyBy)
                    .IsRequired()
                    .HasColumnName("MODIFY_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifyDt)
                    .HasColumnName("MODIFY_DT")
                    .HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.RoleGroupDescriptionTxt)
                    .IsRequired()
                    .HasColumnName("ROLE_GROUP_DESCRIPTION_TXT")
                    .HasMaxLength(200)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.RoleGroupNameTxt)
                    .IsRequired()
                    .HasColumnName("ROLE_GROUP_NAME_TXT")
                    .HasMaxLength(100);

                entity.Property(e => e.SpecificClientId).HasColumnName("SPECIFIC_CLIENT_ID");

                entity.Property(e => e.SpecificEntityId).HasColumnName("SPECIFIC_ENTITY_ID");
            });

            modelBuilder.Entity<TbSecSecurityProfile>(entity =>
            {
                entity.HasKey(e => e.SecurityProfileId);

                entity.ToTable("TB_SEC_SECURITY_PROFILE");

                entity.Property(e => e.SecurityProfileId).HasColumnName("SECURITY_PROFILE_ID");

                entity.Property(e => e.CreateBy)
                    .IsRequired()
                    .HasColumnName("CREATE_BY")
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('SYSTEM')");

                entity.Property(e => e.CreateDt)
                    .HasColumnName("CREATE_DT")
                    .HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.ModifyBy)
                    .IsRequired()
                    .HasColumnName("MODIFY_BY")
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('SYSTEM')");

                entity.Property(e => e.ModifyDt)
                    .HasColumnName("MODIFY_DT")
                    .HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.SecurityProfileDescriptionTxt)
                    .IsRequired()
                    .HasColumnName("SECURITY_PROFILE_DESCRIPTION_TXT")
                    .HasMaxLength(200)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.SecurityProfileNameTxt)
                    .IsRequired()
                    .HasColumnName("SECURITY_PROFILE_NAME_TXT")
                    .HasMaxLength(100);

                entity.Property(e => e.SpecificClientId).HasColumnName("SPECIFIC_CLIENT_ID");

                entity.Property(e => e.SpecificEntityId).HasColumnName("SPECIFIC_ENTITY_ID");
            });

            modelBuilder.Entity<TbSecSecurityProfileDetail>(entity =>
            {
                entity.HasKey(e => new { e.SecurityProfileId, e.RoleGroupId, e.DataAccessGroupId });

                entity.ToTable("TB_SEC_SECURITY_PROFILE_DETAIL");

                entity.HasIndex(e => e.DataAccessGroupId);

                entity.HasIndex(e => e.RoleGroupId);

                entity.Property(e => e.SecurityProfileId).HasColumnName("SECURITY_PROFILE_ID");

                entity.Property(e => e.RoleGroupId).HasColumnName("ROLE_GROUP_ID");

                entity.Property(e => e.DataAccessGroupId).HasColumnName("DATA_ACCESS_GROUP_ID");

                entity.HasOne(d => d.DataAccessGroup)
                    .WithMany(p => p.TbSecSecurityProfileDetail)
                    .HasForeignKey(d => d.DataAccessGroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_SEC_SECURITY_PROFILE_DETAIL_TB_SEC_DATA_ACCESS_GROUP");

                entity.HasOne(d => d.RoleGroup)
                    .WithMany(p => p.TbSecSecurityProfileDetail)
                    .HasForeignKey(d => d.RoleGroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_SEC_SECURITY_PROFILE_DETAIL_TB_SEC_ROLE_GROUP");
            });

            modelBuilder.Entity<TbSecUser>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.ToTable("TB_SEC_USER");

                entity.HasIndex(e => new { e.ClientId, e.EntityId, e.UserName })
                    .HasName("UC_TB_SEC_USER")
                    .IsUnique();

                entity.Property(e => e.UserId).HasColumnName("USER_ID");

                entity.Property(e => e.Avatar).HasColumnName("AVATAR");

                entity.Property(e => e.ClientId)
                    .HasColumnName("CLIENT_ID")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(20);

                entity.Property(e => e.CreatedDt)
                    .HasColumnName("CREATED_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .HasColumnName("EMAIL")
                    .HasMaxLength(255);

                entity.Property(e => e.EmployeeId).HasColumnName("EMPLOYEE_ID");

                entity.Property(e => e.EntityId)
                    .HasColumnName("ENTITY_ID")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.EssAccessEndDt)
                    .HasColumnName("ESS_ACCESS_END_DT")
                    .HasColumnType("date");

                entity.Property(e => e.EssAccessFlag).HasColumnName("ESS_ACCESS_FLAG");

                entity.Property(e => e.EssAccessStartDt)
                    .HasColumnName("ESS_ACCESS_START_DT")
                    .HasColumnType("date");

                entity.Property(e => e.FullName)
                    .HasColumnName("FULL_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.LastActive)
                    .HasColumnName("LAST_ACTIVE")
                    .HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(20);

                entity.Property(e => e.ModifiedDt)
                    .HasColumnName("MODIFIED_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("PASSWORD");

                entity.Property(e => e.ResetPwdExpiryDt).HasColumnName("RESET_PWD_EXPIRY_DT");

                entity.Property(e => e.ResetPwdToken)
                    .HasColumnName("RESET_PWD_TOKEN")
                    .HasMaxLength(100);

                entity.Property(e => e.SingleSignOnId)
                    .HasColumnName("SINGLE_SIGN_ON_ID")
                    .HasMaxLength(20);

                entity.Property(e => e.StatusCd)
                    .HasColumnName("STATUS_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasColumnName("USER_NAME")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbSecUserSecurityProfile>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.SecurityProfileId });

                entity.ToTable("TB_SEC_USER_SECURITY_PROFILE");

                entity.HasIndex(e => e.SecurityProfileId);

                entity.Property(e => e.UserId).HasColumnName("USER_ID");

                entity.Property(e => e.SecurityProfileId).HasColumnName("SECURITY_PROFILE_ID");

                entity.Property(e => e.EffectiveEndDt).HasColumnName("EFFECTIVE_END_DT");

                entity.Property(e => e.EffectiveStartDt)
                    .HasColumnName("EFFECTIVE_START_DT")
                    .HasDefaultValueSql("('9999-12-31')");

                entity.HasOne(d => d.SecurityProfile)
                    .WithMany(p => p.TbSecUserSecurityProfile)
                    .HasForeignKey(d => d.SecurityProfileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_SEC_USER_SECURITY_PROFILE_TB_SEC_SECURITY_PROFILE");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TbSecUserSecurityProfile)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_SEC_USER_SECURITY_PROFILE_TB_SEC_USER");
            });

            modelBuilder.Entity<VwSecFarDarFlatten>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VW_SEC_FAR_DAR_FLATTEN");

                entity.Property(e => e.ClientLevel)
                    .HasColumnName("CLIENT_LEVEL")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DataAccessGroupId).HasColumnName("DATA_ACCESS_GROUP_ID");

                entity.Property(e => e.DataAccessGroupNameTxt)
                    .HasColumnName("DATA_ACCESS_GROUP_NAME_TXT")
                    .HasMaxLength(100);

                entity.Property(e => e.EffectiveEndDt).HasColumnName("EFFECTIVE_END_DT");

                entity.Property(e => e.EffectiveStartDt).HasColumnName("EFFECTIVE_START_DT");

                entity.Property(e => e.EntityLevel)
                    .HasColumnName("ENTITY_LEVEL")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FunctionId).HasColumnName("FUNCTION_ID");

                entity.Property(e => e.IsSelectAllDescendants).HasColumnName("IS_SELECT_ALL_DESCENDANTS");

                entity.Property(e => e.MaxGradeLevelNum).HasColumnName("MAX_GRADE_LEVEL_NUM");

                entity.Property(e => e.MinGradeLevelNum).HasColumnName("MIN_GRADE_LEVEL_NUM");

                entity.Property(e => e.NodeLevel)
                    .HasColumnName("NODE_LEVEL")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RemunerationPackageId).HasColumnName("REMUNERATION_PACKAGE_ID");

                entity.Property(e => e.RoleGroupId).HasColumnName("ROLE_GROUP_ID");

                entity.Property(e => e.RoleGroupNameTxt)
                    .HasColumnName("ROLE_GROUP_NAME_TXT")
                    .HasMaxLength(100);

                entity.Property(e => e.SecurityProfileId).HasColumnName("SECURITY_PROFILE_ID");

                entity.Property(e => e.SecurityProfileNameTxt)
                    .HasColumnName("SECURITY_PROFILE_NAME_TXT")
                    .HasMaxLength(100);

                entity.Property(e => e.SpecificClientId).HasColumnName("SPECIFIC_CLIENT_ID");

                entity.Property(e => e.SpecificCompanyId).HasColumnName("SPECIFIC_COMPANY_ID");

                entity.Property(e => e.SpecificEntityId).HasColumnName("SPECIFIC_ENTITY_ID");

                entity.Property(e => e.UserId).HasColumnName("USER_ID");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
