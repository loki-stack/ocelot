﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Core
{
    public partial class TbSecSecurityProfile
    {
        public TbSecSecurityProfile()
        {
            TbSecUserSecurityProfile = new HashSet<TbSecUserSecurityProfile>();
        }

        public int SecurityProfileId { get; set; }
        public string SecurityProfileNameTxt { get; set; }
        public string CreateBy { get; set; }
        public DateTimeOffset CreateDt { get; set; }
        public string ModifyBy { get; set; }
        public DateTimeOffset ModifyDt { get; set; }
        public int SpecificClientId { get; set; }
        public int SpecificEntityId { get; set; }
        public string SecurityProfileDescriptionTxt { get; set; }

        public virtual ICollection<TbSecUserSecurityProfile> TbSecUserSecurityProfile { get; set; }
    }
}
