﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbPayGlCode
    {
        public TbPayGlCode()
        {
            TbPayMasterPayrollItem = new HashSet<TbPayMasterPayrollItem>();
        }

        public int GlCodeId { get; set; }
        public string GlCode { get; set; }
        public string Currency { get; set; }
        public string AccountType { get; set; }
        public string Remark { get; set; }
        public string StatusCd { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
        public string ModifiedBy { get; set; }

        public virtual ICollection<TbPayMasterPayrollItem> TbPayMasterPayrollItem { get; set; }
    }
}
