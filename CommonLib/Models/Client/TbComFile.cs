﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbComFile
    {
        public TbComFile()
        {
            TbEmpPayslip = new HashSet<TbEmpPayslip>();
            TbEmpTaxReturn = new HashSet<TbEmpTaxReturn>();
        }

        public int FileId { get; set; }
        public string Category { get; set; }
        public string Name { get; set; }
        public byte[] Content { get; set; }
        public bool IsDeleted { get; set; }
        public string Remark { get; set; }
        public DateTimeOffset CreatedDt { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset ModifiedDt { get; set; }
        public string ModifiedBy { get; set; }

        public virtual ICollection<TbEmpPayslip> TbEmpPayslip { get; set; }
        public virtual ICollection<TbEmpTaxReturn> TbEmpTaxReturn { get; set; }
    }
}
