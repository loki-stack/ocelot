﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbCfgCostCenter
    {
        public int CostCenterId { get; set; }
        public string CostCenterCd { get; set; }
        public string CostCenterName { get; set; }
        public string StatusCd { get; set; }
        public string Remark { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
    }
}
