﻿using CommonLib.Utils;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;

namespace CommonLib.Models.Client
{
    public partial class ClientBasicDBContext : DbContext
    {
        public ClientBasicDBContext(string connectionStr) : base(DbContextUtil.CreateDDRConnection<ClientBasicDBContext>(connectionStr))
        {
        }
        public ClientBasicDBContext([NotNullAttribute] DbContextOptions options) : base(options)
        {

        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder)
        {
            OnModelCreatingExtention(modelBuilder);
        }

        public virtual void OnModelCreatingExtention(ModelBuilder modelBuilder)
        {

        }
    }
}