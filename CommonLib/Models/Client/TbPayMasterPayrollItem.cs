﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbPayMasterPayrollItem
    {
        public TbPayMasterPayrollItem()
        {
            TbPayMasterPayrollItemFlag = new HashSet<TbPayMasterPayrollItemFlag>();
            TbPayMasterPayrollItemTax = new HashSet<TbPayMasterPayrollItemTax>();
            TbPayPayrollItem = new HashSet<TbPayPayrollItem>();
            TbPayPayrollRunTransaction = new HashSet<TbPayPayrollRunTransaction>();
        }

        public int PayrollItemId { get; set; }
        public int ClientId { get; set; }
        public int EntityId { get; set; }
        public string ItemName { get; set; }
        public string ItemCode { get; set; }
        public byte PaymentSign { get; set; }
        public string Remark { get; set; }
        public string StatusCd { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
        public string ModifiedBy { get; set; }
        public int? GlCodeId { get; set; }

        public virtual TbPayGlCode GlCode { get; set; }
        public virtual ICollection<TbPayMasterPayrollItemFlag> TbPayMasterPayrollItemFlag { get; set; }
        public virtual ICollection<TbPayMasterPayrollItemTax> TbPayMasterPayrollItemTax { get; set; }
        public virtual ICollection<TbPayPayrollItem> TbPayPayrollItem { get; set; }
        public virtual ICollection<TbPayPayrollRunTransaction> TbPayPayrollRunTransaction { get; set; }
    }
}
