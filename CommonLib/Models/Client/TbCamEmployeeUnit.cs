﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbCamEmployeeUnit
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public int UnitId { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
    }
}
