﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbEmpEeBankAccount
    {
        public int Id { get; set; }
        public string EeBankAccountId { get; set; }
        public int? EmployeeId { get; set; }
        public string AccountName { get; set; }
        public string AccountCurrency { get; set; }
        public string BankCode { get; set; }
        public string BranchCode { get; set; }
        public string AccountNo { get; set; }
        public string TransactionReference { get; set; }
        public bool? DefaultAccount { get; set; }
        public string Remark { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }

        public virtual TbEmpEmployee Employee { get; set; }
    }
}
