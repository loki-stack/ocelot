﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbEmpPayslip
    {
        public int PayslipId { get; set; }
        public int EmployeeId { get; set; }
        public int FileId { get; set; }
        public string PayslipDescription { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
        public DateTime? PayslipDt { get; set; }

        public virtual TbComFile File { get; set; }
    }
}
