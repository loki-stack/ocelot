﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CommonLib.Models.Client
{
    public partial class ClientBasicDBContext : DbContext
    {
        public ClientBasicDBContext()
        {
        }

        public ClientBasicDBContext(DbContextOptions<ClientBasicDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TbCamEmployeeUnit> TbCamEmployeeUnit { get; set; }
        public virtual DbSet<TbCamHoliday> TbCamHoliday { get; set; }
        public virtual DbSet<TbCamUnit> TbCamUnit { get; set; }
        public virtual DbSet<TbCamWorkCalendar> TbCamWorkCalendar { get; set; }
        public virtual DbSet<TbCfgCostCenter> TbCfgCostCenter { get; set; }
        public virtual DbSet<TbCfgI18n> TbCfgI18n { get; set; }
        public virtual DbSet<TbCfgJobGrade> TbCfgJobGrade { get; set; }
        public virtual DbSet<TbCfgWorkingSchedule> TbCfgWorkingSchedule { get; set; }
        public virtual DbSet<TbComFile> TbComFile { get; set; }
        public virtual DbSet<TbEmpAttachment> TbEmpAttachment { get; set; }
        public virtual DbSet<TbEmpContractStatus> TbEmpContractStatus { get; set; }
        public virtual DbSet<TbEmpCostAllocation> TbEmpCostAllocation { get; set; }
        public virtual DbSet<TbEmpCostBreakdown> TbEmpCostBreakdown { get; set; }
        public virtual DbSet<TbEmpDependent> TbEmpDependent { get; set; }
        public virtual DbSet<TbEmpEeBankAccount> TbEmpEeBankAccount { get; set; }
        public virtual DbSet<TbEmpEmergencyContactPerson> TbEmpEmergencyContactPerson { get; set; }
        public virtual DbSet<TbEmpEmployee> TbEmpEmployee { get; set; }
        public virtual DbSet<TbEmpEmploymentContract> TbEmpEmploymentContract { get; set; }
        public virtual DbSet<TbEmpEmploymentStatus> TbEmpEmploymentStatus { get; set; }
        public virtual DbSet<TbEmpMovement> TbEmpMovement { get; set; }
        public virtual DbSet<TbEmpPayslip> TbEmpPayslip { get; set; }
        public virtual DbSet<TbEmpPayslipPassword> TbEmpPayslipPassword { get; set; }
        public virtual DbSet<TbEmpRentalAddress> TbEmpRentalAddress { get; set; }
        public virtual DbSet<TbEmpTax> TbEmpTax { get; set; }
        public virtual DbSet<TbEmpTaxReturn> TbEmpTaxReturn { get; set; }
        public virtual DbSet<TbEmpTermination> TbEmpTermination { get; set; }
        public virtual DbSet<TbPayEnrolledScheme> TbPayEnrolledScheme { get; set; }
        public virtual DbSet<TbPayExchangeRate> TbPayExchangeRate { get; set; }
        public virtual DbSet<TbPayGlCode> TbPayGlCode { get; set; }
        public virtual DbSet<TbPayMasterPayrollItem> TbPayMasterPayrollItem { get; set; }
        public virtual DbSet<TbPayMasterPayrollItemFlag> TbPayMasterPayrollItemFlag { get; set; }
        public virtual DbSet<TbPayMasterPayrollItemTax> TbPayMasterPayrollItemTax { get; set; }
        public virtual DbSet<TbPayOrsoScheme> TbPayOrsoScheme { get; set; }
        public virtual DbSet<TbPayPayrollCycle> TbPayPayrollCycle { get; set; }
        public virtual DbSet<TbPayPayrollCycleSetting> TbPayPayrollCycleSetting { get; set; }
        public virtual DbSet<TbPayPayrollItem> TbPayPayrollItem { get; set; }
        public virtual DbSet<TbPayPayrollProfile> TbPayPayrollProfile { get; set; }
        public virtual DbSet<TbPayPayrollProfileEmployee> TbPayPayrollProfileEmployee { get; set; }
        public virtual DbSet<TbPayPayrollProfileItem> TbPayPayrollProfileItem { get; set; }
        public virtual DbSet<TbPayPayrollRun> TbPayPayrollRun { get; set; }
        public virtual DbSet<TbPayPayrollRunEmployee> TbPayPayrollRunEmployee { get; set; }
        public virtual DbSet<TbPayPayrollRunImport> TbPayPayrollRunImport { get; set; }
        public virtual DbSet<TbPayPayrollRunTransaction> TbPayPayrollRunTransaction { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=hris-dev-sqlserver.database.windows.net;Database=DEV_HRIS_CLIENT_1;User ID=hrisdev;Password=N!4LhRc$gK?=cS5-;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;", x => x.UseHierarchyId());
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TbCamEmployeeUnit>(entity =>
            {
                entity.ToTable("TB_CAM_EMPLOYEE_UNIT");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.EmployeeId).HasColumnName("EMPLOYEE_ID");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.UnitId).HasColumnName("UNIT_ID");
            });

            modelBuilder.Entity<TbCamHoliday>(entity =>
            {
                entity.HasKey(e => e.HolidayDateId)
                    .HasName("PK__TB_CFG_H__8186AB53802ED4DD");

                entity.ToTable("TB_CAM_HOLIDAY");

                entity.Property(e => e.HolidayDateId).HasColumnName("HOLIDAY_DATE_ID");

                entity.Property(e => e.CalendarId).HasColumnName("CALENDAR_ID");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.Date)
                    .HasColumnName("DATE")
                    .HasColumnType("date");

                entity.Property(e => e.DescriptionPrimaryLanguage)
                    .HasColumnName("DESCRIPTION_PRIMARY_LANGUAGE")
                    .HasMaxLength(400);

                entity.Property(e => e.DescriptionSecondaryLanguage)
                    .HasColumnName("DESCRIPTION_SECONDARY_LANGUAGE")
                    .HasMaxLength(400);

                entity.Property(e => e.HolidayType)
                    .HasColumnName("HOLIDAY_TYPE")
                    .HasMaxLength(20);

                entity.Property(e => e.IsWorkingDay).HasColumnName("IS_WORKING_DAY");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.HasOne(d => d.Calendar)
                    .WithMany(p => p.TbCamHoliday)
                    .HasForeignKey(d => d.CalendarId)
                    .HasConstraintName("FK_TB_CFG_HOLIDAY_TB_CAM_WORK_CALENDAR");
            });

            modelBuilder.Entity<TbCamUnit>(entity =>
            {
                entity.HasKey(e => e.UnitId)
                    .HasName("PK_TB_CFG_UNIT");

                entity.ToTable("TB_CAM_UNIT");

                entity.Property(e => e.UnitId).HasColumnName("UNIT_ID");

                entity.Property(e => e.ClientId).HasColumnName("CLIENT_ID");

                entity.Property(e => e.CodeLevel).HasColumnName("CODE_LEVEL");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(20);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.DisplayName)
                    .IsRequired()
                    .HasColumnName("DISPLAY_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.EntityId).HasColumnName("ENTITY_ID");

                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasColumnName("FULL_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(20);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.ParentId).HasColumnName("PARENT_ID");

                entity.Property(e => e.Remark).HasColumnName("REMARK");

                entity.Property(e => e.StatusCd)
                    .HasColumnName("STATUS_CD")
                    .HasMaxLength(10);

                entity.Property(e => e.UnitCode)
                    .IsRequired()
                    .HasColumnName("UNIT_CODE")
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<TbCamWorkCalendar>(entity =>
            {
                entity.HasKey(e => e.CalendarId)
                    .HasName("PK__TB_CAM_W__EB730BD4AE88AE81");

                entity.ToTable("TB_CAM_WORK_CALENDAR");

                entity.Property(e => e.CalendarId).HasColumnName("CALENDAR_ID");

                entity.Property(e => e.CalendarDay).HasColumnName("CALENDAR_DAY");

                entity.Property(e => e.CalendarName)
                    .HasColumnName("CALENDAR_NAME")
                    .HasMaxLength(200);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.DefaultWorkingDayFriday)
                    .HasColumnName("DEFAULT_WORKING_DAY_FRIDAY")
                    .HasMaxLength(20);

                entity.Property(e => e.DefaultWorkingDayMonday)
                    .HasColumnName("DEFAULT_WORKING_DAY_MONDAY")
                    .HasMaxLength(20);

                entity.Property(e => e.DefaultWorkingDaySaturday)
                    .HasColumnName("DEFAULT_WORKING_DAY_SATURDAY")
                    .HasMaxLength(20);

                entity.Property(e => e.DefaultWorkingDaySunday)
                    .HasColumnName("DEFAULT_WORKING_DAY_SUNDAY")
                    .HasMaxLength(20);

                entity.Property(e => e.DefaultWorkingDayThursday)
                    .HasColumnName("DEFAULT_WORKING_DAY_THURSDAY")
                    .HasMaxLength(20);

                entity.Property(e => e.DefaultWorkingDayTuesday)
                    .HasColumnName("DEFAULT_WORKING_DAY_TUESDAY")
                    .HasMaxLength(20);

                entity.Property(e => e.DefaultWorkingDayWednesday)
                    .HasColumnName("DEFAULT_WORKING_DAY_WEDNESDAY")
                    .HasMaxLength(20);

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.ParentCalendarId).HasColumnName("PARENT_CALENDAR_ID");

                entity.Property(e => e.SpecificEntityId).HasColumnName("SPECIFIC_ENTITY_ID");

                entity.Property(e => e.Status)
                    .HasColumnName("STATUS")
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<TbCfgCostCenter>(entity =>
            {
                entity.HasKey(e => e.CostCenterId)
                    .HasName("PK__TB_CFG_C__F84839770B1EE774");

                entity.ToTable("TB_CFG_COST_CENTER");

                entity.Property(e => e.CostCenterId).HasColumnName("COST_CENTER_ID");

                entity.Property(e => e.CostCenterCd)
                    .HasColumnName("COST_CENTER_CD")
                    .HasMaxLength(100);

                entity.Property(e => e.CostCenterName)
                    .HasColumnName("COST_CENTER_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.Remark)
                    .HasColumnName("REMARK")
                    .HasMaxLength(500);

                entity.Property(e => e.StatusCd)
                    .HasColumnName("STATUS_CD")
                    .HasMaxLength(10);
            });

            modelBuilder.Entity<TbCfgI18n>(entity =>
            {
                entity.HasKey(e => new { e.LanguageTag, e.TextKey })
                    .HasName("PK__TB_CFG_I__ADE6303609C232BA");

                entity.ToTable("TB_CFG_I18N");

                entity.Property(e => e.LanguageTag)
                    .HasColumnName("LANGUAGE_TAG")
                    .HasMaxLength(10)
                    .HasDefaultValueSql("('en')");

                entity.Property(e => e.TextKey)
                    .HasColumnName("TEXT_KEY")
                    .HasMaxLength(41);

                entity.Property(e => e.CreateBy)
                    .IsRequired()
                    .HasColumnName("CREATE_BY")
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('SYSTEM')");

                entity.Property(e => e.CreateDt)
                    .HasColumnName("CREATE_DT")
                    .HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.DefaultFlag)
                    .IsRequired()
                    .HasColumnName("DEFAULT_FLAG")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifyBy)
                    .IsRequired()
                    .HasColumnName("MODIFY_BY")
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('SYSTEM')");

                entity.Property(e => e.ModifyDt)
                    .HasColumnName("MODIFY_DT")
                    .HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.TextValue)
                    .IsRequired()
                    .HasColumnName("TEXT_VALUE")
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<TbCfgJobGrade>(entity =>
            {
                entity.HasKey(e => e.JobGradeId)
                    .HasName("PK__TB_CFG_J__BDB49011C0BBA14E");

                entity.ToTable("TB_CFG_JOB_GRADE");

                entity.Property(e => e.JobGradeId).HasColumnName("JOB_GRADE_ID");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.JobGradeLevel).HasColumnName("JOB_GRADE_LEVEL");

                entity.Property(e => e.JobGradeName)
                    .HasColumnName("JOB_GRADE_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.Remark)
                    .HasColumnName("REMARK")
                    .HasMaxLength(500);

                entity.Property(e => e.StatusCd)
                    .HasColumnName("STATUS_CD")
                    .HasMaxLength(10);
            });

            modelBuilder.Entity<TbCfgWorkingSchedule>(entity =>
            {
                entity.ToTable("TB_CFG_WORKING_SCHEDULE");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Name)
                    .HasColumnName("NAME")
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<TbComFile>(entity =>
            {
                entity.HasKey(e => e.FileId)
                    .HasName("PK__TB_COM_F__49C04C7A12192062");

                entity.ToTable("TB_COM_FILE");

                entity.Property(e => e.FileId).HasColumnName("FILE_ID");

                entity.Property(e => e.Category)
                    .HasColumnName("CATEGORY")
                    .HasMaxLength(20);

                entity.Property(e => e.Content)
                    .IsRequired()
                    .HasColumnName("CONTENT");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('SYSTEM')");

                entity.Property(e => e.CreatedDt)
                    .HasColumnName("CREATED_DT")
                    .HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.IsDeleted).HasColumnName("IS_DELETED");

                entity.Property(e => e.ModifiedBy)
                    .IsRequired()
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('SYSTEM')");

                entity.Property(e => e.ModifiedDt)
                    .HasColumnName("MODIFIED_DT")
                    .HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.Name)
                    .HasColumnName("NAME")
                    .HasMaxLength(200);

                entity.Property(e => e.Remark).HasColumnName("REMARK");
            });

            modelBuilder.Entity<TbEmpAttachment>(entity =>
            {
                entity.HasKey(e => e.AttachmentId);

                entity.ToTable("TB_EMP_ATTACHMENT");

                entity.Property(e => e.AttachmentId).HasColumnName("ATTACHMENT_ID");

                entity.Property(e => e.AttachmentFileId).HasColumnName("ATTACHMENT_FILE_ID");

                entity.Property(e => e.AttachmentFileName)
                    .HasColumnName("ATTACHMENT_FILE_NAME")
                    .HasMaxLength(200);

                entity.Property(e => e.AttachmentFileRemark)
                    .HasColumnName("ATTACHMENT_FILE_REMARK")
                    .HasMaxLength(500);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.EmployeeId).HasColumnName("EMPLOYEE_ID");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(200);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");
            });

            modelBuilder.Entity<TbEmpContractStatus>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("TB_EMP_CONTRACT_STATUS");

                entity.Property(e => e.Confirmed).HasColumnName("CONFIRMED");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.EmploymentContractId).HasColumnName("EMPLOYMENT_CONTRACT_ID");

                entity.Property(e => e.EndDate)
                    .HasColumnName("END_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.Remarks)
                    .HasColumnName("REMARKS")
                    .HasMaxLength(500)
                    .IsFixedLength();

                entity.Property(e => e.StartDate)
                    .HasColumnName("START_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.StatusCode)
                    .HasColumnName("STATUS_CODE")
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.HasOne(d => d.EmploymentContract)
                    .WithMany()
                    .HasForeignKey(d => d.EmploymentContractId)
                    .HasConstraintName("FK_TB_EMP_CONTRACT_STATUS_TB_EMP_EMPLOYMENT_CONTRACT");
            });

            modelBuilder.Entity<TbEmpCostAllocation>(entity =>
            {
                entity.HasKey(e => e.AllocationId);

                entity.ToTable("TB_EMP_COST_ALLOCATION");

                entity.Property(e => e.AllocationId).HasColumnName("ALLOCATION_ID");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(255);

                entity.Property(e => e.CreatedDt)
                    .HasColumnName("CREATED_DT")
                    .HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.EmployeeId).HasColumnName("EMPLOYEE_ID");

                entity.Property(e => e.EntityId).HasColumnName("ENTITY_ID");

                entity.Property(e => e.IsPercentage)
                    .IsRequired()
                    .HasColumnName("IS_PERCENTAGE")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy)
                    .IsRequired()
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(255);

                entity.Property(e => e.ModifiedDt)
                    .HasColumnName("MODIFIED_DT")
                    .HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.PayrollItemId).HasColumnName("PAYROLL_ITEM_ID");

                entity.Property(e => e.StartDate)
                    .HasColumnName("START_DATE")
                    .HasColumnType("date");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.TbEmpCostAllocation)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_EMP_COST_ALLOCATION_TB_EMP_EMPLOYEE");
            });

            modelBuilder.Entity<TbEmpCostBreakdown>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("TB_EMP_COST_BREAKDOWN");

                entity.Property(e => e.AllocationId).HasColumnName("ALLOCATION_ID");

                entity.Property(e => e.CostCenterId).HasColumnName("COST_CENTER_ID");

                entity.Property(e => e.CurrencyCd)
                    .HasColumnName("CURRENCY_CD")
                    .HasMaxLength(41);

                entity.Property(e => e.IsRemainder).HasColumnName("IS_REMAINDER");

                entity.Property(e => e.Portion)
                    .HasColumnName("PORTION")
                    .HasColumnType("decimal(18, 6)");

                entity.HasOne(d => d.Allocation)
                    .WithMany()
                    .HasForeignKey(d => d.AllocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_EMP_COST_BREAKDOWN_TB_EMP_COST_ALLOCATION");
            });

            modelBuilder.Entity<TbEmpDependent>(entity =>
            {
                entity.HasKey(e => e.DependentId);

                entity.ToTable("TB_EMP_DEPENDENT");

                entity.Property(e => e.DependentId).HasColumnName("DEPENDENT_ID");

                entity.Property(e => e.Address)
                    .HasColumnName("ADDRESS")
                    .HasMaxLength(200);

                entity.Property(e => e.ChristianName)
                    .HasColumnName("CHRISTIAN_NAME")
                    .HasMaxLength(55);

                entity.Property(e => e.ChristianNameSecondary)
                   .HasColumnName("CHRISTIAN_NAME_SECONDARY")
                   .HasMaxLength(55);

                entity.Property(e => e.CitizenIdNo)
                    .HasColumnName("CITIZEN_ID_NO")
                    .HasMaxLength(70);

                entity.Property(e => e.CountryOfIssue)
                    .HasColumnName("COUNTRY_OF_ISSUE")
                    .HasMaxLength(20);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.DateOfBirth)
                    .HasColumnName("DATE_OF_BIRTH")
                    .HasColumnType("date");

                entity.Property(e => e.EmployeeId).HasColumnName("EMPLOYEE_ID");

                entity.Property(e => e.FirstName)
                    .HasColumnName("FIRST_NAME")
                    .HasMaxLength(55);

                entity.Property(e => e.FirstNameSecondary)
                    .HasColumnName("FIRST_NAME_SECONDARY")
                    .HasMaxLength(55);

                entity.Property(e => e.Gender)
                    .HasColumnName("GENDER")
                    .HasMaxLength(20);

                entity.Property(e => e.LastName)
                    .HasColumnName("LAST_NAME")
                    .HasMaxLength(55);

                entity.Property(e => e.LastNameSecondary)
                    .HasColumnName("LAST_NAME_SECONDARY")
                    .HasMaxLength(55);

                entity.Property(e => e.MarriageDate)
                    .HasColumnName("MARRIAGE_DATE")
                    .HasColumnType("datetime");

                entity.Property(e => e.MiddleName)
                    .HasColumnName("MIDDLE_NAME")
                    .HasMaxLength(55);

                entity.Property(e => e.MiddleNameSecondary)
                    .HasColumnName("MIDDLE_NAME_SECONDARY")
                    .HasMaxLength(55);

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.NamePrefix)
                    .HasColumnName("NAME_PREFIX")
                    .HasMaxLength(20);

                entity.Property(e => e.Nationality)
                    .HasColumnName("NATIONALITY")
                    .HasMaxLength(20);

                entity.Property(e => e.PassportIdNo)
                    .HasColumnName("PASSPORT_ID_NO")
                    .HasMaxLength(70);

                entity.Property(e => e.RelationshipOther)
                    .HasColumnName("RELATIONSHIP_OTHER")
                    .HasMaxLength(100);

                entity.Property(e => e.RelationshipToEmployee)
                    .HasColumnName("RELATIONSHIP_TO_EMPLOYEE")
                    .HasMaxLength(20);

                entity.Property(e => e.Remarks)
                    .HasColumnName("REMARKS")
                    .HasMaxLength(500);

                entity.Property(e => e.VisaIssueDate)
                    .HasColumnName("VISA_ISSUE_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.VisaLandingDate)
                    .HasColumnName("VISA_LANDING_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.WorkVisaHolder).HasColumnName("WORK_VISA_HOLDER");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.TbEmpDependent)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK_TB_EMP_DEPENDENT_TB_EMP_EMPLOYEE");
            });

            modelBuilder.Entity<TbEmpEeBankAccount>(entity =>
            {
                entity.ToTable("TB_EMP_EE_BANK_ACCOUNT");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AccountCurrency)
                    .HasColumnName("ACCOUNT_CURRENCY")
                    .HasMaxLength(20);

                entity.Property(e => e.AccountName)
                    .HasColumnName("ACCOUNT_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.AccountNo)
                    .HasColumnName("ACCOUNT_NO")
                    .HasMaxLength(30);

                entity.Property(e => e.BankCode)
                    .HasColumnName("BANK_CODE")
                    .HasMaxLength(20);

                entity.Property(e => e.BranchCode)
                    .HasColumnName("BRANCH_CODE")
                    .HasMaxLength(20);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.DefaultAccount).HasColumnName("DEFAULT_ACCOUNT");

                entity.Property(e => e.EeBankAccountId)
                    .HasColumnName("EE_BANK_ACCOUNT_ID")
                    .HasMaxLength(100);

                entity.Property(e => e.EmployeeId).HasColumnName("EMPLOYEE_ID");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.Remark)
                    .HasColumnName("REMARK")
                    .HasMaxLength(500);

                entity.Property(e => e.TransactionReference)
                    .HasColumnName("TRANSACTION_REFERENCE")
                    .HasMaxLength(100);

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.TbEmpEeBankAccount)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK_TB_EMP_EE_BANK_ACCOUNT_TB_EMP_EMPLOYEE");
            });

            modelBuilder.Entity<TbEmpEmergencyContactPerson>(entity =>
            {
                entity.HasKey(e => e.EmergencyContactPersonId)
                    .HasName("PK_EMERGENCY_CONTACT_PERSON");

                entity.ToTable("TB_EMP_EMERGENCY_CONTACT_PERSON");

                entity.Property(e => e.EmergencyContactPersonId).HasColumnName("EMERGENCY_CONTACT_PERSON_ID");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.EmployeeId).HasColumnName("EMPLOYEE_ID");

                entity.Property(e => e.MobilePhone)
                    .HasColumnName("MOBILE_PHONE")
                    .HasMaxLength(50);

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.Name)
                    .HasColumnName("NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.RelationshipOther)
                    .HasColumnName("RELATIONSHIP_OTHER")
                    .HasMaxLength(100);

                entity.Property(e => e.RelationshipToEmployee)
                    .HasColumnName("RELATIONSHIP_TO_EMPLOYEE")
                    .HasMaxLength(20);

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.TbEmpEmergencyContactPerson)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK_TB_EMP_EMERGENCY_CONTACT_PERSON_TB_EMP_EMPLOYEE");
            });

            modelBuilder.Entity<TbEmpEmployee>(entity =>
            {
                entity.HasKey(e => e.EmployeeId);

                entity.ToTable("TB_EMP_EMPLOYEE");

                entity.Property(e => e.EmployeeId).HasColumnName("EMPLOYEE_ID");

                entity.Property(e => e.Address)
                    .HasColumnName("ADDRESS")
                    .HasMaxLength(30);

                entity.Property(e => e.Age).HasColumnName("AGE");

                entity.Property(e => e.Amount)
                    .HasColumnName("AMOUNT")
                    .HasMaxLength(30);

                entity.Property(e => e.ChristianName)
                    .HasColumnName("CHRISTIAN_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.ChristianNameSecondary)
                    .HasColumnName("CHRISTIAN_NAME_SECONDARY")
                    .HasMaxLength(100);

                entity.Property(e => e.CitizenIdNo)
                    .HasColumnName("CITIZEN_ID_NO")
                    .HasMaxLength(70);

                entity.Property(e => e.CompanyName)
                    .HasColumnName("COMPANY_NAME")
                    .HasMaxLength(30);

                entity.Property(e => e.CountryOfIssue)
                    .HasColumnName("COUNTRY_OF_ISSUE")
                    .HasMaxLength(20);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.Currency)
                    .HasColumnName("CURRENCY")
                    .HasMaxLength(20);

                entity.Property(e => e.DateOfBirth)
                    .HasColumnName("DATE_OF_BIRTH")
                    .HasColumnType("date");

                entity.Property(e => e.DisplayName)
                    .HasColumnName("DISPLAY_NAME")
                    .HasMaxLength(200);

                entity.Property(e => e.DisplayNameSecondary)
                    .HasColumnName("DISPLAY_NAME_SECONDARY")
                    .HasMaxLength(200);

                entity.Property(e => e.EntityId).HasColumnName("ENTITY_ID");

                entity.Property(e => e.FirstName)
                    .HasColumnName("FIRST_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.FirstNameSecondary)
                    .HasColumnName("FIRST_NAME_SECONDARY")
                    .HasMaxLength(100);

                entity.Property(e => e.Gender)
                    .HasColumnName("GENDER")
                    .HasMaxLength(20);

                entity.Property(e => e.HomePhone)
                    .HasColumnName("HOME_PHONE")
                    .HasMaxLength(50);

                entity.Property(e => e.LastName)
                    .HasColumnName("LAST_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.LastNameSecondary)
                    .HasColumnName("LAST_NAME_SECONDARY")
                    .HasMaxLength(100);

                entity.Property(e => e.MarriageDate)
                    .HasColumnName("MARRIAGE_DATE")
                    .HasColumnType("datetime");

                entity.Property(e => e.MartialStatus)
                    .HasColumnName("MARTIAL_STATUS")
                    .HasMaxLength(20);

                entity.Property(e => e.MiddleName)
                    .HasColumnName("MIDDLE_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.MiddleNameSecondary)
                    .HasColumnName("MIDDLE_NAME_SECONDARY")
                    .HasMaxLength(100);

                entity.Property(e => e.MobilePhone)
                    .HasColumnName("MOBILE_PHONE")
                    .HasMaxLength(50);

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.NamePrefix)
                    .HasColumnName("NAME_PREFIX")
                    .HasMaxLength(20);

                entity.Property(e => e.Nationality)
                    .HasColumnName("NATIONALITY")
                    .HasMaxLength(20);

                entity.Property(e => e.OfficePhone)
                    .HasColumnName("OFFICE_PHONE")
                    .HasMaxLength(50);

                entity.Property(e => e.OverseasPayments).HasColumnName("OVERSEAS_PAYMENTS");

                entity.Property(e => e.PassportIdNo)
                    .HasColumnName("PASSPORT_ID_NO")
                    .HasMaxLength(70);

                entity.Property(e => e.PersonalEmail)
                    .HasColumnName("PERSONAL_EMAIL")
                    .HasMaxLength(100);

                entity.Property(e => e.Photo).HasColumnName("PHOTO");

                entity.Property(e => e.PostalAddress1)
                    .HasColumnName("POSTAL_ADDRESS_1")
                    .HasMaxLength(30);

                entity.Property(e => e.PostalAddress1Secondary)
                    .HasColumnName("POSTAL_ADDRESS_1_SECONDARY")
                    .HasMaxLength(30);

                entity.Property(e => e.PostalAddress2)
                    .HasColumnName("POSTAL_ADDRESS_2")
                    .HasMaxLength(30);

                entity.Property(e => e.PostalAddress2Secondary)
                    .HasColumnName("POSTAL_ADDRESS_2_SECONDARY")
                    .HasMaxLength(30);

                entity.Property(e => e.PostalAddress3)
                    .HasColumnName("POSTAL_ADDRESS_3")
                    .HasMaxLength(30);

                entity.Property(e => e.PostalAddress3Secondary)
                    .HasColumnName("POSTAL_ADDRESS_3_SECONDARY")
                    .HasMaxLength(30);

                entity.Property(e => e.PostalCity)
                    .HasColumnName("POSTAL_CITY")
                    .HasMaxLength(30);

                entity.Property(e => e.PostalCitySecondary)
                    .HasColumnName("POSTAL_CITY_SECONDARY")
                    .HasMaxLength(30);

                entity.Property(e => e.PostalCountry)
                    .HasColumnName("POSTAL_COUNTRY")
                    .HasMaxLength(20);

                entity.Property(e => e.PostalDistrict)
                    .HasColumnName("POSTAL_DISTRICT")
                    .HasMaxLength(30);

                entity.Property(e => e.PostalDistrictSecondary)
                    .HasColumnName("POSTAL_DISTRICT_SECONDARY")
                    .HasMaxLength(30);

                entity.Property(e => e.PostalHongkongTaxForm)
                    .HasColumnName("POSTAL_HONGKONG_TAX_FORM")
                    .HasMaxLength(30);

                entity.Property(e => e.PostalPostalCode)
                    .HasColumnName("POSTAL_POSTAL_CODE")
                    .HasMaxLength(30);

                entity.Property(e => e.PostalState)
                    .HasColumnName("POSTAL_STATE")
                    .HasMaxLength(30);

                entity.Property(e => e.PostalStateSecondary)
                    .HasColumnName("POSTAL_STATE_SECONDARY")
                    .HasMaxLength(30);

                entity.Property(e => e.PreferredLanguage)
                    .HasColumnName("PREFERRED_LANGUAGE")
                    .HasMaxLength(20);

                entity.Property(e => e.RemarkBankAccount)
                    .HasColumnName("REMARK_BANK_ACCOUNT")
                    .HasMaxLength(500);

                entity.Property(e => e.RemarkCompany)
                    .HasColumnName("REMARK_COMPANY")
                    .HasMaxLength(500);

                entity.Property(e => e.RemarkContact)
                    .HasColumnName("REMARK_CONTACT")
                    .HasMaxLength(500);

                entity.Property(e => e.RemarkDependent)
                    .HasColumnName("REMARK_DEPENDENT")
                    .HasMaxLength(500);

                entity.Property(e => e.RemarkEmploymentContract)
                    .HasColumnName("REMARK_EMPLOYMENT_CONTRACT")
                    .HasMaxLength(500);

                entity.Property(e => e.RemarkEmploymentMovement)
                    .HasColumnName("REMARK_EMPLOYMENT_MOVEMENT")
                    .HasMaxLength(500);

                entity.Property(e => e.RemarkEmploymentStatus)
                    .HasColumnName("REMARK_EMPLOYMENT_STATUS")
                    .HasMaxLength(500);

                entity.Property(e => e.RemarkRentalAddress)
                    .HasColumnName("REMARK_RENTAL_ADDRESS")
                    .HasMaxLength(500);

                entity.Property(e => e.RemarkTax)
                    .HasColumnName("REMARK_TAX")
                    .HasMaxLength(500);

                entity.Property(e => e.Remarks)
                    .HasColumnName("REMARKS")
                    .HasMaxLength(500);

                entity.Property(e => e.ResidentialAddress1)
                    .HasColumnName("RESIDENTIAL_ADDRESS_1")
                    .HasMaxLength(30);

                entity.Property(e => e.ResidentialAddress1Secondary)
                    .HasColumnName("RESIDENTIAL_ADDRESS_1_SECONDARY")
                    .HasMaxLength(30);

                entity.Property(e => e.ResidentialAddress2)
                    .HasColumnName("RESIDENTIAL_ADDRESS_2")
                    .HasMaxLength(30);

                entity.Property(e => e.ResidentialAddress2Secondary)
                    .HasColumnName("RESIDENTIAL_ADDRESS_2_SECONDARY")
                    .HasMaxLength(30);

                entity.Property(e => e.ResidentialAddress3)
                    .HasColumnName("RESIDENTIAL_ADDRESS_3")
                    .HasMaxLength(30);

                entity.Property(e => e.ResidentialAddress3Secondary)
                    .HasColumnName("RESIDENTIAL_ADDRESS_3_SECONDARY")
                    .HasMaxLength(30);

                entity.Property(e => e.ResidentialCity)
                    .HasColumnName("RESIDENTIAL_CITY")
                    .HasMaxLength(30);

                entity.Property(e => e.ResidentialCitySecondary)
                    .HasColumnName("RESIDENTIAL_CITY_SECONDARY")
                    .HasMaxLength(30);

                entity.Property(e => e.ResidentialCountry)
                    .HasColumnName("RESIDENTIAL_COUNTRY")
                    .HasMaxLength(20);

                entity.Property(e => e.ResidentialDistrict)
                    .HasColumnName("RESIDENTIAL_DISTRICT")
                    .HasMaxLength(30);

                entity.Property(e => e.ResidentialDistrictSecondary)
                    .HasColumnName("RESIDENTIAL_DISTRICT_SECONDARY")
                    .HasMaxLength(30);

                entity.Property(e => e.ResidentialHongkongTaxForm)
                    .HasColumnName("RESIDENTIAL_HONGKONG_TAX_FORM")
                    .HasMaxLength(30);

                entity.Property(e => e.ResidentialPostalCode)
                    .HasColumnName("RESIDENTIAL_POSTAL_CODE")
                    .HasMaxLength(30);

                entity.Property(e => e.ResidentialState)
                    .HasColumnName("RESIDENTIAL_STATE")
                    .HasMaxLength(30);

                entity.Property(e => e.ResidentialStateSecondary)
                    .HasColumnName("RESIDENTIAL_STATE_SECONDARY")
                    .HasMaxLength(30);

                entity.Property(e => e.SysEndTime).HasDefaultValueSql("('9999-12-31 23:59:59.9999999')");

                entity.Property(e => e.SysStartTime).HasDefaultValueSql("(sysutcdatetime())");

                entity.Property(e => e.WorkArrivingDate)
                    .HasColumnName("WORK_ARRIVING_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.WorkEmail)
                    .HasColumnName("WORK_EMAIL")
                    .HasMaxLength(100);

                entity.Property(e => e.WorkVisaExpiryDate)
                    .HasColumnName("WORK_VISA_EXPIRY_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.WorkVisaHolder).HasColumnName("WORK_VISA_HOLDER");

                entity.Property(e => e.WorkVisaIssueDate)
                    .HasColumnName("WORK_VISA_ISSUE_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.WorkVisaLandingDate)
                    .HasColumnName("WORK_VISA_LANDING_DATE")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<TbEmpEmploymentContract>(entity =>
            {
                entity.HasKey(e => e.EmploymentContractId);

                entity.ToTable("TB_EMP_EMPLOYMENT_CONTRACT");

                entity.Property(e => e.EmploymentContractId).HasColumnName("EMPLOYMENT_CONTRACT_ID");

                entity.Property(e => e.ContractNo)
                    .HasColumnName("CONTRACT_NO")
                    .HasMaxLength(30);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.EmployeeId).HasColumnName("EMPLOYEE_ID");

                entity.Property(e => e.EmployeeNo)
                    .HasColumnName("EMPLOYEE_NO")
                    .HasMaxLength(30);

                entity.Property(e => e.GlobalNo)
                    .HasColumnName("GLOBAL_NO")
                    .HasMaxLength(30);

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.Remarks)
                    .HasColumnName("REMARKS")
                    .HasMaxLength(500);

                entity.Property(e => e.StaffType)
                    .HasColumnName("STAFF_TYPE")
                    .HasMaxLength(20);

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.TbEmpEmploymentContract)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK_TB_EMP_EMPLOYMENT_CONTRACT_TB_EMP_EMPLOYEE");
            });

            modelBuilder.Entity<TbEmpEmploymentStatus>(entity =>
            {
                entity.HasKey(e => e.EmploymentStatusId)
                    .HasName("PK__TB_EMP_E__D04DB0940B91D0AC");

                entity.ToTable("TB_EMP_EMPLOYMENT_STATUS");

                entity.Property(e => e.EmploymentStatusId).HasColumnName("EMPLOYMENT_STATUS_ID");

                entity.Property(e => e.Confirmed).HasColumnName("CONFIRMED");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(20);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.EmploymentContractId).HasColumnName("EMPLOYMENT_CONTRACT_ID");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(20);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.Remark)
                    .HasColumnName("REMARK")
                    .HasMaxLength(500);

                entity.Property(e => e.StartDate)
                    .HasColumnName("START_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnName("STATUS")
                    .HasMaxLength(20);

                entity.HasOne(d => d.EmploymentContract)
                    .WithMany(p => p.TbEmpEmploymentStatus)
                    .HasForeignKey(d => d.EmploymentContractId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_EMP_EMPLOYMENT_STATUS_TB_EMP_EMPLOYMENT_CONTRACT");
            });

            modelBuilder.Entity<TbEmpMovement>(entity =>
            {
                entity.HasKey(e => e.AssignmentId);

                entity.ToTable("TB_EMP_MOVEMENT");

                entity.Property(e => e.AssignmentId).HasColumnName("ASSIGNMENT_ID");

                entity.Property(e => e.AssignmentStartDate)
                    .HasColumnName("ASSIGNMENT_START_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.BaseSalaryUnit)
                    .HasColumnName("BASE_SALARY_UNIT")
                    .HasMaxLength(20);

                entity.Property(e => e.BasicSalary).HasColumnName("BASIC_SALARY");

                entity.Property(e => e.BusinessUnitId).HasColumnName("BUSINESS_UNIT_ID");

                entity.Property(e => e.CostCenter).HasColumnName("COST_CENTER");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.DefaultPaymentCurrency)
                    .HasColumnName("DEFAULT_PAYMENT_CURRENCY")
                    .HasMaxLength(20);

                entity.Property(e => e.DefaultSalaryCurrency)
                    .HasColumnName("DEFAULT_SALARY_CURRENCY")
                    .HasMaxLength(20);

                entity.Property(e => e.DirectManager).HasColumnName("DIRECT_MANAGER");

                entity.Property(e => e.EmploymentContractId).HasColumnName("EMPLOYMENT_CONTRACT_ID");

                entity.Property(e => e.ExternalJobTitle)
                    .HasColumnName("EXTERNAL_JOB_TITLE")
                    .HasMaxLength(40);

                entity.Property(e => e.InternalJobTitle)
                    .HasColumnName("INTERNAL_JOB_TITLE")
                    .HasMaxLength(40);

                entity.Property(e => e.JobGrade).HasColumnName("JOB_GRADE");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.MovementName)
                    .HasColumnName("MOVEMENT_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.NotificationRequirements).HasColumnName("NOTIFICATION_REQUIREMENTS");

                entity.Property(e => e.Remarks)
                    .HasColumnName("REMARKS")
                    .HasMaxLength(500);

                entity.Property(e => e.RemunerationPackage).HasColumnName("REMUNERATION_PACKAGE");

                entity.Property(e => e.SpecificEntityId).HasColumnName("SPECIFIC_ENTITY_ID");

                entity.Property(e => e.StaffTags)
                    .HasColumnName("STAFF_TAGS")
                    .HasMaxLength(100);

                entity.Property(e => e.WorkCalendar).HasColumnName("WORK_CALENDAR");

                entity.HasOne(d => d.EmploymentContract)
                    .WithMany(p => p.TbEmpMovement)
                    .HasForeignKey(d => d.EmploymentContractId)
                    .HasConstraintName("FK_TB_EMP_MOVEMENT_TB_EMP_EMPLOYMENT_CONTRACT");
            });

            modelBuilder.Entity<TbEmpPayslip>(entity =>
            {
                entity.HasKey(e => e.PayslipId);

                entity.ToTable("TB_EMP_PAYSLIP");

                entity.Property(e => e.PayslipId).HasColumnName("PAYSLIP_ID");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.EmployeeId).HasColumnName("EMPLOYEE_ID");

                entity.Property(e => e.FileId).HasColumnName("FILE_ID");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(200);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.PayslipDescription)
                    .HasColumnName("PAYSLIP_DESCRIPTION")
                    .HasMaxLength(200);

                entity.Property(e => e.PayslipDt)
                    .HasColumnName("PAYSLIP_DT")
                    .HasColumnType("date");

                entity.HasOne(d => d.File)
                    .WithMany(p => p.TbEmpPayslip)
                    .HasForeignKey(d => d.FileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_EMP_PAYSLIP_FILE");
            });

            modelBuilder.Entity<TbEmpPayslipPassword>(entity =>
            {
                entity.HasKey(e => e.PayslipPasswordId);

                entity.ToTable("TB_EMP_PAYSLIP_PASSWORD");

                entity.Property(e => e.PayslipPasswordId).HasColumnName("PAYSLIP_PASSWORD_ID");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.EmployeeId).HasColumnName("EMPLOYEE_ID");

                entity.Property(e => e.EncryptedPassword)
                    .IsRequired()
                    .HasColumnName("ENCRYPTED_PASSWORD")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.TbEmpPayslipPassword)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_EMP_PAYSLIP_PASSWORD_TB_EMP_EMPLOYEE");
            });

            modelBuilder.Entity<TbEmpRentalAddress>(entity =>
            {
                entity.HasKey(e => e.AddressId);

                entity.ToTable("TB_EMP_RENTAL_ADDRESS");

                entity.Property(e => e.AddressId).HasColumnName("ADDRESS_ID");

                entity.Property(e => e.Address)
                    .HasColumnName("ADDRESS")
                    .HasMaxLength(200);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.EmployeeId).HasColumnName("EMPLOYEE_ID");

                entity.Property(e => e.EndDate)
                    .HasColumnName("END_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.Nature)
                    .HasColumnName("NATURE")
                    .HasMaxLength(20);

                entity.Property(e => e.RentPaidToEmployerByEmployee).HasColumnName("RENT_PAID_TO_EMPLOYER_BY_EMPLOYEE");

                entity.Property(e => e.RentPaidToLandlordByEmployee).HasColumnName("RENT_PAID_TO_LANDLORD_BY_EMPLOYEE");

                entity.Property(e => e.RentPaidToLandlordByEmployer).HasColumnName("RENT_PAID_TO_LANDLORD_BY_EMPLOYER");

                entity.Property(e => e.RentRefundedToEmployeeByEmployer).HasColumnName("RENT_REFUNDED_TO_EMPLOYEE_BY_EMPLOYER");

                entity.Property(e => e.StartDate)
                    .HasColumnName("START_DATE")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<TbEmpTax>(entity =>
            {
                entity.HasKey(e => e.TaxId);

                entity.ToTable("TB_EMP_TAX");

                entity.Property(e => e.TaxId).HasColumnName("TAX_ID");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.EmploymentContractId).HasColumnName("EMPLOYMENT_CONTRACT_ID");

                entity.Property(e => e.Ir56bRemarks)
                    .HasColumnName("IR56B_REMARKS")
                    .HasMaxLength(500);

                entity.Property(e => e.Ir56eFlutuatingEmoluments)
                    .HasColumnName("IR56E_FLUTUATING_EMOLUMENTS")
                    .HasMaxLength(30);

                entity.Property(e => e.Ir56eMonthlyRateOfAllownace)
                    .HasColumnName("IR56E_MONTHLY_RATE_OF_ALLOWNACE")
                    .HasMaxLength(30);

                entity.Property(e => e.Ir56ePreviousEmployerAddress)
                    .HasColumnName("IR56E_PREVIOUS_EMPLOYER_ADDRESS")
                    .HasMaxLength(100);

                entity.Property(e => e.Ir56ePreviousEmployerName)
                    .HasColumnName("IR56E_PREVIOUS_EMPLOYER_NAME")
                    .HasMaxLength(50);

                entity.Property(e => e.Ir56eShareOptionGranted).HasColumnName("IR56E_SHARE_OPTION_GRANTED");

                entity.Property(e => e.Ir56fAddressAfterTermination)
                    .HasColumnName("IR56F_ADDRESS_AFTER_TERMINATION")
                    .HasMaxLength(100);

                entity.Property(e => e.Ir56fNewEmployerAddress)
                    .HasColumnName("IR56F_NEW_EMPLOYER_ADDRESS")
                    .HasMaxLength(100);

                entity.Property(e => e.Ir56fNewEmployerName)
                    .HasColumnName("IR56F_NEW_EMPLOYER_NAME")
                    .HasMaxLength(50);

                entity.Property(e => e.Ir56gAddressAfterTermination)
                    .HasColumnName("IR56G_ADDRESS_AFTER_TERMINATION")
                    .HasMaxLength(100);

                entity.Property(e => e.Ir56gDepartureDate)
                    .HasColumnName("IR56G_DEPARTURE_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.Ir56gEmployeeReturningHk).HasColumnName("IR56G_EMPLOYEE_RETURNING_HK");

                entity.Property(e => e.Ir56gEmployeeReturningHkDate)
                    .HasColumnName("IR56G_EMPLOYEE_RETURNING_HK_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.Ir56gEmployerHasHoldMoney).HasColumnName("IR56G_EMPLOYER_HAS_HOLD_MONEY");

                entity.Property(e => e.Ir56gGrantedDate)
                    .HasColumnName("IR56G_GRANTED_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.Ir56gHeldAmount).HasColumnName("IR56G_HELD_AMOUNT");

                entity.Property(e => e.Ir56gNoOfSharesNotYetExercised)
                    .HasColumnName("IR56G_NO_OF_SHARES_NOT_YET_EXERCISED")
                    .HasMaxLength(30);

                entity.Property(e => e.Ir56gReasonForDeparture)
                    .HasColumnName("IR56G_REASON_FOR_DEPARTURE")
                    .HasMaxLength(20);

                entity.Property(e => e.Ir56gReasonForDepartureOthers)
                    .HasColumnName("IR56G_REASON_FOR_DEPARTURE_OTHERS")
                    .HasMaxLength(100);

                entity.Property(e => e.Ir56gReasonForNotHoldingMoney)
                    .HasColumnName("IR56G_REASON_FOR_NOT_HOLDING_MONEY")
                    .HasMaxLength(100);

                entity.Property(e => e.Ir56gShareOptionGranted).HasColumnName("IR56G_SHARE_OPTION_GRANTED");

                entity.Property(e => e.Ir56gTaxBorneByEmployer).HasColumnName("IR56G_TAX_BORNE_BY_EMPLOYER");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.HasOne(d => d.EmploymentContract)
                    .WithMany(p => p.TbEmpTax)
                    .HasForeignKey(d => d.EmploymentContractId)
                    .HasConstraintName("FK_TB_EMP_TAX_TB_EMP_EMPLOYMENT_CONTRACT");
            });

            modelBuilder.Entity<TbEmpTaxReturn>(entity =>
            {
                entity.HasKey(e => e.TaxReturnId);

                entity.ToTable("TB_EMP_TAX_RETURN");

                entity.Property(e => e.TaxReturnId).HasColumnName("TAX_RETURN_ID");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.EmployeeId).HasColumnName("EMPLOYEE_ID");

                entity.Property(e => e.FileId).HasColumnName("FILE_ID");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(200);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.TaxReturnDescription)
                    .HasColumnName("TAX_RETURN_DESCRIPTION")
                    .HasMaxLength(200);

                entity.Property(e => e.TaxReturnDt)
                    .HasColumnName("TAX_RETURN_DT")
                    .HasColumnType("date");

                entity.Property(e => e.TaxReturnYear).HasColumnName("TAX_RETURN_YEAR");

                entity.HasOne(d => d.File)
                    .WithMany(p => p.TbEmpTaxReturn)
                    .HasForeignKey(d => d.FileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_TAX_RETURN_FILE");
            });

            modelBuilder.Entity<TbEmpTermination>(entity =>
            {
                entity.ToTable("TB_EMP_TERMINATION");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.EmploymentContractId).HasColumnName("EMPLOYMENT_CONTRACT_ID");

                entity.Property(e => e.HoldLastPayment).HasColumnName("HOLD_LAST_PAYMENT");

                entity.Property(e => e.LastEmploymentDate)
                    .HasColumnName("LAST_EMPLOYMENT_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.LastWorkingDate)
                    .HasColumnName("LAST_WORKING_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.LeavingHongKong).HasColumnName("LEAVING_HONG_KONG");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.NotificationDate)
                    .HasColumnName("NOTIFICATION_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.Remark)
                    .HasColumnName("REMARK")
                    .HasMaxLength(500);

                entity.Property(e => e.TerminationCodeTax)
                    .HasColumnName("TERMINATION_CODE_TAX")
                    .HasMaxLength(20);

                entity.Property(e => e.TerminationContractEndDate)
                    .HasColumnName("TERMINATION_CONTRACT_END_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.TerminationReasonAlternative)
                    .HasColumnName("TERMINATION_REASON_ALTERNATIVE")
                    .HasMaxLength(20);

                entity.Property(e => e.TerminationReasonMpf)
                    .HasColumnName("TERMINATION_REASON_MPF")
                    .HasMaxLength(20);

                entity.Property(e => e.TerminationReasonPrimary)
                    .HasColumnName("TERMINATION_REASON_PRIMARY")
                    .HasMaxLength(20);

                entity.Property(e => e.YearServiceEndDate)
                    .HasColumnName("YEAR_SERVICE_END_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.YearServiceStartDate)
                    .HasColumnName("YEAR_SERVICE_START_DATE")
                    .HasColumnType("date");

                entity.HasOne(d => d.EmploymentContract)
                    .WithMany(p => p.TbEmpTermination)
                    .HasForeignKey(d => d.EmploymentContractId)
                    .HasConstraintName("FK_TB_EMP_TERMINATION_TB_EMP_EMPLOYMENT_CONTRACT");
            });

            modelBuilder.Entity<TbPayEnrolledScheme>(entity =>
            {
                entity.HasKey(e => e.EnrolledSchemeId)
                    .HasName("PK_TB_EMP_ENROLLED_SCHEME");

                entity.ToTable("TB_PAY_ENROLLED_SCHEME");

                entity.Property(e => e.EnrolledSchemeId).HasColumnName("ENROLLED_SCHEME_ID");

                entity.Property(e => e.ClosedDate)
                    .HasColumnName("CLOSED_DATE")
                    .HasColumnType("datetime");

                entity.Property(e => e.ContactPerson)
                    .HasColumnName("CONTACT_PERSON")
                    .HasMaxLength(50);

                entity.Property(e => e.ContributionCycle)
                    .HasColumnName("CONTRIBUTION_CYCLE")
                    .HasMaxLength(20);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.Currency)
                    .HasColumnName("CURRENCY")
                    .HasMaxLength(20);

                entity.Property(e => e.EnrolledSchemeCode)
                    .HasColumnName("ENROLLED_SCHEME_CODE")
                    .HasMaxLength(50);

                entity.Property(e => e.EnrolledSchemeName)
                    .HasColumnName("ENROLLED_SCHEME_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.EnrolledSchemeNo)
                    .HasColumnName("ENROLLED_SCHEME_NO")
                    .HasMaxLength(50);

                entity.Property(e => e.EnrolledSchemeType)
                    .HasColumnName("ENROLLED_SCHEME_TYPE")
                    .HasMaxLength(20);

                entity.Property(e => e.FaxNumber)
                    .HasColumnName("FAX_NUMBER")
                    .HasMaxLength(50);

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.MpfRegisteredSchemeId).HasColumnName("MPF_REGISTERED_SCHEME_ID");

                entity.Property(e => e.NearestDec).HasColumnName("NEAREST_DEC");

                entity.Property(e => e.ParticipationNo)
                    .HasColumnName("PARTICIPATION_NO")
                    .HasMaxLength(50);

                entity.Property(e => e.PayCentre)
                    .HasColumnName("PAY_CENTRE")
                    .HasMaxLength(50);

                entity.Property(e => e.PaymentMethod)
                    .HasColumnName("PAYMENT_METHOD")
                    .HasMaxLength(20);

                entity.Property(e => e.PhoneNumber)
                    .HasColumnName("PHONE_NUMBER")
                    .HasMaxLength(50);

                entity.Property(e => e.RoundType)
                    .HasColumnName("ROUND_TYPE")
                    .HasMaxLength(50);

                entity.Property(e => e.StatusCd)
                    .HasColumnName("STATUS_CD")
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<TbPayExchangeRate>(entity =>
            {
                entity.HasKey(e => e.ExchangeRateId)
                    .HasName("PK_TB_CFG_EXCHANGE_RATE");

                entity.ToTable("TB_PAY_EXCHANGE_RATE");

                entity.Property(e => e.ExchangeRateId).HasColumnName("EXCHANGE_RATE_ID");

                entity.Property(e => e.AverageRate).HasColumnName("AVERAGE_RATE");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(20);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.CurrencyFrom)
                    .IsRequired()
                    .HasColumnName("CURRENCY_FROM")
                    .HasMaxLength(20);

                entity.Property(e => e.CurrencyTo)
                    .IsRequired()
                    .HasColumnName("CURRENCY_TO")
                    .HasMaxLength(20);

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(20);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.MonthEndRate).HasColumnName("MONTH_END_RATE");

                entity.Property(e => e.PayrollCycleId).HasColumnName("PAYROLL_CYCLE_ID");

                entity.HasOne(d => d.PayrollCycle)
                    .WithMany(p => p.TbPayExchangeRate)
                    .HasForeignKey(d => d.PayrollCycleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_PAY_EXCHANGE_RATE_TB_PAY_PAYROLL_CYCLE");
            });

            modelBuilder.Entity<TbPayGlCode>(entity =>
            {
                entity.HasKey(e => e.GlCodeId)
                    .HasName("PK_TB_CFG_GL_CODE");

                entity.ToTable("TB_PAY_GL_CODE");

                entity.Property(e => e.GlCodeId).HasColumnName("GL_CODE_ID");

                entity.Property(e => e.AccountType)
                    .IsRequired()
                    .HasColumnName("ACCOUNT_TYPE")
                    .HasMaxLength(20);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(20);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.Currency)
                    .IsRequired()
                    .HasColumnName("CURRENCY")
                    .HasMaxLength(20);

                entity.Property(e => e.GlCode)
                    .IsRequired()
                    .HasColumnName("GL_CODE")
                    .HasMaxLength(20);

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(20);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.Remark)
                    .IsRequired()
                    .HasColumnName("REMARK");

                entity.Property(e => e.StatusCd)
                    .IsRequired()
                    .HasColumnName("STATUS_CD")
                    .HasMaxLength(10);
            });

            modelBuilder.Entity<TbPayMasterPayrollItem>(entity =>
            {
                entity.HasKey(e => e.PayrollItemId);

                entity.ToTable("TB_PAY_MASTER_PAYROLL_ITEM");

                entity.Property(e => e.PayrollItemId).HasColumnName("PAYROLL_ITEM_ID");

                entity.Property(e => e.ClientId).HasColumnName("CLIENT_ID");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(20);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.EntityId).HasColumnName("ENTITY_ID");

                entity.Property(e => e.GlCodeId).HasColumnName("GL_CODE_ID");

                entity.Property(e => e.ItemCode)
                    .IsRequired()
                    .HasColumnName("ITEM_CODE")
                    .HasMaxLength(20);

                entity.Property(e => e.ItemName)
                    .IsRequired()
                    .HasColumnName("ITEM_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(20);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.PaymentSign).HasColumnName("PAYMENT_SIGN");

                entity.Property(e => e.Remark).HasColumnName("REMARK");

                entity.Property(e => e.StatusCd)
                    .IsRequired()
                    .HasColumnName("STATUS_CD")
                    .HasMaxLength(10);

                entity.HasOne(d => d.GlCode)
                    .WithMany(p => p.TbPayMasterPayrollItem)
                    .HasForeignKey(d => d.GlCodeId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_PAYMASTER_GLCODE");
            });

            modelBuilder.Entity<TbPayMasterPayrollItemFlag>(entity =>
            {
                entity.HasKey(e => e.PayrollItemFlagId)
                    .HasName("PK_TB_PAY_PAYROLL_ITEM_FLAG");

                entity.ToTable("TB_PAY_MASTER_PAYROLL_ITEM_FLAG");

                entity.Property(e => e.PayrollItemFlagId).HasColumnName("PAYROLL_ITEM_FLAG_ID");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.FlagId).HasColumnName("FLAG_ID");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.PayrollItemId).HasColumnName("PAYROLL_ITEM_ID");

                entity.Property(e => e.StatusCd)
                    .IsRequired()
                    .HasColumnName("STATUS_CD")
                    .HasMaxLength(10);

                entity.HasOne(d => d.PayrollItem)
                    .WithMany(p => p.TbPayMasterPayrollItemFlag)
                    .HasForeignKey(d => d.PayrollItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_PAY_MASTER_PAYROLL_ITEM_FLAG_TB_PAY_MASTER_PAYROLL_ITEM");
            });

            modelBuilder.Entity<TbPayMasterPayrollItemTax>(entity =>
            {
                entity.HasKey(e => e.PayrollItemTaxId)
                    .HasName("PK_TB_PAY_PAYROLL_ITEM_TAX");

                entity.ToTable("TB_PAY_MASTER_PAYROLL_ITEM_TAX");

                entity.Property(e => e.PayrollItemTaxId).HasColumnName("PAYROLL_ITEM_TAX_ID");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.PayrollItemId).HasColumnName("PAYROLL_ITEM_ID");

                entity.Property(e => e.StatusCd)
                    .IsRequired()
                    .HasColumnName("STATUS_CD")
                    .HasMaxLength(10);

                entity.Property(e => e.TaxItemId).HasColumnName("TAX_ITEM_ID");

                entity.Property(e => e.TaxItemValue).HasColumnName("TAX_ITEM_VALUE");

                entity.HasOne(d => d.PayrollItem)
                    .WithMany(p => p.TbPayMasterPayrollItemTax)
                    .HasForeignKey(d => d.PayrollItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_PAY_MASTER_PAYROLL_ITEM_TAX_TB_PAY_MASTER_PAYROLL_ITEM");
            });

            modelBuilder.Entity<TbPayOrsoScheme>(entity =>
            {
                entity.HasKey(e => e.OrsoSchemeId)
                    .HasName("PK_TB_EMP_ORSO_SCHEME");

                entity.ToTable("TB_PAY_ORSO_SCHEME");

                entity.Property(e => e.OrsoSchemeId).HasColumnName("ORSO_SCHEME_ID");

                entity.Property(e => e.ClosedDate)
                    .HasColumnName("CLOSED_DATE")
                    .HasColumnType("datetime");

                entity.Property(e => e.ContactPerson)
                    .HasColumnName("CONTACT_PERSON")
                    .HasMaxLength(50);

                entity.Property(e => e.ContributionCycle)
                    .HasColumnName("CONTRIBUTION_CYCLE")
                    .HasMaxLength(20);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.Currency)
                    .HasColumnName("CURRENCY")
                    .HasMaxLength(20);

                entity.Property(e => e.DesignatedPersonName)
                    .HasColumnName("DESIGNATED_PERSON_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.Domicile)
                    .HasColumnName("DOMICILE")
                    .HasMaxLength(100);

                entity.Property(e => e.FaxNumber)
                    .HasColumnName("FAX_NUMBER")
                    .HasMaxLength(50);

                entity.Property(e => e.InvestmentManagerName)
                    .HasColumnName("INVESTMENT_MANAGER_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.MpfExemptionDate)
                    .HasColumnName("MPF_EXEMPTION_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.MpfExemptionNo)
                    .HasColumnName("MPF_EXEMPTION_NO")
                    .HasMaxLength(50);

                entity.Property(e => e.NearestDec).HasColumnName("NEAREST_DEC");

                entity.Property(e => e.OrsoAdminFee).HasColumnName("ORSO_ADMIN_FEE");

                entity.Property(e => e.OrsoAdminUnit)
                    .HasColumnName("ORSO_ADMIN_UNIT")
                    .HasMaxLength(20);

                entity.Property(e => e.OrsoExemptionNo)
                    .HasColumnName("ORSO_EXEMPTION_NO")
                    .HasMaxLength(50);

                entity.Property(e => e.OrsoSchemeCode)
                    .HasColumnName("ORSO_SCHEME_CODE")
                    .HasMaxLength(50);

                entity.Property(e => e.OrsoSchemeName)
                    .HasColumnName("ORSO_SCHEME_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.OrsoSchemeType)
                    .HasColumnName("ORSO_SCHEME_TYPE")
                    .HasMaxLength(20);

                entity.Property(e => e.ParticipationNo)
                    .HasColumnName("PARTICIPATION_NO")
                    .HasMaxLength(50);

                entity.Property(e => e.PayCentre)
                    .HasColumnName("PAY_CENTRE")
                    .HasMaxLength(50);

                entity.Property(e => e.PaymentMethod)
                    .HasColumnName("PAYMENT_METHOD")
                    .HasMaxLength(20);

                entity.Property(e => e.PhoneNumber)
                    .HasColumnName("PHONE_NUMBER")
                    .HasMaxLength(50);

                entity.Property(e => e.RoundType)
                    .HasColumnName("ROUND_TYPE")
                    .HasMaxLength(50);

                entity.Property(e => e.SchemeAdministratorName)
                    .HasColumnName("SCHEME_ADMINISTRATOR_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.SchemeEstablishmentDate)
                    .HasColumnName("SCHEME_ESTABLISHMENT_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.StatusCd)
                    .HasColumnName("STATUS_CD")
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<TbPayPayrollCycle>(entity =>
            {
                entity.HasKey(e => e.PayrollCycleId);

                entity.ToTable("TB_PAY_PAYROLL_CYCLE");

                entity.Property(e => e.PayrollCycleId).HasColumnName("PAYROLL_CYCLE_ID");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(20);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.CutOffDate).HasColumnName("CUT_OFF_DATE");

                entity.Property(e => e.CycleCode)
                    .HasColumnName("CYCLE_CODE")
                    .HasMaxLength(20);

                entity.Property(e => e.DateEnd).HasColumnName("DATE_END");

                entity.Property(e => e.DateStart).HasColumnName("DATE_START");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(20);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.StatusCd)
                    .IsRequired()
                    .HasColumnName("STATUS_CD")
                    .HasMaxLength(20)
                    .HasDefaultValueSql("('ACTIVE')");
            });

            modelBuilder.Entity<TbPayPayrollCycleSetting>(entity =>
            {
                entity.HasKey(e => e.PayrollCycleSettingId);

                entity.ToTable("TB_PAY_PAYROLL_CYCLE_SETTING");

                entity.Property(e => e.PayrollCycleSettingId).HasColumnName("PAYROLL_CYCLE_SETTING_ID");

                entity.Property(e => e.CutOffDay).HasColumnName("CUT_OFF_DAY");

                entity.Property(e => e.PayrollCyclePrequency)
                    .IsRequired()
                    .HasColumnName("PAYROLL_CYCLE_PREQUENCY")
                    .HasMaxLength(20);

                entity.Property(e => e.StartDay).HasColumnName("START_DAY");
            });

            modelBuilder.Entity<TbPayPayrollItem>(entity =>
            {
                entity.HasKey(e => e.ItemId)
                    .HasName("PK_TB_EMP_PAYROLL_ITEM");

                entity.ToTable("TB_PAY_PAYROLL_ITEM");

                entity.Property(e => e.ItemId).HasColumnName("ITEM_ID");

                entity.Property(e => e.MasterPayrollItemId).HasColumnName("MASTER_PAYROLL_ITEM_ID");

                entity.Property(e => e.SchemeId).HasColumnName("SCHEME_ID");

                entity.Property(e => e.SchemeType)
                    .IsRequired()
                    .HasColumnName("SCHEME_TYPE")
                    .HasMaxLength(20);

                entity.HasOne(d => d.MasterPayrollItem)
                    .WithMany(p => p.TbPayPayrollItem)
                    .HasForeignKey(d => d.MasterPayrollItemId)
                    .HasConstraintName("FK_TB_EMP_PAYROLL_ITEM_TB_PAY_MASTER_PAYROLL_ITEM");
            });

            modelBuilder.Entity<TbPayPayrollProfile>(entity =>
            {
                entity.HasKey(e => e.PayrollProfileId);

                entity.ToTable("TB_PAY_PAYROLL_PROFILE");

                entity.Property(e => e.PayrollProfileId).HasColumnName("PAYROLL_PROFILE_ID");

                entity.Property(e => e.ClientId).HasColumnName("CLIENT_ID");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(20);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.EntityId).HasColumnName("ENTITY_ID");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(20);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.ProfileName)
                    .IsRequired()
                    .HasColumnName("PROFILE_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.Remark).HasColumnName("REMARK");

                entity.Property(e => e.StatusCd)
                    .IsRequired()
                    .HasColumnName("STATUS_CD")
                    .HasMaxLength(10);
            });

            modelBuilder.Entity<TbPayPayrollProfileEmployee>(entity =>
            {
                entity.HasKey(e => e.PayrollProfileEmployeeId);

                entity.ToTable("TB_PAY_PAYROLL_PROFILE_EMPLOYEE");

                entity.Property(e => e.PayrollProfileEmployeeId).HasColumnName("PAYROLL_PROFILE_EMPLOYEE_ID");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(20);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.EmployeeId).HasColumnName("EMPLOYEE_ID");

                entity.Property(e => e.ModifedBy)
                    .HasColumnName("MODIFED_BY")
                    .HasMaxLength(20);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.PayrollProfileId).HasColumnName("PAYROLL_PROFILE_ID");

                entity.Property(e => e.StatusCd)
                    .IsRequired()
                    .HasColumnName("STATUS_CD")
                    .HasMaxLength(10);

                entity.HasOne(d => d.PayrollProfile)
                    .WithMany(p => p.TbPayPayrollProfileEmployee)
                    .HasForeignKey(d => d.PayrollProfileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_PAY_PAYROLL_PROFILE_EMPLOYEE_TB_PAY_PAYROLL_PROFILE");
            });

            modelBuilder.Entity<TbPayPayrollProfileItem>(entity =>
            {
                entity.HasKey(e => e.PayrollProfileItemId);

                entity.ToTable("TB_PAY_PAYROLL_PROFILE_ITEM");

                entity.Property(e => e.PayrollProfileItemId).HasColumnName("PAYROLL_PROFILE_ITEM_ID");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(20);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.DisplayName)
                    .HasColumnName("DISPLAY_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.DisplayStatus).HasColumnName("DISPLAY_STATUS");

                entity.Property(e => e.FormulasId).HasColumnName("FORMULAS_ID");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(20);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.PayrollItemId).HasColumnName("PAYROLL_ITEM_ID");

                entity.Property(e => e.PayrollProfileId).HasColumnName("PAYROLL_PROFILE_ID");

                entity.Property(e => e.RecurrentItem).HasColumnName("RECURRENT_ITEM");

                entity.Property(e => e.StatusCd)
                    .IsRequired()
                    .HasColumnName("STATUS_CD")
                    .HasMaxLength(10);
            });

            modelBuilder.Entity<TbPayPayrollRun>(entity =>
            {
                entity.HasKey(e => e.PayrollRunId);

                entity.ToTable("TB_PAY_PAYROLL_RUN");

                entity.Property(e => e.PayrollRunId).HasColumnName("PAYROLL_RUN_ID");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(20);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(20);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.PayrollCycleId).HasColumnName("PAYROLL_CYCLE_ID");

                entity.Property(e => e.Remark).HasColumnName("REMARK");

                entity.Property(e => e.StatusCd)
                    .IsRequired()
                    .HasColumnName("STATUS_CD")
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<TbPayPayrollRunEmployee>(entity =>
            {
                entity.HasKey(e => e.PayrollRunEmployeeId);

                entity.ToTable("TB_PAY_PAYROLL_RUN_EMPLOYEE");

                entity.Property(e => e.PayrollRunEmployeeId).HasColumnName("PAYROLL_RUN_EMPLOYEE_ID");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(20);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.EmployeeId).HasColumnName("EMPLOYEE_ID");

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(20);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.PayrollRunId).HasColumnName("PAYROLL_RUN_ID");

                entity.Property(e => e.RecurrentTransactionFlg).HasColumnName("RECURRENT_TRANSACTION_FLG");

                entity.Property(e => e.ScheduleTransactionFlg).HasColumnName("SCHEDULE_TRANSACTION_FLG");

                entity.Property(e => e.StatusCd)
                    .IsRequired()
                    .HasColumnName("STATUS_CD")
                    .HasMaxLength(20);

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.TbPayPayrollRunEmployee)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_PAY_PAYROLL_RUN_EMPLOYEE_TB_EMP_EMPLOYEE");

                entity.HasOne(d => d.PayrollRun)
                    .WithMany(p => p.TbPayPayrollRunEmployee)
                    .HasForeignKey(d => d.PayrollRunId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_PAY_PAYROLL_RUN_EMPLOYEE_TB_PAY_PAYROLL_RUN");
            });

            modelBuilder.Entity<TbPayPayrollRunImport>(entity =>
            {
                entity.HasKey(e => e.PayrollRunImportId);

                entity.ToTable("TB_PAY_PAYROLL_RUN_IMPORT");

                entity.Property(e => e.PayrollRunImportId).HasColumnName("PAYROLL_RUN_IMPORT_ID");

                entity.Property(e => e.Content)
                    .IsRequired()
                    .HasColumnName("CONTENT");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasColumnName("FILE_NAME");

                entity.Property(e => e.PayrollRunId).HasColumnName("PAYROLL_RUN_ID");

                entity.HasOne(d => d.PayrollRun)
                    .WithMany(p => p.TbPayPayrollRunImport)
                    .HasForeignKey(d => d.PayrollRunId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_PAY_PAYROLL_RUN_IMPORT_TB_PAY_PAYROLL_RUN");
            });

            modelBuilder.Entity<TbPayPayrollRunTransaction>(entity =>
            {
                entity.HasKey(e => e.PayrollRunTransactionId);

                entity.ToTable("TB_PAY_PAYROLL_RUN_TRANSACTION");

                entity.Property(e => e.PayrollRunTransactionId).HasColumnName("PAYROLL_RUN_TRANSACTION_ID");

                entity.Property(e => e.CostCenterId).HasColumnName("COST_CENTER_ID");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(20);

                entity.Property(e => e.CreatedDt).HasColumnName("CREATED_DT");

                entity.Property(e => e.Currency)
                    .HasColumnName("CURRENCY")
                    .HasMaxLength(20);

                entity.Property(e => e.ModifiedBy)
                    .HasColumnName("MODIFIED_BY")
                    .HasMaxLength(20);

                entity.Property(e => e.ModifiedDt).HasColumnName("MODIFIED_DT");

                entity.Property(e => e.PayrollItemId).HasColumnName("PAYROLL_ITEM_ID");

                entity.Property(e => e.PayrollRunEmployeeId).HasColumnName("PAYROLL_RUN_EMPLOYEE_ID");

                entity.Property(e => e.StatusCd)
                    .IsRequired()
                    .HasColumnName("STATUS_CD")
                    .HasMaxLength(20);

                entity.HasOne(d => d.PayrollItem)
                    .WithMany(p => p.TbPayPayrollRunTransaction)
                    .HasForeignKey(d => d.PayrollItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_PAY_PAYROLL_RUN_TRANSACTION_TB_PAY_MASTER_PAYROLL_ITEM");

                entity.HasOne(d => d.PayrollRunEmployee)
                    .WithMany(p => p.TbPayPayrollRunTransaction)
                    .HasForeignKey(d => d.PayrollRunEmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_PAY_PAYROLL_RUN_TRANSACTION_TB_PAY_PAYROLL_RUN_EMPLOYEE");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
