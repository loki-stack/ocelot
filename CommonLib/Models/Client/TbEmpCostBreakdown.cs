﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbEmpCostBreakdown
    {
        public int AllocationId { get; set; }
        public int CostCenterId { get; set; }
        public string CurrencyCd { get; set; }
        public decimal? Portion { get; set; }
        public bool IsRemainder { get; set; }

        public virtual TbEmpCostAllocation Allocation { get; set; }
    }
}
