﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbPayMasterPayrollItemTax
    {
        public int PayrollItemTaxId { get; set; }
        public int PayrollItemId { get; set; }
        public int TaxItemId { get; set; }
        public int TaxItemValue { get; set; }
        public string StatusCd { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }

        public virtual TbPayMasterPayrollItem PayrollItem { get; set; }
    }
}
