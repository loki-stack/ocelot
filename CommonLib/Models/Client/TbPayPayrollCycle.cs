﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbPayPayrollCycle
    {
        public TbPayPayrollCycle()
        {
            TbPayExchangeRate = new HashSet<TbPayExchangeRate>();
        }

        public int PayrollCycleId { get; set; }
        public DateTimeOffset DateStart { get; set; }
        public DateTimeOffset DateEnd { get; set; }
        public string CycleCode { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
        public string ModifiedBy { get; set; }
        public string StatusCd { get; set; }
        public DateTimeOffset? CutOffDate { get; set; }

        public virtual ICollection<TbPayExchangeRate> TbPayExchangeRate { get; set; }
    }
}
