﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbPayPayrollProfile
    {
        public TbPayPayrollProfile()
        {
            TbPayPayrollProfileEmployee = new HashSet<TbPayPayrollProfileEmployee>();
        }

        public int PayrollProfileId { get; set; }
        public int ClientId { get; set; }
        public int EntityId { get; set; }
        public string ProfileName { get; set; }
        public string StatusCd { get; set; }
        public string Remark { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
        public string ModifiedBy { get; set; }

        public virtual ICollection<TbPayPayrollProfileEmployee> TbPayPayrollProfileEmployee { get; set; }
    }
}
