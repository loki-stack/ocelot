﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbPayPayrollProfileItem
    {
        public int PayrollProfileItemId { get; set; }
        public int PayrollProfileId { get; set; }
        public int PayrollItemId { get; set; }
        public string DisplayName { get; set; }
        public byte? DisplayStatus { get; set; }
        public byte? RecurrentItem { get; set; }
        public string StatusCd { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
        public string ModifiedBy { get; set; }
        public int? FormulasId { get; set; }
    }
}
