﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbPayPayrollCycleSetting
    {
        public int PayrollCycleSettingId { get; set; }
        public string PayrollCyclePrequency { get; set; }
        public int StartDay { get; set; }
        public int CutOffDay { get; set; }
    }
}
