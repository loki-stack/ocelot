﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbPayPayrollRunTransaction
    {
        public int PayrollRunTransactionId { get; set; }
        public int PayrollRunEmployeeId { get; set; }
        public int PayrollItemId { get; set; }
        public int CostCenterId { get; set; }
        public string StatusCd { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
        public string ModifiedBy { get; set; }
        public string Currency { get; set; }

        public virtual TbPayMasterPayrollItem PayrollItem { get; set; }
        public virtual TbPayPayrollRunEmployee PayrollRunEmployee { get; set; }
    }
}
