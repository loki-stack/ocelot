﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbPayExchangeRate
    {
        public int ExchangeRateId { get; set; }
        public int PayrollCycleId { get; set; }
        public string CurrencyFrom { get; set; }
        public string CurrencyTo { get; set; }
        public double AverageRate { get; set; }
        public double MonthEndRate { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
        public string ModifiedBy { get; set; }

        public virtual TbPayPayrollCycle PayrollCycle { get; set; }
    }
}
