﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbCfgJobGrade
    {
        public int JobGradeId { get; set; }
        public string JobGradeName { get; set; }
        public int? JobGradeLevel { get; set; }
        public string StatusCd { get; set; }
        public string Remark { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
    }
}
