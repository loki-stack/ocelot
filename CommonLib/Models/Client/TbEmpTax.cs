﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbEmpTax
    {
        public int TaxId { get; set; }
        public int? EmploymentContractId { get; set; }
        public string Ir56bRemarks { get; set; }
        public string Ir56eMonthlyRateOfAllownace { get; set; }
        public string Ir56eFlutuatingEmoluments { get; set; }
        public string Ir56ePreviousEmployerName { get; set; }
        public string Ir56ePreviousEmployerAddress { get; set; }
        public bool? Ir56eShareOptionGranted { get; set; }
        public string Ir56fNewEmployerName { get; set; }
        public string Ir56fNewEmployerAddress { get; set; }
        public string Ir56fAddressAfterTermination { get; set; }
        public DateTime? Ir56gDepartureDate { get; set; }
        public string Ir56gReasonForDeparture { get; set; }
        public string Ir56gReasonForDepartureOthers { get; set; }
        public bool? Ir56gEmployeeReturningHk { get; set; }
        public DateTime? Ir56gEmployeeReturningHkDate { get; set; }
        public string Ir56gAddressAfterTermination { get; set; }
        public bool? Ir56gEmployerHasHoldMoney { get; set; }
        public int? Ir56gHeldAmount { get; set; }
        public string Ir56gReasonForNotHoldingMoney { get; set; }
        public bool? Ir56gTaxBorneByEmployer { get; set; }
        public bool? Ir56gShareOptionGranted { get; set; }
        public DateTime? Ir56gGrantedDate { get; set; }
        public string Ir56gNoOfSharesNotYetExercised { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }

        public virtual TbEmpEmploymentContract EmploymentContract { get; set; }
    }
}
