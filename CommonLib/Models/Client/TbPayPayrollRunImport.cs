﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbPayPayrollRunImport
    {
        public int PayrollRunImportId { get; set; }
        public int PayrollRunId { get; set; }
        public string FileName { get; set; }
        public byte[] Content { get; set; }
        public DateTimeOffset CreatedDt { get; set; }
        public string CreatedBy { get; set; }

        public virtual TbPayPayrollRun PayrollRun { get; set; }
    }
}
