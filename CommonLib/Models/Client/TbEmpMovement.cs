﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbEmpMovement
    {
        public int AssignmentId { get; set; }
        public int? EmploymentContractId { get; set; }
        public int? BusinessUnitId { get; set; }
        public string MovementName { get; set; }
        public string InternalJobTitle { get; set; }
        public string ExternalJobTitle { get; set; }
        public int? JobGrade { get; set; }
        public int? DirectManager { get; set; }
        public int? CostCenter { get; set; }
        public DateTime? AssignmentStartDate { get; set; }
        public int? RemunerationPackage { get; set; }
        public int? BasicSalary { get; set; }
        public string BaseSalaryUnit { get; set; }
        public string DefaultSalaryCurrency { get; set; }
        public string DefaultPaymentCurrency { get; set; }
        public string StaffTags { get; set; }
        public int? SpecificEntityId { get; set; }
        public string Remarks { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
        public int? NotificationRequirements { get; set; }
        public int? WorkCalendar { get; set; }

        public virtual TbEmpEmploymentContract EmploymentContract { get; set; }
    }
}
