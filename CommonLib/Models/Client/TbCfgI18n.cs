﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbCfgI18n
    {
        public string LanguageTag { get; set; }
        public string TextKey { get; set; }
        public string TextValue { get; set; }
        public bool? DefaultFlag { get; set; }
        public string CreateBy { get; set; }
        public DateTimeOffset CreateDt { get; set; }
        public string ModifyBy { get; set; }
        public DateTimeOffset ModifyDt { get; set; }
    }
}
