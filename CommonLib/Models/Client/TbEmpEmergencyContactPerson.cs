﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbEmpEmergencyContactPerson
    {
        public int EmergencyContactPersonId { get; set; }
        public int? EmployeeId { get; set; }
        public string RelationshipToEmployee { get; set; }
        public string RelationshipOther { get; set; }
        public string Name { get; set; }
        public string MobilePhone { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }

        public virtual TbEmpEmployee Employee { get; set; }
    }
}
