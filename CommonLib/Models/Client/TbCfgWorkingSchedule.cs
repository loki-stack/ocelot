﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbCfgWorkingSchedule
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
