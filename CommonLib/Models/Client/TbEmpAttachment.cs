﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbEmpAttachment
    {
        public int AttachmentId { get; set; }
        public int EmployeeId { get; set; }
        public int AttachmentFileId { get; set; }
        public string AttachmentFileName { get; set; }
        public string AttachmentFileRemark { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
    }
}
