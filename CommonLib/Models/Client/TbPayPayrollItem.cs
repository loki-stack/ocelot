﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbPayPayrollItem
    {
        public int ItemId { get; set; }
        public string SchemeType { get; set; }
        public int? SchemeId { get; set; }
        public int? MasterPayrollItemId { get; set; }

        public virtual TbPayMasterPayrollItem MasterPayrollItem { get; set; }
    }
}
