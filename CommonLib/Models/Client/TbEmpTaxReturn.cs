﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbEmpTaxReturn
    {
        public int TaxReturnId { get; set; }
        public int EmployeeId { get; set; }
        public int FileId { get; set; }
        public string TaxReturnDescription { get; set; }
        public DateTime? TaxReturnDt { get; set; }
        public int? TaxReturnYear { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }

        public virtual TbComFile File { get; set; }
    }
}
