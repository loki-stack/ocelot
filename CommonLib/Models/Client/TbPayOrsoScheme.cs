﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbPayOrsoScheme
    {
        public int OrsoSchemeId { get; set; }
        public string OrsoSchemeName { get; set; }
        public string OrsoSchemeType { get; set; }
        public string OrsoSchemeCode { get; set; }
        public string OrsoExemptionNo { get; set; }
        public string ContributionCycle { get; set; }
        public string MpfExemptionNo { get; set; }
        public DateTime? MpfExemptionDate { get; set; }
        public string Domicile { get; set; }
        public DateTime? SchemeEstablishmentDate { get; set; }
        public string InvestmentManagerName { get; set; }
        public string DesignatedPersonName { get; set; }
        public string SchemeAdministratorName { get; set; }
        public string ContactPerson { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string ParticipationNo { get; set; }
        public string PaymentMethod { get; set; }
        public string Currency { get; set; }
        public string PayCentre { get; set; }
        public int? OrsoAdminFee { get; set; }
        public string OrsoAdminUnit { get; set; }
        public string RoundType { get; set; }
        public int? NearestDec { get; set; }
        public string StatusCd { get; set; }
        public DateTime? ClosedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
    }
}
