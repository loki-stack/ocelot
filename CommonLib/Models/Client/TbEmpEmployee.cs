﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbEmpEmployee
    {
        public TbEmpEmployee()
        {
            TbEmpCostAllocation = new HashSet<TbEmpCostAllocation>();
            TbEmpDependent = new HashSet<TbEmpDependent>();
            TbEmpEeBankAccount = new HashSet<TbEmpEeBankAccount>();
            TbEmpEmergencyContactPerson = new HashSet<TbEmpEmergencyContactPerson>();
            TbEmpEmploymentContract = new HashSet<TbEmpEmploymentContract>();
            TbEmpPayslipPassword = new HashSet<TbEmpPayslipPassword>();
            TbPayPayrollRunEmployee = new HashSet<TbPayPayrollRunEmployee>();
        }

        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string ChristianName { get; set; }
        public string DisplayName { get; set; }
        public string NamePrefix { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Gender { get; set; }
        public int? Photo { get; set; }
        public int? Age { get; set; }
        public string MartialStatus { get; set; }
        public string PreferredLanguage { get; set; }
        public string CitizenIdNo { get; set; }
        public string Nationality { get; set; }
        public string PassportIdNo { get; set; }
        public string CountryOfIssue { get; set; }
        public bool? WorkVisaHolder { get; set; }
        public DateTime? WorkArrivingDate { get; set; }
        public DateTime? WorkVisaExpiryDate { get; set; }
        public DateTime? WorkVisaIssueDate { get; set; }
        public DateTime? WorkVisaLandingDate { get; set; }
        public string HomePhone { get; set; }
        public string OfficePhone { get; set; }
        public string MobilePhone { get; set; }
        public string WorkEmail { get; set; }
        public string PersonalEmail { get; set; }
        public string ResidentialAddress1 { get; set; }
        public string ResidentialAddress2 { get; set; }
        public string ResidentialAddress3 { get; set; }
        public string ResidentialDistrict { get; set; }
        public string ResidentialCity { get; set; }
        public string ResidentialState { get; set; }
        public string ResidentialCountry { get; set; }
        public string ResidentialPostalCode { get; set; }
        public string ResidentialHongkongTaxForm { get; set; }
        public string PostalAddress1 { get; set; }
        public string PostalAddress2 { get; set; }
        public string PostalAddress3 { get; set; }
        public string PostalDistrict { get; set; }
        public string PostalCity { get; set; }
        public string PostalState { get; set; }
        public string PostalCountry { get; set; }
        public string PostalPostalCode { get; set; }
        public string PostalHongkongTaxForm { get; set; }
        public string Remarks { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
        public string RemarkDependent { get; set; }
        public string RemarkBankAccount { get; set; }
        public string RemarkContact { get; set; }
        public int? EntityId { get; set; }
        public string RemarkEmploymentContract { get; set; }
        public string RemarkEmploymentMovement { get; set; }
        public string RemarkCompany { get; set; }
        public string RemarkTax { get; set; }
        public bool? OverseasPayments { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string Amount { get; set; }
        public DateTime SysEndTime { get; set; }
        public DateTime SysStartTime { get; set; }
        public string RemarkRentalAddress { get; set; }
        public string Currency { get; set; }
        public string RemarkEmploymentStatus { get; set; }
        public DateTime? MarriageDate { get; set; }
        public string FirstNameSecondary { get; set; }
        public string MiddleNameSecondary { get; set; }
        public string LastNameSecondary { get; set; }
        public string ChristianNameSecondary { get; set; }
        public string DisplayNameSecondary { get; set; }
        public string ResidentialAddress1Secondary { get; set; }
        public string ResidentialAddress2Secondary { get; set; }
        public string ResidentialAddress3Secondary { get; set; }
        public string ResidentialDistrictSecondary { get; set; }
        public string ResidentialCitySecondary { get; set; }
        public string ResidentialStateSecondary { get; set; }
        public string PostalAddress1Secondary { get; set; }
        public string PostalAddress2Secondary { get; set; }
        public string PostalAddress3Secondary { get; set; }
        public string PostalDistrictSecondary { get; set; }
        public string PostalCitySecondary { get; set; }
        public string PostalStateSecondary { get; set; }

        public virtual ICollection<TbEmpCostAllocation> TbEmpCostAllocation { get; set; }
        public virtual ICollection<TbEmpDependent> TbEmpDependent { get; set; }
        public virtual ICollection<TbEmpEeBankAccount> TbEmpEeBankAccount { get; set; }
        public virtual ICollection<TbEmpEmergencyContactPerson> TbEmpEmergencyContactPerson { get; set; }
        public virtual ICollection<TbEmpEmploymentContract> TbEmpEmploymentContract { get; set; }
        public virtual ICollection<TbEmpPayslipPassword> TbEmpPayslipPassword { get; set; }
        public virtual ICollection<TbPayPayrollRunEmployee> TbPayPayrollRunEmployee { get; set; }
    }
}
