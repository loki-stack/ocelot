﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace CommonLib.Models.Client
{
    public partial class TbCamUnit
    {
        public int UnitId { get; set; }
        public int? ParentId { get; set; }
        public int ClientId { get; set; }
        public int EntityId { get; set; }
        public HierarchyId CodeLevel { get; set; }
        public string FullName { get; set; }
        public string DisplayName { get; set; }
        public string UnitCode { get; set; }
        public string StatusCd { get; set; }
        public string Remark { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
        public string ModifiedBy { get; set; }
    }
}
