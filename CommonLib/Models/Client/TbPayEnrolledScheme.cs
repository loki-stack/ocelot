﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbPayEnrolledScheme
    {
        public int EnrolledSchemeId { get; set; }
        public int MpfRegisteredSchemeId { get; set; }
        public string EnrolledSchemeName { get; set; }
        public string EnrolledSchemeCode { get; set; }
        public string EnrolledSchemeType { get; set; }
        public string EnrolledSchemeNo { get; set; }
        public string ContributionCycle { get; set; }
        public string ContactPerson { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string ParticipationNo { get; set; }
        public string PaymentMethod { get; set; }
        public string Currency { get; set; }
        public string PayCentre { get; set; }
        public string RoundType { get; set; }
        public int? NearestDec { get; set; }
        public string StatusCd { get; set; }
        public DateTime? ClosedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
    }
}
