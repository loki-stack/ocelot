﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbPayPayrollProfileEmployee
    {
        public int PayrollProfileEmployeeId { get; set; }
        public int EmployeeId { get; set; }
        public int PayrollProfileId { get; set; }
        public string StatusCd { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
        public string ModifedBy { get; set; }

        public virtual TbPayPayrollProfile PayrollProfile { get; set; }
    }
}
