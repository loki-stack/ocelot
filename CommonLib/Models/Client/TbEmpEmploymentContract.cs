﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbEmpEmploymentContract
    {
        public TbEmpEmploymentContract()
        {
            TbEmpEmploymentStatus = new HashSet<TbEmpEmploymentStatus>();
            TbEmpMovement = new HashSet<TbEmpMovement>();
            TbEmpTax = new HashSet<TbEmpTax>();
            TbEmpTermination = new HashSet<TbEmpTermination>();
        }

        public int EmploymentContractId { get; set; }
        public int? EmployeeId { get; set; }
        public string EmployeeNo { get; set; }
        public string GlobalNo { get; set; }
        public string Remarks { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
        public string StaffType { get; set; }
        public string ContractNo { get; set; }

        public virtual TbEmpEmployee Employee { get; set; }
        public virtual ICollection<TbEmpEmploymentStatus> TbEmpEmploymentStatus { get; set; }
        public virtual ICollection<TbEmpMovement> TbEmpMovement { get; set; }
        public virtual ICollection<TbEmpTax> TbEmpTax { get; set; }
        public virtual ICollection<TbEmpTermination> TbEmpTermination { get; set; }
    }
}
