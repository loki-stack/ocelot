﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbEmpPayslipPassword
    {
        public int PayslipPasswordId { get; set; }
        public int EmployeeId { get; set; }
        public string EncryptedPassword { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }

        public virtual TbEmpEmployee Employee { get; set; }
    }
}
