﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbEmpEmploymentStatus
    {
        public int EmploymentStatusId { get; set; }
        public int EmploymentContractId { get; set; }
        public DateTime StartDate { get; set; }
        public string Status { get; set; }
        public string Remark { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
        public string ModifiedBy { get; set; }
        public bool Confirmed { get; set; }

        public virtual TbEmpEmploymentContract EmploymentContract { get; set; }
    }
}
