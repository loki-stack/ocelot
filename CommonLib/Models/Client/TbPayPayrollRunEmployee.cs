﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbPayPayrollRunEmployee
    {
        public TbPayPayrollRunEmployee()
        {
            TbPayPayrollRunTransaction = new HashSet<TbPayPayrollRunTransaction>();
        }

        public int PayrollRunEmployeeId { get; set; }
        public int PayrollRunId { get; set; }
        public int EmployeeId { get; set; }
        public byte RecurrentTransactionFlg { get; set; }
        public byte ScheduleTransactionFlg { get; set; }
        public string StatusCd { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
        public string ModifiedBy { get; set; }

        public virtual TbEmpEmployee Employee { get; set; }
        public virtual TbPayPayrollRun PayrollRun { get; set; }
        public virtual ICollection<TbPayPayrollRunTransaction> TbPayPayrollRunTransaction { get; set; }
    }
}
