﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbEmpRentalAddress
    {
        public int AddressId { get; set; }
        public int? EmployeeId { get; set; }
        public string Nature { get; set; }
        public string Address { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? RentPaidToLandlordByEmployer { get; set; }
        public int? RentPaidToLandlordByEmployee { get; set; }
        public int? RentRefundedToEmployeeByEmployer { get; set; }
        public int? RentPaidToEmployerByEmployee { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
    }
}
