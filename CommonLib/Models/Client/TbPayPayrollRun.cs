﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbPayPayrollRun
    {
        public TbPayPayrollRun()
        {
            TbPayPayrollRunEmployee = new HashSet<TbPayPayrollRunEmployee>();
            TbPayPayrollRunImport = new HashSet<TbPayPayrollRunImport>();
        }

        public int PayrollRunId { get; set; }
        public string Name { get; set; }
        public int PayrollCycleId { get; set; }
        public string StatusCd { get; set; }
        public string Remark { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
        public string ModifiedBy { get; set; }

        public virtual ICollection<TbPayPayrollRunEmployee> TbPayPayrollRunEmployee { get; set; }
        public virtual ICollection<TbPayPayrollRunImport> TbPayPayrollRunImport { get; set; }
    }
}
