﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbEmpCostAllocation
    {
        public int AllocationId { get; set; }
        public int EntityId { get; set; }
        public int EmployeeId { get; set; }
        public int PayrollItemId { get; set; }
        public bool? IsPercentage { get; set; }
        public DateTime StartDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset ModifiedDt { get; set; }

        public virtual TbEmpEmployee Employee { get; set; }
    }
}
