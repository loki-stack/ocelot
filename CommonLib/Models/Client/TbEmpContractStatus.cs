﻿using System;
using System.Collections.Generic;

namespace CommonLib.Models.Client
{
    public partial class TbEmpContractStatus
    {
        public int? EmploymentContractId { get; set; }
        public string StatusCode { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? Confirmed { get; set; }
        public string Remarks { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }

        public virtual TbEmpEmploymentContract EmploymentContract { get; set; }
    }
}
