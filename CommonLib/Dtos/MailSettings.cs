namespace CommonLib.Dtos
{
    public class MailSettings
    {
        public string SupportEmail { get; set; }
        public int MaximumRetry { get; set; }
        public string EssPortalBaseUrl { get; set; }
        public string TricorPortalBaseUrl { get; set; }
    }
}