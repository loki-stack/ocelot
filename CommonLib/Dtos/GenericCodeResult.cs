﻿namespace CommonLib.Dtos
{
    public class GenericCodeResult
    {
        public int CodeId { get; set; }
        public string CodeLevel { get; set; }
        public string CodeType { get; set; }
        public string CodeValue { get; set; }
        public int DisplayOrder { get; set; }
        public string I18NTextKey { get; set; }
        public string StatusCd { get; set; }
        public string TextValue { get; set; }
    }
}