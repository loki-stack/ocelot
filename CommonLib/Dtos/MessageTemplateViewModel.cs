using System;

namespace CommonLib.Dtos
{
    public class MessageTemplateViewModel
    {

        public int TemplateId { get; set; }
        public int ClientId { get; set; }
        public int EntityId { get; set; }
        public string TemplateCode { get; set; }
        public string TemplateType { get; set; }
        public bool FlagHtml { get; set; }
        public string Portal { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }

    }
}