using System;

namespace CommonLib.Dtos
{
    public class MessageTemplateLangViewModel
    {

        public int TemplateLangId { get; set; }
        public int TemplateId { get; set; }
        public string LangCode { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
    }
}