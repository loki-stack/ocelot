﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonLib.Dtos
{
    public partial class FormulasTagViewResult
    {
        public int FormulasTagId { get; set; }
        public byte? Flag { get; set; }
        public string TagName { get; set; }
        public string Remark { get; set; }
        public string FieldName { get; set; }
        public string Predefined { get; set; }
        public int? FormulasId { get; set; }
        public string ValueType { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
    }
}
