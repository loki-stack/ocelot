﻿using System.Collections.Generic;

namespace CommonLib.Dtos
{
    /// <summary>
    /// The template of response which is returned by endpoint
    /// </summary>
    /// <typeparam name="TData">type of data which is retrieved by api</typeparam>
    public class ApiResponse<TData>
    {
        /// <summary>
        /// Used to track each request.
        /// This value is obtained from request
        /// </summary>
        public string Ray { get; set; }

        /// <summary>
        /// Return parametes back to frontend
        /// </summary>
        public Dictionary<string, object> Parameter { get; set; }

        /// <summary>
        /// The result of a request
        /// </summary>
        public TData Data { get; set; }

        /// <summary>
        /// Message is retured to frondend
        /// </summary>
        public Message Message { get; set; }

        /// <summary>
        /// If result of a request need to redirect to other url, return this value
        /// </summary>
        public string RedirectUrl { get; set; }

        public ApiResponse()
        {
            this.Parameter = new Dictionary<string, object>();
            this.Message = new Message
            {
                Toast = new List<MessageItem>(),
                Alert = new List<MessageItem>(),
                ValidateResult = new List<MessageItem>()
            };
        }

        public ApiResponse(TData data) : this()
        {
            Data = data;
        }
    }

    public class Message
    {
        public List<MessageItem> Toast { get; set; }
        public List<MessageItem> Alert { get; set; }
        public List<MessageItem> ValidateResult { get; set; }
    }

    public class MessageItem
    {
        public MessageItem()
        {

        }

        public MessageItem(string level, string labelCode)
        {
            Level = level;
            LabelCode = labelCode;
        }

        public string ComponentId { get; set; }

        /// <summary>
        /// Values: ERROR, INFO, WARN
        /// </summary>
        public string Level { get; set; }

        /// <summary>
        /// The code of label
        /// </summary>
        public string LabelCode { get; set; }

        public string[] Placeholder { get; set; }
    }
}