namespace CommonLib.Dtos
{
    public class MessageTemplateLangCreateDto
    {
        public int TemplateId { get; set; }
        public string LangCode { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Status { get; set; }


    }
}