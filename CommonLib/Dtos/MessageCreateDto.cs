using System;

namespace CommonLib.Dtos
{
    public class MessageCreateDto
    {
        public int TemplateLangId { get; set; }
        public string Recipient { get; set; }
        public string MessageTitle { get; set; }
        public string MessageContent { get; set; }
        public DateTimeOffset? ProcessedDt { get; set; }
        public string Status { get; set; }
        public int? AttemptCount { get; set; }
        public int? UserId { get; set; }
    }
}