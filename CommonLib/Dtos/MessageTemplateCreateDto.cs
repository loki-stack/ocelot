namespace CommonLib.Dtos
{
    public class MessageTemplateCreateDto
    {
        public int ClientId { get; set; }
        public int EntityId { get; set; }
        public string TemplateCode { get; set; }
        public string TemplateType { get; set; }
        public bool FlagHtml { get; set; }
        public string Portal { get; set; }
        public string Status { get; set; }

    }
}