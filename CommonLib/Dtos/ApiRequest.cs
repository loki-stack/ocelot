﻿namespace CommonLib.Dtos
{
    /// <summary>
    /// The template of request of api
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    public class ApiRequest<TData>
    {
        /// <summary>
        /// The value is generated from frontend which is used to track each request
        /// </summary>
        public string Ray { get; set; }

        /// <summary>
        /// The parameter of a request
        /// </summary>
        public StdRequestParam Parameter { get; set; }

        /// <summary>
        /// Additional information of a request
        /// </summary>
        public TData Data { get; set; }
    }
    /// <summary>
    /// The template of request of api
    /// </summary>
    public class ApiRequest
    {
        /// <summary>
        /// The value is generated from frontend which is used to track each request
        /// </summary>
        public string Ray { get; set; }

        /// <summary>
        /// The parameter of a request
        /// </summary>
        public StdRequestParam Parameter { get; set; }
    }
}