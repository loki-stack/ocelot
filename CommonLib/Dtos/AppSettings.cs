﻿namespace CommonLib.Dtos
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public string CoreServiceGenericCode { get; set; }
        public string EmployeeServiceProfile { get; set; }
    }
}
