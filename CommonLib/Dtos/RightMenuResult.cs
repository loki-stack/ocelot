﻿namespace CommonLib.Dtos
{
    public class RightMenuResult
    {
        public string ModuleCd { get; set; }
        public string FunctionCd { get; set; }
        public string ModuleName { get; set; }
        public string FunctionName { get; set; }
        public string FunctionNature { get; set; }
        public string MenuName { get; set; }
        public int MenuDisplayOrder { get; set; }
    }
}