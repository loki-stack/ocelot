﻿namespace CommonLib.Dtos
{
    /// <summary>
    /// The standard parameters which is added to each request
    /// </summary>
    public class StdRequestParam
    {
        public string Locale { get; set; }
        public int ClientId { get; set; }
        public int EntityId { get; set; }
        public string[] GenericCodeList { get; set; }
    }
}