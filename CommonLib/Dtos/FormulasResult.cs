﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CommonLib.Dtos
{
    public class FormulasResult
    {
        public int FormulasId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public byte Level { get; set; }
        public string StatusCd { get; set; }
        public string Formulas { get; set; }
        public bool? EditAble { get; set; }
        public DateTimeOffset? CreatedDt { get; set; }
        public DateTimeOffset? ModifiedDt { get; set; }
        [NotMapped]
        public string LevelText { get; set; }
        [NotMapped]
        public string StatusText { get; set; }

        public int ClientId { get; set; }
        public int EntityId { get; set; }
    }
}
