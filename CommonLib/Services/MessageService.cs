using CommonLib.Constants;
using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using CommonLib.Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace CommonLib.Services
{
    public class MessageService : IMessageService
    {
        private readonly ILogger<MessageService> _logger;
        protected readonly IMessageRepository _messageRepository;
        private readonly SmtpSettings _smtpSettings;
        private readonly MailSettings _mailSettings;
        protected readonly IMessageTemplateLangRepository _messageTemplateLangRepository;
        protected readonly IMessageTemplateRepository _messageTemplateRepository;

        public MessageService(
            IMessageRepository messageRepository,
            IMessageTemplateLangRepository messageTemplateLangRepository,
            IMessageTemplateRepository messageTemplateRepository,
            ILogger<MessageService> logger,
            IOptions<SmtpSettings> smtpSettings,
            IOptions<MailSettings> mailSettings
        )
        {
            _smtpSettings = smtpSettings.Value;
            _mailSettings = mailSettings.Value;
            _logger = logger;
            _messageRepository = messageRepository;
            _messageTemplateLangRepository = messageTemplateLangRepository;
            _messageTemplateRepository = messageTemplateRepository;
        }

        #region Common

        private async Task<bool> ProcessEmail(TbMsgMessage msg, SmtpClient smtpClient, string username)
        {
            msg.AttemptCount = msg.AttemptCount + 1;
            msg.ProcessedDt = DateTime.Now;
            try
            {
                var mailMessage = new MailMessage
                {
                    From = new MailAddress(_smtpSettings.Username),
                    Subject = msg.MessageTitle,
                    Body = msg.MessageContent,
                    IsBodyHtml = msg.FlagHtml,
                };

                string[] recipients = msg.Recipient.Split(';');

                foreach (var recipient in recipients)
                {
                    mailMessage.To.Add(recipient);
                }

                smtpClient.Send(mailMessage);
                msg.Status = Messages.MessageStatus.SUCCESS;

                await _messageRepository.Update(msg, username);

                return true;


            }
            catch (System.Exception ex)
            {
                msg.Status = Messages.MessageStatus.FAILED;
                if (msg.AttemptCount < _mailSettings.MaximumRetry)
                {
                    msg.Status = Messages.MessageStatus.RETRY;
                }

                await _messageRepository.Update(msg, username);
                _logger.LogError(ex, ex.Message);

                return false;
            }
        }

        /// <summary>
        /// Process Email
        /// </summary>
        /// <returns></returns>
        public async Task<bool> ProcessEmails(string username)
        {
            var watch = new System.Diagnostics.Stopwatch();

            watch.Start();

            var smtpClient = new SmtpClient(_smtpSettings.Host)
            {
                Port = _smtpSettings.Port,
                Credentials = new NetworkCredential(_smtpSettings.Username, _smtpSettings.Password),
                EnableSsl = true, // TODO - move to _smtpSettings
            };

            bool rv = false;
            try
            {

                var pendingMsgs = await _messageRepository.GetPendingMessage();
                foreach (var pendingMsg in pendingMsgs)
                {
                    var isSuccess = ProcessEmail(pendingMsg, smtpClient, username);
                }
                rv = true;
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }
            finally
            {
                watch.Stop();
                _logger.LogInformation($"Execution Time: {watch.ElapsedMilliseconds} ms");
            }
            return rv;

        }

        #endregion Common

        #region Message
        /// <summary>
        /// Update Message
        /// </summary>
        /// <returns></returns>
        public async Task<bool> Update(MessageUpdateDto msg, string username)
        {
            bool result = false;
            try
            {
                TbMsgMessage data = new TbMsgMessage();
                data.MessageId = msg.MessageId;
                data.TemplateLangId = msg.TemplateLangId;
                data.MessageType = msg.MessageType;
                data.Recipient = msg.Recipient;
                data.MessageTitle = msg.MessageTitle;
                data.MessageContent = msg.MessageContent;
                data.ProcessedDt = msg.ProcessedDt;
                data.Status = msg.Status;
                data.AttemptCount = msg.AttemptCount;
                data.Portal = msg.Portal;
                data.FlagHtml = msg.FlagHtml;
                data.UserId = msg.UserId;
                int id = (int)await _messageRepository.Update(data, username);
                if (id > 0)
                {
                    // var createdMsg = await _messageRepository.GetById(id);
                    result = true;
                }
                return result;
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return result;
            }

        }

        /// <summary>
        /// Create Message
        /// </summary>
        /// <returns></returns>
        public async Task<bool> Create(MessageCreateDto msg, string username)
        {
            bool result = false;
            try
            {
                var templateLang = await _messageTemplateLangRepository.GetById(msg.TemplateLangId);
                if (templateLang != null)
                {
                    TbMsgMessage data = new TbMsgMessage();
                    data.TemplateLangId = msg.TemplateLangId;
                    data.MessageType = templateLang.Template.TemplateType;
                    data.Recipient = msg.Recipient;
                    data.MessageTitle = msg.MessageTitle;
                    data.MessageContent = msg.MessageContent;
                    data.ProcessedDt = msg.ProcessedDt;
                    data.Status = Messages.MessageStatus.NEW;
                    data.AttemptCount = msg.AttemptCount;
                    data.Portal = templateLang.Template.Portal;
                    data.FlagHtml = templateLang.Template.FlagHtml;
                    data.UserId = msg.UserId;
                    int id = (int)await _messageRepository.Create(data, username);
                    if (id > 0)
                    {
                        // var createdMsg = await _messageRepository.GetById(id);
                        result = true;
                    }

                }
                return result;

            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return result;
            }

        }

        #endregion Message

        #region Message Template Lang 
        /// <summary>
        /// Update Message Template Lang
        /// </summary>
        /// <returns></returns>
        public async Task<bool> Update(MessageTemplateLangUpdateDto msg, string username)
        {
            bool result = false;
            try
            {
                TbMsgTemplateLang data = new TbMsgTemplateLang();
                data.TemplateLangId = msg.TemplateLangId;
                data.TemplateId = msg.TemplateId;
                data.LangCode = msg.LangCode;
                data.Title = msg.Title;
                data.Content = msg.Content;
                data.Status = msg.Status;
                int id = (int)await _messageTemplateLangRepository.Update(data, username);
                if (id > 0)
                {
                    // var createdMsg = await _messageTemplateLangRepository.GetById(id);
                    result = true;
                }
                return result;

            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return false;
            }

        }

        /// <summary>
        /// Create Message Template Lang
        /// </summary>
        /// <returns></returns>
        public async Task<bool> Create(MessageTemplateLangCreateDto msg, string username)
        {
            bool result = false;
            try
            {
                var template = await _messageTemplateRepository.GetById(msg.TemplateId);
                TbMsgTemplateLang data = new TbMsgTemplateLang();
                if (template != null)
                {
                    data.TemplateId = msg.TemplateId;
                    data.LangCode = msg.LangCode;
                    data.Title = msg.Title;
                    data.Content = msg.Content;
                    data.Status = msg.Status;
                    int id = (int)await _messageTemplateLangRepository.Create(data, username);
                    if (id > 0)
                    {
                        // var createdMsg = await _messageTemplateLangRepository.GetById(id);
                        result = true;
                    }

                }
                return result;
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return result;
            }

        }
        #endregion Message Template Lang 

        #region Message Template 
        /// <summary>
        /// Update Message Template
        /// </summary>
        /// <returns></returns>
        public async Task<bool> Update(MessageTemplateUpdateDto msg, string username)
        {
            bool result = false;
            try
            {
                TbMsgTemplate data = new TbMsgTemplate();
                data.TemplateId = msg.TemplateId;
                data.ClientId = msg.ClientId;
                data.EntityId = msg.EntityId;
                data.TemplateCode = msg.TemplateCode;
                data.TemplateType = msg.TemplateType;
                data.FlagHtml = msg.FlagHtml;
                data.Portal = msg.Portal;
                data.Status = msg.Status;
                int id = (int)await _messageTemplateRepository.Update(data, username);
                if (id > 0)
                {
                    // var createdMsg = await _messageTemplateRepository.GetById(id);
                    result = true;
                }
                return result;

            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return result;
            }

        }

        /// <summary>
        /// Create Message Template
        /// </summary>
        /// <returns></returns>
        public async Task<bool> Create(MessageTemplateCreateDto msg, string username)
        {
            bool result = false;
            try
            {
                TbMsgTemplate data = new TbMsgTemplate();
                data.ClientId = msg.ClientId;
                data.EntityId = msg.EntityId;
                data.TemplateCode = msg.TemplateCode;
                data.TemplateType = msg.TemplateType;
                data.FlagHtml = msg.FlagHtml;
                data.Portal = msg.Portal;
                data.Status = msg.Status;
                int id = (int)await _messageTemplateRepository.Create(data, username);
                if (id > 0)
                {
                    // var createdMsg = await _messageTemplateRepository.GetById(id);
                    result = true;
                }
                return result;
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return result;
            }

        }
        #endregion Message Template

    }
}