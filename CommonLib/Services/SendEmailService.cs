﻿using CommonLib.Dtos;
using CommonLib.EmailTemplate;
using CommonLib.Models;
using CommonLib.Models.Core;
using CommonLib.Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Services
{
    public class SendEmailService : ISendEmailService
    {
        protected readonly IMessageRepository _messageRepository;
        protected readonly IMessageTemplateLangRepository _messageTemplateLangRepository;
        protected readonly IMessageTemplateRepository _messageTemplateRepository;
        private readonly ILogger<SendEmailService> _logger;

        public SendEmailService(IMessageRepository messageRepository,
                            IMessageTemplateLangRepository messageTemplateLangRepository,
                            IMessageTemplateRepository messageTemplateRepository,
                            ILogger<SendEmailService> logger)
        {
            _messageRepository = messageRepository;
            _messageTemplateLangRepository = messageTemplateLangRepository;
            _messageTemplateRepository = messageTemplateRepository;
            _logger = logger;
        }

        public async Task<bool> Send(object emailTemplate, SmtpSettings smtpSetting, MailSettings mailSetting, StdRequestParam requestParam, TbMsgMessage message)
        {
            var smtpClient = new SmtpClient(smtpSetting.Host)
            {
                Port = smtpSetting.Port,
                Credentials = new NetworkCredential(smtpSetting.Username, smtpSetting.Password),
                EnableSsl = true,
            };
            System.Attribute[] emailAttrs = System.Attribute.GetCustomAttributes(emailTemplate.GetType());
            if (emailAttrs != null && emailAttrs.Length > 0)
            {
                EmailInfoAttribute attr = (EmailInfoAttribute)emailAttrs[0];
                string emailCode = attr.EmailCode;
                TbMsgTemplate template = await _messageTemplateRepository.GetById(requestParam.EntityId, requestParam.ClientId, emailCode, CommonLib.Constants.Messages.MessageTemplateType.Email);
                TbMsgTemplateLang templateLang = template.TbMsgTemplateLang.Where(x => x.LangCode == requestParam.Locale).FirstOrDefault();
                if (templateLang != null)
                {
                    string messageTitle = templateLang.Title;
                    string messageContent = templateLang.Content;
                    foreach (PropertyInfo propertyInfo in emailTemplate.GetType().GetProperties())
                    {
                        var emailPropertyAttr = propertyInfo.GetCustomAttributes(typeof(EmailPropertyAttribute), false).Cast<EmailPropertyAttribute>();
                        string value = propertyInfo.GetValue(emailTemplate).ToString();
                        if (emailPropertyAttr.First().Type == (int)EmailProperty.Title)
                        {
                            messageTitle = messageTitle.Replace("$" + propertyInfo.Name + "$", value);
                        }
                        else if (emailPropertyAttr.First().Type == (int)EmailProperty.Content)
                        {
                            messageContent = messageContent.Replace("$" + propertyInfo.Name + "$", value);
                        }
                    }
                    message.TemplateLangId = template.TbMsgTemplateLang.Where(x => x.LangCode == requestParam.Locale).First().TemplateLangId;
                    message.MessageTitle = messageTitle;
                    message.MessageContent = messageContent;
                    long? messageId = await _messageRepository.Create(message, message.CreatedBy);
                    if (messageId != null)
                    {
                        message.MessageId = messageId.Value;
                        var isSuccess = await ProcessEmail(message, smtpClient, smtpSetting, mailSetting, message.CreatedBy);
                        return isSuccess;
                    }
                    else return false;
                }
            }
            return false;
        }

        private async Task<bool> ProcessEmail(TbMsgMessage msg, SmtpClient smtpClient, SmtpSettings smtpSetting, MailSettings mailSetting, string username)
        {
            msg.AttemptCount = msg.AttemptCount + 1;
            msg.ProcessedDt = DateTime.Now;
            try
            {
                var mailMessage = new MailMessage
                {
                    From = new MailAddress(smtpSetting.Username),
                    Subject = msg.MessageTitle,
                    Body = msg.MessageContent,
                    IsBodyHtml = msg.FlagHtml,
                };

                string[] recipients = msg.Recipient.Split(';');

                foreach (var recipient in recipients)
                {
                    mailMessage.To.Add(recipient);
                }

                smtpClient.Send(mailMessage);
                msg.Status = CommonLib.Constants.Messages.MessageStatus.SUCCESS;

                await _messageRepository.Update(msg, username);

                return true;


            }
            catch (System.Exception ex)
            {
                msg.Status = CommonLib.Constants.Messages.MessageStatus.FAILED;
                if (msg.AttemptCount < mailSetting.MaximumRetry)
                {
                    msg.Status = CommonLib.Constants.Messages.MessageStatus.RETRY;
                }

                await _messageRepository.Update(msg, username);
                _logger.LogError(ex, ex.Message);

                return false;
            }
        }
    }
}
