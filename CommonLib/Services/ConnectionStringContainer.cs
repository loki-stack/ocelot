﻿using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CommonLib.Services
{
    public class ConnectionStringContainer : IConnectionStringContainer
    {
        protected Dictionary<int, string> _connectionStrings;
        protected string _connectionString;
        protected readonly IHttpRequestAnalyzer _httpRequestAnalyzer;
        protected readonly CoreDbContext _coreDBContext;

        public ConnectionStringContainer(IHttpRequestAnalyzer httpRequestAnalyzer, CoreDbContext coreDBContext)
        {
            _httpRequestAnalyzer = httpRequestAnalyzer;
            _coreDBContext = coreDBContext;
        }

        public Dictionary<int, string> GetListConnectionString()
        {
            if (_connectionStrings == null)
            {
                _connectionStrings = new Dictionary<int, string>();
                List<TbCamEntity> entities;

                StdRequestParam parameters = _httpRequestAnalyzer.GetStandardParameters();

                if (parameters.ClientId == 0)
                {
                    entities = _coreDBContext.TbCamEntity.ToList();
                }
                else if (parameters.ClientId != 0 && parameters.EntityId == 0)
                {
                    entities = _coreDBContext.TbCamEntity.Where(x => x.ParentClientId == parameters.ClientId).ToList();
                }
                else
                {
                    entities = new List<TbCamEntity>();
                    if (parameters.EntityId != 0 && parameters.EntityId != 0)
                    {
                        entities.Add(_coreDBContext.TbCamEntity.FirstOrDefault(x => x.EntityId == parameters.EntityId));
                    }
                }

                foreach (var entity in entities)
                {
                    _connectionStrings.Add(entity.EntityId, entity.ConnStringParam);
                }
                return _connectionStrings;
            }
            return _connectionStrings;
        }

        public string GetConnectionString()
        {
            if (string.IsNullOrEmpty(_connectionString))
            {
                StdRequestParam parameters = _httpRequestAnalyzer.GetStandardParameters();
                _connectionString = _coreDBContext.TbCamEntity.FirstOrDefault(x => x.EntityId == parameters.EntityId).ConnStringParam;
                
                //TODO: LucTT: Testing
                //_connectionString = "Server=172.20.20.18;Database=HRIS_TENANT;MultipleActiveResultSets=true;User ID=sa;Password=123456a@";
            }
            return _connectionString;
        }

        public DbContext GetClientDbContext(Type type)
        {
            return Activator.CreateInstance(type, GetConnectionString()) as DbContext;
        }
    }
}
