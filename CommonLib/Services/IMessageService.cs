using CommonLib.Dtos;
using System.Threading.Tasks;

namespace CommonLib.Services
{
    public interface IMessageService
    {

        /// <summary>
        /// Process Email
        /// </summary>
        /// <returns></returns>       
        Task<bool> ProcessEmails(string username);


        #region Message
        /// <summary>
        /// Update Message
        /// </summary>
        /// <returns></returns>
        Task<bool> Update(MessageUpdateDto msgMessage, string username);

        /// <summary>
        /// Create Message
        /// </summary>
        /// <returns></returns>
        Task<bool> Create(MessageCreateDto msgMessage, string username);
        #endregion Message

        #region Message Template Lang 
        /// <summary>
        /// Update Message Template Lang
        /// </summary>
        /// <returns></returns>
        Task<bool> Update(MessageTemplateLangUpdateDto msgMessage, string username);

        /// <summary>
        /// Create Message Template Lang
        /// </summary>
        /// <returns></returns>
        Task<bool> Create(MessageTemplateLangCreateDto msgMessage, string username);
        #endregion Message Template Lang 

        #region Message Template 
        /// <summary>
        /// Update Message Template
        /// </summary>
        /// <returns></returns>
        Task<bool> Update(MessageTemplateUpdateDto msgMessage, string username);

        /// <summary>
        /// Create Message Template
        /// </summary>
        /// <returns></returns>
        Task<bool> Create(MessageTemplateCreateDto msgMessage, string username);
        #endregion Message Template 
    }
}