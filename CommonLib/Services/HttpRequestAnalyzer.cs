﻿using CommonLib.Dtos;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;

namespace CommonLib.Services
{
    public class HttpRequestAnalyzer : IHttpRequestAnalyzer
    {
        protected readonly IHttpContextAccessor _httpContext;

        public HttpRequestAnalyzer(IHttpContextAccessor httpContextAccessor)
        {
            _httpContext = httpContextAccessor;
        }

        public StdRequestParam GetStandardParameters()
        {
            var result = new StdRequestParam();
            int id;
            if (_httpContext.HttpContext.Request.Method == "GET")
            {
                var query = _httpContext.HttpContext.Request.Query;
                if (query.ContainsKey("Parameter.ClientId"))
                {
                    if (int.TryParse(query["Parameter.ClientId"].ToString(), out id))
                    {
                        result.ClientId = id;
                    }
                }
                if (query.ContainsKey("Parameter.EntityId"))
                {
                    if (int.TryParse(query["Parameter.EntityId"].ToString(), out id))
                    {
                        result.EntityId = id;
                    }
                }
                if (query.ContainsKey("Parameter.Locale"))
                {
                    result.Locale = query["Parameter.Locale"].ToString();
                }
            }
            else //Other Http method
            {
                var contentType = _httpContext.HttpContext.Request.ContentType.ToString();
                if (contentType == "application/json" || contentType == "application/json; charset=UTF-8" || contentType == "text/plain; charset=UTF-8" || contentType == "text/plain")
                {
                    HttpRequestRewindExtensions.EnableBuffering(_httpContext.HttpContext.Request);
                    using (StreamReader stream = new StreamReader(_httpContext.HttpContext.Request.Body, Encoding.UTF8,
                                                                    detectEncodingFromByteOrderMarks: false,
                                                                    leaveOpen: true))
                    {
                        string requestBody = stream.ReadToEnd();
                        dynamic requestObj = JsonConvert.DeserializeObject(requestBody);
                        dynamic parameter = requestObj.parameter;
                        if (parameter.clientId != null)
                        {
                            result.ClientId = parameter.clientId;
                        }
                        if (parameter.entityId != null)
                        {
                            result.EntityId = parameter.entityId;
                        }
                        result.Locale = parameter.locale;
                        _httpContext.HttpContext.Request.Body.Position = 0;
                    }
                }
                else
                {
                    var clientId = _httpContext.HttpContext.Request.Form["Parameter.clientId"].ToString();
                    var entityId = _httpContext.HttpContext.Request.Form["Parameter.entityId"].ToString();
                    var locale = _httpContext.HttpContext.Request.Form["Parameter.locale"].ToString();

                    if (!string.IsNullOrEmpty(clientId))
                    {
                        result.ClientId = Convert.ToInt32(clientId);
                    }
                    if (!string.IsNullOrEmpty(entityId))
                    {
                        result.EntityId = Convert.ToInt32(entityId);
                    }
                    result.Locale = locale;
                }
            }
            return result;
        }
    }
}