﻿using CommonLib.Dtos;

namespace CommonLib.Services
{
    /// <summary>
    /// The class which is used to analyze a http request
    /// </summary>
    public interface IHttpRequestAnalyzer
    {
        /// <summary>
        /// Get the standard parameters which are passed by http request
        /// </summary>
        /// <returns></returns>
        StdRequestParam GetStandardParameters();
    }
}
