﻿using CommonLib.Dtos;
using CommonLib.Models;
using CommonLib.Models.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Services
{
    public interface ISendEmailService
    {
        Task<bool> Send(object emailTemplate, SmtpSettings smtpSetting, MailSettings mailSetting, StdRequestParam requestParam, TbMsgMessage message);
    }
}
