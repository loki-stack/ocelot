﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace CommonLib.Services
{
    /// <summary>
    /// The interface which is used to get connection strings of entity databases
    /// </summary>
    public interface IConnectionStringContainer
    {
        /// <summary>
        /// Get list of connection string of entity's database when user is client or Global (All) level
        /// </summary>
        /// <returns></returns>
        Dictionary<int, string> GetListConnectionString();

        /// <summary>
        /// Use to retrieve a connection string when user is at enity level
        /// </summary>
        /// <returns></returns>
        string GetConnectionString();

        /// <summary>
        /// return db context which type is type
        /// </summary>
        /// <param name="type">Type of db context</param>
        /// <returns></returns>
        DbContext GetClientDbContext(Type type);
    }
}
