﻿using CommonLib.Dtos;
using System.Collections.Generic;

namespace CommonLib.Validation
{
    /// <summary>
    /// A generic interface for validating a model
    /// </summary>
    public interface IValidator
    {
        /// <summary>
        /// Validate a object, then return a list of <see cref="MessageItem"/>
        /// </summary>
        /// <typeparam name="TModel"> type of the object which is validated </typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        List<MessageItem> Validate<TModel>(TModel model, string componentPrefix = null) where TModel : class;
    }
}
