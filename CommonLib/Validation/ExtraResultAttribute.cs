﻿using System;

namespace CommonLib.Validation
{

    /// <summary>
    /// Hold additional information which is returned to frontend for handling validation result
    /// </summary>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = false)]
    public class ExtraResultAttribute : Attribute
    {
        /// <summary>
        /// Id of a html component which value is validated
        /// </summary>
        public string ComponentId { get; set; }

        /// <summary>
        /// Values: ERROR, INFO, WARN
        /// </summary>
        public string Level { get; set; }

        /// <summary>
        /// The code of label which is used to get i18n message to be displayed
        /// </summary>
        // public string LabelCode { get; set; } //LabelCode should be get from ErrorMessage in ValidateAttribute

        /// <summary>
        /// Values which is replace in a template string which code is "LabelCode"
        /// </summary>
        public string[] Placeholder { get; set; }
    }
}
