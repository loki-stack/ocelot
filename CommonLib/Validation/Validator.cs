﻿using CommonLib.Dtos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace CommonLib.Validation
{
    /// <summary>
    /// Class which is used to validate a model/object
    /// </summary>
    public class Validator : IValidator
    {
        public IServiceProvider ServiceProvider { get; set; }

        public Validator(IServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;
        }

        public List<MessageItem> Validate<T>(T model, string componentPrefix = null) where T : class
        {
            var result = new List<MessageItem>();

            //Validate a model
            Type type = model.GetType();
            PropertyInfo[] properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            ValidationContext vc = new ValidationContext(model, ServiceProvider, null);
            ICollection<ValidationResult> validateResult = new List<ValidationResult>();
            bool isValid = System.ComponentModel.DataAnnotations.Validator.TryValidateObject(model, vc, validateResult, true);

            //Customize result if validation fails
            if (!isValid)
            {
                var propertyNames = new List<string>();
                foreach (var item in validateResult)
                {
                    propertyNames.AddRange(item.MemberNames);
                }
                propertyNames = propertyNames.Distinct().ToList();


                foreach (var property in properties)
                {
                    ValidationResult propResult = validateResult.FirstOrDefault(x => x.MemberNames.Contains(property.Name));
                    if (propResult != null)
                    {
                        var msgInfo = property.GetCustomAttributes(typeof(ExtraResultAttribute), false).Cast<ExtraResultAttribute>();
                        if (msgInfo != null)
                        {
                            foreach (var msg in msgInfo)
                            {
                                result.Add(new MessageItem()
                                {
                                    Level = msg.Level,
                                    ComponentId = string.IsNullOrEmpty(componentPrefix) ? msg.ComponentId : componentPrefix + msg.ComponentId,
                                    LabelCode = propResult.ErrorMessage,
                                    Placeholder = msg.Placeholder
                                });
                            }
                        }
                    }
                }
            }
            return result;
        }
    }
}
