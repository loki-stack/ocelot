﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace CommonLib.Validation
{
    /// <summary>
    /// The class used to disable automatic model state validation.
    /// Please use <see cref="IValidator"/> to validate model/dto manually
    /// </summary>
    public class IgnoreAutoValidator : IObjectModelValidator
    {
        public void Validate(ActionContext actionContext, ValidationStateDictionary validationState, string prefix, object model)
        {
        }
    }
}
