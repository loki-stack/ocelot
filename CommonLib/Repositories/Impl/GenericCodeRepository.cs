﻿using CommonLib.Dtos;
using CommonLib.Models.Core;
using CommonLib.Repositories.Interfaces;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace CommonLib.Repositories.Impl
{
    public class GenericCodeRepository : IGenericCodeRepository
    {
        private readonly AppSettings _appSettings;
        private readonly CoreDbContext _coreDBContext;

        public GenericCodeRepository(IOptions<AppSettings> appSettings, CoreDbContext coreDBContext)
        {
            _appSettings = appSettings.Value;
            _coreDBContext = coreDBContext;
        }

        public async Task<Dictionary<string, List<GenericCodeResult>>> GetGenericCodes(List<string> genericCodeStrs, string token, StdRequestParam requestParam)
        {
            Dictionary<string, List<GenericCodeResult>> result = new Dictionary<string, List<GenericCodeResult>>();
            try
            {
                using (var table = new DataTable())
                {
                    table.Columns.Add("VALUE", typeof(string));

                    if (genericCodeStrs != null && genericCodeStrs.Count() > 0)
                    {
                        foreach (var item in genericCodeStrs)
                        {
                            table.Rows.Add(item);
                        }
                    }

                    var localeParam = new SqlParameter("@locale", requestParam.Locale);
                    var clientIdParam = new SqlParameter("@clientid", requestParam.ClientId);
                    var entityIdParam = new SqlParameter("@entityid", requestParam.EntityId);
                    var codeTypeParams = new SqlParameter("@codeTypes", SqlDbType.Structured)
                    {
                        TypeName = "UDT_STRING_ARRAY",
                        Value = table
                    };

                    var data = await _coreDBContext.Set<GenericCodeResult>()
                        .FromSqlRaw("EXECUTE SP_GET_GENERIC_CODE @locale, @clientid, @entityid, @codeTypes", localeParam, clientIdParam, entityIdParam, codeTypeParams)
                        .ToListAsync();

                    result = data.GroupBy(x => x.CodeType)
                                .ToDictionary(g => g.Key.ToLower(), g => g.Select(x => x).ToList());

                    return result;
                }
            }
            catch (System.Exception)
            {
            }
            return result;
        }
    }
}