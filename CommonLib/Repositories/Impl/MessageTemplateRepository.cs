using CommonLib.Models;
using CommonLib.Models.Core;
using CommonLib.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommonLib.Repositories.Impl
{
    public class MessageTemplateRepository : IMessageTemplateRepository
    {
        protected readonly CoreDbContext _context;
        private readonly ILogger<MessageTemplateRepository> _logger;
        public MessageTemplateRepository(
            CoreDbContext context,
            ILogger<MessageTemplateRepository> logger
            )
        {
            _context = context;
            _logger = logger;
        }

        public async Task<List<TbMsgTemplate>> GetAll(int clientId, int entityId)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    return await _context.TbMsgTemplate
                    .Where(x => x.ClientId == clientId)
                    .Where(x => x.EntityId == entityId)
                    .ToListAsync();
                }
                catch (System.Exception ex)
                {
                    await transaction.RollbackAsync();
                    _logger.LogError(ex, ex.Message);
                    return null;
                }
            }
        }

        public async Task<int?> Update(TbMsgTemplate msg, string username)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    msg.ModifiedBy = username;
                    msg.ModifiedDt = DateTime.Now;
                    _context.TbMsgTemplate.Update(msg);
                    await _context.SaveChangesAsync();
                    await transaction.CommitAsync();
                    return msg.TemplateId;
                }
                catch (System.Exception ex)
                {
                    await transaction.RollbackAsync();
                    _logger.LogError(ex, ex.Message);
                    return null;
                }
            }
        }

        public async Task<TbMsgTemplate> GetById(int id)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    return await _context.TbMsgTemplate.Where(x => x.TemplateId == id).FirstOrDefaultAsync();
                }
                catch (System.Exception ex)
                {
                    await transaction.RollbackAsync();
                    _logger.LogError(ex, ex.Message);
                    return null;
                }
            }
        }
        public async Task<List<TbMsgTemplate>> GetById(int clientId, int entityId)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    return await _context.TbMsgTemplate.Where(x => x.ClientId == clientId && x.EntityId == entityId).ToListAsync();
                }
                catch (System.Exception ex)
                {
                    await transaction.RollbackAsync();
                    _logger.LogError(ex, ex.Message);
                    return null;
                }
            }
        }
        public async Task<TbMsgTemplate> GetById(int clientId, int entityId, string templateCode, string templateType)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var results = await _context.TbMsgTemplate
                                    .Include(x => x.TbMsgTemplateLang)
                                    .Where(x => x.ClientId == clientId && x.EntityId == entityId)
                                    .Where(x => x.TemplateCode == templateCode && x.TemplateType == templateType)
                                    .FirstOrDefaultAsync();
                    return results;
                }
                catch (System.Exception ex)
                {
                    await transaction.RollbackAsync();
                    _logger.LogError(ex, ex.Message);
                    return null;
                }
            }
        }
        public async Task<TbMsgTemplate> GetById(int clientId, string templateCode, string templateType)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var results = await _context.TbMsgTemplate
                                    .Include(x => x.TbMsgTemplateLang)
                                    .Where(x => x.ClientId == clientId)
                                    .Where(x => x.TemplateCode == templateCode && x.TemplateType == templateType)
                                    .FirstOrDefaultAsync();
                    return results;
                }
                catch (System.Exception ex)
                {
                    await transaction.RollbackAsync();
                    _logger.LogError(ex, ex.Message);
                    return null;
                }
            }
        }

        public async Task<int?> Create(TbMsgTemplate msg, string username)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    msg.CreatedBy = username;
                    msg.CreatedDt = DateTime.Now;
                    await _context.AddAsync(msg);
                    await _context.SaveChangesAsync();
                    await transaction.CommitAsync();
                    return msg.TemplateId;
                }
                catch (System.Exception ex)
                {
                    await transaction.RollbackAsync();
                    _logger.LogError(ex, ex.Message);
                    return 0;
                }
            }
        }
    }
}