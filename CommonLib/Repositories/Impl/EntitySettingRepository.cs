﻿using CommonLib.Models;
using CommonLib.Models.Core;
using CommonLib.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace CommonLib.Repositories.Impl
{
    public class EntitySettingRepository : IEntitySettingRepository
    {
        protected readonly CoreDbContext _context;
        public EntitySettingRepository(CoreDbContext context)
        {
            _context = context;
        }

        public async Task<TbCfgSetting> GetSettingOfEntity(int clientId, int entityId)
        {
            return await _context.TbCfgSetting.Where(x => x.SpecificClientId == clientId && x.SpecificEntityId == entityId).FirstOrDefaultAsync();
        }
    }
}
