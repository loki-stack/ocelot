﻿using CommonLib.Models.Core;
using CommonLib.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CommonLib.Repositories.Impl
{
    public class EntityRepository : IEntityRepsitory
    {
        private readonly CoreDbContext _context;

        public EntityRepository(CoreDbContext context)
        {
            _context = context;
        }

        public async Task<List<TbCamEntity>> GetAll()
        {
            return await _context.TbCamEntity.ToListAsync();
        }
    }
}