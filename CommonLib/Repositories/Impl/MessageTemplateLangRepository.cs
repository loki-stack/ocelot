using CommonLib.Models;
using CommonLib.Models.Core;
using CommonLib.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommonLib.Repositories.Impl
{
    public class MessageTemplateLangRepository : IMessageTemplateLangRepository
    {
        protected readonly CoreDbContext _context;
        private readonly ILogger<MessageTemplateLangRepository> _logger;
        public MessageTemplateLangRepository(
            CoreDbContext context,
            ILogger<MessageTemplateLangRepository> logger
            )
        {
            _context = context;
            _logger = logger;
        }

        public async Task<List<TbMsgTemplateLang>> GetAllByTemplateId(int id)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    return await _context.TbMsgTemplateLang.Where(x => x.TemplateId == id).ToListAsync();
                }
                catch (System.Exception ex)
                {
                    await transaction.RollbackAsync();
                    // _logger.LogError(ex, ex.Message);
                    _logger.LogError(ex.Message); // caller to log the stacktrace
                    throw ex;
                }
            }
        }

        public async Task<TbMsgTemplateLang> GetByTemplateIdAndLang(int id, string langCode)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    return await _context.TbMsgTemplateLang
                        .Where(x => (x.TemplateId == id) && (string.Equals(x.LangCode, langCode)))
                        .FirstOrDefaultAsync();
                }
                catch (System.Exception ex)
                {
                    await transaction.RollbackAsync();
                    // _logger.LogError(ex, ex.Message);
                    _logger.LogError(ex.Message); // caller to log the stacktrace
                    throw ex;
                }
            }
        }

        public async Task<int?> Update(TbMsgTemplateLang msg, string username)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    msg.ModifiedBy = username;
                    msg.ModifiedDt = DateTime.Now;
                    _context.TbMsgTemplateLang.Update(msg);
                    await _context.SaveChangesAsync();
                    await transaction.CommitAsync();
                    return msg.TemplateLangId;
                }
                catch (System.Exception ex)
                {
                    await transaction.RollbackAsync();
                    // _logger.LogError(ex, ex.Message);
                    _logger.LogError(ex.Message); // caller to log the stacktrace
                    throw ex;
                }
            }
        }

        public async Task<TbMsgTemplateLang> GetById(int id)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var result = await _context.TbMsgTemplateLang
                                                .Include(x => x.Template)
                                                .Where(x => x.TemplateLangId == id)
                                                .FirstOrDefaultAsync();
                    return result;
                }
                catch (System.Exception ex)
                {
                    await transaction.RollbackAsync();
                    // _logger.LogError(ex, ex.Message);
                    _logger.LogError(ex.Message); // caller to log the stacktrace
                    throw ex;
                }
            }
        }

        public async Task<int?> Create(TbMsgTemplateLang msg, string username)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    msg.CreatedBy = username;
                    msg.CreatedDt = DateTime.Now;
                    await _context.AddAsync(msg);
                    await _context.SaveChangesAsync();
                    await transaction.CommitAsync();
                    return msg.TemplateLangId;
                }
                catch (System.Exception ex)
                {
                    await transaction.RollbackAsync();
                    // _logger.LogError(ex, ex.Message);
                    _logger.LogError(ex.Message); // caller to log the stacktrace
                    throw ex;
                }
            }
        }
    }
}