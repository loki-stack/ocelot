using CommonLib.Constants;
using CommonLib.Models;
using CommonLib.Models.Core;
using CommonLib.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommonLib.Repositories.Impl
{
    public class MessageRepository : IMessageRepository
    {
        protected readonly CoreDbContext _context;
        private readonly ILogger<MessageRepository> _logger;
        public MessageRepository(
            CoreDbContext context,
            ILogger<MessageRepository> logger
            )
        {
            _context = context;
            _logger = logger;
        }

        public async Task<List<TbMsgMessage>> GetAll(int userId)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    return await _context.TbMsgMessage.Where(x => x.UserId == userId).ToListAsync();
                }
                catch (System.Exception ex)
                {
                    await transaction.RollbackAsync();
                    _logger.LogError(ex, ex.Message);
                    return null;
                }
            }
        }

        public async Task<long?> Update(TbMsgMessage msg, string username)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var result = await _context.TbMsgMessage.Where(x => x.MessageId == msg.MessageId).FirstOrDefaultAsync();
                    msg.ModifiedBy = username;
                    msg.ModifiedDt = DateTime.Now;
                    msg.AttemptCount = result.AttemptCount++;
                    _context.TbMsgMessage.Update(msg);
                    // _context.Entry(msg).State = EntityState.Modified;
                    await _context.SaveChangesAsync();
                    await transaction.CommitAsync();
                    return msg.MessageId;
                }
                catch (System.Exception ex)
                {
                    await transaction.RollbackAsync();
                    _logger.LogError(ex, ex.Message);
                    return 0;
                }
            }
        }

        public async Task<TbMsgMessage> GetById(int id)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    return await _context.TbMsgMessage.Where(x => x.MessageId == id).FirstOrDefaultAsync();
                }
                catch (System.Exception ex)
                {
                    await transaction.RollbackAsync();
                    _logger.LogError(ex, ex.Message);
                    return null;
                }
            }
        }

        public async Task<List<TbMsgMessage>> GetByStatus(string status)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    return await _context.TbMsgMessage.Where(x => x.Status == status).ToListAsync();
                }
                catch (System.Exception ex)
                {
                    await transaction.RollbackAsync();
                    _logger.LogError(ex, ex.Message);
                    return null;
                }
            }
        }

        public async Task<long?> Create(TbMsgMessage msg, string username)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    msg.CreatedBy = username;
                    msg.CreatedDt = DateTime.Now;
                    msg.AttemptCount = 0;
                    await _context.AddAsync(msg);
                    await _context.SaveChangesAsync();
                    await transaction.CommitAsync();
                    return msg.MessageId;
                }
                catch (System.Exception ex)
                {
                    await transaction.RollbackAsync();
                    _logger.LogError(ex, ex.Message);
                    return null;
                }
            }
        }

        public async Task<List<TbMsgMessage>> GetPendingMessage()
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    return await _context.TbMsgMessage
                    .Include(x => x.TemplateLang)
                    .ThenInclude(x => x.Template)
                    .Where(
                        x =>
                        x.Status == Messages.MessageStatus.NEW ||
                        x.Status == Messages.MessageStatus.RETRY
                        ).
                        ToListAsync();
                }
                catch (System.Exception ex)
                {
                    await transaction.RollbackAsync();
                    _logger.LogError(ex, ex.Message);
                    return null;
                }
            }
        }
    }
}