﻿using CommonLib.Dtos;
using CommonLib.Models.Core;
using CommonLib.Repositories.Impl;
using CommonLib.Repositories.Interfaces;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace CommonLib.Repositories.Impl
{
    public class FormulasRepository : IFormulasRepository
    {

        private readonly AppSettings _appSettings;
        private readonly CoreDbContext _coreDBContext;

        public FormulasRepository(IOptions<AppSettings> appSettings, CoreDbContext coreDBContext)
        {
            _appSettings = appSettings.Value;
            _coreDBContext = coreDBContext;
        }

        public async Task<List<FormulasResult>> GetFormulas(int clientId, int entityId)
        {
            List<FormulasResult> result = new List<FormulasResult>();

            var clientIdPar = new SqlParameter("@clientId", clientId);
            var entityIdPar = new SqlParameter("@entityId", entityId);

            return await _coreDBContext.Set<FormulasResult>().FromSqlRaw("EXECUTE GET_FORMULAS @clientId, @entityId", clientIdPar, entityIdPar).ToListAsync();
        }

        public async Task<List<FormulasTagViewResult>> GetFormulasTag()
        {
            return await _coreDBContext.Set<FormulasTagViewResult>().FromSqlRaw("EXECUTE GET_FORMULAS_TAG").ToListAsync();
        }
    }
}
