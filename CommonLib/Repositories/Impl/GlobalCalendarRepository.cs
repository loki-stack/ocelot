﻿using CommonLib.Models;
using CommonLib.Models.Core;
using CommonLib.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommonLib.Repositories.Impl
{
    public class GlobalCalendarRepository : IGlobalCalendarRepository
    {
        private readonly CoreDbContext _context;

        public GlobalCalendarRepository(CoreDbContext context)
        {
            _context = context;
        }

        public async Task<List<TbCfgGlobalCalendar>> GetAll()
        {
            return await _context.TbCfgGlobalCalendar.ToListAsync();
        }

        public async Task<List<TbCfgHoliday>> GetHolidayOfGlobalCalendar(int calendarId)
        {
            return await _context.TbCfgHoliday.Where(x => x.CalendarId == calendarId).ToListAsync();
        }
    }
}