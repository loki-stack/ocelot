﻿using CommonLib.Models;
using CommonLib.Models.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CommonLib.Repositories.Interfaces
{
    public interface IGlobalCalendarRepository
    {
        Task<List<TbCfgGlobalCalendar>> GetAll();

        Task<List<TbCfgHoliday>> GetHolidayOfGlobalCalendar(int calendarId);
    }
}