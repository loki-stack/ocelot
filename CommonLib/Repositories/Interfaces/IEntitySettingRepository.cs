﻿using CommonLib.Models;
using CommonLib.Models.Core;
using System.Threading.Tasks;

namespace CommonLib.Repositories.Interfaces
{
    public interface IEntitySettingRepository
    {
        Task<TbCfgSetting> GetSettingOfEntity(int clientId, int entityId);
    }
}
