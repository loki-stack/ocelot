using CommonLib.Models;
using CommonLib.Models.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CommonLib.Repositories.Interfaces
{
    public interface IMessageTemplateRepository
    {
        Task<List<TbMsgTemplate>> GetAll(int clientId, int entityId);
        Task<int?> Update(TbMsgTemplate msg, string username);
        Task<TbMsgTemplate> GetById(int id);
        Task<List<TbMsgTemplate>> GetById(int clientId, int entityId);
        Task<TbMsgTemplate> GetById(int clientId, int entityId, string templateCode, string templateType);
        Task<TbMsgTemplate> GetById(int clientId, string templateCode, string templateType);
        Task<int?> Create(TbMsgTemplate msg, string username);
    }
}