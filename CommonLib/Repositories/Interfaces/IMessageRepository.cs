using CommonLib.Models;
using CommonLib.Models.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CommonLib.Repositories.Interfaces
{
    public interface IMessageRepository
    {
        Task<List<TbMsgMessage>> GetAll(int userId);
        Task<long?> Update(TbMsgMessage msg, string username);
        Task<TbMsgMessage> GetById(int id);
        Task<List<TbMsgMessage>> GetByStatus(string status);
        Task<long?> Create(TbMsgMessage msg, string username);
        Task<List<TbMsgMessage>> GetPendingMessage();

    }
}