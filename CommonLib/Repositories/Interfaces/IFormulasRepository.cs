﻿using CommonLib.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Repositories.Interfaces
{
    public interface IFormulasRepository
    {
        Task<List<FormulasResult>> GetFormulas(int clientId, int entityId);
        Task<List<FormulasTagViewResult>> GetFormulasTag();
    }
}
