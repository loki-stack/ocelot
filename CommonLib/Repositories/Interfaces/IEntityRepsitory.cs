﻿using CommonLib.Models;
using CommonLib.Models.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CommonLib.Repositories.Interfaces
{
    public interface IEntityRepsitory
    {
        Task<List<TbCamEntity>> GetAll();
    }
}