using CommonLib.Models;
using CommonLib.Models.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CommonLib.Repositories.Interfaces
{
    public interface IMessageTemplateLangRepository
    {
        Task<List<TbMsgTemplateLang>> GetAllByTemplateId(int id);
        Task<int?> Update(TbMsgTemplateLang msg, string username);
        Task<int?> Create(TbMsgTemplateLang msg, string username);
        Task<TbMsgTemplateLang> GetById(int id);
        Task<TbMsgTemplateLang> GetByTemplateIdAndLang(int id, string langCode);
    }
}