﻿using CommonLib.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CommonLib.Repositories.Interfaces
{
    public interface IGenericCodeRepository
    {
        Task<Dictionary<string, List<GenericCodeResult>>> GetGenericCodes(List<string> genericCodes, string token, StdRequestParam param);
    }
}
