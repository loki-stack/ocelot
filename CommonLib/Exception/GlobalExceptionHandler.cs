﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace CommonLib.Exception
{
    /// <summary>
    /// The class which is used to handle all exception globally.
    /// So Developer no need use "try catch" block in Controller layer
    /// </summary>
    public class GlobalExceptionHandler
    {
        public ILoggerFactory LoggerFactory { get; set; }
        public GlobalExceptionHandler(ILoggerFactory loggerFactory)
        {
            LoggerFactory = loggerFactory;
        }

        /// <summary>
        /// Hanle an exception
        /// </summary>
        /// <param name="context"> The context in which an exception is thrown </param>
        /// <returns></returns>
        public async Task Handle(HttpContext context)
        {
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            context.Response.ContentType = "application/json";

            var ex = context.Features.Get<IExceptionHandlerFeature>();
            if (ex != null)
            {
                ILogger logger = LoggerFactory.CreateLogger(typeof(GlobalExceptionHandler));
                context.Request.Body.Seek(0, SeekOrigin.Begin);
                string bodyRequest;
                using (StreamReader stream = new StreamReader(context.Request.Body))
                {
                    bodyRequest = stream.ReadToEnd();
                }
                logger.LogError(ex.Error, context.Request.Path.Value + Environment.NewLine + bodyRequest);
            }
        }
    }
}
