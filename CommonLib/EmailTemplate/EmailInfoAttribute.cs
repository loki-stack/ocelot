﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonLib.EmailTemplate
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public sealed class EmailInfoAttribute : Attribute
    {
        public string EmailCode { get; set; }
    }
}
