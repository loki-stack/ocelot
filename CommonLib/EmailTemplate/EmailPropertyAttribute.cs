﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonLib.EmailTemplate
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public sealed class EmailPropertyAttribute : Attribute
    {
        public int Type { get; set; }
    }
}
