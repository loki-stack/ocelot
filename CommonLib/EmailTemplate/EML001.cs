﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonLib.EmailTemplate
{
    [EmailInfo(EmailCode = "EML001")]
    public class EML001
    {
        [EmailProperty(Type = (int)EmailProperty.Content)]
        public string UserName { get; set; }

        [EmailProperty(Type = (int)EmailProperty.Content)]
        public string Link { get; set; }
    }
}
