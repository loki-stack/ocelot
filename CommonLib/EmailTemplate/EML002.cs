﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonLib.EmailTemplate
{
    [EmailInfo(EmailCode = "EML002")]
    public class EML002
    {
        [EmailProperty(Type = (int)EmailProperty.Content)]
        public string EmployeeName { get; set; }
    }
}
