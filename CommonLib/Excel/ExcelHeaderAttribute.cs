﻿using System;

namespace CommonLib.Excel
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public sealed class ExcelHeaderAttribute : Attribute
    {
        public string HeaderName { get; set; }
    }
}
