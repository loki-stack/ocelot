﻿using System;

namespace CommonLib.Excel
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public sealed class SheetInfoAttribute : Attribute
    {
        public SheetType Type { get; set; }
        public string SheetName { get; set; }
    }
}
