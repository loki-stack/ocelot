﻿using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace CommonLib.Excel
{
    /// <summary>
    /// The class which is used to deal with excel file
    /// </summary>
    public class ExcelUtil
    {
        /// <summary>
        /// Parse an excel file which is read by <paramref name="stream"/>
        /// </summary>
        /// <param name="stream">Stream from which data is read</param>
        /// <param name="configuration">The configuration which is used to parse data in the excel file</param>
        /// <param name="errors">list of error if have</param>
        /// <returns> A Dictionary with key is name of the class which is corresponding datatype of a row in a sheet, and value is a list of data, each element is a row in the sheet </returns>
        public Dictionary<string, IEnumerable<object>> Parse(Stream stream, Configuration configuration, ref List<Error> errors)
        {

            XSSFWorkbook wb = new XSSFWorkbook(stream);
            List<Type> sheetTypes = configuration.SheetTypes;
            TimeSpan browserTimeOffset = configuration.BrowserTimeOffset;

            Dictionary<string, IEnumerable<object>> results = new Dictionary<string, IEnumerable<object>>();
            for (int i = 0; i < sheetTypes.Count; i++)
            {
                System.Attribute[] attrs = System.Attribute.GetCustomAttributes(sheetTypes[i]);

                if (attrs != null && attrs.Length > 0)
                {
                    SheetInfoAttribute sheetTypeAttr = (SheetInfoAttribute)attrs[0];
                    ISheet sheet = wb.GetSheet(sheetTypeAttr.SheetName);
                    if (sheet != null)
                    {
                        IEnumerable<object> result = ParseSheet(sheet, sheetTypes[i], sheetTypeAttr.Type, browserTimeOffset, ref errors);
                        results.Add(sheetTypes[i].Name, result);
                    }
                }
            }
            return results;
        }

        public IEnumerable<object> ParseSheet(ISheet sheet, Type modelType, SheetType sheetType, TimeSpan browserTimeOffset, ref List<Error> errors)
        {
            var results = new List<object>();
            var excelDescriptors = InitializeExcelDescriptors<object>(modelType);
            int rowIndex = 0;
            Dictionary<int, List<string>> rowDic = new Dictionary<int, List<string>>();
            while (rowIndex <= sheet.LastRowNum)
            {
                var nowRow = sheet.GetRow(rowIndex);
                int cellNum = nowRow.LastCellNum;
                List<string> rowData = new List<string>();
                for (int j = 0; j < cellNum; j++)
                {
                    var cell = nowRow.GetCell(j);

                    if (cell == null)
                    {
                        rowData.Add(string.Empty);
                        continue;
                    }

                    switch (cell.CellType)
                    {
                        case NPOI.SS.UserModel.CellType.Numeric:
                            if (HSSFDateUtil.IsCellDateFormatted(cell))
                            {
                                rowData.Add(cell.DateCellValue.ToString());
                            }
                            else rowData.Add(cell.NumericCellValue.ToString());
                            break;
                        case NPOI.SS.UserModel.CellType.String:
                            rowData.Add(cell.StringCellValue);
                            break;
                        case NPOI.SS.UserModel.CellType.Formula:
                            if (HSSFDateUtil.IsCellDateFormatted(cell))
                            {
                                rowData.Add(cell.DateCellValue.ToString());
                            }
                            else rowData.Add(cell.NumericCellValue.ToString());
                            break;
                        default:
                            rowData.Add(cell.StringCellValue);
                            break;
                    }
                }
                rowDic.Add(rowIndex, rowData);
                rowIndex++;
            }
            var boundIndexExcelDescriptors = BindIndexToExcelDescriptors(rowDic, excelDescriptors, sheetType);
            var sortedCsvDescriptors = new SortedDictionary<int, ExcelDescriptor>(boundIndexExcelDescriptors.ToDictionary(x => x.Index, x => x));
            if (sheetType == SheetType.HORIZONTAL)
            {
                rowDic.Remove(0);
                for (int i = 0; i < rowDic.Count; i++)
                {
                    results.Add(BindData(sheet.SheetName, modelType, rowDic.ElementAt(i).Key, rowDic.ElementAt(i).Value, sortedCsvDescriptors, browserTimeOffset, ref errors));
                }
            }
            else if (sheetType == SheetType.VERTICAL)
            {
                var array = rowDic.Values.ToArray();
                int count = array.First().Count;
                for (int j = 1; j < count; j++)
                {
                    List<string> row = new List<string>();
                    for (int k = 0; k < array.Length; k++)
                    {
                        row.Add(array[k][j]);
                    }
                    results.Add(BindData(sheet.SheetName, modelType, j, row, sortedCsvDescriptors, browserTimeOffset, ref errors));
                }
            }
            return results;
        }

        private static IEnumerable<ExcelDescriptor> InitializeExcelDescriptors<T>(Type modelType) where T : new()
        {
            var targetType = modelType;
            var properties = targetType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            return from prop in properties
                   let customAttributes = prop.GetCustomAttributes(typeof(ExcelHeaderAttribute), false).Cast<ExcelHeaderAttribute>()
                   from attribute in customAttributes
                   where !string.IsNullOrEmpty(attribute.HeaderName)
                   select new ExcelDescriptor(prop, attribute.HeaderName);
        }

        private static IEnumerable<ExcelDescriptor> BindIndexToExcelDescriptors(Dictionary<int, List<string>> dic, IEnumerable<ExcelDescriptor> excelDescriptors, SheetType sheetType)
        {
            var descriptors = new List<ExcelDescriptor>(excelDescriptors);
            List<string> headers = new List<string>();
            if (sheetType == (int)SheetType.HORIZONTAL)
            {
                headers = dic[0];

            }
            else if (sheetType == SheetType.VERTICAL)
            {
                foreach (var item in dic)
                {
                    headers.Add(item.Value[0]);
                }
            }
            for (var i = 0; i < headers.Count; i++)
            {
                var header = headers[i];
                var excelDescriptor = descriptors.FirstOrDefault(x => x.HeaderName == header.Trim());

                if (excelDescriptor == null)
                {
                    continue;
                }
                excelDescriptor.Index = i;
            }

            return descriptors;
        }

        private static object BindData(string sheetName, Type modelType, int rowNum, List<string> data, IDictionary<int, ExcelDescriptor> excelDescriptors, TimeSpan browserTimeOffset, ref List<Error> errors)
        {
            object row = Activator.CreateInstance(modelType);
            int intOut = 0;
            DateTime dateTimeOut = new DateTime();
            bool boolOut = false;
            double doubleOut = 0;
            Type dateTimeOffsetNull = typeof(DateTimeOffset?);
            Type dateTimeNull = typeof(DateTime?);
            for (var i = 0; i < data.Count; i++)
            {
                if (excelDescriptors.ContainsKey(i))
                {
                    var excelDescriptor = excelDescriptors[i];
                    var col = data[i];

                    if (!string.IsNullOrEmpty(col))
                    {
                        if (excelDescriptor.PropertyInfo.PropertyType == typeof(int))
                        {
                            if (!int.TryParse(col, out intOut))
                            {
                                errors.Add(new Error
                                {
                                    SheetName = sheetName,
                                    Row = rowNum + 1,
                                    Column = i + 1,
                                    Message = excelDescriptor.PropertyInfo.Name + " - convert int - " + col
                                });
                                continue;
                            }
                        }
                        if (excelDescriptor.PropertyInfo.PropertyType == typeof(DateTime?))
                        {
                            if (!DateTime.TryParse(col, out dateTimeOut))
                            {
                                errors.Add(new Error
                                {
                                    SheetName = sheetName,
                                    Row = rowNum + 1,
                                    Column = i + 1,
                                    Message = excelDescriptor.PropertyInfo.Name + " - convert date time - " + col
                                });
                                continue;
                            }
                        }

                        if (excelDescriptor.PropertyInfo.PropertyType == typeof(double))
                        {
                            if (!double.TryParse(col, out doubleOut))
                            {
                                errors.Add(new Error
                                {
                                    SheetName = sheetName,
                                    Row = rowNum + 1,
                                    Column = i + 1,
                                    Message = excelDescriptor.PropertyInfo.Name + " - convert double - " + col
                                });
                                continue;
                            }
                        }

                        if (excelDescriptor.PropertyInfo.PropertyType == typeof(bool))
                        {
                            if (!bool.TryParse(col, out boolOut))
                            {
                                errors.Add(new Error
                                {
                                    SheetName = sheetName,
                                    Row = rowNum + 1,
                                    Column = i + 1,
                                    Message = excelDescriptor.PropertyInfo.Name + " - convert bool - " + col
                                });
                                continue;
                            }
                        }

                        if (excelDescriptor.PropertyInfo.PropertyType == dateTimeOffsetNull)
                        {
                            object d;
                            if (dateTimeOffsetNull.IsGenericType && dateTimeOffsetNull.GetGenericTypeDefinition() == typeof(Nullable<>))
                            {
                                if (String.IsNullOrEmpty(col))
                                    d = null;
                                else
                                    d = Convert.ChangeType(col, dateTimeNull.GetGenericArguments()[0]);
                            }
                            else
                            {
                                d = Convert.ChangeType(col, dateTimeNull);
                            }
                            excelDescriptor.PropertyInfo.SetValue(
                                obj: row,
                                value: new DateTimeOffset(Convert.ToDateTime(d), browserTimeOffset),
                                index: null
                            );
                        }
                        else
                        {
                            excelDescriptor.PropertyInfo.SetValue(
                                obj: row,
                                value: Convert.ChangeType(col, excelDescriptor.PropertyInfo.PropertyType),
                                index: null
                            );
                        }
                    }
                }
            }
            return row;
        }

        /// <summary>
        /// Export List of data to a <paramref name="outStream"/>
        /// </summary>
        /// <param name="configuration"> The configuration which is used to export data into the excel file</param>
        /// <param name="outStream"> Data is saved to this stream </param>
        /// <param name="sheetdata"> List of data which is exported </param>
        public void Export(Configuration configuration, Stream outStream, params IEnumerable<object>[] sheetdata)
        {
            if (outStream == null)
                throw new ApplicationException("Please provide a stream");

            XSSFWorkbook wb = new XSSFWorkbook();
            foreach (var model in sheetdata)
            {
                var type = GetListType(model);
                System.Attribute[] attrs = System.Attribute.GetCustomAttributes(type);
                SheetInfoAttribute sheetTypeAttr = new SheetInfoAttribute();
                if (attrs != null && attrs.Length > 0)
                {
                    sheetTypeAttr = (SheetInfoAttribute)attrs[0];
                    ISheet sheet = wb.CreateSheet(sheetTypeAttr.SheetName);
                    System.Collections.IList collection = model.ToList();
                    ICellStyle headerStyle = wb.CreateCellStyle();
                    IFont headerFont = wb.CreateFont();

                    headerStyle.FillForegroundColor = HSSFColor.Grey40Percent.Index;
                    headerStyle.FillPattern = FillPattern.SolidForeground;
                    headerStyle.BorderBottom = BorderStyle.Double;
                    headerStyle.BorderRight = BorderStyle.Double;
                    headerStyle.BorderTop = BorderStyle.Thin;
                    headerStyle.BottomBorderColor = HSSFColor.Black.Index;
                    headerStyle.RightBorderColor = HSSFColor.Black.Index;
                    headerStyle.TopBorderColor = HSSFColor.Black.Index;
                    headerStyle.WrapText = true;
                    headerFont.IsBold = true;
                    headerFont.Color = HSSFColor.White.Index;
                    headerStyle.SetFont(headerFont);

                    if (sheetTypeAttr.Type == SheetType.HORIZONTAL)
                    {
                        var rowHeader = sheet.CreateRow(0);
                        int index = 0;

                        //create header
                        foreach (PropertyInfo propertyInfo in type.GetProperties())
                        {
                            ICell headerCell = rowHeader.CreateCell(index);
                            var customAttributes = propertyInfo.GetCustomAttributes(typeof(ExcelHeaderAttribute), false).Cast<ExcelHeaderAttribute>();
                            if (customAttributes != null && customAttributes.Count() > 0)
                            {
                                if (!string.IsNullOrEmpty(customAttributes.First().HeaderName))
                                {
                                    headerCell.SetCellValue(customAttributes.First().HeaderName);
                                }
                                else headerCell.SetCellValue(propertyInfo.Name);
                            }
                            headerCell.CellStyle = headerStyle;
                            index++;
                        }
                        int rowIndex = 1;
                        foreach (var item in collection)
                        {
                            index = 0;
                            var newRow = sheet.CreateRow(rowIndex);
                            Type itemType = item.GetType();
                            foreach (PropertyInfo propertyInfo in itemType.GetProperties())
                            {
                                if (itemType.GetProperty(propertyInfo.Name).GetValue(item) != null)
                                {
                                    newRow.CreateCell(index).SetCellValue(itemType.GetProperty(propertyInfo.Name).GetValue(item).ToString());
                                }
                                else newRow.CreateCell(index).SetCellValue(String.Empty);
                                index++;
                            }
                            rowIndex++;
                        }
                    }
                    else if (sheetTypeAttr.Type == SheetType.VERTICAL)
                    {
                        Dictionary<string, List<string>> rows = new Dictionary<string, List<string>>();
                        foreach (PropertyInfo propertyInfo in type.GetProperties())
                        {
                            rows.Add(propertyInfo.Name, new List<string>());
                        }
                        foreach (var item in collection)
                        {
                            Type itemType = item.GetType();
                            foreach (PropertyInfo propertyInfo in item.GetType().GetProperties())
                            {
                                var value = itemType.GetProperty(propertyInfo.Name).GetValue(item).ToString();
                                rows[propertyInfo.Name].Add(value);
                            }
                        }
                        int rowIndex = 0;
                        int index = 0;
                        foreach (var row in rows)
                        {
                            index = 0;
                            var newRow = sheet.CreateRow(rowIndex);
                            ICell headerCell = newRow.CreateCell(index);
                            headerCell.SetCellValue(row.Key);
                            headerCell.CellStyle = headerStyle;
                            foreach (var str in row.Value)
                            {
                                index++;
                                newRow.CreateCell(index).SetCellValue(str);
                            }
                            rowIndex++;
                        }
                    }
                }
            }
            wb.Write(outStream);
        }
        
        /// <summary>
        /// Get type of list object
        /// </summary>
        /// <param name="enumerable"></param>
        /// <returns></returns>
        private Type GetListType(IEnumerable enumerable)
        {
            var type = enumerable.GetType();
            var enumerableType = type
                .GetInterfaces()
                .Where(x => x.GetGenericTypeDefinition() == typeof(IEnumerable<>))
                .First();
            return enumerableType.GetGenericArguments()[0];
        }
    }
}
