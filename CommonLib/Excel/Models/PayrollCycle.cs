﻿using System;

namespace CommonLib.Excel.Models
{
    [SheetInfo(Type = SheetType.HORIZONTAL, SheetName = "Payroll Cycles")]
    public class PayrollCycle
    {
        [ExcelHeader(HeaderName = "Cycle Name")]
        public string CycleName { get; set; }
        [ExcelHeader(HeaderName = "Start Date")]
        public DateTimeOffset? StartDate { get; set; }
        [ExcelHeader(HeaderName = "End Date")]
        public DateTimeOffset? EndDate { get; set; }
    }
}
