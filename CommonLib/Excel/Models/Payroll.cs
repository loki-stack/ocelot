﻿using System;

namespace CommonLib.Excel.Models
{
    [SheetInfo(Type = SheetType.VERTICAL, SheetName = "Payroll")]
    public class Payroll
    {
        [ExcelHeader(HeaderName = "Client ID")]
        public string ClientId { get; set; }
        [ExcelHeader(HeaderName = "Payroll Run ID")]
        public string PayrollRunId { get; set; }
        [ExcelHeader(HeaderName = "Default Payment Currency")]
        public string DefaultPaymentCurrency { get; set; }
        [ExcelHeader(HeaderName = "Export Time")]
        public DateTimeOffset? ExportTime { get; set; }
        [ExcelHeader(HeaderName = "Exported By")]
        public string ExportBy { get; set; }
    }
}
