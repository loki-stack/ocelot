﻿namespace CommonLib.Excel.Models
{
    [SheetInfo(Type = SheetType.HORIZONTAL, SheetName = "Master Payroll Items")]
    public class MasterPayrollItem
    {
        [ExcelHeader(HeaderName = "Payroll Code")]
        public string PayrollCode { get; set; }
        [ExcelHeader(HeaderName = "Item Name")]
        public string ItemName { get; set; }
        [ExcelHeader(HeaderName = "Sign")]
        public string Sign { get; set; }
        [ExcelHeader(HeaderName = "Recurrence")]
        public string Recurrence { get; set; }
    }
}
