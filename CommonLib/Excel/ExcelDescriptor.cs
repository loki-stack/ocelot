﻿using System.Reflection;

namespace CommonLib.Excel
{
    public class ExcelDescriptor
    {
        public PropertyInfo PropertyInfo { get; set; }
        public string HeaderName { get; set; }
        public int Index { get; set; }
        public ExcelDescriptor(PropertyInfo propertyInfo, string headerName)
        {
            PropertyInfo = propertyInfo;
            HeaderName = headerName;
        }
    }
}
