﻿using System;
using System.Collections.Generic;

namespace CommonLib.Excel
{
    /// <summary>
    /// Configuration which is used for importing/exporting an excel file
    /// </summary>
    public class Configuration
    {
        /// <summary>
        /// Gets the time's offset from Coordinated Universal Time (UTC).
        /// The difference between the current time value at frontend
        /// and Coordinated Universal Time (UTC).
        /// </summary>
        public TimeSpan BrowserTimeOffset { get; set; }

        /// <summary>
        /// List of representative type of a row in a specific sheet
        /// </summary>
        public List<Type> SheetTypes { get; set; }
    }
}
