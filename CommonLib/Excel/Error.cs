﻿namespace CommonLib.Excel
{
    /// <summary>
    /// Store information when error occurs when importing or exporting from/to an excel file
    /// </summary>
    public class Error
    {
        /// <summary>
        /// Name of the sheet in which error occurs
        /// </summary>
        public string SheetName { get; set; }

        /// <summary>
        /// Row number at which error occurs
        /// </summary>
        public int Row { get; set; }

        /// <summary>
        /// Column number at which error occurs
        /// </summary>
        public int Column { get; set; }

        /// <summary>
        /// Content of the error
        /// </summary>
        public string Message { get; set; }
    }
}
