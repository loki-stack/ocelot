using CommonLib.Dtos;
using Microsoft.EntityFrameworkCore;

namespace CommonLib.Utils
{
    public class DbContextUtil
    {
        /// <summary>
        /// Create options for a db context
        /// </summary>
        /// <typeparam name="TDbContext">The actual Db context class</typeparam>
        /// <param name="connectionStr"> The connection string which is used to access a database </param>
        /// <returns></returns>
        public static DbContextOptions CreateDDRConnection<TDbContext>(string connectionStr) where TDbContext : DbContext
        {
            var optionsBuilder = new DbContextOptionsBuilder<TDbContext>();
            var options = optionsBuilder.UseSqlServer(connectionStr, conf =>
            {
                conf.UseHierarchyId();
            }).Options;
            return options;
        }
    }
}