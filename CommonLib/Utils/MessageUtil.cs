using CommonLib.Dtos;

namespace CommonLib.Utils
{
    public class MessageUtil
    {
        /// <summary>
        /// Shorthand to add toast message to response
        /// </summary>
        /// <param name="response"></param>
        /// <param name="level"></param>
        /// <param name="labelCode"></param>
        /// <typeparam name="TData"></typeparam>
        public static void AddToast<TData>(ApiResponse<TData> response, string level, string labelCode)
        {
            if (response != null)
            {
                response.Message.Toast.Add(
                    new MessageItem
                    {
                        Level = level,
                        LabelCode = labelCode
                    }
                );
            }
        }
    }
}