﻿namespace CommonLib.Constants
{
    public class Level
    {
        public const string ERROR = "ERROR";
        public const string SUCCESS = "SUCCESS";
        public const string INFO = "INFO";
        public const string WARN = "WARN";
    }

}
