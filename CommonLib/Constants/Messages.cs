namespace CommonLib.Constants
{
    public class Messages
    {
        public class MessageStatus
        {
            /// <summary>
            /// Status Code for new created email message
            /// </summary>
            /// <returns></returns>
            public const string NEW = "NW";

            /// <summary>
            /// Status Code for email message with failed sending attempt
            /// </summary>
            /// <returns></returns>
            public const string RETRY = "RT";

            /// <summary>
            /// Status Code for email message with success sending attempt
            /// </summary>
            /// <returns></returns>
            public const string SUCCESS = "SC";

            /// <summary>
            /// Status Code for email message cancelled by user
            /// </summary>
            /// <returns></returns>
            public const string CANCELLED = "XX";

            /// <summary>
            /// Status Code for email message attempated count reach max attempted
            /// </summary>
            /// <returns></returns>
            public const string FAILED = "FL";
        }
        public class MessageTemplateStatus
        {
            /// <summary>
            /// Status Code for active message
            /// </summary>
            /// <returns></returns>
            public const string ACTIVE = "AT";

            /// <summary>
            /// Status Code for inactive message
            /// </summary>
            /// <returns></returns>
            public const string INACTIVE = "IN";
        }
        public class MessageTemplateCode
        {

            /// <summary>
            /// Message Template for creating new entity
            /// </summary>
            /// <returns></returns>
            public const string EM_ENTITY_NEW = "EM_ENTITY_NEW";
            /// <summary>
            /// Forgot password email for ESS portal 
            /// </summary>
            public const string EM_ESS_FORGOT_PWD = "EM_ESS_FORGOT_PWD";

        }
        public class MessageTemplateType
        {

            /// <summary>
            /// Email
            /// </summary>
            /// <returns></returns>
            public const string Email = "EML";

            /// <summary>
            /// In-App Notification
            /// </summary>
            /// <returns></returns>
            public const string App = "APP";

            /// <summary>
            /// Cloud messaging
            /// </summary>
            /// <returns></returns>
            public const string CloudSms = "CMS";
        }
        public class Portal
        {
            /// <summary>
            /// hris-bs
            /// </summary>
            /// <returns></returns>
            public const string BS = "BS";

            /// <summary>
            /// hris-hr
            /// </summary>
            /// <returns></returns>
            public const string HR = "HR";

            /// <summary>
            /// hris-ee
            /// </summary>
            /// <returns></returns>
            public const string EE = "EE";
        }
        public class TemplateType
        {
            /// <summary>
            /// Message Template for Application (Apps)
            /// </summary>
            /// <returns></returns>
            public const string Application = "APP";

            /// <summary>
            /// Message Template for Email (Email)
            /// </summary>
            /// <returns></returns>
            public const string Email = "EML";

            /// <summary>
            /// Message Template for Notification GCM
            /// </summary>
            /// <returns></returns>
            public const string Notification = "NOT";
        }
    }
}